#!/bin/bash
# NODE_VERSION=14.15.4

# # workaround to override the v8 alias
# npm config delete prefix

# echo "export NODE_BINARY='node --expose-gc --max_old_space_size=4096'" >> ~/.bashrc
# echo "export NODE_OPTIONS=--max_old_space_size=4096" >> ~/.bashrc
# cd $APPCENTER_SOURCE_DIRECTORY
pwd
echo $APPCENTER_SOURCE_DIRECTORY
# $APPCENTER_SOURCE_DIRECTORY = $APPCENTER_SOURCE_DIRECTORY/mobile/platforms/android
# mv gradle_appcenter.properties gradle.properties
# nvm install "$NODE_VERSION"
# nvm alias node8 "$NODE_VERSION"
echo "npm install"
date
npm install
echo "npm install -g cordova"
date
npm install -g cordova
# if [ "$APPCENTER_BRANCH" != "master" ];
# then
# cd ../../scripts/
# sh update_modules_version.sh $APPCENTER_BRANCH
# cd $APPCENTER_SOURCE_DIRECTORY
# fi
echo "npm run build-prod"
date
npm run build -prod
# cordova platform rm android
# cordova platform add android
cordova plugin rm cordova-plugin-kiosk
cordova plugin add cordova-plugin-kiosk
#cordova plugin rm cordova-plugin-kiosk
#cordova plugin add cordova-plugin-kiosk

echo "cordova prepare android"
cordova prepare android

