#!/bin/bash
##  copie e cole o comando abaixo dentro de /autoatendimento para rodar o script ##
## chmod +x mobile/buildar.sh && mobile/buildar.sh ##
date 
## variaveis do ambiente necessarias para build ##
## certifique de ter instalado o java-1.8.0-openjdk e o gradle-5.1.1 ##
echo "java_home"
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
export PATH=${PATH}:/home/GRUPOTEKNISA/$user/.gradle/wrapper/dists/gradle-5.1.1-all/97z1ksx6lirer3kbvdnh7jtjg/gradle-5.1.1/bin
export PATH=${PATH}:/home/GRUPOTEKNISA/$user/Android/Sdk/platform-tools/
export PATH=${PATH}:/home/GRUPOTEKNISA/$user/Android/Sdk/build-tools/
export PATH=${PATH}:/home/GRUPOTEKNISA/$user/Android/Sdk/tools/
echo $JAVA_HOME
cd mobile
## dependencias necessarias, se não estiver instalado descomente as linhas abaixo ##

# echo "npm install"
# npm install
# echo "cordova install"
# npm install -g cordova

## concertar erros no aplicativo na funçaão de barrar a saida ##
## concertar erros de build por conta do plugin cordova-plugin-kiosk ##
echo "fix platforms"
cordova platform rm android
cordova platform add android
echo "fix plugin kiosk"
cordova plugin rm cordova-plugin-kiosk
cordova plugin add cordova-plugin-kiosk
cordova plugin rm cordova-plugin-kiosk
cordova plugin add cordova-plugin-kiosk
cordova prepare android
## gerar a dist para buildar ##
echo "run build -prod"
npm run build -prod

## gerar a build para release ##
echo "cordova build"
cordova build android --release

## para buildar para debug descomente abaixo e comente acima ##
#  cordova build android --debug