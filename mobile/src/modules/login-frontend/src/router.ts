//General
import Vue from 'vue';
import Router from 'vue-router';

const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);

Vue.use(Router);
const router = new Router({
  routes: [
  ]
});
export default router;