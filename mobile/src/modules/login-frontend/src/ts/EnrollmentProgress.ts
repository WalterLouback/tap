import { Component, Vue } from 'vue-property-decorator';
import moment from 'moment';
import router from '../router';
import Request from '@/ts/Request';
@Component
export default class EnrollmentProgress extends Vue {
    
    static mapScreenNames: any[] = [
        {
            'webService': 'exige_primeira_aula',
            'screenName': 'ClassEnrollFirst',
            'order': 0
        },
        {
            'webService': 'exige_avaliacao',
            'screenName': 'ClassEnrollAvaliation',
            'order': 1
        },
        {
            'webService': 'exige_parq',
            'screenName': 'Questionarie',
            'order': 2
        },
        {
            'webService': 'exige_atestado',
            'screenName': 'ClassEnrollAttest',
            'order': 3
        }
    ];

    static goNextEnrollmentScreen(webService: any, currentScreen: string, isFirstScreen: boolean) {
        let workflow : any = Object.entries(webService)
        .filter((web: any) => web[1] === 'S') 
        .map((web: any) => {
            return EnrollmentProgress.mapScreenNames
            .find((m: any) => m.webService === web[0]);
        })
        .sort((a: any, b: any) => a.order >= b.order ? 1 : -1)
        .map((obj: any) => obj.screenName);
        
        let nextScreen : any;
        if(currentScreen != "") nextScreen = workflow[workflow.findIndex((w: any) => w === currentScreen) + 1]
        
        if (isFirstScreen === true) {
            router.push(workflow[0]);
        } else if (nextScreen === undefined || nextScreen === null || nextScreen === '') {
            var registrationObj = this.createFinalObject()
            this.send(registrationObj);
        } else {
            router.push(nextScreen);
        }
    }

    static createFinalObject() {
        let currentClass: any = JSON.parse(localStorage.getItem('CURRENT_CLASS_DETAILS') || '[]');
        let codModalidade =  currentClass.cod_mod_curso;  
        let codTurma =  currentClass.cod_turma;
        let codCurso =  currentClass.cod_curso;       
        let firstClass = localStorage.getItem('FIRSTCLASS_ANSWER') || '';
        let evaluation = localStorage.getItem('EVALUATION_ANSWER') || '';
        
        let parQ = JSON.parse(localStorage.getItem('PARQ_ANSWER') || '');

        firstClass && firstClass != ''  ?  firstClass : '';
        evaluation && evaluation != '' ? evaluation : '';
        parQ && parQ != '' ? parQ : '';

        let registrationObj = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT'),
            'EXTERNAL_ID' : localStorage.getItem('DEPENDENT_EXT_ID'),
            'CMC': codModalidade,
            'CT' : codTurma,
            'CC' : codCurso,
            'IDP' : firstClass,
            'IDA'  : evaluation,
            'ATESTADO' : '',
            'DATA_ATT' : '',
            'PARQ' : parQ
        }
        return registrationObj;
    }

    static send(body: any) {
        try {
            const req: any = new Request();
            req.body = body;
            req.route = 'setPermanentEnroll';
            //getPermanentEnroll
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                if (response && response.response && response.response.res && response.response.res.status == 'ok') {
                    let mens = response.response.res.mens;

                    localStorage.removeItem('FIRSTCLASS_ANSWER');
                    localStorage.removeItem('EVALUATION_ANSWER');
                    localStorage.removeItem('PARQ_ANSWER');

                    localStorage.setItem('CAME_FROM_ENROLL', 'true');
                    router.push('ClassEnrollList');
                    req.showSuccessMessage('Inscrição realizada com sucesso.');
               } else if (response && response.response && response.response.res && response.response.res.des_erro) {
                    let mens = response.response.res.des_erro;
                    req.showErrorMessage(mens);
               }
            };
            req.callbackError = (error: any) => {
                req.showErrorMessage(error);                
            };
            req.send();
            req.loadingMessage = 'Realizando inscrição...';
            req.blockTouchEvents = true;;
        } catch (e) {
            console.log(e)
        }
    }

    /*Sample promise*/
    static freshStart(currentClass: any, lastScreen: string){
        if (currentClass && lastScreen) {
            this.checkNext(currentClass, lastScreen)
            .then((next: any) => {
                router.push(next)
            }).catch(() => {

            });
        }
    }

    static checkNext(a: any, b : any) {
        return new Promise((resolve: any, reject: any) => {
            switch (a) {
                case '1':
                    resolve('tela 1');
                    break;
                case '2':
                    resolve('tela 2');
                    break;
                default:
                    reject();
            };
        });
    }
    /* End sample */

}