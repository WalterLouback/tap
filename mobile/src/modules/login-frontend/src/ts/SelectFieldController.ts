import Vue from 'vue';
import { Component } from 'vue-property-decorator';

declare var M: any;
declare var $: any;
@Component
export default class SelectFieldController extends Vue {
    static refresh(): void {
        $('select').formSelect();
        const optionSpans = document.querySelectorAll('.dropdown-content>li span');
        for (const span in optionSpans) {
            if (!$.isNumeric(optionSpans[span])) optionSpans[span].className += ' primary-color';
        }
    }
}
