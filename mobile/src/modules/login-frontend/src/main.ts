import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import App from './App.vue';
import router from './router';
import store from './store';
import './font-awesome-imports';
import "./thirdparty/vivify.min.css";
import './thirdparty/card/card.js';
import './thirdparty/card/card.css';
import './thirdparty/slick/slick.css';
import './thirdparty/slick/slick.js';
import './thirdparty/slick/slick-theme.css';
import './thirdparty/to-title-case.js';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
//import './stylesheet/app.scss';
//import './stylesheet/components.scss';
//import './stylesheet/views.scss';
import "./thirdparty/fonts.css";

Vue.config.productionTip = true;
Vue.config.devtools = true;

declare var $: any;

Vue.use(VueAnalytics, {
  id: 'UA-144027546-2',
  router,
  // debug: {
    //   enabled: true, // default value
    //   trace: true, // default value
    //   sendHitTask: true // default value
    // }
  })
  
  new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

