import { Component, Prop, Vue } from 'vue-property-decorator';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SectionArea from '@/components/SectionArea.vue';
import TextLink from '@/components/TextLink.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import DefaultButtonOrd from '@/components/DefaultButtonOrd.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import ValueBalance from '@/components/ValueBalance.vue';
import Dependent from './Dependent';
import Request from '@/ts/Request';
// import BiometryCheck from '@/ts/BiometryCheck';
import Util from '@/ts/Util';
import {VMoney} from 'v-money';

declare var M: any;
declare var $: any;

@Component({
    components: { HeaderFluxo, TextLink, DefaultButton, ModalCodigo, ValueBalance, DefaultButtonOrd, SectionArea, VMoney }
})

export default class AddBalance extends Vue {
    creditCards: Array<object> = [];
    mainCartao: any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
    values: any = [];
    outroValor: any = true;
    valueInput : any = '0';
    mainCardId : number = 0;
    showModal: boolean = false;
    showModalTelaCartoes: boolean = false;
    showModalSuccess: boolean = false;
    creditSuccess: boolean = false;
    USER_DATA = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    DEPENDENT_USERS = JSON.parse(localStorage.getItem('USER_DEPENDENTS') || '[]');
    USER_BALANCE = JSON.parse(localStorage.getItem('WALLET') || '{}');
    errorMessage: string = 'Ocorreu um erro desconhecido. Por favor, tente novamente mais tarde.'
    modalPasswordRequest: boolean = false;
    passwordValue: any = '';
    passwordWrong: boolean = false;
    passwordUser: any = localStorage.getItem('QBTT');
    acceptFinger: any = localStorage.getItem('ACCEPT_BIOMETRY') || '';
    // biometry: BiometryCheck = new BiometryCheck();
    card: any;
	BALANCE?: number;

    money: any = {
        decimal: ',',
        thousands: '.',
        prefix: 'R$ ',
        suffix: '',
        precision: 2,
        masked: false /* doesn't work with directive */
    };

    verifyChecked(ID: any){
        if($('input#valueOutroValor').prop('checked') == true) {
            $('#anotherValueBalance').removeClass('display-none').addClass('display-flex');
        }
        if($('input#valueOutroValor').prop('checked') == false){
            $('#anotherValueBalance').removeClass('display-flex').addClass('display-none');
            $('#balance').val('');
            let value = this.values.find((value: any) => value.ID == ID).VALUE;
            this.valueInput = value;
            // if(typeof value === 'number') this.valueInput = value;
            // if(typeof value != 'number') this.valueInput = 0;
        }
    }

    telaPaymentMethods(){
        localStorage.setItem('deliverToBalcony', 'false');
        localStorage.setItem('deliversToTable', 'true');
        this.$router.push('PaymentMethod');
    }

    goToExtrato() {
        this.$router.push('ExtratoEvents');
    }

    goBack() {
        this.$router.go(-1);
    }

    goToTelaCartoes() {
        this.$router.push('RegisteredCards');
    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getCreditCards();
        this.getUserDependents();
        this.getUserData();
        if(this.USER_DATA && this.USER_DATA.ORGANIZATION_DATA) this.BALANCE = this.USER_DATA.ORGANIZATION_DATA.BALANCE
        const self = this;
        this.values.push(
            { 'ID': 0, 'VALUE': 'R$ 100,00' },
            { 'ID': 1, 'VALUE': 'R$ 200,00' },
            { 'ID': 2, 'VALUE': 'R$ 300,00' },
            { 'ID': 3, 'VALUE': 'R$ 400,00' },
            { 'ID': 4, 'VALUE': 'R$ 500,00' },
            { 'ID': 5, 'VALUE': 'R$ 600,00' },
            { 'ID': 'OutroValor', 'VALUE': 'OUTRO VALOR' }
        );
        new Dependent().getMainCardId(function(res:any) { self.mainCardId = res.user.MAIN_CARD_ID; });
        $('#balance').focus(function() { $('#balance')[0].select() });
        $('#balance').focus();
    }

    getCreditCards() {

        try {
            const self = this;
            const req = new Request();
                req.body = {
                    'USER_ID': localStorage.getItem('USER_ID'),
                    'TOKEN': localStorage.getItem('TOKEN'),
                    'NRORG': localStorage.getItem('NRORG'),
            };
            req.route  = 'getValidCreditCard';
            req.callbackSuccess = function(response: any) {
                let creditCards = response.creditcards;
                self.creditCards = creditCards;
                if (response.creditcardFromParent) self.creditCards.push(response.creditcardFromParent);
                self.mainCartao = {};
                self.creditCards.forEach(( card:any ) => {
                    if(card.IS_MAIN_CARD == true){
                        self.mainCartao = card;
                    }
                });
                localStorage.setItem('MAIN_CARTAO', JSON.stringify(self.mainCartao))
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e){console.log(e);}
    }

    getUserData(){
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT'),
                'USER_TYPE_ID': 1
            };
            req.route = 'getUserData';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('USER_DATA', JSON.stringify(response.user));
                self.mainCardId = response.user.MAIN_CARD_ID; 
                self.card = self.findMainCard(self.mainCardId, self.creditCards);
				self.USER_DATA = response.user;
				self.BALANCE   = response.user.ORGANIZATION_DATA.BALANCE;
                self.getBalance();
            };
            req.callbackError = function(error: any) {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    getBalance() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID')
            };
            req.route = 'getBalance';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('WALLET', JSON.stringify(response.wallet));
                self.USER_BALANCE = response.wallet;
            };
            req.callbackError = function(error: any) {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    findMainCard(mainCardId: number, cards: any) {
        return cards.find((card:any) => card.ID == mainCardId);
    }

    lastNumberCard( lastNumbers:any ){
        let lastNumber = '';
        if (lastNumbers)
            lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
        return lastNumber;
    }
    
    addBalance(){
        var self = this;
        /* Buscando o cartão principal para poder realizar o pagamento */
        try {
            let valor = Number(self.valueInput.split(' ')[1].replace(',', '.'));
            const req = new Request();
                req.body = {
                    "NRORG": localStorage.getItem('NRORG'),
                    "TOKEN": localStorage.getItem('TOKEN'),
                    "USER_ID": localStorage.getItem('USER_ID'),
                    "CARD_ID": self.mainCartao.ID,
                    "USER_RECEIVED_ID": localStorage.getItem('USER_RECEIVED_ID'),
                    "BALANCE": valor,
                    "EVENT_ID": 9,
                    "USER_TYPE_ID" : 1,
                };
                req.route  = 'createOrderWallet';
                req.callbackSuccess = function(response: any){
                    // self.getUserData();
                    self.openCreditSuccess('Seu crédito foi comprado com sucesso!');
                };
                req.callbackError = function(error: any) {
                if (error.response.data.errorCode == 12)
                    var mensagem:string = 'Seu limite mensal neste cartão de crédito foi excedido.';
                else if (error.response.data.errorCode == 5)
                    var mensagem:string = 'Esta transação não foi foi autorizada pelo seu cartão.';
                else var mensagem: string = 'Não foi possível realizar esta transação.';
                    self.mostraModal(mensagem);
                }
                req.blockTouchEvents = true;
                req.send();
        } catch (e){
            console.log(e);
        }
    }

    openPasswordRequest(){
        const self = this;
        if (!self.valueInput || self.valueInput == 'R$ 0') self.mostraModal('Por favor, escolha um valor para realizar a recarga.');
        else if(!self.mainCartao.FLAG) self.mostraModalTelaCartoes();
        else if(self.acceptFinger == 'true'){
            // self.biometry.useTouchID(function() {
            //     self.addBalance();
            // });
        } else self.modalPasswordRequest = true;
    }

    closePasswordRequest(){
        this.modalPasswordRequest = false;
        this.passwordValue = '';
        this.passwordWrong = false;
    }

    afterPasswordRequest(){
        let self = this;
        if(this.passwordValue == this.passwordUser){
            this.addBalance();
            this.modalPasswordRequest = false;
        }else if(this.passwordValue != this.passwordUser){
            self.passwordWrong = false;
            setTimeout(function(){self.passwordWrong = true;}, 10);
        }
    }

    getUserDependents() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'USER_TYPE_ID': 1
            };
            req.route = 'getUserDependents';
            req.callbackSuccess = function(response: any) {
                self.DEPENDENT_USERS = response.dependents;
                localStorage.setItem('USER_DEPENDENTS', JSON.stringify(response.dependents));
            };
            req.callbackError = function(error: any) {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                console.log(e);
            }
    }

    openCreditSuccess( msg:any ){
        this.errorMessage = msg;
        this.creditSuccess = true;
    }

    closeCreditSuccess(){
        this.creditSuccess = false;
        this.$router.push({ name: "HomeApp" });
    }

    mostraModal(message: string) {
        this.errorMessage = message;
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
    }

    mostraModalTelaCartoes() {
        this.showModalTelaCartoes = true;
    }

    fechaModalTelaCartoes() {
        this.showModalTelaCartoes = false;
    }

    mostraModalSuccess(){
        this.showModalSuccess = true;
    }

    fechaModalSuccess() {
        this.showModalSuccess = false;
        this.$router.push({ name: "Balance" });
    }

    focusInput( id:any ){
        const offset = -60;
        const el: any = $("#"+id);
        setTimeout(function(){
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        },150);
    }

}