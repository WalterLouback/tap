import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '@/components/SectionArea.vue';
import TextLink from '@/components/TextLink.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import Carousel from '@/components/Carousel.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SelectField from '@/components/SelectField.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import ListDefault from '@/components/ListDefault.vue';   
import OptionCard from '@/components/OptionCard.vue';
import SectionTitle from '@/components/SectionTitle.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';

declare var M: any;
declare var $: any;
@Component({
    components: { SectionArea, TextLink, InputText, DefaultButton, Carousel, HeaderFluxo, SelectField, ModalCodigo, OptionCard, ListDefault, SectionTitle }
})

export default class Balance extends Vue {

    inputValues: any;
    WALLET: object = {};
    DEPENDENT_USERS = JSON.parse(localStorage.getItem('USER_DEPENDENTS') || '[]');
    USER_DATA = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    USER_BALANCE = JSON.parse(localStorage.getItem('WALLET') || '{}');
    showModal: boolean = false;

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        // if (!this.DEPENDENT_USERS || this.DEPENDENT_USERS.length == 0) {
        //     window.history.replaceState({}, '', '#/HomeApp');
        //     localStorage.setItem('USER_RECEIVED_ID', localStorage.getItem('USER_ID') || '');
        //     this.$router.push('AddBalance');
        // }
        this.getUserData();
        this.getBalance();
        this.getUserDependents();
    }

    goBack() {
        this.$router.go(-1);
    }

    mostraModal() {
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
    }

    goToAddBalance(id: any){
        localStorage.setItem('USER_RECEIVED_ID', id);
        this.$router.push({ name: 'AddBalance' });
    }

    getUserData(){
        try {
            const self = this
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'USER_TYPE_ID': 1,
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || "")
            };
            req.route = 'getUserData';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('USER_DATA', JSON.stringify(response.user));
                self.USER_DATA = response.user;
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            
            console.log(e);
        }
    }

    goToExtrato() {
        this.$router.push('ExtratoEvents');
    }

    getBalance() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID')
            };
            req.route = 'getBalance';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('WALLET', JSON.stringify(response.wallet));
                self.USER_BALANCE = response.wallet;
            };
            req.callbackError = function(error: any) {
                console.log(error);
            };
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    getUserDependents() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'USER_TYPE_ID': 1
            };
            req.route = 'getUserDependents';
            req.callbackSuccess = function(response: any) {
                self.DEPENDENT_USERS = response.dependents;
                localStorage.setItem('USER_DEPENDENTS', JSON.stringify(response.dependents));
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    teste(){
        const self = this;
        self.DEPENDENT_USERS = JSON.parse(localStorage.getItem('DEPENDENTS') || '{}');
        self.USER_DATA = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
        self.USER_BALANCE = JSON.parse(localStorage.getItem('WALLET') || '{}');
    }
}