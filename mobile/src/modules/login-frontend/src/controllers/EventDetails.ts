import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionTitle from '@/components/SectionTitle.vue';
import EventDetailImage from '@/components/EventDetailImage.vue';
import TextLink from '@/components/TextLink.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import SelectField from '@/components/SelectField.vue';
import OptionField from '@/components/OptionField.vue';
import InvisibleHeader from '@/components/InvisibleHeader.vue'
import Request from '@/ts/Request';
import Util from '@/ts/Util';


declare var M: any;
declare var $: any;
@Component({
    components: { SectionTitle, EventDetailImage, TextLink, InputText, DefaultButton, SelectField, OptionField, InvisibleHeader }
})

export default class EventDetails extends Vue {
    inputValues: any;
    structures: any = [];
    events: any = [];
    eventDetail: any = JSON.parse(localStorage.getItem('EVENT') || '{}');
    ticketsEvent: any = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
    nome: any = [];
    cheapestTicket: any = {};
    isFavorited: boolean = !!this.eventDetail.favoriteId;
    componentKey: number = 0;
    

    formatDate(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('DD [de] MMM [às] HH:mm');
    }

    getTicketByEvent() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.eventDetail.id
            };
            req.route = 'getTicketByEvent';
            req.callbackSuccess = function(response: any) { 
                self.ticketsEvent = response.eventTickets;
                self.findCheapestTicket();
                const allTicketsEvents = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]');
                allTicketsEvents[self.eventDetail.id] = response.eventTickets;
                localStorage.setItem('TICKETS_EVENTS', JSON.stringify(allTicketsEvents));
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getEvent();
        this.getTicketByEvent();
        this.findCheapestTicket();
    }

    toggleFavorite() {
        const isFavorited = this.isFavorited;
        if (!isFavorited) {
            this.isFavorited = true;
            this.favoriteEvent();
        } else {
            this.isFavorited = false; 
            this.unfavoriteEvent();
        }
    }

    favoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'createFavoriteEvent';
        req.callbackSuccess = function (response: any) { 
            //self.loadEvents(); 
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();
    }

    unfavoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'deleteFavoriteEvent';
        req.callbackSuccess = function (response: any) { 
            //self.loadEvents(); 
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();
    }

    goToSecondProductMenu(){
        localStorage.setItem('SELECTED_EVENT', JSON.stringify(this.eventDetail.id));
        localStorage.setItem('EVENT_NAME_QRCODE', JSON.stringify(this.eventDetail.name));
        this.$router.push({ name: 'GetProduct' });
    }

    goToProductMenu(){
        this.$router.push({ name: 'ProductMenu' });
    }

    goToBuyEventTicket(){
        this.$router.push({ name: 'BuyEventTicket' });
    }

    getEvent() {
        this.events = JSON.parse(localStorage.getItem('EVENTS') || '{}');
        this.eventDetail = JSON.parse(localStorage.getItem('EVENT') || '{}');
    }

    findCheapestTicket() {
        if (this.ticketsEvent.length > 0) {
            this.cheapestTicket = this.ticketsEvent.reduce((a: any, b: any) => { 
                if (a && b) return a.price < b.price ? a : b;
                else return a;
            });
        } else return {};
    }

    goBack() {
        this.$router.go(-1);
    }
}