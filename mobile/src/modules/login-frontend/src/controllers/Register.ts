import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '../components/SectionArea.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import TextLink from '../components/TextLink.vue';
import DefaultButton from '../components/DefaultButton.vue';
import DivInput from '../components/DivInput.vue';
import ImageModal from '../components/ImageModal.vue';
import LoadingPhases from '../components/LoadingPhases.vue';
import HeaderRelease from '../components/HeaderRelease.vue'
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import LoginFrontEnd from '../../LoginFrontEnd';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import moment  from 'moment';

@Component({
components: { SectionArea, TextLink, DefaultButton , DivInput, ModalCodigo, ImageModal, LoadingPhases, HeaderRelease, HeaderCircleButton }
})

export default class Register extends Vue {

    loginProps: any = (localStorage.getItem("LOGIN_PROPS")) ? JSON.parse(localStorage.getItem('LOGIN_PROPS') || '{}') : {};
    logoBig: any = this.loginProps.logo;
    identifier: string = '';
    identifiers: any = [];
    identifierIndex: number = 0;
    maskLength: number = 0;
    maskLengths: any = [];
    inputFocused: boolean = false;
    passwordVisibility: boolean = false;
    msgModalError: string = "";
    modalError: boolean = false;
    validInput: boolean = false;
    backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack};
    next: any = {};
    passwordConfirmationIndex: number = 0;
    lp: any = LoadingPhases;
    registerArray = [
        {
            index: 0,
            label: 'Digite seu ',
            type: 'text',
            mask: '',
            placeholder: 'nome completo',
            value: ''
        },
        {
            index: 1,
            label: 'Digite seu ',
            type: 'tel',
            mask: '(__) _____-____',
            placeholder: 'telefone',
            value: ''
        },
        // {
        //     index: 2,
        //     label: 'Digite seu ',
        //     type: 'email',
        //     mask: '',
        //     placeholder: 'e-mail',
        //     value: ''
        // },
        {
            index: 2,
            label: 'Digite seu ',
            type: 'tel',
            mask: '___.___.___-__',
            placeholder: 'CPF',
            value: ''
        },
        {
            index: 3,
            label: 'Digite sua ',
            type: 'tel',
            mask: '__/__/____',
            placeholder: 'data de nascimento',
            value: ''
        },
        {
            index: 4,
            label: 'Digite sua ',
            type: 'password',
            mask: '',
            placeholder: 'senha',
            value: ''
        }
    ];

    currentStep: any = {index: -1};
    loginMethods: any[] = this.loginProps.loginMethods || ['EMAIL'];

    mounted(){
        const self = this;
        Util.setBackButtonBehavior(this.goBack);
        if(this.registerArray.find((step:any)=>step.type == 'password')){
            this.addPasswordConfirmation();
        }
        window.addEventListener('keyup', (ev)=>{
            if(this.currentStep.mask){
                this.applyMask();
            }
        });
    }

    goNext() {
        if (this.currentStep.index + 1 < this.registerArray.length) {
            var next = 'regInput' + (this.currentStep.index + 1);
            var elements: any = this.$refs[next];
            var element = <HTMLElement> elements[0];
            element.focus();
            this.updateScreen();
        } 
        else if ( this.validInput )this.finishRegister();
        
    }

    finishRegister() {
        if(this.checkInputs()){
            const req: any = new Request();
            const currentExtId: any = this.registerArray.find((obj:any)=>obj.placeholder == 'CPF')!;
            const fullName: string = this.registerArray.find((obj:any)=>obj.placeholder=='nome completo')!.value;
            const phoneNumber: any = this.registerArray.find((obj:any)=>obj.placeholder == 'telefone')!.value;
            req.body = {
                'CPF': this.getValidCharacters(currentExtId),
                // 'EMAIL': this.registerArray.find((obj:any)=>obj.placeholder == 'e-mail')!.value,
                'PASSWORD': this.registerArray.find((obj:any)=>obj.placeholder == 'senha')!.value,

                'PHONE': phoneNumber,
                'BIRTH_DATE': this.registerArray.find((obj:any)=>obj.placeholder == 'data de nascimento')!.value,

                'FIRST_NAME': fullName.split(" ")[0],
                'LAST_NAME': fullName.split(" ").slice(1,fullName.length-1).join(" "),
                'USER_TYPE_ID': 1,
                'NRORG': this.loginProps.nrorg,
                'TOKEN': "a",
                'USER_ID': 1
                
            } 
            req.route = 'createUser';

            req.callbackSuccess = (response: any) => {
                // lp.start(lp.SUCCESS, 'Cadastro realizado com sucesso');
                localStorage.setItem('USER_EXT_ID', currentExtId);
                this.auth();
                
            }
            req.callbackError = (error: any) => {
                this.lp.start(this.lp.ERROR, 'Não foi possível realizar o cadastro');
            }
            req.loadingMessage = 'Realizando cadastro...';
            req.blockTouchEvents = false;
            if(window.navigator.onLine)req.send();
            else {
                this.lp.start(this.lp.ERROR, 'Sem conexão com a internet');
            }
        }
    }

    goToHome() {
        LoginFrontEnd.callback();
    }

    goBack() {
        this.$router.go(-1);
    }

    updateScreen(){
        this.allValidInputs();
        M.updateTextFields();
        if(this.currentStep.mask) this.applyMask();
    }

    validate_char(c: string) {
        if ((c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57)
            || (c.charCodeAt(0) >= 65 && c.charCodeAt(0) <= 90)
            || (c.charCodeAt(0) >= 97 && c.charCodeAt(0) <= 122)) {
            return true;
        } else {
            return false;
        }
    }

    validate_num(c: string) {
        if (c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57) {
            return true;
        } else {
            return false;
        }
    }

    getMaskLength(step:any) {
        return (step.mask.match(/_/g) || []).length;
    }

    // checkMask() {
    //     if (this.getValidCharacters().length == this.getMaskLength()) {
    //         this.applyMask();
    //     }
    // }

    applyMask() {
        var validChars: string[] = [];
        var typeNum = (this.currentStep.type == 'tel'|| this.currentStep.type == 'number');
        var text = this.currentStep.value;
        var maskedText = "";

        for (var i = 0; i < text.length; i++) {
            if ((typeNum && this.validate_num(text.charAt(i))) || (!typeNum && this.validate_char(text.charAt(i)))) {
                validChars.push(text.charAt(i));
            }
        }
        

        for (var j = 0, i = 0; j < this.currentStep.mask.length && i < validChars.length; j++) {
            if (this.currentStep.mask.charAt(j) == "_") { 
                    maskedText = maskedText + validChars[i];
                    i++;
            } else {
                maskedText = maskedText + this.currentStep.mask.charAt(j);
            }

        }

        this.currentStep.value = maskedText;
    }

    getValidCharacters(step: any) {
        var text = step.value;
        var finalText = '';
        var typeNum = (step.type == 'tel'|| step.type == 'number');

        for (var i = 0; i < text.length; i++) {
            if ((typeNum && this.validate_num(text.charAt(i))) || (!typeNum && this.validate_char(text.charAt(i)))) {
                finalText = finalText + text.charAt(i);
            }
        }
        return finalText.toUpperCase();
    }

    changeInputPasswd(step: number){
        this.passwordVisibility = !this.passwordVisibility;
        var elements:any = this.$refs["regInput" + step];
        var element = <HTMLElement> elements[0];
        element.focus();
    }

    validValue(step:any){
        var elements:any = this.$refs["regInput" + step.index];
        var element = <HTMLInputElement>elements[0];

        if (step.value == ""){
            this.modalError = true;
            this.msgModalError = "Campo inválido.\nPor favor, " + step.label[0].toLowerCase() + step.label.substring(1) + step.placeholder + "."
            return false;
        }
        else if (step.type == 'email' && !element.checkValidity() ){
            this.modalError = true;
            this.msgModalError = "Campo inválido.\nPor favor, digite um email válido."
            return false;
            
        }
        // else if (step.mask == '__/__/____' && (!moment(step.value,'DD/MM/YYYY',true).isValid())){
        //     this.modalError = true;
        //     this.msgModalError = "Campo inválido.\nPor favor, digite uma data válida."
        //     return false;
            
        // }
        else if (step.mask && this.getMaskLength(step)!=this.getValidCharacters(step).length ){
            this.modalError = true;
            this.msgModalError = "Campo inválido.\nPor favor, " + step.label[0].toLowerCase() + step.label.substring(1) + step.placeholder + "."
            return false;
        }
        else if (step.index == this.passwordConfirmationIndex && step.value != this.registerArray.find((obj:any)=>obj.placeholder == 'senha')!.value ){
            this.modalError = true;
            this.msgModalError = "As senhas digitadas não são iguais."
            return false;
        }
        else return true;
    }

    updateInputFocus(focus: boolean, step: any = null){
        setTimeout(() => {
            this.inputFocused = focus;
            if(focus) {
                this.currentStep = step;
                this.scrollToThis(step.index);
            }
        }, 50);
    }

    isValidInput(step: any){
        var elements:any = this.$refs["regInput" + step.index];
        var element = <HTMLInputElement>elements[0];

        if (step.value == ""){
            return false;
        }
        else if (step.type == 'email' && !element.checkValidity() ){
            return false;
        }
        else if (step.mask && this.getMaskLength(step)!=this.getValidCharacters(step).length ){
            return false;
        }
        // else if (step.mask == '__/__/____' && (!moment(step.value,'DD/MM/YYYY',true).isValid())){
        //     return false;
        // }
        else if (step.index == this.passwordConfirmationIndex && step.value != this.registerArray.find((obj:any)=>obj.placeholder == 'senha')!.value ){
            return false;
        }
        else {
            return true;
        }
    }

    scrollToThis(id: any) {
        var elements:any = this.$refs["anchorInput" + id];
        var element = <HTMLInputElement>elements[0];
        element!.scrollIntoView(false);
    }

    allValidInputs(){
        this.validInput = !(this.registerArray.find((step:any) => !this.isValidInput(step)));
        this.$forceUpdate();
    }

    checkInputs(){
        var validInputs = !this.registerArray.find((step:any) => !this.validValue(step));
        return validInputs; 
    }

    sendPhoneAuthCode(){
        console.log('outro');
    }

    addPasswordConfirmation(){
        this.passwordConfirmationIndex =  this.registerArray.length;
        // const obj: any =  {
        //     index: this.passwordConfirmationIndex,
        //     label: 'Digite sua ',
        //     type: 'password',
        //     mask: '',
        //     placeholder: 'confirmação de senha',
        //     value: ''
        // }
        // this.registerArray.push(obj);
       
    }
    
    
       
    auth() {
        const req = new Request();
        const currentExtId: any = this.registerArray.find((obj:any)=>obj.placeholder == 'CPF')!;
        req.body = {
            'EMAIL': this.loginMethods.includes("EMAIL") ? this.registerArray.find((obj:any)=>obj.placeholder == 'e-mail')!.value: null,
            'PHONE': this.loginMethods.includes("PHONE") ? this.registerArray.find((obj:any)=>obj.placeholder == 'telefone')!.value: null,
            'CPF': this.loginMethods.includes("EXTERNAL_ID") ? this.getValidCharacters(currentExtId): null,
            'PASSWORD': this.registerArray.find((obj:any)=>obj.placeholder == 'senha')!.value,
            'NRORG': this.loginProps.nrorg
        };
        req.route = 'login';
        req.callbackSuccess = (response: any) => {
            const userId: number = response.user.userId;
            const refreshTKN: string = response.user.refresh_token;
            const tokenExt: string = response.user.token_ext || "";
            const keyExt: string = response.user.key_ext || "";
            const valIdExt: string = response.user.val_id_ext || "";
            const id_ext: string = response.user.id_ext || "";
            const valPhoto: string = response.user.val_pic || "";

            localStorage.setItem('USER_ID', JSON.stringify((userId)));
            localStorage.setItem('REFRESH_TOKEN', refreshTKN);
            localStorage.setItem('QBTT', this.registerArray.find((obj:any)=>obj.placeholder == 'senha')!.value);
            sessionStorage.setItem('USER_ID', JSON.stringify((userId)));
            localStorage.setItem('TOKEN_EXT', tokenExt);
            localStorage.setItem('KEY_EXT', keyExt);
            localStorage.setItem('VAL_EXT_ID', JSON.stringify((valIdExt)));
            localStorage.setItem('VAL_PIC_CARD', JSON.stringify((valPhoto)));
            localStorage.setItem('ID_EXT', id_ext);
            localStorage.setItem('IS_AUTHORIZED_EXTERNAL', 'AUTHORIZED');
            localStorage.setItem('FIRST_TIME_ON_APP', 'false');
            
            this.sessionLogIn();
        };
        req.callbackError = (error: any) => {
            const _window: any = window;
            var mensagem: string;

            if (_window.navigator.onLine === false) mensagem = 'Sem conexão com a internet.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 1) mensagem = 'Usuário não encontrado.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 2) mensagem = 'Email já cadastrado em outra conta.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 3) mensagem = 'Email já cadastrado em outra conta.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 10 || error && error.response && error.response.data && error.response.data.errorCode == 11) mensagem = 'Usuário não autenticado.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 1002) mensagem = error.response.data.error;
            else mensagem = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';

            this.lp.start(this.lp.ERROR, mensagem);
        }
        // req.loadingMessage = 'Buscando suas informações...';
        req.blockTouchEvents = false;
        req.send();
        localStorage.setItem('USER_EXT_ID', "");
    }

    //Method to get Organization Data
    getOrganizationData() {

        const self = this
        const req = new Request();
        req.body = {
            'NRORG': this.loginProps.nrorg,
            'USER_ID': localStorage.getItem('USER_ID'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_TYPE_ID': 1,
        };
        req.route = 'getOrganizationData';
        req.callbackSuccess = function (response: any) {
            const name: string = response.organization.NAME;
            const editableFields: any = response.organization.EDITABLE_FIELDS;
            let PRIMARY_COLOR = response.organization.CONFIGURATIONS.PRIMARY_COLOR;
            let SUPPORT_COLOR = response.organization.CONFIGURATIONS.SECONDARY_COLOR;

            localStorage.setItem('ORG_NAME', name);
            localStorage.setItem('ORG_FIELDS', JSON.stringify((editableFields)));
            localStorage.setItem('ORGANIZATION_DATA', JSON.stringify((response.organization)));

            // if (document.documentElement) document.documentElement.style.setProperty('--primary-color', PRIMARY_COLOR);
            // if (document.documentElement) document.documentElement.style.setProperty('--support-color', SUPPORT_COLOR);
            self.getUserData();
            
        };
        req.callbackError = function (error: any) {
            let mensagem: string;
            if (error.response.data.errorCode == 24) {
                mensagem = 'Usuário não autorizado pelo titular!';
            }
            if (error.response.data.errorCode == 14) {
                mensagem = 'Usuário não autorizado pela organização!';
            }
            console.log(error);
        };
        // req.loadingMessage = 'Preparando o aplicativo...';
        req.blockTouchEvents = true;
        req.send();
    }

    // Method to start a session from the Login
    sessionLogIn() {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'REFRESH_TOKEN': localStorage.getItem('REFRESH_TOKEN') || ''
        };
        req.route = 'requestToken';
        req.callbackSuccess = function (response: any) {
            localStorage.setItem('TOKEN', response.user.token);
            self.getOrganizationData();
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        // req.loadingMessage = 'Buscando suas informações...';
        req.blockTouchEvents = true;
        req.send();
    }

    getUserData() {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'NRORG': this.loginProps.nrorg,
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'USER_TYPE_ID': 1,
            'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
        };
        req.route = 'getUserData';
        req.callbackSuccess = function (response: any) {
            const now: any = moment();
            localStorage.setItem('USER_DATA', JSON.stringify(response.user));
            localStorage.setItem('USER_NAME', response.user.FIRST_NAME);
            localStorage.setItem('LAST_GET_USER_DATA', now);
            LoginFrontEnd.callback();
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.send();
  }

  closeLP(){
      this.lp.stop();
  }

    
}