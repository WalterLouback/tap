import { Component, Prop, Vue } from 'vue-property-decorator';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import CircleButton from '@/modules/shared-components/CircleButton.vue'
// import Orders, { StoresConfig, RecentOrdersConfig } from "../../../../teknisa-modules/ordersModule/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Banner from '@/modules/shared-components/Banner.vue';
import Util from '@/ts/Util';
import Request from '@/ts/Request';
import BannerStore from '@/modules/shared-components/BannerStore.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import ListDefault from "../components/ListDefault.vue";
import Shortcuts from "../components/Shortcuts.vue";
import TextLink from "../components/TextLink.vue";
import moment from 'moment';
import Minicard from '@/modules/shared-components/Minicard.vue';
import LoginFrontEnd, { LoginConfig } from '../../LoginFrontEnd';
import LoadingPhases from '../components/LoadingPhases.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import DefaultButton from '../components/DefaultButton.vue';


@Component({
  components: { BottomNavigation, HeaderBackground, CircleButton, HeaderCircleButton, Banner, BannerStore, LoginModal, ListDefault, Shortcuts, TextLink, Minicard, LoadingPhases, ModalCodigo, DefaultButton }
})

export default class ChooseStructure extends Vue {
  componentKey: number = 0;
  image: any = { name: 'class_aqua', ext: '.jpg' }

  backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.confirmLogout }
  menuOptions: boolean = true;


  storesLoaded: boolean = true;
  notificationsKey: number = 1;


  placeholderImage: string = Util.getImgUrl('placeholder', '.png')
  showLoginModal: boolean = false;
  selectedNews: string = "";

  structures: any = JSON.parse(localStorage.getItem('ORGANIZATION_STRUCTURES') || '[]') || [];
  parentStructures: any = (this.structures && this.structures.length) ? this.structures.filter((s: any) => s.level == 0 || s.level == 1) : [];
  lp: any = LoadingPhases;
  confirmLogoutModal: boolean = false;

  mounted() {
    Util.setBackButtonBehavior(this.confirmLogout);
    // this.getStoreData();
  }


  goBack() {
    // Util.removeFCMToken();
    var loginSettings = localStorage.getItem('LOGIN_PROPS') || '';
    localStorage.clear();
    localStorage.setItem('LOGIN_PROPS', loginSettings);
    this.$router.go(-1);
    // LoginFrontEnd.start(new LoginConfig(
    //   ['PHONE'], 
    //   'Bem-vindo ao BipFun. Entre no app e ',
    //   'compre sem enfrentar filas.', 
    //   Util.getImgUrl('bipfun-logo','.svg'),
    //   LoginFrontEnd.callback,
    //   (process.env.VUE_APP_NRORG || '')
    // ));
  }

  goHome() {
    this.$router.push('Home');
  }

  setStructureDataAndEnter(structure: any) {
    localStorage.setItem('STRUCTURE_DATA', JSON.stringify(structure));
    localStorage.setItem('STRUCTURE_ID', JSON.stringify(structure.id));
    localStorage.setItem('SHOW_EXPLORAR_OVERLAY', 'false');
    //LoginFrontEnd.callback();
    const req = new Request();
    req.getAvailablePosPrivate();
    this.$router.push('ChooseTaas');
  }

  confirmLogout() {
    this.confirmLogoutModal = true;
  }

  closeError() {
    this.lp.stop();
  }

  goNext() {
    this.getUserData(() => { LoginFrontEnd.callback(); });
    this.updateFavoriteList();
    this.getUserTicketsByOrg();
  }

  getUserData(callback?: Function) {
    const self = this
    const req = new Request();
    req.body = {
      'USER_ID': localStorage.getItem('USER_ID') || '',
      'NRORG': localStorage.getItem('NRORG'),
      'TOKEN': localStorage.getItem('TOKEN') || '',
      'USER_TYPE_ID': 1,
      'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
    };
    req.route = 'getUserData';
    req.callbackSuccess = function (response: any) {
      const now: any = moment();
      localStorage.setItem('USER_DATA', JSON.stringify(response.user));
      localStorage.setItem('USER_NAME', response.user.FIRST_NAME);
      localStorage.setItem('LAST_GET_USER_DATA', now);
      if (callback) callback(response);
    };

    req.blockTouchEvents = false;
    req.callbackError = function (error: any) {
      console.log(error);
    };
    req.send();
  }

  updateFavoriteList() {
    try {
      const req = new Request();
      req.body = {
        'USER_ID': localStorage.getItem('USER_ID'),
        "NRORG": localStorage.getItem("NRORG"),
        'TOKEN': localStorage.getItem("TOKEN")
      }
      req.route = 'favorite';
      req.callbackSuccess = (response: any) => {
        localStorage.setItem('FAVORITE_EVENTS_AND_STORES', JSON.stringify(response.calendar.filter((obj: any) => !!obj.favoriteId)));
      }
      req.callbackError = () => {
        console.log("Error");
      }
      req.blockTouchEvents = false;
      req.send();
    } catch (e) {
      console.log(e)
    }
  }

  getUserTicketsByOrg() {
    try {
      const self = this
      const req = new Request();
      req.body = {
        "USER_ID": localStorage.getItem('USER_ID'),
        "TOKEN": localStorage.getItem('TOKEN'),
        "NRORG": localStorage.getItem('NRORG')
      };
      req.route = 'getUserTicketsByOrg';
      req.callbackSuccess = function (response: any) {
        localStorage.setItem('USER_TICKETS', JSON.stringify(response.userTickets));
      };
      req.callbackError = function (error: any) {
        console.log(error);
      };
      req.blockTouchEvents = false;
      if (localStorage.getItem('USER_ID')) req.send();
    } catch (e) {

      console.log(e);
    }
  }
}
