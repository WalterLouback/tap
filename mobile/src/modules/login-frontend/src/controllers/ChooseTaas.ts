import { Component, Prop, Vue } from 'vue-property-decorator';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import CircleButton from '@/modules/shared-components/CircleButton.vue'
import Orders, { StoresConfig, RecentOrdersConfig } from "../../../orders-module/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Banner from '@/modules/shared-components/Banner.vue';
import Util from '@/ts/Util';
import Request from '@/ts/Request';
import BannerStore from '@/modules/shared-components/BannerStore.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import ListDefault from "../components/ListDefault.vue";
import Shortcuts from "../components/Shortcuts.vue";
import TextLink from "../components/TextLink.vue";
import moment from 'moment';
import Minicard from '@/modules/shared-components/Minicard.vue';
import LoginFrontEnd, { LoginConfig } from '../../LoginFrontEnd';
import LoadingPhases from '../components/LoadingPhases.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import DefaultButton from '../components/DefaultButton.vue';
import EmptyState from '../components/EmptyState.vue';

@Component({
  components: { BottomNavigation, HeaderBackground, CircleButton, HeaderCircleButton, Banner, BannerStore, LoginModal, ListDefault, Shortcuts, TextLink, Minicard, LoadingPhases, ModalCodigo, DefaultButton, EmptyState }
})

export default class ChooseTaas extends Vue {
  componentKey: number = 0;
  image: any = { name: 'class_aqua', ext: '.jpg' }

  backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack }
  menuOptions: boolean = true;

  taasLoaded: boolean = false;
  // notificationsKey: number = 1;

  placeholderImage: string = Util.getImgUrl('placeholder', '.png')
  showLoginModal: boolean = false;
  selectedNews: string = "";
  // logo: string = 'logo-teknisa.svg';
  // logoOverlay: string = Util.getImgUrl('logo-teknisa', '.svg');

  taas: any = JSON.parse(localStorage.getItem('TAA_CONFIG') || '[]') ? JSON.parse(localStorage.getItem('TAA_CONFIG') || '[]') : "[]";
  lp: any = LoadingPhases;
  confirmLogoutModal: boolean = false;

  // overlay
  logoOverlay: string = Util.getImgUrl('logo-teknisa', '.svg');
  overlayBackgroundImage: string = Util.getImgUrl('celebrate', '.jpg');

  /*Overlay com instruções*/
  showInstructionOverlay: boolean = false; //(localStorage.getItem('SHOW_TAA_OVERLAY')) ? true : false;
  overlayTimer: any = null
  exitCounter: any = 0;
  availablePos: any = "{}";
  posReferenceId: string = "";

  beforeCreated() {
    //localStorage.setItem('SHOW_TAA_OVERLAY', 'true');
  }

  //mounted() {
  created() {
    this.getAvailableTaa();
    // this.taas = JSON.parse(localStorage.getItem('TAA_CONFIG') || '{}');
    // this.taasLoaded = true;
    // console.log(this.taas);
    this.overlayTimer = setTimeout(() => this.openOverlay(), 45000);
    //document.addEventListener("click", this.refreshOverlayTimer, true);
    //Util.setBackButtonBehavior(this.confirmLogout);
  }

  goBack() {
    this.$router.push('ChooseStructure');
  }

  goHome() {
    this.$router.push('Home');
  }

  confirmLogout() {
    this.confirmLogoutModal = true;
  }

  closeError() {
    this.lp.stop();
  }

  goToNext(t: any) {
    const self = this;
    self.showInstructionOverlay = true;
    localStorage.setItem('TAA_SELECTED', JSON.stringify(t));

    if (t.posSerialNumber != "null" && t.posSerialNumber != '' && t.posSerialNumber != null) {
      localStorage.setItem('POS_SERIAL_NUMBER', t.posSerialNumber);
      this.getStonePosReferenceId(t.posSerialNumber)
    }

    if (t.posReferenceId != "null" && t.posReferenceId != '' && t.posReferenceId != null) {
      localStorage.setItem('POS_REFERENCE_ID', t.posReferenceId);
    } else if (t.posSerialNumber != "null" && t.posSerialNumber != '') {
      localStorage.setItem('POS_SERIAL_NUMBER', t.posSerialNumber);
      self.getReferenceIdBySerialNumber(t.posSerialNumber);
    } else console.log('taa sem S/N!');

    if (t.modality.toUpperCase() == 'L' && t.storeId != "null" && t.storeId != '' && t.storeId != null) {
      localStorage.setItem('TAA_STORE', t.storeId);
      this.$router.push('TicketPayment');
    } else if (t.modality.toUpperCase() == 'D' && t.storeId != "null" && t.storeId != '' && t.storeId != null) {
      localStorage.setItem('TAA_STORE', t.storeId);
      localStorage.setItem('storeId', t.storeId);
      localStorage.setItem('IS_VISIBLE', 'true');
      this.$router.push('Explorar');
    } else if (t.modality.toUpperCase() == 'D' && (t.storeId == "null" || t.storeId == '' || t.storeId == null)) {
      console.log('TAA Dedicado sem storeId');
    } else if (t.modality.toUpperCase() == 'C') {
      localStorage.setItem('IS_VISIBLE', 'false');
      this.$router.push('Explorar');
    } else {
      // localStorage.setItem('IS_VISIBLE', 'false');
      // localStorage.setItem('STORE_UNICA','U');
      // localStorage.setItem('TAA_STORE', t.storeId);
      // this.goToStore(t.storeId);
      console.log('Nenhum TAA configurado corretamente!');
    }

    self.prepareToRegisterReferenceIdOrSerialNumber();
  }

  getAvailableTaa() {
    try {
      const self = this;
      const req = new Request();
      req.body = {
        'USER_ID': localStorage.getItem('USER_ID'),
        "NRORG": localStorage.getItem("NRORG"),
        'TOKEN': localStorage.getItem("TOKEN"),
        "STRUCTURE_ID": localStorage.getItem("STRUCTURE_ID"),
        "STATUS": "A"
      };
      req.route = 'getAvailableTaa';
      req.callbackSuccess = (response: any) => {
        localStorage.setItem('TAA_CONFIG', JSON.stringify(response.taas));
        self.taasLoaded = true;
        self.taas = JSON.parse(localStorage.getItem('TAA_CONFIG') || '{}');
        if (response.taas.length <= 1) this.goToNext(response.taas[0])
      };
      req.callbackError = () => {
        console.log("Error");
      };
      req.blockTouchEvents = false;
      req.send();
    } catch (e) {
      console.log(e);
    }
  }

  openOverlay() {
    this.showInstructionOverlay = true;
  }

  fechaOverlay() {
    this.showInstructionOverlay = false;
    localStorage.setItem('SHOW_TAA_OVERLAY', '');
  }

  refreshOverlayTimer() {
    clearInterval(this.overlayTimer);
    this.overlayTimer = undefined;
    this.overlayTimer = setTimeout(() => this.openOverlay(), 45000);
    this.$router.push('Explorar');
  }

  goToStore(id: string) {
    localStorage.setItem('IS_AN_EVENT', '');
    Util.removeBackButtonBehavior();
    localStorage.setItem('SHARE_ID', '')
    localStorage.setItem('SHARE_TYPE', '')
    Orders.goToStores(new StoresConfig(this.goHome, id));
  }

  updateReferenceIdOrSerialNumber(serialNumber: any, posReferenceId: any, taaId: any) {
    const req = new Request();

    try {
      req.body = {
        "USER_ID": localStorage.getItem('USER_ID'),
        "NRORG": localStorage.getItem('NRORG'),
        "TOKEN": localStorage.getItem('TOKEN'),
        "TAA_ID": taaId,
        "POS_SERIAL_NUMBER": serialNumber || null,
        "POS_REFERENCE_ID": posReferenceId || null
      };
      req.route = 'updateReferenceIdOrSerialNumber';
      req.callbackSuccess = function (response: any) {
        console.log(response);
        if (posReferenceId !== '' && posReferenceId !== null && posReferenceId !== 'undefined') {
          localStorage.setItem('POS_REFERENCE_ID', posReferenceId);
        }
        if (serialNumber !== '' && serialNumber !== null && serialNumber !== 'undefined') {
          localStorage.setItem('POS_SERIAL_NUMBER', serialNumber);
        }
      };

      req.callbackError = function (error: any) {
        console.log(error);
      };
      req.send();
    } catch (e) {
      console.log(e);
    }
  }

  prepareToRegisterReferenceIdOrSerialNumber() {
    const self = this;
    if (localStorage.getItem('TAA_SELECTED') !== 'undefined') {
      var taaSelected = JSON.parse(localStorage.getItem('TAA_SELECTED') || '{}');

      if (localStorage.getItem('POS_REFERENCE_ID') !== 'undefined' && localStorage.getItem('POS_REFERENCE_ID') !== '') {
        var posReferenceId = localStorage.getItem('POS_REFERENCE_ID');
        if (taaSelected.posReferenceId == null ||
          taaSelected.posReferenceId == "" ||
          posReferenceId != taaSelected.posReferenceId ||
          taaSelected.posReferenceId == 'null'
        ) {
          self.updateReferenceIdOrSerialNumber(null, posReferenceId, taaSelected.id);
          console.log('referenceId atualizado porque taa nao tinha esse dado!');
        }
      }

      if (localStorage.getItem('POS_SERIAL_NUMBER') !== 'undefined' && localStorage.getItem('POS_SERIAL_NUMBER') !== '') {
        var serialNumber = localStorage.getItem('POS_SERIAL_NUMBER');
        if (taaSelected.posSerialNumber == null ||
          taaSelected.posSerialNumber == "" ||
          serialNumber != taaSelected.posSerialNumber ||
          taaSelected.posSerialNumber == 'null'
        ) {
          self.updateReferenceIdOrSerialNumber(serialNumber, null, taaSelected.id);
          console.log('serialNumber atualizado atualizado porque taa nao tinha esse dado!');
        }
      }
    } else {
      console.log('nenhum taa foi selecionado!');
    }
  }

  verifyCommand() {
    const taa = JSON.parse(localStorage.getItem('TAA_SELECTED') || '[]');
    if (taa.modality.toUpperCase() == 'L' && (taa.storeId != null || taa.storeId != "null")) {
      this.$router.push('TicketPayment');
    } else { return; }
  }

  getStonePosReferenceId(serialNumber: string) {
    const self = this
    return new Promise((resolve: Function, reject: Function) => {
      const req = new Request();
      self.getReferenceIdBySerialNumber(serialNumber);
      req.body = {
        "pos_serial_number_to_link": "",
        "cashier_number": "",
        "pdv_number": "",
        "pos_link_label": "",
        "pos_reference_id_to_link": localStorage.getItem('POS_REFERENCE_ID'),
      }
      req.callbackSuccess = (response: any) => {
        var posReferenceId = response.pos_link.pos_reference_id;

        if (posReferenceId !== "" && posReferenceId !== null && posReferenceId !== 'undefined') {
          localStorage.setItem('POS_REFERENCE_ID', posReferenceId);
        }
        if (serialNumber !== "" && serialNumber !== null && serialNumber !== 'undefined') {
          localStorage.setItem('POS_SERIAL_NUMBER', serialNumber);
        }
      };
      req.callbackError = (error: any) => {
        console.log(error);
      };
      req.getPosReferenceId();
    });
  }

  getReferenceIdBySerialNumber(serialNumber: string) {
    const self = this;
    return new Promise((resolve: Function, reject: Function) => {
      self.availablePos = JSON.parse(localStorage.getItem('AVAILABLE_POS') || "{}");

      if (self.availablePos.error) {
        console.log(self.availablePos.msg);
      } else if (self.availablePos.available_pos_list) {
        let result = self.availablePos.available_pos_list.find((element: any) => element.pos_serial_number == serialNumber);
        if (result !== 'undefined') {
          self.posReferenceId = result.valueOf().pos_reference_id;
          localStorage.setItem('POS_REFERENCE_ID', self.posReferenceId);
        }
        else console.log(result);
      } else console.log(self.availablePos);
    });
  }
}
