import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '../components/SectionArea.vue';
import TextBegin from '../components/TextBegin.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import DefaultButton from '../components/DefaultButton.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import DivInput from '../components/DivInput.vue';
import ImageModal from '../components/ImageModal.vue';
import SelectField from '../components/SelectField.vue';
import OptionField from '../components/OptionField.vue';
import LoadingPhases from '../components/LoadingPhases.vue';
import CleanButton from '@/modules/shared-components/CleanButton.vue';
import SelectFieldController from '../ts/SelectFieldController';
import LoginFrontEnd from '../../LoginFrontEnd';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import InputText from '@/modules/shared-components/InputText.vue'
import LoginModal from "@/modules/shared-components/LoginModal.vue";
import VirtualKeyboard from '@/modules/shared-components/VirtualKeyboard.vue';
import moment from 'moment';
import Vuee from 'vue';

declare var KioskPlugin: any;
declare var window: any;
Vuee.config.silent = true;

@Component({
    components: { SectionArea, LoadingPhases, TextBegin, DefaultButton, ModalCodigo, DivInput, ImageModal, SelectField, OptionField, SelectFieldController, CleanButton, HeaderCircleButton, InputText, LoginModal, VirtualKeyboard }
})




export default class LogIn extends Vue {

    /* Properties*/

    loginProps: any = JSON.parse(localStorage.getItem('LOGIN_PROPS') ||
        JSON.stringify({
            loginMethods: ['PHONE'],
            introductionMessage: 'Bem-vindo ao BipFun! Entre no app e ',
            introductionMessageHighlight: 'compre sem enfrentar filas.',
            logo: Util.getImgUrl('bipfun-logo', '.svg'),
            nrorg: process.env.VUE_APP_NRORG || '0'
        })
    );
    nrorg: string = process.env.VUE_APP_NRORG || this.loginProps.nrorg || '0';
    loginMethods: any[] = this.loginProps.loginMethods || ['PHONE'];
    showLoginModal: boolean = false;


    /* Inputs */
    emailInput: string = '';
    passInput: string = '';
    authCodeInput: string = '';
    phoneInput: string = '';
    inputCpfOrNpf: string = localStorage.getItem('NPF') || localStorage.getItem('CPF') || '';
    passwordVisibility: boolean = false;
    urlInput: string = '';
    showPass: boolean = false;
    emailFocused: boolean = false;
    passFocused: boolean = false;
    caracter: string = "";
    passType: string = "password";
    /* Teclado */
    virtualKeyboard = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" }, { value: "7" }, { value: "8" }, { value: "9" }, { value: "0" }, { value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
    { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
    { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];

    /* CPF or NPF */
    cpf: string = '';
    npf: string = '';

    /* Messages */
    introductionMessage: string = this.loginProps.introductionMessage || '';
    introductionMessageHighlight: string = this.loginProps.introductionMessageHighlight || '';

    /* Images */
    logo: string = '';
    backgroundImage: string = "";
    // backgroundImage: string = Util.getImgUrl('12210 (1)', '.png')


    /* Modal */
    modal: boolean = false;
    msgModal: string = '';
    modalUrl: boolean = false;
    LoginModal: boolean = false;
    alteraInput: number = 1;

    lp: any = LoadingPhases;
    posSerialNumber: string = "";
    exitCounter: number = 0;
    misterio: number = 0;
    active: boolean = true;

    created() {
        if (this.loginProps.backgroundImage) this.backgroundImage = Util.getImgUrl(this.loginProps.backgroundImage.split('.')[0], '.' + this.loginProps.backgroundImage.split('.')[1]);
        if (this.loginProps.logo) {
            this.logo = Util.getImgUrl(this.loginProps.logo.split('.')[0], '.' + this.loginProps.logo.split('.')[1]);
        }
    }

    mounted() {
        document.addEventListener('keypress', (e: any) =>{
            if (e.key === 'Enter' && this.misterio >= 10){
                if(this.alteraInput == 1) {
                    this.pushCharacter(this.emailInput)
                } else if (this.alteraInput == 2) {
                    this.pushCharacter(this.passInput)
                }
            }
         }, false);
        var self = this;
        /* Preparando os fields de texto*/
        Util.setBackButtonBehavior(() => { });
        setTimeout(function () {
            M.updateTextFields();
            $('select').formSelect();
            self.addMask();
        }, 50);
        localStorage.setItem('NRORG', this.loginProps.nrorg);
        localStorage.setItem('ORGANIZATION_LOGO_SMALL', this.loginProps.logo);
        localStorage.setItem('PRIMARY_COLOR', this.loginProps.primaryColor)
        localStorage.setItem('SECONDARY_COLOR', this.loginProps.secondaryColor)
        if (document.documentElement) document.documentElement.style.setProperty('--primary-color', this.loginProps.primaryColor);
        if (document.documentElement) document.documentElement.style.setProperty('--secundary-color', this.loginProps.secondaryColor);
        /* Limpando campo USER_DATA para evitar problemas com cache */
        localStorage.setItem('USER_DATA', '');
        /* Permitindo prosseguir apertando enter */
        $("input#EMAIL").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                self.focusPassword();
                return false;
            } else {
                return true;
            }
        });
        $("input#NPF").keypress(function (e) {
            if (!(e.which && e.which == 8) && !(e.keyCode && e.keyCode == 46)) self.applyMask("___.___.___-__");
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                self.focusPassword();
                return false;
            } else {
                return true;
            }
        });
        $("input#PHONE").keyup(function (e) {
            if (!(e.which && e.which == 8) && !(e.keyCode && e.keyCode == 46)) self.applyMask("(__) _____-____");
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                // self.focusAuthCode();
                return false;
            } else {
                return true;
            }
        });
        $("input#PASSWORD").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $("#auth > button").click();
                return false;
            } else {
                return true;
            }
        });
        $("input#AUTHCODE").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $("#auth > button").click();
                return false;
            } else {
                return true;
            }
        });

        localStorage.setItem('CAME_FROM_FORGOT_PASSWORD', '');

        // if (document.documentElement) document.documentElement.style.setProperty('--primary-color', '#f4485a');
        // if (document.documentElement) document.documentElement.style.setProperty('--secundary-color', '#ac3844');
        // if (document.documentElement) document.documentElement.style.setProperty('--support-color', '#f821c3');
        // if (document.documentElement) document.documentElement.style.setProperty('--secundary-support-color', '#a13b70');
    }

    focusPassword() {
        $("#PASSWORD").focus();
    }

    focusAuthCode() {
        $("#AUTHCODE").focus();
    }

    scrollToThis(id: any) {
        var element = document.getElementById(id);
        setTimeout(function () {
            element!.scrollIntoView({ block: "start", behavior: "smooth" })
        }, 600);
        if (id == "PASSWORD_d") document.getElementById("LOGIN_FORM")!.scrollBy(0, 80);
        // else document.getElementById("LOGIN_FORM")!.scrollBy(0,80);
    }

    addMask() {
        /* Verificando se é um CPF */
        if (this.inputCpfOrNpf.length == 11) {
            this.inputCpfOrNpf = Util.addCpfMask(this.inputCpfOrNpf);
        } else {
            this.inputCpfOrNpf = Util.addNpfMask(this.inputCpfOrNpf);
        }
    }

    validateField(fieldId: string) {
        const fieldValue = String($('#' + fieldId).val());
        if (fieldValue.length == 0) {
            $('#' + fieldId).removeClass('valid');
            $('#' + fieldId).addClass('invalid');
        } else {
            $('#' + fieldId).removeClass('invalid');
            $('#' + fieldId).addClass('valid');
        }
    }

    //Method to authenticate
    preAuth() {

        if (this.emailInput == '') {
            this.showModal('Por favor, informe seu e-mail.');
        } else if (this.passInput == '' || this.passInput == null) {
            this.showModal('Por favor, preencha a senha.');
        } else {
            this.totemAuth();
        }
    }

    removeEmailFocus() {
        setTimeout(() => {
            this.emailFocused = false;
        }, 10);
    }

    removePassFocus() {
        setTimeout(() => {
            this.passFocused = false;
        }, 10);
    }

    isNumber(n: any) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    goToRegister() {
        this.$router.push('Register');
    }

    goForgotPassword() {
        this.$router.push('ForgotPassword');
    }

    showModal(msg: any) {
        this.msgModal = msg;
        this.modal = true;
    }

    hideModal() {
        this.modal = false;
    }


    changeInputPasswd(e: Event) {
        e.preventDefault();
        e.stopPropagation();
        this.passwordVisibility = !this.passwordVisibility;
        this.scrollToThis("PASSWORD")
        this.focusPassword();
        // setTimeout(() => {
        //     const passwordField: any = this.$refs.password;
        //     passwordField.focus();
        //     passwordField.selectionStart = passwordField.selectionEnd = 10000;
        // }, 10);
    }

    closeError() {
        this.lp.stop();
    }

    validate_char(c: string) {
        if ((c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57)
            || (c.charCodeAt(0) >= 65 && c.charCodeAt(0) <= 90)
            || (c.charCodeAt(0) >= 97 && c.charCodeAt(0) <= 122)) {
            return true;
        } else {
            return false;
        }
    }

    validate_num(c: string) {
        if (c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57) {
            return true;
        } else {
            return false;
        }
    }

    getMaskLength(step: any) {
        return (step.mask.match(/_/g) || []).length;
    }

    applyMask(mask: string) {
        var validChars: string[] = [];
        var text = this.phoneInput;
        var maskedText = "";

        for (var i = 0; i < text.length; i++) {
            if (this.validate_num(text.charAt(i))) {
                validChars.push(text.charAt(i));
            }
        }

        for (var j = 0, i = 0; j < mask.length && i < validChars.length; j++) {
            if (mask.charAt(j) == "_") {
                maskedText = maskedText + validChars[i];
                i++;
            } else {
                maskedText = maskedText + mask.charAt(j);
            }
        }
        this.phoneInput = maskedText;
    }

    getValidCharacters(text: string) {
        var finalText = '';

        for (var i = 0; i < text.length; i++) {
            if (this.validate_num(text.charAt(i))) {
                finalText = finalText + text.charAt(i);
            }
        }
        return finalText.toUpperCase();
    }

    openLoginModal() {
        this.showLoginModal = true;
    }

    closeLoginModal() {
        this.showLoginModal = false;
        if (localStorage.getItem('USER_DATA')) this.$router.push('Home');
    }

    openLogin() {
        this.LoginModal = true;
    }

    handleVirtualKeyPress(digit: string) {
        switch (digit) {
            case "\u232B":
                this.popCharacter()
                break;
            case "\u21E7":
                this.alterarTeclado(digit)
                break;
            case "\u21EA":
                this.alterarTeclado(digit)
                break;
            default:
                this.pushCharacter(digit)
                console.log(digit)
                break;
        }
    }
    pushCharacter(digit: string) {
        if (this.alteraInput == 1) {

            this.emailInput = $('#login-email').val() + digit
            $('#login-email').val(this.emailInput)
            $('#ADMIN_EMAIL_INPUT').val(this.emailInput)

        } else if (this.alteraInput == 2) {

            this.passInput = $('#login-senha').val() + digit
            $('#login-senha').val(this.passInput)
            $('#ADMIN_PASSWORD_INPUT').val(this.passInput)

        } else {

            this.urlInput = $('#url-input').val() + digit
            $('#url-input').val(this.urlInput)
            $('#ADMIN_URL_INPUT').val(this.urlInput)
        }
    }

    popCharacter() {
        if (this.alteraInput == 1) {

            this.emailInput = this.emailInput.substring(0, this.emailInput.length - 1);
            $('#login-email').val(this.emailInput)
            $('#ADMIN_EMAIL_INPUT').val(this.emailInput)

        } else if (this.alteraInput == 2) {

            this.passInput = this.passInput.substring(0, this.passInput.length - 1);
            $('#login-senha').val(this.passInput)
            $('#ADMIN_PASSWORD_INPUT').val(this.passInput)

        } else if (this.alteraInput == 3) {

            this.urlInput = this.urlInput.substring(0, this.urlInput.length - 1);
            $('#url-input').val(this.urlInput)
            $('#ADMIN_URL_INPUT').val(this.urlInput)
        }

    }

    /* E NECESSARIO VE O NRORG E PERFIL DE UM GEN_USER_TAA */
    totemAuth() {
        const req = new Request();
        req.route = 'loginBackofficeCheckingStatusdifferentofI';
        req.body = {
            'EMAIL': this.emailInput,
            'PASSWORD': this.passInput,
            'NRORG': 0,
            'USER_TYPE_ID': 4,
            'TOTEM_USER': true
        };
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('USER_ID', String(response.user.userId));
            localStorage.setItem('REFRESH_TOKEN', response.user.refresh_token);
            this.posSerialNumber = response.user.pos_serial_number;
            this.sessionLogIn();
        };
        req.callbackError = (error: any) => {
            const _window: any = window;
            var mensagem: string;

            if (_window.navigator.onLine === false) mensagem = 'Sem conexão com a internet.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 404) mensagem = 'Usuário não encontrado.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 2) mensagem = 'Usuário ou senha incorretos.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 3) mensagem = 'Usuário ou senha incorretos.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 10 || error && error.response && error.response.data && error.response.data.errorCode == 11) mensagem = 'Usuário não autenticado.';
            else if (error && error.response && error.response.data && error.response.data.errorCode == 1002) mensagem = error.response.data.error;
            else mensagem = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';

            this.showModal(mensagem);
        };
        req.callbackTimeout = () => { this.showModal('Não foi possível estabelecer uma conexão para completar'); };
        if (!window.navigator.onLine) { this.showModal("Não é possível completar a operação offline"); }
        else req.send();
    }

    sessionLogIn() {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'REFRESH_TOKEN': localStorage.getItem('REFRESH_TOKEN') || ''
        };
        req.route = 'requestToken';
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('TOKEN', response.user.token);
            this.getUserOrganizations();
            this.getStoneToken();
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        // req.loadingMessage = 'Buscando suas informações...';
        req.blockTouchEvents = false;
        req.send();
    }

    getUserOrganizations() {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'USER_TYPE_ID': 4,
            'TOKEN': localStorage.getItem('TOKEN')
        };
        req.route = 'getUserOrganizations';
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('USER_ORGANIZATIONS', JSON.stringify(response.user.organizations));
            this.nrorg = response.user.organizations[0].nrorg;
            localStorage.setItem('NRORG', response.user.organizations[0].nrorg)
            this.getOrganizationData();
            this.getStructures();
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        // req.loadingMessage = 'Buscando suas informações...';
        req.blockTouchEvents = false;
        req.send();
    }

    getOrganizationData() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': this.nrorg,
            'USER_ID': localStorage.getItem('USER_ID'),
            'USER_TYPE_ID': 4,
        };
        req.route = 'getOrganizationData';
        req.callbackSuccess = (response: any) => {
            const name: string = response.organization.NAME;
            const editableFields: any = response.organization.EDITABLE_FIELDS;
            let PRIMARY_COLOR = response.organization.CONFIGURATIONS.PRIMARY_COLOR;
            let SUPPORT_COLOR = response.organization.CONFIGURATIONS.SECONDARY_COLOR;

            localStorage.setItem('ORG_NAME', name);
            localStorage.setItem('ORG_FIELDS', JSON.stringify((editableFields)));
            localStorage.setItem('ORGANIZATION_DATA', JSON.stringify((response.organization)));
            localStorage.setItem('ORGANIZATION_LOGO', response.organization.CONFIGURATIONS.LOGO_IMAGE);
            localStorage.setItem('ORGANIZATION_LOGO_SMALL', response.organization.CONFIGURATIONS.LOGO_IMAGE_SMALL);
            localStorage.setItem('STONE_ESTABLISHMENT_ID', response.organization.CONFIGURATIONS.STONE_ESTABLISHMENT_ID);
            this.getEstablishmentData(response.organization.CONFIGURATIONS.STONE_ESTABLISHMENT_ID);
            if (document.documentElement) {
                document.documentElement.style.setProperty('--primary-color', PRIMARY_COLOR);
                localStorage.setItem('PRIMARY_COLOR', PRIMARY_COLOR)

            }
            if (document.documentElement) {
                document.documentElement.style.setProperty('--secundary-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--support-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--secundary-support-color', SUPPORT_COLOR);
                localStorage.setItem('SECONDARY_COLOR', SUPPORT_COLOR);
            }

            //LoginFrontEnd.callback();

        };
        req.callbackError = function (error: any) {
            let mensagem: string;
            if (error.response.data.errorCode == 24) {
                mensagem = 'Usuário não autorizado pelo titular!';
                self.showModal(mensagem);
            }
            if (error.response.data.errorCode == 14) {
                mensagem = 'Usuário não autorizado pela organização!';
                self.showModal(mensagem);
            }
            console.log(error);
        };
        // req.loadingMessage = 'Preparando o aplicativo...';
        req.blockTouchEvents = false;
        req.send();
    }


    getStructures() {
        return new Promise((resolve: Function, reject: Function) => {
            const req = new Request();
            req.route = 'getStructures';
            req.body = {
                'NRORG': this.nrorg,
                'TYPE': 'U'
            };
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('ORGANIZATION_STRUCTURES', JSON.stringify(response.structures));
                this.$router.push('ChooseStructure');
            };
            req.callbackError = (error: any) => {
                const _window: any = window;
                var mensagem: string;

                if (_window.navigator.onLine === false) mensagem = 'Sem conexão com a internet.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 1) mensagem = 'Usuário não encontrado.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 2) mensagem = 'Usuário ou senha incorretos.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 3) mensagem = 'Usuário ou senha incorretos.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 10 || error && error.response && error.response.data && error.response.data.errorCode == 11) mensagem = 'Usuário não autenticado.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 1002) mensagem = error.response.data.error;
                else mensagem = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';

                this.showModal(mensagem);
            };
            req.callbackTimeout = () => { this.showModal('Não foi possível estabelecer uma conexão para completar'); };
            if (!window.navigator.onLine) { this.showModal("Não é possível completar a operação offline"); }
            else req.send();
        });
    }

    getStoneToken() {
        const req = new Request();

        req.callbackSuccess = (response: any) => {
            // this.getStonePosReferenceId(this.posSerialNumber);
        };
        req.callbackError = (error: any) => {
            console.log(error);
        };
        req.getStoneToken();
    }

    getStonePosReferenceId(serialNumber: string) {
        const req = new Request();

        req.body = {
            "pos_serial_number_to_link": serialNumber,
            "cashier_number": "",
            "pdv_number": "",
            "pos_link_label": "",
        }
        req.callbackSuccess = (response: any) => {
            var posReferenceId = response.pos_link.pos_reference_id;

            if (posReferenceId !== "" && posReferenceId !== null && posReferenceId !== 'undefined') {
                localStorage.setItem('POS_REFERENCE_ID', posReferenceId);
            }
            if (serialNumber !== "" && serialNumber !== null && serialNumber !== 'undefined') {
                localStorage.setItem('POS_SERIAL_NUMBER', serialNumber);
            }
        };
        req.callbackError = (error: any) => {
            console.log(error);
        };
        req.getPosReferenceId();
    }

    getEstablishmentData(stoneEstablishmentId: string) {
        const req = new Request();

        req.body = stoneEstablishmentId;
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('STONE_ESTABLISHMENT_DATA', JSON.stringify(response.establishment));
        };
        req.callbackError = (error: any) => {
            console.log(error);
        };
        req.getEstablishmentData();
    }

    updateEmailInput(newValue: string) {
        this.emailInput = newValue;
    }

    updatePasswordInput(newValue: string) {
        this.passInput = newValue;
    }

    updateUrlInput(newValue: string) {
        this.urlInput = newValue;
    }

    alterBackendUrl() {
        this.modalUrl = true;
    }

    confirmUrlChange() {
        this.modalUrl = false;
        const req = new Request();
        if (this.urlInput) req.setUrlBase(this.urlInput);
    }

    cancelUrlChange() {
        this.modalUrl = false;
        this.urlInput = "";
    }

    revertBackendUrl() {
        this.modalUrl = false;
        this.urlInput = "";
        const req = new Request();
        req.setUrlBaseToDefault();
    }

    exitKiosk() {

        if (this.exitCounter > 15) {
            KioskPlugin.exitKiosk();
            Util.syncCodePush();
        }
        else {
            this.exitCounter += 1;
        }
    }

    alterarTeclado(digit: string) {
        if (digit == "\u21E7") {
            this.virtualKeyboard = [{ value: "." }, { value: "@" }, { value: "/" }, { value: "-" }, { value: "_" }, { value: ":" }, { value: "!" }, { value: "?" }, { value: "´" }, { value: "`" }, { value: "Q" }, { value: "W" }, { value: "E" }, { value: "R" }, { value: "T" }, { value: "Y" }, { value: "U" }, { value: "I" }, { value: "O" }, { value: "P" },
            { value: "nan" }, { value: "A" }, { value: "S" }, { value: "D" }, { value: "F" }, { value: "G" }, { value: "H" }, { value: "J" }, { value: "K" }, { value: "L" },
            { value: "\u21EA" }, { value: "Z" }, { value: "X" }, { value: "C" }, { value: "V" }, { value: "B" }, { value: "N" }, { value: "M" }];

        } else if (digit == "\u21EA") {
            this.virtualKeyboard = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" }, { value: "7" }, { value: "8" }, { value: "9" }, { value: "0" }, { value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
            { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
            { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];
        }
    }

    alterarReadOnly() {
        this.misterio++
        if (this.misterio >= 10 && this.misterio < 15) {
            this.active = false
        }
    }
}
