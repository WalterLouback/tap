declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module "*.axios" {
    import axios from "axios";
    export default axios;
}

declare module "*.moment" {
  import moment from "moment";
  export default moment;
}

declare module "v-money";

declare module "vue-swipeable-bottom-sheet";

// declare module "*.cordova-plugin-device" {
//   import device from "cordova-plugin-device";
//   export default device;
// }

// declare module "*.cordova-vue"{
//   import VueCordova from "cordova-vue";
//   export default VueCordova;
// }
//declare module "cordova-plugin-device";npm install --save-dev @types/cordova-