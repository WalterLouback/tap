import Router from '@/router';
import LogIn from './src/views/LogIn.vue';
import ForgotPassword from './src/views/ForgotPassword.vue';
import Register from './src/views/Register.vue';
import ChooseOrganization from './src/views/ChooseOrganization.vue';
import ChooseStructure from './src/views/ChooseStructure.vue';
import ChooseTaas from './src/views/ChooseTaas.vue';

import './src/stylesheet/app.scss';
import './src/stylesheet/components.scss';
import './src/stylesheet/views.scss';
import Util from './src/ts/Util';

export default class LoginFrontEnd { 

    static callback = new Function();

    public static start(config: LoginConfig) {
        this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback();
        }
        if (!localStorage.getItem('STRUCTURE_DATA')) {
            localStorage.setItem('LOGIN_PROPS', JSON.stringify(config));
            if(!localStorage.getItem('NRORG'))localStorage.setItem('NRORG', config.nrorg);
            this.safePush('LogIn')
            
        } else {
            localStorage.setItem('LOGIN_PROPS', JSON.stringify(config));
            if(!localStorage.getItem('NRORG'))localStorage.setItem('NRORG', config.nrorg);
            this.callback();
        }
    }

    public static safePush(d: string){
        if (localStorage.getItem('LOGIN_PROPS') && localStorage.getItem('NRORG')){
            Router.push(d);
        }
        else setTimeout(()=>{this.safePush(d)}, 10000)
    }

    public static goToRegister(callback: Function) {
        this.callback = callback;
        if (!localStorage.getItem('USER_ID')) {
            Router.push('Register');
        } else callback();
    }

    static logOff() {
        localStorage.setItem('CAME_FROM_LOGOUT', 'true');
        localStorage.setItem('CAME_FROM_FORGOT_PASSWORD', 'true');
        localStorage.setItem('USER_ID', '');
        localStorage.setItem('USER_DATA', '');
        localStorage.setItem('TOKEN', '');
        localStorage.setItem('REFRESH_TOKEN', '');
        localStorage.setItem('TOKEN_EXT', '');
        localStorage.setItem('ID_EXT', '');
        localStorage.setItem('KEY_EXT', '');
        localStorage.setItem('TOTP', '');
        localStorage.setItem('GET_MODALITY_FROM_LS', '');
        localStorage.setItem('GET_CLASS_FROM_LS', '');
        $('.sidenav').sidenav('close');

        this.addRoutes();
        Router.push('LogIn');
    }

    public static addRoutes() {
        Router.addRoutes([
            {
                path: '/LogIn',
                component: LogIn
            },
            {
                path: '/ForgotPassword',
                component: ForgotPassword
            },
            {
                path: '/Register',
                component: Register
            },
            {
                path: '/ChooseOrganization',
                component: ChooseOrganization
            },
            {
                path: '/ChooseStructure',
                component: ChooseStructure
            },
            {
                path: '/ChooseTaas',
                component: ChooseTaas
            }
        ]);
    }

}

export class LoginConfig {

    loginMethods: Array<string>;
    introductionMessage: string;
    introductionMessageHighlight: string;
    logo: string;
    backgroundImage:string;
    callback: Function;
    nrorg: string;
    // registerData: any;
    introductionRegisterMessage: string;
    introductionRegisterMessageHighlight: string;
    primaryColor: string;
    secondaryColor: string;
     

    constructor(loginMethods: Array<string>, introductionMessage: string, introductionMessageHighlight: string,
        logo: string, callback: Function, introductionRegisterMessage: string, introductionRegisterMessageHighlight: string,
        nrorg: string = "0", backgroundImage:string = "",
        primaryColor:string = "#2D68C4", secondaryColor:string = "#11386B") {
            this.loginMethods = loginMethods;
            this.introductionMessage = introductionMessage;
            this.introductionMessageHighlight = introductionMessageHighlight;
            this.logo = logo;
            this.callback = callback;
            this.nrorg = (nrorg) ? nrorg : "0";
            // this.registerData = registerData;
            this.introductionRegisterMessage = introductionRegisterMessage;
            this.introductionRegisterMessageHighlight = introductionRegisterMessageHighlight;
            this.backgroundImage = backgroundImage; 
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
            localStorage.setItem('ORGANIZATION_LOGO_SMALL', logo)
            if(!localStorage.getItem('PRIMARY_COLOR')){
                localStorage.setItem('PRIMARY_COLOR', primaryColor)
                localStorage.setItem('SECONDARY_COLOR', secondaryColor)
                if (document.documentElement) document.documentElement.style.setProperty('--primary-color', primaryColor);
                if (document.documentElement) document.documentElement.style.setProperty('--secundary-color', secondaryColor);
                if (document.documentElement) document.documentElement.style.setProperty('--support-color', secondaryColor);
                if (document.documentElement) document.documentElement.style.setProperty('--secundary-support-color', secondaryColor);
            }
    }

}
