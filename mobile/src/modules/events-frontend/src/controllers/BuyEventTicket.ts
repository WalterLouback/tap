import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import TicketBuyHeader from '../components/TicketBuyHeader.vue';
import TicketBuy from '../components/TicketBuy.vue';
import EventDetailImage from '../components/EventDetailImage.vue';
import TextLink from '../components/TextLink.vue';
import InputText from '../components/InputText.vue';
import DefaultButton from '../components/DefaultButton.vue';
import SelectField from '../components/SelectField.vue';
import SectionArea from '../components/SectionArea.vue';
import OptionField from '../components/OptionField.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import InvisibleHeader from '../components/InvisibleHeader.vue';
import SectionTitle from '../components/SectionTitle.vue';
import ProductMenu from './ProductMenu';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components//HeaderCircleButton.vue'
import LoginModal from '@/modules/shared-components//LoginModal.vue';

declare var $: any;
@Component({
    components: { TicketBuyHeader, TicketBuy, EventDetailImage, TextLink, InputText, DefaultButton, SelectField, OptionField, HeaderFluxo, ModalCodigo, InvisibleHeader, SectionTitle, SectionArea, HeaderCircleButton, LoginModal }
})

export default class BuyEventTicket extends Vue {
    structures: any = [];
    events: any = [];
    eventDetail: any = JSON.parse(localStorage.getItem('EVENT') || '{}');
    ticketsEvent: any = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
    showModalTelaCartoes: boolean = false;
    nome: any = [];
    userData: any = JSON.parse(localStorage.getItem('USER_DATA') || '[]');
    items: any = [];
    USER_DATA: any = [];
    totalValue: any = 0;
    countTicket: any = 0;
    showModal: boolean = false;
    amounts: any = [];
    mainCardId: any;
    card: any;
    creditCards: Array<object> = JSON.parse(localStorage.getItem('CREDIT_CARDS') || '[]');
    mainCartao: any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
    allTicketsAreFree: boolean = false;
    backButton: any = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack};
    showModalLogin: boolean = false;

    disabledButton(ID: any){
        const self = this;
        setTimeout(function() {
            if(self.items.length == 0) $('#'+ID).prop('disabled', true);
            if(self.items.length > 0) $('#'+ID).prop('disabled',false);
        }, 200);
    }

    addCountTicket(ID: any) {
        const ticket = this.ticketsEvent.find(((ticket:any) => ticket.id == ID));
        let amountToAdd = ticket.minPurchaseAmount ? ticket.minPurchaseAmount : 1;

        const amount = this.amounts.find(((amount:any) => amount.ID == ID));
        amount.AMOUNT += amountToAdd;
        
        for (let i = 0; i < amountToAdd; i++) this.items.push(ticket);

        this.totalTicketsValue();
        this.disabledButton('finalizarCompra');
    }

    telaCartoes(){
        this.$router.push('RegisteredCards');
    }

    subCountTicket(ID: any) {
        const pos = this.items.findIndex(((ticket:any) => ticket.id == ID));

        let amountToSub = this.items[pos].minPurchaseAmount ? this.items[pos].minPurchaseAmount : 1;
        const amount = this.amounts.find(((amount:any) => amount.ID == ID));
        if(amount.AMOUNT == 0)amount.AMOUNT += 0;
        if(amount.AMOUNT > 0)amount.AMOUNT -= amountToSub;

        if (pos != -1) this.items.splice(pos, amountToSub);

        this.totalTicketsValue();
        this.disabledButton('finalizarCompra');
    }

    totalTicketsValue() {
        this.totalValue = 0;
        this.items.forEach((ticket:any) => {
            this.totalValue += ticket.price;
        });
    }

    formatDate(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('DD [de] MMM [às] HH:mm');
    }

    verify( date:any ){
        return moment() < moment(date);
    }

    verifyAmount( amount:any ){
        return amount > 0;
    }

    verifyAllTickets(){
        let ticketsOpen:number = 0;
        this.ticketsEvent.forEach(( ticket:any ) => {
            if(moment(ticket.finalSale.date) > moment()) ticketsOpen+= 1;
        });
        if(ticketsOpen == 0) return false;
        if(ticketsOpen > 0) return true;
    }

    formatFinalSale(date: any){
        let format: any;
        format = moment(date).lang('pt-br').format('DD/MM/YY');
        return format;
    }

    formatHour(ID: any){
        let format: any;
        let event = this.events.find((event: any) => event.id == ID);
        if (event) format = moment(event.initialDate.date).lang('pt-br').format('hh[h]mm');
        return format;
    }

    getMenus() {
        const MENU = new ProductMenu();
        const self = this;
        MENU.getMenus(function() {
            self.nome = JSON.parse(localStorage.getItem('EVENT_MENU') || '{}');
        });
        
    }

    getTicketByEvent() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.eventDetail.id
            };
            req.route = 'getTicketByEvent';
            req.callbackSuccess = function(response: any) { 
                let ticketsEvent = response.eventTickets;
                self.ticketsEvent = ticketsEvent;
                self.prepareTickets();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    prepareTickets() {
        let allTicketsAreFree = true;
        this.ticketsEvent.forEach((ticket:any) => {
            this.amounts.push({'ID': ticket.id, 'AMOUNT': 0 });
            allTicketsAreFree = allTicketsAreFree && ticket.price == 0;
        });
        this.allTicketsAreFree = allTicketsAreFree;
    }

    getUserData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'USER_TYPE_ID': 1,
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || "")

            };
            req.route = 'getUserData';
            req.callbackSuccess = function(response: any) { 
                self.USER_DATA = response.user;
                localStorage.setItem('USER_DATA', JSON.stringify(response.user));
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    createOrderTicket(ID: any) {
        try {
            const self = this;
            const req = new Request();
            const getUserDataValues: any = {
                'PAYMENT_METHOD': 'CC',
                'CREDIT_CARD_ID': this.USER_DATA.MAIN_CARD_ID,
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': ID,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'ORDER_ITEMS':[],
            };
            req.body = getUserDataValues;
            req.route = 'createOrderTicket';
            this.amounts.forEach(function(item: any) {
                getUserDataValues.ORDER_ITEMS.push(
                    {
                        'QUANTITY': item.AMOUNT,
                        'EVENT_TICKET_ID': item.ID
                    }
                )
            });

            req.callbackSuccess = function(response: any) { 
                self.goToTicketBuyed();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = true;
            req.send();
            } catch (e) {
                
                console.log(e);
            }

    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getCreditCards();
        this.getEvent();
        this.getTicketByEvent();
        this.getMenus();
        this.disabledButton('finalizarCompra');
        this.prepareTickets();
        this.verifyAllTickets();
    }

    amountFromId(ID: any){
        var a = this.amounts.find((amount:any) => amount.ID == ID);
        if (a) return a.AMOUNT;
        else return 0;
    }

    continuar() {
        if (!localStorage.getItem('USER_ID')){
            this.openModalLogin();
        }
        else {
            const self = this;
            this.getMainCardId();
        }
    }

    lastNumberCard( lastNumbers:any ){
        let lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
        return lastNumber;
    }

    telaPaymentMethods(){
        
        // localStorage.setItem('deliverToBalcony', 'false');
        // localStorage.setItem('deliversToTable', 'true');
        if (!localStorage.getItem('USER_ID')){
            this.openModalLogin();
        }
        else this.$router.push('TicketPaymentMethod');
    }

    getMainCardId(){ 
        if(!this.mainCartao.FLAG && !this.allTicketsAreFree) this.mostraModalTelaCartoes();
        else {
            localStorage.setItem('ORDER_ITEMS', JSON.stringify(this.items));
            localStorage.setItem('AMOUNT_ITEMS', JSON.stringify(this.amounts));
            this.$router.push('NominateTicket');
        }
    }

    findMainCard(mainCardId: number, cards: any) {
        return cards.find((card:any) => card.ID == mainCardId);
    }

    getCreditCards() {

        try {
            const self = this;
            const req = new Request();
                req.body = {
                    'USER_ID': localStorage.getItem('USER_ID'),
                    'TOKEN': localStorage.getItem('TOKEN'),
                    'NRORG': localStorage.getItem('NRORG'),
            };
            req.route  = 'getValidCreditCard';
            req.callbackSuccess = function(response: any) {

                let creditCards = response.creditcards;
                self.creditCards = creditCards;
                if (response.creditcardFromParent) self.creditCards.push(response.creditcardFromParent);
                if (self.creditCards && self.creditCards.length > 0) localStorage.setItem('CREDIT_CARDS', JSON.stringify(self.creditCards));
                self.mainCartao = {};
                self.creditCards.forEach(( card:any ) => {
                    if(card.IS_MAIN_CARD == true){
                        self.mainCartao = card;
                    }
                });
                localStorage.setItem('MAIN_CARTAO', JSON.stringify(self.mainCartao));
                self.getUserData();
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            if(localStorage.getItem('USER_ID')) req.send();
        } catch (e){console.log(e);}
    }

    mostraModalTelaCartoes() {
        this.showModalTelaCartoes = true;
    }

    closeModal() {
        this.showModal = false;
    }

    goToProductMenu(){
        this.$router.push({ name: 'ProductMenu' });
    }

    goToTicketBuyed(){
        this.$router.push({ name: 'TicketBuyed' });
    }

    getEvent() {
       this.events      = JSON.parse(localStorage.getItem('EVENTS') || '{}');
       this.eventDetail = JSON.parse(localStorage.getItem('EVENT') || '{}');
    }

    goBack() {
        this.$router.go(-1);
    }

    openModalLogin(){
        this.showModalLogin = true;
    }
    closeModalLogin() {
        this.showModalLogin = false;
        this.getCreditCards();
    }
}