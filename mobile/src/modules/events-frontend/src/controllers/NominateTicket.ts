import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionTitle from '@/components/SectionTitle.vue';
import SectionArea from '@/components/SectionArea.vue';
import EventDetailImage from '@/components/EventDetailImage.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import DefaultButtonOrd from '@/components/DefaultButtonOrd.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SelectField from '@/components/SelectField.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import TextLink from '@/components/TextLink.vue';
import Request from '@/ts/Request';
import BiometryCheck from '@/ts/BiometryCheck';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import EventsInitializer from '../../EventsInitializer';


declare var M: any;
declare var $: any;
@Component({
    components: { SectionTitle, EventDetailImage, DefaultButton, HeaderFluxo, SelectField, TextLink, ModalCodigo, DefaultButtonOrd, SectionArea, HeaderCircleButton }
})

export default class NominateTicket extends Vue {
    newsDetail: any = [];
    dependents: any = [];
    selectedDependents: any = [];
    userNames: string[] = [];
    ticketsNominate: any = [];
    ORDER_ITEMS: any = [];
    showModal: any = false;
    showModalError: any = false;
    showModalSuccess: any = false;
    showModalTelaCartoes: boolean = false;
    modalMessage: String = '';
    dependenteX: String = '';
    orders: any = JSON.parse(localStorage.getItem('ORDER_ITEMS') || '[]');
    totalValueOrders: number = 0;
    amount_items: any = JSON.parse(localStorage.getItem('AMOUNT_ITEMS') || '[]');
    user_data: any = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    event: any = JSON.parse(localStorage.getItem('EVENT') || '{}');
    modalPasswordRequest: boolean = false;
    passwordValue: any = '';
    passwordWrong: boolean = false;
    passwordUser: any = localStorage.getItem('QBTT');
    acceptFinger: any = localStorage.getItem('ACCEPT_BIOMETRY') || '';
    biometry: BiometryCheck = new BiometryCheck();
    backButton: any = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.orders.forEach((order:any) => {
            this.totalValueOrders += order.price;
        });
        const userName = this.user_data['FIRST_NAME'] + ' ' + this.user_data['LAST_NAME'];
        this.dependents.push({'name': userName, 'id': this.user_data['ID']});

        (this.user_data.dependents || []).forEach((dependent: any) => {
            let dependentName = dependent['FIRST_NAME'] + ' ' + dependent['LAST_NAME'];
            this.dependents.push({'name': dependentName, 'id': dependent['ID']});
        })
        
        Vue.nextTick(() => {
            $('select').formSelect();
        });
    }

    updateTextFields() {
        Vue.nextTick(() => {
            M.updateTextFields();
        });
    }

    nominateAllTickets(){
        this.orders.forEach((item:any, index: number) => {
            let user   = this.createUser(index);
            let ticket = this.ORDER_ITEMS.find((t: any) => t.EVENT_TICKET_ID == item.id);

            if (!ticket) {
                this.ORDER_ITEMS.push({
                    'EVENT_TICKET_ID': item.id,
                    'USERS': [ user]
                });
            } else {
                ticket.USERS.push(user);
            }
        });
    }

    goBack() {
        this.$router.go(-1);
    }
    
    createUser(index: number) {
        let name: any = '', idUser: any = '';

        if (this.selectedDependents[index] != -3) {
            idUser = this.selectedDependents[index];
        }
        else {
            name = this.userNames[index];
        }

        return { 'NAME': name, 'ID': idUser };
    }

    openPasswordRequest(){
        var self = this
        this.showModal = false;
        if(self.acceptFinger == 'true'){
            self.biometry.useTouchID(function() {
                self.createOrderTicket();
            });
        } else this.modalPasswordRequest = true;
    }

    closePasswordRequest(){
        this.modalPasswordRequest = false;
        this.passwordValue = '';
        this.passwordWrong = false;
    }

    afterPasswordRequest(){
        let self = this;
        if(this.passwordValue == this.passwordUser){
            this.createOrderTicket();
            this.modalPasswordRequest = false;
            this.showModal = false;
        }else if(this.passwordValue != this.passwordUser){
            self.passwordWrong = false;
            setTimeout(function(){self.passwordWrong = true;}, 10);
        }
    }

    validateTicket(index: any){
        let self = this;
        let encontrou = false;
        const el = $('#dependente_nome' + index);
        const value = el.val();
        self.dependents.forEach((dependent:any) => {
            if(value == dependent.NAME){
                encontrou = true;
                return 0;
            }
        });
        if (!encontrou) self.dependenteX = value;
        return encontrou;
    }

    validateAllTickets(){
        let self = this;
        let encontrou = false;
        this.orders.forEach((order: any , index: number) => {
            if($('#dependente'+index).prop('checked') == true){
                if(!self.validateTicket(index)){
                    const text: String = "O dependente/titular "+self.dependenteX+" do ingresso "+(index+1)+" não existe!";
                    self.openModalError(text);
                    encontrou = true;
                    return 0;
                }
            }
            if($('#outros'+index).prop('checked') == true){
                const el = $('#outro_name'+index);
                const value = el.val();
                const regex = '[^a-zA-Z záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+';
                if(value == ''){
                    const text: String = "Você não definiu um privilegiado para o ingresso "+(index+1)+" !";
                    self.openModalError(text);
                    encontrou = true;
                    return 0;
                }
                if(value.match(regex)){
                    const text: String = "Favor colocar apenas letras no nome !";
                    self.openModalError(text);
                    encontrou = true;
                    return 0;
                }
            }
            if( $('#outros'+index).prop('checked') == false && $('#dependente'+index).prop('checked') == false){
                const text: String = "Favor definir algum privilegiado para o ingresso "+(index+1)+" !";
                self.openModalError(text);
                encontrou = true;
                return 0;
            }
        });
        if (!encontrou) this.openModal();
    }

    createOrderTicket() {
        if(!this.user_data.MAIN_CARD_ID){
            this.mostraModalTelaCartoes();
            return 0;
        }
        const self = this;
        const req = new Request();
            req.body = {
                'PAYMENT_METHOD': 'CC',
                'CREDIT_CARD_ID': this.user_data.MAIN_CARD_ID,
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.event.id,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'TOKEN_EXT': localStorage.getItem('TOKEN_EXT'),
                'ORDER_ITEMS': this.ORDER_ITEMS,
        };
        req.route  = 'createOrderTicket';
        req.callbackSuccess = function(response: any) {
            self.getUserTicketsByOrg();
        };
        req.callbackError = function(error: any) {
            $('body').css('pointer-events', 'auto');
            $('html').css('pointer-events', 'auto');

            if (error.response.data.errorCode == 105317){
                self.showModalError = true;
                self.modalMessage = error.response.data.error.replace('Exception: ', '');
            }
            else if (error.response.data.errorCode == 12) {
                self.showModalError = true;
                self.modalMessage =  'Seu limite de compras neste cartão foi excedido.';
            } 
            else if (error.response.data.errorCode == 7){
                //req.showErrorMessage('Esta transação não foi foi autorizada pelo seu cartão.');
                self.showModalError = true;
                self.modalMessage = 'Esta transação não foi foi autorizada pelo seu cartão.';
            }
            else if (error.response.data.errorCode == 6){
                self.showModalError = true;
                self.modalMessage = 'Ocorreu um erro com o pagamento.'
            } 
            else {
                self.showModalError = true;
                self.modalMessage = 'Ocorreu algum erro desconhecido.'
            }
        }
        req.blockTouchEvents = true;
        req.send();
    }

    getUserTicketsByOrg() {
        var self = this;
        try {
            const req: any = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "NRORG": localStorage.getItem('NRORG')
            };
            req.route = 'getUserTicketsByOrg';
            req.callbackSuccess = function(response: any) {
                let mensagem: string = "Compra de ingressos feita com sucesso!";
                localStorage.setItem('USER_TICKETS', JSON.stringify(response.userTickets.filter((tickets: any) => moment().isBefore(tickets.finalDate.date))));
                self.openModalSuccess(mensagem);
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.send();
        } catch (e) {
            
            console.log(e)
        }
    }

    verifyRadioDependent(INDEX: any){
        if($('#dependente'+INDEX).prop('checked')){
            $('#nameDependente'+INDEX).removeClass('display-none').addClass('display-flex');
            $('#nameOutro'+INDEX).removeClass('display-flex').addClass('display-none');
            $('#outro_name'+INDEX).val("");
        }

        let input: any = document.querySelector('#dependente_nome' + INDEX);
        const instance = M.Autocomplete.getInstance(input);
        instance.options.minLength = 0;
    }

    goToTelaCartoes() {
        this.$router.push('RegisteredCards');
    }

    mostraModalTelaCartoes() {
        this.showModalTelaCartoes = true;
    }

    fechaModalTelaCartoes() {
        this.showModalTelaCartoes = false;
    }

    openModal() {
        this.showModal = true;
        this.nominateAllTickets();
    }

    openModalError(text: any){
        this.modalMessage = text;
        this.showModalError = true;
    }

    openModalSuccess(text: any){
        this.modalMessage = text
        this.showModalSuccess = true;
    }

    closeModal() {
        this.showModal = false;
        // this.$router.push({ name: 'BuyEventTicket' });
    }

    closeModalError() {
        this.showModalError = false;
    }

    verifyRadioOutros(INDEX: any){
        if($('#outros'+INDEX).prop('checked')){
            $('#nameDependente'+INDEX).removeClass('display-flex').addClass('display-none');
            $('#nameOutro'+INDEX).removeClass('display-none').addClass('display-flex');
            $('#dependente_nome'+INDEX).val('');
        }
    }

    goToCarteira(){
        this.showModalSuccess = false;
        this.$router.push({ name: 'HomeApp' });
    }

    focusInput( id:any ){
        const offset = -60;
        const el: any = $("#"+id);
        setTimeout(function(){
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        },150);
    }

    goToRecentOrders(){
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'NominateTicket')
        if(EventsInitializer.hasCallback) this.$router.push('PedidosRealizados');
        else this.$router.push('Ingressos');
    }

}