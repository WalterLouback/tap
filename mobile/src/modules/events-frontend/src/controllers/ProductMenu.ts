import {Component, Prop, Vue} from 'vue-property-decorator';
import BalanceAndCredit from '@/components/BalanceAndCredit.vue';
import SectionArea from '@/components/SectionArea.vue';
import Balance from '../views/Balance.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import Carousel from '@/components/Carousel.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SelectField from '@/components/SelectField.vue';
import ListDefault from '@/components/ListDefault.vue';
import OptionCard from '@/components/OptionCard.vue';
import SectionTitle from '@/components/SectionTitle.vue';
import ImageHeader from '@/components/ImageHeader.vue';
import Product from '@/components/Product.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';

declare var M: any;
declare var $: any;
@Component({
	components: {
		SectionArea,
		BalanceAndCredit,
		InputText,
		DefaultButton,
		Carousel,
		HeaderFluxo,
		SelectField,
		OptionCard,
		ListDefault,
		SectionTitle,
		ImageHeader,
		Product,
		ModalCodigo
	}
})

export default class ProductMenu extends Vue {

	menu: object[] = [];
	event: any = JSON.parse(localStorage.getItem('event') || '{}');
	balance: any = JSON.parse(localStorage.getItem('WALLET') || '{}');
	menus: any = [];
	categories: any = [];
	errorMessage : string =  "";
	showModalPagamento: boolean = false;
	alreadyShowed : any = localStorage.getItem('MODAL_INFO');

	mounted() {
		Util.setBackButtonBehavior(this.goBack);
		if(!this.alreadyShowed)	this.modalInfoEvent();
		this.getWallet();
		this.getMenus();
	}

	goBack() {
		this.$router.go(-1);
	}

	goToBalance() {
		this.$router.push('Balance');
	}

	getMenus(callback=function(){}) {  
		try {
            const self = this;
            const req = new Request();
            req.body = {
				'EVENT_ID': localStorage.getItem('SELECTED_EVENT'),
				'USER_ID': localStorage.getItem('USER_ID'),
				'TOKEN': localStorage.getItem('TOKEN')
            };
            req.route = 'getMenus';
            req.callbackSuccess = function(response: any) { 
				self.event = response.event;
				self.menus = self.event.menus || [];
				localStorage.setItem('EVENT_MENU', JSON.stringify(self.menus));
				localStorage.setItem('EVENT_NAME_QRCODE', JSON.stringify(self.event.name));
				self.categories = self.menus && self.menus[0] ? self.menus[0].productGroups : [];
				callback();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            if(localStorage.getItem('USER_ID')) req.send();
            } catch (e) {
                
                console.log(e);
			}
	}

	modalInfoEvent() {
		this.errorMessage = 'Ao clicar em um produto você automaticamente estará o comprando, e será debitado o valor do mesmo do seu saldo de eventos.';
		this.showModalPagamento = true;
		localStorage.setItem('MODAL_INFO', '1');
    }

    fechaModalPagamento() {
        this.showModalPagamento = false;
    }
    
    mostraModalPagamento(errorMessage : string) {
		this.errorMessage = errorMessage;
        this.showModalPagamento = true;
    }
	getWallet() {
        const wallet: any = new Balance();
        const self = this;
		this.getBalance(function(wallet) {
			self.balance = wallet; 
			localStorage.setItem('WALLET', JSON.stringify(wallet));
		});		
	}

    getBalance(callback=function(wallet: any) {}) {
		try {
            const self = this;
            const req = new Request();
            req.body = {
				'NRORG': localStorage.getItem('NRORG'),
				'TOKEN': localStorage.getItem('TOKEN'),
				'USER_ID': localStorage.getItem('USER_ID')
            };
            req.route = 'getBalance';
            req.callbackSuccess = function(response: any) { 
				callback(response.wallet);
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
			}

    }
	
	finishOrder(productId: number, productPrice: number) {
		try {
            const self = this;
            const req = new Request();
            req.body = {
				'EVENT_ID': localStorage.getItem('SELECTED_EVENT'),
				'USER_ID': localStorage.getItem('USER_ID'),
				'TOKEN': localStorage.getItem('TOKEN'),
				'NRORG': localStorage.getItem('NRORG'),
				'PAYMENT_METHOD': 'BALANCE',
				'ORDER_ITEMS': [{
					'quant': 1,
					'productId': productId
				}],
				'TOTAL_VALUE': productPrice
            };
            req.route = 'event/finishOrder';
            req.callbackSuccess = function(response: any) { 
				if (!!response) localStorage.setItem('ORDER', JSON.stringify(response));
				self.$router.push('QrcodeProduct');
            };
            req.callbackError = function(error: any) {
				if(error.response.data.errorCode == 102) self.mostraModalPagamento("Saldo insuficiente! Faça uma recarga para consumir produtos.");
                else {
					self.mostraModalPagamento("Erro desconhecido");
				}
            }
            req.blockTouchEvents = true;
            req.send();
            } catch (e) {
                
                console.log(e);
			}
	}

	chooseProduct(productId: number, productPrice: number) {
		this.categories.forEach(function(category: any) {
			const products = category.products;
			if (category.products) {
				category.products.forEach(function(product: any) {
					if (product.id == productId) {
						localStorage.setItem('PRODUCT', JSON.stringify(product));
					}
				});
			}
		});
		this.finishOrder(productId, productPrice);
	}
}