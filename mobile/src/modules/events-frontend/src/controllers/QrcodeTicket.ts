import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionTitle from '@/components/SectionTitle.vue';
import EventDetailImage from '@/components/EventDetailImage.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'


declare var M: any;
declare var $: any;
import QRCode from "qrcode";
@Component({
    components: { SectionTitle, EventDetailImage, DefaultButton, HeaderFluxo, HeaderCircleButton}
})

export default class QrcodeTicket extends Vue {
    ticketDetail: any = [];
    qrCodeImage: string = '';
    backButton: any = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        
        this.getTicket();
        // window.history.replaceState({}, '', '#/HomeApp');
    }

    getTicket() {
        let ticketDetail: any = JSON.parse(localStorage.getItem('TICKET_DETAIL') || '{}');
        this.ticketDetail = ticketDetail;
        const self = this;
        setTimeout(function() {
            self.montarQrCodeTicket(ticketDetail.id);
        }, 50);
    }

    montarQrCodeTicket(id: number) {
        var el:any = document.getElementById("qrcode") || {};
        el.innerHTML = "";
        // new QRCode(document.getElementById("qrcode"),  String(id));
        QRCode.toDataURL(String(id),{ errorCorrectionLevel: 'H', scale:6 }, (err, string) => {
            if (err) this.qrCodeImage = '';
            this.qrCodeImage = string;})
        this.$forceUpdate();
    }

    goBack() {
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'QrcodeTicket')
        this.$router.go(-1);
    }

}