import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '@/components/SectionArea.vue';
import TextLink from '@/components/TextLink.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import SelectField from '@/components/SelectField.vue';
import OptionField from '@/components/OptionField.vue';
import SelectFieldController from '@/ts/SelectFieldController';
import DatePicker from '@/components/DatePicker.vue';
import HeaderRelease from '@/components/HeaderRelease.vue';
import moment from 'moment';
import Request from '@/ts/Request';
import Util from '@/ts/Util';

declare var $: any;
@Component({
    components: { SectionArea, TextLink, InputText, DefaultButton, SelectField, OptionField, DatePicker, HeaderRelease }
})

export default class FilterEvents extends Vue {
    inputValues: any;
    reqType: string = 'login';
    structures: any = JSON.parse(localStorage.getItem('ALL_STRUCTURES') || '[]');
    categories: any = JSON.parse(localStorage.getItem('ALL_CATEGORIES') || '[]');
    tags:       any = JSON.parse(localStorage.getItem('ALL_TAGS') || '[]');
    dateValue: string = '';
    logoBig: any = localStorage.getItem('LOGO_BIG');

    eventDate: any = localStorage.getItem('SELECTED_DATE') || '' ? moment(localStorage.getItem('SELECTED_DATE') || '').lang('pt-br').format('DD[/]MM[/]YYYY') : '';
    eventCategoryId: any = localStorage.getItem('SELECTED_CATEGORY_ID')

    currentCategories: any = localStorage.getItem('CATEGORIES_FORMATTED');
    currentStructures: any = localStorage.getItem('SELECTED_STRUCTURES_FORMATTED');
    currentTags: any = localStorage.getItem('SELECTED_TAGS');

    categoria: any = JSON.parse(localStorage.getItem('CATEGORIES_FORMATTED') || '[]');
    estrutura: any = JSON.parse(localStorage.getItem('SELECTED_STRUCTURES_FORMATTED') || '[]');
    tag: any = $('#select-tag :selected').val() != '';
    mes: any = $('#month :selected').val();
    anos: any = $('#years :selected').val() != '';
    months: object = [];

    currentMonth: any = moment().month();
    currentYear: any = moment().year();
    years: any = [];

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        // this.getMonths();
      
        this.getStructures();
        this.getCategories();
        this.getTags();
        // this.fillStructures();
        // this.fillCategories();
        // this.fillTags();
        // this.fillMonths();
        this.addYearOnArray();

        if (this.eventDate) {
            $('#date').val(this.eventDate);
            $('#l-date').addClass('active');
        }

        this.validateFilterCategory();
    }

    // getMonths(){
    //     this.months = [
    //         { id: '0' , name: 'Janeiro' },
    //         { id: '1' , name: 'Fevereiro' },
    //         { id: '2' , name: 'Março' },
    //         { id: '3' , name: 'Abril' },
    //         { id: '4' , name: 'Maio' },
    //         { id: '5' , name: 'Junho' },
    //         { id: '6' , name: 'Julho' },
    //         { id: '7' , name: 'Agosto' },
    //         { id: '8' , name: 'Setembro' },
    //         { id: '9' , name: 'Outubro' },
    //         { id: '10', name: 'Novembro' },
    //         { id: '11' , name: 'Dezembro' }
    //     ];
    //     SelectFieldController.refresh();
    //     // this.fillStructures();
    // }

    goBack(){
        this.$router.go(-1);
    }

    testei(){
        return Object.values(this.currentStructures || {}).length;
    }

    setValue(data: any){
        this.inputValues[data.name] = data.value;
    }

    addYearOnArray(){
        for(let year = 2018; year <= moment().year(); year++)
            this.years.push({'id': year});
        
        setTimeout(function() {
            SelectFieldController.refresh();
        }, 50);
    }

    // formatDateToMoment(month: any, year: any) {
    formatDateToMoment(month: any) {
        // return moment(moment().month(month).year(year)).format('YYYY-MM');
        return moment(moment().month(month)).format('YYYY-MM');
    }

    buttonAvancar() {
        localStorage.setItem('SELECTED_TAGS', '');
        localStorage.setItem('SELECTED_DATE', '');
        localStorage.setItem('SELECTED_STRUCTURES', '');
        localStorage.setItem('CATEGORIES', '');
        localStorage.setItem('SELECTED_STRUCTURES_FORMATTED', '');
        localStorage.setItem('CATEGORIES_FORMATTED', '');

        if(!!this.estrutura) {
            let structures:any = [];
            this.estrutura.forEach((strut:any) => structures.push({'id': strut}))
            console.log(structures);
            localStorage.setItem('SELECTED_STRUCTURES_FORMATTED', this.estrutura.length > 0 ? JSON.stringify(this.estrutura) : '');
            localStorage.setItem('SELECTED_STRUCTURES', structures.length > 0 ? JSON.stringify(structures) : '');
        }
        if(!!this.categoria) {
            let categories:any = [];
            this.categoria.forEach((categoryId:number) => categories.push({'id': categoryId}))
            localStorage.setItem('CATEGORIES_FORMATTED', this.categoria.length > 0 ? JSON.stringify(this.categoria) : '');
            localStorage.setItem('CATEGORIES', categories.length > 0 ? JSON.stringify(categories) : '');
        }
        // if($('#select-tag').val()) {
        //     let tags:any = [];
        //     $('#select-tag').val().forEach((tagId:number) => tags.push({'id': tagId}))
        //     localStorage.setItem('SELECTED_TAGS', tags.length > 0 ? JSON.stringify(tags) : '');
        // }
        // console.log(this.categoria);
        this.$router.push({ name: 'Events' });
    }

    verifyIfCategoryIsChecked( id:any ){
        let el : any = $('#category'+id)
        let label : any = $('#labelCategory'+id)
        el.prop('checked') ? label.addClass('optionChecked') : label.removeClass('optionChecked');
    }

    verifyIfStructureIsChecked( id:any ){
        let el :any = $('#structure'+id)
        let label :any = $('#labelStructure'+id)
        el.prop('checked') ? label.addClass('optionChecked') : label.removeClass('optionChecked');
    }

    getStructures() { 
        try {
        const self = this;
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'STRUCTURE_LEVEL': 0
        };
         req.route = 'getStructures';
        req.callbackSuccess = function(response: any) { 
            let structures = response.structures;
            self.structures = structures;
            localStorage.setItem('ALL_STRUCTURES', JSON.stringify(response.structures));
            // self.fillStructures();
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        }
        req.blockTouchEvents = false;
        req.send();
        } catch (e) {
            
            console.log(e);
        }
    }

    // fillStructures() {
    //     const self = this;
    //     setTimeout(function() {
    //         if (self.currentStructures) {
    //             let structures: any = [];
    //             JSON.parse(self.currentStructures).forEach((struct: any) => structures.push(struct.id + ''));
    //             $('#select-structure').val(structures);
    //             $('#select-structure').formSelect();
    //         }
    //         SelectFieldController.refresh();
    //     }, 50);
    // }

    // fillMonths() {
    //     const self = this;
    //     setTimeout(function() {
    //         if (self.currentMonth) {
    //             $('#month').val(self.months);
    //             $('#month').formSelect();
    //         }
    //     }, 50);
    //     SelectFieldController.refresh();
    // }

    getCategories() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN')
            };
             req.route = 'getCategories';
            req.callbackSuccess = function(response: any) { 
                let categories = response.categories;
                self.categories = categories;
                SelectFieldController.refresh();
                localStorage.setItem('ALL_CATEGORIES', JSON.stringify(response.categories));
                // self.fillCategories();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    // fillCategories() {
    //     const self = this;
    //     setTimeout(function() {
    //         if (self.currentCategories) {
    //             let categories: any = [];
    //             JSON.parse(self.currentCategories).forEach((cat: any) => categories.push(cat.id + ''));
    //             $('#select-category').val(categories);
    //             $('#select-category').formSelect();
    //         }
    //         SelectFieldController.refresh();
    //     }, 50);
    // }

    getTags() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'TOKEN': localStorage.getItem('TOKEN')
            };
             req.route = 'getTagsFromOrganization';
            req.callbackSuccess = function(response: any) { 
                self.tags = response.tags;
                localStorage.setItem('ALL_TAGS', JSON.stringify(response.tags));
                // self.fillTags();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }

        let body: object = {
            'NRORG': localStorage.getItem('NRORG'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'TOKEN': localStorage.getItem('TOKEN')
        }
    }

    // fillTags() {
    //     const self = this;
    //     setTimeout(function() {
    //         if (self.currentTags) {
    //             let tags: any = [];
    //             JSON.parse(self.currentTags).forEach((tag: any) => tags.push(tag.id + ''));
    //             $('#select-tag').val(tags);
    //             $('#select-tag').formSelect();
    //         }
    //         SelectFieldController.refresh();
    //     }, 50);
    // }

    limparFiltros() {
        localStorage.setItem('SELECTED_DATE', '');
        localStorage.setItem('SELECTED_STRUCTURES', '');
        localStorage.setItem('CATEGORIES', '');
        localStorage.setItem('SELECTED_STRUCTURES_FORMATTED', '');
        localStorage.setItem('CATEGORIES_FORMATTED', '');
        localStorage.setItem('SELECTED_TAGS', '');

        this.categoria = [];
        this.estrutura = [];

        $('label').removeClass('optionChecked');
        $('input').prop('checked', false);
    }

    validateFilterCategory(){
        if(!!this.categoria){
            this.categoria.forEach(( cat:any ) => {
                let obj = this.categories.find(( cate:any ) => cate.ID === cat);
                if(!!obj) {
                    var el = $('#category'+obj.ID);
                    el.prop('checked', true);
                    var label = $('#labelCategory'+obj.ID);
                    label.addClass('optionChecked');
                }
            });
        }

        if(!!this.estrutura){
            this.estrutura.forEach(( est:any ) => {
                let obj = this.structures.find(( str:any ) => str.id === est);
                if(!!obj) {
                    var el = $('#structure'+obj.id);
                    el.prop('checked', true);
                    var label = $('#labelStructure'+obj.id);
                    label.addClass('optionChecked');
                }
            });
        }
    }

}
declare var M: any;