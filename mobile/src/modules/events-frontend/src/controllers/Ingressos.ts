import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionArea from '@/components/SectionArea.vue';
import TextLink from '@/components/TextLink.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import TabsHome from '@/components/TabsHome.vue';
import FullTicket from '@/components/FullTicket.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SelectField from '@/components/SelectField.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import ListDefault from '@/components/ListDefault.vue' ;   
import OptionCard from '@/components/OptionCard.vue';
import SectionTitle from '@/components/SectionTitle.vue';
import EmptyState from '@/components/EmptyState.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';


declare var $: any;
@Component({
    components: { SectionArea, TextLink, InputText, DefaultButton, TabsHome, HeaderFluxo, SelectField, ModalCodigo, OptionCard, ListDefault, SectionTitle, FullTicket, EmptyState, HeaderCircleButton}
})

export default class Ingressos extends Vue {
    
    userData:     any = localStorage.getItem('USER_DATA') ? JSON.parse(localStorage.getItem('USER_DATA') || '{}') : {};
    testeClass : any = true;
    profileImage: string = localStorage.getItem('USER_DATA') ? this.userData.IMAGE :'';
    fullName:     string = localStorage.getItem('USER_DATA') ? this.userData.FIRST_NAME + ' ' + this.userData.LAST_NAME : '';
    npf:          string = localStorage.getItem('USER_DATA') ? this.userData.ORGANIZATION_DATA.EXTERNAL_ID : '';
    ticketsValidos:number = JSON.parse(localStorage.getItem('TICKETS_VALIDOS') || '0');
    timeouts:      any = [];
    ticketsArePrepared: boolean = false;
    madeRequest:   boolean = false;
    userTickets:   any = JSON.parse(localStorage.getItem('USER_TICKETS') || '[]');
    abas:          any = [
                    { id: 1, NAME: 'Carteirinha'},
                    { id: 2, NAME: 'Ingressos'}
                ];
    appHasTickets : any = localStorage.getItem('APP_HAS_TICKETS') || false;    
    backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack }

    mounted() {
        this.appHasTickets = false;
        Util.setBackButtonBehavior(this.goBack);
        this.getUserTicketsByOrg();
        this.ticketsValidos = this.userTickets.length;
        moment.updateLocale('pt', {
            monthsShort : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            months : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            weekdays : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
            weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
        });
    }

    beforeDestroy() {
        if(this.timeouts){
            for (let i = 0; i < this.timeouts.length; i++) {
                clearTimeout(this.timeouts[i]);
            }
        }
        this.timeouts = [];
        Util.removeBackButtonBehavior();
    }

    getUserTicketsByOrg() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "NRORG": localStorage.getItem('NRORG')
            };
            req.route = 'getUserTicketsByOrg';
            req.callbackSuccess = function(response: any) {

                // let appHass : any;
                // if (response.appHass) appHass = response.appHass
                // else appHass = false;
                // localStorage.setItem('APP_HAS_TICKETS', appHass);

                self.userTickets = response.userTickets;
                localStorage.setItem('USER_TICKETS', JSON.stringify(self.userTickets));
                self.ticketsValidos = self.userTickets.length;
                self.userTickets.forEach((ticket:any) => {
                    if(ticket.userName != self.fullName){
                        const el = '#'+ticket.id
                        $(el).addClass('dependentTicket');
                    }
                });
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    formatData(ID: any){
        let format: any;
        format = moment(this.userTickets.find((userTicket: any) => userTicket.id == ID).initialDate.date).lang('pt-br').format("DD [de] MMMM, HH[h]mm");
        return format;
    }

    setTicketDetails(ID: any){
        localStorage.setItem('TICKET_DETAIL', JSON.stringify(this.userTickets.find((ticket: any) => ticket.id == ID)));
        this.$router.push({ name: "QrcodeTicket" });
    }
    
    
    goBack() {
        // Util.removeBackButtonBehavior();
        this.$router.go(-1)
    }

    goToEvents(){
        localStorage.setItem('EVENTS', '[]');
        this.$router.push({ name: "Events" });
    }

}