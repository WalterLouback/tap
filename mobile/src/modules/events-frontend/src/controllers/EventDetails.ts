import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionTitle from '../components/SectionTitle.vue';
import EventDetailImage from '../components/EventDetailImage.vue';
import TextLink from '../components/TextLink.vue';
import InputText from '../components/InputText.vue';
import DefaultButton from '../components/DefaultButton.vue';
import SelectField from '../components/SelectField.vue';
import OptionField from '../components/OptionField.vue';
import InvisibleHeader from '../components/InvisibleHeader.vue'
import EmptyState from '../components/EmptyState.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import Request from '@/ts/Request';
import Util from '@/ts/Util';
// import Orders, { StoresConfig, RecentOrdersConfig } from "../../../../teknisa-modules/ordersModule/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import SCLoadingDots from '@/modules/shared-components/SCLoadingDots.vue'
import Minicard from '@/modules/shared-components/Minicard.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import EventsInitializer from '../../EventsInitializer'

declare var M: any;
declare var $: any;
declare let cordova: any;

@Component({
    components: { SCLoadingDots, SectionTitle, EventDetailImage, TextLink, InputText, DefaultButton, SelectField, OptionField, InvisibleHeader, HeaderCircleButton, HeaderBackground, Minicard, LoginModal, EmptyState, ModalCodigo}
})

export default class EventDetails extends Vue {
    inputValues: any;
    structures: any = [];
    events: any = [];
    eventDetail: any = (localStorage.getItem('EVENT') && localStorage.getItem('EVENT')!='undefined')? JSON.parse(localStorage.getItem('EVENT') || '') : '';
    ticketsEvent: any = localStorage.getItem('TICKETS_EVENTS') ? JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [] : [];
    nome: any = [];
    cheapestTicket: any = {};
    isFavorited: boolean = (this.eventDetail && this.eventDetail.hasOwnProperty('favoriteId')) ? Boolean(this.eventDetail.favoriteId): false;
    componentKey: number = 0;
    backButton: any = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}
    otherButton: any = EventsInitializer.share ? [{iconClass: (this.isFavorited) ? 'fas': 'fal', icon: 'heart', colorClass: (this.isFavorited) ? 'font-red':'default', callback: this.toggleFavorite},{iconClass: 'fal', icon: 'share-alt', colorClass: 'default', callback: this.openWebShare}]: [{iconClass: (this.isFavorited) ? 'fas': 'fal', icon: 'heart', colorClass: (this.isFavorited) ? 'font-red':'default', callback: this.toggleFavorite}];
    parentEvent: any = (localStorage.getItem('PARENT_EVENT')) ? JSON.parse(localStorage.getItem('EVENT_DETAILS_CACHE')||'[]') : [];
    children: any = [];
    schedule: any = [];
    breadCrumbs: any = [];
    sharedEventParent: boolean = true;
    loadedMenuData: boolean = false; 
    favoritedKey: number = 123;
    eventDays: any = [];
    reloadingData: boolean = false;
    failedToLoadData: boolean = false;
    loadingEventFromCache: boolean = false;
    showModalLogin: boolean = false;
    syncRequest: boolean = false; 
    eventSalesMethod: any = localStorage.getItem('EVENT_SALES_METHOD'); 
    // aboutOverflow: boolean = false;
    // aboutExpand: boolean = false;

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        if(this.eventDetail){
            if(!this.eventDetail.hasOwnProperty('favoriteId')){
                this.isFavorite(this.eventDetail.id);
            }
            else this.updateFavoriteList(localStorage.getItem('SELECTED_EVENT'));
            this.getEvent();
            this.findCheapestTicket();
            this.verifySharedEventParent();
            this.getEventData(this.eventDetail.id);
            this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
        }
        else{
            this.updateFavoriteList(localStorage.getItem('SELECTED_EVENT'));
            this.startEvent(localStorage.getItem('SELECTED_EVENT'));
        }
        var el:any = this.$refs.cardAbout; 
        setTimeout(()=>{
            // if (el.scrollHeight > el.clientHeight) this.aboutOverflow = true;
        }, 10)
        
    }

    formatDate(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('DD [de] MMM [às] HH:mm');
    }

    getTicketByEvent() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.eventDetail.id
            };
            req.route = 'getTicketByEvent';
            req.callbackSuccess = function(response: any) { 
                if(!self.loadingEventFromCache){
                    self.ticketsEvent = response.eventTickets;
                    self.findCheapestTicket();
                    const allTicketsEvents = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]');
                    allTicketsEvents[self.eventDetail.id] = response.eventTickets;
                    localStorage.setItem('TICKETS_EVENTS', JSON.stringify(allTicketsEvents));
                }
            };
            req.callbackError = function(error: any) {
                console.log(error);
                self.failedToLoadData = true;
            }
            // req.callbackTimeout = (error: any)=>{
            //     self.failedToLoadData = true;
            // }
            req.blockTouchEvents = false;
            req.showLoader = false;
            req.send();
            } catch (e) {
                console.log(e);
        }
    }

    toggleFavorite() {
        if(!localStorage.getItem('USER_ID')){
            this.openModalLogin();
        }
        else{
            const isFavorited = this.isFavorited;
            if (!isFavorited) {
                this.isFavorited = true;
                this.otherButton[0].iconClass ='fas';
                this.otherButton[0].colorClass='font-red';
                this.favoritedKey = this.favoritedKey + 1;
                this.syncRequest = true;
                this.favoriteEvent();
            } else {
                this.isFavorited = false;
                this.otherButton[0].iconClass = 'fal';
                this.otherButton[0].colorClass= 'default';
                this.favoritedKey = this.favoritedKey + 1;
                this.unfavoriteEvent();
            }
        }
    }

    favoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'createFavoriteEvent';
        req.callbackSuccess = (response: any) => { 
            self.updateFavoriteList();
            self.isFavorited = true;
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();

        const events = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        const event = events.find((event: any) => event.id === this.eventDetail.id);
        if (event) event.favoriteId = 9999;
        localStorage.setItem('EVENTS', JSON.stringify(events));
    }

    unfavoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'deleteFavoriteEvent';
        req.callbackSuccess = (response: any) => { 
            self.updateFavoriteList();
            self.isFavorited = false;
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();
        
        const events = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        const event = events.find((event: any) => event.id === this.eventDetail.id);
        if (event) event.favoriteId = null;
        localStorage.setItem('EVENTS', JSON.stringify(events));
    }

    updateFavoriteList(id: any = '0'){
        try{
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'favorite';
            req.callbackSuccess = (response:any) => {
                localStorage.setItem('FAVORITE_EVENTS_AND_STORES',JSON.stringify(response.calendar.filter((obj: any)=> !!obj.favoriteId)));
                if(this.eventDetail) this.isFavorite(this.eventDetail.id);
                else this.isFavorite(id);
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            req.showLoader = false;
            if(localStorage.getItem('USER_ID')) req.send();
        }catch (e){
            console.log(e)
        }
    }

    loadEvents() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "DATE": localStorage.getItem('SELECTED_DATE'),
                "STRUCTURES": localStorage.getItem('SELECTED_STRUCTURES') || '[]',
                "CATEGORIES": localStorage.getItem('CATEGORIES') || '[]',
                "TAGS": localStorage.getItem('SELECTED_TAGS') || '[]',
                "USER_ID": localStorage.getItem('USER_ID')
            };
            req.route = 'getEventsByOrg';
            req.callbackSuccess = function(response: any) { 
                let events = response.events;
                self.events = events;
                localStorage.setItem('EVENTS', JSON.stringify(response.events));
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.showLoader = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    goToSecondProductMenu(){
        localStorage.setItem('SELECTED_EVENT', JSON.stringify(this.eventDetail.id));
        localStorage.setItem('EVENT_NAME_QRCODE', JSON.stringify(this.eventDetail.name));
        this.$router.push({ name: 'GetProduct' });
    }

    goToProductMenu(){
        this.$router.push({ name: 'ProductMenu' });
    }

    goToBuyEventTicket(){
        localStorage.setItem('EVENT', JSON.stringify(this.eventDetail));
        this.$router.push('/BuyEventTicket');
    }

    getEvent() {
       this.events = JSON.parse(localStorage.getItem('EVENTS') || '{}');
       this.eventDetail = JSON.parse(localStorage.getItem('EVENT') || '{}');
    }

    findCheapestTicket() {
        if (this.ticketsEvent.length > 0) {
            this.cheapestTicket = this.ticketsEvent.reduce((a: any, b: any) => { 
                if (a && b) return a.price < b.price ? a : b;
                else return a;
        });
        } else return {};
    }

    goBack() {
        if(this.parentEvent.length && localStorage.getItem('EVENT_SCHEDULE_CACHE')){
            var scheduleList = JSON.parse(localStorage.getItem('EVENT_SCHEDULE_CACHE')||'[]');
            var schedule = scheduleList.pop();
            localStorage.setItem('EVENT_SCHEDULE', JSON.stringify(schedule));
            localStorage.setItem('EVENT_SCHEDULE_CACHE', JSON.stringify(scheduleList)); 
            this.$router.go(-1);
        }
        else if(localStorage.getItem('EVENT_DETAILS_CACHE')){
            this.failedToLoadData = false;
            this.loadingEventFromCache = true;
            this.loadCachedEventData()
        }
        else{
            this.$router.go(-1);
        }
        
    }

    isFavorite(id: any){
        if (!!localStorage.getItem('FAVORITE_EVENTS_AND_STORES')){
            var favorites = JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES')||'{}');
            // return !!JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES')||'{}').find((e:any)=>e.id == this.store.id);
            this.isFavorited = !!favorites.find((ev:any)=>ev.id == id);
        }
        else this.isFavorited = false;
        
        this.otherButton[0].iconClass = (this.isFavorited) ? 'fas': 'fal';
        this.otherButton[0].colorClass= (this.isFavorited) ? 'font-red':'default';
        this.favoritedKey = this.favoritedKey + 1;
    }

    goToEventDetails(){
        this.$router.push('/EventDetails');
    }

    goToStore() {
        Util.removeBackButtonBehavior();
        // Orders.goToStores(new StoresConfig(this.goToEventDetails, this.eventDetail.id));
        localStorage.setItem('ENTERED_ORDERS_FROM', 'EventDetails');
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', '');
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        localStorage.setItem('ENTERED_REGISTERED_CARDS', '');
        if(this.eventDetail.ordMenuId  || this.eventDetail.menuId ||this.eventDetail.menus){
            localStorage.setItem('IS_AN_EVENT','t');
            localStorage.setItem('EVENT', JSON.stringify(this.eventDetail));
            localStorage.setItem('STORE_ID', this.eventDetail.id);
            localStorage.setItem('storeId', this.eventDetail.id);
        }
        else{
            localStorage.setItem('IS_AN_EVENT','');
            localStorage.setItem('STORE', JSON.stringify(this.children[0]));
            localStorage.setItem('STORE_ID', this.children[0].id);
            localStorage.setItem('storeId', this.children[0].id);
        }
        this.$router.push('Produtos');
    }

    goToEventStores(){
        localStorage.setItem('IS_AN_EVENT','t');
        this.parentEvent.push(this.eventDetail);
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent))
        this.$router.push('/EventStores')
    }

    goToEventSchedule(){
        localStorage.setItem('IS_AN_EVENT','t');
        this.parentEvent.push(this.eventDetail);
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
        this.$router.push('/EventSchedule')
    }

    goToSharedStore(id:string){
        if (id == this.eventDetail.id) localStorage.setItem('IS_AN_EVENT', 't');
        else localStorage.setItem('IS_AN_EVENT', '');
        Util.removeBackButtonBehavior();
        localStorage.setItem('SHARE_ID', '')
        localStorage.setItem('SHARE_TYPE', '')
        Util.removeBackButtonBehavior();
        // Orders.goToStores(new StoresConfig(this.goToEventDetails, id));

        localStorage.setItem('ENTERED_ORDERS_FROM', 'EventStores');
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', '');
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        localStorage.setItem('ENTERED_REGISTERED_CARDS', '');
        localStorage.setItem('STORE_ID', id);
        localStorage.setItem('storeId', id);
        this.$router.push('Produtos');
    }

    goToSharedEvent(id: string, last: boolean){
        localStorage.setItem('IS_AN_EVENT','t');
        if (last){
            localStorage.setItem('SHARE_ID', '')
            localStorage.setItem('SHARE_TYPE', '')
        }
        else{
            this.parentEvent.push(this.eventDetail);
            localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
            this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
        }
        this.getEventData(id);
    }

    verifySharedEventParent(){
        this.sharedEventParent = !!localStorage.getItem('SHARE_PARENT_ID');
    }

    openWebShare(){
        const w: any = window;
        var idType: string = '&et=e';
        var parentEvent: string = this.parentEvent.map((obj:any)=>{return obj.id}).join(',');
        var id: string = (parentEvent.length) ? 'p=' + parentEvent +'&id=' + this.eventDetail.id:'id=' + this.eventDetail.id;
        var suggestionType: string = 'deste evento';         
        // w.plugins.socialsharing.share(EventsInitializer.shareMessage + ' ' + EventsInitializer.shareUrl + id + idType);
        console.log(EventsInitializer.shareMessage + ' ' + EventsInitializer.shareUrl + id + idType);
    }

    getStoreData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "EVENT_ID": this.eventDetail.id
            };
            req.route = 'getEventChildren';
            req.callbackSuccess = (response: any) => {
                // localStorage.setItem('PARENT_EVENT', JSON.stringify(response.store))
                // localStorage.setItem('EVENT_STORES', JSON.stringify(response.store.children));
                //-----TEMP para teste
                // var temp = (localStorage.getItem('STORES')) ? JSON.parse(localStorage.getItem('STORES')||'[]') : [];
                // this.children = temp.filter((obj:any)=>obj.type=='S');
                // this.schedule = temp.filter((obj:any)=>obj.type=='E');

                //-----Correto
                if (!this.loadingEventFromCache){
                    this.children = response.children.filter((obj:any)=>obj.type=='S' && obj.ordMenuId);
                    this.schedule = response.children.filter((obj:any)=>obj.type=='E' && moment(obj.finalDate.date).isAfter(moment()));
                    // this.parentEvent = response.store.schedule;
                    localStorage.setItem('EVENT_STORES', JSON.stringify(this.children));
                    localStorage.setItem('EVENT_SCHEDULE', JSON.stringify(this.schedule));
                    if(localStorage.getItem('SHARE_TYPE')) this.sendToSharedEvent()
                    else this.loadedMenuData = true;
                    this.getEventDays()
                    this.$forceUpdate();
                }
                //
                
                //setTimeout(self.addEventListeners, 1);
            };
            req.callbackError = function(error: any) {
                console.log(error);
                self.failedToLoadData = true;
            }
            // req.callbackTimeout = (error: any)=>{
            //     self.failedToLoadData = true;
            // }
            req.blockTouchEvents = false;
            req.showLoader = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    

    startEvent(id: any) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "EVENT_ID": id
            };
            req.route = 'getEventData';
            req.callbackSuccess = (response: any) => {
                this.reloadingData = false;
                if(!this.loadingEventFromCache){
                    this.eventDetail = response.event;
                    this.verifySharedEventParent()
                    this.ticketsEvent = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
                    localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent))
                    localStorage.setItem('EVENT', JSON.stringify(this.eventDetail));
                    this.getTicketByEvent();
                    this.getStoreData();
                    this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
                    // this.updateFavoriteList();
                }
            };
            req.callbackError = function(error: any) {
                console.log(error);
                self.failedToLoadData = true;
            }
            // req.callbackTimeout = (error: any)=>{
            //     self.failedToLoadData = true;
            // }
            req.blockTouchEvents = false;
            req.showLoader = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    


    getEventData(id: any) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "EVENT_ID": id
            };
            req.route = 'getEventData';
            req.callbackSuccess = (response: any) => {
                this.reloadingData = false;
                if(!this.loadingEventFromCache){
                    this.eventDetail = response.event;
                    this.verifySharedEventParent()
                    this.ticketsEvent = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
                    localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent))
                    localStorage.setItem('EVENT', JSON.stringify(this.eventDetail));
                    this.getTicketByEvent();
                    this.getStoreData();
                    // this.updateFavoriteList();
                }
            };
            req.callbackError = function(error: any) {
                console.log(error);
                self.failedToLoadData = true;
            }
            // req.callbackTimeout = (error: any)=>{
            //     self.failedToLoadData = true;
            // }
            req.blockTouchEvents = false;
            req.showLoader = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    

    sendToSharedEvent(){
        
        if (localStorage.getItem('SHARE_PARENT_ID')) {
            var parentEventList:any = localStorage.getItem('SHARE_PARENT_ID')!.split(',');
            parentEventList.shift();
            localStorage.setItem('SHARE_PARENT_ID', parentEventList.join(','));
        }
        else{
            var parentEventList:any = [];
        } 

        if (parentEventList.length){
            var parentEvent = parentEventList[0];
            this.goToSharedEvent(parentEvent||'', false);
        }
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='e') {
            this.goToSharedEvent(localStorage.getItem('SHARE_ID')!, true);
        }
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='s') {
            this.goToSharedStore(localStorage.getItem('SHARE_ID')!);
        }
        
    }

    getEventDays(){
        var uniqueEventDays = new Set(this.schedule.map((obj:any)=>{return moment(obj.initialDate.date).format('DD/MM')}));
        this.eventDays = [...uniqueEventDays];
    }

    eventsWithDate(day: string){
        return this.schedule.filter((obj:any)=>moment(obj.initialDate.date).format('DD/MM')==day);
    }

    getEventStartTime(time: string){
        return moment(time).format('DD/MM HH:mm');
    }
    
    reloadEventData(id: any){
        this.reloadingData = true;
        this.loadedMenuData = false;
        this.loadingEventFromCache = false;
        this.pushEventToCache();
        this.isFavorite(id);
        this.getEventData(id);
    }

    loadCachedEventData(){
        this.reloadingData = true;
        this.loadedMenuData = false;
        if (localStorage.getItem('EVENT_DETAILS_CACHE')){  
            this.popEventFromCache();
            setTimeout((()=>{
                this.reloadingData = false;
                this.loadedMenuData = true;
            }), 20);
        };
    }

    pushEventToCache(){
        if (!localStorage.getItem('EVENT_DETAILS_CACHE'))
            localStorage.setItem('EVENT_DETAILS_CACHE', JSON.stringify([{eventDetail: this.eventDetail, children: this.children, schedule: this.schedule, ticketsEvent: this.ticketsEvent}]));
        else {
            var cache = JSON.parse(localStorage.getItem('EVENT_DETAILS_CACHE')||'[]');
            cache.push({eventDetail: this.eventDetail, children: this.children, schedule: this.schedule, ticketsEvent: this.ticketsEvent});
            localStorage.setItem('EVENT_DETAILS_CACHE', JSON.stringify(cache));
        }
        this.parentEvent.push(this.eventDetail);
        this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));

    }

    popEventFromCache(){
        var cache = JSON.parse(localStorage.getItem('EVENT_DETAILS_CACHE')||'[]');
        var previousEventData = cache.pop();
        
        this.eventDetail = previousEventData.eventDetail; 
        this.children = previousEventData.children;
        this.schedule = previousEventData.schedule;
        this.ticketsEvent = previousEventData.ticketsEvent;
        localStorage.setItem('EVENT_STORES', JSON.stringify(this.children));
        localStorage.setItem('EVENT_SCHEDULE', JSON.stringify(this.schedule));
        
        if (cache.length) localStorage.setItem('EVENT_DETAILS_CACHE', JSON.stringify(cache));
        else localStorage.setItem('EVENT_DETAILS_CACHE', '');

        this.parentEvent.pop();
        this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
        this.isFavorite(this.eventDetail.id);
    }

    openModalLogin(){
        this.showModalLogin = true;
    }
    closeModalLogin() {
        this.showModalLogin = false;
    }

    closeModalSync(){
        this.syncRequest = false;
    }

    callSyncCalendar(){
        const w: any = window;
        this.syncRequest = false;
        w.plugins.calendar.createEventInteractively(this.eventDetail.name , this.eventDetail.location, this.eventDetail.about, new Date(this.eventDetail.initialDate.date), new Date(this.eventDetail.finalDate.date), ()=>{}, ()=>{});
    }

    goToLerQrMesa(){
        localStorage.setItem('DELIVER_AT', 'T');
        localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
        this.$router.push('LerQrMesa');
    }
}