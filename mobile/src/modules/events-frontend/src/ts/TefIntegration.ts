import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
// import { format } from 'util';
// import { timingSafeEqual } from 'crypto';

declare var cordova: any;

@Component
export default class TefIntegration extends Vue {
    
    onTransactionMessage: any = () => {};
    onMenuTitle: any = () => {};
    onPromptCommand: any = () => {};
    onPromptBoolean: any = () => {};
    private static instance = new TefIntegration();

    public static TEXT_ALIGN_LEFT   : String = 'LEFT';
    public static TEXT_ALIGN_CENTER : String = 'CENTER';
    public static TEXT_ALIGN_RIGHT  : String = 'RIGHT';

    public static FONT_WEIGHT_NORMAL: String = 'NORMAL';
    public static FONT_WEIGHT_BOLD  : String = 'BOLD';

    public static PAYMENT_DEBIT     : number = 2;
    public static PAYMENT_CREDIT    : number = 3;
    public static PAYMENT_VOUCHER   : number = 627;

    static getInstance() {
        return TefIntegration.instance;
    }

    init(activationCode: string):Promise<any> {
        return new Promise((resolve: Function, reject: Function) => {
            const w:any = window;
            w.onReturnValue = (message: string, error: number) => {
                if (error) reject(message);
                else resolve(message);
            };
            this.callPlugin([JSON.stringify({'activationCode': activationCode})], 'init', (message: String) => { }, (message: String) => { });
        });
    }

    execPayment(paymentType: number, paymentValue: String, paymentInvoice: String, installmentsNumber = 1): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            const w:any = window;

            w.onTransactionMessage = this.onTransactionMessage;
            w.promptCommand        = this.onPromptCommand;
            w.promptBoolean        = this.onPromptBoolean;
            w.setMenuTitle         = this.onMenuTitle;
            w.onReturnValue        = (message: String, statusCode: number, documentId: String, cardNumber: String, cardBrand: String, authorizationCode: String) => {
                if (statusCode == 0) resolve({message, documentId, cardNumber, cardBrand, authorizationCode});
                else reject(message);
            };

            var args = {
                paymentType: paymentType,
                paymentValue: paymentValue,
                paymentInvoice: paymentInvoice,
                paymentIp: localStorage.getItem('PAYMENT_IP') || '10.2.2.110',
                paymentStore: '00000000',
                paymentTerminal: '00000000',
                paymentDate: this.formattedCurrentDate(),
                paymentHour: this.formattedCurrentHour(),
                paymentOperator: '',
                storeCnpj: '33475775000172',
                automationCnpj: '26269316000177',
                installmentsNumber: installmentsNumber,
                tlsType: localStorage.getItem('TLS_TYPE') || ''
            };

            this.callPlugin([JSON.stringify(args)], 'payment', (r: any) => { alert(r)}, (e: any) => { alert(e) });
        });
    }

    formattedCurrentDate() {
        return moment().format('YYYYMMDD');
    }

    formattedCurrentHour() {
        return moment().format('HHmm');
    }

    printQrCode(seed: string):Promise<any> {
        return new Promise((resolve: Function, reject: Function) => {
            const w:any = window;
            w.onReturnValue = resolve;
            this.callPlugin([JSON.stringify({'seed': seed})], 'printQrCode', (message: String) => { }, (message: String) => { });
        });
    }

    printImage(base64: string):Promise<any> {
        return new Promise((resolve: Function, reject: Function) => {
            const w:any = window;
            w.onReturnValue = (message: string, error: number) => {
                if (error) reject(message);
                else resolve(message);
            };
            this.callPlugin([JSON.stringify({'image': base64})], 'printImage', (message: String) => { }, (message: String) => { });
        });
    }

    printBarCode(seed: string, isLast: boolean):Promise<any> {
        return new Promise((resolve: Function, reject: Function) => {
            const w:any = window;
            w.onReturnValue = resolve;
            this.callPlugin([JSON.stringify({'seed': seed, 'isLast': isLast ? 'true' : 'false'})], 'printBarCode', (message: String) => { }, (message: String) => { });
        });
    }

    printMultipleTexts(printQueue: any[], originalResolve?: any): Promise<any> {
        console.log(printQueue);
        const self = this;
        const pq   = JSON.parse(JSON.stringify(printQueue));

        return new Promise((resolve: any, reject: any) => {
            if (pq.length === 0) {
                originalResolve ? originalResolve() : resolve();
                return;
            }
            self.printText(pq[0]).then((e: string) => {
                if (e) {
                    originalResolve ? originalResolve(e) : resolve(e);
                    return ;
                }
                pq.splice(0, 1);
                self.printMultipleTexts(pq, originalResolve || resolve);
            }).catch((e) => {
                console.error(e);
                reject(e);
            });
        });
    }

    printText(item: any): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            const w: any    = window;
            w.onReturnValue = resolve;
            try {
                this.callPlugin([JSON.stringify(item)], 'printText', (message: string) => { alert(message) }, reject);
            } catch (e) { reject(e); }
        });
    }

    printCoupon(): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            const w: any    = window;
            w.onReturnValue = resolve;
            this.callPlugin([JSON.stringify({})], 'printCoupon', (message: string) => { }, (message: string) => { });
        });
    }

    abortTransaction() {
        this.callPlugin([JSON.stringify({})], 'abort', (message: String) => { alert(message); }, (message: String) => { alert(message); });
    }

    callPlugin(args: String[], action: String, success: Function, error: Function) {
        cordova.exec(success, error, 'PosStrategy', action, args);
    }

    printLastCoupon() {
        
    }

    resetListeners() {
        const w: any = window;
        this.onTransactionMessage = () => {};
        this.onPromptCommand      = () => {};
        this.onPromptBoolean      = () => {};
        this.onMenuTitle          = () => {};

        w.onTransactionMessage    = () => {};
        w.promptCommand           = () => {};
        w.promptBoolean           = () => {};
        w.setMenuTitle            = () => {};
        w.onReturnValue           = () => {};
    }

    mountTicketPrintArray(isFirstItem: boolean, eventName: string, text: string) {
        return [
            {
                'text': isFirstItem ? eventName : '\n' + eventName,
                'fontSize': 40,
                'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
            },
            {
                'text': this.ticketFormattedCurrentDate(),
                'fontSize': 36,
                'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
            },
            {
                'text': "Operador: " + localStorage.getItem('USER_NAME') + "\n",
                'fontSize': 18,
                'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
            },
            {
                'text': text + "\n",
                'fontSize': 56,
                'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                'fontWeight': TefIntegration.FONT_WEIGHT_BOLD
            }
        ];
    }

    mountShiftReportPrintArray(eventName: string, userName: string, productsNames: string[], paymentMethods: Array<string>, funds: string[]) {
        return [
            this.shiftReportTitle(eventName.toUpperCase() + '\n' + moment(Number(localStorage.getItem('SHIFT_INITIAL_TIME'))).format('DD/MM/YYYY HH:mm')),
            this.shiftReportTitle('MOVIMENTAÇÕES\n'),
            this.shiftReportSubtitle('VENDAS POR PRODUTO'),
            ...productsNames.map((productName: any) => this.shiftReportValue(productName)),
            this.shiftReportSubtitle('RECEBIMENTOS'),
            ...paymentMethods.map((paymentMethod: any) => this.shiftReportValue(paymentMethod)),
            this.shiftReportSubtitle('FINANCEIRO'),
            ...funds.map((fund: any) => this.shiftReportValue(fund))
        ];
    }

    ticketFormattedCurrentDate() {
        return moment().format('DD/MM/YYYY HH:mm');
    }

    ticketFooter(lineBreak: string) {
        return {
            'text': 'www.teknisa.com' + lineBreak,
            'fontSize': 36,
            'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
            'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
        };
    }

    cashierUser() {
        return {
            'text': "Operador: " + localStorage.getItem('USER_NAME'),
            'fontSize': 18,
            'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
            'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
        };
    }

    private shiftReportTitle(title: string) {
        return {
            'text': title,
            'fontSize': 36,
            'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
            'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
        };
    }

    private shiftReportUser(userName: string) {
        return {
            'text': "Operador: " + userName,
            'fontSize': 18,
            'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
            'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
        };
    }

    private shiftReportSubtitle(subtitle: string) {
        return {
            'text': '- ' + subtitle + ' -',
            'fontSize': 26,
            'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
            'fontWeight': TefIntegration.FONT_WEIGHT_BOLD
        };
    }

    private shiftReportValue(value: string) {
        return {
            'text': value,
            'fontSize': 18,
            'textAlign': TefIntegration.TEXT_ALIGN_RIGHT,
            'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
        };
    }

}

