import Request from '../ts/Request';
import ShiftManager from './ShiftManager';
import moment from 'moment';

export default class SynchronizationQueue {

    queue = JSON.parse(localStorage.getItem('SYNC_QUEUE') || '[]');
    currentCashierId: string = localStorage.getItem('CASHIER_ID') || '';
    lastOrders: any = [];

    addToQueue(request: any, info: string) {
        this.queue.push({
            'body'  : request.body, 
            'route' : request.route,
            'date'  : moment().format('DD/MM/YYYY HH:mm'), 
            'event' : localStorage.getItem('EVENT_NAME'),
            'info'  : info,
        });
        // this.queue.push({'body': request.body, 'route': request.route});
        localStorage.setItem('SYNC_QUEUE', JSON.stringify(this.queue));
    }
    
    startSync(callback: Function) {
        if (this.queue.length === 0) {
            //console.log('resolving');
            callback(true);
            // alert("Operações sincronizadas com sucesso.")
            return ;
        }
        if (!window.navigator.onLine) {
            callback(false);
            // alert("Houve um erro ao sincronizar as operações. Verifique sua conexão com a internet.")
            return ;
        }

        const req = this.queue[0];
        if (req.route === 'event/finishOrderPOS' && this.currentCashierId) {
            req.body.CASHIER_ID = this.currentCashierId;
        }
        if (req.route === 'event/cashierMovement' && this.currentCashierId) {
            req.body.CASHIER_ID = this.currentCashierId;
        }
        if (req.route === 'event/finishCashier' && this.currentCashierId) {
            req.body.CASHIER_ID = this.currentCashierId;
        }

        this.makeRequest(req.route, req.body).then((response: any) => {
            if (req.route === 'event/finishOrderPOS') {
                req.body.id = response.order.id;
                this.lastOrders = [req.body];
            }
            if (req.route === 'event/startCashier') {
                this.currentCashierId = response.cashier.id;
                localStorage.setItem('CASHIER_ID', this.currentCashierId);
            }
            if (req.route === 'event/finishCashier') {
                this.currentCashierId = '';
                localStorage.setItem('CASHIER_ID', '');
                ShiftManager.cleanShiftVariables();
            }
            
            this.queue.splice(0, 1);
            localStorage.setItem('SYNC_QUEUE', JSON.stringify(this.queue));
            this.startSync(callback);
        }).catch(() => {
            callback(false);
            // alert("Houve um erro ao sincronizar as operações.");
        });
    }

    private makeRequest(route: string, body: any) {
        return new Promise((resolve: Function, reject: Function) => {
            const req = new Request();
            req.route = route;
            req.body  = body;
            req.callbackSuccess = resolve;
            req.callbackError   = reject;
            req.callbackTimeout = reject;
            req.send();
        });
    }

}