import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import Request from '../ts/Request';
import TefIntegration from './TefIntegration';
import SynchronizationQueue from './SynchronizationQueue';
import ArrayToPng from '../components/ArrayToPng.vue';
import Util from '@/ts/Util';

declare var cordova: any;

@Component
export default class ShiftManager extends Vue {

    public static shiftStarted = localStorage.getItem('SHIFT_STARTED') === 'true';
    public static notSynchronizedOrders = JSON.parse(localStorage.getItem('NOT_SYNCHRONIZED_ORDERS') || '[]');

    private static products: any;
    private static orders: any;

    private static orderCancelledValue: number = 0;
    private static orderTotalValue: number;

    public static startShift(changeFund: number) {
        ShiftManager.shiftStarted = true;
        new SynchronizationQueue().startSync(() => {
            ShiftManager.shiftStarted = true;
            localStorage.setItem('SHIFT_STARTED', 'true');
            localStorage.setItem('SHIFT_INITIAL_TIME', String(+ new Date()));
            localStorage.setItem('CHANGE_FUND', String(changeFund));
            this.startShiftOnLine()
        });
    }

    public static finishShift(remainingFunds: number, callbackSuccess: Function, callbackError: Function) {
        this.products = Util.flatten(JSON.parse(localStorage.getItem('CATEGORIES') || '[]').map((c: any) => c.products)).filter((p: any) => !p.offerProducts || p.offerProducts.length === 0);
        this.orders = JSON.parse(localStorage.getItem('ORDERS') || '[]');
        this.notSynchronizedOrders = JSON.parse(localStorage.getItem('NOT_SYNCHRONIZED_ORDERS') || '[]');

        if (process.env.VUE_APP_REPORT_PRINT_ENABLED === 'true') {
            this.printReport(
                () => this.afterPrintReport(remainingFunds, callbackSuccess, callbackError),
                callbackError, 
                remainingFunds
            );
        } else {
            this.afterPrintReport(remainingFunds, callbackSuccess, callbackError);
        }
    }

    private static afterPrintReport(remainingFunds: number, callbackSuccess: Function, callbackError: Function) {
        if (confirm('Deseja reimprimir o relatório de movimentações?')) {
            this.finishShift(remainingFunds, callbackSuccess, callbackError);
        } else {
            this.continueFinishShift(remainingFunds, callbackSuccess, callbackError);
        }
    }

    private static continueFinishShift(remainingFunds: number, callbackSuccess: any, callbackError: any) {
        if (window.navigator.onLine) {
            ShiftManager.synchronizeSales(() => {
                this.finishShiftOnLine(remainingFunds).then(() => {
                    this.cleanShiftVariables();
                    callbackSuccess();
                });
            }, callbackError);
        } else {
            this.finishShiftOnLine(remainingFunds).then(() => {
                this.cleanShiftVariables();
                callbackSuccess();
            });
        }
    }

    public static printReport(callbackSuccess: any, callbackError: any, remainingFunds = 0) {
        this.products = Util.flatten(JSON.parse(localStorage.getItem('CATEGORIES') || '[]').map((c: any) => c.products)).filter((p: any) => !p.offerProducts || p.offerProducts.length === 0);
        this.orders = JSON.parse(localStorage.getItem('ORDERS') || '[]');
        this.notSynchronizedOrders = JSON.parse(localStorage.getItem('NOT_SYNCHRONIZED_ORDERS') || '[]');

        this.orderCancelledValue = 0;

        let orders: any[] = [];
        let productsNames: string[] = [];

        ShiftManager.ordersReportStrategy().then((o: any) => {
            orders = o;

            return ShiftManager.paymentMethodsStrategy();
        }).then((paymentMethods: any) => {
            /* Calculating total value and amount of items */
            this.orderTotalValue = 0;
            let amountOfItems = 0;
            orders.forEach((p: any) => {
                this.orderTotalValue += p.total;
                amountOfItems += Number(p.count);
            });

            /* Formatting data */
            productsNames = orders.map((p: any) => p.count + 'x ' + p.name + ': R$' + p.total.toFixed(2))
            .concat('Itens vendidos: ' + amountOfItems)
            .concat('Total bruto: R$' + this.orderTotalValue.toFixed(2))
            .concat('Total cancelamentos: R$' + this.orderCancelledValue.toFixed(2))
            .concat('Total líquido: R$' + (this.orderTotalValue - this.orderCancelledValue).toFixed(2));

            /* Sending to printer */
            const tef = new TefIntegration();
            const report = tef.mountShiftReportPrintArray(localStorage.getItem('EVENT_NAME') || '', localStorage.getItem('USER_NAME')||'', productsNames, paymentMethods, this.generateFundsArray(remainingFunds));
            
            // console.log(report);
            // tef.printMultipleTexts(report).then(callbackSuccess).catch(callbackError);
            console.log(report);
            const atp: any = ArrayToPng;
            atp.printArray(report, 25).then(callbackSuccess);
        });
    }

    /* ----- ORDERS REPORT ----- */

    public static ordersReportStrategy() {
        return new Promise((resolve: any) => {
            ShiftManager.getOrdersReport().then((orders: any) => {
                resolve(orders.getCashierOrdersReport.map((product: any) => {
                    return {
                        id: product.id,
                        name: product.name,
                        count: product.amount,
                        total: Number(product.total)
                    };
                }));
            }).catch(() => {
                resolve(ShiftManager.calculateOrdersReport());
            });
        });
    }

    public static getOrdersReport() {
        return new Promise((resolve: any, reject: any) => {
            const req = new Request();
            req.route = 'event/getCashierOrdersReport';
            req.body  = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'CASHIER_ID': localStorage.getItem('CASHIER_ID')
            };
            req.callbackSuccess = resolve;
            req.callbackTimeout = reject;
            req.callbackError= reject;
            if (window.navigator.onLine) req.send();
            else reject();
        });
    }
    
    static calculateOrdersReport() {
        return ShiftManager.products.map((product: any) => {
            let count = 0;
            let total = 0;
            ShiftManager.orders.forEach((o: any) => { 
                if (o && o.ORDER_ITEMS) {
                    o.ORDER_ITEMS.forEach((i: any) => {
                        if (i.name == product.name) {
                            total += i.totalItem;
                            count += i.quant;
                        }
                    });
                }
            });
            ShiftManager.notSynchronizedOrders.forEach((o: any) => {
                if (o && o.ORDER_ITEMS) {
                    o.ORDER_ITEMS.forEach((i: any) => {
                        if (i.name == product.name) {
                            total += i.totalItem;
                            count += i.quant;
                        }
                    });
                }
            });
            return {
                'id': product.id,
                'name': product.name,
                'count': count,
                'total': total
            }}
        );
    }

    /* ----- PAYMENT METHODS ----- */

    public static paymentMethodsStrategy() {
        return new Promise((resolve: any) => {
            ShiftManager.getReceiptsReport().then((response: any) => {
                const paymentMethods = response.getCashierReceiptsReport;
                let total = 0, totalLiquido = 0;
                paymentMethods.forEach((pm: any) => {
                    if (pm.total > 0) total += Number(pm.total);
                    totalLiquido += Number(pm.total);
                });
                this.orderCancelledValue = total - totalLiquido;
                paymentMethods.push({ label: 'Total bruto', total });
                paymentMethods.push({ label: 'Total cancelamentos', total: this.orderCancelledValue });
                paymentMethods.push({ label: 'Total líquido', total: totalLiquido });

                resolve(paymentMethods.map((paymentMethod: any) => {
                    return paymentMethod.label + ': R$' + Number(paymentMethod.total).toFixed(2);
                }));
            }).catch(() => {
                resolve(ShiftManager.generatePaymentMethodsArray());
            });
        });
    }

    public static getReceiptsReport() {
        return new Promise((resolve: any, reject: any) => {
            const req = new Request();
            req.route = 'event/getCashierReceiptsReport';
            req.body  = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'CASHIER_ID': localStorage.getItem('CASHIER_ID')
            };
            req.callbackSuccess = resolve;
            req.callbackTimeout = reject;
            req.callbackError= reject;
            if (window.navigator.onLine) req.send();
            else reject();
        });
    }

    static generatePaymentMethodsArray() {
        const paymentMethods = JSON.parse(localStorage.getItem('PAYMENT_METHODS') || '[]');
        let totalBruto = 0;
        let totalLiquido = 0;
        let array = paymentMethods.map((paymentMethod: any) => {
            let totalValue = 0;
            ShiftManager.orders.forEach((o: any) => {
                if (o && o.PAYMENT_METHOD == paymentMethod.PAYMENT_METHOD) {
                    totalValue += o.TOTAL_VALUE;
                }
            });
            ShiftManager.notSynchronizedOrders.forEach((o: any) => {
                if (o && o.PAYMENT_METHOD == paymentMethod.PAYMENT_METHOD) {
                    totalValue += o.TOTAL_VALUE;
                }
            });
            totalBruto += totalValue;
            if (totalValue > 0) totalLiquido += Number(totalValue);
            return paymentMethod.LABEL + ': R$' + totalValue.toFixed(2);
        });
        array = array.concat('Total bruto: R$' + totalBruto.toFixed(2));
        array = array.concat('Total cancelamentos: R$' + this.orderCancelledValue.toFixed(2));
        array = array.concat('Total líquido: R$' + totalLiquido.toFixed(2));
        return array;
    }

    /* ----- FUNDS ------ */
    
    static generateFundsArray(remainingFunds: number) {
        let returnArray = [
            'Fundo de troco inicial: R$' + Number(localStorage.getItem('CHANGE_FUND')).toFixed(2),
            'Suprimentos: R$' + Number(localStorage.getItem('SUPPLIES')).toFixed(2),
            'Retiradas: R$' + Number(localStorage.getItem('WITHDRAWS')).toFixed(2),
            'Restante no caixa: R$' + remainingFunds.toFixed(2)
        ];
        // let difference = remainingFunds - this.orderTotalValue - this.getChangeFund() - this.getSupplies() + this.getWithdraws();
        // returnArray = returnArray.concat(difference >= 0 ? 'Diferença: R$' + difference.toFixed(2) : 'Diferença: - R$' + Math.abs(difference).toFixed(2));
        return returnArray;
    }

    public static cleanShiftVariables() {
        localStorage.setItem('ORDERS', '[]');
        localStorage.setItem('NOT_SYNCHRONIZED_ORDERS', '[]');
        localStorage.setItem('SHIFT_STARTED', 'false');
        localStorage.setItem('SHIFT_INITIAL_TIME', '');
        localStorage.setItem('CHANGE_FUND', '');
        localStorage.setItem('SUPPLIES', '');
        localStorage.setItem('WITHDRAWS', '');
        localStorage.setItem('CASHIER_ID','');
        ShiftManager.notSynchronizedOrders = [];
        ShiftManager.shiftStarted = false;
    }

    public static synchronizeSales(callbackSuccess: any, callbackError: Function) {
        new SynchronizationQueue().startSync(callbackSuccess);
    }

    public static getChangeFund(): number{
        return Number(localStorage.getItem('CHANGE_FUND') || '0');
    }

    public static getSupplies(): number{
        return Number(localStorage.getItem('SUPPLIES') || '0');
    }

    public static getWithdraws(): number{
        return Number(localStorage.getItem('WITHDRAWS') || '0');
    }

    public static startShiftOnLine() {
        const now = localStorage.getItem('SHIFT_INITIAL_TIME') ? moment(Number(localStorage.getItem('SHIFT_INITIAL_TIME')) || '') : moment();
        return new Promise((resolve: Function, reject: Function) => {
            const req = new Request();
            req.route  = 'event/startCashier';
            req.body   = { 
                EVENT_ID: JSON.parse(localStorage.getItem('event') || '{}').id,
                EVENT_SELLER_ID: localStorage.getItem('EVT_EVENT_SELLER_ID'),
                NRORG: localStorage.getItem('NRORG'),
                INITIAL_VALUE: localStorage.getItem('CHANGE_FUND'),
                INITIAL_TIME: now.format('YYYY/MM/DD HH:mm'), 
                USER_ID: localStorage.getItem('USER_ID'),
                TOKEN: localStorage.getItem('TOKEN')
            };
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('CASHIER_ID', response.cashier.id);     
                resolve(response.cashier.id);
            };
            req.callbackError = () => { new SynchronizationQueue().addToQueue(req, 'e'); };
            req.callbackTimeout = () => { new SynchronizationQueue().addToQueue(req, 't'); };
            if (!window.navigator.onLine) { new SynchronizationQueue().addToQueue(req, 'o'); }
            else req.send();
        });
    }

    public static finishShiftOnLine(remainingFunds: number) {
        return new Promise((resolve: any, reject: Function) => {
            this.newWithdrawOnLine(remainingFunds).then(() => {
                const req = new Request();
                req.route  = 'event/finishCashier';
                req.body   = { 
                    EVENT_ID: JSON.parse(localStorage.getItem('event') || '{}').id,
                    NRORG: localStorage.getItem('NRORG'),
                    CASHIER_ID: localStorage.getItem('CASHIER_ID'),
                    FINAL_TIME: moment().format('YYYY/MM/DD HH:mm'), 
                    USER_ID: localStorage.getItem('USER_ID'),
                    TOKEN: localStorage.getItem('TOKEN')
                };
                req.callbackSuccess = resolve;
                req.callbackError = () => { new SynchronizationQueue().addToQueue(req, 'e'); resolve(); };
                req.callbackTimeout = () => { new SynchronizationQueue().addToQueue(req, 't'); resolve(); };
                if (!window.navigator.onLine) { new SynchronizationQueue().addToQueue(req, 'o'); resolve(); }
                else req.send();
            });
        });
    }

    public static newSupplyOnLine(value: number) {
        const req = new Request();
        req.route  = 'event/cashierMovement';
        req.body   = {
            CASHIER_ID: localStorage.getItem('CASHIER_ID'), 
            EVENT_ID: JSON.parse(localStorage.getItem('event') || '{}').id,
            NRORG: localStorage.getItem('NRORG'),
            VALUE: value,
            MOVEMENT_TIME: moment().format('YYYY/MM/DD HH:mm'), 
            USER_ID: localStorage.getItem('USER_ID'),
            TOKEN: localStorage.getItem('TOKEN')
        };
        req.callbackSuccess = (response: any) => {

        };
        req.callbackError = () => { new SynchronizationQueue().addToQueue(req, 'e'); };
        req.callbackTimeout = () => { new SynchronizationQueue().addToQueue(req, 't'); };
        if (!window.navigator.onLine) { new SynchronizationQueue().addToQueue(req, 'o'); }
        else new SynchronizationQueue().startSync(() => {
            req.body.CASHIER_ID = localStorage.getItem('CASHIER_ID');
            req.send();
        });
    }

    public static newWithdrawOnLine(value: number) {
        return new Promise((resolve: Function,) => {
            const req = new Request();
            req.route  = 'event/cashierMovement';
            req.body   = {
                CASHIER_ID: localStorage.getItem('CASHIER_ID'), 
                EVENT_ID: JSON.parse(localStorage.getItem('event') || '{}').id,
                NRORG: localStorage.getItem('NRORG'),
                VALUE: (-1*value),
                MOVEMENT_TIME: moment().format('YYYY/MM/DD HH:mm'), 
                USER_ID: localStorage.getItem('USER_ID'),
                TOKEN: localStorage.getItem('TOKEN')
            };
            req.callbackSuccess = resolve;
            req.callbackError = () => { new SynchronizationQueue().addToQueue(req, 'e'); resolve(); };
            req.callbackTimeout = () => { new SynchronizationQueue().addToQueue(req, 't'); resolve(); };
            if (!window.navigator.onLine) { new SynchronizationQueue().addToQueue(req, 'o'); resolve(); }
            else new SynchronizationQueue().startSync(() => {
                req.body.CASHIER_ID = localStorage.getItem('CASHIER_ID');
                req.send();
            });
        });
    }

    public static shiftHasStarted(callback: any) {
        const req = new Request();
        req.route = 'event/getCurrentCashierFromEventSeller';
        req.body  = {
            'EVENT_SELLER_ID': localStorage.getItem('EVT_EVENT_SELLER_ID'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'NRORG': localStorage.getItem('NRORG')
        };
        req.callbackSuccess = (response: any) => {
            if (response.cashier && response.cashier.id) {
                localStorage.setItem('CASHIER_ID', response.cashier.id);
                localStorage.setItem('SHIFT_INITIAL_TIME', String(+ new Date(response.cashier.openning.date)));
                localStorage.setItem('SHIFT_STARTED', 'true');
                this.shiftStarted = true;
                if (callback) callback(true);
            } else if (callback) callback(false);
        };
        req.send();
        return false;
    }

    public static authAdmin(admin: string, password: string) {
        return new Promise((resolve: Function, reject: Function) => {
            const req = new Request();
            req.route = 'loginBackoffice';
            req.body   = {
                'EMAIL': admin,
                'PASSWORD': password,
                'NRORG': 0,
                'USER_TYPE_ID': 4
            };
            req.callbackSuccess = resolve;
            req.callbackError = () => { reject("Senha inválida"); };
            req.callbackTimeout = () => { reject("Ocorreu um erro na autenticação");};
            if (!window.navigator.onLine) { reject("Não é possível completar a operação offline"); }
            else req.send();
        });
    }

}