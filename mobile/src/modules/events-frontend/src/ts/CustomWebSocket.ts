export default class CustomWebSocket {

    private wsUri: string;
    private onMessage: any;
    private webSocket: WebSocket;

    constructor(wsUri: string, onMessage: any) {
        this.wsUri = wsUri;
        this.onMessage = onMessage;
        this.webSocket = new WebSocket(wsUri);
        this.webSocket.onopen = (evt: Event) => {
            console.log('Connected to websocket ' + wsUri);
        };
        this.webSocket.onclose = (evt: Event) => {
            console.log('Aborted connection to websocket ' + wsUri);
        };
        this.webSocket.onmessage = this.onMessage;
        this.webSocket.onerror = (evt: Event) => {
            console.log('Error with websocket ' + wsUri + ' connection');
        };
    }

}