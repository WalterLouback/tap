import { Component, Prop, Vue } from 'vue-property-decorator';
import axios from 'axios';
import NProgress from 'nprogress';

@Component
export default class DeprecatedRequest extends Vue {

    static BLOCK_TOUCH: boolean = true;

    // Function which makes the request
    requestApi(reqValues: object, reqType: string, callbackSuccess: any, callbackError: any, blockTouchEvents: boolean = false, isFirstTry: boolean = true): any {
        // Defines de request url
        // const urlBase: string = 'https://mtc-teste.teknisa.com/generalbackend/service/index.php/';
        const urlBase: string = 'http://devidyoliveira.zeedhi.com/workfolder/appCostumer/generalbackend/service/index.php/';
        const urlReq: string = urlBase + reqType;
        const self = this;
        // Spinner
        this.startLoading(blockTouchEvents);
        // Post Function
        axios.post(urlReq,
            {
            'requestType': 'Row',
            'row': reqValues
            })
            .then(function(response) {
                self.finishLoading(blockTouchEvents);
                const requestData: any = response.data.dataset;
                callbackSuccess(requestData);
            })
            .catch(function(error) {
                self.finishLoading(blockTouchEvents);
                if (!error.response) console.log(error);
                else {
                    const errorResp = error.response.data;
                    if (errorResp.errorCode !== 42) {
                        console.log('Error:');
                        console.log(error.response.data);
                        callbackError(error);
                    } else {
                        console.log('Generating new session...');
                        self.generateSession({reqValues, reqType, callbackSuccess, callbackError, blockTouchEvents, isFirstTry});
                    }
                }
            })
            .then(function() {
            });
    }

    startLoading(blockTouchEvents: boolean = false)  {
        NProgress.start();
        if (blockTouchEvents) {
            $('body').css('pointer-events', 'none');
            $('html').css('pointer-events', 'none');
        }
    }

    finishLoading(blockTouchEvents: boolean = false)  {
        NProgress.done();
        if (blockTouchEvents) {
            $('body').css('pointer-events', 'auto');
            $('html').css('pointer-events', 'auto');
        }
    }

    generateSession(oldRequest: any) {
        console.log('GENERATING SESSION');
        try {
            if (oldRequest.isFirstTry) {
                const userId: string = localStorage.getItem('USER_ID') || '';
                const refreshTKN: string = localStorage.getItem('REFRESH_TOKEN') || '';
                const loginData: object = {
                    'USER_ID': userId,
                    'REFRESH_TOKEN': refreshTKN
                };
                const reqType = 'requestToken';
                const self = this;
                this.requestApi(
                    loginData,
                    reqType,
                    function(response: any) {
                        console.log('OK');
                        console.log(response);
                        localStorage.setItem('TOKEN', response.user.token);
                        self.remakeRequest(oldRequest);
                    },
                    function(error: any) {
                        console.log('ERRO SESSION');
                        console.log(error);
                        oldRequest.callbackError(error);
                        //self.$router.push('/');
                    }
                );
            }

        } catch (e) {
            console.log('Erro Try Catch');
            console.log(e);
        }
    }

    remakeRequest(oldRequest: any) {
        oldRequest.reqValues.TOKEN = localStorage.getItem('TOKEN');
        this.requestApi(oldRequest.reqValues, oldRequest.reqType, oldRequest.callbackSuccess, oldRequest.callbackError, oldRequest.blockTouchEvents, false);
    }

}