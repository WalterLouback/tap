import { Component, Prop, Vue } from 'vue-property-decorator';
import axios from 'axios';
import NProgress from 'nprogress';

@Component
export default class Request extends Vue {

    body: any = {};
    route: string = '';
    callbackSuccess: Function = new Function();
    callbackError: Function = new Function();
    callbackTimeout: Function = new Function();
    blockTouchEvents: boolean = false;
    showLoader: boolean = true;
    timeout: number = 30000;
    static staticBaseUrl = '';
    urlBase: string = Request.staticBaseUrl || process.env.VUE_APP_BACKEND_URI || '';

    private isFirstTry: boolean = true;

    // Function which makes the request
    send(): any {
        // Defines the request url
        // http://lucasmacedo.zeedhi.com/workfolder/appCostumer/generalbackend
        // https://mtc-teste.teknisa.com/generalbackend
        
        const urlBackEnd = this.urlBase;
        const urlReq: string = urlBackEnd + this.route;
        const self = this;
        // Spinner
        this.startLoading();
        // Post Function
        axios.post(urlReq,
            {
            'requestType': 'Row',
            'row': this.body
            },
            {
                'timeout': this.timeout
            })
            .then(function(response) {
                self.finishLoading();
                const requestData: any = response.data.dataset;
                self.callbackSuccess(requestData);
            })
            .catch(function(error) {
                self.finishLoading();
                console.log(error.code);
                if (error.code === 'ECONNABORTED') { self.callbackTimeout(); return ; }
                if (!error.response) console.log(error);
                else {
                    const errorResp = error.response.data;
                    if (errorResp.errorCode !== 42) {
                        console.log('Error:');
                        console.log(error.response.data);
                        self.callbackError(error);
                    } else {
                        console.log('Generating new session...');
                        self.generateSession();
                    }
                }
            })
            .then(function() {
            }
        );
    }

    startLoading()  {
        if (this.showLoader) {
            NProgress.start();
            if (this.blockTouchEvents) {
                $('body').css('pointer-events', 'none');
                $('html').css('pointer-events', 'none');
            }
        }
    }
    
    finishLoading()  {
        if (this.showLoader) {
            NProgress.done();
            if (this.blockTouchEvents) {
                $('body').css('pointer-events', 'auto');
                $('html').css('pointer-events', 'auto');    
            }
        }
    }

    generateSession() {
        console.log('GENERATING SESSION');
        if (this.isFirstTry) {
            const self = this;
            const userId: string = localStorage.getItem('USER_ID') || '';
            const refreshTKN: string = localStorage.getItem('REFRESH_TOKEN') || '';

            const req = new Request();
            req.body = {
                'USER_ID': userId,
                'REFRESH_TOKEN': refreshTKN
            };
            req.route = 'requestToken';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('TOKEN', response.user.token);
                self.remakeRequest();
            }
            req.callbackError = function(error: any) {
                console.log('ERRO SESSION');
                console.log(error);
                self.callbackError(error);
            }
            req.send();
        }
    }

    remakeRequest() {
        this.isFirstTry = false;
        if (this.body) this.body.TOKEN = localStorage.getItem('TOKEN');
        this.send();
    }

}
