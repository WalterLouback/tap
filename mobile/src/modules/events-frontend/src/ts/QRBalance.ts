import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import Request from '../ts/Request';
import ArrayToPng from '../components/ArrayToPng.vue';
import TefIntegration from './TefIntegration';
import SynchronizationQueue from './SynchronizationQueue';
import PrinterPaperManager from '../ts/PrinterPaperManager';
import QRCode from 'qrcode';

declare var cordova: any;

@Component
export default class QRBalance extends Vue {

    private static operation: string = "";//C: compra, R: recarga, B: ver saldo, 
    private static active: boolean = false;
    private static qrOpError: boolean = false;
    private static noIdentifier: boolean = false;
    private static printQRCard: boolean = false;
    public static values = JSON.parse(localStorage.getItem('QR_VALUES')||'[]');
    public static qrURL = process.env.VUE_APP_FRONTEND_URI + 'wallet?';
    private static printerPaperManager = PrinterPaperManager.getInstance();
    private static qrCodeImage: any = null;

    public static setOperation(o: string) {
        this.operation = o; 
    }

    public static getOperation(){
        return this.operation;
    }

    public static setAsActive(a: boolean) {
        this.active = a; 
    }

    public static isActive(){
        return this.active;
    }

    public static setQrOpError(e: boolean) {
        this.qrOpError = e; 
    }

    public static getQrOpError(){
        return this.qrOpError;
    }

    public static setPrintCard(p: boolean) {
        this.printQRCard = p; 
    }

    public static getPrintCard(){
        return this.printQRCard;
    }

    public static setNoIdentifier(e: boolean) {
        this.noIdentifier = e; 
    }

    public static getNoIdentifier(){
        return this.noIdentifier;
    }

    public static printQRCode(seed: string, balanceValue: number) :Promise<any> {
        const atp: any = ArrayToPng;
        const balance = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL', minimumFractionDigits: 2 }).format(balanceValue);
        seed = this.qrURL + seed; 

        QRCode.toDataURL(seed,{ errorCorrectionLevel: 'H', scale:6 }, (err: any, string: string) => {
            if (err) this.qrCodeImage = '';
            this.qrCodeImage = string;
        })

        return new Promise((resolve: any, reject: any) => {
            const tef = new TefIntegration();
            const eventName = (localStorage.getItem('EVENT_NAME') || '').toUpperCase();

            atp.printArray(
                [
                    {
                        'text': eventName,
                        'fontSize': 40,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': moment().format('DD/MM/YYYY HH:mm'),
                        'fontSize': 36,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    tef.cashierUser(),
                    { qrcode: seed },
                    {
                        'text': (this.getOperation() == 'B')? 'Saldo disponível: ' + balance : 'Saldo carregado: ' + balance,
                        'fontSize': 18,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': (localStorage.getItem('IDENTIFIER'))? 'ID: '+ (localStorage.getItem('IDENTIFIER')||'')+'\n':'',
                        'fontSize': 18,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': (this.getOperation() != 'C')? 'Reimpressão'+'\n':'',
                        'fontSize': 18,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    tef.ticketFooter('\n\n')
                ], 10
            ).then(resolve);
        });
    }

    public static printQRCodeStatement(statement: any, balanceValue: number, receivedValue: number, spendsValue: number) :Promise<any>{
        const self = this;
        const atp: any = ArrayToPng;
        const balance = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL', minimumFractionDigits: 2 }).format(balanceValue);
        const received = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL', minimumFractionDigits: 2 }).format(receivedValue);
        const spends = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL', minimumFractionDigits: 2 }).format(spendsValue);

        return new Promise((resolve: any, reject: any) => {
            const tef = new TefIntegration();

            atp.printArray(
                [
                    {
                        'text': 'EXTRATO',
                        'fontSize': 40,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': '\nPeríodo: ' +  moment().subtract(30, 'days').format('DD/MM/YYYY')+ " - "+ moment().format('DD/MM/YYYY'),
                        'fontSize': 25,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': (localStorage.getItem('IDENTIFIER'))? 'ID: '+ (localStorage.getItem('IDENTIFIER')||'')+'\n\n':'',
                        'fontSize': 25,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    {
                        'text': ' - MOVIMENTAÇÕES -\n',
                        'fontSize': 30,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_BOLD
                    },
                    ...QRBalance.printStatementItens(statement),
                    {
                        'text': ' - TOTALIZADORES -\n',
                        'fontSize': 30,
                        'textAlign': TefIntegration.TEXT_ALIGN_CENTER,
                        'fontWeight': TefIntegration.FONT_WEIGHT_BOLD
                    },
                    {
                        'text': 'Recebimentos: ' + received +'\n'+'Gastos: ' + spends +'\n'+ 'Saldo atual: '+ balance,
                        'fontSize': 25,
                        'textAlign': TefIntegration.TEXT_ALIGN_RIGHT,
                        'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL
                    },
                    tef.ticketFooter('\n\n')
                ], 10
            ).then(resolve);
        });
    }

    public static printStatementItens(statement: any){
        let array: any[] = [];
        var s:any = Array.from(statement);

        const tef = new TefIntegration();
        
        if (s.length == 0) { return []; }
        for(var i = 0; i<statement.length; i++){
            array = array.concat({
                'text': s[i].name+ "\n" + s[i].value+"\n"+s[i].spec+"\n",
                'fontSize': 25,
                'textAlign': TefIntegration.TEXT_ALIGN_LEFT,
                'fontWeight': TefIntegration.FONT_WEIGHT_NORMAL                   
            });
        }
        
        return array;
    }
    public static testPrint(something: any){
        return new Promise((resolve: any, reject: any) => {console.log(something); resolve()})
    }
}