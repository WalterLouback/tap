export default class PrinterPaperManager {

    private        LOCAL_VAR_NAME     = 'CURRENT_PAPER_AMOUNT';

    private        INITIAL_AMOUNT     = 700; // in centimeters
    public static  TAM_TICKET         = 9.5; // in centimeters
    public static  TAM_COMPROVANTE    = 6;   // in centimeters

    private static instance           = new PrinterPaperManager();
    private        currentPaperAmount = 0;

    private constructor() {
        this.currentPaperAmount = localStorage.getItem(this.LOCAL_VAR_NAME) ? Number(localStorage.getItem(this.LOCAL_VAR_NAME)) : this.INITIAL_AMOUNT;
    }

    public static getInstance() {
        return PrinterPaperManager.instance;
    }

    private saveLocalVar() {
        localStorage.setItem('CURRENT_PAPER_AMOUNT', String(this.currentPaperAmount));
    }

    restorePaperAmount() {
        this.currentPaperAmount = this.INITIAL_AMOUNT;
        this.saveLocalVar();
    }

    beforePrint() {
        const self = this;
        return new Promise(function(resolve: Function, reject: Function) {
            if (self.currentPaperAmount >= PrinterPaperManager.TAM_TICKET)
                resolve(false);
            else resolve(true);
        });
    }

    afterPrint(size: any) {
        this.currentPaperAmount -= size;
        this.saveLocalVar();
    }

}