import { Component, Prop, Vue } from 'vue-property-decorator'
import 'cordova-plugin-qrscanner'

declare var $: any;
declare var QRScanner: any;

@Component({
    components: { }
})

export default class QRscanner extends Vue {

    scan(callback: Function) {
        QRScanner.prepare(function(response: any) {
            QRScanner.scan(callback);
            QRScanner.show(() => { });
        });
    }

    stop() {
        QRScanner.destroy(function(status: any) { 
            $('html').css('background', 'white') 
        });
    }
    
}