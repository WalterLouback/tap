import { Vue, Component } from 'vue-property-decorator';
import Router from '@/router';
import EventDetails from './src/views/EventDetails.vue';
import EventStores from './src/views/EventStores.vue';
import Request from './src/ts/Request';
import EventSchedule from './src/views/EventSchedule.vue';
import BuyEventTicket from './src/views/BuyEventTicket.vue';
import NominateTicket from './src/views/NominateTicket.vue';
import FilterEvents from './src/views/FilterEvents.vue';
import Ingressos from './src/views/Ingressos.vue';
import QrcodeTicket from './src/views/QrcodeTicket.vue';
import TicketPaymentMethod from './src/views/TicketPaymentMethod.vue';
import './src/font-awesome-imports';
import $ from 'jquery';
import Util from './src/ts/Util';



export default class EventsInitializer extends Vue {

    static callback: any = null;
    static hasCallback: boolean = false;
    static share: boolean = false;
    static shareUrl: string = '';
    static shareMessage: string = '';
    static requireLogin: boolean = false;
    static returnRoute: string = 'HomeApp'

    public static prepare(baseUrl: string, callback?: any) {
        if (callback){
            this.hasCallback = true;
            this.callback = ()=>{
                Util.removeBackButtonBehavior();
                callback(); 
            }
        }
        Request.staticBaseUrl = baseUrl + 'index.php/';
        Router.addRoutes([
            {
                path: '/EventDetails',
                name: 'EventDetails',
                component: EventDetails,
                beforeEnter: (to: any, from: any, next) => {
                    const condition = localStorage.getItem('LAST_ROUTE') == 'Ingressos' || localStorage.getItem('SHOW_FILTERED_EVENTS') == 'true';
                    if (condition == true) {
                    localStorage.setItem('SHOW_FILTERED_EVENTS', 'true');
                    }
                    next();
                }
            },
            {
                path: '/EventStores',
                name: 'EventStores',
                component: EventStores
            },
            {
                path: '/FilterEvents',
                name: 'FilterEvents',
                component: FilterEvents,
                beforeEnter: (to: any, from: any, next) => {
                    const condition = localStorage.getItem('LAST_ROUTE') == 'Ingressos' || localStorage.getItem('SHOW_FILTERED_EVENTS') == 'true';
                    if (condition == true) {
                    localStorage.setItem('SHOW_FILTERED_EVENTS', 'true');
                    }
                    next();
                }
            },
            {
                path: '/EventSchedule',
                component: EventSchedule
            },
            {
                path: '/BuyEventTicket',
                component: BuyEventTicket
            },
            {
                path: '/NominateTicket',
                component: NominateTicket
            },
            {
                path: '/Ingressos',
                component: Ingressos,
                name: 'Ingressos'
                
            },
            {
                path: '/QrcodeTicket',
                component: QrcodeTicket,
                name: 'QrcodeTicket'
            },
            {
                path: '/TicketPaymentMethod',
                component: TicketPaymentMethod,
                name: 'TicketPaymentMethod'
            }
        ]);
    }

    public static setShareOptions(share: boolean, shareMessage: string, shareUrl: string, requireLogin: boolean, returnRoute: string) {
        this.share = share; 
        this.shareMessage = shareMessage;
        this.shareUrl = shareUrl + '?';
        this.requireLogin = requireLogin; 
        this.returnRoute = returnRoute;
        if(share){
            const w: any = window;
            w.handleOpenURL = this.handleOpenURL;
        }
    }

    public static handleOpenURL(url:any) {
        var data:any = url.split('?')[1].split('&');
        localStorage.setItem('SHARE_TYPE', '');
        localStorage.setItem('SHARE_ID', '');
        localStorage.setItem('SHARE_PARENT_ID', '');
        data.forEach(
          (d:any) =>{
            if (d.split('=')[0] == 'p') localStorage.setItem('SHARE_PARENT_ID', d.split('=')[1]);
            else if (d.split('=')[0] == 'id') localStorage.setItem('SHARE_ID', d.split('=')[1]);
            else if (d.split('=')[0] == 'et') localStorage.setItem('SHARE_TYPE', d.split('=')[1]);
          }
        );
        Router.push(EventsInitializer.returnRoute);
        EventsInitializer.sendToSharedEvent();
    }

    public static sendToSharedEvent(){
        if (localStorage.getItem('SHARE_PARENT_ID')) {
            var parentEventList = localStorage.getItem('SHARE_PARENT_ID')!.split(',');
            var parentEvent = parentEventList[0];
            this.goToEvent(parentEvent||'');
        }
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='e') this.goToEvent(localStorage.getItem('SHARE_ID')!);
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='s') this.goToStore(localStorage.getItem('SHARE_ID')!);
    }

    public static goToEvent(id: string){
        localStorage.setItem('SELECTED_EVENT', id);
        localStorage.setItem('EVENT', '');
        Router.push("/EventDetails" );
    }

    public static goToStore(id: string) {
        localStorage.setItem('IS_AN_EVENT', '');
        Util.removeBackButtonBehavior();
        localStorage.setItem('SHARE_ID', '')
        localStorage.setItem('SHARE_TYPE', '')
        // Orders.goToStores(new StoresConfig(this.goToEventDetails, this.eventDetail.id));
        localStorage.setItem('IS_AN_EVENT','');
        localStorage.setItem('STORE_ID', id);
        localStorage.setItem('storeId', id);
        Router.push('Produtos');
    }
}
