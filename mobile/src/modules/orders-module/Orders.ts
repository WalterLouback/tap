import Router from '@/router'
import Lojas from './src/views/Lojas.vue';
import Produtos from './src/views/Produtos.vue'
import Produto from './src/views/Produto.vue'
import Pedido from './src/views/Pedido.vue'
import PedidoComanda from './src/views/PedidoComanda.vue'
import EscolherUnidade from './src/views/EscolherUnidade.vue'
import PedidosRealizados from './src/views/PedidosRealizados.vue'
import PaymentMethod from './src/views/PaymentMethod.vue';
import AddCard from './src/views/AddCard.vue';
import LerQrMesa from './src/views/LerQrMesa.vue';
import RegisteredCards from './src/views/RegisteredCards.vue';
import OrderSuccess from './src/views/OrderSuccess.vue';
import ExtratoPedido from './src/views/ExtratoPedido.vue';
import DeliverLocation from './src/views/DeliverLocation.vue';

import './src/font-awesome-imports';
import './src/stylesheet/app.scss';
import './src/stylesheet/components.scss';
import './src/stylesheet/views.scss';
import '@/../node_modules/card/dist/card.js';
import './src/store'
import Util from './src/ts/Util';

export default class Orders {

    static callback = new Function();
    static share: boolean = false;
    static shareMessage: string = '';
    static shareUrl: string = '';

    public static addRoutes() {
        try { 
            Router.addRoutes([
                {
                    path: '/Produtos',
                    component: Produtos,
                    name: 'Produtos'
                },
                {
                    path: '/Produto',
                    component: Produto,
                    name: 'Produto'
                },
                {
                    path: '/Pedido',
                    component: Pedido,
                    name: 'Pedido'
                },
                {
                    path: '/PedidoComanda',
                    component: PedidoComanda,
                    name: 'PedidoComanda'
                },
                {
                    path: '/LerQrMesa',
                    component: LerQrMesa,
                    name: 'LerQrMesa'
                },
                {
                    path: '/DeliverLocation',
                    component: DeliverLocation,
                    name: 'DeliverLocation'
                }
            ]);
        } catch (e) { }
    }

    public static addPaymentRoutes(){
        try { 
            Router.addRoutes([
                {
                    path: '/PaymentMethod',
                    component: PaymentMethod,
                    name: 'PaymentMethod'
                },
                {
                    path: '/AddCard',
                    component: AddCard,
                    name: 'AddCard'
                },
                {
                    path: '/RegisteredCards',
                    component: RegisteredCards,
                     name: 'RegisteredCards'
                 }
            ]);
        } catch (e) { }

    }

    public static addRecentOrdersRoutes(){
        try { 
            Router.addRoutes([
                {
                    path: '/PedidosRealizados',
                    component: PedidosRealizados,
                    name: 'PedidosRealizados'
                },
                {
                    path: '/ExtratoPedido',
                    component: ExtratoPedido,
                    name: 'ExtratoPedido'
                }
            ]);
        } catch (e) { }
    }

    public static setShareOptions(share:boolean = false, shareMessage: string = '', shareUrl: string = '') {
        //share options 
        this.share = share;
        this.shareMessage = shareMessage;
        this.shareUrl = shareUrl + '?';
    }

    public static goToStores(config: StoresConfig) {
        // this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback(); 
        }

        localStorage.setItem('ENTERED_ORDERS_MODULE_FROM', (Router as any).history.current.path);
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', '');
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        localStorage.setItem('ENTERED_REGISTERED_CARDS', '');
        localStorage.setItem('STORE_ID', config.storeId);
        localStorage.setItem('storeId', config.storeId);
        Router.push('Produtos');
        localStorage.setItem('STORE_PROPS', JSON.stringify(config));
    }

    public static goToChooseStructure(config: ChooseStructureConfig){
        // this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback(); 
        }
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', (Router as any).history.current.path);  
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        localStorage.setItem('ENTERED_ORDERS_MODULE_FROM', '');
        localStorage.setItem('ENTERED_REGISTERED_CARDS', '');
        Router.push('EscolherUnidade');
        localStorage.setItem('CHOOSE_STRUCTURE_PROPS', JSON.stringify(config));
    }

    public static goToRecentOrders(config: RecentOrdersConfig){  
        // this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback(); 
        }
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', (Router as any).history.current.path);
        localStorage.setItem('ENTERED_ORDERS_MODULE_FROM', '');
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', '');
        localStorage.setItem('ENTERED_REGISTERED_CARDS', '');
        Router.push('PedidosRealizados');
        localStorage.setItem('RECENT_ORDERS_PROPS', JSON.stringify(config));
    }

    public static goToRegisteredCards(config: RecentOrdersConfig){  
        // this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback(); 
        }
        localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        localStorage.setItem('ENTERED_ORDERS_MODULE_FROM', '');
        localStorage.setItem('ENTERED_CHOOSE_STRUCTURE_FROM', '');
        Router.push('RegisteredCards');
        localStorage.setItem('ENTERED_REGISTERED_CARDS',  (Router as any).history.current.path);
        localStorage.setItem('REGISTERED_CARDS_PROPS', JSON.stringify(config));
    }

}

export class RecentOrdersConfig {

    callback: Function;

    constructor(callback: Function) {
            this.callback = callback; 
    }

}

export class StoresConfig {

    callback: Function;
    storeId: string; 

    constructor(callback: Function, storeId: string) {
            this.callback = callback; 
            this.storeId = storeId;
    }

}

export class ChooseStructureConfig {

    callback: Function;


    constructor(callback: Function) {
            this.callback = callback; 
    }

}

