import { Component, Prop, Vue } from 'vue-property-decorator'
import TextLink from '../components/TextLink.vue'
import DefaultButton from '../components/DefaultButtonOrd.vue'
import QRscanner from '../ts/QRscanner'
import ModalCodigo from '../components/ModalCodigo.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import Util from '../ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';

declare var M: any;
declare var $: any;
declare var window: any;

@Component({
    components: { DefaultButton, TextLink, ModalCodigo, HeaderFluxo, HeaderCircleButton }
})

export default class LerQrMesa extends Vue {
    showModal: boolean = false;
    errorMessage: string = 'Ocorreu um erro desconhecido. Verifique sua conexão.'

    scanner: any = new QRscanner();
    showModalTypeQrCode: boolean = false;
    typedCode: string = "";
    backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack};

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        const self = this;

        this.scanner.scan(
            function (err: any, content:string) {
            if (err) {
                self.mostraModal('Não foi possível abrir a câmera');
            }
            else {
                localStorage.setItem('deliverToBalcony', "false");
                let structureId = content;
                localStorage.setItem('structureId', structureId); 
                self.$router.push('Pedido');
            }
        });

    }

    goBack(){
        this.$router.go(-1);
    }

    beforeDestroy() {
        this.scanner.stop();
    }

    mostraModal(message: string) {
        this.errorMessage = message;
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
        // this.$router.go(-1);
    }

    openModalTypeQrCode() {
        this.showModalTypeQrCode = true;
    }

    closeModalTypeQrCode() {
        this.typedCode = "";
        this.showModalTypeQrCode = false;
    }

    enterCode() {
        localStorage.setItem('structureId', this.typedCode); 
        this.$router.push('Pedido');
    }

}