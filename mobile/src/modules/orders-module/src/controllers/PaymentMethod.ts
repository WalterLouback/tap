import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '../components/SectionArea.vue';
import TextLink from '../components/TextLink.vue';
import InputText from '../components/InputText.vue';
import DefaultButton from '../components/DefaultButton.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import ListDefault from '../components/ListDefault.vue';
import OptionCollapse from '../components/OptionCollapse.vue';
import OptionCollapseParent from '../components/OptionCollapseParent.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import Request from '@/ts/Request';
import Util from '../ts/Util';
import HeaderRelease from '../components/HeaderRelease.vue';
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import ButtonCard from '../components/ButtonCard.vue';

@Component({
    components: { SectionArea, TextLink, InputText, DefaultButton, ModalCodigo, HeaderFluxo, ListDefault, OptionCollapse, OptionCollapseParent, HeaderRelease, DefaultButtonOrd, LoginModal, HeaderCircleButton, ButtonCard }
})

export default class PaymentMethod extends Vue {
cards: any = JSON.parse(localStorage.getItem('CREDIT_CARDS') || '[]');
newCard: any = JSON.parse(localStorage.getItem('NEW_CC') || '{}');
cardsParent: any = localStorage.getItem('CARDS_PARENT') ? JSON.parse(localStorage.getItem('CARDS_PARENT') || '{}') : null;
LAST_NUMBERS: any = 0;
FLAG_CARD: any = '';
CREDIT_CARD_ID: number = -1;

showModal: boolean = false;
modalConfirm: boolean = false;
modalVerify: boolean = false;
msgVerify: any = '';
deliverToBalcony: any = localStorage.getItem('deliverToBalcony');
deliversToTable:  any = localStorage.getItem('deliversToTable');
store: any = JSON.parse(localStorage.getItem('STORE') || '[]');
mainCartao: any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
aceitaCC : any = localStorage.getItem('ACEITA_CC') || '';
storePaymentMethods: any = this.store.paymentMethods;
allPaymentMethods: any = [];
paymentMethodSelected: any = localStorage.getItem('METHOD_SELECTED') || '';
fullMethodSelected: any = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '{}');
showLoginModal: boolean = false;
backButton: any = {iconClass: 'fas', icon:'chevron-left', callback:this.goBack};

mounted() {
    Util.setBackButtonBehavior(this.goBack);
    // this.allPaymentMethods = (this.storePaymentMethods) ? this.storePaymentMethods.filter((x:any)=>{return (!x.PAYMENT_METHOD.startsWith('CC') && !x.PAYMENT_METHOD.startsWith('TEF') && !x.PAYMENT_METHOD.startsWith('PICPAY') && !x.PAYMENT_METHOD.startsWith('EXT'))}) : [];
    this.allPaymentMethods = this.storePaymentMethods;
    if(this.aceitaCC){
        this.getCreditCards();
    }
}

getCreditCards() {
    return new Promise((resolve: Function, reject: Function) => {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route = 'getValidCreditCard';
            req.callbackSuccess = function(response: any) {
                self.cards = response.creditcards;
                if (response.creditcardFromParent) self.cards.push(response.creditcardFromParent);
                self.cardsParent = response.creditcardFromParent;

                if(self.cardsParent) {
                    localStorage.setItem('CARDS_PARENT', JSON.stringify(self.cardsParent));
                }
                else {
                    localStorage.setItem('CARDS_PARENT', '');
                }

                if(self.deliverToBalcony && self.newCard.FLAG) {
                    self.allPaymentMethods.push(self.newCard);
                    localStorage.setItem('NEW_CC', '');
                }

                localStorage.setItem('CREDIT_CARDS', JSON.stringify(self.cards || []));
                this.$forceUpdate();
                resolve();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
        this.fechaModal();
    });
}

deleteCC() {
    try {
        const self = this
        const req = new Request();
        req.body = {
            'CREDITCARD_ID': this.CREDIT_CARD_ID,
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'TOKEN': localStorage.getItem('TOKEN') || ''
        };
        req.route = 'deleteCC';
        req.callbackSuccess = function(response: any) {
            self.fechaModal();
            self.getCreditCards();
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.blockTouchEvents = false;
        req.send();
    } catch (e) {
        console.log(e);
    } 
}

choiceThisPaymentMethod( id:any ){
    const selected = this.allPaymentMethods.find(( payment:any ) => payment.ID == id);
    this.fullMethodSelected = selected;
    /* If selected Method is a card */
    if(this.fullMethodSelected.FLAG) {
        this.paymentMethodSelected = this.fullMethodSelected.ID;
        this.CREDIT_CARD_ID = id;
        this.LAST_NUMBERS = this.fullMethodSelected.LAST_NUMBERS;
        this.FLAG_CARD = this.fullMethodSelected.FLAG;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
        localStorage.setItem('MAIN_CARTAO', JSON.stringify(this.fullMethodSelected)); 
        this.changeMainCard();
    }
    else {
        this.paymentMethodSelected = this.fullMethodSelected.PAYMENT_METHOD;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
    }
    this.goBack();
    localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));
}

lastNumberCard( lastNumbers:any ){
    let lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
    return lastNumber;
}

mostraModal(id: number, cardNumber: number) {
    this.CREDIT_CARD_ID = id;
    this.LAST_NUMBERS = cardNumber;
    this.showModal = true;
}

setSelectedCard( id:number, cardNumber:number, flag:any ) {
    this.CREDIT_CARD_ID = id;
    this.LAST_NUMBERS = this.lastNumberCard(cardNumber);
    this.FLAG_CARD = flag;
    this.fullMethodSelected = this.cards.find(( card:any ) => card.ID == this.CREDIT_CARD_ID);
    localStorage.setItem('MAIN_CARTAO', JSON.stringify(this.fullMethodSelected));
    localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));
    localStorage.setItem('METHOD_SELECTED', JSON.stringify(this.fullMethodSelected.ID));
    
    this.changeMainCard();
    this.goBack();
}

fechaModal() {
    this.showModal = false;
    this.modalConfirm = false;
    this.modalVerify = false;
}

goBack() {
    this.$router.go(-1);
}

goToPedido() {
    this.$router.push({ name: 'Pedido' });
}

goToAddCard() {
    if (localStorage.getItem('USER_ID')) this.$router.push({ name: 'RegisteredCards' });
    else this.openLoginModal();
}

openLoginModal(){
    this.showLoginModal = true;
}

closeLoginModal(){
    this.showLoginModal = false;
}

goToExtrato(){
    this.$router.push({ name: 'Extrato' });
}

changeMainCard() {
    try {
        const self = this
        const req = new Request();
        req.body = {
            'CREDITCARD_ID' : this.CREDIT_CARD_ID || '',
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'NRORG': localStorage.getItem('NRORG')
        };
        req.route = 'setMainCreditCard';
        req.callbackSuccess = function(response: any) {
            self.fullMethodSelected = self.cards.find(( card:any ) => card.ID == self.CREDIT_CARD_ID);
            self.paymentMethodSelected = self.fullMethodSelected.ID
            // localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(self.fullMethodSelected));
            // localStorage.setItem('METHOD_SELECTED', self.paymentMethodSelected);
            self.getCreditCards();
        };
        req.callbackError = function(error: any) {
            console.log(error);
        };
        req.showLoader = false;
        req.blockTouchEvents = false;
        req.send();
    } catch (e) {
        console.log(e);
    }
}

}