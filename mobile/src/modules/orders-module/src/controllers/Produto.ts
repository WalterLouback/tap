import { Component, Prop, Vue } from 'vue-property-decorator'
import ImageHeader from '../components/ImageHeader.vue'
import Card from '../components/Card.vue'
import InputText from '../components/InputText.vue'
import InvisibleHeader from '../components/InvisibleHeader.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import TextLink from '../components/TextLink.vue'
import Product from '../components/Product.vue'
import Option from '../components/Option.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import Util from '../ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import VirtualKeyboard from '@/modules/shared-components/VirtualKeyboard.vue';
import moment from 'moment';


declare var M: any;
declare var $: any;
@Component({
    components: { ImageHeader, Card, InputText, DefaultButtonOrd, SelectField, OptionField, ModalCodigo, InvisibleHeader, TextLink, Product, Option, HeaderCircleButton, HeaderBackground, VirtualKeyboard}
})

export default class Produto extends Vue {
    product: any = JSON.parse(localStorage.getItem('product') || "{}");
    quant: number = 1;
    unitPrice: number = this.product.price;
    price: number = this.product.price;
    showModalRequiredExtras: boolean = false;
    backButton: any         = {iconClass: 'fal', icon: 'chevron-left', callback:this.goBack}
    lastPasswordRequest  : any = localStorage.getItem('LAST_ORDER_PASSWORD_REQUEST');
    store: any = JSON.parse(localStorage.getItem('STORE') || "{}");
    observacaoModal: boolean = false;
    observacaoInput: string = "";

    virtualKeyboard = [{value: "1"},{value: "2"},{value: "3"},{value: "4"},{value: "5"},{value: "6"},{value: "7"},{value: "8"},{value: "9"},{value: "0"},{value: "q"},{value: "w"},{value: "e"},{value: "r"},{value: "t"},{value: "y"},{value: "u"},{value: "i"},{value: "o"},{value: "p"},
    {value: "nan"},{value: "a"}, {value: "s"}, {value: "d"}, {value: "f"}, {value: "g"}, {value: "h"}, {value: "j"}, {value: "k"}, {value: "l"},
    {value: "\u21E7"},{value: "z"}, {value: "x"}, {value: "c"}, {value: "v"}, {value: "b"}, {value: "n"}, {value: "m"}];

    created(){
        this.store = JSON.parse(localStorage.getItem('STORE') || "{}");
        localStorage.removeItem('CHECK')
    }

    mounted() {
        Util.setBackButtonBehavior(() => { });
        localStorage.setItem('quantity', "1");
        localStorage.setItem('currentItem', JSON.stringify({ 
            'productId': this.product.id,
            'extras': [],
            'quant': 1,
            'price': this.product.price
        }));
        window.scrollTo(0, 0);
    }

    addAmount() {
        var oldAmount = Number.parseInt(localStorage.getItem('quantity') || "1");
        if (oldAmount <= 98) {
            var newAmount = oldAmount + 1;
            localStorage.setItem('quantity', newAmount.toString());
            $('#item-amount').val(newAmount);
            this.quant = newAmount;
            this.price = this.unitPrice * this.quant;
        }
    }
    
    removeAmount() {
        var oldAmount = Number.parseInt(localStorage.getItem('quantity') || "1");
        if (oldAmount >= 2) {
            var newAmount = oldAmount - 1;
            localStorage.setItem('quantity', newAmount.toString());
            $('#item-amount').val(newAmount);
            this.quant = newAmount;
            this.price = this.unitPrice * this.quant;
        }
    }

    changeAmount() {
        let newValue = $('#item-amount').val();
        if (newValue >= 1 && newValue <= 99) {
            localStorage.setItem('quantity', newValue);
            this.quant = newValue;
            this.price = this.unitPrice * this.quant;
        } else {
            $('#item-amount').val(localStorage.getItem('quantity'));
        }
    }

    addItemToOrder() {
        if (this.allRequiredExtrasAreChecked()) {
            let order      = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
            let item       = JSON.parse(localStorage.getItem('currentItem') || "{}");
            let lastItemId: number = Number.parseInt(localStorage.getItem('lastItemId') || "-1");
            item.quant = localStorage.getItem('quantity');
            item.productName = this.product.name;
            item.estimatedTime = this.product.estimatedTime;
            item.image = this.product.image;
            item.itemId = lastItemId + 1;
            item.note   = $("#observacao").val();
            item.totalItem = item.quant * item.price;
            localStorage.setItem('lastItemId', item.itemId);
            order.ORDER_ITEMS.push(item);
            order.TOTAL_VALUE += item.price * item.quant;
            order.TOTAL_VALUE = parseFloat(order.TOTAL_VALUE.toFixed(2));
            order.AMOUNT_OF_ITEMS = Number.parseInt(order.AMOUNT_OF_ITEMS) + Number.parseInt(item.quant);
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(order));
            this.goBack();
        } else {
            this.mostraModalRequiredExtras();
        }
    }

    addItemToOrderAndFinish() {
        if (this.allRequiredExtrasAreChecked()) {
            let order      = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
            let item       = JSON.parse(localStorage.getItem('currentItem') || "{}");
            let lastItemId: number = Number.parseInt(localStorage.getItem('lastItemId') || "-1");
            item.quant = localStorage.getItem('quantity');
            item.productName = this.product.name;
            item.estimatedTime = this.product.estimatedTime;
            item.image = this.product.image;
            item.itemId = lastItemId + 1;
            item.note   = $("#observacao").val();
            localStorage.setItem('lastItemId', item.itemId);
            order.ORDER_ITEMS.push(item);
            order.TOTAL_VALUE += item.price * item.quant;
            order.AMOUNT_OF_ITEMS = Number.parseInt(order.AMOUNT_OF_ITEMS) + Number.parseInt(item.quant);
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(order));
            if(localStorage.getItem('DELIVER_AT'))this.$router.push('Pedido');
            else {
                if(this.deliversOnlyToBalcony()){
                    localStorage.setItem('DELIVER_AT', 'B')
                    this.$router.push('Pedido');
                }
                else {
                    localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM','Produto');
                    this.$router.push('DeliverLocation');
                }
            }

        } else {
            this.mostraModalRequiredExtras();
        }
    }
    
    deliversOnlyToBalcony():boolean {
        if(!localStorage.getItem('deliversToTable') || localStorage.getItem('deliversToTable')=='false' || localStorage.getItem('deliversToTable')=='undefined') return true;
        else return false;
    }

    deliversOnlyToTable():boolean {
        if(!localStorage.getItem('deliversToBalcony') || localStorage.getItem('deliversToBalcony')=='false' || localStorage.getItem('deliversToBalcony')=='undefined') return true;
        else return false;
    }

    allRequiredExtrasAreChecked():boolean {
        let requiredExtras = this.product.extras.filter((extra: any) => extra.required == true);
        let checkedExtras  = JSON.parse(localStorage.getItem('currentItem') || "{}").extras;
        for (let i in requiredExtras) {
            let isChecked = checkedExtras.find((extra: any) => {
                return extra.extraId == requiredExtras[i].id;
            });
            if (!isChecked) {
                const idElementScroll = 'extraName'+requiredExtras[i].id; 
                $('#extraName'+requiredExtras[i].id).removeClass('primary-color');
                $('#extraName'+requiredExtras[i].id).addClass('color1');
                $('#required'+requiredExtras[i].id).addClass('pls-require');
                // this.goToLocation(idElementScroll);
                return false;
            }
        }
        return true;
    }
    
    mostraModalRequiredExtras() {
        this.showModalRequiredExtras = true;
    }

    fechaModalRequiredExtras() {
        this.showModalRequiredExtras = false;
    }

    optionChange(optionId: number, optionName: string,  optionPrice: number, extraId: number, extraIsMultiple: boolean, target: any, elemento: any ) {
        let isChecked   = $(`#option-${optionId}`)[0].checked;
        let currentItem = JSON.parse(localStorage.getItem('currentItem') || "{}");
        target = document.getElementById('quadroExtras' + extraId)
        elemento = document.getElementsByClassName('extras-list')
        let posicao = target.offsetHeight + target.offsetTop
        setTimeout(() => {
            if(localStorage.getItem('CHECK') == 'check') {
                localStorage.removeItem('CHECK')
                $(elemento).animate({
                    scrollTop: posicao
                }, 500)
            }
        }, 300);
        if (isChecked) {
            currentItem = this.addOptionToCurrentItem(optionId, optionName, extraId, extraIsMultiple, currentItem, optionPrice);
            $('#extraName'+extraId).removeClass('color1');
            $('#required'+extraId).removeClass('pls-require');
            $('#extraName'+extraId).addClass('primary-color');
        } else {
            currentItem = this.removeOptionFromCurrentItem(optionId, currentItem, optionPrice);
        }
        localStorage.setItem('currentItem', JSON.stringify(currentItem));
    }

    goToLocation( idElement:any ){
        const offset = -45;
        $('html, body').animate({
            scrollTop: $("#"+idElement).offset().top + offset
        }, 300);
    }

    addOptionToCurrentItem(id:number, name: string, extraId:number, extraIsMultiple:boolean, currentItem:any, price: number) {
        if (!currentItem.extras) currentItem.extras = [];
        let objToPush:any   = { extraId, price: price, selectedOptions: [ { id, name, price } ] };
        let placeToPush:any = currentItem.extras;
        let extra = currentItem.extras.find((extra:any) => extra.extraId == extraId);
        
        /* If the extra was already added to the item */
        if (!!extra) {
            /* If the extra is a radio, reset the current price of the extra */
            if (!extraIsMultiple) { extra.selectedOptions = []; currentItem.price -= extra.price; extra.price = price; }
            /* Define the new place to push the option */
            objToPush   = { id, name, price };
            placeToPush = extra.selectedOptions;
        }

        currentItem.price += price;
        this.unitPrice = currentItem.price;
        this.price = currentItem.price * this.quant;
        placeToPush.push(objToPush);
        return currentItem;
    }

    removeOptionFromCurrentItem(id: number, currentItem: any, price: number) {
        /* Iterate through the extras */
        for (var e in currentItem.extras) {
            let extra = currentItem.extras[e];
            /* Iterate through the options */
            for (var i in extra.selectedOptions) {
                /* Find the option to remove */
                if (extra.selectedOptions[i].id == id) {
                    /* If it's not the last option from the extra: */
                    if (extra.selectedOptions.length > 1) {
                        /* Remove the option and decrement the price from the extra */
                        extra.price -= price;
                        extra.selectedOptions.splice(i, 1);
                    } else {
                        /* If it's the last option from the extra, remove the extra */
                        currentItem.extras.splice(e, 1);
                    }
                    /* Decrement price from item */
                    currentItem.price -= price;
                }
            }
        }
        /* Decrement price from class attribute */
        this.unitPrice = currentItem.price;
        this.price     = currentItem.price * this.quant;
        return currentItem;
    }

    goBack() {
        this.$router.go(-1);
    }

    focusInput( id:any ){
        const offset = -60;
        const el: any = $("#"+id);
        setTimeout(function(){
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        },150);
    }

    handleVirtualKeyPress(digit: string){
        switch(digit){  
            case "\u232B": 
                this.popCharacter()
                break;
            case "\u21E7":
                this.alterarTeclado(digit)
                break;
            case "\u21EA":
                this.alterarTeclado(digit)
                break;
            default:
                this.pushCharacter(digit)
                console.log(digit)
                break;
        }
    }

    pushCharacter(digit: string) {
        this.observacaoInput=$('#observacao').val()+digit
        $('#observacao').val(this.observacaoInput)
    }

    popCharacter() {            
        this.observacaoInput=this.observacaoInput.substring(0, this.observacaoInput.length - 1);
        $('#observacao').val(this.observacaoInput)
    }

    alterarTeclado(digit: string){
        if(digit == "\u21E7"){    
            this.virtualKeyboard = [{value: "1"},{value: "2"},{value: "3"},{value: "4"},{value: "5"},{value: "6"},{value: "7"},{value: "8"},{value: "9"},{value: "0"},{value: "Q"},{value: "W"},{value: "E"},{value: "R"},{value: "T"},{value: "Y"},{value: "U"},{value: "I"},{value: "O"},{value: "P"},
            {value: "nan"},{value: "A"}, {value: "S"}, {value: "D"}, {value: "F"}, {value: "G"}, {value: "H"}, {value: "J"}, {value: "K"}, {value: "L"},
            {value: "\u21EA"},{value: "Z"}, {value: "X"}, {value: "C"}, {value: "V"}, {value: "B"}, {value: "N"}, {value: "M"}];
        
        }else if(digit == "\u21EA"){
            this.virtualKeyboard = [{value: "1"},{value: "2"},{value: "3"},{value: "4"},{value: "5"},{value: "6"},{value: "7"},{value: "8"},{value: "9"},{value: "0"},{value: "q"},{value: "w"},{value: "e"},{value: "r"},{value: "t"},{value: "y"},{value: "u"},{value: "i"},{value: "o"},{value: "p"},
            {value: "nan"},{value: "a"}, {value: "s"}, {value: "d"}, {value: "f"}, {value: "g"}, {value: "h"}, {value: "j"}, {value: "k"}, {value: "l"},
            {value: "\u21E7"},{value: "z"}, {value: "x"}, {value: "c"}, {value: "v"}, {value: "b"}, {value: "n"}, {value: "m"}]; }
    }
}