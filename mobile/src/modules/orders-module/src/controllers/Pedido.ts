import { Component, Prop, Vue } from 'vue-property-decorator'
import ImageHeader from '../components/ImageHeader.vue'
// import Card from '../components/Card.vue'
import InputText from '../components/InputText.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import TextLink from '../components/TextLink.vue'
import Product from '../components/Product.vue'
import Option from '../components/Option.vue'
import OrderItem from '../components/OrderItem.vue'
import SelectFieldController from '../ts/SelectFieldController'
import Request from '@/ts/Request'
import DefaultButton from '../components/DefaultButton.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import moment from 'moment';
import BiometryCheck from '../ts/BiometryCheck';
import Util from '@/ts/Util';
import HeaderRelease from '../components/HeaderRelease.vue';
import Orders from '../../Orders';
import LoadingPhases from '@/modules/shared-components/LoadingPhases.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import ButtonCard from '../components/ButtonCard.vue';
import Minicard from '@/modules/shared-components/Minicard.vue';
import SCLoadingDots from '@/modules/shared-components/SCLoadingDots.vue';
import VirtualKeyboardCPF from '@/modules/shared-components/VirtualKeyboardCPF.vue';
import VirtualKeyboard from '@/modules/shared-components/VirtualKeyboard.vue';
import { throws } from 'assert';
import { faFileContract } from '@fortawesome/pro-light-svg-icons';
import { getElement } from 'web-social-share/dist/types/stencil-public-runtime'
import { component } from 'vue/types/umd'
import router from '@/router'
declare var M: any;
declare var $: any;
declare var KioskPlugin: any;

declare var Card: any;
@Component({
    components: { ImageHeader, InputText, DefaultButtonOrd, DefaultButton, SelectField, OptionField, ModalCodigo, HeaderFluxo, TextLink, Product, Option, OrderItem, HeaderRelease, LoadingPhases, LoginModal, HeaderCircleButton, ButtonCard, Minicard, SCLoadingDots, VirtualKeyboardCPF, VirtualKeyboard }
})

export default class Pedido extends Vue {
    order: any = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
    paymentMethods: Array<object> = JSON.parse(localStorage.getItem('ACCEPTED_PAYMENT_METHODS') || '[]');
    storePaymentMoment: string | null = localStorage.getItem('storePaymentMoment');
    structures: Array<object> = [];
    mainCartao: any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
    allPaymentMethods: any = JSON.parse(localStorage.getItem('ALL_PAYMENT_METHODS') || '[]');
    paymentMethodSelected: any = localStorage.getItem('METHOD_SELECTED') || '';
    fullMethodSelected: any = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '{}');
    structureSelects: Array<Array<object>> = [];
    itemToRemove: any = {};
    mainCreditcard: any = JSON.parse(localStorage.getItem('USER_DATA') || '{}').MAIN_CARD_ID;
    passwordUser: any = localStorage.getItem('QBTT');
    store: any = JSON.parse(localStorage.getItem('STORE') || '[]');
    passwordValue: any = '';
    lastPasswordRequest: any = localStorage.getItem('LAST_ORDER_PASSWORD_REQUEST');
    PAYMENT_METHOD: any;
    STRUCTURE_ID: any;
    alreadyHasStructure: boolean = false;
    lastStructure: any = localStorage.getItem('STRUCTURE_ID') || '';
    currentStructure: any = localStorage.getItem('currentStructure');
    lastDayOfLastStructure: any = localStorage.getItem('LAST_DAY_OF_LAST_STRUCTURE') || '';
    getOrderDataInterval: any;
    exitCounter: any = 0;
    orderP: any;
    barCode: any;
    otherButton: any = [{ iconClass: 'fal', colorClass: 'font-primary', icon: 'times', callback: () => { this.limpar() } }];
    showAvailable: boolean = false;
    lastOrder: any = JSON.parse(localStorage.getItem('LAST_ORDER') || "{}");
    instantAprove:boolean=false;
    modalAproximacao:boolean=false;
    /* Modais */
    showModalAproximaçao:boolean=false;
    showModalSucesso: boolean = false;
    showModalErro: boolean = false;
    showModalRemoverItem: boolean = false;
    showModal: boolean = false;
    showModalTelaCartoes: boolean = false;
    modalPasswordRequest: boolean = false;
    passwordWrong: boolean = false;
    orderSuccess: boolean = false;
    selectPaymentMethod: boolean = false;
    errorMessage: string = '';
    iditem: number = 1;
    showModalConfirmPaymentMethod: boolean = false;
    showModalChoosePaymentMethod: boolean = localStorage.getItem('ORDER_TICKET_DATA')?true:false;
    showModalCardPayment: boolean = false;
    showModalPaymentCompleted: boolean = false;
    showModalChangePaymentPassword: boolean = false;
    showModalChangePaymentCard: boolean = false;
    showModalAddPhone: boolean = false;
    showModalLoadingCreateOrder: boolean = false;
    showModalLoadingFinishOrder: boolean = false;
    showModalConfirmAddCpf: boolean = false;
    showModalAddCpf: boolean = false;
    showModalConfirmClear: boolean = false;
    retirarDoCarrinho: boolean = false;
    showModalAddName: boolean = false;
    inputNome: string = "";
    nameAdded: boolean = false;
    noteCont: number = 0;
    showModalAddObs:boolean=false;
    taaName:string=JSON.parse(localStorage.getItem('TAA_SELECTED') || "{}").name;
    barcodeComandas:string = JSON.parse(localStorage.getItem('TICKET_BARCODE') ||'');
    modalAddEmail:boolean=false;
    emailInput:string=""
    modalConfirmAddEmail:boolean=false;
    hasError:boolean=false;
    modalExitPedido:boolean=false;
    /* Locais de entrega escolhidos pela loja */
    checkDeliversToTable: string = (localStorage.getItem('deliversToTable') && localStorage.getItem('deliversToTable') == 'true') ? 'true' : 'false'
    checkDeliversToTableQR: string = (localStorage.getItem('deliversToTable') && localStorage.getItem('deliversToTable') == 'true') ? 'true' : 'false'
    checkDeliversToBalcony: string = (localStorage.getItem('deliversToBalcony') == 'true' || localStorage.getItem('deliversToBalcony') == 'undefined') ? 'true' : 'false'
    deliversToTable: string = JSON.parse(this.checkDeliversToTable);
    deliversToTableQR: string = JSON.parse(this.checkDeliversToTable);
    deliversToBalcony: string = JSON.parse(this.checkDeliversToBalcony);

    /* Opções escolhidas até o momento */
    selectedDeliverLocation: string = localStorage.getItem('DELIVER_AT') || '';
    hasMoreDeliverLocations: boolean = false;
    currentPaymentMethod: string = "";
    selectedStructure: any = null;
    acceptFinger: any = localStorage.getItem('ACCEPT_BIOMETRY') || '';
    biometry: any = new BiometryCheck();
    showModalLogin: boolean = false;
    backButton: any = { iconClass: 'fal', icon: 'chevron-left', callback: this.goBack }
    lp: any = LoadingPhases
    userCards: any = [];
    reqFailure:number=0
    /*Imgs*/
    cardPaymentImg: string = Util.getImgUrl('stone', '.png');
    askNoteImg: string = Util.getImgUrl('asknote', '.png');
    receiptImg: string = Util.getImgUrl('invoice', '.png');
    paymentMethodsImg: string = Util.getImgUrl('cards-payment', '.png');
    paymentMainCardImg: string = Util.getImgUrl('card-isolated', '.png');
    greenSymbol: string = Util.getImgUrl('stone-symbol', '.png');
    tickets: any = JSON.parse(localStorage.getItem("TICKETS") || "{}")
    contErro:number=0
    /*Timer de desativação*/
    logoutTimer: any = {};
    showLogoutMessageTimer: any = {};
    logoutRemainingTime: number = 30;
    logoutTimerStart: boolean = false;
    processedOrder: any = {};
    priceFormatter: any = new Intl.NumberFormat('pt-BR', { minimumFractionDigits: 2 });
    password: any;
    produtoNotFound: boolean = false;
    finishedOrder:boolean = false
    /*flag para desativar transação*/
    preTransactionActive: string = "";

    loginProps: any = JSON.parse(localStorage.getItem('LOGIN_PROPS') ||
        JSON.stringify({
            loginMethods: ['PHONE'],
            introductionMessage: 'Bem-vindo ao BipFun! Entre no app e ',
            introductionMessageHighlight: 'compre sem enfrentar filas.',
            logo: Util.getImgUrl('bipfun-logo', '.svg'),
            nrorg: process.env.VUE_APP_NRORG || '0'
        })
    );

    logo: string = '';
    currentCard: number = (this.mainCartao.ID) ? this.mainCartao.ID : -1;
    virtualKeyboard = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" }, { value: "7" }, { value: "8" }, { value: "9" }, { value: "0" }, { value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
    { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
    { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];


    virtualKeyboardCpf = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" },
    { value: "7" }, { value: "8" }, { value: "9" }, { value: "C" }, { value: "0" },];
    userPhone = "";
    userCpf = "";
    structureData: any = {};
    structureAddress: any = null;
    sendStoneOrder: any = null;
    showModalCpfInvalido: boolean = false;
    msgModal: any
    modal: boolean = false
    itemToAdd: any
    showModalConfirmNote: boolean = false;
    showModalConfirmNoteV: boolean = false;
    posIsAvailable: boolean = true;
    loadedPos: boolean = true;
    timer: string = JSON.stringify(localStorage.getItem("TIMER_TAA"))
    posStatus: any
    posInterval: any = undefined
    noteContinterval: any
    hasLast: boolean=false;
    cont:number=0; 
    availableValid: boolean=false;
    posIntervalB: any;
    processingCreate: boolean=false;
    showModalIntegracao:boolean=false;
    TRANSACTION_ID: any;
    obsId: number=0;
    quantityItem: any = this.order.AMOUNT_OF_ITEMS;
    character: number = 0;
    formatedOrder: any;
    apiUrl: string | undefined;
    apiRequest: { barcode: string; method_payment: string; subtotal: any; discount: string; total: any}  | undefined;
    modalLoadingFechamento:boolean=false;
    validaVoltar: boolean=false;
    created() {
        localStorage.setItem('atualizar', 'false');
        localStorage.setItem('FROM_PEDIDO','true')
        this.userCards = (localStorage.getItem('ALL_CC')) ? JSON.parse(localStorage.getItem('ALL_CC') || '[]') : [];
        this.logo = Util.getImgUrl(this.loginProps.logo.split('.')[0], '.' + this.loginProps.logo.split('.')[1]);
        this.structureData = JSON.parse(localStorage.getItem('STRUCTURE_DATA') || "{}") || {};
        if (this.structureData.addressData) {
            this.structureAddress = this.structureData.addressData;
            if (this.structureAddress && this.structureAddress.PROVINCY && this.structureAddress.PROVINCY.length != 2) this.structureAddress.PROVINCY = "  ";
            if (this.structureAddress && this.structureAddress.CEP && this.structureAddress.CEP.length != 9) this.structureAddress.CEP = "00000-000"
        }
    }

    mounted() {
        Util.setBackButtonBehavior(() => { });
        let self = this;
        // console.log(this.lastOrder.TOTAL_VALUE)
        this.refreshDeliverOptions();

        if (!!this.lastStructure && !!this.currentStructure && moment().date().toString() == this.lastDayOfLastStructure) {
            this.alreadyHasStructure = true;
        }

        /* Tratando formas de pagamento */
        var posCC = (this.paymentMethods) ? this.paymentMethods.findIndex((paymentMethod: any) => paymentMethod.PAYMENT_METHOD == 'CC') : -1;
        if (posCC != -1) {
            /* Carregar cartões do usuário no select caso CC seja uma das formas de pagamento aceitas */
            this.paymentMethods.splice(posCC, 1);
            //this.getCreditCards();
        }

        if (!this.allPaymentMethods || this.allPaymentMethods.length == 0) {
            this.allPaymentMethods = this.store ? this.store.paymentMethods : [];
            if (this.allPaymentMethods.length) {
                this.allPaymentMethods.sort((a: any, b: any) => (a.LABEL > b.LABEL) ? 1 : -1);
                this.allPaymentMethods = this.allPaymentMethods.filter((p: any) => p.PAYMENT_METHOD != 'CC' && !p.LABEL.includes('Dinheiro') && !p.LABEL.includes('dinheiro'));
            }
        }

        if (this.allPaymentMethods.length) {
            this.allPaymentMethods.sort((a: any, b: any) => (a.LABEL > b.LABEL) ? 1 : -1);
            this.allPaymentMethods = this.allPaymentMethods.filter((p: any) => p.PAYMENT_METHOD != 'CC' && !p.LABEL.includes('Dinheiro') && !p.LABEL.includes('dinheiro'));
        }

        /* EventListeners */
        Util.setAddPedido((event: any) => {
            const keyName = event.key;
            if (keyName == 'Enter') {
                this.quantityItem = this.quantityItem + 1
                setTimeout(() => {
                    if (this.quantityItem != this.order.AMOUNT_OF_ITEMS) {
                       this.produtoNotFound = true
                    }
                }, 300);
                setTimeout(() => { this.order = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}"); }, 50)
                setTimeout(() => {
                    var idLastProduct: any = localStorage.getItem('LAST_PRODUCT');
                    var element: any = document.getElementById('#' + idLastProduct);
                    if (element) element.scrollIntoView({ block: "start" });
                }, 150);
            }
        })

        $('#entregar-balcao').click(this.setEntregarNoBalcao);
        $('#entregar-mesa').click(this.setEntregarNaMesa);
        $('#entregar-mesa-qr').click(this.setEntregarNaMesa);
        this.getMaxEstimatedTime();
        this.lojaAberta();

    }

    refreshDeliverOptions() {
        if (!!this.selectedDeliverLocation) {
            if (this.selectedDeliverLocation === 'T') {
                if (this.deliversToTable) {
                    this.getStructure(Number(localStorage.getItem('structureId')));
                    this.hasMoreDeliverLocations = true;
                } else if (this.deliversToBalcony) this.selectedDeliverLocation = 'B';
                else this.selectedDeliverLocation = '';
            }
            else if (this.selectedDeliverLocation === 'B') {
                if (this.deliversToBalcony) {
                    if (this.deliversToTable) this.hasMoreDeliverLocations = true;
                } else if (this.deliversToTable) this.selectedDeliverLocation = 'T';
                else this.selectedDeliverLocation = '';
            }
        }
    }

    sortedPaymentMethods() {

    }

    switchDeliverLocation() {
        this.$router.push('DeliverLocation');
    }

    telaPaymentMethods() {
        if (!localStorage.getItem('USER_ID')) {
            this.openModalLogin();
        } else {
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));
            this.$router.push('PaymentMethod');
        }
    }
    testeNome() {
        var nrorg = localStorage.getItem('NRORG') || 0;
        this.showModalConfirmAddCpf = false

        if (this.store.askName) {
            this.showModalAddName = true
        }
        else this.cpfadded()

    }

    telaAddCard() {
        this.$router.push('AddCard');
    }

    addProdutos() {
        this.$router.push('Produtos');
        localStorage.getItem('ORDER_TICKET_DATA')?true:false
    }

    goBack() {
        localStorage.getItem('ORDER_TICKET_DATA')?true:false
        this.$router.go(-1);
    }

    setEntregarNoBalcao() {
        this.selectedDeliverLocation = 'B';
        localStorage.setItem('deliverToBalcony', 'true');
        this.refreshDeliverOptions();
    }

    setEntregarNaMesa() {
        this.selectedDeliverLocation = 'T';
        localStorage.setItem('deliverToBalcony', 'false');
        this.refreshDeliverOptions();
        this.goToScanQr();
    }

    clickItem(itemId: number) {
        this.itemToRemove = this.order.ORDER_ITEMS.find((item: any) => item.itemId == itemId);
        this.removeItem();
    }
    clickSub(itemId: number) {
        this.itemToRemove = this.order.ORDER_ITEMS.find((item: any) => item.itemId == itemId);
        this.subItem();
    }
    clickAdd(itemId: number) {
        this.itemToAdd = this.order.ORDER_ITEMS.find((item: any) => item.itemId == itemId);
        this.addItem(itemId);
    }

    isInAppPayment() {
        return $.isNumeric(this.currentPaymentMethod);
    }

    limpar() {
        this.showModalConfirmClear = true;
    }

    confirmClear() {
        localStorage.setItem('CURRENT_ORDER', '');
        this.order = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
        this.showModalConfirmClear = false;
        this.$router.go(-1);
    }

    removeItem() {
        let index = this.order.ORDER_ITEMS.findIndex((item: any) => item.itemId == this.itemToRemove.itemId);
        this.order.TOTAL_VALUE -= this.itemToRemove.price * this.itemToRemove.quant;
        this.order.TOTAL_VALUE = parseFloat(this.order.TOTAL_VALUE.toFixed(2));
        this.order.AMOUNT_OF_ITEMS -= this.itemToRemove.quant;
        this.order.ORDER_ITEMS.splice(index, 1);
        localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));
        this.fechaModalRemoverItem();
        if (this.order.ORDER_ITEMS.length == 0) this.$router.go(-1);
    }

    subItem() {
        if (this.itemToRemove.quant > 1) {
            let index = this.order.ORDER_ITEMS.findIndex((item: any) => item.itemId == this.itemToRemove.itemId);
            this.order.TOTAL_VALUE -= this.itemToRemove.price;
            this.order.TOTAL_VALUE = parseFloat(this.order.TOTAL_VALUE.toFixed(2));
            this.order.AMOUNT_OF_ITEMS--;
            this.itemToRemove.quant--;
            this.order.ORDER_ITEMS[index].totalItem = this.itemToRemove.quant * this.itemToRemove.price;
            //  console.log(this.order.ORDER_ITEMS[index].totalItem)

            // this.order.ORDER_ITEMS.splice(index, 1); 
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));

            // if (this.order.ORDER_ITEMS.length == 0) this.$router.go(-1);
        } else { this.removeItem(); }
    }
    addItem(itemId:any) {
        let index = this.order.ORDER_ITEMS.findIndex((item: any) => item.itemId == this.itemToAdd.itemId);
        var item: any = this.order.ORDER_ITEMS.find((prod: any) => prod.itemId == itemId);
      //  console.log(itemId)
        // item.quant = Number(item.quant) + 1;
        //item.productName = product.name;
       // item.price = product.price;
        //item.estimatedTime = product.estimatedTime;
        //item.image = product.image;
        //item.itemId = lastItemId + 1;
        item.totalItem = (item.quant+1) * item.price;
        this.order.TOTAL_VALUE += this.itemToAdd.price;
        this.order.TOTAL_VALUE = parseFloat(this.order.TOTAL_VALUE.toFixed(2));
        this.order.AMOUNT_OF_ITEMS++;
        this.order.ORDER_ITEMS[index]=item;
     //   console.log(this.order.ORDER_ITEMS[index].totalItem)
        this.itemToAdd.quant++;
        // this.order.ORDER_ITEMS.splice(index, 1); 
        this.order.ORDER_ITEMS[index].totalItem = this.itemToAdd.quant * this.itemToAdd.price;
        localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));
        // if (this.order.ORDER_ITEMS.length == 0) this.$router.go(-1);
    }
    addObs(id:number){
      //  console.log(id)
        this.obsId =id;
        this.showModalAddObs=true
    }
    structureOptionChange(e: any) {
        let selectIndex: number = Number.parseInt(e.target.name);
        if (this.structureSelects.length > selectIndex + 1) {
            this.structureSelects.splice(selectIndex + 1, this.structureSelects.length - selectIndex - 1);
        }
        this.getStructures(e.target.value, selectIndex);
    }

    choicePaymentMethod(methodId: any) {
        // const method:any = this.allPaymentMethods.find(( payment:any ) => 
        //     (payment.ID == methodId) && !(payment.PAYMENT_METHOD.startsWith('CC') || payment.PAYMENT_METHOD.startsWith('TEF') || payment.PAYMENT_METHOD.startsWith('PICPAY') || payment.PAYMENT_METHOD.startsWith('EXT') ));
        const method: any = this.allPaymentMethods.find((payment: any) => (payment.ID == methodId));

        this.fullMethodSelected = method;
        if (method.PAYMENT_METHOD) {
            this.paymentMethodSelected = method.PAYMENT_METHOD;
        } else { this.paymentMethodSelected = method.ID }

        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
        localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));

        this.selectPaymentMethod = false;
    }

    getStructures(parent: number, selectIndex: number) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "PARENT_ID": parent
            };
            req.route = 'getStructures';
            req.callbackSuccess = function (response: any) {
                let structures = response.structures;
                if (self.structureSelects.length > selectIndex + 1) {
                    self.structureSelects.splice(selectIndex + 1, self.structureSelects.length - selectIndex - 1);
                }
                if (structures.length > 0) {
                    self.structureSelects.push(structures);
                    SelectFieldController.refresh();
                }
            };
            req.callbackError = function (error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    getStructure(structureId: number) {
        try {
            if (!localStorage.getItem('structureId')) return;
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_ID": structureId
            };
            req.route = 'getStructureById';
            req.callbackSuccess = function (response: any) {
                self.selectedStructure = response.structure;
                self.currentStructure = self.selectedStructure.name;
                self.alreadyHasStructure = true;
                localStorage.setItem('currentStructure', self.selectedStructure.name);
                localStorage.setItem('STRUCTURE_ID', self.selectedStructure.topLevelStructure.id);
            };
            req.callbackError = function (error: any) {
                localStorage.setItem('structureId', '');
                localStorage.setItem('deliverToBalcony', 'true');
                localStorage.setItem('currentStructure', '');
                self.currentStructure = '';
                self.alreadyHasStructure = false;
                self.showModal = true;
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    getCreditCards() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "NRORG": localStorage.getItem('NRORG')
            };
            req.route = 'getValidCreditCard';
            req.callbackSuccess = function (response: any) {
                /* Variável que armazena todos os cartões do usuário e de seu titular (caso aplique) */
                let allCards = response.creditcards;
                self.userCards = response.creditcards;
                if (response.creditcardFromParent) allCards.push(response.creditcardFromParent);

                /* Identificando cartão principal */
                if (!self.mainCartao.ID) self.mainCartao = {};
                allCards.forEach((card: any) => {
                    if (card.IS_MAIN_CARD == true && !self.mainCartao.ID)
                        self.mainCartao = card;
                    self.currentCard = card.ID;
                });

                /* Variável que armazena os métodos de pagamento que a loja aceita unidos com os cartões do usuário (Caso a loja aceite cartão) */
                self.allPaymentMethods = [].concat(self.store.paymentMethods);

                /* Variável que armazena o indíce de 'CC' nos métodos de pagamento */
                let aceitaCC = self.allPaymentMethods.findIndex((paymentMethod: any) => paymentMethod.PAYMENT_METHOD == 'CC');

                /* Carregar cartões do usuário no select caso CC seja uma das formas de pagamento aceitas */
                if (aceitaCC != -1) {
                    self.allPaymentMethods.splice(aceitaCC, 1);
                    self.allPaymentMethods = allCards.concat(self.allPaymentMethods);
                }

                /* Caso método de pagamento atual não exista, limpar variável */
                self.handleNonExistantPaymentMethod();
                /* Caso cartão principal exista, definí-lo como método escolhido */
                if (self.mainCartao.FLAG)
                    self.setMainCardAsSelectedMethod();
                /* Se não, método de pagamento será o que consta no local storage */
                else if (!!localStorage.getItem('FULL_METHOD_SELECTED') && localStorage.getItem('FULL_METHOD_SELECTED') != '{}')
                    self.setMainPaymentMethodFromLocalStorage();
                /* Se não definir método de pagamento como o primeiro */
                else if (self.allPaymentMethods[0])
                    self.setFirstPaymentMethodAsSelected();
                /* Se não existir nenhum método de pagamento, limpar variáveis */
                else {
                    localStorage.setItem('METHOD_SELECTED', '');
                    localStorage.setItem('FULL_METHOD_SELECTED', '{}');
                }

                localStorage.setItem('ALL_CC', JSON.stringify(allCards));
                localStorage.setItem('MAIN_CARTAO', JSON.stringify(self.mainCartao));
                self.allPaymentMethods.sort((a: any, b: any) => (a.LABEL > b.LABEL) ? 1 : -1);
                localStorage.setItem('ALL_PAYMENT_METHODS', JSON.stringify(self.allPaymentMethods));
                localStorage.setItem('ACEITA_CC', JSON.stringify(aceitaCC));
                setTimeout(() => self.buildCardView(), 100);
            };
            req.callbackError = function (error: any) {
                console.log(error);
            }
            req.showLoader = true;
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) { req.send(); }
        } catch (e) {
            console.log(e);
        }
    }

    handleNonExistantPaymentMethod() {
        let lasBalconyMethodSelected;
        if (this.fullMethodSelected && this.fullMethodSelected.FLAG) {
            lasBalconyMethodSelected = this.allPaymentMethods.find((method: any) => method.ID == this.paymentMethodSelected);
            if (!lasBalconyMethodSelected) {
                localStorage.setItem('METHOD_SELECTED', '');
                localStorage.setItem('FULL_METHOD_SELECTED', '{}');
            }
        } else if (this.fullMethodSelected && this.fullMethodSelected.PAYMENT_METHOD) {
            lasBalconyMethodSelected = this.allPaymentMethods.find((method: any) => method.PAYMENT_METHOD == this.paymentMethodSelected);
            if (!lasBalconyMethodSelected) {
                localStorage.setItem('METHOD_SELECTED', '');
                localStorage.setItem('FULL_METHOD_SELECTED', '{}');
            }
        }
    }

    setMainCardAsSelectedMethod() {
        if (!this.fullMethodSelected || !this.paymentMethodSelected) {
            this.fullMethodSelected = this.mainCartao;
            this.paymentMethodSelected = this.mainCartao.ID;
            localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
            localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));
        }
    }

    setMainPaymentMethodFromLocalStorage() {
        this.fullMethodSelected = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '');
        this.paymentMethodSelected = this.fullMethodSelected.PAYMENT_METHOD;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
    }

    setFirstPaymentMethodAsSelected() {
        // if(!(this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('CC') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('TEF') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('PICPAY') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('EXT') )){
        this.fullMethodSelected = this.allPaymentMethods[0];
        this.paymentMethodSelected = this.fullMethodSelected.PAYMENT_METHOD;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
        localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));
        // } 
    }

    getPosReferenceId(posSerialNumber: any) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            let pos = response.available_pos_list.find((pos: any) => { return pos.pos_serial_number == posSerialNumber });
            if (!pos) {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.clearModal();
                this.showModalErro = true;
            }
            else {
                localStorage.setItem("POS_REFERENCE_ID", pos.pos_reference_id);
                this.prepareOrder(null, this.paymentMethodSelected, true);
            }
        };
        req.callbackError = (error: any) => {
            if (error.success == undefined) this.getOrderDataInterval = setTimeout(() => this.getPosReferenceId(posSerialNumber), 5000);
            else {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.clearModal();
                this.showModalErro = true;
            }
        };
        req.body = localStorage.getItem('STONE_ESTABLISHMENT_ID');
        req.getAvailablePos();
    }

    getUserData() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'USER_TYPE_ID': 4,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
            };
            req.route = 'getUserData';
            req.callbackSuccess = (response: any) => {
                this.getPosReferenceId(response.user.POS);
            };
            req.callbackError = (error: any) => {
                this.getOrderDataInterval = setTimeout(() => this.getUserData(), 5000);
            };
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
        } catch (e) {

            console.log(e);
        }
    }

    mostraModalSucesso() {
        this.showModalSucesso = true;
    }

    fechaModalSucesso() {
        localStorage.setItem('SHOW_EXPLORAR_OVERLAY', 'true');
        localStorage.setItem('CURRENT_ORDER', '');
        this.showModalSucesso = false;
        this.showModalPaymentCompleted = false;
        clearTimeout(this.logoutTimer);
        clearTimeout(this.showLogoutMessageTimer);
        // localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido')
        // this.$router.push('PedidosRealizados');
       // document.location.reload();
        this.$router.push('Explorar');
        
    }

    fechaModalSucessoERetornaParaCompras() {
        this.showModalSucesso = false;
        this.showModalPaymentCompleted = false;
        clearTimeout(this.logoutTimer);
        clearTimeout(this.showLogoutMessageTimer);
        localStorage.setItem('CURRENT_ORDER', '');
        // localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido')
        // this.$router.push('PedidosRealizados');
        this.$router.push('Explorar');
    }

    changePaymentMethod(e: any) {
        // this.currentPaymentMethod = e.target.value;
        console.log(e);
    }

    goToScanQr() {
        localStorage.setItem('QR_FROM', 'PEDIDO');
        this.$router.push('LerQrMesa');
    }

    mostraModalErro(mensagem: string = 'Houve um erro ao processar o seu pedido. Por favor, tente novamente mais tarde.') {
        this.errorMessage = mensagem;
        this.showModalErro = true;
        if (localStorage.getItem('ORDER_TICKET_DATA')?true:false) {
            this.$router.push('PedidoComanda')
        }
    }

    fechaModalErro() {
        if(this.hasError)
        

        this.errorMessage = "";
        if(!this.availableValid)
        this.showModalErro = false;
        if (localStorage.getItem('ORDER_TICKET_DATA')) {
            this.$router.push('Explorar');

        }
    }

    fechaModal() {
        this.showModal = false;
    }

    mostraModalRemoverItem() {
        this.showModalRemoverItem = true;
    }

    fechaModalRemoverItem() {
        this.showModalRemoverItem = false;
    }

    openPasswordRequest() {
        this.modalPasswordRequest = true;
    }

    closePasswordRequest() {
        this.modalPasswordRequest = false;
        this.passwordValue = '';
        this.passwordWrong = false;
    }

    openOrderSuccess(msg: any) {
        this.errorMessage = msg;
        this.orderSuccess = true;
    }

    closeOrderSuccess() {
        this.orderSuccess = false;
        Orders.callback();
    }

    lastNumberCard(lastNumbers: any) {
        let lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
        return lastNumber;
    }

    saveStructures() {
        if (this.selectedDeliverLocation !== 'B') {
            var self = this;
            var arr: any = [];
            const date = moment().date();
            this.structureSelects.forEach(function (select: any, index: number) {
                var selectedOption = $('#select-structure-deliver-' + index).val();
                var option: any = self.structureSelects[index].find((s: any) => s.id == selectedOption);
                arr.push(option.name);
            });
            localStorage.setItem('currentStructure', arr.join(', '));
            localStorage.setItem('lastStructureId', this.STRUCTURE_ID);
            localStorage.setItem('LAST_DAY_OF_LAST_STRUCTURE', date.toString());
        }
    }

    cleanStructureSaved() {
        localStorage.setItem('currentStructure', '');
        localStorage.setItem('lastStructureId', '');
        this.alreadyHasStructure = false;
    }

    verifyOrderPass() {
        this.prepareOrder(null, this.paymentMethodSelected, false);
    }

    validateOrder() {
        if (!localStorage.getItem('USER_ID')) {
            this.openModalLogin();
        } else try {
            let structureId;
            /* Obtendo local de entrega */
            if (this.selectedDeliverLocation === 'T') {
                if (!!localStorage.getItem('lastStructureId') && localStorage.getItem('lastStructureId') != 'undefined') structureId = localStorage.getItem('lastStructureId');
                structureId = localStorage.getItem('structureId')
            } else {
                structureId = this.selectedStructure ? this.selectedStructure.id : null;
            }

            let paymentMethod;
            if (this.paymentMethodSelected)
                paymentMethod = this.paymentMethodSelected;

            if (!structureId && this.selectedDeliverLocation === 'T' && this.alreadyHasStructure == false) {
                /* Caso seja para entregar na mesa porém nenhum local tenha sido selecionado */
                this.mostraModalErro('Por favor, escolha um local de entrega.');
            } else if (this.selectedDeliverLocation === 'T' && !this.currentStructure) {
                /* Caso não tenha sido escolhida a forma de pagamento */
                this.mostraModalErro('Por favor, escolha uma mesa para receber seu pedido.');
            } else if (!paymentMethod) {
                /* Caso não tenha sido escolhida a forma de pagamento */
                this.mostraModalErro('Por favor, escolha uma forma de pagamento.');
            } else if (!this.selectedDeliverLocation) {
                this.mostraModalErro('Por favor, escolha um local de entrega.');
            } else {
                /* Setar dados para proceder com o pedido */
                this.STRUCTURE_ID = structureId;
                this.PAYMENT_METHOD = paymentMethod;
                this.verifyOrderPass();
            }
        } catch (e) {
            console.log(e)
        }
    }

    prepareOrder(structureId: any, paymentMethod: any, isVisibleAddCPF: boolean = true) {
        /* Caso esteja tudo ok para proceder com o pedido */

        var order: any = this.order;

        /* Setando dados do pedido */
        order.DELIVER_TO = this.selectedDeliverLocation;
        order.STRUCTURE_ID = this.selectedDeliverLocation === 'T' ? structureId : null;
        order.TOKEN = localStorage.getItem('TOKEN');
        order.NOTE = $("#observacao").val();
        order.ORDER_FROM_TAA = 'A';
        /* Caso método de pagamento tenha recebido o id do cartão de crédito */
        if ($.isNumeric(paymentMethod)) {
            order.PAYMENT_METHOD = 'CC';
            order.CREDIT_CARD_ID = paymentMethod;
        } else {
            /* Se entrega for no balcão */
            // if (this.selectedDeliverLocation === 'B') 
            order.PAYMENT_METHOD = paymentMethod;
            // else {
            /* Em entrega na mesa, utilizar cartão principal */
            // order.PAYMENT_METHOD = 'CC';
            if (order.PAYMENT_METHOD == 'CC') {
                order.CREDIT_CARD_ID = this.mainCreditcard;
                if (!this.mainCreditcard) {
                    console.log('Não foi identificado nenhum cartão cadastrado para realizar essa compra.');
                    return;
                }
            }
        }
        localStorage.setItem('refresh', 'true');
        this.sendStoneOrder = order;
         //this.testeNome();
       // this.showModalConfirmAddCpf = isVisibleAddCPF ? true : false;
      
    }

    finishOrder(order: any) {
        // this.lp.start(this.lp.LOADING, "Realizando pedido...");
        try {
            const self = this;
            const req = new Request();
            order.USER_ID = localStorage.getItem('USER_ID');
            order.TOKEN = localStorage.getItem('TOKEN');
            req.body = order;
            req.route = 'finishOrder';
            // req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                if (self.alreadyHasStructure == false) self.saveStructures();
                localStorage.setItem('LAST_ORDER_ID', '');
                localStorage.setItem('CURRENT_ORDER', '');
                localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido');
                this.showModalCardPayment = false;
                this.startModalSuccessfulPayment();
                // localStorage.setItem('MOST_RECENT_ORDER', JSON.stringify(order));
                // this.lp.start(this.lp.SUCCESS, 'Seu pedido foi realizado com sucesso');
                // self.$router.push('PedidosRealizados');
                // self.$router.push('Home');
                // req.showSuccessMessage('Seu pedido foi realizado com sucesso.');

            };
            req.callbackError = (error: any) => {
                this.showModalCardPayment = false;
                if (error.response.data.errorCode == 12) {
                    //req.showErrorMessage('Seu limite de compras mensal neste cartão foi excedido.');
                    this.lp.start(this.lp.ERROR, 'Seu limite de compras neste cartão foi excedido.');
                }
                else if (error.response.data.errorCode == 7) {
                    //req.showErrorMessage('Esta transação não foi foi autorizada pelo seu cartão.');
                    this.lp.start(this.lp.ERROR, 'Esta transação não foi foi autorizada pelo seu cartão.');
                }
                else { //req.showErrorMessage('Houve um erro ao processar o seu pedido. Por favor, tente novamente mais tarde.');
                    this.lp.start(this.lp.ERROR, 'Esta transação não foi foi autorizada pelo seu cartão.')
                }

                console.log(error);
            }
            // req.callbackTimeout = (error: any) => {
            //     this.lp.start(this.lp.ERROR, 'Não foi possível completar a transação')
            //     console.log(error);
            // }
            // req.loadingMessage = 'Finalizando o pedido...';
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    focusInput(id: any) {
        const offset = -60;
        const el: any = $("#" + id);
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        }, 150);
    }

    openModalLogin() {
        this.showModalLogin = true;
    }
    closeModalLogin() {
        this.showModalLogin = false;
        //this.getCreditCards();
    }

    handleClickError() {
        //console.log('yes');
        this.lp.stop();
    }

    handleClickSuccess() {
       // this.lp.stop();
        // this.$router.push('PedidosRealizados');
        if(!this.validaVoltar)
        this.$router.push('PedidoComanda');
        else
        this.loadedPos=false;
    }

    goToSelectTable() {
        localStorage.setItem('DELIVER_AT', 'T');
        this.$router.push('LerQrMesa');
    }

    getMaxEstimatedTime() {
        return this.order.ORDER_ITEMS.reduce((a: any, b: any) => { return (a.estimatedTime > b.estimatedTime ? a : b) }).estimatedTime;
    }

    choosePaymentMethod(payment: any) {
        this.fullMethodSelected = payment;
        this.paymentMethodSelected = payment.PAYMENT_METHOD;
        this.selectPaymentMethod = true;
        this.prepareOrder(null, this.paymentMethodSelected, true);
    }

    closeModalConfirm(){
        if (localStorage.getItem('ORDER_TICKET_DATA')) {
            this.$router.push('PedidoComanda')
        }
        this.showModalConfirmAddCpf=false;
        this.showModalChoosePaymentMethod=false;
        clearInterval(this.posIntervalB);

    }
    goToPayment() {
        clearInterval(this.posIntervalB);
        this.hasLast=true;
        this.availableValid=false;
        this.posIntervalB= setInterval(()=>{
            if(this.posStatus!="undone" && this.lastOrder.TRANSACTION_ID){this.posAvailable();
            this.getTransactionDataPos(this.processedOrder.id, this.lastOrder.TRANSACTION_ID);
        }}, 2000);        // this.showModalPaymentCompleted = true;
        localStorage.setItem('atualizar', 'false');

        // this.showLogoutMessageTimer = setTimeout(this.startLogoutTimer,500);

        this.showModalChoosePaymentMethod = true;

        // this.showModalChoosePaymentMethod = true;
        //console.log(this.allPaymentMethods)

    }
    refreshLogout() {
        this.showLogoutMessageTimer = setTimeout(this.startLogoutTimer, 500);

    }

    openModalCardPayment(payment: any) {
        this.showModalChoosePaymentMethod = false;
        this.choosePaymentMethod(payment);
        localStorage.setItem('atualizar', 'true');
       // this.showModalConfirmAddCpf = true; 
        //this.modalConfirmAddEmail = true; 
        this.showModalAproximaçao=true;

        // this.saveDeliverySale();
    }

    confirmOperation() {
        this.showModalChoosePaymentMethod = false;
        this.showModalConfirmPaymentMethod = true;
        if (this.mainCartao.ID) setTimeout(() => this.buildCardView(), 100);
    }

    startModalSuccessfulPayment() {
        if(!this.showModalPaymentCompleted){
        this.showModalCardPayment = false;
        this.showModalAddPhone = false;
        this.modalLoadingFechamento=false;
        this.showModalLoadingFinishOrder = false;
        this.showModalPaymentCompleted = true;
        localStorage.setItem('LAST_ORDER', '');

        this.showLogoutMessageTimer = setTimeout(this.startLogoutTimer, 5000);
    }}

    startLogoutTimer() {
        this.logoutTimerStart = true;
        setInterval(() => { if (this.logoutRemainingTime >= 1) this.logoutRemainingTime -= 1 }, 1000);
        this.logoutTimer = setTimeout(this.fechaModalSucesso, this.logoutRemainingTime * 1000);
    }

    checkUserDataAndGoToMainCard() {
        if (localStorage.getItem('USER_DATA')) {
            this.confirmOperation();
        }
        else {
            this.showModalLogin = true;
            this.showModalChoosePaymentMethod = false;
        }
    }

    selectCard(id: number) {
        this.currentCard = id;
    }

    setMainCard() {
        this.mainCartao = this.userCards.find((c: any) => { return c.ID == this.currentCard });
        this.showModalChangePaymentCard = false;
        setTimeout(() => { this.buildCardView(); }, 10);
    }

    buildCardView() {
        var card = new Card({
            // a selector or DOM element for the form where users will
            // be entering their information
            form: 'form', // *required*
            // a selector or DOM element for the container
            // where you want the card to appear
            container: '.card-wrapper', // *required*

            formSelectors: {

            },

            width: 300, // optional — default 350px
            formatting: true, // optional - default true

            // Strings for translation - optional
            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },

            // Default placeholders for rendered fields - optional
            placeholders: {
                number: (this.mainCartao.ID) ? '•••• •••• •••• ' + this.lastNumberCard(this.mainCartao.LAST_NUMBERS) : '•••• •••• •••• ••••',
                name: (this.mainCartao.ID) ? this.mainCartao.CARD_FULL_NAME : 'Nome Impresso no cartão',
                expiry: (this.mainCartao.ID) ? this.mainCartao.EXPIRATION_DATE : '••/••',
                cvc: '•••'
            },

            masks: {
                cardNumber: '•' // optional - mask card number
            },

            // if true, will log helpful messages for setting up Card
            debug: false // optional - default false
        });
    }

    handleVirtualKeyPress(digit: string) {
        switch (digit) {
            case "\u232B":
                this.popCharacter()
                break;
            case "\u21E7":
                this.alterarTeclado(digit)
                break;
            case "\u21EA":
                this.alterarTeclado(digit)
                break;
            default:
                this.pushCharacter(digit)
                //console.log(digit)
                break;
        }
    }
    pushCharacter(digit: string) {
        this.character++
        if(this.character <= 42) {
            this.emailInput = $('#inputNome').val() + digit
            $('#inputNome').val(this.emailInput)
            // this.efetuarBusca();
        }
    }


    popCharacter() {
        this.emailInput = this.emailInput.substring(0, this.emailInput.length - 1);
        $('#inputNome').val(this.emailInput)
        //  this.efetuarBusca();
    }
    clearD() {
        throw new Error('Method not implemented.');
    }

    handleVirtualKeyPressCpf(digit: string) {
        switch (digit) {
            case "C":
                this.clearUserCpf();
                break;
            case "\u232B":
                this.popCharacterInCpf();
                break;
            default:
                this.pushCharacterToCpf(digit);
                break;
        }
    }

    pushCharacterToPhone(c: string) {
        var uI = Array.from(this.userPhone);
        if (uI.length == 13) uI.shift();
        uI.push(c);
        this.userPhone = Util.applyMask(uI.join(""), "__ _____-____");
        // this.userPhone = uI.join("")
    }

    popCharacterInPhone() {
        var uI = Array.from(this.userPhone);
        uI.pop();
        this.userPhone = Util.applyMask(uI.join(""), "__ _____-____");
        // this.userPhone = uI.join("")
    }

    clearUserPhone() {
        this.userCpf = "";
    }

    pushCharacterToCpf(c: string) {
        var uI = Array.from(this.userCpf);
        if (uI.length == 14) uI.shift();
        uI.push(c);
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
        // this.userPhone = uI.join("")
    }

    popCharacterInCpf() {
        var uI = Array.from(this.userCpf);
        uI.pop();
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
        // this.userPhone = uI.join("")
    }

    clearUserCpf() {
        this.userCpf = "";
    }

    waitForPaymentConfirmation(orderId: string) {
        this.getOrderDataInterval = setInterval(() => {
            const req = new Request();
            req.route = 'getOrderData';
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'ORDER_ID': orderId
            };
            req.callbackSuccess = (response: any) => {
                if (response.order.paymentStatus === 'A') {
                    clearInterval(this.getOrderDataInterval);

                }
            };
            req.send();
        }, 5000);
    }

    validarCPF(inputCPF: string) {
        var soma = 0;
        var resto = 0;
        var aux = '';
        if (inputCPF == '00000000000') return false;
        for (var i = 1; i <= 9; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        for (i = 1; i <= 9; i++) {
            for (var j = 1; j <= 11; j++)
                aux = aux.concat(String(i))
        }
        if (aux.includes(inputCPF)) return false
        if ((resto == 10) || (resto == 11)) resto = 0;
        //console.log(parseInt(inputCPF.substring(9, 11)), resto)
        if (resto != parseInt(inputCPF.substring(9, 10))) return false;
        soma = 0;
        for (i = 1; i <= 10; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(inputCPF.substring(10, 11))) return false;
        return true;
    }

    cpfadded() {

        if (this.validarCPF(this.userCpf.replace('.', '').replace('.', '').replace('-', '')) || this.userCpf == '') {
            this.showModalAddCpf = false;
            this.modalAddEmail=false;
            this.modalConfirmAddEmail=false;
            this.showModalAddName = false;
            this.showModalLoadingCreateOrder = true;

            if(this.posStatus!="processing" && !this.processingCreate)
            this.preTransactionDate()
           

            //this.testaPosLivre();
        } else {
            this.showModalCpfInvalido = true;


        }
        
       // this.closeTicket(this.tickets.comandasU[0])

         //this.tickets.comandasU.forEach((com: any) => {this.closeTicket(com)})

        // this.getPreTransactionData(this.orderP);
    }
    saveDeliverySale(){
        const req = new Request();
        var items;
        this.order.ORDER_ITEMS.forEach((i:any) => {
            if(i.extras.length>0){
            var extras
            i.extras.array.forEach((s:any) => {
            s.selectedOptions.array.forEach((e:any) => {
                var extra={
                    "toTravel": false, // Para viagem ou não
                    "productCode": e.id, // Código do produto
                    "productName": e.name, // Nome do produto
                    "unitValue": e.price, // Valor unitário
                    "quantity": 1, // Quantidade
                    "discount": 0, // Desconto
                    "note": "", // Observação digitada pelo usuário
                    "observations": []
                }
                extras.push(extra)
            });
            });
        }
            var item={
                "toTravel": false,
                "unitValue": i.price,
                "productCode": i.productId,
                "productName": i.productName,
                "quantity": i.quant,
                "discount": 0,
                "note": i.note,
                "observations": [],
                "childItems": []
            }
            items.push(item)   
        });
       
        req.body=  {
    "taaSaleId": null,
    "order": {
        "closeByApi": true,
        "subsidiaryCode": String,
        "storeCode": "1",
        "posCode": "026",
        "pagerId": null,
        "consumerId": String,
        "clientCode": null,
        "consumerCode": "SDE000000000000000013",
        "items": [
            items
        ],
        "orderDescription": {
            "orderType": "TAA",
            "productsValue": "",
            "deliveryFee": 0,
            "totalValue": "",
            "orderCPF":"" ,
            "changeValue": 0,
            "serviceTax": 0,
            "payments": [
                {
                    "quantParcel": 1,
                    "paymentCode": "001",
                    "paymentName": "TEF_CC_TAA",
                    "value": 4,
                    "nsu": "null"
                }
            ],
            "cashBack": 0,
            "deliveryAddress": null,
            "isScheduled": false,
            "scheduledDate": null
        }
    }
}
this.formatedOrder=req.body;
console.log("teste")

console.log(req.body)

    }

    createPendentOrder(order: any) {
        const self = this;
        const req = new Request();
        localStorage.setItem('USER_NAME', this.inputNome);
        order.STONE_NOTE = self.createClaimCheck();
        order.TRANSACTION_ID = this.preTransactionActive;
        req.route = 'event/createPendentOrder';
        req.body = order;
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            //console.log(response);
            clearInterval(this.posIntervalB);
            this.processedOrder = response.order;
            // this.sendStonePreTransaction(order);
            // this.showModalConfirmAddCpf= true;
            this.sendStonePreTransaction(order)
           //this.updatePaymentStatus(response.order.id)
           //this.closeTicket(this.tickets.comandasU[0])

            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            // this.updatePaymentStatus(response.order.id) 
            this.orderP = response.order.id;
            //this.printClaimCheck();
            //this.waitForPaymentConfirmation(response.order.id);
            this.showModalLoadingCreateOrder = false;
            this.showModalCardPayment = true;
            this.showModalConfirmNoteV = true;
            this.lastOrder = this.order;
            this.hasLast=false;
            localStorage.setItem('LAST_ORDER', JSON.stringify(this.order));
            //  this.getOrderDataInterval = setTimeout(()=>this.posAvailable(), 5000);
            this.posAvailable()
            //   if(this.posInterval==undefined)
            //   this.posInterval= setInterval(()=>{if(this.posStatus!="undone")this.posAvailable(); console.log( this.posInterval);}, 2000);
            //   else{
            clearInterval(this.posInterval);
            this.posInterval = setInterval(() => {
                if (this.posStatus != "undone") this.posAvailable();
            }, 2000);
            //   }
            this.noteContinterval = setInterval(() => { if (this.posStatus == "processing" && this.noteCont < 63) this.noteCont++ }, 1000);

            this.reqFailure=0
            this.processingCreate=false;
        };
        req.callbackError = () => {
            this.reqFailure++;
            this.getOrderDataInterval = setTimeout(() => self.createPendentOrder(order), 5000);
            this.processingCreate=false;

            // this.clearModal();
            // this.cancelPreTransaction();
            //this.errorMessage = "Erro na criação do pedido. Caso já tenha efetuado a compra, procure um atendente do estabelecimento para cancelar o pagamento."
            // this.showModalErro = true; 
        };
        req.callbackTimeout = () => { };
        req.send();
    }
    updateStoneReferenceId() {
        const req = new Request();
        req.route = 'updateStoneReferenceId';
        req.body = {
        'USER_ID': localStorage.getItem('USER_ID'),
        'TOKEN': localStorage.getItem('TOKEN'),
        "ORDER_ID": this.orderP,
        "pre_transaction_id": this.preTransactionActive

        };
        req.callbackSuccess = (response: any) => {
            this.getPreTransactionData(this.preTransactionActive);
        };
        req.callbackError = (error: any) => {

        };
        req.send();
    }

    updateStonePosReferenceId() {
        if (this.store.stoneRecipientId && this.store.stoneRecipientId.substring(0, 13) == "establishment") {
            localStorage.setItem('STONE_ESTABLISHMENT_ID', this.store.stoneRecipientId.substring(13));
            this.getEstablishmentData(this.store.stoneRecipientId.substring(13));
        }
        else { this.getUserData(); }
    }

    getEstablishmentData(stoneEstablishmentId: string) {
        const req = new Request();

        req.body = stoneEstablishmentId;
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('STONE_ESTABLISHMENT_DATA', JSON.stringify(response.establishment));
            this.getUserData();
        };
        req.callbackError = (error: any) => {

        };
        req.getEstablishmentData();
    }

    sendStonePreTransaction(order: any) {
        
        var paymentMethodName = '';
        var paymentType = 0;
        const req = new Request();
        var paymentMethod: any = this.paymentMethods.find((p: any) => { return p.PAYMENT_METHOD == order.PAYMENT_METHOD }) || {};
        if (paymentMethod.PAYMENT_METHOD.includes('EXT')) {
            paymentMethodName = (paymentMethod.LABEL) ? Util.removeSpecialCharacters(paymentMethod.LABEL) : "";
            paymentType = (paymentMethodName.includes('credito')) ? 2 : 1;
        } else {
            paymentMethodName = (paymentMethod.PAYMENT_METHOD);
            if (paymentMethodName.includes('TEF_CC_TAA')) {
                paymentType = 2;
            }
            else if (paymentMethodName.includes('TEF_DC_TAA')) {
                paymentType = 1;
            }
            else paymentType = 3;
        }
        // adequar quando não for possivel usar o metodo de pagamento TEF_CC_TAA
        // var paymentMethodName = (paymentMethod.LABEL) ? Util.removeSpecialCharacters(paymentMethod.LABEL): "";
        // var paymentType = (paymentMethodName.includes('credito')) ? 2:1; 
        req.body = {
            "establishment_id": localStorage.getItem('STONE_ESTABLISHMENT_ID'),
            "amount_total": parseFloat((parseFloat(order.TOTAL_VALUE) * 100).toFixed()),
            "pos_reference_id": localStorage.getItem('POS_REFERENCE_ID'),
            "payment": {
                "type": paymentType,
                "installment": null
            }
        }
        if (this.store.stoneRecipientId && this.store.stoneRecipientId.substring(0, 13) != "establishment") {
            req.body.enable_split_adjust_in_mdr_math_fail_scenario = true;
            req.body.split_fee_liability = "Recipient",
                req.body.recipients = [{
                    "recipient_id": this.store.stoneRecipientId,
                    "amount": parseFloat((parseFloat(order.TOTAL_VALUE) * 100 ).toFixed(2))
                }]
        }
        req.callbackSuccess = (response: any) => {
            this.preTransactionActive = response.pre_transaction.pre_transaction_id;
            // this.getPreTransactionData(response.pre_transaction.pre_transaction_id, order.id);
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            //this.createPendentOrder(order);
            this.updateStoneReferenceId();
            this.reqFailure=0;
        };
        req.callbackError = (error: any) => {
            console.log(error);
            if (!localStorage.getItem('POS_REFERENCE_ID')) {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.updateStonePosReferenceId();
            }
            else if (!error.error) {
                console.log(error);
                this.reqFailure++;
                this.showModalLoadingCreateOrder = false;

                this.msgModal="Não foi possível comunicar com servidor de pagamentos. Tente novamente."
                this.modalExitPedido=true
                //this.getOrderDataInterval = setTimeout(() => this.sendStonePreTransaction(order), 5000);
            }
            else if (error.error == "3113" || error.error == "3116" || error.error == "3101") {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.modalExitPedido=true

                this.updateStonePosReferenceId();
            }
            else {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.modalExitPedido=true
                this.clearModal();
                this.showModalErro = true;
            }
        };
        req.sendStonePreTransaction();
    }

    lojaAberta() {
        try {

            const req = new Request();
            req.showLoader = false;

            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                "STORE_ID": this.store.id
            }
            req.route = 'isOpened';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem("OPENED", response.opened.value)

                if (!response.opened.value) {
                    this.$router.push('produtos');
                }

            }
            req.callbackError = () => {
                console.log("Error");
            }

            req.send();
        } catch (e) {
            console.log(e)
        }
    }
    posAvailable() {
        //  localStorage.setItem('atualizar', 'false');

        const req = new Request();
        req.showLoader = false;
        req.body = {
            'TOKEN': localStorage.getItem('TOKEN'),
            "pre_transaction_id":this.hasLast?this.lastOrder.TRANSACTION_ID: this.preTransactionActive,
            "show": "show"
        }
        req.route = 'posAvailable';
        req.callbackSuccess = (response: any) => {
            //console.log(response.status.value);
            //localStorage.setItem("OPENED", response.opened.value)
            this.posStatus = response.status.value;
            if (response.status.value == "processing") {
                this.posIsAvailable = false;
                if(!this.availableValid){
                this.showAvailable = true;
                this.showModalCardPayment = false;
                }
                //  this.getOrderDataInterval =  setTimeout(()=>{this.posAvailable()}, 3000);
                localStorage.setItem('atualizar', '');
            }
            else if (response.status.value == null) {
                //           this.getOrderDataInterval =  setTimeout(()=>{this.posAvailable()}, 3000);
                if (this.noteCont > 90)
                    localStorage.setItem('atualizar', '');

                //  $.trigger('click')

            } else if (response.status.value == "undone") {
                this.posIsAvailable = true;
                this.showAvailable = false;
                if(!this.hasLast){
                this.fechaModalSucesso();
                } else {
                    this.showModal=false;
                    this.showModalLoadingCreateOrder = false;

                }
              //  else this.sendStonePreTransaction(this.sendStoneOrder);

              //  localStorage.setItem('atualizar', '');

            }
            // if(!response.opened.value){
            //     this.$router.push('produtos');
            // }
            this.loadedPos = true;

            //  this.showModalChoosePaymentMethod = true;
        }
        req.callbackError = () => {
            //  this.showModalChoosePaymentMethod = true;
            console.log("Error");
            //   this.getOrderDataInterval =  setTimeout(()=>{this.posAvailable()}, 5000);

            this.posIsAvailable = true;
            //this.showAvailable=true;
            this.loadedPos = true;
        }
        if (this.finishedOrder)
            this.showAvailable=false
        else req.send();
    }
    closeTicket(com:any) {
        const req = new Request();
        var data =new Date()
        var paymentMethod: any = this.paymentMethods.find((p: any) => { return p.PAYMENT_METHOD == this.paymentMethodSelected }) || {};
        var barCode: string = "";
        
        var paymentType: string = this.paymentMethodSelected == "TEF_CC_TAA" ? '004' : '003';       
            //this.tickets.comandasU.forEach((com: any) => {
           // if(barCode.substring(0,4) == "DLV_") {
           //     console.log('fafdafd');
           //     barCode = barCode.substring(4);
           // }
           // else console.log(barCode.substring(0,4))
         console.log("closeticket")

           if (com.products != undefined) {
            
                req.body = {
                   "barcode": com.products[0].barcode,
                    "method_payment": paymentType,
                   "subtotal": com.total_value.toFixed(2),
                    "discount": "0",
                    "total": com.total_value.toFixed(2),
                    "cpf":this.userCpf?this.userCpf:null,
                    "email":this.emailInput?this.emailInput:null,
                    "TAA":this.taaName,
                    
                };
    
                req.callbackSuccess = (response: any) => { 
                    console.log("success")
                    if(!response.error && response.data.length>0){
                 this.crialog('fecha-'+data.getHours()+':'+data.getMinutes()+":"+data.getSeconds()+' - '+JSON.stringify(com.products[0].barcode)+' '+JSON.stringify(com.total_value.toFixed(2))+'\n')
                    this.cont++;
                if (this.tickets.comandasU.length<=this.cont &&this.hasError==false) this.startModalSuccessfulPayment()
                else if(this.tickets.comandasU.length<=this.cont &&this.hasError==true){
                    this.modalLoadingFechamento=false
                    this.errorMessage = "Entregue seu comprovante e sua comanda na saida.";
                    this.showModalLoadingCreateOrder = false;
                    this.showModalLoadingFinishOrder = false;
                    this.showModalErro = true;
                    this.contErro=0;
                 }
                else setTimeout(() => this.closeTicket(this.tickets.comandasU[this.cont]), 500) 
                
            }else if(this.contErro<2){
                this.closeTicket(com)
                     this.contErro++
            }else if(this.tickets.comandasU.length<=this.cont){
                    this.modalLoadingFechamento=false
                    this.errorMessage = "Entregue seu comprovante e sua comanda na saida.";
                    this.showModalLoadingCreateOrder = false;
                    this.showModalLoadingFinishOrder = false;
                    this.showModalErro = true;
                    this.contErro=0;
                 }else {
                    this.contErro=0;    
                    this.cont++;
                    this.hasError=true
                    setTimeout(() => this.closeTicket(this.tickets.comandasU[this.cont]), 500)
                 }

                 };
                req.callbackError = (error: any) => {
                    console.log("error")

                    if(this.contErro<2){
                        setTimeout(() => this.closeTicket(com), 500);
                             this.contErro++
                    }else if(this.tickets.comandasU.length<=this.cont+1){
                            this.modalLoadingFechamento=false
                            this.errorMessage = "Entregue seu comprovante e sua comanda na saida.";
                            this.showModalLoadingCreateOrder = false;
                            this.showModalLoadingFinishOrder = false;
                            this.showModalErro = true;
                            this.contErro=0;
                         }else {
                            this.hasError=true
                            this.contErro=0;    
                            this.cont++;
                            setTimeout(() => this.closeTicket(this.tickets.comandasU[this.cont]), 500)
                         }




                //     if (error.message && this.contErro>1) {
                //         this.errorMessage = "Não foi possível concluir o pagamento da comanda";
                //         this.showModalLoadingCreateOrder = false;
                //        this.showModalLoadingFinishOrder = false;
                //        this.modalLoadingFechamento=false;
                //        this.showModalErro = true;
                //        this.contErro=0;
                //    }
                //     else{ this.getOrderDataInterval = setTimeout(() => this.closeTicket(com), 5000);
                //         this.contErro++
                //     }
                        };
               req.closeTicket(); 
           } 
       //});       
   }

    getPreTransactionData(orderId: any) {
        const req = new Request();

        req.callbackSuccess = (response: any) => {
            if (response.pre_transaction.processed && !response.pre_transaction.is_active) {
                this.finishedOrder= true
                var preTransactionId = this.preTransactionActive;
                this.preTransactionActive = "";
               // this.saveSale();
               if (localStorage.getItem('ORDER_TICKET_DATA')){
                this.closeTicket(this.tickets.comandasU[0])
                this.modalLoadingFechamento=true;
            }
                this.getTransactionData(orderId, preTransactionId)
                //this.updatePaymentStatus(orderId);
                //this.printClaimCheck();
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
            }
            else if (!response.pre_transaction.processed && !response.pre_transaction.is_active) {
                this.preTransactionActive = "";
                this.cancelOrder(orderId);
                this.showAvailable=false;
                this.getOrderDataInterval = undefined;
            }
            else this.getOrderDataInterval = setTimeout(() => { this.getPreTransactionData(orderId) }, 1000);

            //     // if(this.showModalConfirmNoteV) this.getOrderDataInterval= setTimeout(()=>{if(this.showModalConfirmNoteV){this.showModalCardPayment=false 
            //   this.showModalConfirmNote=true
            // }
            // this.showModalConfirmNoteV=false;
            // clearTimeout(this.getOrderDataInterval)
            //  }, 20000)
            this.reqFailure=0;

        };


        //             modificar abaixo
        // 
        // this.posAvailable()
        //if(this.posStatus=="processing" && this.noteCont==0){
        // this.noteContinterval=  setInterval(()=> this.noteCont++, 1000)
        // console.log(this.noteCont)
        if (this.noteCont == 63)
            this.closeModalCardPayment()
        // this.showModalCardPayment=false 
        // this.showModalConfirmNote=true
        //localStorage.setItem('atualizarNow', 'true');}
        req.callbackError = (error: any) => {
            this.reqFailure++;

            //this.getOrderDataInterval = setTimeout(() => this.getPreTransactionData(orderId), 2000);
        };
        req.body = this.preTransactionActive;
        if (this.preTransactionActive) req.getStonePreTransactionStatus();
    }
    exitPedido(){
        if (this.exitCounter > 5) {
            this.showAvailable=false;
            this.modalExitPedido=true;
        }
        else {
            this.exitCounter += 1;
        }
    }

    getTransactionData(orderId: any, preTransactionId: string) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            var transactionData = response.transaction;
            this.showModalCardPayment = false;
            this.showModalLoadingFinishOrder = true;
            //this.showModalConfirmAddCpf= true;
            this.updatePaymentStatus(orderId, transactionData);
        };
        req.callbackError = (error: any) => {
            this.showModalCardPayment = false;
            this.showModalLoadingFinishOrder = true;
            this.updatePaymentStatus(orderId);
        };
        req.body = preTransactionId;
        req.getStoneTransactionData();
    }
    getTransactionDataPos(orderId: any, preTransactionId: string) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            var transactionData = response.transaction;
            this.showModalCardPayment = false;
            this.showModalLoadingFinishOrder = true;
            //this.showModalConfirmAddCpf= true;
            clearInterval(this.posIntervalB);

            this.updatePaymentStatus(orderId, transactionData);
        };
        req.callbackError = (error: any) => {
            console.log("teste")
            //this.showModalCardPayment = false;
            //this.showModalLoadingFinishOrder = true;
            //this.updatePaymentStatus(orderId);
        };
        req.body = preTransactionId;
        req.getStoneTransactionData();
    }
    //função para mandar requisição de imprimir no stone
    /*
    printClaimCheck(){
        var establishmentData: any = null;
        var organizationData: any = null;
        var paymentMethodName = '';
        var paymentType =0;
        //if(localStorage.getItem('STONE_ESTABLISHMENT_DATA')){
            //establishmentData = JSON.parse(localStorage.getItem('STONE_ESTABLISHMENT_DATA')||'{}')||null;
        //}
        if(localStorage.getItem('ORGANIZATION_DATA')){
            organizationData = JSON.parse(localStorage.getItem('ORGANIZATION_DATA')||'{}')||null;
        }
        if(this.processedOrder.paymentMethod.includes('EXT')){
            paymentMethodName = (this.processedOrder.paymentMethodName) ? Util.removeSpecialCharacters(this.processedOrder.paymentMethodName): "";
            paymentType = ( paymentMethodName.includes('credito')) ? 3:2; 
        }else{
            paymentMethodName = (this.processedOrder.paymentMethod);
            paymentType = (paymentMethodName.includes('TEF_CC_TAA')) ? 3:2; 
            }
        const req = new Request();
        req.body = {
            "establishment_id": localStorage.getItem("STONE_ESTABLISHMENT_ID"),
            "pos_reference_id": localStorage.getItem("POS_REFERENCE_ID"),
            "amount": this.priceFormatter.format(this.order.TOTAL_VALUE),
            "document_number": "              ",
            "company_name": " ",
            "date_time_created": moment().format("DD/MM/YYYY HH:mm"),
            // "writing_area_1":"senha:" + String(this.processedOrder.orderIdentifier),
            //"writing_area_1": "/n/n SENHA: " + String(this.processedOrder.orderIdentifier) + "/n/n",
            "writing_area_4": "/n/n SENHA: " + String(this.processedOrder.orderIdentifier),
             "number": String(this.processedOrder.orderIdentifier),
            "title": this.store.name,
            "footer": "www.teknisa.com",
            "is_number_item": true,
            "address": {
                "street_name": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "street": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "number": (this.structureAddress && this.structureAddress.NUMBER) ? String(this.structureAddress.NUMBER) : " ",
                "neighborhood": (this.structureAddress && this.structureAddress.NEIGHBORHOOD) ? this.structureAddress.NEIGHBORHOOD : " ",
                "zip_code": (this.structureAddress && this.structureAddress.CEP) ?  this.structureAddress.CEP : "00000-000",
                "city": (this.structureAddress && this.structureAddress.CITY ) ? this.structureAddress.CITY : " ",
                "state": (this.structureAddress && this.structureAddress.PROVINCY) ? this.structureAddress.PROVINCY : "  "
            },
            "item": [
            ],
            "payment": [
                {
                "transaction_type": paymentType,
                "amount": this.priceFormatter.format(this.order.TOTAL_VALUE),
                }
            ]
        }
        this.order.ORDER_ITEMS.forEach((item:any) => {
            var newItem = {
                "name": item.productName.toUpperCase(),
                "amount": 'R$ ' + this.priceFormatter.format(item.totalItem),
                "quantity": String(item.quant),
                "unitary_value": 'R$ ' + this.priceFormatter.format(item.price)
            }
            req.body.item.push(newItem)
        })
        req.callbackSuccess = (response: any) => {    
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            this.showModalLoadingFinishOrder = false;
            this.startModalSuccessfulPayment();
        };
        req.callbackError = (error: any) => {
            this.getOrderDataInterval = setTimeout(()=>this.printClaimCheck(), 5000); 
        };
        req.claimCheckCreate()
    }
*/
    getNameByCPF(cpf: number) {
        const req = new Request();
        req.route = 'userExistsByCpfAndReturnData';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'CPF': cpf
        };
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            var nomeSobrenome = (response.response.user.firstName + " ") + response.response.user.lastName;
            localStorage.setItem('USER_NAME', JSON.stringify(nomeSobrenome));
            this.inputNome = nomeSobrenome;
            //console.log(response.user.firstName)
            this.cpfadded();
        };
        req.callbackError = (error: any) => {
            localStorage.setItem('USER_NAME', '');
            this.inputNome = "";
            this.cpfadded();
        };

        req.send();
    }

    createClaimCheck() {
        // var establishmentData: any = null;
        var organizationData: any = null;
        //if(localStorage.getItem('STONE_ESTABLISHMENT_DATA')){
        //establishmentData = JSON.parse(localStorage.getItem('STONE_ESTABLISHMENT_DATA')||'{}')||null;
        //}

        if (localStorage.getItem('ORGANIZATION_DATA')) {
            organizationData = JSON.parse(localStorage.getItem('ORGANIZATION_DATA') || '{}') || null;
        }
        // var paymentMethodName = (this.processedOrder.paymentMethodName) ? Util.removeSpecialCharacters(this.processedOrder.paymentMethodName): "";
        var paymentType: number = this.paymentMethodSelected == "TEF_CC_TAA" ? 3 : 2;
        var claimCheck: any = {
            "establishment_id": localStorage.getItem("STONE_ESTABLISHMENT_ID"),
            "pos_reference_id": localStorage.getItem("POS_REFERENCE_ID"),
            "amount": this.priceFormatter.format(this.order.TOTAL_VALUE),
            "document_number": "    ",
            "company_name": this.store.name +this.taaName,
            "date_time_created": moment().format("DD/MM/YYYY HH:mm"),
            "number": "@@INSERT@@NUMBER@@HERE@@",
            "title": this.store.structureName,
            "writing_area_3": 'comandas pagas: '+this.barcodeComandas,
            "writing_area_4": "/n<h1><b>Senha:" + "@@INSERT@@NUMBER@@HERE@@</b></h1>",
            "footer": "Seu cupom fiscal foi emitido no caixa./Caso necessite, apresente este/comprovante e retire-o.",
            "is_number_item": true,
            "address": {
                "street_name": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "street": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "number": (this.structureAddress && this.structureAddress.NUMBER) ? String(this.structureAddress.NUMBER) : " ",
                "neighborhood": (this.structureAddress && this.structureAddress.NEIGHBORHOOD) ? this.structureAddress.NEIGHBORHOOD : " ",
                "zip_code": (this.structureAddress && this.structureAddress.CEP) ? this.structureAddress.CEP : "00000-000",
                "city": (this.structureAddress && this.structureAddress.CITY) ? this.structureAddress.CITY : " ",
                "state": (this.structureAddress && this.structureAddress.PROVINCY) ? this.structureAddress.PROVINCY : "  "
            },
            "client": {
                "name": this.inputNome?this.inputNome:" ",
                "document_number": this.userCpf ?  this.userCpf:" "
            },
            "item": [
            ],
            "payment": [
                {
                    "transaction_type": paymentType,
                    "amount": this.priceFormatter.format(this.order.TOTAL_VALUE),
                    "installment": paymentType == 3 ? 1 : ""

                }
            ],
            "note_type": 2
        }
        this.order.ORDER_ITEMS.forEach((item: any) => {
            var newItem = {
                "name": item.productName.toUpperCase(),
                "amount": 'R$ ' + this.priceFormatter.format(item.totalItem),
                "quantity": String(item.quant),
                "unitary_value": 'R$ ' + this.priceFormatter.format(item.price)
            }
            claimCheck.item.push(newItem);
        })
                       // "document_number": this.userCpf ? "***" + this.userCpf.substr(3, 9) + "**" : " "

        // localStorage.setItem('USER_NAME','');  
        //this.inputNome = "";

        return JSON.stringify(claimCheck);
    }
    saveSale(){
        const req = new Request();
        var items:any
        this.order.ORDER_ITEMS.array.forEach((i:any) => {
            var item=[
                {
                    "toTravel": false,
                    "unitValue": i.price,
                    "productCode": i.externalId,
                    "productName": i.productName,
                    "quantity": i.quant,
                    "discount": 0,
                    "note": null,
                    "observations": [],
                    "childItems": []
                }
            ]
        items.push(item)
        });
       
        req.body = {
            
                "taaSaleId": null,
                "order": {
                    "closeByApi": true,
                    "subsidiaryCode": "0012",
                    "storeCode": "1",
                    "posCode": "026",
                    "pagerId": null,
                    "consumerId": this.userCpf,
                    "clientCode": null,
                    "consumerCode": null,
                    "items": items   ,
                    "orderDescription": {
                        "orderType": "TAA",
                        "productsValue": 4,
                        "deliveryFee": 0,
                        "totalValue": 4,
                        "orderCPF": this.userCpf,
                        "changeValue": 0,
                        "serviceTax": 0,
                        "payments": [
                            {
                                "quantParcel": 1,
                                "paymentCode": "001",
                                "paymentName": "TEF_CC_TAA",
                                "value": 4,
                                "nsu": null
                            }
                        ],
                        "cashBack": 0,
                        "deliveryAddress": null,
                        "isScheduled": false,
                        "scheduledDate": null
                    }
                }
            }
            req.callbackSuccess = (response: any) => {  this.closeOrder(response.data[0].saleCode)  };
            req.callbackError = (error: any) => {   };
            req.saveSale();
    }
    danfeCreate(){
        const req = new Request();
        req.body = {
            "establishment_id":localStorage.getItem('STONE_ESTABLISHMENT_ID'),
            "pos_reference_id": localStorage.getItem('POS_REFERENCE_ID'),
            "amount": this.priceFormatter.format(this.order.TOTAL_VALUE),
            "amount_to_pay": this.priceFormatter.format(this.order.TOTAL_VALUE),
            "document_number": "26269316000177",
            "company_name": "PISCINA LANCHONETE",
            "is_contigency": true,
            "nf_type_described": "NFC-E",
            "invoice_link": "",
            "qrcode_link": "",
            "date_time_authorization": "",
            "date_time_created": "",
            "authorization_protocol": "",
            "number": "35200692",
            "serie": "1",
            "transshipment": "",
            "discount": "",
            "discount_percentage": "",
            "tip": "",
            "service_rate": "",
            "service_rate_percentage": "",
            "title": "",
            "footer": "",
            "site": "",
            "phone": "",
            "address": {
              "street": "RUA ANGELINA MAFFEI VITA",
              "number": "493",
              "neighborhood": "JARDIM EUROPA",
              "zip_code": "001455902",
              "city": "SAO PAULO",
              "state": "SP"
            },
            "client": {
              "name": "",
              "document_number": "26269316000177",
              "address": {
                "street_name": "RUA ANGELINA MAFFEI VITA",
                "number": "493",
                "neighborhood": "JARDIM EUROPA",
                "zip_code": "001455902",
                "city": "SAO PAULO",
                "state": "SP"
              }
            },
            "is_number_item": true,
            "item": [
              {
                "name": "cafe",
                "amount": "4",
                "code": "",
                "quantity": "",
                "unitary_value": ""
              }
            ],
            "payment": [
              {
                "transaction_type": 3,
                "installment": 0,
                "date": "",
                "amount": "4",
                "others_description": ""
              }
            ],
            "information_title": "",
            "writing_area_1": "string",
            "writing_area_2": "string",
            "writing_area_3": "string",
            "writing_area_4": "string"
          }
            
            req.callbackSuccess = (response: any) => {  this.closeOrder(response.data[0].saleCode)  };
            req.callbackError = (error: any) => {   };
            req.danfeCreate();
    }

    closeOrder(saleCode:string){
        const req = new Request();
        req.body = {
                "subsidiaryCode": "0012",
                "saleCode": saleCode,
                "posCode": "026"
               }
    req.callbackSuccess = (response: any) => {  this.getSale(saleCode)  };
    req.callbackError = (error: any) => {   };
        req.closeOrder();

    }
    pedidoComanda(){
        var paymentMethod:any = this.paymentMethods.find((p:any)=>{ return p.PAYMENT_METHOD == this.paymentMethodSelected}) || {};
        this.apiUrl=localStorage.getItem('STORE_IP') || 'http://10.0.25.126:9091/odhen-eattake/backend/service/index.php'
        this.apiRequest={
            "barcode": localStorage.getItem("TICKET_BARCODE")||"",
            "method_payment": String(paymentMethod.ID),
            "subtotal": this.order.total_value,
            "discount": "0",
            "total": this.order.total_value
        }
 }
    getSale(saleCode:string){
        const req = new Request();
        req.body = {
                "subsidiaryCode": "0012",
                "saleCode": saleCode,
                "posCode": "026"
               }
        req.callbackSuccess = (response: any) => { 
            if(response.message!="")
            setTimeout(() => this.getSale(saleCode) , 5000); 
    };
        req.callbackError = (error: any) => {this.getSale(saleCode)   };
        req.getSale()
    }
    updatePaymentStatus(orderId: any, transactionData: any = {}) {
        const req = new Request();
        localStorage.setItem('USER_NAME', this.inputNome);
       // localStorage.getItem('ticketdata')?this.pedidoComanda():false;
        req.route = 'updateStonePaymentStatus';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_NAME': localStorage.getItem('USER_NAME') ? localStorage.getItem('USER_NAME') : this.inputNome,
            'STORE_ID': this.store.id,
            'ORDER_ID': this.orderP,
            'CREDIT_CARD_BRAND': null,
            'CARD_HOLDER_NAME': null,
            'CREDIT_CARD_NUMBERS': null,
            'USER_CPF': this.userCpf,
            'API_REQUEST': this.apiRequest,
            'API_EXTERNAL_URL': this.apiUrl,
        };
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
          
        this.showModalCardPayment=false;
          //  this.tickets.comandasU.forEach((com: any) => {setTimeout(() => this.closeTicket(com),5000)})
  
          //this.startModalSuccessfulPayment()
            this.showModalLoadingFinishOrder=false;
            this.processedOrder.orderIdentifier = response.order.orderIdentifier;
            if(response.order.OAsuccess=="false"||response.order.OAsuccess==false){
            this.showModalIntegracao=true;
            this.showModalLoadingFinishOrder=false;
        }
      //  else if (localStorage.getItem('ORDER_TICKET_DATA')) this.closeTicket; else  this.startModalSuccessfulPayment();
            this.password = this.processedOrder.orderIdentifier.toString().slice(-3);
            //this.printClaimCheck();
        };
        req.callbackError = (error: any) => {
            this.getOrderDataInterval = setTimeout(() => this.updatePaymentStatus(orderId, transactionData), 5000);
        };
        req.send();
    }

    crialog(data:string){
        var log = localStorage.getItem('LOG')||""
        log ='\n '+log.concat(data);
        localStorage.setItem("LOG",log)
    }
    crialogTransaction(data:string){
        var log = sessionStorage.getItem('LOG_TRANSACTION')||""
        log ='\n '+log.concat(data);
        sessionStorage.setItem("LOG_TRANSACTION",log)
    }
    
    fechaModalIntegracao(){
        this.showModalIntegracao;
        this.startModalSuccessfulPayment();
        if (localStorage.getItem('ORDER_TICKET_DATA')?true:false) {
            this.$router.push('PedidoComanda')
        }

    }

    cancelOrder(orderId: any, openModal = true) {
      
        this.availableValid=true;
        const req = new Request();
        req.route = 'cancelOrder';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'ORDER_ID': orderId
        };
        req.callbackSuccess = (response: any) => {
            this.clearModal();

            if (openModal) {
                this.showModalErro = true;
                this.errorMessage = "Operação cancelada pelo usuário.";
                // if (localStorage.getItem('ORDER_TICKET_DATA')?true:false) {
                //     this.$router.push('PedidoComanda')
                // }
            }

        };
        localStorage.setItem('USER_NAME', '');
        req.send();
        // if (localStorage.getItem('ORDER_TICKET_DATA')) {
        //     this.$router.push('PedidoComanda')
        //  }
    }

    showModals(msg: any) {
        this.msgModal = msg;
        this.modal = true;
    }

    clearModal() {
        this.showModalSucesso = false;
        this.showModalErro = false;
        this.showModalRemoverItem = false;
        this.showModal = false;
        this.showModalTelaCartoes = false;
        this.modalPasswordRequest = false;
        this.showModalConfirmPaymentMethod = false;
        this.showModalChoosePaymentMethod = false;
        this.showModalCardPayment = false;
        this.showModalPaymentCompleted = false;
        this.showModalChangePaymentPassword = false;
        this.showModalChangePaymentCard = false;
        this.showModalAddPhone = false;
        this.showModalLoadingCreateOrder = false;
        this.showModalLoadingFinishOrder = false;
        this.showModalAddCpf = false;
        this.showModalConfirmAddCpf =false;
        this.showModalConfirmNoteV = false;
        this.showModalConfirmNote = false;
        this.noteCont = 0;
        this.showAvailable=false;
        if (localStorage.getItem('ORDER_TICKET_DATA')&& !this.modalExitPedido) {
            this.$router.push('PedidoComanda')
        }
    }

    cancelPreTransaction() {
        // if (localStorage.getItem('ORDER_TICKET_DATA')) {
        //     this.$router.push('PedidoComanda')
        // }
        clearInterval(this.getOrderDataInterval);
        this.getOrderDataInterval = undefined;
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            this.preTransactionActive = "";
            if (this.processedOrder.id) this.cancelOrder(this.processedOrder.id, false);
            //this.printClaimCheck();
        };
        req.callbackError = (error: any) => {
            setTimeout(() => { this.cancelPreTransaction()   }, 2000);
            this.contErro++
            this.loadedPos=false
            if(this.contErro>2&& !this.validaVoltar){
                this.loadedPos=true
            this.modalExitPedido=true;
            this.validaVoltar=true;
            this.msgModal="Não foi possível cancelar a operação anterior, verifique conexão com a internet";
            }
        };
        req.body = this.preTransactionActive;
        if(req.body)
        req.cancelPreTransaction();
    }
    cancelLastPreTransaction(preTransactionActive:string) {
        // if (localStorage.getItem('ORDER_TICKET_DATA')) {
        //     this.$router.push('PedidoComanda')
        // }
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            this.createPendentOrder(this.sendStoneOrder);
          //  this.sendStonePreTransaction(this.sendStoneOrder);
        };
        req.callbackError = (error: any) => {
        setTimeout(() => { this.cancelPreTransaction()   }, 2000);

        };
        req.body = preTransactionActive;
      
        req.cancelPreTransaction();
    }
    preTransactionDate() {  
        this.processingCreate=true;
        const req = new Request();
        var data = new Date();
        var dia = String(data.getDate()).padStart(2, '0');
        var mes = String(data.getMonth() + 1).padStart(2, '0');
        var ano = data.getFullYear();
        var dataAtual =ano  + mes +dia;
        var preToRemove
        req.callbackSuccess = (response: any) => {
            preToRemove   = response.pre_transactions.find((p: any) => p.pre_transaction.is_active ==true&&p.pre_transaction.pos_reference_id == localStorage.getItem('POS_REFERENCE_ID'));
            console.log(preToRemove)
            if(preToRemove==undefined)
            this.createPendentOrder(this.sendStoneOrder);
           // this.sendStonePreTransaction(this.sendStoneOrder);

            else
            this.cancelLastPreTransaction(preToRemove.pre_transaction.pre_transaction_id)
            this.preTransactionSame()
        };
        req.callbackError = (error: any) => {
            this.createPendentOrder(this.sendStoneOrder);
         //   this.sendStonePreTransaction(this.sendStoneOrder);

        };
        req.body = {
            "date":dataAtual,
            "establishment_id":localStorage.getItem('STONE_ESTABLISHMENT_ID')
        };
      
        req.getTransactionDate();
    }

    preTransactionSame() {  
        this.processingCreate=true;
        const req = new Request();
        var data = new Date();
        var dia = String(data.getDate()).padStart(2, '0');
        var mes = String(data.getMonth() + 1).padStart(2, '0');
        var ano = data.getFullYear();
        var dataAtual =ano  + mes +dia;
        var preToRemove
        var minuteMinus=ano+"-"+mes+"-"+dia+" "+data.getHours()+":"+(data.getMinutes()-2)+":"+data.getSeconds()
        var minutePlus=ano+"-"+mes+"-"+dia+" "+data.getHours()+":"+(data.getMinutes()+2)+":"+data.getSeconds()
        req.callbackSuccess = (response: any) => {
            preToRemove   = response.pre_transactions.find((p: any) => p.pre_transaction.processed ==true&&p.pre_transaction.pos_reference_id == localStorage.getItem('POS_REFERENCE_ID')&&  p.pre_transaction.amount==this.order.TOTAL_VALUE && p.pre_transaction.created_at<=minutePlus && p.pre_transaction.created_at>=minuteMinus );
            console.log(preToRemove)
            if(preToRemove!=undefined)
            this.instantAprove=true
        };
        req.callbackError = (error: any) => {
        };
        req.body = {
            "date":dataAtual,
            "establishment_id":localStorage.getItem('STONE_ESTABLISHMENT_ID')
        };
      
        req.getTransactionDate();
    }
    exitKiosk() {
        if (this.exitCounter > 15) {
            KioskPlugin.exitKiosk();
            Util.syncCodePush();
        }
        else {
            this.exitCounter += 1;
        }
    }

    beforeDestroy() {
        if (this.preTransactionActive) this.cancelPreTransaction();
        clearInterval(this.getOrderDataInterval);
        localStorage.setItem('atualizar', '');
        clearInterval(this.posInterval);
        clearInterval(this.noteContinterval);
        this.posInterval = undefined;
        Util.removeAddProdutos();
        //document.location.reload();
    }

    closeModalCardPayment() {
        this.loadedPos = false;
      //  this.hasLast=true;
        this.posAvailable();
        // console.log("teste1")
        this.noteCont = 0
        //         if(this.posInterval==undefined)
        //         this.posInterval= setInterval(()=>{if(this.posStatus=="processing")this.posAvailable(); console.log( this.posInterval);         clearInterval(this.posInterval);
        //     }, 2000);
        //         else{
        //         clearInterval(this.posInterval);
        //         this.posInterval = undefined;
        //         this.posInterval= setInterval(()=>{if(this.posStatus=="processing")this.posAvailable();console.log( "teste"); clearInterval(this.posInterval);
        //  }, 2000);
        //     }
        clearInterval(this.noteContinterval)
        if (this.posStatus != "processing")
            if (this.preTransactionActive) {
                clearInterval(this.posInterval);
                this.posInterval = undefined;
                this.cancelPreTransaction();

            }
        this.showModalCardPayment = false
        this.showModalConfirmNoteV = false
        this.showModalConfirmNote = false

    }

    closeModalLoading() {
        if (this.preTransactionActive) this.cancelPreTransaction();
        this.showModalLoadingCreateOrder = false
        this.showModalCpfInvalido = false
        this.showModalLoadingFinishOrder = false
        clearInterval(this.getOrderDataInterval);
        this.getOrderDataInterval = undefined;
        this.showModalConfirmNoteV = false
        this.showModalConfirmNote = false
    }

    alterarTeclado(digit: string) {
        if (digit == "\u21E7") {
            this.virtualKeyboard = [{ value: "." }, { value: "@" }, { value: "/" }, { value: "-" }, { value: "_" }, { value: ":" }, { value: "!" }, { value: "?" }, { value: "´" }, { value: "`" }, { value: "Q" }, { value: "W" }, { value: "E" }, { value: "R" }, { value: "T" }, { value: "Y" }, { value: "U" }, { value: "I" }, { value: "O" }, { value: "P" },
            { value: "nan" }, { value: "A" }, { value: "S" }, { value: "D" }, { value: "F" }, { value: "G" }, { value: "H" }, { value: "J" }, { value: "K" }, { value: "L" },
            { value: "\u21EA" }, { value: "Z" }, { value: "X" }, { value: "C" }, { value: "V" }, { value: "B" }, { value: "N" }, { value: "M" }];

        } else if (digit == "\u21EA") {
            this.virtualKeyboard = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" }, { value: "7" }, { value: "8" }, { value: "9" }, { value: "0" }, { value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
            { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
            { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];
        }
    }

    hideModal() {
        this.produtoNotFound = false;
        this.quantityItem = this.order.AMOUNT_OF_ITEMS
    }
}