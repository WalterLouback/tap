import { Component, Prop, Vue } from 'vue-property-decorator';
import ImageHeader from '../components/ImageHeader.vue';
import Card from '../components/Card.vue';
import InputText from '../components/InputText.vue';
import InvisibleHeader from '../components/InvisibleHeader.vue';
import SelectField from '../components/SelectField.vue';
import OptionField from '../components/OptionField.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import TextLink from '../components/TextLink.vue';
import Product from '../components/Product.vue';
import Request from '@/ts/Request';
import moment from 'moment'
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import VirtualKeyboard from '@/modules/shared-components/VirtualKeyboard.vue';
import VirtualKeyboardCPF from '@/modules/shared-components/VirtualKeyboardCPF.vue';

import DefaultButtonOrd from '../components/DefaultButtonOrd.vue';
import DefaultButton from '../components/DefaultButton.vue'
import Util from '@/ts/Util';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import StoreCard from '@/modules/shared-components/StoreCard.vue';
import LoadingPhases from '../components/LoadingPhases.vue';
import Orders from '../../Orders';
import EmptyState from '../components/EmptyState.vue'
import router from '../router';
import { faFileContract } from '@fortawesome/pro-regular-svg-icons';

declare var M: any;
declare var $: any;
declare var Keyboard: any;
@Component({
    components: { ImageHeader, Card, InputText, DefaultButtonOrd, DefaultButton, SelectField, OptionField, ModalCodigo, InvisibleHeader, TextLink, Product, HeaderCircleButton, HeaderBackground, LoginModal, LoadingPhases, StoreCard, EmptyState, VirtualKeyboard, VirtualKeyboardCPF }
})

export default class Produtos extends Vue {
    //Declarations
    storeId: any = localStorage.getItem('storeId') || '';
    store: any = (localStorage.getItem('STORE_' + this.storeId) && localStorage.getItem('STORE_' + this.storeId) != 'undefined') ? JSON.parse(localStorage.getItem('STORE_' + this.storeId) || "{}") : {};
    // storeCache = JSON.parse(localStorage.getItem('STORES_CACHE')|| '[]');
    // store = this.storeCache.find( (s:any)=>s.id == localStorage.getItem('storeId')) || {};
    // store: any = JSON.parse(localStorage.getItem('STORE') || "{}");
    event: any = (localStorage.getItem('EVENT') && localStorage.getItem('EVENT') != 'undefined') ? JSON.parse(localStorage.getItem('EVENT') || "{}") : {};
    eventSalesMethod: any = localStorage.getItem('EVENT_SALES_METHOD');
    currentOrder: any = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
    storeCardEvent: any = !!(localStorage.getItem('IS_AN_EVENT') || '');
    lojaUnica = localStorage.getItem('STORE_UNICA');
    testeUnica: boolean = true;
    modalPopUp: boolean = false;
    //Menus
    menus: any = this.store.menus ? this.store.menus.currentWorkshift.concat(this.store.menus.notCurrentWorkshift) || [] : [];
    categories: any = [];
    productsFiltered: any = [];
    itemToRemove: any = {};
    virtualKeyboard = [{ value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
    { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
    { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];
    virtualKeyboardCpf = [{ value: "1" }, { value: "2" }, { value: "3" }, { value: "4" }, { value: "5" }, { value: "6" },
    { value: "7" }, { value: "8" }, { value: "9" }, { value: "C" }, { value: "0" },];
    menuNotFound: boolean = false;
    showModalLojaFechada: boolean = false;
    seeMore: boolean = false;
    noFilter: boolean = false;
    test: string = "";
    showSearch: boolean = false;
    verifyVK: boolean = true;
    descente: boolean = false;
    userId: string = localStorage.getItem('USER_ID') || ''
    cover: string = "";
    logo: string = "";
    scroll: any = 0;
    collapseHeader: boolean = false;
    fixCategories: boolean = false;
    favorite: boolean = false;
    keyBoardShow: boolean = true;
    showModalClosed: boolean = false;
    // key = '';
    inputEle = document.getElementsByClassName('gLFyf gsfi')[0];
    codigorecebido: string = '';
    numb = 0;
    inputSearch: any;
    backButton: any = { iconClass: 'fal', icon: 'chevron-left', callback: this.goBack }
    // otherButton: any        = [{iconClass: (this.favorite) ? 'fas': 'fal', icon: 'heart', colorClass: (this.favorite) ? 'font-red':'default', callback: this.favoriteStore}]
    //otherButton: any = Orders.share ? [{iconClass: (this.favorite) ? 'fas': 'fal', icon: 'heart', colorClass: (this.favorite) ? 'font-red':'default', callback: this.favoriteStore},{iconClass: 'fal', icon: 'share-alt', colorClass: 'default', callback: this.openWebShare}]: [];
    otherButton: any = [{ iconClass: 'fal', colorClass: 'font-primary', icon: (this.showSearch) ? 'times' : 'search', callback: () => { this.toggleShowSearch(); this.showSearch = !this.showSearch; } }];
    showModalLogin: boolean = false;
    expandedStoreCard: boolean = false;
    pageLoaded: boolean = true;
    temp_listener: any = null;
    favoritedKey: number = 123;
    selectedCategory: number = 0;
    target_offset: any;
    shareOptions = {
        message: 'share this', // not supported on some apps (Facebook, Instagram)
        subject: 'the subject', // fi. for email
        files: ['', ''], // an array of filenames either locally or remotely
        url: 'https://www.website.com/foo/#bar?a=b',
        chooserTitle: 'Pick an app', // Android only, you can override the default share sheet title
        appPackageName: 'com.apple.social.facebook', // Android only, you can provide id of the App you want to share with
        iPadCoordinates: '0,0,0,0' //IOS only iPadCoordinates for where the popover should be point.  Format with x,y,width,height
    };
    categorias: any;
    isVisible: boolean = localStorage.getItem('IS_VISIBLE') && localStorage.getItem('IS_VISIBLE') == 'false' ? false : true;
    barCodeFromExplorar: string = localStorage.getItem('BARCODE_EXPLORAR') || '';
    produtoNotFound: boolean = false;

    Unica: any = this.testeUnica;
    showModalloadingP: boolean = false;
    pesquisa: string = "";
    modalIdade: boolean = false;
    lockId: any;
    open: boolean = false;
    haveLock: boolean = false;
    openInstruction: boolean = false;
    timeoutInstruction:any = undefined
    showModalAddCpf: boolean = false
    userCpf = "";
    showModalCpfInvalido: boolean = false;
    underAge: boolean = false
    showModalSenha: boolean = false;
    passwordLock = "";
    showModalErroPassword: boolean = false;
    validacao: boolean = false;

    selectCategory(index: number) {
        this.selectedCategory = index;
        if (this.categories && this.categories.length && this.categories[index]) {
            this.categories.forEach((category: any) => {
                if ($('#categoria' + category.id).hasClass('categoriaSelecionada'))
                    $('#categoria' + category.id).removeClass('categoriaSelecionada');
            });

            $('#categoria' + this.categories[index].id).addClass('categoriaSelecionada');

        }
    }

    created() {
        document.body.click()
    }

    mounted() {
        // localStorage.getItem("OPENED")=='true'?this.showModalClosed=false:this.showModalClosed=true
        this.menus.forEach((menu:any)=>{         
            this.categories= this.categories.concat(menu.productGroups);
        });
        
        // localStorage.setItem('USER_NAME','');

        // if(!localStorage.getItem('STORES_CACHE'))localStorage.setItem('STORES_CACHE','[]');
        if (this.lojaUnica != 'U') this.testeUnica = false;
        this.Unica = this.testeUnica;
        if (this.barCodeFromExplorar != '') this.showModalloadingP = true;

        if (this.currentOrder.STORE_ID != this.storeId) this.currentOrder = [];
        localStorage.setItem(('STORE'), JSON.stringify(this.store));
        // Manter entrega no balcão para o totem
        localStorage.setItem('DELIVER_AT', 'B');
        Util.setBackButtonBehavior(() => { });
        $('#search_event').focusin(function () {
            $('#search_icon_btn').addClass('primary-color');
        });

        $('#search_event').focusout(function () {
            $('#search_icon_btn').removeClass('primary-color');
        });
        this.scroll = $(window).scrollTop();
        if (this.scroll > 200) this.fixCategories = true;
        if (this.scroll < 200) this.fixCategories = false;

        const self = this;

        if (!this.store || this.store.id != localStorage.getItem('storeId')) {
            this.store = { id: 0 };
            this.currentOrder = [];
            this.menus = [];
            this.categories = [];
            this.selectedCategory = 0;
            localStorage.setItem('structureId', "");
            // this.pageLoaded = false;
        }
        else {
            this.verifyStoreAbout(this.store);
            //if(Orders.share) this.isFavorite();
            if (this.deliversOnlyToBalcony()) localStorage.setItem('DELIVER_AT', 'B');
            // this.selectCategory(0);
        }
        this.getStoreData();

        if (!this.currentOrder || !this.currentOrder.ORDER_ITEMS) {
            this.currentOrder = {
                "DELIVER_TO": "T",
                "STORE_ID": localStorage.getItem('storeId'),
                "TOTAL_VALUE": 0,
                "USER_ID": localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                "ORDER_ITEMS": [],
                "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
                "STRUCTURE_NAME": localStorage.getItem('STRUCTURE_NAME'),
                "AMOUNT_OF_ITEMS": 0
            };
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.currentOrder));
        }

        this.verifyCategories();
        this.addEventListeners();


    }

    //Define the values oF the object to the request
    data() {
        $(window).scroll(() => {
            this.scroll = $(window).scrollTop();
            if (this.scroll > 200) this.fixCategories = true;
            if (this.scroll < 200) this.fixCategories = false;
        })
        return {
        }
    }

    goToLocation(id: any) {

        // this.categories.forEach(( category:any ) => {
        //     if($('#categoria'+category.id).hasClass('categoriaSelecionada'))
        //         $('#categoria'+category.id).removeClass('categoriaSelecionada');
        // });        

        // const offset = -100;

        // $('html, body').animate({
        //     scrollTop: $("#"+id).offset().top + offset
        // }, 300,  this.stopScrollspy(id));
        //(this.$refs.sectionCategories as HTMLElement).scrollTop;
        // const oldCategory = $('.categoriaSelecionada');

        // $('#categoria'+id).addClass('categoriaSelecionada');

        // const el:any = $('#sectionCategories');

        // let scrollLeft = 0;
        // setTimeout(function() {
        //     let newCategory = $('.categoriaSelecionada');
        //     scrollLeft      = newCategory.position().left + el.scrollLeft();

        //     if (!newCategory.hasClass('last')) scrollLeft -= Number.parseInt(newCategory.css('width'));

        //     el[0].scrollTo(scrollLeft, 0);

        // }, 100);

    }

    stopScrollspy(id: any) {
        // let self = this;
        // const el:any = $('#sectionCategories');

        // setTimeout(function () {
        //     self.categories.forEach(( category:any ) => {
        //         if($('#categoria'+category.id).hasClass('categoriaSelecionada'))
        //             $('#categoria'+category.id).removeClass('categoriaSelecionada');
        //     });        

        //     $('#categoria'+id).addClass('categoriaSelecionada');

        //     let newCategory = $('.categoriaSelecionada');
        //     let scrollTop   = newCategory.position().top + el.scrollTop();

        //     if (!newCategory.hasClass('last')) scrollTop -= Number.parseInt(newCategory.css('height'));

        //     el[0].scrollTo(0, scrollTop);

        //     $('html, body').stop().animate();
        // }, 300)
    }

    getActiveElement(id: any) {
        // $('#categoria' + id).addClass('categoriaSelecionada');

        // let categoriaAnterior = $('.categoriaSelecionada');

        // if(categoriaAnterior.length > 1) {
        //     if(categoriaAnterior.hasClass('categoriaSelecionada')){               
        //         $(categoriaAnterior).removeClass('categoriaSelecionada');
        //         $('#categoria' + id).addClass('categoriaSelecionada');
        //     }
        // }

        // const el: any = $('#sectionCategories');
        // let scrollTop = 0;

        // setTimeout(function () {
        //     let newCategory = $('.categoriaSelecionada');
        //     scrollTop = newCategory.position().top + el.scrollTop();

        //     if (!newCategory.hasClass('last')) scrollTop -= Number.parseInt(newCategory.css('height'));

        //     el[0].scrollTo(0, scrollTop);

        // }, 100);

    }

    beforeDestroy() {
        Util.removeBackButtonBehavior();
        // Util.removeAddProdutos();
        // Util.removeBackButtonBehavior();
    }

    addEventListeners() {
        const self = this;
        // $('#search_product').on('keyup', function(e:any) {
        //     self.productsFiltered = [];
        //     setTimeout(function() {
        //         self.categories.forEach((category:any ) => {
        //             let filterResult = category.products.filter((produto: any) => {
        //                 return (
        //                     !$('#search_product').val() || 
        //                     self.removeSpecialCharacters(produto.name).includes(self.removeSpecialCharacters($('#search_product').val()))
        //                 )
        //             });
        //             self.productsFiltered = self.productsFiltered.concat(filterResult);
        //         });
        //         self.noFilter = !!$('#search_product').val()
        //     }, 1);
        // })
        Util.setAddProdutos((event: any) => {
            const keyName = event.key;

            if (keyName != 'Enter') {
                this.codigorecebido += keyName;
            } else {
                // this.receiveProduct(this.codigorecebido);
                this.receiveBarcode(this.codigorecebido);
                this.codigorecebido = '';
            }
        });

    }

    receiveBarcode(barCode: any) {
        // (this.$route.path !== '/Pedido') this.goToOrder();
        let productId: any = '';
        let categoryId: any = '';
        this.currentOrder = JSON.parse(localStorage.getItem('CURRENT_ORDER') || "{}");
        localStorage.setItem('BARCODE_EXPLORAR', '')
        this.categories.forEach(function (category: any) {
            category.products.forEach((product: any) => {
                if (barCode.includes(product.originalProduct.barCode)) {
                    productId = product.id;
                    categoryId = category.id;
                }
            });

        });
        if (productId) {
            this.goToOrder();
            this.receiveProduct(productId, categoryId);
        } else if (!this.showSearch) this.produtoNotFound = true;
    }


    initScrollSpy() {
        // let el = $('.scrollspy');

        // el.scrollSpy({
        //     activeClass: '.categoriaSelecionada',
        //     getActiveElement: this.getActiveElement
        // });
        // // Evita que o scroll inicia rolagem até embaixo.
        // if(typeof el[0] !== 'undefined'){
        //     this.getActiveElement(el[0].id);
        // }
    }


    removeSpecialCharacters(s: string) {
        s = s ? s.toLowerCase() : '';
        s = s.toLowerCase();
        s = s.replace(new RegExp(/\s/g), '_');
        s = s.replace(new RegExp(/[àáâãäå]/g), 'a');
        s = s.replace(new RegExp(/æ/g), 'ae');
        s = s.replace(new RegExp(/ç/g), 'c');
        s = s.replace(new RegExp(/[èéêë]/g), 'e');
        s = s.replace(new RegExp(/[ìíîï]/g), 'i');
        s = s.replace(new RegExp(/ñ/g), 'n');
        s = s.replace(new RegExp(/[òóôõö]/g), 'o');
        s = s.replace(new RegExp(/œ/g), 'oe');
        s = s.replace(new RegExp(/[ùúûü]/g), 'u');
        s = s.replace(new RegExp(/[ýÿ]/g), 'y');
        s = s.replace(new RegExp(/\W/g), '');
        return s;
    }

    goBack() {
        localStorage.setItem('IS_AN_EVENT', '');
        localStorage.setItem('STORE_BACK', '[]');
        this.$router.go(-1);
    }

    goToStores() {
        // this.$router.push('Lojas');
    }

    chooseProduct(productId: number) {
        this.categories.forEach(function (category: any) {
            let products = category.products;
            if (category.products) {
                category.products.forEach(function (product: any) {
                    if (product.id == productId) {
                        localStorage.setItem('product', JSON.stringify(product));
                    }
                });
            }
        });
        this.$router.push('Produto');

    }

    receiveProduct(productId: any, categoryId: any) {
        localStorage.setItem("BARCODE_EXPLORAR", '')
        // this.categories.forEach(function(category: any) {    
        //     if (category.products) {
        //         category.products.forEach(function(product: any) {
        //             if (product.id == productId) {
        //                 //console.log(category.id, productId
        localStorage.setItem('LAST_PRODUCT', JSON.stringify(productId));
        //             }
        //         });
        //     }
        // });
        //console.log(localStorage.getItem('categorya'))
        //this.selectCategory(2);
        let catId = categoryId;
        //this.categoryIdToIndex(catId);
        // setTimeout(() => this.scrollToProduct(productId),100);  
        this.addItemToOrder(catId, productId);
    }
    categoryIdToIndex(catId: any) {
        let contIndex = 0;
        let index = 0;
        this.categories.forEach(function (category: any) {
            if (category.id == catId) {
                index = contIndex;

            }
            contIndex++;
        });
        this.selectCategory(index)
    }
    //this.addItemToOrder(this.categories.id, productId); 
    verifyCategories() {
        let count = 0;
        this.categories.forEach(function (category: any) {
            category.products.forEach((product: any) => {
                if (product.status == 'A') count++;
            });
        });
        if (count == 0) return true
        else return false;
    }
    goToExplorar() {
        localStorage.setItem('SHOW_EXPLORAR_OVERLAY', "true")
        this.$router.push("Explorar");
    }
    isIntegration() {
        const req = new Request();
        req.route = 'isIntegration';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'STORE_ID': localStorage.getItem('storeId'),
            "NRORG": localStorage.getItem("NRORG"),
            'TOKEN': localStorage.getItem("TOKEN")
        }
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
         localStorage.setItem("INTEGRATION",JSON.stringify(response.integration))
        };
        req.callbackError = function (error: any) {
    
            console.log(error);
        }
        req.send();
    }

    getStoreData() {
        var data = new Date();
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "STORE_ID": localStorage.getItem('storeId'),
                "CURRENT_TIME": {
                    "horas": data.getHours(),
                    "minutos": data.getMinutes()
                }
            };

            req.route = 'getStoreData';
            req.callbackSuccess = (response: any) => {
                this.showModalClosed = !response.store.opened;
                this.lockId = JSON.parse(response.store.lockId)
                this.isIntegration()
                localStorage.getItem("FROM_EXPLORAR") && response.store.lockId ? response.ModalIdade = true : false
                if (response.store.establishmentId != null)
                    localStorage.setItem("STONE_ESTABLISHMENT_ID", response.store.establishmentId)
                setTimeout
                let shouldUpdateScrollSpy = this.store.id === 0;
                self.verifyStoreAbout(response.store);
                self.menus = self.store.menus.currentWorkshift || [];

                let newCategories: any = [];

                this.menus.forEach((menu: any) => {
                    newCategories = newCategories.concat(menu.productGroups);
                });

                self.categories = newCategories;
                self.categories.forEach((category: any) => {
                    category.products = category.products.filter((p: any) => p.status == 'A');
                })



                if (self.menus.length == 0) {
                    self.menuNotFound = true;
                }
                else self.menuNotFound = false;


                self.currentOrder.STORE_NAME = self.store.name;
                if (!self.store.opened) {
                    self.mostraModalLojaFechada();
                }
                // localStorage.setItem('CURRENT_ORDER', JSON.stringify(self.currentOrder));
                localStorage.setItem('ACCEPTED_PAYMENT_METHODS', JSON.stringify(self.store.paymentMethods));
                localStorage.setItem('deliversToTable', self.store.deliversToTable);
                localStorage.setItem('deliversToBalcony', self.store.deliversToBalcony);
                if (self.store.timerTaa == null) localStorage.setItem('TIMER_TAA', '1');
                else localStorage.setItem('TIMER_TAA', (self.store.timerTaa))
                if (self.deliversOnlyToBalcony()) localStorage.setItem('DELIVER_AT', 'B');
                localStorage.setItem('storePaymentMoment', self.store.paymentMoment);
                let storesCache = JSON.parse(localStorage.getItem('STORES_CACHE') || '[]');
                storesCache.push(self.store);
                // localStorage.setItem('STORES_CACHE', JSON.stringify(storesCache));
                if (!self.menuNotFound) localStorage.setItem(('STORE_' + this.storeId), JSON.stringify(self.store));
                localStorage.setItem(('STORE'), JSON.stringify(self.store));

                self.descente = true;
                //if(Orders.share) self.isFavorite();

                if (shouldUpdateScrollSpy) {
                    setTimeout(() => {
                        if (self.categories.length > 0) this.getActiveElement(self.categories[0].id)
                        this.initScrollSpy();
                    });
                    this.selectCategory(0);
                }
                setTimeout(() => {
                    // console.log(this.barCodeFromExplorar)
                    if (this.barCodeFromExplorar.includes("undefined"))
                        this.receiveBarcode(this.barCodeFromExplorar.substring(9,))
                    else if (this.barCodeFromExplorar != '') this.receiveBarcode(this.barCodeFromExplorar);
                    this.showModalloadingP = false;
                });
                self.pageLoaded = true;
                //setTimeout(self.addEventListeners, 1);
                this.store.lockId ? this.haveLock = true : false
                this.store.lockId ? this.modalIdade = true : false

                if (this.store.lockId) {
                    localStorage.setItem("HAVE_LOCK", 'true')
                }
                if (localStorage.getItem('HAVE_LOCK') && localStorage.getItem("FROM_EXPLORAR")) {
                    localStorage.removeItem("HAVE_LOCK")
                    localStorage.removeItem("FROM_EXPLORAR")
                    this.openInstruction = true
                } 
                this.timeoutInstruction = setTimeout(this.instruction, 4000)
            };
            req.callbackError = function (error: any) {

                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }

    }


    deliversOnlyToBalcony(): boolean {
        if (!localStorage.getItem('deliversToTable') || localStorage.getItem('deliversToTable') == 'false' || localStorage.getItem('deliversToTable') == 'undefined') return true;
        else return false;
    }

    deliversOnlyToTable(): boolean {
        if (!localStorage.getItem('deliversToBalcony') || localStorage.getItem('deliversToBalcony') == 'false' || localStorage.getItem('deliversToBalcony') == 'undefined') return true;
        else return false;
    }
    hideModal() {
        this.produtoNotFound = false;

    }
    handleVirtualKeyPress(digit: string) {
        switch (digit) {
            case "\u232B":
                this.popCharacter()
                break;
            case "\u21E7":
                this.alterarTeclado(digit)
                break;
            case "\u21EA":
                this.alterarTeclado(digit)
                break;
            default:
                this.pushCharacter(digit)
                console.log(digit)
                break;
        }
    }
    pushCharacter(digit: string) {
        this.pesquisa = $('#search_product').val() + digit
        $('#search_product').val(this.pesquisa)
        this.efetuarBusca();

    }
    popCharacter() {
        this.pesquisa = this.pesquisa.substring(0, this.pesquisa.length - 1);
        $('#search_product').val(this.pesquisa)
        this.efetuarBusca();
    }
    clearD() {
        throw new Error('Method not implemented.');
    }

    showSearchfocus() {

        //return true
    }

    sendFavoriteRequest() {
        try {
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': localStorage.getItem('storeId'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'createFavoriteEvent';
            req.callbackSuccess = () => {
                this.updateFavoriteList();
            }
            req.callbackError = () => {

            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    deleteFavoriteEvent() {
        try {
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': localStorage.getItem('storeId'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'deleteFavoriteEvent';
            req.callbackSuccess = () => {
                this.updateFavoriteList();
            }
            req.callbackError = () => {

            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    updateFavoriteList() {
        try {
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'favorite';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('FAVORITE_EVENTS_AND_STORES', JSON.stringify(response.calendar.filter((obj: any) => !!obj.favoriteId)));
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    verifyStoreAbout(store: any) {
        if (store.about && store.about.length <= 150) {
            $("#about-store").removeClass('overflow-text');
            $("#about-store").removeClass('escondeTexto');
            this.store = store;
            this.cover = store.imageCover;
            this.seeMore = false;
            this.logo = store.imageLogo;
        } else {
            this.store = store;
            this.cover = store.imageCover;
            this.seeMore = true;
            this.logo = store.imageLogo;

        }
    }

    openLock() {
        this.showModalSenha = false
        this.lockId.forEach((id: number) => {
            this.sendCreateGenLockRequest(id);
        });
    }

    sendCreateGenLockRequest(lockId: number) {
        try {
            const req = new Request();

            req.body = {
                //'LOCK_ID': localStorage.getItem('LOCK_ID'),
                'LOCK_ID': lockId,
                'NRORG': localStorage.getItem('NRORG'),
                'STATUS': 'P',
                'ORD_ORDER_ID': localStorage.getItem('ORD_ORDER_ID'),
                'DOOR_NUMBER': localStorage.getItem('DOOR_NUMBER'),
                'TOKEN': localStorage.getItem("TOKEN"),
                'USER_ID': localStorage.getItem('USER_ID'),
            }
            req.route = 'createLocker';
            req.callbackSuccess = () => {
                localStorage.setItem("MAIOR_IDADE", "true");
                this.modalPopUp = true;
                setTimeout(() => { this.test = "p" }, 4700);
                setTimeout(() => { this.modalPopUp = false, this.test = "" }, 5500);
            }
            req.callbackError = () => {

            }
            req.send();
        } catch (e) {
            console.log(e)
        }
    }
    maiorIdade() {
        this.modalPopUp = true;
        this.test = "p";
        setTimeout(() => { this.modalPopUp = false }, 300);
    }
    menorIdade() {
        localStorage.setItem("MAIOR_IDADE", "false");
    }

    seeMorOrLess() {
        if ($("#about-store").hasClass('overflow-text') == true && $("#about-store").hasClass('escondeTexto') == true) {
            $("#about-store").removeClass('overflow-text');
            $("#seeMore").html('Ver menos');
        }
        else if ($("#about-store").hasClass('overflow-text') == false && $("#about-store").hasClass('escondeTexto') == true) {
            $("#about-store").addClass('overflow-text');
            $("#seeMore").html('Ver mais');
        }
    }

    mostraModalLojaFechada() {
        this.showModalLojaFechada = true;
    }

    fechaModalLojaFechada() {
        this.showModalLojaFechada = false;
        // this.$router.push('Lojas');
    }

    horarioFuncionamento() {
        const date = moment().day();
        return date;
    }

    formatTime(stringDate: string, stringDate2: string) {
        const momentDate: any = moment(stringDate);
        const momentDate2: any = moment(stringDate2);
        const t1: any = momentDate.lang('pt-br').format('HH:mm');
        const t2: any = momentDate2.lang('pt-br').format('HH:mm');
        return t1 + ' às ' + t2;
    }

    favoriteStore() {
        if (localStorage.getItem("USER_ID")) {
            this.favorite = !this.favorite;
            this.otherButton[0].iconClass = (this.favorite) ? 'fas' : 'fal';
            this.otherButton[0].colorClass = (this.favorite) ? 'font-red' : 'default';
            this.favoritedKey = this.favoritedKey + 1;
            if (this.favorite) this.sendFavoriteRequest();
            else this.deleteFavoriteEvent();
        }
        else this.openModalLogin();
    }

    openModalLogin() {
        this.showModalLogin = true;
    }

    closeModalLogin() {
        this.showModalLogin = false;
    }

    closeModalIdade() {
        this.modalIdade = false;
    }

    isFavorite() {
        if (!!localStorage.getItem('FAVORITE_EVENTS_AND_STORES')) {
            var favorites = JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES') || '{}');
            var store: any = JSON.parse(localStorage.getItem('STORE') || "{}");
            // return !!JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES')||'{}').find((e:any)=>e.id == this.store.id);
            this.favorite = !!favorites.find((ev: any) => ev.id == store.id);

        }
        else this.favorite = false;
        this.otherButton[0].iconClass = (this.favorite) ? 'fas' : 'fal';
        this.otherButton[0].colorClass = (this.favorite) ? 'font-red' : 'default';
        this.favoritedKey = this.favoritedKey + 1;
    }

    toggleExpandedStoreCard() {
        const self = this;
        if (this.storeCardEvent) this.expandedStoreCard = false;
        else {
            this.expandedStoreCard = !this.expandedStoreCard;
            if (this.expandedStoreCard) {
                this.temp_listener = Util.getBackButtonBehavior();
                Util.setBackButtonBehavior(this.toggleExpandedStoreCard);
            }
            else {
                if (this.temp_listener) {
                    Util.setBackButtonBehavior(this.temp_listener);
                    this.temp_listener = null;
                }
                else Util.setBackButtonBehavior(this.goBack)
                setTimeout(() => {
                    if (self.categories.length > 0) {
                        this.getActiveElement(self.categories[0].id)
                        this.selectCategory(0);
                        this.initScrollSpy();
                    }
                });
            }
        }
    }

    toggleShowSearch() {
        var show = !this.showSearch;
        this.showSearch = !this.showSearch;
        this.otherButton[0].icon = (this.showSearch) ? 'times' : 'search';
        const self = this;
        if (show && !this.Unica) this.testeUnica = true
        else if (!this.Unica) this.testeUnica = false;

        if (this.showSearch) {
            this.inputSearch = document.getElementById("search_product");
            setTimeout(() => {
                this.inputSearch.select();
                this.inputSearch.focus();
            }, 10);
        }


        if (!show) {
            this.noFilter = false;
            if (this.temp_listener) {
                Util.setBackButtonBehavior(this.temp_listener);
                this.temp_listener = null;
            }
            else Util.setBackButtonBehavior(this.goBack)
            setTimeout(() => {
                if (self.categories.length > 0) {
                    this.getActiveElement(self.categories[0].id)
                    this.selectCategory(0);
                    this.initScrollSpy();
                }
            }, 10);
        }
        else {

            this.temp_listener = Util.getBackButtonBehavior();
            $('#search_product').off('keyup');
            self.noFilter = false;
            console.log(window.innerHeight)
            // console.log(activityRootView.getHeight())
            $('#search_product').on('keyup', function (e: any) {
                self.productsFiltered = [];
                self.categories.forEach((category: any) => {
                    let filterResult = category.products.filter((produto: any) => {
                        return (
                            !$('#search_product').val() ||
                            self.removeSpecialCharacters(produto.name).includes(self.removeSpecialCharacters($('#search_product').val()))
                        )
                    });
                    if (!self.productsFiltered.includes(filterResult))
                        self.productsFiltered = self.productsFiltered.concat(filterResult);


                });

                self.noFilter = !!$('#search_product').val();
                // if (!$('#search_product').val()) self.toggleShowSearch(false);

            })

            // const element = <HTMLElement>this.$refs['search_product_bar'];
            // setTimeout(()=>element.focus(),20);
        }
    }
    efetuarBusca() {
        var self = this;
        self.productsFiltered = [];



        self.categories.forEach((category: any) => {

            let filterResult = category.products.filter((produto: any) => {

                return (
                    !$('#search_product').val() ||
                    self.removeSpecialCharacters(produto.name).includes(self.removeSpecialCharacters($('#search_product').val()))
                )
            });
            if (!self.productsFiltered.includes(filterResult))
                self.productsFiltered = self.productsFiltered.concat(filterResult);


        });

        self.noFilter = !!$('#search_product').val();
        // if (!$('#search_product').val()) self.toggleShowSearch(false);

    }
    goToOrder() {
        localStorage.setItem('DELIVER_AT', 'B');
        if (localStorage.getItem('DELIVER_AT')) this.$router.push('Pedido');
        else {
            if (this.deliversOnlyToBalcony()) {
                localStorage.setItem('DELIVER_AT', 'B')
                this.$router.push('Pedido');
            }
            else {
                localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', 'Produto');
                this.$router.push('DeliverLocation');
            }
        }
    }

    removeItemOrder(categoryId: any, productId: number) {
        var product: any = null;
        if (categoryId) {
            var selectedCategory: any = this.categories.find((c: any) => c.id == categoryId);
            product = selectedCategory.products.find((p: any) => p.id == productId);
        }
        else if (this.productsFiltered) {
            product = this.productsFiltered.find((p: any) => p.id == productId);
        }
        if (product) {

            var item: any = this.currentOrder.ORDER_ITEMS.find((prod: any) => prod.productId == productId);
            var newItem: boolean = false;
            if (!item) {
                item = {
                    quant: 0,
                    productName: "",
                    itemId: 0,
                    note: "",
                    extras: [],
                    productId: productId,
                    estimatedTime: 0,
                    price: 0
                };
                newItem = true;

            }
            var itemnull = this.currentOrder.ORDER_ITEMS.find((prod: any) => prod.productId == productId);
            let index = this.currentOrder.ORDER_ITEMS.findIndex((item: any) => item.itemId == itemnull.itemId);

            let lastItemId: number = Number.parseInt(localStorage.getItem('lastItemId') || "-1");
            item.quant = Number(item.quant) - 1;
            item.productName = product.name;
            item.price = product.price;
            item.estimatedTime = product.estimatedTime;
            item.image = product.image;
            item.itemId = lastItemId + 1;
            item.totalItem = item.quant * item.price;
            localStorage.setItem('lastItemId', item.itemId);
            item.externalId=product.originalProduct.externalId;
            if (newItem) this.currentOrder.ORDER_ITEMS.push(item);
            this.currentOrder.TOTAL_VALUE -= item.price;
            this.currentOrder.TOTAL_VALUE = parseFloat(this.currentOrder.TOTAL_VALUE.toFixed(2));
            this.currentOrder.STORE_NAME = this.store.name;
            this.currentOrder.AMOUNT_OF_ITEMS = Number.parseInt(this.currentOrder.AMOUNT_OF_ITEMS) - 1;
            if (Number(item.quant) == 0) {
                this.currentOrder.ORDER_ITEMS.splice(index, 1);
                // console.log(item.quant);

            }
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.currentOrder));

            this.$forceUpdate();

        } else if (product) {
            localStorage.setItem('product', JSON.stringify(product));
        }
    }

    scrollToProduct(produtoId: number) {
        // this.target_offset = $("#produto"+produtoId).offset();
        // let offsetTop = this.target_offset.top ;
        // $('div#products-list').animate({ 
        //     scrollTop: offsetTop 
        //  },0);
        //  console.log(offsetTop)
        var element: any = document.getElementById("produto" + produtoId);
        if (element) element.scrollIntoView({ block: "start" });
    }

    addItemToOrder(categoryId: any, productId: number) {
        // console.log(productId)
        var product: any = null;
        if (categoryId) {
            var selectedCategory: any = this.categories.find((c: any) => c.id == categoryId);
            product = selectedCategory.products.find((p: any) => p.id == productId);
        }
        else if (this.productsFiltered) {
            product = this.productsFiltered.find((p: any) => p.id == productId);
        }
        if (!product.extras.length && product.originalProduct.extras.length)
            product.extras = product.originalProduct.extras
        if (product && !product.extras.length) {

            var item: any = this.currentOrder.ORDER_ITEMS.find((prod: any) => prod.productId == productId);
            var newItem: boolean = false;
            if (!item) {
                item = {
                    quant: 0,
                    productName: "",
                    itemId: 0,
                    note: "",
                    extras: [],
                    productId: productId,
                    estimatedTime: 0,
                    price: 0
                };
                newItem = true;

            }


            let lastItemId: number = Number.parseInt(localStorage.getItem('lastItemId') || "-1");
            item.quant = Number(item.quant) + 1;
            item.productName = product.name;
            item.price = product.price;
            item.estimatedTime = product.estimatedTime;
            item.image = product.image;
            item.externalId=product.originalProduct.externalId

            item.itemId = lastItemId + 1;
            item.totalItem = item.quant * item.price;
            localStorage.setItem('lastItemId', item.itemId);
            if (newItem) this.currentOrder.ORDER_ITEMS.push(item);
            this.currentOrder.TOTAL_VALUE += item.price;
            this.currentOrder.TOTAL_VALUE = parseFloat(this.currentOrder.TOTAL_VALUE.toFixed(2));
            this.currentOrder.STORE_NAME = this.store.name;
            this.currentOrder.AMOUNT_OF_ITEMS = Number.parseInt(this.currentOrder.AMOUNT_OF_ITEMS) + 1;

            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.currentOrder));
            this.$forceUpdate();
        } else if (product) {
            this.verifyExtras(product)
            //  console.log(product)
            this.$router.push('Produto');
        }
    }
    verifyExtras(product: any) {
        var optionsChosed
        product.extras.forEach((extra: any) => {
            optionsChosed = extra.options.filter((option: any) => option.active == 1);
            extra.options = optionsChosed;
        });
        //    console.log(product)
        localStorage.setItem('product', JSON.stringify(product));

    }

    getQuantityInCurrentOrder(id: number) {
        if (!this.currentOrder.ORDER_ITEMS || !this.currentOrder.ORDER_ITEMS.length) return 0;
        var itens: any = this.currentOrder.ORDER_ITEMS.filter((obj: any) => obj.productId == id);
        var quantity: number = 0;
        if (itens && itens.length) {
            itens.forEach(
                (item: any) => {
                    quantity += Number(item.quant);
                }
            );
        }
        return quantity;
    }

    openWebShare() {
        const w: any = window;
        var idType: string = (this.storeCardEvent) ? '&et=e' : '&et=s';
        var suggestionType: string = (this.storeCardEvent) ? 'deste evento' : 'desta loja';
        // var parent: any = (localStorage.getItem('PARENT_EVENT')) ? JSON.parse(localStorage.getItem('PARENT_EVENT')||'{}') : null; 
        var parentEventList: any = (localStorage.getItem('PARENT_EVENT')) ? JSON.parse(localStorage.getItem('PARENT_EVENT') || '[]') : [];
        var parent: string = parentEventList.map((obj: any) => { return obj.id }).join(',');
        var id: string = (parent) ? 'p=' + parent + '&id=' + this.store.id : 'id=' + this.store.id;
        w.plugins.socialsharing.share(Orders.shareMessage + ' ' + Orders.shareUrl + id + idType);
        // console.log(Orders.shareMessage + ' ' + Orders.shareUrl + id + idType);
    }

    goToLerQrMesa() {
        localStorage.setItem('DELIVER_AT', 'T');
        localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
        this.$router.push('LerQrMesa');
    }

    onSuccess(result: any) {

    };

    onError(msg: any) {

    };

    alterarTeclado(digit: string) {
        if (digit == "\u21E7") {
            this.virtualKeyboard = [{ value: "Q" }, { value: "W" }, { value: "E" }, { value: "R" }, { value: "T" }, { value: "Y" }, { value: "U" }, { value: "I" }, { value: "O" }, { value: "P" },
            { value: "nan" }, { value: "A" }, { value: "S" }, { value: "D" }, { value: "F" }, { value: "G" }, { value: "H" }, { value: "J" }, { value: "K" }, { value: "L" },
            { value: "\u21EA" }, { value: "Z" }, { value: "X" }, { value: "C" }, { value: "V" }, { value: "B" }, { value: "N" }, { value: "M" }];

        } else if (digit == "\u21EA") {
            this.virtualKeyboard = [{ value: "q" }, { value: "w" }, { value: "e" }, { value: "r" }, { value: "t" }, { value: "y" }, { value: "u" }, { value: "i" }, { value: "o" }, { value: "p" },
            { value: "nan" }, { value: "a" }, { value: "s" }, { value: "d" }, { value: "f" }, { value: "g" }, { value: "h" }, { value: "j" }, { value: "k" }, { value: "l" },
            { value: "\u21E7" }, { value: "z" }, { value: "x" }, { value: "c" }, { value: "v" }, { value: "b" }, { value: "n" }, { value: "m" }];
        }
    }

    instruction() {
        this.openInstruction = false;
    }

    handleVirtualKeyPressCpf(digit: string) {
        switch (digit) {
            case "C":
                this.clearUserCpf();
                break;
            case "\u232B":
                this.popCharacterInCpf();
                break;
            default:
                this.pushCharacterToCpf(digit);
                break;
        }
    }

    clearUserCpf() {
        this.userCpf = "";
    }

    popCharacterInCpf() {
        var uI = Array.from(this.userCpf);
        uI.pop();
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
    }

    pushCharacterToCpf(c: string) {
        var uI = Array.from(this.userCpf);
        if (uI.length == 14) uI.shift();
        uI.push(c);
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
    }

    handleVirtualKeyPressPassword(digit: string) {
        switch (digit) {
            case "C":
                this.clearPasswordLock();
                break;
            case "\u232B":
                this.popCharacterInPassword();
                break;
            default:
                this.pushCharacterToPassword(digit);
                break;
        }
    }

    clearPasswordLock() {
        this.passwordLock = "";
    }

    popCharacterInPassword() {
        var uI = Array.from(this.passwordLock);
        uI.pop();
        this.passwordLock = Util.applyMask(uI.join(""), "______");
    }

    pushCharacterToPassword(c: string) {
        var uI = Array.from(this.passwordLock);
        if (uI.length == 6) uI.shift();
        uI.push(c);
        this.passwordLock = Util.applyMask(uI.join(""), "______");
    }

    validarCPF(inputCPF: string) {
        var soma = 0;
        var resto = 0;
        var aux = '';
        if (inputCPF == '00000000000') return false;
        for (var i = 1; i <= 9; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        for (i = 1; i <= 9; i++) {
            for (var j = 1; j <= 11; j++)
                aux = aux.concat(String(i))
        }
        if (aux.includes(inputCPF)) return false
        if ((resto == 10) || (resto == 11)) resto = 0;
        //console.log(parseInt(inputCPF.substring(9, 11)), resto)
        if (resto != parseInt(inputCPF.substring(9, 10))) return false;
        soma = 0;
        for (i = 1; i <= 10; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(inputCPF.substring(10, 11))) return false;
        return true;
    }

    cpfadded() {
        if (this.validarCPF(this.userCpf.replace('.', '').replace('.', '').replace('-', '')) || this.userCpf == '') {
            this.showModalAddCpf = false;
        } else {
            this.showModalCpfInvalido = true;
        }
    }

    getNameByCPF(cpf: number) {
        const req = new Request();
        req.route = 'userExistsByCpfAndReturnData';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'CPF': cpf,
            'PASSWORD': this.passwordLock
        };
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            var dataAtual = Date.parse(String(new Date()));
            var idade = (dataAtual - Date.parse(response.response.user.birthDate.date)) / 31560000000
            if (idade >= 18 && this.passwordLock == "") {
                this.validacao = true
                this.showModalSenha = true
            } else if (idade <= 18) {
                this.validacao = true
                this.underAge = true
            }
            if (idade >= 18 && this.passwordLock != '' && response.response.password == response.response.user.password) {
                this.openLock()
            }
            if (response.response.password != response.response.user.password && this.validacao == false && this.passwordLock != '') {
                this.showModalErroPassword = true
            }
            this.cpfadded();
        };
        req.callbackError = (error: any) => {
            console.log(error)
            this.cpfadded();
        };

        req.send();
    }
    
}