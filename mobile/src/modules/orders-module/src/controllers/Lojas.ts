import { Component, Prop, Vue } from 'vue-property-decorator'
import SectionArea from '../components/SectionArea.vue'
import TextLink from '../components/TextLink.vue'
import InputText from '../components/InputText.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import EmptyState from '../components/EmptyState.vue'
import Card from '../components/StoreCard.vue'
import Request from '@/ts/Request'
import Util from '../ts/Util';
import moment from 'moment';
@Component({
    components: { SectionArea, TextLink, InputText, DefaultButtonOrd, HeaderFluxo, Card, ModalCodigo,EmptyState }
})

export default class Lojas extends Vue {

    stores: any = JSON.parse(localStorage.getItem('stores') || '[]') || [];
    storesLoaded: boolean = false;
    showModalTelaCartoes: boolean = false;
    mainCartao : any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
    card: any;
    creditCards: Array<object> = [];
    userData: any = JSON.parse(localStorage.getItem('USER_DATA') || '[]');
    lastDate: any = localStorage.getItem('LAST_DAY_ESCOLHER_UNIDADE') || '';
    
    mounted() {
        let day = JSON.stringify(moment().date());
        if( !this.lastDate || day != this.lastDate ){
        this.$router.push('EscolherUnidade');
        }
        if (!localStorage.getItem('STRUCTURE_ID')) {
        this.$router.push('EscolherUnidade');
        }
        Util.setBackButtonBehavior(this.goBack);
        if (this.stores && this.stores.length > 0) this.prepareStores(this.stores);
        this.getCreditCards();
        this.getStores();
        localStorage.setItem('CURRENT_ORDER', "");
        localStorage.setItem('currentItem', "");
        localStorage.setItem('ALL_PAYMENT_METHODS', '[]');
        localStorage.setItem('METHOD_SELECTED', '');
        // localStorage.setItem('FULL_METHOD_SELECTED', '{}');
    }
    
    horarioFuncionamento( day:any ) {
        const date = moment().day();
        const workTime = day.find(( d:any ) => d.DAY == date )
        return workTime;
    }

    verifyMainCard(){ 
        this.mainCartao = this.userData.MAIN_CARD_ID;
        this.card = this.findMainCard(this.mainCartao, this.creditCards);
        if(!this.card) this.showModalTelaCartoes = true;
    };

    findMainCard(mainCartao: number, cards: any) {
        return cards.find((card:any) => card.ID == mainCartao);
    }

    goBack() {
        if(localStorage.getItem('ENTERED_ORDERS_FROM')) this.$router.push(localStorage.getItem('ENTERED_ORDERS_FROM')||'Lojas');
        else this.$router.go(-1);
    }

    changeStructure(){
        this.$router.push('EscolherUnidade');
    }

    selectStore(storeId: number) {
        localStorage.setItem('storeId', storeId.toString());
        this.$router.push('Produtos');
    }

    getStores() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID')
            };
            req.route  = 'getStores';
            req.callbackSuccess = function(response: any) {
                let stores  = response.stores;

                self.prepareStores(stores);

                localStorage.setItem('stores', JSON.stringify(self.stores));
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                console.log(e);
            }
    }

    prepareStores(stores: any) {
        stores.forEach((store: any) => {
            if (!store.deliversToTable && !store.deliversToBalcony)
                store.opened = false
        });

        let openedStores = stores.filter((s: any) => s.opened == true)
        let closedStores = stores.filter((s: any) => s.opened == false)
        
        stores = openedStores.concat(closedStores)

        this.stores = stores;
        this.storesLoaded = true;
    }

    getCreditCards() {
        try {
            const self = this;
            const req = new Request();
                req.body = {
                    'USER_ID': localStorage.getItem('USER_ID'),
                    'TOKEN': localStorage.getItem('TOKEN'),
                    'NRORG': localStorage.getItem('NRORG'),
            };
            req.route  = 'getValidCreditCard';
            req.callbackSuccess = function(response: any) {
                let creditCards = response.creditcards;
                self.creditCards = creditCards;
                if (response.creditcardFromParent) self.creditCards.push(response.creditcardFromParent);
                
                self.mainCartao = {};
                creditCards.forEach(( card:any ) => {
                    if(card.IS_MAIN_CARD == true)
                        self.mainCartao = card;
                });

                if(!self.mainCartao && !self.mainCartao.FLAG) self.mainCartao = true;
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e){console.log(e);}
    }

    telaCartoes(){
        this.$router.push('RegisteredCards');
    }

}