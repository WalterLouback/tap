import { Component, Prop, Vue } from 'vue-property-decorator';
import ImageHeader from '../components/ImageHeader.vue';
import Card from '../components/Card.vue';
import InputText from '../components/InputText.vue';
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import SelectField from '../components/SelectField.vue';
import OptionField from '../components/OptionField.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import TextLink from '../components/TextLink.vue';
import TabsHome from '../components/TabsHome.vue';
import Product from '../components/Product.vue';
import Option from '../components/Option.vue';
import ListDefault from '../components/ListDefault.vue';
import Request from '../ts/Request';
import Util from '../ts/Util';
import ThreeOptionsListOrd from '../components/ThreeOptionsListOrd.vue';
import ThreeOptionsListQrOrder from '../components/ThreeOptionsListQrOrder.vue';
import moment from 'moment';
import EmptyState from '../components/EmptyState.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Orders from '../../Orders';
import BottomNavigation from "@/modules/shared-components/BottomNavigation.vue";
import QRCodeImg from "../components/QRCodeImg.vue";
import QRCode from 'qrcode';
import FCM from '../ts/FCM';

declare var M: any;
declare var $: any;

interface Order {
    id: number,
    cancelled: boolean,
    nsu?: any,
    transactionId?: any,
    orderId: string,
    type: string,
    createDate: any,
    deliverTo: string,
    structure?: any,
    paymentMethod: string,
    paymentMethodName: string,
    orderStatus: string,
    paymentStatus?: any,
    total: number,
    nrorg: number,
    note: string,
    status: number,
    creditCardId?: any,
    storeId: number,
    storeName: string,
    storeLogo: string,
    userFirstName: string, 
    userLastName: string,
    userProfilePicture: string,
    numberPhone?: any,
    statusColor?: string,
    items: any,
    isFinished: boolean,
    enabledToCancel: boolean,
    qr: string
}

@Component({
    components: { ImageHeader, Card, InputText, DefaultButtonOrd, SelectField, TabsHome, OptionField, ModalCodigo, HeaderFluxo, TextLink, Product, Option, ListDefault, ThreeOptionsListOrd, EmptyState, HeaderCircleButton, BottomNavigation, ThreeOptionsListQrOrder, QRCodeImg}
})
export default class Produto extends Vue {

    
    orders:Array<Order> = JSON.parse(localStorage.getItem('ORDERS') || '[]');
    currentOrders:Array<Order> = JSON.parse(localStorage.getItem('EXTRATO_CURRENT_ORDERS') || '[]');
    finishedOrders:Array<Order> = JSON.parse(localStorage.getItem('EXTRATO_FINISHED_ORDERS') || '[]');

    notFound: boolean = false; 
    notFoundCurrent: boolean = false;
    notFoundFinished: boolean = false;
    selectedImage: any 
    qrCodeImage: string = '';
    selectedOrder?: Order = {
        id: 0,
        cancelled: false,
        nsu: null,
        transactionId: null,
        orderId:"",
        type:"",
        createDate: null,
        deliverTo:"",
        structure: null,
        paymentMethod:"",
        paymentMethodName:"",
        orderStatus:"",
        paymentStatus: null,
        total: 0,
        nrorg: 0,
        note:"",
        status: 0,
        creditCardId: null,
        storeId: 0,
        storeName:"",
        storeLogo:"",
        userFirstName:"", 
        userLastName:"",
        userProfilePicture:"",
        numberPhone: null,
        statusColor:"",
        items: null,
        isFinished: false,
        enabledToCancel: false,
        qr:""
    };
    orderInstructions: string ='';
    enteredFrom: string = localStorage.getItem('ENTERED_RECENT_ORDERS_FROM') ? localStorage.getItem('ENTERED_RECENT_ORDERS_FROM')||'' : '';

    //Ingressos
    appHasTickets : any = localStorage.getItem('APP_HAS_TICKETS') || false; 
    userData:     any = localStorage.getItem('USER_DATA') ? JSON.parse(localStorage.getItem('USER_DATA') || '{}') : {};
    profileImage: string = localStorage.getItem('USER_DATA') ? this.userData.IMAGE :'';
    fullName:     string = localStorage.getItem('USER_DATA') ? this.userData.FIRST_NAME + ' ' + this.userData.LAST_NAME : '';
    ticketsValidos:number = localStorage.getItem('TICKETS_VALIDOS') ? JSON.parse(localStorage.getItem('TICKETS_VALIDOS') || '0') : 0;
    userTickets:   any = localStorage.getItem('USER_TICKETS') ? JSON.parse(localStorage.getItem('USER_TICKETS') || '[]') : [];
    timeouts:      any = [];
    backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack }

    abas: any = (this.userTickets && this.userTickets.length) ? 
        [
            { id: 1, NAME: 'Em andamento'},
            { id: 2, NAME: 'Finalizadas' }
            // { id: 3, NAME: 'Meus Ingressos'}
        ] :
        [
            { id: 1, NAME: 'Em andamento'},
            { id: 2, NAME: 'Finalizadas' }
        ]
    ;
    logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL')||'bipfun-logo.svg';
    goHome: boolean = false;

    mounted() {
        const self = this;
        Util.removeBackButtonBehavior();
        Util.setBackButtonBehavior(this.goBack);
        this.getOrdersFromUserInOrganization();
        this.goToCorrectTab();
        this.initFCM();

        //Ingressos
        this.appHasTickets = false;
        this.getUserTicketsByOrg();
        this.ticketsValidos = this.userTickets.length;
        if (this.enteredFrom === 'Pedido') this.goHome = true;
        if(this.enteredFrom === 'NominateTicket' || this.enteredFrom === 'QrcodeTicket'){
            this.goToTicketsTab();
            this.enteredFrom = '';
            localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        }
        moment.updateLocale('pt', {
            monthsShort : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            months : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            weekdays : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
            weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
        });
    }

    beforeDestroy() {
        if(this.timeouts){
            for (let i = 0; i < this.timeouts.length; i++) {
                clearTimeout(this.timeouts[i]);
            }
        }
        this.timeouts = [];
        Util.removeBackButtonBehavior();
    }

    goToCorrectTab() {
        const path       = window.location.href.split('/');
        const pathName   = path[path.length - 1];
        
        if (pathName.includes('#')) {
            const tabName = pathName.split('#')[1];
            const instance = M.Tabs.getInstance($('#tabs'));
            instance.select(tabName);
        }
    }

    goToTicketsTab(){
        $('#tabs').tabs("select",'meus_ingressos');
    }

    goBack() {
        // if(localStorage.getItem('ENTERED_RECENT_ORDERS_FROM')) this.$router.push(localStorage.getItem('ENTERED_RECENT_ORDERS_FROM')||'PedidosRealizados');
        // else
        //Not Home
        // Util.removeBackButtonBehavior(); 
        if (this.goHome) {
            this.$router.push('Home')
            localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
        }
        else this.$router.go(-1);
    }

    formatDate(date: string){
        let dateD  = new Date(date);
        var dia    = dateD.getDate().toString(),
            diaF   = (dia.length == 1) ? '0'+dia : dia,
            mes    = (dateD.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF   = (mes.length == 1) ? '0'+mes : mes,
            anoF   = dateD.getFullYear();
        return diaF+"/"+mesF+"/"+anoF;
    }

    formatDateTime(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format(' DD/MM [às] HH:mm');
    }

    clickOrder(orderId: number) {
        localStorage.setItem('ORDER_ID', orderId+'');
        this.$router.push('ExtratoPedido');
    }

    getOrdersFromUserInOrganization() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
            "NRORG": localStorage.getItem("NRORG"),
            "USER_ID": localStorage.getItem("USER_ID"),
            "TOKEN": localStorage.getItem("TOKEN")
            //"MAX_RESULTS": 10
            };
            req.route = 'getOrdersFromUser';
            req.callbackSuccess = function(response: any) {      
                self.orders = response.orders;
                if( self.orders.length == 0) self.notFound = true;
                else self.notFound = false

                let currentOrders = self.orders.filter((s: any) => s.isFinished == false)
                self.currentOrders = currentOrders;
                if( self.currentOrders.length == 0 ) self.notFoundCurrent = true;
                else self.notFoundCurrent = false;

                let finishedOrders = self.orders.filter((s: any) => s.isFinished == true)
                self.finishedOrders = finishedOrders;
                if( self.finishedOrders.length == 0 ) self.notFoundFinished = true;
                else self.notFoundFinished = false;
                // self.orders = currentOrders.concat(finishedOrders)

                localStorage.setItem('ORDERS', JSON.stringify(self.orders));
                localStorage.setItem('EXTRATO_CURRENT_ORDERS', JSON.stringify(self.currentOrders));
                localStorage.setItem('EXTRATO_FINISHED_ORDERS', JSON.stringify(self.finishedOrders)); 
                if(self.enteredFrom === 'Pedido' && self.currentOrders[0].deliverTo == 'B') { 
                    // self.openQrCodeModal(self.currentOrders[0].qr, 'Apresente o código acima para retirar seu pedido.');
                    self.enteredFrom = '';
                    // localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', '');
                }
                this.$forceUpdate();
                //const currentOrders = response.orders.filter((order: any) => order.orderStatus != 'Entregue' && order.orderStatus != 'Cancelado');
                //localStorage.setItem('CURRENT_ORDERS', JSON.stringify(currentOrders));
            };
            req.callbackError = function(error: any) {
                console.log(error);
                self.notFound = true;
            }
            req.blockTouchEvents = false;
            if(localStorage.getItem("USER_ID")) req.send();
        } catch (e) {
            console.log(e);
        }
    }

    openQrCodeModal(qrCode: string, orderInstructions: any = null){
        const im: any = QRCodeImg;
        this.getQrImage(qrCode);
        this.selectedOrder = this.orders.find((obj:Order)=>obj.qr == qrCode);
        if(orderInstructions)
        this.orderInstructions = orderInstructions;
        else if(this.selectedOrder && this.selectedOrder!.deliverTo =='B')
            this.orderInstructions ='Apresente o código acima para retirar seu pedido.';
        else if (this.selectedOrder && this.selectedOrder!.deliverTo =='T')
            this.orderInstructions ='Aguarde a entrega do pedido em sua mesa.';
        im.openModal('qrCode-modal');
    }
   
    getQrImage(qrCode: string){
        QRCode.toDataURL(qrCode,{ errorCorrectionLevel: 'H', scale:6 }, (err, string) => {
                if (err) this.qrCodeImage = '';
                this.qrCodeImage = string;})
    }

    initFCM() {
        new FCM().init(Util.onNewFCMToken, this.onPushOrderTapped, this.forceRerender);
    }

    forceRerender(data: any) {
        this.getOrdersFromUserInOrganization();
    }

    onPushOrderTapped(data: any) {
    }

    //Ingressos

    getUserTicketsByOrg() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "NRORG": localStorage.getItem('NRORG')
            };
            req.route = 'getUserTicketsByOrg';
            req.callbackSuccess = function(response: any) {

                // let appHass : any;
                // if (response.appHass) appHass = response.appHass
                // else appHass = false;
                // localStorage.setItem('APP_HAS_TICKETS', appHass);

                self.userTickets = response.userTickets;
                localStorage.setItem('USER_TICKETS', JSON.stringify(self.userTickets));
                self.ticketsValidos = self.userTickets.length;
                if(self.userTickets.length){
                    self.abas =
                        [
                            { id: 1, NAME: 'Em andamento'},
                            { id: 2, NAME: 'Finalizadas' },
                            { id: 3, NAME: 'Meus Ingressos'}
                        ];
                }
                self.userTickets.forEach((ticket:any) => {
                    if(ticket.userName != self.fullName){
                        const el = '#'+ticket.id
                        $(el).addClass('dependentTicket');
                    }
                });

            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    formatData(ID: any){
        let format: any;
        format = moment(this.userTickets.find((userTicket: any) => userTicket.id == ID).initialDate.date).lang('pt-br').format("DD [de] MMMM, HH[h]mm");
        return format;
    }

    setTicketDetails(ID: any){
        localStorage.setItem('TICKET_DETAIL', JSON.stringify(this.userTickets.find((ticket: any) => ticket.id == ID)));
        this.$router.push({ name: "QrcodeTicket" });
    }
}