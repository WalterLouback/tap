import { Component, Prop, Vue } from 'vue-property-decorator'
import ImageHeader from '../components/ImageHeader.vue'
// import Card from '../components/Card.vue'
import InputText from '../components/InputText.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import TextLink from '../components/TextLink.vue'
import Product from '../components/Product.vue'
import Option from '../components/Option.vue'
import OrderItem from '../components/OrderItem.vue'
import SelectFieldController from '../ts/SelectFieldController'
import Request from '@/ts/Request'
import DefaultButton from '../components/DefaultButton.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import moment from 'moment';
import BiometryCheck from '../ts/BiometryCheck';
import Util from '@/ts/Util';
import HeaderRelease from '../components/HeaderRelease.vue';
import Orders from '../../Orders';
import LoadingPhases from '@/modules/shared-components/LoadingPhases.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import ButtonCard from '../components/ButtonCard.vue';
import Minicard from '@/modules/shared-components/Minicard.vue';
import SCLoadingDots from '@/modules/shared-components/SCLoadingDots.vue';
import VirtualKeyboard from '@/modules/shared-components/VirtualKeyboard.vue';
import { throws } from 'assert';
import { faFileContract } from '@fortawesome/pro-light-svg-icons';
import router from '@/router';

declare var M: any;
declare var $: any;
declare var KioskPlugin: any;

declare var Card: any;
@Component({
    components: { ImageHeader, InputText, DefaultButtonOrd, DefaultButton, SelectField, OptionField, ModalCodigo, HeaderFluxo, TextLink, Product, Option, OrderItem, HeaderRelease, LoadingPhases, LoginModal, HeaderCircleButton, ButtonCard, Minicard, SCLoadingDots, VirtualKeyboard }
})

export default class PedidoComanda extends Vue {
    order: any = JSON.parse(localStorage.getItem('ORDER_TICKET_DATA') || "{}");
    orgData: any = JSON.parse(localStorage.getItem('ORGANIZATION_DATA') || "{}");
    orderItems: any = (this.order.products.length) ? this.order.products : [];
    orderTotalValue: number = (this.order.total_value) ? this.order.total_value : 0;
    orderTotalValueOrigin: number = this.orderTotalValue;
    orderTotalAmount: number = 0;
    paymentMethods: Array<object> = JSON.parse(localStorage.getItem('ACCEPTED_PAYMENT_METHODS') || '[]');
    comandasUnidas: any = JSON.parse(localStorage.getItem("TICKETS")||'{}');
    structures: Array<object> = [];
    multComanda: boolean = localStorage.getItem('ORDER_TICKET_DATAS')!= null? true :false  
    allPaymentMethods: any = JSON.parse(localStorage.getItem('ALL_PAYMENT_METHODS') || '[]');
    paymentMethodSelected: any = localStorage.getItem('METHOD_SELECTED') || '';
    fullMethodSelected: any = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '{}');
    structureSelects: Array<Array<object>> = [];
    itemToRemove: any = {};
    adicionarComanda:boolean=true;


    store: any = JSON.parse(localStorage.getItem('STORE') || '[]');
    passwordValue: any = '';
    lastPasswordRequest: any = localStorage.getItem('LAST_ORDER_PASSWORD_REQUEST');
    PAYMENT_METHOD: any;
    STRUCTURE_ID: any;
    alreadyHasStructure: boolean = false;
    lastStructure: any = localStorage.getItem('STRUCTURE_ID') || '';
    currentStructure: any = localStorage.getItem('currentStructure');
    lastDayOfLastStructure: any = localStorage.getItem('LAST_DAY_OF_LAST_STRUCTURE') || '';
    getOrderDataInterval: any;
    exitCounter: any = 0;
    orderP: any;
    /* Modais */
    showModalSucesso: boolean = false;
    showModalErro: boolean = false;
    showModalRemoverItem: boolean = false;
    showModal: boolean = false;
    showModalTelaCartoes: boolean = false;
    modalPasswordRequest: boolean = false;
    passwordWrong: boolean = false;
    orderSuccess: boolean = false;
    selectPaymentMethod: boolean = false;
    errorMessage: string = '';
    fromPedido:boolean= localStorage.getItem('FROM_PEDIDO')?true:false

    showModalConfirmPaymentMethod: boolean = false;
    showModalChoosePaymentMethod: boolean = false;
    showModalCardPayment: boolean = false;
    showModalPaymentCompleted: boolean = false;
    showModalChangePaymentPassword: boolean = false;
    showModalChangePaymentCard: boolean = false;
    showModalAddPhone: boolean = false;
    showModalLoadingCreateOrder: boolean = false;
    showModalLoadingFinishOrder: boolean = false;
    showModalConfirmAddCpf: boolean = false;
    showModalAddCpf: boolean = false;
    taaPequeno: boolean = false;
    showModalCpfInvalido: boolean = false;
    /* Locais de entrega escolhidos pela loja */



    /* Opções escolhidas até o momento */
    selectedDeliverLocation: string = localStorage.getItem('DELIVER_AT') || '';
    hasMoreDeliverLocations: boolean = false;
    currentPaymentMethod: string = "";
    selectedStructure: any = null;
    acceptFinger: any = localStorage.getItem('ACCEPT_BIOMETRY') || '';
    biometry: any = new BiometryCheck();
    showModalLogin: boolean = false;
    backButton: any = { iconClass: 'fal', icon: 'chevron-left', callback: this.gotobackTickets }
    lp: any = LoadingPhases
    userCards: any = [];

    /*Imgs*/
    cardPaymentImg: string = Util.getImgUrl('stone', '.png');
    receiptImg: string = Util.getImgUrl('invoice', '.png');
    paymentMethodsImg: string = Util.getImgUrl('cards-payment', '.png');
    paymentMainCardImg: string = Util.getImgUrl('card-isolated', '.png');
    greenSymbol: string = Util.getImgUrl('stone-symbol', '.png');
    successefulPaymentImg: string = Util.getImgUrl('happy', '.png');
    tickets: any = JSON.parse(localStorage.getItem("TICKETS") || "{}")

    /*Timer de desativação*/
    logoutTimer: any = {};
    showLogoutMessageTimer: any = {};
    logoutRemainingTime: number = 30;
    logoutTimerStart: boolean = false;
    processedOrder: any = {};
    priceFormatter: any = new Intl.NumberFormat('pt-BR', { minimumFractionDigits: 2 });

    /*flag para desativar transação*/
    preTransactionActive: string = "";

    loginProps: any = JSON.parse(localStorage.getItem('LOGIN_PROPS') ||
        JSON.stringify({
            loginMethods: ['PHONE'],
            introductionMessage: 'Bem-vindo ao BipFun! Entre no app e ',
            introductionMessageHighlight: 'compre sem enfrentar filas.',
            logo: Util.getImgUrl('bipfun-logo', '.svg'),
            nrorg: process.env.VUE_APP_NRORG || '0'
        }));
        logo: string = '';
    
    virtualKeyboard = [ {value: "1"},{value: "2"},{value: "3"},{value: "4"},{value: "5"},{value: "6"},
        {value: "7"},{value: "8"},{value: "9"},{value: "C"},{value: "0"},{value: "\u232B"},];
    
    userPhone       = "";
    userCpf         = "";
    structureData       : any       = {};
    structureAddress    : any       = null;
    orgConvenienceFee   : number    = 0;
    convenienceFee      : number    = 0;
    allCategories       : any       = [];
    finalOrderItems     : string    = "";
    larguraDaTela       : number    = screen.width;
    barCode: any;
    msgModal: string='';
    modal: boolean=false;
    loadingComanda: boolean= false;
    armazenarComandas: any = localStorage.getItem("ORDER_TICKET_DATA");
    currentOrder: any;
    modalLoadingComanda:boolean =false;

    created() {
        this.logo = Util.getImgUrl(this.loginProps.logo.split('.')[0], '.' + this.loginProps.logo.split('.')[1]);
        this.structureData = JSON.parse(localStorage.getItem('STRUCTURE_DATA') || "{}") || {};
        if (this.structureData.addressData) {
            this.structureAddress = this.structureData.addressData;
            if (this.structureAddress && this.structureAddress.PROVINCY && this.structureAddress.PROVINCY.length != 2) this.structureAddress.PROVINCY = "  ";
            if (this.structureAddress && this.structureAddress.CEP && this.structureAddress.CEP.length != 9) this.structureAddress.CEP = "00000-000"
        }
        if (this.orgData.CONFIGURATIONS && this.orgData.CONFIGURATIONS.CONVENIENCE_FEE) {
            this.orgConvenienceFee = parseFloat(this.orgData.CONFIGURATIONS.CONVENIENCE_FEE);
            this.convenienceFee = this.orgConvenienceFee;
        }
        if (this.convenienceFee) {
            this.orderTotalValue += this.orderTotalValue * (this.convenienceFee / 100);
        }
        this.allCategories = JSON.parse(localStorage.getItem("ALL_CATEGORIES") || "[]");
        // this.orderItems.forEach((item:any)=>{this.orderTotalAmount += item.amount })
    
        this.disableConvenienceFee()

    }

    mounted() {
        Util.setBackButtonBehavior(() => { });
        let self = this;
        if (this.larguraDaTela < 900) this.taaPequeno = true;
        //console.log(this.taaPequeno)
        localStorage.removeItem('ORDER_TICKET_DATAS');
        //console.log(this.tickets)

        if (!!this.lastStructure && !!this.currentStructure && moment().date().toString() == this.lastDayOfLastStructure) {
            this.alreadyHasStructure = true;
        }

        /* Tratando formas de pagamento */
        var posCC = (this.paymentMethods) ? this.paymentMethods.findIndex((paymentMethod: any) => paymentMethod.PAYMENT_METHOD == 'CC') : -1;
        if (posCC != -1) {
            /* Carregar cartões do usuário no select caso CC seja uma das formas de pagamento aceitas */
            this.paymentMethods.splice(posCC, 1);
            //this.getCreditCards();
        }
        if (!this.allPaymentMethods || this.allPaymentMethods.length == 0) {
            this.allPaymentMethods = this.store ? this.store.paymentMethods : [];
            if (this.allPaymentMethods.length) {
                this.allPaymentMethods.sort((a: any, b: any) => (a.LABEL > b.LABEL) ? 1 : -1);
                this.allPaymentMethods = this.allPaymentMethods.filter((p: any) => p.PAYMENT_METHOD != 'CC' && !p.LABEL.includes('Dinheiro') && !p.LABEL.includes('dinheiro'));
            }

        }
        if (this.allPaymentMethods.length) {
            this.allPaymentMethods.sort((a: any, b: any) => (a.LABEL > b.LABEL) ? 1 : -1);
            this.allPaymentMethods = this.allPaymentMethods.filter((p: any) => p.PAYMENT_METHOD != 'CC' && !p.LABEL.includes('Dinheiro') && !p.LABEL.includes('dinheiro'));
        }

        // Util.setAddProdutos( (event:any) => {
        //     var keyName = event.key;

        //     if(keyName!='Enter'){
        //         this.barCode+=keyName;
        //     }else{
        //         console.log(this.barCode)
        //         this.getTicketData(this.barCode);
        //         this.barCode='';
        //     }
        // });

    }

    receiveBarcode(barCode:any){
        let comandaRepetida:boolean =false;
        
        
        localStorage.setItem("BARCODE_EXPLORAR", '')
        // console.log(this.comandasUnidas)
        // console.log(this.multComanda)

         if(this.multComanda && this.comandasUnidas)
         this.comandasUnidas.comandasU.forEach((com:any) => {
            // console.log(com.products) 
             // if(com.products)
             //console.log(com.products[0].barcode)
             if(com.products)
              if(com.products[0].barcode.includes(barCode))
             comandaRepetida=true
         })
        if(!comandaRepetida)
        this.getTicketData(barCode);
        else this.mostraModalErro("Comanda Já adicionada.")
    }

    addComandas() {
        // localStorage.setItem("ADD_COMANDA",'true' );
        this.fixProducts()
        this.order= JSON.parse(localStorage.getItem('ORDER_TICKET_DATA') || "{}");
        localStorage.setItem("ORDER_TICKET_DATAS", JSON.stringify(this.order))
        router.push("TicketPayment")
    }
    fixProducts() {
        //  console.log(this.order.products)
        this.order.products = this.order.products.filter((p: any) => {
            if (p.amount > 1)
                return true
            else {
                var aux = 0
                this.order.products.forEach((q: any) => {
                    if (p.product.id == q.product.id) {
                        aux++
                        //       console.log(aux)
                    }
                })
                if (aux == 1)
                    return true
            }
        })
    }

    gotobackTickets() {
        this.$router.push('TicketPayment');
    }

    getTicketData(barCode: string) {
        // console.log(barCode)
        const req = new Request();
        this.modalLoadingComanda = true;
        req.callbackSuccess = (response: any) => {
            // console.log(barCode);
            this.modalLoadingComanda = false;
            if (response.data.paid) {
                this.mostraModalErro("O pagamento desta comanda já foi realizado.");
            }
            else if (!response.data.products.length) {
                this.mostraModalErro("Sem produtos na comanda.");
            }
            else {
                var totalValue = 0;
                response.data.products.forEach((product: any) => {
                    totalValue += product.total_price;
                    totalValue = parseFloat(totalValue.toFixed(2));
                })
                if (!totalValue) {
                    this.mostraModalErro("Sem produtos na comanda.");
                }
                else {
                    response.data.total_value = totalValue;
                    var newOrder = this.order;
                    // console.log( JSON.stringify(response.data.products).concat(newOrder.products))
                    localStorage.setItem("ORDER_TICKET_DATA", JSON.stringify(this.order));
                    this.updateComanda();
                    localStorage.setItem("TICKET_BARCODE", barCode);
                    //this.$router.push('PedidoComanda');
                }
            }
        };
        req.callbackError = (error: any) => {
            this.mostraModalErro("Comanda não encontrada.");
            this.modalLoadingComanda = false;
            console.log(error)
        };
        req.body = barCode;
        if (barCode) req.getTicketData();
    }
    updateComanda() {
        this.orderItems = this.order.products;
        this.orderTotalValue = (this.order.total_value) ? this.order.total_value : 0;
        this.orderTotalValueOrigin = this.orderTotalValue;
    }

    showModals(msg: any) {
        this.msgModal = msg;
        this.modal = true;
    }

    hideModal() {
        this.modal = false;
    }


    switchDeliverLocation() {
        this.$router.push('DeliverLocation');
    }

    telaPaymentMethods() {
        if (!localStorage.getItem('USER_ID')) {
            this.openModalLogin();
        } else {
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));
            this.$router.push('PaymentMethod');
        }
    }

    telaAddCard() {
        this.$router.push('AddCard');
    }

    addProdutos() {
        this.$router.push('Produtos');
    }

    goBack() {
        this.$router.go(-1);
    }



    clickItem(itemId: number) {
        this.itemToRemove = this.order.ORDER_ITEMS.find((item: any) => item.itemId == itemId);
        this.removeItem();
    }
    clickSub(itemId: number) {
        this.itemToRemove = this.order.ORDER_ITEMS.find((item: any) => item.itemId == itemId);
        this.subItem();
    }

    isInAppPayment() {
        return $.isNumeric(this.currentPaymentMethod);
    }

    removeItem() {
        let index = this.order.ORDER_ITEMS.findIndex((item: any) => item.itemId == this.itemToRemove.itemId);
        this.order.TOTAL_VALUE -= this.itemToRemove.price * this.itemToRemove.quant;
        this.order.TOTAL_VALUE = parseFloat(this.order.TOTAL_VALUE.toFixed(2));
        this.order.AMOUNT_OF_ITEMS -= this.itemToRemove.quant;
        this.order.ORDER_ITEMS.splice(index, 1);
        localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));
        this.fechaModalRemoverItem();
        if (this.order.ORDER_ITEMS.length == 0) this.$router.go(-1);
    }
    subItem() {
        if (this.itemToRemove.quant > 1) {
            let index = this.order.ORDER_ITEMS.findIndex((item: any) => item.itemId == this.itemToRemove.itemId);
            this.order.TOTAL_VALUE -= this.itemToRemove.price;
            this.order.TOTAL_VALUE = parseFloat(this.order.TOTAL_VALUE.toFixed(2));
            this.order.AMOUNT_OF_ITEMS--;
            this.itemToRemove.quant--;
            // this.order.ORDER_ITEMS.splice(index, 1); 
            localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.order));

            // if (this.order.ORDER_ITEMS.length == 0) this.$router.go(-1);
        } else this.removeItem();
    }

    structureOptionChange(e: any) {
        let selectIndex: number = Number.parseInt(e.target.name);
        if (this.structureSelects.length > selectIndex + 1) {
            this.structureSelects.splice(selectIndex + 1, this.structureSelects.length - selectIndex - 1);
        }
        this.getStructures(e.target.value, selectIndex);
    }

    choicePaymentMethod(methodId: any) {
        // const method:any = this.allPaymentMethods.find(( payment:any ) => 
        //     (payment.ID == methodId) && !(payment.PAYMENT_METHOD.startsWith('CC') || payment.PAYMENT_METHOD.startsWith('TEF') || payment.PAYMENT_METHOD.startsWith('PICPAY') || payment.PAYMENT_METHOD.startsWith('EXT') ));
        const method: any = this.allPaymentMethods.find((payment: any) =>
            (payment.ID == methodId));

        this.fullMethodSelected = method;
        if (method.PAYMENT_METHOD) {
            this.paymentMethodSelected = method.PAYMENT_METHOD;
        } else this.paymentMethodSelected = method.ID

        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
        localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));

        this.selectPaymentMethod = false;
    }

    getStructures(parent: number, selectIndex: number) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "PARENT_ID": parent
            };
            req.route = 'getStructures';
            req.callbackSuccess = function (response: any) {
                let structures = response.structures;
                if (self.structureSelects.length > selectIndex + 1) {
                    self.structureSelects.splice(selectIndex + 1, self.structureSelects.length - selectIndex - 1);
                }
                if (structures.length > 0) {
                    self.structureSelects.push(structures);
                    SelectFieldController.refresh();
                }
            };
            req.callbackError = function (error: any) {

                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {

            console.log(e);
        }
    }

    getStructure(structureId: number) {
        try {
            if (!localStorage.getItem('structureId')) return;
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_ID": structureId
            };
            req.route = 'getStructureById';
            req.callbackSuccess = function (response: any) {
                self.selectedStructure = response.structure;
                self.currentStructure = self.selectedStructure.name;
                self.alreadyHasStructure = true;
                localStorage.setItem('currentStructure', self.selectedStructure.name);
                localStorage.setItem('STRUCTURE_ID', self.selectedStructure.topLevelStructure.id);
            };
            req.callbackError = function (error: any) {
                localStorage.setItem('structureId', '');
                localStorage.setItem('deliverToBalcony', 'true');
                localStorage.setItem('currentStructure', '');
                self.currentStructure = '';
                self.alreadyHasStructure = false;
                self.showModal = true;
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }



    handleNonExistantPaymentMethod() {
        let lasBalconyMethodSelected;
        if (this.fullMethodSelected && this.fullMethodSelected.FLAG) {
            lasBalconyMethodSelected = this.allPaymentMethods.find((method: any) => method.ID == this.paymentMethodSelected);
            if (!lasBalconyMethodSelected) {
                localStorage.setItem('METHOD_SELECTED', '');
                localStorage.setItem('FULL_METHOD_SELECTED', '{}');
            }
        } else if (this.fullMethodSelected && this.fullMethodSelected.PAYMENT_METHOD) {
            lasBalconyMethodSelected = this.allPaymentMethods.find((method: any) => method.PAYMENT_METHOD == this.paymentMethodSelected);
            if (!lasBalconyMethodSelected) {
                localStorage.setItem('METHOD_SELECTED', '');
                localStorage.setItem('FULL_METHOD_SELECTED', '{}');
            }
        }
    }



    setMainPaymentMethodFromLocalStorage() {
        this.fullMethodSelected = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '');
        this.paymentMethodSelected = this.fullMethodSelected.PAYMENT_METHOD;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
    }

    setFirstPaymentMethodAsSelected() {
        // if(!(this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('CC') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('TEF') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('PICPAY') || this.allPaymentMethods[0].PAYMENT_METHOD.startsWith('EXT') )){
        this.fullMethodSelected = this.allPaymentMethods[0];
        this.paymentMethodSelected = this.fullMethodSelected.PAYMENT_METHOD;
        localStorage.setItem('METHOD_SELECTED', this.paymentMethodSelected);
        localStorage.setItem('FULL_METHOD_SELECTED', JSON.stringify(this.fullMethodSelected));
        // } 
    }

    getPosReferenceId(posSerialNumber: any) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            let pos = response.available_pos_list.find((pos: any) => { return pos.pos_serial_number == posSerialNumber });
            if (!pos) {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.clearModal();
                this.showModalErro = true;
            }
            else {
                localStorage.setItem("POS_REFERENCE_ID", pos.pos_reference_id);
                this.prepareOrder(null, this.paymentMethodSelected);
            }
        };
        req.callbackError = (error: any) => {
            if (error.success == undefined) this.getOrderDataInterval = setTimeout(() => this.getPosReferenceId(posSerialNumber), 5000);
            else {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.clearModal();
                this.showModalErro = true;
            }

        };
        req.body = localStorage.getItem('STONE_ESTABLISHMENT_ID');
        req.getAvailablePos();
    }

    getUserData() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'USER_TYPE_ID': 4,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
            };
            req.route = 'getUserData';
            req.callbackSuccess = (response: any) => {
                this.getPosReferenceId(response.user.POS);
            };
            req.callbackError = (error: any) => {
                this.getOrderDataInterval = setTimeout(() => this.getUserData(), 5000);
            };
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
        } catch (e) {

            console.log(e);
        }

    }

    mostraModalSucesso() {
        this.showModalSucesso = true;
    }

    fechaModalSucesso() {
        this.showModalSucesso = false;
        this.showModalPaymentCompleted = false;
        clearTimeout(this.logoutTimer);
        clearTimeout(this.showLogoutMessageTimer);
        localStorage.setItem('CURRENT_ORDER', '');
        // localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido')
        // this.$router.push('PedidosRealizados');
        this.$router.push('Explorar');
    }

    fechaModalSucessoERetornaParaCompras() {
        this.showModalSucesso = false;
        this.showModalPaymentCompleted = false;
        clearTimeout(this.logoutTimer);
        clearTimeout(this.showLogoutMessageTimer);
        localStorage.setItem('CURRENT_ORDER', '');
        // localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido')
        // this.$router.push('PedidosRealizados');
        this.$router.push('Explorar');
    }

    changePaymentMethod(e: any) {
        // this.currentPaymentMethod = e.target.value;
        console.log(e);
    }

    goToScanQr() {
        localStorage.setItem('QR_FROM', 'PEDIDO');
        this.$router.push('LerQrMesa');
    }

    mostraModalErro(mensagem: string = 'Houve um erro ao processar o seu pedido. Por favor, tente novamente mais tarde.') {
        this.errorMessage = mensagem;
        this.showModalErro = true;
    }

    fechaModalErro() {
        this.errorMessage = "";
        this.showModalErro = false;
    }

    fechaModal() {
        this.showModal = false;
    }

    mostraModalRemoverItem() {
        this.showModalRemoverItem = true;
    }

    fechaModalRemoverItem() {
        this.showModalRemoverItem = false;
    }

    openPasswordRequest() {
        this.modalPasswordRequest = true;
    }

    closePasswordRequest() {
        this.modalPasswordRequest = false;
        this.passwordValue = '';
        this.passwordWrong = false;
    }

    openOrderSuccess(msg: any) {
        this.errorMessage = msg;
        this.orderSuccess = true;
    }

    closeOrderSuccess() {
        this.orderSuccess = false;
        Orders.callback();
    }

    lastNumberCard(lastNumbers: any) {
        let lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
        return lastNumber;
    }

    saveStructures() {
        if (this.selectedDeliverLocation !== 'B') {
            var self = this;
            var arr: any = [];
            const date = moment().date();
            this.structureSelects.forEach(function (select: any, index: number) {
                var selectedOption = $('#select-structure-deliver-' + index).val();
                var option: any = self.structureSelects[index].find((s: any) => s.id == selectedOption);
                arr.push(option.name);
            });
            localStorage.setItem('currentStructure', arr.join(', '));
            localStorage.setItem('lastStructureId', this.STRUCTURE_ID);
            localStorage.setItem('LAST_DAY_OF_LAST_STRUCTURE', date.toString());
        }
    }

    cleanStructureSaved() {
        localStorage.setItem('currentStructure', '');
        localStorage.setItem('lastStructureId', '');
        this.alreadyHasStructure = false;
    }

    verifyOrderPass() {
        this.prepareOrder(null, this.paymentMethodSelected);
    }

    validateOrder() {
        if (!localStorage.getItem('USER_ID')) {
            this.openModalLogin();
        } else try {
            let structureId;
            /* Obtendo local de entrega */
            if (this.selectedDeliverLocation === 'T') {
                if (!!localStorage.getItem('lastStructureId') && localStorage.getItem('lastStructureId') != 'undefined') structureId = localStorage.getItem('lastStructureId');
                structureId = localStorage.getItem('structureId')
            }
            else
                structureId = this.selectedStructure ? this.selectedStructure.id : null;

            let paymentMethod;
            if (this.paymentMethodSelected)
                paymentMethod = this.paymentMethodSelected;

            if (!structureId && this.selectedDeliverLocation === 'T' && this.alreadyHasStructure == false) {
                /* Caso seja para entregar na mesa porém nenhum local tenha sido selecionado */
                this.mostraModalErro('Por favor, escolha um local de entrega.');
            } else if (this.selectedDeliverLocation === 'T' && !this.currentStructure) {
                /* Caso não tenha sido escolhida a forma de pagamento */
                this.mostraModalErro('Por favor, escolha uma mesa para receber seu pedido.');
            } else if (!paymentMethod) {
                /* Caso não tenha sido escolhida a forma de pagamento */
                this.mostraModalErro('Por favor, escolha uma forma de pagamento.');
            } else if (!this.selectedDeliverLocation) {
                this.mostraModalErro('Por favor, escolha um local de entrega.');
            } else {
                /* Setar dados para proceder com o pedido */
                this.STRUCTURE_ID = structureId;
                this.PAYMENT_METHOD = paymentMethod;
                this.verifyOrderPass();
            }
        } catch (e) {

            console.log(e)
        }
    }

    prepareOrder(structureId: any, paymentMethod: any) {
        /* Caso esteja tudo ok para proceder com o pedido */
        var order: any = {
            "DELIVER_TO": "B",
            "STORE_ID": localStorage.getItem('storeId'),
            "TOTAL_VALUE": this.orderTotalValue,
            "USER_ID": localStorage.getItem('USER_ID'),
            "NRORG": localStorage.getItem("NRORG"),
            "ORDER_ITEMS": [],
            "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
            "STRUCTURE_NAME": localStorage.getItem('STRUCTURE_NAME'),
            "AMOUNT_OF_ITEMS": this.orderTotalAmount,
            "BAR_CODE": localStorage.getItem("TICKET_BARCODE") || "",
            "CONVENIENCE_FEE": this.orderTotalValue * (this.convenienceFee / 100)
        }
        /* Setando dados do pedido */
        order.DELIVER_TO = this.selectedDeliverLocation;
        order.STRUCTURE_ID = this.selectedDeliverLocation === 'T' ? structureId : null;
        order.TOKEN = localStorage.getItem('TOKEN');
        order.NOTE = this.fillOrderItems();
        if (this.convenienceFee) {
            order.CONVENIENCE_FEE = this.orderTotalValue * (this.convenienceFee / 100);
        }

        /* Caso método de pagamento tenha recebido o id do cartão de crédito */
        if ($.isNumeric(paymentMethod)) {
            order.PAYMENT_METHOD = 'CC';
            order.CREDIT_CARD_ID = paymentMethod;
        } else {
            /* Se entrega for no balcão */
            // if (this.selectedDeliverLocation === 'B') 
            order.PAYMENT_METHOD = paymentMethod;
            // else {
            /* Em entrega na mesa, utilizar cartão principal */
            // order.PAYMENT_METHOD = 'CC';
        }

        //this.createPendentOrder(order);
        this.showModalAddCpf = true
        this.sendStonePreTransaction(order)
        // this.closeTicket();
        //this.updatePaymentStatus(null);
    }

    finishOrder(order: any) {
        // this.lp.start(this.lp.LOADING, "Realizando pedido...");

        try {
            const self = this;
            const req = new Request();
            order.USER_ID = localStorage.getItem('USER_ID');
            order.TOKEN = localStorage.getItem('TOKEN');
            req.body = order;
            req.route = 'finishOrder';
            // req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                if (self.alreadyHasStructure == false) self.saveStructures();
                localStorage.setItem('LAST_ORDER_ID', '');
                localStorage.setItem('CURRENT_ORDER', '');
                localStorage.setItem('ENTERED_RECENT_ORDERS_FROM', 'Pedido');
                this.showModalCardPayment = false;
                this.startModalSuccessfulPayment();
                // localStorage.setItem('MOST_RECENT_ORDER', JSON.stringify(order));
                // this.lp.start(this.lp.SUCCESS, 'Seu pedido foi realizado com sucesso');
                // self.$router.push('PedidosRealizados');
                // self.$router.push('Home');
                // req.showSuccessMessage('Seu pedido foi realizado com sucesso.');

            };
            req.callbackError = (error: any) => {
                this.showModalCardPayment = false;
                if (error.response.data.errorCode == 12) {
                    //req.showErrorMessage('Seu limite de compras mensal neste cartão foi excedido.');
                    this.lp.start(this.lp.ERROR, 'Seu limite de compras neste cartão foi excedido.');
                }
                else if (error.response.data.errorCode == 7) {
                    //req.showErrorMessage('Esta transação não foi foi autorizada pelo seu cartão.');
                    this.lp.start(this.lp.ERROR, 'Esta transação não foi foi autorizada pelo seu cartão.');
                }
                else { //req.showErrorMessage('Houve um erro ao processar o seu pedido. Por favor, tente novamente mais tarde.');
                    this.lp.start(this.lp.ERROR, 'Esta transação não foi foi autorizada pelo seu cartão.')
                }

                console.log(error);
            }
            // req.callbackTimeout = (error: any) => {
            //     this.lp.start(this.lp.ERROR, 'Não foi possível completar a transação')
            //     console.log(error);
            // }
            // req.loadingMessage = 'Finalizando o pedido...';
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    focusInput(id: any) {
        const offset = -60;
        const el: any = $("#" + id);
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        }, 150);
    }

    openModalLogin() {
        this.showModalLogin = true;
    }
    closeModalLogin() {
        this.showModalLogin = false;
        //this.getCreditCards();
    }

    handleClickError() {
        console.log('yes');
        this.lp.stop();
    }

    handleClickSuccess() {
        this.lp.stop();
        // this.$router.push('PedidosRealizados');
        this.$router.push('Explorar');
    }

    goToSelectTable() {
        localStorage.setItem('DELIVER_AT', 'T');
        this.$router.push('LerQrMesa');
    }

    getMaxEstimatedTime() {
        return this.order.ORDER_ITEMS.reduce((a: any, b: any) => { return (a.estimatedTime > b.estimatedTime ? a : b) }).estimatedTime;
    }

    choosePaymentMethod(payment: any) {
        this.fullMethodSelected = payment;
        this.paymentMethodSelected = payment.PAYMENT_METHOD;
        this.selectPaymentMethod = true;
        this.prepareOrder(null, this.paymentMethodSelected);
    }

    goToPayment() {
        this.currentOrder = {
            "DELIVER_TO": "T",
            "STORE_ID": localStorage.getItem('storeId'),
            "TOTAL_VALUE": this.orderTotalValue,
            "USER_ID": localStorage.getItem('USER_ID'),
            "NRORG": localStorage.getItem("NRORG"),
            "ORDER_ITEMS": [],
            "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
            "STRUCTURE_NAME": localStorage.getItem('STRUCTURE_NAME'),
            "AMOUNT_OF_ITEMS": this.orderTotalAmount,
        };
        this.orderItems.forEach((item: any) => {
            var itemFormatado = {
                estimatedTime: 0,
                extras: [],
                image: "",
                itemId: "",
                note: "",
                price: item.unit_price,
                productId: item.product.id,
                productName: item.product.name,
                quant: item.amount,
                totalItem: item.total_price.toFixed(2)

            };
           
            this.currentOrder.ORDER_ITEMS.push(itemFormatado)
        });

        localStorage.setItem('CURRENT_ORDER', JSON.stringify(this.currentOrder))

        this.$router.push('Pedido');

    }

    openModalCardPayment(payment: any) {
        this.showModalChoosePaymentMethod = false;
        this.showModalLoadingCreateOrder = true;
        this.choosePaymentMethod(payment);

    }

    confirmOperation() {
        this.showModalChoosePaymentMethod = false;
        this.showModalConfirmPaymentMethod = true;
    }

    startModalSuccessfulPayment() {
        this.showModalCardPayment = false;
        this.showModalAddPhone = false;
        this.showModalLoadingFinishOrder = false;
        this.showModalPaymentCompleted = true;
        this.showLogoutMessageTimer = setTimeout(this.startLogoutTimer, 10000);
    }

    startLogoutTimer() {
        this.logoutTimerStart = true;
        setInterval(() => { if (this.logoutRemainingTime >= 1) this.logoutRemainingTime -= 1 }, 1000);
        this.logoutTimer = setTimeout(this.fechaModalSucesso, this.logoutRemainingTime * 1000);
    }

    checkUserDataAndGoToMainCard() {
        if (localStorage.getItem('USER_DATA')) {

            this.confirmOperation();
        }
        else {
            this.showModalLogin = true;
            this.showModalChoosePaymentMethod = false;
        }
    }




    handleVirtualKeyPress(digit: string) {
        switch (digit) {
            case "C":
                this.clearUserPhone();
                break;
            case "\u232B":
                this.popCharacterInPhone()
                break;
            default:
                this.pushCharacterToPhone(digit)

                break;
        }
    }
    handleVirtualKeyPressCpf(digit: string) {
        switch (digit) {
            case "C":
                this.clearUserCpf();
                break;
            case "\u232B":
                this.popCharacterInCpf()
                break;
            default:
                this.pushCharacterToCpf(digit)

                break;
        }
    }
    validarCPF(inputCPF: string) {
        var soma = 0;
        var resto = 0;
        var aux = '';
        if (inputCPF == '00000000000') return false;

        for (var i = 1; i <= 9; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        // console.log(soma)
        for (i = 1; i <= 9; i++) {
            for (var j = 1; j <= 11; j++)
                aux = aux.concat(String(i))
        }
        if (aux.includes(inputCPF)) return false
        //if(inputCPF==i)

        if ((resto == 10) || (resto == 11)) resto = 0;
        //console.log(parseInt(inputCPF.substring(9, 11)), resto)
        if (resto != parseInt(inputCPF.substring(9, 10))) return false;
        soma = 0;
        for (i = 1; i <= 10; i++) soma = soma + parseInt(inputCPF.substring(i - 1, i)) * (12 - i);
        resto = (soma * 10) % 11;

        if ((resto == 10) || (resto == 11)) resto = 0;
        if (resto != parseInt(inputCPF.substring(10, 11))) return false;
        return true;
    }

    pushCharacterToPhone(c: string) {
        var uI = Array.from(this.userPhone);
        if (uI.length == 13) uI.shift();
        uI.push(c);
        this.userPhone = Util.applyMask(uI.join(""), "__ _____-____");
        // this.userPhone = uI.join("")
    }

    popCharacterInPhone() {
        var uI = Array.from(this.userPhone);
        uI.pop();
        this.userPhone = Util.applyMask(uI.join(""), "__ _____-____");
        // this.userPhone = uI.join("")
    }

    clearUserPhone() {
        this.userCpf = "";
    }

    pushCharacterToCpf(c: string) {
        var uI = Array.from(this.userCpf);
        if (uI.length == 14) uI.shift();
        uI.push(c);
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
        // this.userPhone = uI.join("")
    }

    popCharacterInCpf() {
        var uI = Array.from(this.userCpf);
        uI.pop();
        this.userCpf = Util.applyMask(uI.join(""), "___.___.___-__");
        // this.userPhone = uI.join("")
    }

    clearUserCpf() {
        this.userCpf = "";
    }

    waitForPaymentConfirmation(orderId: string) {
        this.getOrderDataInterval = setInterval(() => {
            const req = new Request();
            req.route = 'getOrderData';
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'ORDER_ID': orderId
            };
            req.callbackSuccess = (response: any) => {
                if (response.order.paymentStatus === 'A') {
                    clearInterval(this.getOrderDataInterval);

                }
            };
            req.send();
        }, 5000);
    }
    cpfadded() {
        //  console.log(this.validarCPF(this.userCpf.replace('.','').replace('.','').replace('-','')));
        if (this.validarCPF(this.userCpf.replace('.', '').replace('.', '').replace('-', '')) || this.userCpf == '')
            this.showModalAddCpf = false;
        else {
            this.showModalCpfInvalido = true;
        }

        //console.log(this.validarCPF(this.userCpf.replace('.','').replace('.','').replace('-','')));
        // this.showModalAddCpf = false;
        // else
        // this.showModalCpfInvalido
        // this.showModalConfirmAddCpf = false;
        // this.updatePaymentStatus(this.orderP);
        // this.getPreTransactionData(this.orderP)
    }

    createPendentOrder(order: any) {
        const req = new Request();
        const self = this;
        order.TRANSACTION_ID = this.preTransactionActive;
        order.STONE_NOTE = this.createClaimCheck();
        req.route = 'event/createPendentOrder';
        req.body = order;
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            //  console.log(response);
            this.processedOrder = response.order;
            //this.sendStonePreTransaction(order, response.order.id);
            //this.showModalConfirmAddCpf= true;
            this.getPreTransactionData(response.order.id)
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            // this.updatePaymentStatus(response.order.id) 
            this.orderP = response.order.id;
            //this.printClaimCheck();
            //this.waitForPaymentConfirmation(response.order.id);
            this.showModalLoadingCreateOrder = false;
            this.showModalCardPayment = true;
        };
        req.callbackError = () => {

            this.getOrderDataInterval = setTimeout(() => this.createPendentOrder(order), 5000);
            // this.clearModal();
            // this.cancelPreTransaction();
            //this.errorMessage = "Erro na criação do pedido. Caso já tenha efetuado a compra, procure um atendente do estabelecimento para cancelar o pagamento."
            // this.showModalErro = true; 
        };
        req.callbackTimeout = () => { };
        req.send();

    }



    updateStonePosReferenceId() {
        if (this.store.stoneRecipientId && this.store.stoneRecipientId.substring(0, 13) == "establishment") {
            localStorage.setItem('STONE_ESTABLISHMENT_ID', this.store.stoneRecipientId.substring(13));
            this.getEstablishmentData(this.store.stoneRecipientId.substring(13));
        }
        else { this.getUserData(); }
    }

    getEstablishmentData(stoneEstablishmentId: string) {
        const req = new Request();

        req.body = stoneEstablishmentId;
        req.callbackSuccess = (response: any) => {
            localStorage.setItem('STONE_ESTABLISHMENT_DATA', JSON.stringify(response.establishment));
            this.getUserData();
        };
        req.callbackError = (error: any) => {

        };
        req.getEstablishmentData();
    }


    sendStonePreTransaction(order: any) {
        var paymentMethodName = '';
        var paymentType = 0;
        const req = new Request();
        var paymentMethod: any = this.paymentMethods.find((p: any) => { return p.PAYMENT_METHOD == order.PAYMENT_METHOD }) || {};
        if (paymentMethod.PAYMENT_METHOD.includes('EXT')) {
            paymentMethodName = (paymentMethod.LABEL) ? Util.removeSpecialCharacters(paymentMethod.LABEL) : "";
            paymentType = (paymentMethodName.includes('credito')) ? 2 : 1;
        } else {
            paymentMethodName = (paymentMethod.PAYMENT_METHOD);
            paymentType = (paymentMethodName.includes('TEF_CC_TAA')) ? 2 : 1;
        }
        // adequar quando não for possivel usar o metodo de pagamento TEF_CC_TAA
        // var paymentMethodName = (paymentMethod.LABEL) ? Util.removeSpecialCharacters(paymentMethod.LABEL): "";
        // var paymentType = (paymentMethodName.includes('credito')) ? 2:1; 
        req.body = {
            "establishment_id": localStorage.getItem('STONE_ESTABLISHMENT_ID'),
            "amount_total": parseFloat((parseFloat(order.TOTAL_VALUE) * 100).toFixed()),
            "pos_reference_id": localStorage.getItem('POS_REFERENCE_ID'),
            "payment": {
                "type": paymentType,
                "installment": null
            }
        }
        if (this.store.stoneRecipientId && this.store.stoneRecipientId.substring(0, 13) != "establishment") {
            req.body.enable_split_adjust_in_mdr_math_fail_scenario = true;
            req.body.split_fee_liability = "Recipient",
                req.body.recipients = [{
                    "recipient_id": this.store.stoneRecipientId,
                    "amount": parseFloat((parseFloat(order.TOTAL_VALUE) * 100).toFixed()),
                }]
        }
        req.callbackSuccess = (response: any) => {
            this.preTransactionActive = response.pre_transaction.pre_transaction_id;
            //this.getPreTransactionData(response.pre_transaction.pre_transaction_id, orderId);
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            this.createPendentOrder(order);
        };
        req.callbackError = (error: any) => {
            if (error.success == undefined) {
                this.getOrderDataInterval = setTimeout(() => this.sendStonePreTransaction(order), 5000);
            }
            else if (error.error == "3113" || error.error == "3116") {
                this.updateStonePosReferenceId();
            }
            else {
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
                this.clearModal();
                this.showModalErro = true;
            }
        };
        req.sendStonePreTransaction();

    }

    getPreTransactionData(orderId: any) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            if (response.pre_transaction.processed && !response.pre_transaction.is_active) {
                var preTransactionId = this.preTransactionActive;
                this.preTransactionActive = "";
                this.getTransactionData(orderId, preTransactionId)
                //this.updatePaymentStatus(orderId);
                //this.printClaimCheck();
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
            }
            else if (!response.pre_transaction.processed && !response.pre_transaction.is_active) {
                this.preTransactionActive = "";
                this.cancelOrder(orderId);
                clearInterval(this.getOrderDataInterval);
                this.getOrderDataInterval = undefined;
            }
            else this.getOrderDataInterval = setTimeout(() => this.getPreTransactionData(orderId), 1000);
        };
        req.callbackError = (error: any) => {
            this.getOrderDataInterval = setTimeout(() => this.getPreTransactionData(orderId), 2000);
        };
        req.body = this.preTransactionActive;
        if (this.preTransactionActive) req.getStonePreTransactionStatus();
    }

    getTransactionData(orderId: any, preTransactionId: string) {
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            var transactionData = response.transaction;
            this.showModalCardPayment = false;
            //this.showModalConfirmAddCpf= true;
            this.updatePaymentStatus(orderId, transactionData);
            
        };
        req.callbackError = (error: any) => {
            this.showModalCardPayment = false;
            this.showModalLoadingFinishOrder = true;
            this.updatePaymentStatus(orderId);
        };
        req.body = preTransactionId;
        req.getStoneTransactionData();
    }
    //função para mandar requisição de imprimir no stone
    /*
    printClaimCheck(){
        var establishmentData: any = null;
        var organizationData: any = null;
        var paymentMethodName = '';
        var paymentType =0;
        //if(localStorage.getItem('STONE_ESTABLISHMENT_DATA')){
            //establishmentData = JSON.parse(localStorage.getItem('STONE_ESTABLISHMENT_DATA')||'{}')||null;
        //}
        if(localStorage.getItem('ORGANIZATION_DATA')){
            organizationData = JSON.parse(localStorage.getItem('ORGANIZATION_DATA')||'{}')||null;
        }
        if(this.processedOrder.paymentMethod.includes('EXT')){
            paymentMethodName = (this.processedOrder.paymentMethodName) ? Util.removeSpecialCharacters(this.processedOrder.paymentMethodName): "";
            paymentType = ( paymentMethodName.includes('credito')) ? 3:2; 
        }else{
            paymentMethodName = (this.processedOrder.paymentMethod);
            paymentType = (paymentMethodName.includes('TEF_CC_TAA')) ? 3:2; 
            }
        const req = new Request();
        req.body = {
            "establishment_id": localStorage.getItem("STONE_ESTABLISHMENT_ID"),
            "pos_reference_id": localStorage.getItem("POS_REFERENCE_ID"),
            "amount": this.priceFormatter.format(this.order.total_value),
            "document_number": "              ",
            "company_name": " ",
            "date_time_created": moment().format("DD/MM/YYYY HH:mm"),
            // "writing_area_1":"senha:" + String(this.processedOrder.orderIdentifier),
            //"writing_area_1": "/n/n SENHA: " + String(this.processedOrder.orderIdentifier) + "/n/n",
            "writing_area_4": "/n/n SENHA: " + String(this.processedOrder.orderIdentifier),
             "number": String(this.processedOrder.orderIdentifier),
            "title": this.store.name,
            "footer": "www.teknisa.com",
            "is_number_item": true,
            "address": {
                "street_name": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "street": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "number": (this.structureAddress && this.structureAddress.NUMBER) ? String(this.structureAddress.NUMBER) : " ",
                "neighborhood": (this.structureAddress && this.structureAddress.NEIGHBORHOOD) ? this.structureAddress.NEIGHBORHOOD : " ",
                "zip_code": (this.structureAddress && this.structureAddress.CEP) ?  this.structureAddress.CEP : "00000-000",
                "city": (this.structureAddress && this.structureAddress.CITY ) ? this.structureAddress.CITY : " ",
                "state": (this.structureAddress && this.structureAddress.PROVINCY) ? this.structureAddress.PROVINCY : "  "
            },
            "item": [
            ],
            "payment": [
                {
                "transaction_type": paymentType,
                "amount": this.priceFormatter.format(this.order.total_value),
                }
            ]
        }
        this.order.ORDER_ITEMS.forEach((item:any) => {
            var newItem = {
                "name": item.productName.toUpperCase(),
                "amount": 'R$ ' + this.priceFormatter.format(item.totalItem),
                "quantity": String(item.quant),
                "unitary_value": 'R$ ' + this.priceFormatter.format(item.price)
            }
            req.body.item.push(newItem)
        })
        req.callbackSuccess = (response: any) => {    
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            this.showModalLoadingFinishOrder = false;
            this.startModalSuccessfulPayment();
        };
        req.callbackError = (error: any) => {
            this.getOrderDataInterval = setTimeout(()=>this.printClaimCheck(), 5000); 
        };
        req.claimCheckCreate()
    }
*/
    createClaimCheck() {
        var establishmentData: any = null;
        var organizationData: any = null;
        //if(localStorage.getItem('STONE_ESTABLISHMENT_DATA')){
        //establishmentData = JSON.parse(localStorage.getItem('STONE_ESTABLISHMENT_DATA')||'{}')||null;
        //}
        if (localStorage.getItem('ORGANIZATION_DATA')) {
            organizationData = JSON.parse(localStorage.getItem('ORGANIZATION_DATA') || '{}') || null;
        }

        var paymentMethodName = (this.processedOrder.paymentMethodName) ? Util.removeSpecialCharacters(this.processedOrder.paymentMethodName) : "";
        var paymentType = (paymentMethodName && paymentMethodName.includes('credito')) ? 3 : 2;

        var claimCheck: any = {
            "establishment_id": localStorage.getItem("STONE_ESTABLISHMENT_ID"),
            "pos_reference_id": localStorage.getItem("POS_REFERENCE_ID"),
            "amount": this.priceFormatter.format(this.order.total_value),
            "document_number": "              ",
            "company_name": " ",
            "date_time_created": moment().format("DD/MM/YYYY HH:mm"),
            "number": "@@INSERT@@NUMBER@@HERE@@",
            "title": this.store.name,
            "writing_area_4": "/n/nSenha: " + "@@INSERT@@NUMBER@@HERE@@",
            "footer": "www.teknisa.com",
            "is_number_item": true,
            "address": {
                "street_name": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "street": (this.structureAddress && this.structureAddress.STREET) ? this.structureAddress.STREET : " ",
                "number": (this.structureAddress && this.structureAddress.NUMBER) ? String(this.structureAddress.NUMBER) : " ",
                "neighborhood": (this.structureAddress && this.structureAddress.NEIGHBORHOOD) ? this.structureAddress.NEIGHBORHOOD : " ",
                "zip_code": (this.structureAddress && this.structureAddress.CEP) ? this.structureAddress.CEP : "00000-000",
                "city": (this.structureAddress && this.structureAddress.CITY) ? this.structureAddress.CITY : " ",
                "state": (this.structureAddress && this.structureAddress.PROVINCY) ? this.structureAddress.PROVINCY : "  "
            },
            "item": [
            ],
            "payment": [
                {
                    "transaction_type": paymentType,
                    "amount": this.priceFormatter.format(this.orderTotalValue),
                }
            ],
            "note_type": 2
        }
        this.orderItems.forEach((item: any) => {
            var newItem = {
                "name": item.product.name,
                "amount": 'R$ ' + item.total_price.toFixed(2),
                "quantity": String(item.amount),
                "unitary_value": 'R$ ' + item.unit_price
            }
            claimCheck.item.push(newItem)
        })
        if (this.orgConvenienceFee && this.convenienceFee) {
            var fee = this.orderTotalValue * (this.convenienceFee / 100);
            var newItem = {
                "name": "Taxa de serviço",
                "amount": 'R$ ' + fee.toFixed(2),
                "quantity": "1",
                "unitary_value": 'R$ ' + fee.toFixed(2)
            }
            claimCheck.item.push(newItem)
        }
        return JSON.stringify(claimCheck);
    }

    updatePaymentStatus(orderId: any, transactionData: any = {}) {
        var paymentMethod: any = this.paymentMethods.find((p: any) => { return p.PAYMENT_METHOD == this.paymentMethodSelected }) || {};
        const req = new Request();
        req.route = 'updateStonePaymentStatus';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'STORE_ID': this.store.id,
            'ORDER_ID': orderId,
            'CREDIT_CARD_BRAND': (transactionData.card_brand) ? transactionData.card_brand : null,
            'CARD_HOLDER_NAME': (transactionData.card_holder_name) ? transactionData.card_holder_name : null,
            'CREDIT_CARD_NUMBERS': (transactionData.card_number) ? transactionData.card_number : null,
            'USER_CPF': this.userCpf,
            'API_REQUEST': {
                "barcode": localStorage.getItem("TICKET_BARCODE") || "",
                "method_payment": String(paymentMethod.ID),
                "subtotal": this.order.total_value,
                "discount": "0",
                "total": this.order.total_value
            },
            'API_EXTERNAL_URL': localStorage.getItem('STORE_IP') || 'http://10.0.25.126:9091/odhen-eattake/backend/service/index.php'

        };
        req.showLoader = false;
        req.callbackSuccess = (response: any) => {
            clearInterval(this.getOrderDataInterval);
            this.getOrderDataInterval = undefined;
            // this.startModalSuccessfulPayment();
            this.$router.push('TicketPayment');
        };
        req.callbackError = (error: any) => {
            this.getOrderDataInterval = setTimeout(() => this.updatePaymentStatus(orderId, transactionData), 5000);
        };

        req.send();
    }

    cancelOrder(orderId: any, openModal = true) {
        const req = new Request();
        req.route = 'cancelOrder';
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'ORDER_ID': orderId
        };
        req.callbackSuccess = (response: any) => {
            this.clearModal();
            if (openModal) {
                this.showModalErro = true;
                this.errorMessage = "Operação cancelada pelo usuário."
            }
        };
        req.send();
    }

    clearModal() {
        this.showModalSucesso = false;
        this.showModalErro = false;
        this.showModalRemoverItem = false;
        this.showModal = false;
        this.showModalTelaCartoes = false;
        this.modalPasswordRequest = false;
        this.showModalConfirmPaymentMethod = false;
        this.showModalChoosePaymentMethod = false;
        this.showModalCardPayment = false;
        this.showModalPaymentCompleted = false;
        this.showModalChangePaymentPassword = false;
        this.showModalChangePaymentCard = false;
        this.showModalAddPhone = false;
        this.showModalLoadingCreateOrder = false;
        this.showModalLoadingFinishOrder = false;
        this.showModalAddCpf = false;
        this.showModalConfirmAddCpf = false;
    }

    cancelPreTransaction() {
        clearInterval(this.getOrderDataInterval);
        this.getOrderDataInterval = undefined;
        const req = new Request();
        req.callbackSuccess = (response: any) => {
            this.preTransactionActive = "";
            if (this.processedOrder.id) this.cancelOrder(this.processedOrder.id, false);
            //this.printClaimCheck();
        };
        req.callbackError = (error: any) => {

        };
        req.body = this.preTransactionActive;
        req.cancelPreTransaction();
    }
    exitKiosk() {
        if (this.exitCounter > 15) {
            KioskPlugin.exitKiosk();
            Util.syncCodePush();
        }
        else {
            this.exitCounter += 1;

        }
    }

    beforeDestroy() {
        if (this.preTransactionActive) this.cancelPreTransaction();
        clearInterval(this.getOrderDataInterval);
        Util.removeAddProdutos();
        localStorage.removeItem('FROM_PEDIDO')

    }

    closeModalCardPayment() {
        if (this.preTransactionActive) this.cancelPreTransaction();
        this.showModalCardPayment = false
    }

    closeModalLoading() {
        if (this.preTransactionActive) this.cancelPreTransaction();
        this.showModalLoadingCreateOrder = false
        this.showModalLoadingFinishOrder = false
        this.showModalCpfInvalido = false
        clearInterval(this.getOrderDataInterval);
        this.getOrderDataInterval = undefined;
    }

    totalizeUnique(value: any, index: any, self: any) {
        let first = self.findIndex((el: any) => el.product.id == self[index].product.id);
        // console.log(index)
        // console.log(self)
        // console.log(first)

        if (first === index) return true;
        else {
            self[first].amount += self[index].amount;
            self[first].total_price += self[index].total_price;
            return false;
        }
    }

    
    enableConvenienceFee() {
        this.convenienceFee = this.orgConvenienceFee;
        this.orderTotalValue += (this.orderTotalValueOrigin * (this.convenienceFee / 100));
    }

    disableConvenienceFee() {
        this.orderTotalValue -= (this.orderTotalValueOrigin * (this.convenienceFee / 100));
        this.convenienceFee = 0;
    }

    // fillOrderItems() {
    //     this.orderItems.forEach(
    //         (item: any)=>{
    //             var newItem: any = {
    //                 quant: 0,
    //                 productName: "",
    //                 itemId: 0,
    //                 note: "",
    //                 extras: [],
    //                 productId: null,
    //                 estimatedTime: 0, 
    //                 price: item.unit_price
    //             }; 

    //             let lastItemId: number = Number.parseInt(localStorage.getItem('lastItemId') || "-1");
    //             newItem.quant = item.amount;
    //             newItem.productName = item.product.name;
    //             newItem.itemId = lastItemId + 1;
    //             newItem.totalItem = item.total_price;
    //             localStorage.setItem('lastItemId', newItem.itemId);
    //             this.finalOrderItems.push(newItem);
    //         }
    //     )
    //     return this.finalOrderItems;

    // }

    fillOrderItems() { }
}