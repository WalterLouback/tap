import { Component, Prop, Vue } from 'vue-property-decorator'
import ImageHeader from '../components/ImageHeader.vue'
import Card from '../components/Card.vue'
import InputText from '../components/InputText.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import TextLink from '../components/TextLink.vue'
import Request from '../ts/Request'
import Util from '../ts/Util';

declare var M: any;
declare var $: any;
@Component({
    components: { ImageHeader, Card, InputText, DefaultButtonOrd, SelectField, OptionField, ModalCodigo, HeaderFluxo, TextLink }
})

export default class Cardapios extends Vue {
    //Declarations
    store: any = localStorage.getItem('STORE');

    //Menus
    menus: any = []

    //Define the values of the object to the request
    data() {
        return {
        }
    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getStoreData();
    }

    goBack() {
        this.$router.go(-1);
    }

    getStoreData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "STORE_ID": localStorage.getItem('STORE_ID')
            };
            req.route  = 'getStoreData';
            req.callbackSuccess = function(response: any) {
                self.store = response.store;
                self.menus = self.store.menus || [];
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                console.log(e);
            }
    }

}