import { Component, Prop, Vue } from 'vue-property-decorator'
import ImageHeader from '../components/ImageHeader.vue'
import Card from '../components/Card.vue'
import InputText from '../components/InputText.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import TextLink from '../components/TextLink.vue'
import Product from '../components/Product.vue'
import Option from '../components/Option.vue'
import ListDefault from '../components/ListDefault.vue'
import OrderItem from '../components/OrderItem.vue'
import moment from 'moment';
import Request from '@/ts/Request'
import Util from '../ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import QRCodeImg from '../components/QRCodeImg.vue';
import QRCode from 'qrcode';

declare var M: any;
declare var $: any;
@Component({
    components: { ImageHeader, Card, InputText, DefaultButtonOrd, SelectField, OptionField, ModalCodigo, HeaderFluxo, TextLink, Product, Option, ListDefault, OrderItem, HeaderCircleButton, QRCodeImg }
})

export default class ExtratoPedido extends Vue {
    orderData : any = JSON.parse(localStorage.getItem('CURRENT_ORDER') || '{}');
    cancelaOrder: boolean = false;
    resultadoOrderCancel : boolean = false;
    msgResultadoOrderCancel : any = '';
    resultado : boolean = true;
    backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack};
    qrCodeImage: string = ""; 
    qrCode: string = ""
    // otherButton: any = [{ name:'openQrCode', iconClass: 'fas', icon: 'trash', colorClass: 'default', notification: 0, callback: this.openCancelaOrder }]
    orderTotal : string = new Intl.NumberFormat('pt-BR',{style: 'currency', currency:'BRL', minimumFractionDigits: 2}).format(0)


    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getOrderData();
        this.qrCode = this.orderData.qr;
    }

    beforeDestroy() {
        Util.removeBackButtonBehavior();
    }

    closeOrderCancel(){
        if(this.resultado == true){
            this.resultadoOrderCancel = false;
            this.goBack();
        } else this.resultadoOrderCancel = false;
    }

    goBack() {
        this.$router.go(-1);
    }

    formatDate(date: string){
        let dateD = new Date(date);
        var dia  = dateD.getDate().toString(),
            diaF = (dia.length == 1) ? '0'+dia : dia,
            mes  = (dateD.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0'+mes : mes,
            anoF = dateD.getFullYear();
        return diaF+"/"+mesF+"/"+anoF;
    }

    formatDateTime(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('dddd, DD/MMM [às] HH:mm');
    }

    getOrderData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "USER_ID": localStorage.getItem("USER_ID"),
                "TOKEN": localStorage.getItem("TOKEN"),
                "ORDER_ID": localStorage.getItem("ORDER_ID")
            };
            req.route  = 'getOrderData';
            req.callbackSuccess = function(response: any) {
                self.orderData = self.updateOrderItemsTotalPrice(response.order);
                self.orderTotal = new Intl.NumberFormat('pt-BR',{style: 'currency', currency:'BRL', minimumFractionDigits: 2}).format(self.orderData.total);
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    updateOrderItemsTotalPrice(order: any):any {
        for (let i in order.items) {
            let itemTotal = order.items[i].unitaryPrice;
            for (let j in order.items[i].extras) {
                itemTotal += order.items[i].extras[j].selectedOptions.reduce(this.sumOptions, 0);
            }
            order.items[i].total = itemTotal * order.items[i].quantity;
        }
        return order;
    }

    sumOptions(a:any, b:any) {
        if (!$.isNumeric(a) && a.price != undefined) a = a.price;
        if (!$.isNumeric(b) && b.price != undefined) b = b.price;
        return a + b;
    }

    cancelOrder(orderId:number) {
        const self = this
        const req = new Request();
        req.body = {
            'ORDER_ID': orderId,
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
        };
        req.route = 'cancelOrder';
        req.callbackSuccess = function (response: any) {
            self.cancelaOrder = false;
            self.msgResultadoOrderCancel = 'Seu pedido número '+orderId+' foi cancelado!';
            self.resultadoOrderCancel = true;
            self.resultado = true;
            self.getOrdersFromUserInOrganization();
        };
        req.callbackError = function(error: any) {
            self.cancelaOrder = false;
            self.msgResultadoOrderCancel = 'Ocorreu um erro para cancelar seu pedido de número '+orderId+', tente novamente!';
            self.resultadoOrderCancel = true;
            self.resultado = false;
        };
        req.showLoader = true;
        req.send();
      }

    getOrdersFromUserInOrganization() {
        var self = this;

        const req = new Request();

        req.body = {
            "NRORG": localStorage.getItem('NRORG'),
            "USER_ID": localStorage.getItem("USER_ID"),
            "TOKEN": localStorage.getItem("TOKEN")
        };
        req.route = 'getOrdersFromUser';
        req.callbackSuccess = function (response: any) {
            const currentOrders = response.orders.filter((order: any) => order.isFinished == false);
            localStorage.setItem('CURRENT_ORDERS', JSON.stringify(currentOrders));
        };
        req.callbackError = function (error: any) {
            
            console.log(error);
        };
        req.showLoader = true;

        req.send();
    }
    
    openCancelaOrder(){
        this.cancelaOrder = true;
    }

    closeModalCancel(){
        this.cancelaOrder = false;
    }

    openQrCodeModal(){
        const im: any = QRCodeImg;
        im.openModal('orderQrCode');
    }
    getQrImage(){
        if(this.orderData.qr){
            QRCode.toDataURL(this.orderData.qr,{ errorCorrectionLevel: 'H', scale:3 }, (err, string) => {
                    if (err) this.qrCodeImage = '';
                    this.qrCodeImage = string;})
            return this.qrCodeImage;
        }
        else return "";
    }

}