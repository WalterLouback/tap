import { Component, Prop, Vue } from "vue-property-decorator";
import DefaultButton from "../components/DefaultButton.vue";
import HeaderFluxo from "../components/HeaderFluxo.vue";
import ListDefault from "../components/ListDefault.vue";
import OptionCollapse from "../components/OptionCollapse.vue";
import SectionArea from "../components/SectionArea.vue";
import Util from "../ts/Util";
import ButtonCard from "../components/ButtonCard.vue";
import HeaderFilter from "../components/HeaderFilter.vue";
import Request from "@/ts/Request";
import { VMoney } from "v-money";
import HeaderRelease from "../components/HeaderRelease.vue";
import LoadingPhases from "../components/LoadingPhases.vue";
import TextLink from "../components/TextLink.vue";
import HeaderCircleButton from "@/modules/shared-components/HeaderCircleButton.vue";

@Component({
  props: { dependentsData2: String },
  components: {
    DefaultButton,
    HeaderFilter,
    ButtonCard,
    HeaderFluxo,
    ListDefault,
    OptionCollapse,
    SectionArea,
    VMoney,
    HeaderRelease,
    LoadingPhases,
    HeaderCircleButton,
    TextLink
  }
})
export default class DeliverLocation extends Vue {
  hasUser: Boolean = false;
  backButton: any = {iconClass: 'fas', icon:'chevron-left', callback:this.goBack};
  checkDeliversToTable      : string = (localStorage.getItem('deliversToTable') && localStorage.getItem('deliversToTable') == 'true')? 'true':'false'
    checkDeliversToTableQR    : string = (localStorage.getItem('deliversToTable') && localStorage.getItem('deliversToTable') == 'true')? 'true':'false'
    checkDeliversToBalcony    : string = (localStorage.getItem('deliversToBalcony')=='true' || localStorage.getItem('deliversToBalcony') == 'undefined')? 'true':'false'
    deliversToTable      : string = JSON.parse(this.checkDeliversToTable);
    deliversToTableQR    : string = JSON.parse(this.checkDeliversToTable);
    deliversToBalcony    : string = JSON.parse(this.checkDeliversToBalcony);

  mounted() {
    Util.setBackButtonBehavior(this.goBack);
  }

  beforeDestroy() {
    localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
    Util.removeBackButtonBehavior();
  }

  setDeliverAtTable(){
      localStorage.setItem('DELIVER_AT', 'T');
      localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
      this.$router.push('LerQrMesa');
  }

  setDeliverAtCounter(){
      localStorage.setItem('DELIVER_AT', 'B');
      localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
      this.$router.push('Pedido');
  }

  goBack() {
    if(localStorage.getItem('ENTERED_DELIVER_LOCATION_FROM') && localStorage.getItem('ENTERED_DELIVER_LOCATION_FROM') == 'Produto'){
      localStorage.setItem('ENTERED_DELIVER_LOCATION_FROM', '');
      this.$router.go(-1);  
    }
    else this.$router.go(-1);
  }
}