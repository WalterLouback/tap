import { Component, Prop, Vue } from 'vue-property-decorator';

@Component({
    data: function() { 
        return {
            visible: false,
            colorError: false,
            colorSuccess: false,
            colorLoading: false,
            animationOn: false,
            notLoading: false
        } 
    }
})

export default class LoadingPhases extends Vue {
    private msg: string = '';
    private icon: string = '';
    private static instance: LoadingPhases;
    private static SUCCESS = 0;
    private static LOADING = 1;
    private static ERROR   = 2;

    mounted() { 
        LoadingPhases.instance = this;
    }

    public static start(changeTo: number, message: string) {
        LoadingPhases.changeContent(changeTo, message);
        LoadingPhases.instance.$data.visible = true;
    }

     public static stop() {
        LoadingPhases.instance.$data.visible = false;
    }

    public static changeContent(changeTo: number, message: string) {
        let status: number = changeTo;
        LoadingPhases.instance.msg = message;
        
        //Success:
        if (status == LoadingPhases.SUCCESS) {
            LoadingPhases.instance.icon = 'check_circle';
            LoadingPhases.instance.$data.animationOn = false;
            LoadingPhases.instance.$data.colorSuccess = true;
            LoadingPhases.instance.$data.colorLoading = false;
            LoadingPhases.instance.$data.colorError = false;
            LoadingPhases.instance.$data.notLoading = true;
        //Loading
        } else if (status == LoadingPhases.LOADING) {
            LoadingPhases.instance.icon = 'loop';
            LoadingPhases.instance.$data.animationOn = true;
            LoadingPhases.instance.$data.colorSuccess = false;
            LoadingPhases.instance.$data.colorLoading = true;
            LoadingPhases.instance.$data.colorError = false;
            LoadingPhases.instance.$data.notLoading = false;
            //Error
        } else if (status == LoadingPhases.ERROR) {
            LoadingPhases.instance.icon = 'error';
            LoadingPhases.instance.$data.animationOn = false;
            LoadingPhases.instance.$data.colorSuccess = false;
            LoadingPhases.instance.$data.colorLoading = false;
            LoadingPhases.instance.$data.colorError = true;
            LoadingPhases.instance.$data.notLoading = true;
        }
    }
}
