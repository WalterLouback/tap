import { Component, Prop, Vue } from 'vue-property-decorator';
import DefaultButton from '../components/DefaultButtonOrd.vue';

declare var window: any;
declare var touchID: any;

@Component({
    components: { DefaultButton }
})

export default class BiometryCheck extends Vue {
  
    biometryExists: string = localStorage.getItem('BIOMETRY_REGISTERED') || 'FALSE';

    bioKey : any = localStorage.getItem('USER_ID') || '';
    bioPass : string = localStorage.getItem('BIO_PASS') || 'FAlSE';
    callback?: Function;
    onCancel?: Function;

    useTouchID(callback: Function, bioKey?: string, onCancel?: Function) {
        var self: any = this;
        if (bioKey) this.bioKey = bioKey;
        if (onCancel) this.onCancel = onCancel;
        this.callback = callback;

        if (this.bioPass != 'FALSE' || this.bioPass == null) {
            self.bioPass = localStorage.getItem('BIO_KEY') || 'FALSE';
            console.log('bioPass got from lcoalstorage: ');
            console.log(self.bioPass);
            this.callBiometry();
        } else {
            let r : string = Math.random().toString(36).substring(7);
            self.bioPass = r;
            localStorage.setItem('BIO_KEY', self.bioPass);
            console.log('bioPass generated: ');
            console.log(self.bioPass);
            this.callBiometry();
        }
    }

    callBiometry() {
        console.log('Cheking if touch is avaliable...');
        var self: any = this;
        if (window && window.plugins) {
            window.plugins.touchid.isAvailable(function(biometryType: any) {
                var serviceName = (biometryType === "face") ? "Face ID" : "Touch ID";
                window.plugins.touchid.has(self.bioKey, function() {
                    console.log(serviceName + " avaialble and Password key available");
                    localStorage.setItem('BIOMETRY_REGISTERED', 'TRUE');
                    self.verifyBiometry(self.callback);
                },function() {
                    console.log(serviceName + " available but no Password Key available");
                    console.log('Trying to register password.');
                    self.saveBiometry();
                });
            }, function(msg:any) {
                    console.log("no Touch ID available!");
                    localStorage.setItem('BIOMETRY_REGISTERED', 'FALSE');
            });
        }
    }

    isAvailableOnCel(callback: Function) {
        if (window && window.plugins) {
            window.plugins.touchid.isAvailable(function(biometryType: any) {
                callback(true);
            }, function(msg:any) {
                callback(false);
            });
        }
    }

    saveBiometry(){
        const self = this;
        if (window && window.plugins) {
            window.plugins.touchid.save(self.bioKey, self.bioPass, function() {
                console.log("Password saved!");
                localStorage.setItem('BIOMETRY_REGISTERED', 'TRUE');
                /* Se dispositivo for iPhone, touchid não é solicitado automaticamente ao salvar */
                if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
                    self.verifyBiometry();
                else if (self.callback) self.callback();
            }, function() {
                console.log('Erro ao salvar biometria');
                if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)
                    self.verifyBiometry();
            });
        }
    }

    verifyBiometry() {
        const self = this;
        if (window && window.plugins) {
            window.plugins.touchid.verify(this.bioKey, "Verifique sua identidade.", self.callback, (errorCode: any) => {
                if (errorCode == -2 && self.onCancel) self.onCancel();
            });
        }
    }

    deleteBiometry() {
        if (window && window.plugins) {
            window.plugins.touchid.delete(this.bioKey, function() {
                console.log("Password key deleted");
            });
        }
    }
  
}