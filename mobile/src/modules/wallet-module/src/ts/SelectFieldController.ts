import Vue from 'vue';
import axios from "axios";
import { Component } from 'vue-property-decorator';

declare var M: any;
declare var $: any;

@Component
export default class SelectFieldController extends Vue {

    mounted() {
        SelectFieldController.refresh();
    }

    static refresh():void {
        setTimeout(function() {
            $('select').formSelect();
            var optionSpans = document.querySelectorAll(".dropdown-content>li span");
            for (var span in optionSpans) {
                if (!$.isNumeric(optionSpans[span])) optionSpans[span].className += ' primary-color';
            }
        }, 500);
    }

}