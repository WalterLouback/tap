import { Component, Vue } from 'vue-property-decorator';
import Request from './Request';
import moment from 'moment';
import router from '@/router';
@Component
export default class Util extends Vue {
    private static listener: EventListener;

    static setBackButtonBehavior(listener: EventListener) {
        if (!!Util.listener) document.removeEventListener("backbutton", Util.listener);
        Util.listener = listener;
        document.addEventListener("backbutton", Util.listener, false);
    }

    static getBackButtonBehavior() {
        return Util.listener;
    }

    static removeBackButtonBehavior(){
        document.removeEventListener("backbutton", Util.listener);
    }

    static addNpfMask(npf: string) {
        npf = Util.removeMasks(npf);
        const len: number = npf.length;
        if (len > 3) {
            npf = npf[0] + ' ' + npf.substring(1, len - 1) + '-' + npf[len - 1];
        }
        return npf;
    }

    static removeMasks(npf: string) {
        return npf.replace(/-/g, '').replace(/\./g, '').replace(/ /g, '');
    }

    static addCpfMask(cpf: string) {
        cpf = Util.removeMasks(cpf);
        const len: number = cpf.length;
        if (len == 11) {
            cpf = cpf.substring(0, 3) + '.' + cpf.substring(3, 6) + '.' + cpf.substring(6, 9) + '-' + cpf.substring(9, 11);
        }
        return cpf;
    }

    static onNewFCMToken(fcmToken: string) {
        const req: Request = new Request();
        req.route = 'updateFCMToken';
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'FCM_TOKEN': fcmToken
        };
        req.callbackSuccess = function (response: any) { };
        req.callbackError = function (error: any) { };
        req.showLoader = false;
        req.send();
    }

    static getUserData(callback?: Function) {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'USER_TYPE_ID': 1,
            'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
        };
        req.route = 'getUserData';
        req.callbackSuccess = function (response: any) {
            const now: any = moment();
            localStorage.setItem('USER_DATA', JSON.stringify(response.user));
            localStorage.setItem('USER_NAME', response.user.FIRST_NAME);
            localStorage.setItem('LAST_GET_USER_DATA', now);
            if (callback) callback(response);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.send();
    }

    static getUserReports(callback: any) {
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'USER_ID': localStorage.getItem('USER_ID'),
            "TOKEN": localStorage.getItem("TOKEN")
        };
        req.route = 'getReportsByUser';
        req.callbackSuccess = function (response: any) {
            if (callback) callback(response.reports);
            const now: any = moment();
            localStorage.setItem('LAST_GET_REPORTS', now);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.showLoader = true;
        req.send();
    }

    static getUserNotifications(callback: any) {

        const req = new Request();
        req.body = {
            "NRORG": localStorage.getItem('NRORG'),
            "USER_ID": localStorage.getItem("USER_ID"),
            "TOKEN": localStorage.getItem("TOKEN")
        };
        req.route = 'getNotificationsByUser';
        req.callbackSuccess = function (response: any) {
            if (callback) callback(response.notifications);
            const now: any = moment();
            localStorage.setItem('LAST_GET_NOTIFICATIONS', now);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.showLoader = true;
        req.send();
    }

    static getImgUrl(imageName: string, extension: string = ".svg") {
        var images = require.context('../assets/', false)
        return images('./' + imageName + extension)
    }

    static capitalize(text: string) {
        return text ? text
            .split(" ")
            .map(x => x[0].toUpperCase() + x.substring(1).toLowerCase())
            .join(" ") : ''
    }

    static closeKeyboard() {
        $('input').blur();
    }

    static checkUpdateReadyToInstall() {
        const now: any = moment();
        const lastResume: any = localStorage.getItem('LAST_RESUME');
        const duration: Number = Number(moment.duration(moment().diff(lastResume, 'hours')));
        const actualRoute: string = String(router.currentRoute.name);
        const restartEnabledRoutes = ['HomeApp', 'EventsMenu', 'MenuPedidos', 'MenuServices'];

        if (duration >= 6 && localStorage.getItem('UPDATE_INSTALLED') === 'true' && restartEnabledRoutes.includes(actualRoute)) {
            const _window: any = window;
            localStorage.removeItem('UPDATE_INSTALLED')
            localStorage.setItem('LAST_RESUME', now.toISOString());
            _window.codePush.restartApplication();
        }
        localStorage.setItem('LAST_RESUME', now.toISOString());
    }

    static logOff() {

        try {
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                "TOKEN_EXT": localStorage.getItem('TOKEN_EXT') || ''
            };
            req.route = 'logout';
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = function (response: any) {
                $('.sidenav').sidenav('close');
                router.push('/LogIn');
            };
            req.callbackError = function (error: any) {
                console.log('ws error:');
                console.log(error);
            };
            req.loadingMessage = 'Saindo da sua conta...';
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e);
        }

        try {
            localStorage.setItem('CAME_FROM_LOGOUT', 'true');
            localStorage.setItem('CAME_FROM_FORGOT_PASSWORD', 'true');
            localStorage.setItem('USER_ID', '');
            localStorage.setItem('USER_DATA', '');
            localStorage.setItem('TOKEN', '');
            localStorage.setItem('REFRESH_TOKEN', '');
            localStorage.setItem('TOKEN_EXT', '');
            localStorage.setItem('ID_EXT', '');
            localStorage.setItem('KEY_EXT', '');
            localStorage.setItem('TOTP', '');
            localStorage.setItem('GET_MODALITY_FROM_LS', '');
            localStorage.setItem('GET_CLASS_FROM_LS', '');
            $('.sidenav').sidenav('close');
        } catch (e) {
            console.log(e);
        }
    }

      static cancelLogoff() {
          this.setBackButtonBehavior(function () {
            $('.sidenav').sidenav('close');
          })
      }

}