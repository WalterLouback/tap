import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '../components/SectionArea.vue';
import TextLink from '../components/TextLink.vue';
import InputText from '../components/InputText.vue';
import DefaultButton from '../components/DefaultButton.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import ListDefault from '../components/ListDefault.vue';
import OptionCollapse from '../components/OptionCollapse.vue';
import OptionCollapseParent from '../components/OptionCollapseParent.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import Request from '../ts/Request';
import Util from '../ts/Util';
import ButtonCard from '../components/ButtonCard.vue'
import HeaderFilter from '../components/HeaderFilter.vue';
import HeaderRelease from "../components/HeaderRelease.vue";

import LoadingPhases from "@/modules/shared-components/LoadingPhases.vue"
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';


@Component({
    components: { SectionArea, HeaderFilter, ButtonCard, TextLink, InputText, DefaultButton, ModalCodigo, HeaderFluxo, ListDefault, OptionCollapse, OptionCollapseParent, HeaderRelease, LoadingPhases, LoginModal, HeaderCircleButton}
})

export default class RegisteredCards extends Vue {
cards: any = JSON.parse(localStorage.getItem('CREDIT_CARDS') || '[]');
cardsParent: any = localStorage.getItem('CARDS_PARENT') ? JSON.parse(localStorage.getItem('CARDS_PARENT') || '{}') : null;
CREDIT_CARD_ID: number = -1;
LAST_NUMBERS: number = 0;
FLAG: string = '';
showModal: boolean = false;
paymentMethodSelected : any = localStorage.getItem('METHOD_SELECTED') || '';
fullMethodSelected : any = JSON.parse(localStorage.getItem('FULL_METHOD_SELECTED') || '{}');
allPaymentMethods : Array<object> = JSON.parse(localStorage.getItem('ALL_PAYMENT_METHODS') || '[]');

backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}
showModalLogin: boolean = false;
lp: any = LoadingPhases;

mounted() {
    Util.setBackButtonBehavior(this.goBack);
    if(!localStorage.getItem('USER_ID')) this.openModalLogin();
    else this.getCreditCards();
}

beforeDestroy() {
    Util.removeBackButtonBehavior();
}

getCreditCards() {
    try {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'NRORG': localStorage.getItem('NRORG')
        };
        req.route = 'getValidCreditCard';
        req.callbackSuccess = (response: any) => {
            self.cards = response.creditcards;
            self.cardsParent = response.creditcardFromParent;
            //this.lp.stop();
            if(self.cardsParent) {
                localStorage.setItem('CARDS_PARENT', JSON.stringify(self.cardsParent));
            }
            else {
                localStorage.setItem('CARDS_PARENT', '');
            }
            localStorage.setItem('CREDIT_CARDS', JSON.stringify(self.cards || []));
        };
        req.callbackError = (error: any) => {
            //this.lp.start(this.lp.ERROR, "Não foi possível completar a operação.")
            // console.log(error);
        };
        req.callbackTimeout = (error: any) => {
            //this.lp.start(this.lp.ERROR, "Não foi possível completar a operação.")
            // console.log(error);
        };
        req.blockTouchEvents = false;
        req.send();
        } catch (e) {
            
            console.log(e);
        }
}

deleteCC() {
    try {
        const self = this
        const req = new Request();
        req.body = {
            'CREDITCARD_ID': this.CREDIT_CARD_ID,
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'TOKEN': localStorage.getItem('TOKEN') || ''
        };
        req.route = 'deleteCC';
        req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
        req.callbackSuccess = function(response: any) {
            self.fechaModal();
            self.getCreditCards();
            req.showSuccessMessage('Cartão removido com sucesso!');
            localStorage.setItem('METHOD_SELECTED', '');
            localStorage.setItem('FULL_METHOD_SELECTED', '{}');
        };
        req.callbackError = function(error: any) {
            req.showErrorMessage('Ocorreu um erro desconhecido. Por favor, tente mais tarde');
            console.log(error);
        };
        req.loadingMessage = 'Removendo o cartão...'
        req.blockTouchEvents = false;
        req.send();
        } catch (e) {
            
            console.log(e);
        } 
}

lastNumberCard( lastNumbers:any ){
    let lastNumber = '0000'.substring(0, 4 - String(lastNumbers).length) + lastNumbers
    return lastNumber;
}

mostraModal(id: number, cardNumber: number, flag: string) {
    this.CREDIT_CARD_ID = id;
    this.LAST_NUMBERS = cardNumber;
    this.FLAG = flag;
    this.showModal = true;
}

fechaModal() {
    this.showModal = false;
}

deletarCard(cardId: number){
    let card = this.cards.find((c: any) => c.ID == cardId);
    this.CREDIT_CARD_ID = cardId;
    this.FLAG = card.FLAG;
    this.LAST_NUMBERS = card.LAST_NUMBERS;
    this.showModal = true;
}

goBack() {
    this.$router.go(-1);
}

addCard() {
    if(localStorage.getItem('USER_ID'))this.$router.push({ name: 'AddCard' });
    else this.openModalLogin();
}

goToExtrato(){
    this.$router.push({ name: 'Extrato' });
}

changeMainCard(id: number) {
    const elem: any = $('#check'+id)[0];
    if (elem.checked == true) {
    try {
        const self = this
        const req = new Request();
        req.body = {
            'CREDITCARD_ID' : id || '',
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'NRORG': localStorage.getItem('NRORG')
        };
        req.route = 'setMainCreditCard';
        req.callbackSuccess = function(response: any) {
            self.getCreditCards();
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.blockTouchEvents = false;
        req.send();
        } catch (e) {
            
            console.log(e);
        } 
   } else {
       elem.checked = true;
   }
}

//TESTE

openModalLogin(){
    this.showModalLogin = true;
}

closeModalLogin() {
    this.showModalLogin = false;
}

handleClickError() {
    this.lp.stop();
}

}