    import { Component, Prop, Vue } from 'vue-property-decorator';
    import TextLink from '../components/TextLink.vue';
    import DefaultButton from '../components/DefaultButton.vue';
    import HeaderFluxo from '../components/HeaderFluxo.vue';
    import DatePicker from '../components/DatePicker.vue';
    import SectionArea from '../components/SectionArea.vue';
    import SelectField from '../components/SelectField.vue';
    import OptionField from '../components/OptionField.vue';    
    import SelectFieldController from '../ts/SelectFieldController';
    import ModalCodigo from '../components/ModalCodigo.vue';
    import Request from '../ts/Request';
    import Util from '../ts/Util';
    import moment from 'moment';
    import HeaderRelease from "../components/HeaderRelease.vue";
    import HeaderModality from "../components/HeaderModality.vue";
    import LoadingPhases from '@/modules/shared-components/LoadingPhases.vue';
    import LoginModal from '@/modules/shared-components/LoginModal.vue';
    import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
    // import '../thirdparty/card/card.js';
    // import '../thirdparty/card/card.css';
    // import '../thirdparty/card/card.js'

    declare var Card: any;
    @Component({
        components: { TextLink, DefaultButton, HeaderFluxo, SelectField, OptionField, DatePicker, ModalCodigo, SectionArea, HeaderRelease, HeaderModality, LoadingPhases, LoginModal, HeaderCircleButton }
    })
    export default class AddCard extends Vue {
        numberCard: any     = '';
        cardFullName: any   = '';
        flagCard: any       = '';
        cardFlag: any       = '';
        expirationDate: any = '';
        cvvCard: any        = '';
        reqType: string     = 'login';
        showModal: boolean  = false;
        showModalSuccess: boolean = false;
        errorMessage: string = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde.';
        allFlags: any = [
            { id : '0', name : 'Visa' , originalName : 'Visa'},
            { id : '1', name : 'MasterCard' , originalName : 'Master Card'},
            { id : '2', name : 'Amex' , originalName : 'Amex'},
            { id : '3', name : 'Diners' , originalName : 'Dinners'},
            { id : '4', name : 'Elo', originalName : 'Elo' }
        ];
        showModalLogin: boolean = false;
        backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}
        lp: any = LoadingPhases;

        mounted() {
            Util.setBackButtonBehavior(this.goBack);
            this.buildCardView();
            SelectFieldController.refresh();
            if(!localStorage.getItem('USER_ID')) this.openModalLogin();
            if($('#EXPIRATION_DATE').prop('placeholder') != "") $('#EXPIRATION_DATE_LABEL').addClass('active');
            $('#NUMBERS')[0].onkeyup = this.verifyCardFlag;
        }

        addCreditCard() {
            if (!(<HTMLFormElement>$('form')[0]).checkValidity()) {
                this.mostraModal('Por favor, preencha todos os campos obrigatórios.');
                return ;
            }
            if (this.flagCard == ''){
                this.mostraModal('Infelizmente não aceitamos esse cartão. Verifique se o cartão é de crédito e se a bandeira está listada acima.');
                return ;
            }
            // else this.lp.start(this.lp.LOADING, "Adicionando cartão...");
            try {
            const self = this;
            const req = new Request();
            req.body = {
                'GEN_USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'NUMBERS': String(this.numberCard.split(' ')).replace(/,/g, ''),
                'CARD_FULL_NAME': this.cardFullName.toUpperCase(),
                'FLAG': this.flagCard,
                'CVV': this.cvvCard,
                'EXPIRATION_DATE': String(this.expirationDate),
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route  = 'addCreditCard';
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                this.lp.stop();
                this.getUserData();
                localStorage.setItem('NEW_CC', JSON.stringify(response.creditcard));
                this.showModalSuccess = true;
            };
            req.callbackError = (error: any) => {
                console.log(error);
                $('#NUMBERS').removeClass('valid');
                $('#CARD_FULL_NAME').removeClass('valid');
                $('#EXPIRATION_DATE').removeClass('valid');
                $('#CVV').removeClass('valid');
                let mensagem: string;
                if (error.response.data.errorCode == 6) mensagem = 'Os dados informados são inválidos.';
                else if (error.response.data.errorCode == 13) mensagem = 'O cartão informado já está em uso.';
                else mensagem = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde.';
                req.showErrorMessage(mensagem);
                this.lp.start(this.lp.ERROR, mensagem)
            }
            req.callbackTimeout = (error: any) => {
                this.lp.start(this.lp.ERROR, "Não foi possível completar a operação.")
            }
            req.loadingMessage = 'Adicionando cartão...'
            req.blockTouchEvents = true;
            req.send();
            } catch (e) {
                console.log(e);
                let mensagem: string = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';
                this.mostraModal(mensagem);
            }
        }
        
        getUserData() {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'USER_TYPE_ID': 1,
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || "")
            };
            req.route = 'getUserData';
            req.callbackSuccess = function (response: any) {
                const now: any = moment();
                localStorage.setItem('USER_DATA', JSON.stringify(response.user));
                localStorage.setItem('LAST_GET_USER_DATA', now);
            };
            req.callbackError = (error: any) => {
                this.lp.start(this.lp.ERROR, "Não foi possível atualizar seus dados.")
                console.log(error);
            };
            req.send();
        }

        mostraModal(message: string) {
            this.errorMessage = message;
            this.showModal = true;
        }

        fechaModal() {
            this.showModal = false;
        }

        fechaModalSuccess() {
            this.showModal = false;
            this.$router.go(-2);
        }

        validateField(fieldId: string) {
            const fieldValue = String($('#' + fieldId).val());
            if (fieldValue.includes('_') || fieldValue.length == 0) {
                $('#' + fieldId).removeClass('valid');
                $('#' + fieldId).addClass('invalid');
            } else {
                $('#' + fieldId).removeClass('invalid');
                $('#' + fieldId).addClass('valid');
            }
        }

        validateCardNumber( cc:any ){
            var CC = String(cc.split(' ')).replace(/,/g, '');
            const cartoes = {
                regexVisa : /^4[0-9]{1}(?:[0-9]{3})?/,
                regexMaster: /^5[1-5][0-9]{14}/,
                regexAmex: /^3[47][0-9]{13}/,
                regexDiners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
                regexElo: /^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))/
            };
            
            function validateCC( nr:any, cartoes:any ) {
                for (var cartao in cartoes){
                    if (nr.match(cartoes[cartao])) {
                        $('#NUMBERS').removeClass('invalid');
                        $('#NUMBERS').addClass('valid');
                        break;
                    }
                    else {
                        $('#NUMBERS').removeClass('valid');
                        $('#NUMBERS').addClass('invalid');
                    }
                }
            }
            
            validateCC(CC, cartoes)
        }

        verifyExpiryDate( field:string ){
            const fieldValue = String($('#' + field).val());
            let expiryDate = fieldValue.split(' ')[0];
            const monthExpiry = moment(expiryDate).month()
            
            if (!monthExpiry) {
                $('#' + field).removeClass('valid');
                $('#' + field).addClass('invalid');
            } else {
                $('#' + field).removeClass('invalid');
                $('#' + field).addClass('valid');
            }
        }

        verifyCardFlag(){
            const cardNumber = this.numberCard.replace(/ /g, '').replace(/_/g, '');
            const regexVisa = /^4[0-9]{1}(?:[0-9]{3})?/;
            const regexMaster = /^5[1-5]{1}/;
            const regexAmex = /^3[47][0-9]{1}/;
            const regexDiners = /^3(?:0[0-5]|[68][0-9])[0-9]{1}/;
            const regexElo = /^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))/;

            if(regexVisa.test(cardNumber)) {
                this.flagCard = 'Visa';
                this.cardFlag = 'Visa';
            }
            else if(regexMaster.test(cardNumber)) {
                this.flagCard = 'Master Card';
                this.cardFlag = 'MasterCard'
            }
            else if(regexAmex.test(cardNumber)) {
                this.flagCard = 'Amex';
                this.cardFlag = 'Amex';
            }
            else if(regexDiners.test(cardNumber)) {
                this.flagCard = 'Diners';
                this.cardFlag = 'Diners';
            }
            else if(regexElo.test(cardNumber)) {
                this.flagCard = 'Elo';
                this.cardFlag = 'Elo';
            }
            else this.cardFlag = '';

            if (this.flagCard) {
                $('#FLAG').val(this.flagCard);
                SelectFieldController.refresh();
            }
        }

        numberCardOut() {
            this.validateCardNumber(this.numberCard);
        }

        cardFullNameOut() {
            this.validateField('CARD_FULL_NAME');
        }

        expirationDateOut() {
            this.verifyExpiryDate('EXPIRATION_DATE');
        }

        cvvCardOut() {
            this.validateField('CVV');
        }

        flagChange() {
            $('#FLAG').addClass('valid');
        }

        data () {
            return {
                structures: [
                    { id: '1', name: 'Master Card'},
                    { id: '2', name: 'Visa'},
                    { id: '3', name: 'Amex'},
                    { id: '4', name: 'Hipercard'},
                    { id: '5', name: 'Diners'},
                    { id: '6', name: 'Aura'},
                    { id: '7', name: 'Elo'},
                    { id: '8', name: 'Alelo'},
                    { id: '9', name: 'Sodexo'}
                ]
            };
        }

        goBack() {
            this.$router.go(-1);
        }

        goTo(){
            this.$router.push('NewCardSuccess');
        }

        buildCardView(){
            // var card = new Card({
            //     // a selector or DOM element for the form where users will
            //     // be entering their information
            //     form: 'form', // *required*
            //     // a selector or DOM element for the container
            //     // where you want the card to appear
            //     container: '.card-wrapper', // *required*
            
            //     formSelectors: {
            //         numberInput: 'input#NUMBERS', // optional — default input[name="number"]
            //         expiryInput: 'input#EXPIRATION_DATE', // optional — default input[name="expiry"]
            //         cvcInput: 'input#CVV', // optional — default input[name="cvc"]
            //         nameInput: 'input#CARD_FULL_NAME' // optional - defaults input[name="name"]
            //     },
            
            //     width: 300, // optional — default 350px
            //     formatting: true, // optional - default true
            
            //     // Strings for translation - optional
            //     messages: {
            //         validDate: 'valid\ndate', // optional - default 'valid\nthru'
            //         monthYear: 'mm/yyyy', // optional - default 'month/year'
            //     },
            
            //     // Default placeholders for rendered fields - optional
            //     placeholders: {
            //         number: '•••• •••• •••• ••••',
            //         name: 'Nome Impresso no cartão',
            //         expiry: '••/••',
            //         cvc: '•••'
            //     },
            
            //     masks: {
            //         cardNumber: '•' // optional - mask card number
            //     },
            
            //     // if true, will log helpful messages for setting up Card
            //     debug: false // optional - default false
            // });
        }

        focusInput( id:any ){
            const offset = -60;
            const el: any = $("#"+id);
           
            window.addEventListener('keyup', (ev)=>{
                if(id == 'EXPIRATION_DATE' || id == 'NUMBERS') {
                    this.applyMask(id);
                }
            });
            
            setTimeout(function(){
                $('html, body').animate({
                    scrollTop: el.offset().top + offset
                }, 100);
            },150);
        }

        validate_num(c: string) {
            if (c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57) {
                return true;
            } else {
                return false;
            }
        }
    
        
    
    
        applyMask(id: string) {
            var validChars = [];
            var text = (id == 'NUMBERS') ? this.numberCard : this.expirationDate;
            var maskedText = "";
            var mask = (id == 'NUMBERS') ? "____ ____ ____ ____" : "__/__";
    
            for (var i = 0; i < text.length; i++) {
                if (this.validate_num(text.charAt(i))) {
                    validChars.push(text.charAt(i));
                }
            }
            
    
            for (var j = 0, i = 0; j < mask.length && i < validChars.length; j++) {
                if (mask.charAt(j) == "_") { 
                        maskedText = maskedText + validChars[i];
                        i++;
                } else {
                    maskedText = maskedText + mask.charAt(j);
                }
    
            }
            
            if (id == 'NUMBERS') 
                this.numberCard = maskedText;
            else 
                this.expirationDate = maskedText;
        }

        openModalLogin(){
            this.showModalLogin = true;
        }
        
        closeModalLogin() {
            this.showModalLogin = false;
        }

        handleClickError() {
            this.lp.stop();
        }
    
    }