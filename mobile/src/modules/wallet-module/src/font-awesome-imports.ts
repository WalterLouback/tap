import Vue from "vue";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faChartLine as falChartLine,
  faPlusSquare as falPlusSquare,
  faCreditCard as falCreditCard,
  faReceipt as falReceipt
} from "@fortawesome/pro-light-svg-icons";

import {
  
} from "@fortawesome/pro-regular-svg-icons";

import {
  
} from "@fortawesome/pro-solid-svg-icons";

library.add(
  falChartLine,
  falCreditCard,
  falPlusSquare,
  falReceipt
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
