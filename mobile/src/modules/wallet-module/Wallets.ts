import Router from '@/router'
import Carteira from './src/views/Carteira.vue';
import RegisteredCards from './src/views/RegisteredCards.vue';
import Extrato from './src/views/Extrato.vue';
import Balance from './src/views/Balance.vue';
// import ExtratoEvents from './src/views/ExtratoEvents.vue';
// import FiltroExtratoEvents from './src/views/FiltroExtratoEvents.vue';
import AddBalance from './src/views/AddBalance.vue';
import AddCard from './src/views/AddCard.vue';
import Util from './src/ts/Util'

import './src/font-awesome-imports';
import './src/stylesheet/app.scss';
import './src/stylesheet/components.scss';
import './src/stylesheet/views.scss';
import './src/store'

export default class Wallets {

    static callback = new Function();

    public static addRoutes() {
        try { 
            Router.addRoutes([
                {
                    path: '/Carteira',
                    component: Carteira,
                    name: 'Carteira'
                },
                {
                    path: '/RegisteredCards',
                    component: RegisteredCards,
                    name: 'RegisteredCards'
                },
                {
                    path: '/Extrato',
                    component: Extrato,
                    name: 'Extrato'
                },
                {
                    path: '/Balance',
                    component: Balance,
                    name: 'Balance'
                },
                // {
                //     path: '/ExtratoEvents',
                //     component: ExtratoEvents,
                //     name: 'ExtratoEvents'
                // },
                // {
                //     path: '/FiltroExtratoEvents',
                //     component: FiltroExtratoEvents,
                //     name: 'FiltroExtratoEvents'
                // },
                {
                    path: '/AddBalance',
                    component: AddBalance,
                    name: 'AddBalance'
                },
                {
                    path: '/AddCard',
                    component: AddCard,
                    name: 'AddCard'
                }
            ]);
        } catch (e) { }
    }

    public static goToWallet(config: WalletConfig) {
        // this.addRoutes();
        this.callback = ()=>{
            Util.removeBackButtonBehavior();
            config.callback(); 
        }
        localStorage.setItem('ENTERED_WALLET_FROM', (Router as any).history.current.path);
        Router.push('Carteira');
    }

}

export class WalletConfig {

    callback: Function;

    constructor(callback: Function) {
            this.callback = callback; 
    }

}


