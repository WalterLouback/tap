import { Component, Prop, Vue } from 'vue-property-decorator';
import HeaderRelease from '@/components/HeaderRelease.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import CleanListTwoParams from '@/components/CleanListTwoParams.vue';
import Request from '@/ts/Request.ts'
import Util from '@/ts/Util.ts';

@Component({
    components: { DefaultButton, HeaderRelease, CleanListTwoParams }
})

export default class ModalityDependents extends Vue {
    userData       : any    = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    profileImage   : string = this.userData.IMAGE;
    fullName       : string = this.userData.FIRST_NAME + ' ' + this.userData.LAST_NAME;
    dependents     : any    = [];
    getNpf         : any    = localStorage.getItem('ID_EXT') || "";
    npf            : number = parseInt(this.getNpf);
    checkedUserNpf : any    =localStorage.getItem('DEPENDENT_EXT_ID');
    title          : string = '';
    modalitiesList : any = [];

    mounted() {
        this.dependents = this.userData.dependents;

        Util.setBackButtonBehavior(this.goBack);

        if (this.dependents == '') {
            localStorage.setItem('CURRENT_DEPENDENT_IMAGE', this.profileImage);        
            localStorage.setItem('DEPENDENT_EXT_ID', this.userData.ORGANIZATION_DATA.EXTERNAL_ID);
            if (localStorage.getItem('GO_TO_ENROLL') == 'true') {
                this.$router.push('ClassEnrollList');
            } else {
                this.$router.push('MenuEducation');
            }
        }

        this.title = ' as atividades'

        // if (localStorage.getItem('GO_TO_ENROLL') == 'true') {
        //     this.title = ' as matrículas';
        // } else {
        //     this.title = ' a fila de espera';
        // }
    }

    dependentClick(idExt:any, nome: string, img: string) {
        localStorage.setItem('DEPENDENT_EXT_ID', idExt);
        // if (localStorage.getItem('GO_TO_ENROLL') == 'true') { 
        //     this.$router.push('ClassEnrollList');
        // } else {
        //     this.$router.push('ModalityList');
        // }
        localStorage.setItem('CURRENT_DEPENDENT_NAME', nome);
        localStorage.setItem('CURRENT_DEPENDENT_IMAGE', img);
        this.getCourses();
        this.$router.push('MenuEducation')
    }

    getCourses() {
        try {
            const req: any = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || ""),
                'EXTERNAL_ID' : localStorage.getItem('DEPENDENT_EXT_ID'),
                'USER_ID_CONSULTA' : localStorage.getItem('USER_ID'),
            };
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.route = 'getCourses';
            req.callbackSuccess = (response: any) => {
                if (response && response.response && response.response.length > 0) {
                    localStorage.setItem('MODALITIES_LIST', JSON.stringify(response.response));
                    this.modalitiesList = response.response;
                    localStorage.setItem('GET_MODALITY_FROM_LS', 'true');
                    
                } else {
                    localStorage.setItem('GET_MODALITY_FROM_LS', 'true');
                    localStorage.setItem('MODALITIES_LIST', JSON.stringify([]));
                    this.modalitiesList = [];
                }
            };
            req.callbackError = (error: any) => {
                console.log(error);
            };
            req.loadingMessage = 'Buscando suas atividades...';
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    goBack() {
        this.$router.go(-1);
    }

}