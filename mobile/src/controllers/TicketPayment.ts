import { Component, Prop, Vue } from 'vue-property-decorator';
import SectionArea from '../components/SectionArea.vue';
import TextBegin from '../components/TextBegin.vue';
import ModalCodigo from '../components/ModalCodigo.vue';
import DefaultButton from '../components/DefaultButton.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import DivInput from '../components/DivInput.vue';
import ImageModal from '../components/ImageModal.vue';
import SelectField from '../components/SelectField.vue';
import OptionField from '../components/OptionField.vue';
import CleanButton from '@/modules/shared-components/CleanButton.vue';
import SelectFieldController from '../ts/SelectFieldController';
import LoginFrontEnd from '@/modules/login-frontend/LoginFrontEnd';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import InputText from '@/modules/shared-components/InputText.vue'
import LoginModal from "@/modules/shared-components/LoginModal.vue";
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue';
import moment from 'moment';

declare var KioskPlugin : any;
declare var window: any;

@Component({
    components: { SectionArea, TextBegin,DefaultButtonOrd, DefaultButton, ModalCodigo, DivInput, ImageModal, SelectField, OptionField, SelectFieldController, CleanButton, HeaderCircleButton, InputText, LoginModal}
})




export default class TicketPayment extends Vue {

    /* Properties*/
    comandas: any = JSON.parse(localStorage.getItem('ORDER_TICKET_DATAS') || "{}"); 
    comandasUnidas: any = JSON.parse(localStorage.getItem("TICKETS")||'{}');

    loginProps: any = JSON.parse(localStorage.getItem('LOGIN_PROPS') || 
        JSON.stringify({
            loginMethods: ['PHONE'],
            introductionMessage:  'Bem-vindo ao BipFun! Entre no app e ',
            introductionMessageHighlight: 'compre sem enfrentar filas.',
            logo: Util.getImgUrl('bipfun-logo','.svg'),
            nrorg: process.env.VUE_APP_NRORG || '0'
        })
    );
    nrorg: string = process.env.VUE_APP_NRORG || this.loginProps.nrorg || '0';
    loginMethods: any[] = this.loginProps.loginMethods || ['PHONE'];
    showLoginModal: boolean = false;
    
    
    /* Inputs */
    emailInput: string = '';
    passInput: string = '';
    authCodeInput: string = '';
    phoneInput: string = '';
    inputCpfOrNpf: string = localStorage.getItem('NPF') || localStorage.getItem('CPF') || '';
    passwordVisibility: boolean = false;
    urlInput: string = '';
    barCodeFromExplorar:string = localStorage.getItem('BARCODE_EXPLORAR') || '';
    loadingComanda:boolean=false;
    emailFocused: boolean = false;
    passFocused: boolean = false;
    backButton: any         = {iconClass: 'fal', icon: 'chevron-left', callback:this.goToPedidoComanda}
    /* CPF or NPF */
    cpf: string = '';
    npf: string = '';

    /* Messages */
    introductionMessage: string = this.loginProps.introductionMessage || '';
    introductionMessageHighlight: string = this.loginProps.introductionMessageHighlight || '';

    /* Images */
    logo: string = '';
    backgroundImage: string = ""; 
    // backgroundImage: string = Util.getImgUrl('12210 (1)', '.png')
    

    /* Modal */
    modal: boolean = false;
    msgModal: string = '';
    modalUrl: boolean = false;
    taaConfig: any = localStorage.getItem('TAA_SELECTED') ? JSON.parse(localStorage.getItem('TAA_SELECTED') || "{}") : {};
    storeId: any = localStorage.getItem('TAA_STORE') || ''; 
    store: any = (localStorage.getItem('STORE') && localStorage.getItem('STORE_'+ this.storeId)!= 'undefined') ? JSON.parse(localStorage.getItem('STORE_'+ this.storeId) || "{}") : {};
    multComanda: boolean = localStorage.getItem('ORDER_TICKET_DATAS')? true :false  
    posSerialNumber: string = "";
    exitCounter: number = 0;

    barCodeImg: string         = Util.getImgUrl('bar-code','.png');
    barCode: string     = "";
    log: string='';
    showMLog: boolean=false;
   

    created(){
        if (this.loginProps.backgroundImage) this.backgroundImage = Util.getImgUrl(this.loginProps.backgroundImage.split('.')[0], '.'+ this.loginProps.backgroundImage.split('.')[1]);
        if (this.loginProps.logo) {
            this.logo = Util.getImgUrl(this.loginProps.logo.split('.')[0], '.'+ this.loginProps.logo.split('.')[1]);
        }
        localStorage.setItem("storeId", this.storeId);
    }

    mounted() {

        var self = this;
        /* Preparando os fields de texto*/
        Util.setBackButtonBehavior(() => { });
        if(this.barCodeFromExplorar)
        this.receiveBarcode(this.barCodeFromExplorar)
        Util.setAddProdutos( (event:any) => {
            var keyName = event.key;

            if(keyName!='Enter'){
                this.barCode+=keyName;
            }else{
                this.receiveBarcode(this.barCode);
                this.barCode='';
            }
        });
        this.getOrganizationData();
        setTimeout(() =>  this.getStoreData(), 50);
      
    }
    
    receiveBarcode(barCode:any){
        let comandaRepetida:boolean =false;
        
        
        localStorage.setItem("BARCODE_EXPLORAR", '')
        console.log(this.comandasUnidas)
        console.log(this.multComanda)

         if(this.multComanda && this.comandasUnidas)
         this.comandasUnidas.comandasU.forEach((com:any) => {
             console.log(com.products) 
             // if(com.products)
             //console.log(com.products[0].barcode)
             if(com.products)
              if(com.rf_id.includes(barCode)||com.products[0].barcode==barCode)
             comandaRepetida=true
         })
        if(!comandaRepetida)
        this.getTicketData(barCode);
        else this.showModal("Comanda Já adicionada.")
    }
    
    getTicketData(barCode: string){
        let comandaRepetida:boolean =false;
       // console.log(barCode)
        const req = new Request();
        var data =new Date()
        this.loadingComanda=true;
        req.callbackSuccess = (response: any) => {    
            if(this.multComanda && this.comandasUnidas)
            this.comandasUnidas.comandasU.forEach((com:any) => {
               // console.log(com.products) 
                // if(com.products)
                //console.log(com.products[0].barcode)
                if(com.products)
                 if(com.products[0].barcode==response.data.products[0].barcode)
                comandaRepetida=true
            })
           if(comandaRepetida){
            this.loadingComanda=false;
            this.showModal("Comanda Já adicionada.")
           }else{
           // console.log(barCode);
          // this.comandas=localStorage.getItem("ORDER_TICKET_DATA")
           this.loadingComanda=false;
           console.log(this.comandas)
           console.log(response.data)
            if(response.data.paid){
                this.showModal("O pagamento desta comanda já foi realizado.");
            }
            else if (!response.data.products.length){
                this.showModal("Sem produtos na comanda.");
            }
            else {
              var totalValue =0;
                response.data.products.forEach((product: any)=>{
                   // var priceFormated= (Math.trunc((product.total_price*100))/100);
                  totalValue= this.somaFloat(totalValue,product.total_price);
                // totalValue=    (parseInt((totalValue*100+product.total_price*100).toString()))/100
                //  totalValue= parseFloat( (parseFloat((totalValue.toString()))+parseFloat(priceFormated.toString())).toFixed(2))
                // (parseFloat(10.44)+parseFloat(6.25)).toFixed(2)
                //parseFloat(totalValue) += priceFormated
                    console.log(totalValue)
                   // totalValue= Math.floor(totalValue*100)/100
                })
                if(!totalValue) {
                    this.showModal("Sem produtos na comanda.");
                }
                else {
                    totalValue=response.data.products[0].totalcomanda
                    response.data.rf_id = barCode;
                    response.data.total_value = totalValue;
                  //  console.log(this.comandas.products)
                   // console.log(this.multComanda)
                    if (this.multComanda){
                       
                        this.comandas.products = this.comandas.products.concat(response.data.products)
                        this.comandas.total_value += response.data.total_value
                        console.log(this.comandas)
                        console.log(response.data)
                        localStorage.setItem("ORDER_TICKET_DATA", JSON.stringify(this.comandas))
                    }else{
                    console.log(response.data)
                    localStorage.removeItem("TICKETS")
                    this.comandasUnidas=''
                    localStorage.setItem("ORDER_TICKET_DATA", JSON.stringify(response.data));
                    }
                    this.crialog('add - '+data.getHours()
                    +':'+data.getMinutes()+":"+data.getSeconds()+' - '+JSON.stringify(response.data.products[0].barcode+'\n'))
                    if(this.comandasUnidas.comandasU!=undefined){
                        this.comandasUnidas.comandasU=   this.comandasUnidas.comandasU.concat(response.data)
                    localStorage.setItem("TICKETS", JSON.stringify(this.comandasUnidas) )
                    }else{ 
                     this.comandasUnidas = '{"comandasU":['+JSON.stringify(response.data) + "]}"
                     localStorage.setItem("TICKETS", this.comandasUnidas )
                    }
                  //  this.comandas=response.data
                  //  this.comandasUnidas.forEach((com:any) => {
                  //      com.products.forEach((p:any) => {
                  //      this.comandas.products  = this.comandas.products.concat(p)
                  //      });
                   // });
                   // console.log(this.comandas)
                   
                   console.log("teste1")
                   this.$router.push('PedidoComanda');
                   console.log("teste2")
                   var  barCode1:string=response.data.products[0].barcode.substring(5)
                   var barCodes:string =localStorage.getItem("TICKET_BARCODE")?JSON.parse(localStorage.getItem("TICKET_BARCODE")||''):''
                   if(!this.multComanda)
                    localStorage.setItem("TICKET_BARCODE", JSON.stringify(barCode1));
                    else
                    localStorage.setItem("TICKET_BARCODE",JSON.stringify(barCodes+','+barCode1));

                    
                }
            }                     
        }
        };
        req.callbackError = (error: any) => {
            console.log(error)
            this.showModal("Sua comanda não foi encontrada, caso tenha algum consumo dirija-se ao caixa.");
            this.loadingComanda=false;
        };
        req.body = barCode;
        if (barCode) req.getTicketData();
    }

    somaFloat(x: number, y:number){
        const soma = parseFloat((x + y).toFixed(10));
        return Math.trunc(parseFloat((soma * 100).toFixed(10)))/100;
    }
    crialog(data:string){
        var log = localStorage.getItem('LOG')||""
        log =log.concat(data);
        localStorage.setItem("LOG",log)
    }
    apagaLog(){
    localStorage.removeItem('LOG')
    }


    showModal(msg: any) {
        this.msgModal = msg;
        this.modal = true;
    }

    hideModal() {
        this.modal = false;
    }
goToPedidoComanda(){
    localStorage.setItem('FROM_PEDIDO','true')
    this.$router.push('PedidoComanda');

}
    exitKiosk(){
       
        if (this.exitCounter > 15){
            KioskPlugin.exitKiosk();
            Util.syncCodePush(); 
        }
        else {
            this.exitCounter += 1;
            
        }
    }
    showLog(){
        if (this.exitCounter > 15){
            this.log=  localStorage.getItem('LOG')||""
            this.showMLog=true;
            window.open("mailto:"+"walter.louback@teknisa.com"+"?body="+this.log);
        }
        else {
            this.exitCounter += 1;
            
        }
        


    }
    showLogTransaction(){
        if (this.exitCounter > 15){
            this.log=  sessionStorage.getItem('LOG_TRANSACTION')||""
            //this.showMLog=true;
            window.open("mailto:"+"walter.louback@teknisa.com"+"?body="+this.log);
         //   localStorage.removeItem('LOG_TRANSACTION')
        }
        else {
            this.exitCounter += 1;
            
        }
        
    }

    abreEmail(){
        if (this.exitCounter > 15){
            this.log=  localStorage.getItem('LOG')||""
           // this.showMLog=true;
            window.open("mailto:"+"walter.louback@teknisa.com"+"?body="+this.log);
        }
        else {
            this.exitCounter += 1;
            
        }
        
    }
    
    beforeDestroy(){
        Util.removeAddProdutos();
        var data = new Date();
        "STORE_ID"; localStorage.getItem('storeId'),
        "CURRENT_TIME"; {
            "horas"; data.getHours(),
            "minutos"; data.getMinutes()
        }
       // this.store = response.store;
    }

    getStoreData() {
        var data = new Date();
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "STORE_ID": localStorage.getItem('storeId'),
                      "CURRENT_TIME": {
                      "horas": data.getHours(),
                      "minutos": data.getMinutes()
              }
            };
            req.route = 'getStoreData';
            req.callbackSuccess = (response: any) => {
                this.store = response.store;
                
                localStorage.setItem('ACCEPTED_PAYMENT_METHODS', JSON.stringify(self.store.paymentMethods));
                localStorage.setItem('deliversToTable', self.store.deliversToTable);
                localStorage.setItem('deliversToBalcony', self.store.deliversToBalcony);
                localStorage.setItem('STONE_ESTABLISHMENT_ID', this.store.establishmentId);
                localStorage.setItem('storePaymentMoment', self.store.paymentMoment);
                localStorage.setItem(('STORE'), JSON.stringify(self.store));
                localStorage.setItem('STORE_IP', this.store.store_ip);
                let menus      = self.store.menus.currentWorkshift || [];
                
                let newCategories: any = [];

                menus.forEach((menu:any)=>{         
                    newCategories = newCategories.concat(menu.productGroups);
                });

                let categories = newCategories;
                categories.forEach((category: any) => {
                    category.products = category.products.filter((p: any) => p.status == 'A');
                })
                localStorage.setItem("ALL_CATEGORIES", JSON.stringify(categories));
                
                //setTimeout(self.addEventListeners, 1);
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
        
    }

    getOrganizationData() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'USER_TYPE_ID': 4,
        };
        req.route = 'getOrganizationData';
        req.callbackSuccess = (response: any) =>{
            const name: string = response.organization.NAME;
            const editableFields: any = response.organization.EDITABLE_FIELDS;
            let PRIMARY_COLOR = response.organization.CONFIGURATIONS.PRIMARY_COLOR;
            let SUPPORT_COLOR = response.organization.CONFIGURATIONS.SECONDARY_COLOR;

            localStorage.setItem('ORG_NAME', name);
            localStorage.setItem('ORG_FIELDS', JSON.stringify((editableFields)));
            localStorage.setItem('ORGANIZATION_DATA', JSON.stringify((response.organization)));
            localStorage.setItem('ORGANIZATION_LOGO', response.organization.CONFIGURATIONS.LOGO_IMAGE);
            localStorage.setItem('ORGANIZATION_LOGO_SMALL', response.organization.CONFIGURATIONS.LOGO_IMAGE_SMALL);
           // localStorage.setItem('STONE_ESTABLISHMENT_ID', response.organization.CONFIGURATIONS.STONE_ESTABLISHMENT_ID);
            if (document.documentElement) {
                document.documentElement.style.setProperty('--primary-color', PRIMARY_COLOR);
                localStorage.setItem('PRIMARY_COLOR', PRIMARY_COLOR)
            
            }
            if (document.documentElement) {
                document.documentElement.style.setProperty('--secundary-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--support-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--secundary-support-color', SUPPORT_COLOR);
                localStorage.setItem('SECONDARY_COLOR', SUPPORT_COLOR);
            }

            //LoginFrontEnd.callback();

        };
        req.callbackError = function (error: any) {
            let mensagem: string;
            if (error.response.data.errorCode == 24) {
                mensagem = 'Usuário não autorizado pelo titular!';
                self.showModal(mensagem);
            }
            if (error.response.data.errorCode == 14) {
                mensagem = 'Usuário não autorizado pela organização!';
                self.showModal(mensagem);
            }
            console.log(error);
        };
        // req.loadingMessage = 'Preparando o aplicativo...';
        req.blockTouchEvents = false;
        req.send();
    }
}
