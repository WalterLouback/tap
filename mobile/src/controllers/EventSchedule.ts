import { Component, Prop, Vue } from 'vue-property-decorator';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue';
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue';
import CircleButton from '@/modules/shared-components/CircleButton.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Banner from '@/modules/shared-components/Banner.vue';
import Util from '@/ts/Util';
import Request from '@/ts/Request';
import BannerStore from '@/modules/shared-components/BannerStore.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import ListDefault from "../components/ListDefault.vue";
import Shortcuts from "../components/Shortcuts.vue";
import TextLink from "../components/TextLink.vue";
import NewsModal from "../components/NewsModal.vue";
import moment from 'moment';
import FCM from '@/ts/FCM';
import Minicard from '@/modules/shared-components/Minicard.vue';



@Component({
    components: { BottomNavigation, HeaderBackground, CircleButton, HeaderCircleButton, Banner, BannerStore, LoginModal, ListDefault, Shortcuts, TextLink, NewsModal, Minicard}
})

export default class EventSchedule extends Vue {
    componentKey: number = 0;
    image: any = { name: 'class_aqua', ext: '.jpg' }
    backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack }
    menuOptions: boolean = true;
    storesLoaded: boolean = true;
    notificationsKey: number = 1;       
    placeholderImage: string = Util.getImgUrl('placeholder', '.png')
    showLoginModal: boolean = false;
    selectedNews: string="";
    event: any = {};
    parentEvent: any = JSON.parse(localStorage.getItem('PARENT_EVENT')||'[]')||[];
    schedule: any = JSON.parse(localStorage.getItem('EVENT_SCHEDULE')||'[]')||[];
    eventScheduleCache: any = (localStorage.getItem('EVENT_SCHEDULE_CACHE')) ? JSON.parse(localStorage.getItem('EVENT_SCHEDULE_CACHE')||'[]') : [];
    

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.event = this.parentEvent[this.parentEvent.length-1];
        if (!(Util.isEmpty(this.parentEvent)) && this.event.id == this.parentEvent.id){
            this.schedule = JSON.parse(localStorage.getItem('EVENT_SCHEDULE') || '[]');
            // this.schedule.sort(
            //     (a: any, b:any)=>{
            //         if ( a.finalDate.date && b.finalDate.date && moment(a.finalDate.date).isAfter(b.finalDate.date))
            //             return 1;
            //         else return -1;
            //     }
            // )
            this.$forceUpdate();
        }
        // this.getStoreData();
    }

    goToEvent(event: any) {
        localStorage.setItem('IS_AN_EVENT','t');
        localStorage.setItem('EVENT', JSON.stringify(event));

        Util.removeBackButtonBehavior();
        this.eventScheduleCache.push(this.schedule);
        localStorage.setItem('EVENT_SCHEDULE_CACHE', JSON.stringify(this.eventScheduleCache)); 
        this.$router.push('/EventDetails');
    }   

    goBack() {
        localStorage.setItem('EVENT', JSON.stringify(this.parentEvent.pop()));
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
        this.$router.go(-1);
    }

    goToEventStores(){
        this.$router.push('/EventStores');
    }

    goHome() {
        this.$router.push('Home');
    }

    openMenuOptions() {
        this.menuOptions = true;
        localStorage.setItem('PULSE_CLASS', 'false');
    }

    getStoreData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "STORE_ID": this.event.id
            };
            req.route = 'getStoreData';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('PARENT_EVENT', JSON.stringify(response.store))
                localStorage.setItem('EVENT_STORES', JSON.stringify(response.store.children));
                this.schedule = response.store.children;
                this.parentEvent = response.store;
                //setTimeout(self.addEventListeners, 1);
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
        
    }
    getEventData(id: any) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "EVENT_ID": id
            };
            req.route = 'getEventData';
            req.callbackSuccess = (response: any) => {
                this.goToEvent(response.event);
            };
            req.callbackError = function(error: any) {
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    
}
