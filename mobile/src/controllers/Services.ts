import { Component, Prop, Vue } from 'vue-property-decorator';
import HeaderRelease from '@/components/HeaderRelease.vue';
import HeaderFilter from '@/components/HeaderFilter.vue';
import EmptyState from '@/components/EmptyState.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderHome from '@/components/HeaderHome.vue';
import MenuOptions from '@/components/MenuOptions.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import Util from '@/ts/Util';
import Request from "@/ts/Request";
import HeaderModality from "@/components/HeaderModality.vue";
import feather from 'feather-icons';
import BottomNavigation from "@/modules/shared-components/BottomNavigation.vue";
import LoginModal from "@/modules/shared-components/LoginModal.vue";
import HeaderCircleButton from "@/modules/shared-components/HeaderCircleButton.vue";
import ImageCardButton from "@/modules/shared-components/ImageCardButton.vue";
import Minicard from "@/modules/shared-components/Minicard.vue";

declare var $: any;

@Component({
    props: { servicos: String, servicosFilhos: Boolean, servicosPais: Boolean },
    components: { DefaultButton, HeaderHome, BottomNavigation, MenuOptions,  ModalCodigo, HeaderModality, HeaderRelease, HeaderFilter, EmptyState, LoginModal, HeaderCircleButton, ImageCardButton, Minicard}
})

export default class Services extends Vue {
    serviceChildren: any = JSON.parse(localStorage.getItem('SERVICE_CHILDREN') || '[]');
    services: any = JSON.parse(localStorage.getItem('SERVICE_PARENTS') || '[]');
    services2: any = JSON.parse(localStorage.getItem('SERVICE_CHILDREN') || '[]');
    tiposFiltrados: any = JSON.parse(localStorage.getItem('TYPE_SERVICES') || '[]');
    parentsMenu: any = JSON.parse(localStorage.getItem('MENU_SERVICE_PARENTS') || '[]');
    notFound: boolean = false;
    openModal: boolean = false;
    menuOptions: boolean = false;
    componentKey: number = 0;
    notificationsKey: number = 1;
    userData: any = JSON.parse(localStorage.getItem('USER_DATA') || '[]');
    modalMsg: string = '';
    showModal: boolean = false;
    logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL')||'logo-mtc-home.svg';
    bnKey:number = 0;

    mounted() {
        feather.replace();
        $(document).ready(function () {
            $('.sidenav').sidenav();
        });
        this.getServicesByNrorg();
        localStorage.setItem("MENU_SERVICE_PARENTS", JSON.stringify(this.parentsMenu))
        localStorage.getItem('MENU_SERVICE_PARENTS')        
    }

    getServicesByNrorg(){
        try {
            const req: any = new Request();
            const self = this;
            req.body = {
                USER_ID: localStorage.getItem("USER_ID"),
                NRORG: localStorage.getItem("NRORG"),
                TOKEN: localStorage.getItem("TOKEN")
            };
            req.route = "getServicesByNrorg";
            req.callbackSuccess = (response: any) => {
                    if (response && response.services)
                        localStorage.setItem("SERVICE_PARENTS", JSON.stringify(response.services));
                    self.services = response.services;
                    const parentsMenu = self.services.filter((id: any) => {
                        return id.PARENT_ID == "" && id.TYPE != 'HTML';
                    });
                    self.parentsMenu = parentsMenu;
                    localStorage.setItem("MENU_SERVICE_PARENTS", JSON.stringify(self.parentsMenu))
                },            
            req.callbackError = (error: any) => {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }
    
    removeSpecialCharacters(s: string) {
        s = s ? s.toLowerCase() : '';
        s = s.toLowerCase();
        s = s.replace(new RegExp(/\s/g),'_');
        s = s.replace(new RegExp(/[àáâãäå]/g),'a');
        s = s.replace(new RegExp(/æ/g),'ae');
        s = s.replace(new RegExp(/ç/g),'c');
        s = s.replace(new RegExp(/[èéêë]/g),'e');
        s = s.replace(new RegExp(/[ìíîï]/g),'i');
        s = s.replace(new RegExp(/ñ/g),'n');                
        s = s.replace(new RegExp(/[òóôõö]/g),'o');
        s = s.replace(new RegExp(/œ/g),'oe');
        s = s.replace(new RegExp(/[ùúûü]/g),'u');
        s = s.replace(new RegExp(/[ýÿ]/g),'y');
        s = s.replace(new RegExp(/\W/g),'');
        return s;
    }

    goBack() {
        this.$router.go(-1);
    }

    goTo(PARENT_ID: any) { 
        const serviceChildren = this.services.filter(( d: any ) => d.PARENT_ID == PARENT_ID);
        localStorage.setItem("SERVICE_CHILDREN", JSON.stringify(serviceChildren));

        const servicesName = this.parentsMenu.find((d: any) =>  d.ID === PARENT_ID);
        localStorage.setItem("NAME_SERVICO_MENU", JSON.stringify(servicesName.NAME));
        
        const tipoServicos = this.services.filter((id: any) => {
            return id.TYPE;
        });
        const tiposFiltrados = tipoServicos.find((id: any) => id.ID === PARENT_ID);
        localStorage.setItem("TYPE_SERVICES", JSON.stringify(tiposFiltrados.TYPE))
        
        const descricao = this.parentsMenu.find((id: any) => id.ID === PARENT_ID);
        localStorage.setItem("DECRIPTION_SERVICES", JSON.stringify(descricao.DECRIPTION)) 

        this.$router.push('ServicesChildren');
    }

    goHome() {
        this.$router.push('HomeApp');
    }

    openMenuOptions() {
        this.menuOptions = true;
        localStorage.setItem('PULSE_CLASS', 'false');
    }
    closeMenuOptions() {
        const OPENLIST = 'popInBottom';
        const CLOSELIST = 'popOutBottom';
        const OPEN = 'popIn';
        const CLOSE = 'popOut';

        $('#option0').removeClass(OPENLIST);
        $('#option0').addClass(CLOSELIST);
        $('#option1').removeClass(OPENLIST);
        $('#option1').addClass(CLOSELIST);
        $('#option2').removeClass(OPENLIST);
        $('#option2').addClass(CLOSELIST);
        $('#option3').removeClass(OPENLIST);
        $('#option3').addClass(CLOSELIST);
        $('#option4').removeClass(OPENLIST);
        $('#option4').addClass(CLOSELIST);
        $('#option5').removeClass(OPENLIST);
        $('#option5').addClass(CLOSELIST);
        $('#btn').removeClass(OPEN);
        $('#btn').addClass(CLOSE);

        setTimeout(() => {
            this.menuOptions = false;
            this.componentKey++;
        }, 600);
    }

    openLogOut() {
        this.modalMsg = 'Você realmente deseja sair da sua conta?';
        this.openModal = true;
    }

    openChange() {
        this.modalMsg = 'Para trocar de clube é necessário sair da sua conta, deseja continuar?';
        this.openModal = true;
    }

    openExternalBrowser(item: any) {
        window.open(item.DECRIPTION, '_system');
    }


    logOff() {
        Util.logOff();
    }

    ok(){
        console.log('ok');
    }

    getRoute() {
        if (this.userData.dependents === "") {
            this.$router.push('MenuEducation');
        } else {
            this.$router.push('ModalityDependents');
        }
    }

    handleServiceClick(item: any){
        if (item.type == 'LINK'){
            this.openExternalBrowser(item);
        }
        else this.goTo(item.ID);
    }
}