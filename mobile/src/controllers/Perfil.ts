
import { Component, Prop, Vue } from 'vue-property-decorator'
import TextLink from '@/components/TextLink.vue'
import DefaultButton from '@/components/DefaultButton.vue'
import HeaderFluxo from '@/components/HeaderFluxo.vue'
import OptionList from '@/components/OptionList.vue'
import ModalCodigo from '@/components/ModalCodigo.vue'
import ImageProfile from '@/components/ImageProfile.vue'
import Request from '@/ts/Request';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Util from '@/ts/Util';
import EmptyState from '../components/EmptyState.vue';
import LoginModal from '@/modules/shared-components/Banner.vue';
import LoginFrontEnd, { LoginConfig } from '@/modules/login-frontend/LoginFrontEnd';
import moment from 'moment';


@Component({
    components: { TextLink, DefaultButton, HeaderFluxo, OptionList, ImageProfile, ModalCodigo, BottomNavigation, HeaderCircleButton, EmptyState, LoginModal }
})
export default class Perfil extends Vue {
    
    userData: any = localStorage.getItem('USER_DATA') ? JSON.parse(localStorage.getItem('USER_DATA') || '{}')||{} : {};
    editData: boolean = false;
    //Inputs definitions
    firstName: string = this.userData.FIRST_NAME || '';
    firstNameDisabled : any = null;

    lastName: string = this.userData.LAST_NAME || '';
    lastNameDisabled: any = null;

    fullName: string = this.firstName + ' ' + this.lastName;
    fullNameDisabled : any = null;

    cpf: string = this.userData.CPF;
    cpfDisabled: any = null;  

    npf?: number = this.userData.ORGANIZATION_DATA ? this.userData.ORGANIZATION_DATA.EXTERNAL_ID : undefined;
    npfDisabled: any = null;

    phoneNumber: string = this.userData.CONTACTS && this.userData.CONTACTS[0] ? this.userData.CONTACTS[0].PHONE : '';
    comparePhone: string = '';
    oldPhone: string = '';
    phoneDisabled: any = null;

    email: string = this.userData.EMAIL;
    compareEmail: string = '';
    emailDisabled: any = null;

    imgProfile: string = this.userData.IMAGE;
    imgDisabled: any = null;

    showModal: boolean = false;
    modalIcon: string = 'error_outline';
    modalMessage: string = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde.';

    notificacao: any = localStorage.getItem('HAS_NOTIFICATION');
    isUserLog: boolean = !Util.isEmpty(this.userData);
    showLoginModal: boolean = false;
    otherButton: any = [{ name:'logout', iconClass: 'fal', icon: 'sign-out-alt', colorClass: 'default', notification: 0, callback: this.goToLogin }]
    birthDate: any = "";
    logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL')||'bipfun-logo.svg';

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        setTimeout(function() {
            M.updateTextFields();
        }, 50);
        $('#ATUALIZAR_DADOS > button').prop('disabled', true);
        // const fields : any = localStorage.getItem('ORG_FIELDS') ? JSON.parse(localStorage.getItem('ORG_FIELDS') || '') : '';    
        // if (fields.find((f: any) => f.FIELD_NAME == 'FIRST_NAME').PERMISSION == 'N' || fields.find((f: any) => f.FIELD_NAME == 'LAST_NAME').PERMISSION == 'N') { this.fullNameDisabled = 'disabled' }
        // else { this.fullNameDisabled = ''; }
        // if (fields.find((f: any) => f.FIELD_NAME == 'CPF').PERMISSION == 'N') {
        //     this.cpfDisabled = 'disabled';
        // };
        // if (fields.find((f: any) => f.FIELD_NAME == 'NPF').PERMISSION == 'N') {
        //     this.npfDisabled = 'disabled';
        // };

        setTimeout(function() {
            M.updateTextFields();
        }, 50);

        const self = this;
		if (localStorage.getItem('USER_DATA')) this.fillUserData();
		else if (localStorage.getItem('USER_ID')) this.getUserData(function(response: any) { self.fillUserData(response.user) });
        this.oldPhone = this.phoneNumber;
    }
	
	fillUserData(user: any = null) {
		user = user ? user : this.userData;
		
		const self = this;
		if (user) {
            this.userData = user;
			localStorage.setItem('USER_DATA', JSON.stringify(user));
			self.firstName = user.FIRST_NAME;
			self.lastName = user.LAST_NAME;
			self.fullName = this.firstName + ' ' + this.lastName;
			self.cpf = user.CPF;
			self.email = user.EMAIL;
			self.compareEmail = self.email;
            self.imgProfile = user.IMAGE;
            self.birthDate = (user.BIRTH_DATE && user.BIRTH_DATE.date) ? moment(user.BIRTH_DATE.date).format('DD/MM/YYYY') : "";
			if (user.ORGANIZATION_DATA.EXTERNAL_ID) {
				self.npf = user.ORGANIZATION_DATA.EXTERNAL_ID;
			}
			if (user.CONTACTS && user.CONTACTS[0]) {
				self.phoneNumber = user.CONTACTS[0].PHONE;
				self.comparePhone = self.phoneNumber;
			}
            localStorage.setItem('USER_DATE', JSON.stringify(user));
            setTimeout(function() {
                M.updateTextFields();
            }, 50);
		}
		setTimeout(function() {
			M.updateTextFields();
		}, 200);
	}

    disabledButton(ID: any){
        const self = this;
        setTimeout(function() {
            if(self.compareEmail == self.email && self.comparePhone == self.phoneNumber) $('#'+ID+' > button').prop('disabled', true);
            if(self.compareEmail != self.email || self.comparePhone != self.phoneNumber) $('#'+ID+' > button').prop('disabled',false);
        }, 200);
    }

    mostraModal(message: string, icon: string) {
        this.modalMessage = message;
        this.modalIcon = icon;
        this.showModal = true;
    }

    fechaModal() {
        this.getUserData(function() {} );
        this.showModal = false;
    }
    
    getUserData(callback: any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'USER_TYPE_ID': 1,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || "")

            };
            req.route = 'getUserData';
            req.callbackSuccess = callback;
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }

    }

    changeMobilePhoneNumber() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'OLD_PHONE': this.oldPhone,
                'NEW_PHONE': this.phoneNumber,
                'EMAIL': this.email,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN_EXT': localStorage.getItem('TOKEN_EXT')
            };
            req.route = 'changeMobilePhoneNumber';
            req.callbackSuccess =   function(response: any) {
                return response.status;
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    changeEmail() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'EMAIL': this.email,
                'NRORG': localStorage.getItem('NRORG'),
                'OLD_PHONE': this.oldPhone,
                'NEW_PHONE': this.phoneNumber,
                'TOKEN_EXT': localStorage.getItem('TOKEN_EXT')
            };
            req.route = 'changeEmail';
            req.callbackSuccess =   function(response: any) {
                localStorage.setItem('BIOMETRY_E', self.email);
                return response.status;
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    validateForm() {
        const form: HTMLFormElement = <HTMLFormElement> $('form')[0];
        return form.checkValidity();
    }

    updateUserData() {
        this.editData = false;
        try { 
            this.changeEmail();
            this.changeMobilePhoneNumber();

            if (!this.validateForm()) {
            this.mostraModal('Por favor, preencha todos os campos.', 'error_outline');
            return;
            }
            const self = this
            const req = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID') || '',
                "NRORG": localStorage.getItem('NRORG'),
                "TOKEN": localStorage.getItem('TOKEN') || '',
                "FIRST_NAME": self.firstName || '',
                "LAST_NAME": self.lastName || '',
                "CPF": self.getValidCharacters(self.cpf) || '',
                "DOCUMENT" : "SEMDOC"
            };
            req.route = 'requestUserChange';
            req.callbackSuccess = function(response: any) { 
                self.getUserData(function(response: any) { self.fillUserData(response.user) });
                if (response.status.status.includes('R'))
                    self.mostraModal('Dados enviados para atualização!', 'done');
                else if (response.status.status.includes('A'))
                    self.mostraModal('Dados atualizados com sucesso!', 'done');
                else 
                    self.mostraModal('Dados enviados.', 'error_outline');
            }
            ;
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = true;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    goBack() {
        Util.removeBackButtonBehavior();
        this.$router.push('Home');
    }

    focusInput( id:any ){
        const offset = -60;
        const el: any = $("#"+id);
        setTimeout(function(){
            $('html, body').animate({
                scrollTop: el.offset().top + offset
            }, 100);
        },150);
    }

    getValidCharacters(text: string) {
        var text = text;
        var finalText = '';
        var typeNum = true;

        for (var i = 0; i < text.length; i++) {
            if ((typeNum && this.validate_num(text.charAt(i))) || (!typeNum && this.validate_char(text.charAt(i)))) {
                finalText = finalText + text.charAt(i);
            }
        }
        return finalText.toUpperCase();
    }

    validate_char(c: string) {
        if ((c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57)
            || (c.charCodeAt(0) >= 65 && c.charCodeAt(0) <= 90)
            || (c.charCodeAt(0) >= 97 && c.charCodeAt(0) <= 122)) {
            return true;
        } else {
            return false;
        }
    }

    validate_num(c: string) {
        if (c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57) {
            return true;
        } else {
            return false;
        }
    }

    goToLogin(){
        Util.removeFCMToken();
        var loginSettings = localStorage.getItem('LOGIN_PROPS') || ''
        localStorage.clear(); 
        localStorage.setItem('LOGIN_PROPS', loginSettings);
        this.$router.push('/');
    }

    createAccount() {
        Util.removeBackButtonBehavior();
        LoginFrontEnd.goToRegister(this.goBack);
    }

}