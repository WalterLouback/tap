import { Component, Prop, Vue } from 'vue-property-decorator';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import CircleButton from '@/modules/shared-components/CircleButton.vue'
import Orders, { StoresConfig, RecentOrdersConfig } from "../modules/orders-module/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Banner from '@/modules/shared-components/Banner.vue';
import Util from '@/ts/Util';
import Request from '../ts/Request';
import BannerStore from '@/modules/shared-components/BannerStore.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import ListDefault from "../components/ListDefault.vue";
import Shortcuts from "../components/Shortcuts.vue";
import TextLink from "../components/TextLink.vue";
import NewsModal from "../components/NewsModal.vue";
import moment from 'moment';
import FCM from '@/ts/FCM';
import ImageCardButton from '@/modules/shared-components/ImageCardButton.vue';
import { createDecorator } from 'vue-class-component';


@Component({
    components: { BottomNavigation, HeaderBackground, CircleButton, HeaderCircleButton, Banner, BannerStore, LoginModal, ListDefault, Shortcuts, TextLink, NewsModal, ImageCardButton}
})

export default class Home extends Vue {
    componentKey: number = 0;
    image: any = { name: 'class_aqua', ext: '.jpg' }

    backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack }
    
    menuOptions: boolean = true;

    posts: any = JSON.parse(localStorage.getItem('POSTS')||'[]')||[];
    storesLoaded: boolean = true;
    notificationsKey: number = 1;


    placeholderImage: string = Util.getImgUrl('placeholder', '.png')
    image1: string = "";
    image2: string = "";
    image3: string = "";
    showLoginModal: boolean = false;
    homeNews: object[] = JSON.parse(localStorage.getItem('LAST_NEWS')||'[]')||[];
    selectedNews: string="";
    handleURL: any = null;
    lastScrollPosition: number = 0;
    logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL')||'';
    otherButton: any = [{ name:'notification', iconClass: 'fal', icon: 'bell', colorClass: 'default', notification: 0, callback: this.goToNotifications }]
    purchasePic:string = Util.getImgUrl('shopping','.jpg');
    schedulePic: string = Util.getImgUrl('calendar1','.jpg');
    billingPic:string = Util.getImgUrl('calculator1','.jpg');
    coursesPic: string = Util.getImgUrl('courses1','.jpg');
    carousel: any = {};
    carouselInterval: any = undefined;
        
    created(){
        if (!localStorage.getItem('BOTTOM_NAV_OPTIONS')){
            const bottomNavOptions: any = [ 
                { id:'btn0', name:'Início', icon: 'home-alt', active: false, route: '/Home', notification: 0},
                // { id:'btn1', name:'Comprar', icon: 'store', active: false, route: '/Explorar', notification: 0},
                { id:'btn2', name:'Comprar', icon: 'bags-shopping', active: false, route:'/Explorar', notification: 0},
                // { id:'btn3', name:'Eventos', icon:'compass',  active: false, route:'/MenuEvents', notification: 0},
                // { id:'btn4', name:'Agenda', icon: 'calendar-alt', active: false, route:'/Schedule', notification: 0},
                { id:'btn5', name:'Serviços', icon: 'sparkles', active: false, route:'/Services', notification: 0},
                { id:'btn6', name:'Mais', icon: 'ellipsis-h', active: false, route:'/Extras', notification: 0}
            ];
            localStorage.setItem('BOTTOM_NAV_OPTIONS', JSON.stringify(bottomNavOptions));
        }
    }

    beforeMount() {
        // const w: any = window;
        // this.handleURL = w.handleOpenURL;
        // w.handleOpenURL = this.handleOpenURL;
        this.updateFavoriteList();
    }

    beforeDestroy(){
        // const w: any = window;
        // w.handleOpenURL = this.handleURL;
        localStorage.setItem('LAST_HOME_POSITION', JSON.stringify(this.lastScrollPosition));
        clearInterval(this.carouselInterval);
    }

    mounted() {
        this.clearUserData();
        this.getStores();
        this.getNews();
        if(this.homeNews && this.homeNews.length){
            setTimeout(()=>this.initializeCarousel(), 100);
            setTimeout(()=>this.addCarouselInterval(), 200);
        }
        Util.setBackButtonBehavior(() => { });
        //const lastGetUserData: any = localStorage.getItem('LAST_GET_USER_DATA') && localStorage.getItem('USER_DATA') && localStorage.getItem('USER_DEPENDENTS') ? localStorage.getItem('LAST_GET_USER_DATA') : null;
        const lastGetNotifications: any = localStorage.getItem('LAST_GET_NOTIFICATIONS') ? localStorage.getItem('LAST_GET_NOTIFICATIONS') : null;
        const duration: any = (lastGetNotifications) ? moment.duration(moment().diff(lastGetNotifications)) : null;
        const diffTime: number = (duration) ? duration._data.hours : 0;

        //getEventsWithMenusFromTodayONS
        if (diffTime >= 0 && localStorage.getItem('USER_ID')) {
            Util.getUserNotifications((response: any) => {
                let currentNotifications: any = [];

                if (response.length > 0) {
                    currentNotifications = response;
                    // this.options.find((obj:any) => obj.name=='Notificações').notification = currentNotifications.length;
                    let currentNotificationsStorage: any = JSON.parse(localStorage.getItem('CURRENT_NOTIFICATIONS') || '[]');
                    let oldNotificationsStorage: any = JSON.parse(localStorage.getItem('OLD_NOTIFICATIONS') || '[]');
                    let newNotifications = currentNotifications.filter((objeto: any) => !oldNotificationsStorage.find((obj2: any) => obj2.id == objeto.id)) || [];
                    if (currentNotifications) {

                        if (newNotifications.length) {
                            localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(true));
                            this.otherButton.find((obj:any) => obj.name=='notification').notification = newNotifications.length;
                        } else { 
                            localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                            this.otherButton.find((obj:any) => obj.name=='notification').notification = 0;
                        }

                    }

                    localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                    this.notificationsKey++;

                } else {
                    currentNotifications = [];
                    localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                    localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                    this.notificationsKey++;
                }
            });
        }
        this.initFCM();
        localStorage.setItem('PARENT_EVENT', '');
        localStorage.setItem('EVENT_SCHEDULE_CACHE', '');
        //this.sendToSharedEvent();
        this.goBackToLastScroll();
    }

    sendToSharedEvent(){
        if(this.posts.length){
            if (localStorage.getItem('SHARE_PARENT_ID')) {
                var parentEventList = localStorage.getItem('SHARE_PARENT_ID')!.split(',');
                var parentEvent = parentEventList[0];
                this.goToEvent(parentEvent||'');
            }
            else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='e') this.goToEvent(localStorage.getItem('SHARE_ID')!);
            else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='s') this.goToStore(localStorage.getItem('SHARE_ID')!);
        }
    }

    handleOpenURL(url:any) {
        var data:any = url.split('?')[1].split('&');
        localStorage.setItem('SHARE_TYPE', '');
        localStorage.setItem('SHARE_ID', '');
        localStorage.setItem('SHARE_PARENT_ID', '');
        data.forEach(
          (d:any) =>{
            if (d.split('=')[0] == 'p') localStorage.setItem('SHARE_PARENT_ID', d.split('=')[1]);
            else if (d.split('=')[0] == 'id') localStorage.setItem('SHARE_ID', d.split('=')[1]);
            else if (d.split('=')[0] == 'et') localStorage.setItem('SHARE_TYPE', d.split('=')[1]);
          }
        );
        //this.sendToSharedEvent()
    }

    updated(){
        //this.sendToSharedEvent();
    }


    existsNewValues(a: any, b: any) {
        var newNotifications: number = 0;
        return b.find((objeto: any) => !a.find((obj2: any) => obj2.id == objeto.id)).length;
    }

    goToStore(id: string) {
        localStorage.setItem('IS_AN_EVENT', '');
        Util.removeBackButtonBehavior();
        localStorage.setItem('SHARE_ID', '')
        localStorage.setItem('SHARE_TYPE', '')
        Orders.goToStores(new StoresConfig(this.goHome, id));
    }

    goToEvent(id: string) {
        localStorage.setItem('EVENT', JSON.stringify(this.posts.find((event: any) => event.id == id)));
        localStorage.setItem('SELECTED_EVENT', JSON.stringify(id));
        this.$router.push("/EventDetails" );
    }

    goToNext(el: any){
        // if(el.type == 'E'){
            this.goToEvent(el.id);
        // }
        // else this.goToStore(el.id);
    }

    goToRegisteredCards() {
        Orders.goToRegisteredCards(new RecentOrdersConfig(this.goHome));
    }

    goToRecentOrders() {
        Orders.goToRecentOrders(new RecentOrdersConfig(this.goHome));
    }

   

    goBack() {
        this.showLoginModal = false;
    }

    goHome() {
        this.$router.push('Home');
    }

    openMenuOptions() {
        this.menuOptions = true;
        localStorage.setItem('PULSE_CLASS', 'false');
    }


    getStores() {
        try {
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
                "USER_ID":  (localStorage.getItem('USER_ID')) ? localStorage.getItem('USER_ID') :1,
                "TOKEN": "a",
                "STATUS": "A"
            };
            req.route = 'getEventsByOrg';
            req.callbackSuccess = (response: any) => {
                let posts: any = response.events.filter((evnt:any)=>evnt.status == "A");
                this.prepareStores(posts);
                this.image1 = posts[0].imageCover;
                this.$forceUpdate();
            };
            req.callbackError = function (error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    prepareStores(stores: any) {
        stores.forEach((store: any) => {
            if (!store.deliversToTable && !store.deliversToBalcony)
                store.opened = false
        });

        // let openedStores = stores.filter((s: any) => s.type == 'INTEREST');
        // this.posts = stores;
        let importantPosts = stores.filter((s: any) => s.type == 'INTEREST');
        let posts = stores.filter((s: any) => s.type == 'E');
        // stores = openedStores;
        this.posts = posts.slice(0,10);
        // this.homeNews = importantPosts;
        localStorage.setItem('POSTS', JSON.stringify(this.posts));
        // localStorage.setItem('LAST_NEWS', JSON.stringify(importantPosts));
        this.storesLoaded = true;
    }

    getNews() {
        try {
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "MAX_RESULTS": 4,
                "TOKEN": localStorage.getItem('TOKEN')
            };
            req.route = 'getLastNews';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('LAST_NEWS', JSON.stringify(response.news));
                if(!this.homeNews || !this.homeNews.length){
                    console.log('entrou');
                    this.homeNews = response.news;
                    setTimeout(()=>this.initializeCarousel(), 100);
                    setTimeout(()=>this.addCarouselInterval(), 200);
                }
                else {
                    this.homeNews = response.news;
                    //this.initializeCarousel();
                }
            };
            req.callbackError = function (error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    goToNotifications(){
        this.$router.push('Notifications');   
    }

    openNewsModal(news: any){
        const im: any = NewsModal;
        this.selectedNews = news;
        im.openModal('ad-banner');
    }

    initFCM() {
        new FCM().init(Util.onNewFCMToken, this.onPushOrderTapped, this.forceRerender);
    }

    forceRerender(data: any) {
        Util.getUserNotifications((response: any) => {
            let currentNotifications: any = [];

            if (response.length > 0) {
                currentNotifications = response;
                // this.options.find((obj:any) => obj.name=='Notificações').notification = currentNotifications.length;
                let currentNotificationsStorage: any = JSON.parse(localStorage.getItem('CURRENT_NOTIFICATIONS') || '[]');
                let oldNotificationsStorage: any = JSON.parse(localStorage.getItem('OLD_NOTIFICATIONS') || '[]');
                let newNotifications = currentNotifications.filter((objeto: any) => !oldNotificationsStorage.find((obj2: any) => obj2.id == objeto.id)) || [];
                if (currentNotifications) {

                    if (newNotifications.length) {
                        localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(true));
                        this.otherButton.find((obj:any) => obj.name=='notification').notification = newNotifications.length;
                    } else { 
                        localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                        this.otherButton.find((obj:any) => obj.name=='notification').notification = 0;
                    }

                }

                localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                this.notificationsKey++;

            } else {
                currentNotifications = [];
                localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                this.notificationsKey++;
            }
            this.$forceUpdate();
        });
       
    }

    handleScroll(){ 
        var element = <HTMLElement>this.$refs.bannerContainer;
        if(element) this.lastScrollPosition = element.scrollTop;
    }

    goBackToLastScroll(){
        var element = <HTMLElement>this.$refs.bannerContainer;
        if (localStorage.getItem('LAST_HOME_POSITION')){
            this.lastScrollPosition = Number(localStorage.getItem('LAST_HOME_POSITION'))
        }
        if(element) element.scrollTop = this.lastScrollPosition;
    }

    onPushOrderTapped(data: any) {
    }

    updateFavoriteList(){
        try{
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'favorite';
            req.callbackSuccess = (response:any) => {
                localStorage.setItem('FAVORITE_EVENTS_AND_STORES',JSON.stringify(response.calendar.filter((obj: any)=> !!obj.favoriteId)));
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            if(localStorage.getItem('USER_ID')) req.send();
        }catch (e){
            console.log(e)
        }
    }

    clearUserData(){
        localStorage.removeItem('USER_NAME');
        localStorage.removeItem('MAIN_CARTAO');
        localStorage.setItem('USER_HAS_NOTIFICATIONS', 'false');
        localStorage.removeItem('LAST_GET_USER_DATA');
        localStorage.removeItem('ALL_CC');
        localStorage.removeItem('USER_ID');
        localStorage.removeItem('USER_EXT_ID');
        localStorage.removeItem('USER_DATA');
        localStorage.removeItem('CREDIT_CARDS');
        localStorage.removeItem('USER_ORGANIZATIONS');
        localStorage.removeItem('CURRENT_ORDER');
        localStorage.removeItem('TOKEN');
        localStorage.removeItem('REFRESH_TOKEN');
    }

    goToEvents(){
        localStorage.setItem('CAME_FROM_HOME', 'true');
        this.$router.push('Events');
    }

    ongoingEventCheck(e: any){
        return moment(new Date()).isBetween(new Date(e.initialDate.date), new Date(e.finalDate.date), 'minutes')
    }

    initializeCarousel(){
        var elems = document.querySelectorAll('.carousel');
        var instances = M.Carousel.init(elems, {
            fullWidth: true,
            indicators: true
            });
    }


    addCarouselInterval(){
        if(this.carouselInterval === undefined) this.carouselInterval = setInterval(()=>this.autoplayNextSlide(),5000);
    }

    autoplayNextSlide(){
        var elem: any = this.$refs["bannerList"];
        this.carousel = M.Carousel.getInstance(elem as HTMLElement);
        this.carousel.next();
    }

    goToNextSlide(){
        clearInterval(this.carouselInterval);
        this.carouselInterval = undefined;
        var elem: any = this.$refs["bannerList"];
        this.carousel = M.Carousel.getInstance(elem as HTMLElement);
        this.carousel.next();
        setTimeout( ()=>this.addCarouselInterval(), 5000);
    }

    goToPreviousSlide(){
        clearInterval(this.carouselInterval);
        this.carouselInterval = undefined;
        var elem: any = this.$refs["bannerList"];
        this.carousel= M.Carousel.getInstance(elem as HTMLElement);
        this.carousel.prev();
        setTimeout( ()=>this.addCarouselInterval(), 5000);
    }

    suspendAutoPlay(){
        // clearInterval(this.carouselInterval);
        // this.carouselInterval = undefined;
        // setTimeout( ()=>this.addCarouselInterval(), 5000);
        console.log("scroll");
    }
}
