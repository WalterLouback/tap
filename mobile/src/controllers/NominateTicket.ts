import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionTitle from '@/components/SectionTitle.vue';
import EventDetailImage from '@/components/EventDetailImage.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import SelectField from '@/components/SelectField.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import TextLink from '@/components/TextLink.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';

declare var M: any;
declare var $: any;
@Component({
    components: { SectionTitle, EventDetailImage, DefaultButton, HeaderFluxo, SelectField, TextLink, ModalCodigo, HeaderCircleButton }
})

export default class NominateTicket extends Vue {
    newsDetail: any = [];
    dependentes: any = [];
    ticketsNominate: any = [];
    ORDER_ITEMS: any = [];
    showModal: any = false;
    orders: any = JSON.parse(localStorage.getItem('ORDER_ITEMS') || '[]');
    amount_items: any = JSON.parse(localStorage.getItem('AMOUNT_ITEMS') || '[]');
    user_data: any = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    event: any = JSON.parse(localStorage.getItem('EVENT') || '{}');
    backButton: any = { iconClass: 'fas', icon: 'chevron-left', callback: this.goBack };

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        
        this.getUserDependents();
        let self = this;
        let dependentes:any = {};
        this.dependentes.forEach((dependent:any) => {
            dependentes[dependent.NAME] = dependent.IMAGE_PROFILE;
        });
        $('input.autocomplete').autocomplete({
            data: dependentes,
        });
    }

    getUserTicketsByOrg() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "NRORG": localStorage.getItem('NRORG')
            };
            req.route = 'getUserTicketsByOrg';
            req.callbackSuccess = function(response: any) {
                localStorage.setItem('USER_TICKETS', JSON.stringify(response.userTickets.filter((tickets: any) => moment().isBefore(tickets.finalDate.date))));
                localStorage.setItem('TICKETS_VALIDOS', JSON.stringify(response.userTickets.filter((tickets: any) => moment().isBefore(tickets.finalDate.date)).length));
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    nominateAllTickets(){
        this.orders.forEach((item:any, index: number) => {
            let user   = this.createUser(index);
            let ticket = this.ORDER_ITEMS.find((t: any) => t.EVENT_TICKET_ID == item.id);

            if (!ticket) {
                this.ORDER_ITEMS.push({
                    'EVENT_TICKET_ID': item.id,
                    'USERS': [ user]
                });
            } else {
                ticket.USERS.push(user);
            }
        });
    }
    
    createUser(index: number) {
        let name: any, idUser: any;

        if($('#dependente'+index).prop('checked')) {
            name = $('#dependente_nome'+index).val();
            const dependente = this.dependentes.find(((dependente:any) => dependente.NAME == name));
            idUser = dependente.ID;
            name = '';
        }
        if($('#outros'+index).prop('checked')) {
            name = $('#outro_name'+index).val();
            idUser = null;
        }

        return { 'NAME': name, 'ID': idUser };
    }

    createOrderTicket() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'PAYMENT_METHOD': 'CC',
                'CREDIT_CARD_ID': this.user_data.MAIN_CARD_ID,
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.event.id,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'ORDER_ITEMS': this.ORDER_ITEMS,
            };
            req.route = 'createOrderTicket';
            req.callbackSuccess = function(response: any) {
                self.goToTicketBuyed();
                self.getUserTicketsByOrg();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    getUserDependents() {
        let dependents: any = JSON.parse(localStorage.getItem('USER_DEPENDENTS') || '{}');
        let self = this;
        dependents.forEach((dependent:any) => {
            self.dependentes.push({'ID': dependent.USER_DATA.ID, 'NAME': dependent.USER_DATA.FIRST_NAME+' '+dependent.USER_DATA.LAST_NAME, 'IMAGE_PROFILE': dependent.USER_DATA.IMAGE, 'PRIVILEGIADO': false });
        });
        self.dependentes.push({'ID': self.user_data.ID, 'NAME': self.user_data.FIRST_NAME+' '+self.user_data.LAST_NAME, 'IMAGE_PROFILE': self.user_data.IMAGE, 'PRIVILEGIADO': false });
    }

    verifyRadioDependent(INDEX: any){
        if($('#dependente'+INDEX).prop('checked')){
            $('#nameDependente'+INDEX).removeClass('display-none').addClass('display-flex');
            $('#nameOutro'+INDEX).removeClass('display-flex').addClass('display-none');
            $('#outro_name'+INDEX).val("");
        }
    }

    openModal() {
        this.showModal = true;
        this.nominateAllTickets();
    }

    closeModal() {
        this.showModal = false;
    }

    verifyRadioOutros(INDEX: any){
        if($('#outros'+INDEX).prop('checked')){
            $('#nameDependente'+INDEX).removeClass('display-flex').addClass('display-none');
            $('#nameOutro'+INDEX).removeClass('display-none').addClass('display-flex');
            $('#dependente_nome'+INDEX).val('');
        }
    }

    goToTicketBuyed(){
        this.$router.push({ name: 'TicketBuyed' });
    }

    goBack() {
        this.$router.go(-1);
    }

}