import { Component, Prop, Vue } from 'vue-property-decorator';
import BottomNavigation from '@/modules/shared-components/BottomNavigation.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'
import CircleButton from '@/modules/shared-components/CircleButton.vue'
import Orders, { StoresConfig, RecentOrdersConfig } from "../modules/orders-module/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
import Banner from '@/modules/shared-components/Banner.vue';
import Util from '@/ts/Util';
import Request from '../ts/Request';
import BannerStore from '@/modules/shared-components/BannerStore.vue';
import LoginModal from '@/modules/shared-components/LoginModal.vue';
import ListDefault from "../components/ListDefault.vue";
import Shortcuts from "../components/Shortcuts.vue";
import TextLink from "../components/TextLink.vue";
import NewsModal from "../components/NewsModal.vue";
import moment from 'moment';
import FCM from '@/ts/FCM';
import Minicard from '@/modules/shared-components/Minicard.vue';
import { createDecorator } from 'vue-class-component';
import EmptyState from '../components/EmptyState.vue'


declare var KioskPlugin: any;
declare var window: any;

@Component({
    components: { BottomNavigation, HeaderBackground, CircleButton, HeaderCircleButton, Banner, BannerStore, LoginModal, ListDefault, Shortcuts, TextLink, NewsModal, Minicard, EmptyState }
})

export default class Explorar extends Vue {
    componentKey: number = 0;
    image: any = { name: 'class_aqua', ext: '.jpg' }
    version: string = "1.7"

    backButton: any = { iconClass: 'fal', icon: 'chevron-left', callback: this.goBack }

    menuOptions: boolean = true;

    stores: any = JSON.parse(localStorage.getItem('STORES') || '[]') || [];

    storesLoaded: boolean = false;
    notificationsKey: number = 1;
    storeNotFound: boolean = false;
    unica: boolean = false;

    placeholderImage: string = Util.getImgUrl('placeholder', '.png')
    image1: string = "";
    image2: string = "";
    image3: string = "";
    showLoginModal: boolean = false;
    homeNews: object[] = JSON.parse(localStorage.getItem('LAST_NEWS') || '[]') || [];
    selectedNews: string = "";
    handleURL: any = null;
    lastScrollPosition: number = 0;
    logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL') || '';
    logoOverlay: string = Util.getImgUrl('logo-teknisa', '.svg');
    overlayBackgroundImage: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL') || '';
    otherButton: any = [{ name: 'notification', iconClass: 'fal', icon: 'bell', colorClass: 'default', notification: 0, callback: this.goToNotifications }]
    events: any = JSON.parse(localStorage.getItem('POSTS') || '[]');
    event: any = (this.events.lenght) ? this.events[0] : undefined;
    showEvent: boolean = (this.event && this.event.initialDate.date) ? moment(new Date()).isBetween(new Date(this.event.initialDate.date), new Date(this.event.finalDate.date), "m") : false;
    taas: any = JSON.parse(localStorage.getItem('TAA_CONFIG') || '[]') ? JSON.parse(localStorage.getItem('TAA_CONFIG') || '[]') : "[]";
    /*Overlay com instruções*/
    showInstructionOverlay: boolean = localStorage.getItem('SHOW_EXPLORAR_OVERLAY') !== 'undefined' && localStorage.getItem('SHOW_EXPLORAR_OVERLAY') == 'true' ? true : false;
    overlayTimer: any = null
    exitCounter: any = 0;
    barCode: any = '';
    taa = JSON.parse(localStorage.getItem('TAA_SELECTED') || '[]');
    codigorecebido: any;


    created() {
        this.getOrganizationData();
        localStorage.setItem('EVENT_SALES_METHOD', '');
        if (this.event && this.event.id && !this.event.menuId) {
            this.showEvent = false;
        }
        this.overlayTimer = setTimeout(() => this.openOverlay(), 45000);
        document.addEventListener("click", this.refreshOverlayTimer, true);
        localStorage.setItem('FROM_EXPLORAR', 'true');
        if(this.taa.modality=='L')
        this.fechaOverlay();
    }


    beforeMount() {
        // const w: any = window;
        // this.handleURL = w.handleOpenURL;
        // w.handleOpenURL = this.handleOpenURL;
      //  this.updateFavoriteList();

        if (this.taa.length == 0) {
            this.taa = this.taas[0];
        }
    }

    beforeDestroy() {
        // const w: any = window;
        // w.handleOpenURL = this.handleURL;
        localStorage.removeItem("TICKETS")
        localStorage.removeItem('ORDER_TICKET_DATAS');
        localStorage.setItem('LAST_HOME_POSITION', JSON.stringify(this.lastScrollPosition));
    }

    mounted() {
        if (localStorage.getItem('STORE_BACK') == '[]') {
            this.showInstructionOverlay = false;
            localStorage.setItem('STORE_BACK', '')
        }
        if (!localStorage.getItem('STORES')) {
            this.storesLoaded = false;
        }
        else {
            this.storesLoaded = true;
            Util.removeAddProdutos();
            Util.removeAddPedido();
        }
        this.getAvailableTaa()

        let overlayImage: any = JSON.parse(localStorage.getItem('ORGANIZATION_DATA') || '[]');
        if (overlayImage.CONFIGURATIONS && overlayImage.CONFIGURATIONS.OVERLAY_BACKGROUND) {
            this.overlayBackgroundImage = overlayImage.CONFIGURATIONS.OVERLAY_BACKGROUND;
        } else {
            this.overlayBackgroundImage = 'https://bl6pap003files.storage.live.com/y4mymgckNuZHOqZigphKw_OBNSgppV3JA9ln1WxR2-jY1-btRAMlLSUEEd9fDf1VqgLLlKggNHkMxfoLYL3sg1Ng5TKzRPfuEH_xisA9byIxkVSn_pryWo5Mfusb4Ug7Sz2i8THzac1IfZ39dfwajGagk6yjIoBXVufaro_rXK3OWbucUFQMdW_smktgXAGJs6iCvUGy8kOJas2J089o886Og/taa-generico.png?psid=1&width=491&height=873';
        }

        this.getStores();
        //this.getNews();
        Util.setBackButtonBehavior(() => { });
        //const lastGetUserData: any = localStorage.getItem('LAST_GET_USER_DATA') && localStorage.getItem('USER_DATA') && localStorage.getItem('USER_DEPENDENTS') ? localStorage.getItem('LAST_GET_USER_DATA') : null;
        const lastGetNotifications: any = localStorage.getItem('LAST_GET_NOTIFICATIONS') ? localStorage.getItem('LAST_GET_NOTIFICATIONS') : null;
        const duration: any = (lastGetNotifications) ? moment.duration(moment().diff(lastGetNotifications)) : null;
        const diffTime: number = (duration) ? duration._data.hours : 0;

        //getEventsWithMenusFromTodayONS
        // if (diffTime >= 0 && localStorage.getItem('USER_ID')) {
        //     Util.getUserNotifications((response: any) => {
        //         let currentNotifications: any = [];

        //         if (response.length > 0) {
        //             currentNotifications = response;
        //             // this.options.find((obj:any) => obj.name=='Notificações').notification = currentNotifications.length;
        //             let currentNotificationsStorage: any = JSON.parse(localStorage.getItem('CURRENT_NOTIFICATIONS') || '[]');
        //             let oldNotificationsStorage: any = JSON.parse(localStorage.getItem('OLD_NOTIFICATIONS') || '[]');
        //             let newNotifications = currentNotifications.filter((objeto: any) => !oldNotificationsStorage.find((obj2: any) => obj2.id == objeto.id)) || [];
        //             if (currentNotifications) {

        //                 if (newNotifications.length) {
        //                     localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(true));
        //                     // this.otherButton.find((obj:any) => obj.name=='notification').notification = newNotifications.length;
        //                 } else {
        //                     localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
        //                     // this.otherButton.find((obj:any) => obj.name=='notification').notification = 0;
        //                 }

        //             }

        //             localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
        //             this.notificationsKey++;

        //         } else {
        //             currentNotifications = [];
        //             localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
        //             localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
        //             this.notificationsKey++;
        //         }
        //     });

        // }
        this.initFCM();
        localStorage.setItem('PARENT_EVENT', '');
        localStorage.setItem('EVENT_SCHEDULE_CACHE', '');
        localStorage.setItem('CURRENT_ORDER', '');
        //this.sendToSharedEvent();
        this.goBackToLastScroll();
        
                Util.setAddProdutos( (event:any) => {
                    const keyName = event.key;
        
                    if(keyName!='Enter'){
                        this.codigorecebido+=keyName;
                        
                    }else{
                        localStorage.setItem('BARCODE_EXPLORAR', this.codigorecebido)
                        this.fechaOverlay();
                      // this.receiveProduct(this.codigorecebido);
                    
                    }
                });


    }
    getOrganizationData() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem("NRORG"),
            'USER_ID': localStorage.getItem('USER_ID'),
            'USER_TYPE_ID': 4,
        };
        req.route = 'getOrganizationData';
        req.callbackSuccess = (response: any) => {
            const name: string = response.organization.NAME;
            const editableFields: any = response.organization.EDITABLE_FIELDS;
            let PRIMARY_COLOR = response.organization.CONFIGURATIONS.PRIMARY_COLOR;
            let SUPPORT_COLOR = response.organization.CONFIGURATIONS.SECONDARY_COLOR;

            localStorage.setItem('ORG_NAME', name);
            localStorage.setItem('ORG_FIELDS', JSON.stringify((editableFields)));
            localStorage.setItem('ORGANIZATION_DATA', JSON.stringify((response.organization)));
            localStorage.setItem('ORGANIZATION_LOGO', response.organization.CONFIGURATIONS.LOGO_IMAGE);
            localStorage.setItem('ORGANIZATION_LOGO_SMALL', response.organization.CONFIGURATIONS.LOGO_IMAGE_SMALL);
            localStorage.setItem('STONE_ESTABLISHMENT_ID', response.organization.CONFIGURATIONS.STONE_ESTABLISHMENT_ID);
            //this.getEstablishmentData(response.organization.CONFIGURATIONS.STONE_ESTABLISHMENT_ID);
            if (document.documentElement) {
                document.documentElement.style.setProperty('--primary-color', PRIMARY_COLOR);
                localStorage.setItem('PRIMARY_COLOR', PRIMARY_COLOR)

            }
            if (document.documentElement) {
                document.documentElement.style.setProperty('--secundary-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--support-color', SUPPORT_COLOR);
                document.documentElement.style.setProperty('--secundary-support-color', SUPPORT_COLOR);
                localStorage.setItem('SECONDARY_COLOR', SUPPORT_COLOR);
            }

            //LoginFrontEnd.callback();

        };
        req.callbackError = function (error: any) {
            let mensagem: string;
            if (error.response.data.errorCode == 24) {
                mensagem = 'Usuário não autorizado pelo titular!';
                console.log(mensagem);
            }
            if (error.response.data.errorCode == 14) {
                mensagem = 'Usuário não autorizado pela organização!';
                console.log(mensagem);
            }
            console.log(error);
        };
        // req.loadingMessage = 'Preparando o aplicativo...';
        req.blockTouchEvents = false;
        req.send();
    }

    sendToSharedEvent() {
        if (this.stores.length) {
            if (localStorage.getItem('SHARE_PARENT_ID')) {
                var parentEventList = localStorage.getItem('SHARE_PARENT_ID')!.split(',');
                var parentEvent = parentEventList[0];
                this.goToEvent(parentEvent || '');
            }
            else if (localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE') == 'e') this.goToEvent(localStorage.getItem('SHARE_ID')!);
            else if (localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE') == 's') this.goToStore(localStorage.getItem('SHARE_ID')!);
        }
    }

    handleOpenURL(url: any) {
        var data: any = url.split('?')[1].split('&');
        localStorage.setItem('SHARE_TYPE', '');
        localStorage.setItem('SHARE_ID', '');
        localStorage.setItem('SHARE_PARENT_ID', '');
        data.forEach(
            (d: any) => {
                if (d.split('=')[0] == 'p') localStorage.setItem('SHARE_PARENT_ID', d.split('=')[1]);
                else if (d.split('=')[0] == 'id') localStorage.setItem('SHARE_ID', d.split('=')[1]);
                else if (d.split('=')[0] == 'et') localStorage.setItem('SHARE_TYPE', d.split('=')[1]);
            }
        );
        //this.sendToSharedEvent()
    }

    updated() {
        //this.sendToSharedEvent();
    }


    existsNewValues(a: any, b: any) {
        var newNotifications: number = 0;
        return b.find((objeto: any) => !a.find((obj2: any) => obj2.id == objeto.id)).length;
    }

    goToStore(id: string) {
        localStorage.setItem('IS_AN_EVENT', '');
        Util.removeBackButtonBehavior();
        localStorage.setItem('SHARE_ID', '');
        localStorage.setItem('SHARE_TYPE', '');
        Orders.goToStores(new StoresConfig(this.goHome, id));
    }

    goToEvent(id: string) {
        var event: any = this.stores.find((event: any) => event.id == id);
        // localStorage.setItem('EVENT_SALES_METHOD', event.salesMethod);
        localStorage.setItem('EVENT', JSON.stringify(event));
        localStorage.setItem('SELECTED_EVENT', JSON.stringify(id));
        // this.$router.push("/EventDetails" );
        localStorage.setItem('IS_AN_EVENT', 't');
        Orders.goToStores(new StoresConfig(this.goHome, id));
    }

    goToNext(el: any) {
        if (el.type == 'E') {
            this.goToEvent(el.id);
        } else {
            localStorage.setItem('STORE_IP', el.store_ip);
            this.goToStore(el.id);
        }
    }

    goToRegisteredCards() {
        Orders.goToRegisteredCards(new RecentOrdersConfig(this.goHome));
    }

    goToRecentOrders() {
        Orders.goToRecentOrders(new RecentOrdersConfig(this.goHome));
    }

    goBack() {
        this.$router.push('ChooseTaas');
    }

    goHome() {
        this.$router.push('Home');
    }

    openMenuOptions() {
        this.menuOptions = true;
        localStorage.setItem('PULSE_CLASS', 'false');
    }

    getStores() {
        try {
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
                "USER_ID": (localStorage.getItem('USER_ID')) ? localStorage.getItem('USER_ID') : 1,
                "TOKEN": "a",
                "STATUS": "A"
            };
            req.route = 'getEventsAndStores';
            req.callbackSuccess = (response: any) => {
                let stores: any;
                if (this.taa.modality == 'D') {
                    stores = response.events.filter((evnt: any) => (evnt.status == "A" || evnt.status == "O") && evnt.structureId == localStorage.getItem('STRUCTURE_ID') && evnt.id == localStorage.getItem('storeId'));
                } else {
                    stores = response.events.filter((evnt: any) => (evnt.status == "A" || evnt.status == "O") && evnt.structureId == localStorage.getItem('STRUCTURE_ID'));
                }
                (this.stores.length == 0) ? this.storeNotFound = true : this.storeNotFound = false;
                // let stores: any = response.events;
                localStorage.setItem('SHOW_EXPLORAR_OVERLAY', 'false');
                // console.log(this.stores.length == 1);
                //console.log(stores[0].id)
                this.lojaAberta(stores[0].id);
                this.prepareStores(stores);
                // this.sen
                this.$forceUpdate();
                //this.storesLoaded = true
            };
            req.callbackError = function (error: any) {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }
    lojaAberta(id: string) {
        try {
            const req = new Request();
            req.body = {
                "USER_ID": "3238",
                "TOKEN": "TK_USER_ID_3238",
                "STORE_ID": id
            }
            req.route = 'isOpened';
            req.callbackSuccess = (response:any) => {
            //localStorage.setItem("OPENED", response.opened.value)
            }
            req.callbackError = () => {
                console.log("Error");
            }

            if (localStorage.getItem('USER_ID')) req.send();
        } catch (e) {
            console.log(e)
        }
    }
    lojaUnica() {
        if (this.stores.length == 1) {
            localStorage.setItem('STORE_UNICA', 'U');
            this.goToNext(this.stores[0])
        } else {
            this.unica = false;
            localStorage.setItem('STORE_UNICA', '');
            return false;
        }
    }

    prepareStores(stores: any) {
        // stores.forEach((store: any) => {
        //     if (!store.deliversToTable && !store.deliversToBalcony)
        //         store.opened = false
        // });

        // let openedStores = stores.filter((s: any) => s.opened == true && s.type !='INTEREST')
        // let closedStores = stores.filter((s: any) => s.opened == false && s.type !='INTEREST')

        // stores = openedStores.concat(closedStores)

        this.stores = stores.reverse();
        localStorage.setItem('STORES', JSON.stringify(this.stores));
        this.storesLoaded = true;
        //setTimeout(() => {
         //   this.showInstructionOverlay = true;
        //, 6000);
        this.showInstructionOverlay = true;
        //console.log(this.stores.length == 1);
       // if (!this.showInstructionOverlay) this.lojaUnica();
    }

    getNews() {
        try {
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "MAX_RESULTS": 4,
                "TOKEN": localStorage.getItem('TOKEN')
            };
            req.route = 'getLastNews';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('LAST_NEWS', JSON.stringify(response.news));
                this.homeNews = response.news;

            };
            req.callbackError = function (error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    goToNotifications() {
        this.$router.push('Notifications');
    }

    openNewsModal(news: any) {
        const im: any = NewsModal;
        this.selectedNews = news;
        im.openModal('ad-banner');
    }

    initFCM() {
        new FCM().init(Util.onNewFCMToken, this.onPushOrderTapped, this.forceRerender);
    }

    forceRerender(data: any) {
        Util.getUserNotifications((response: any) => {
            let currentNotifications: any = [];

            if (response.length > 0) {
                currentNotifications = response;
                // this.options.find((obj:any) => obj.name=='Notificações').notification = currentNotifications.length;
                let currentNotificationsStorage: any = JSON.parse(localStorage.getItem('CURRENT_NOTIFICATIONS') || '[]');
                let oldNotificationsStorage: any = JSON.parse(localStorage.getItem('OLD_NOTIFICATIONS') || '[]');
                let newNotifications = currentNotifications.filter((objeto: any) => !oldNotificationsStorage.find((obj2: any) => obj2.id == objeto.id)) || [];
                if (currentNotifications) {

                    if (newNotifications.length) {
                        localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(true));
                        // this.otherButton.find((obj:any) => obj.name=='notification').notification = newNotifications.length;
                    } else {
                        localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                        // this.otherButton.find((obj:any) => obj.name=='notification').notification = 0;
                    }

                }

                localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                this.notificationsKey++;

            } else {
                currentNotifications = [];
                localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
                localStorage.setItem('CURRENT_NOTIFICATIONS', JSON.stringify(currentNotifications));
                this.notificationsKey++;
            }
            this.$forceUpdate();
        });

    }

    handleScroll() {
        var element = <HTMLElement>this.$refs.bannerContainer;
        if (element) this.lastScrollPosition = element.scrollTop;
    }

    goBackToLastScroll() {
        var element = <HTMLElement>this.$refs.bannerContainer;
        if (localStorage.getItem('LAST_HOME_POSITION')) {
            this.lastScrollPosition = Number(localStorage.getItem('LAST_HOME_POSITION'))
        }
        if (element) element.scrollTop = this.lastScrollPosition;
    }

    onPushOrderTapped(data: any) {
    }

    updateFavoriteList() {
        try {
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'favorite';
            req.callbackSuccess = (response: any) => {
                localStorage.setItem('FAVORITE_EVENTS_AND_STORES', JSON.stringify(response.calendar.filter((obj: any) => !!obj.favoriteId)));
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            if (localStorage.getItem('USER_ID')) req.send();
        } catch (e) {
            console.log(e)
        }
    }
    getAvailableTaa() {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN"),
                "STRUCTURE_ID": localStorage.getItem("STRUCTURE_ID"),
                "STATUS": "A"
            }
            req.route = 'getAvailableTaa';
            req.callbackSuccess = (response: any) => { /*{console.log(response.taas);*/
                localStorage.setItem('TAA_CONFIG', JSON.stringify(response.taas));

                self.taas = JSON.parse(localStorage.getItem('TAA_CONFIG') || '{}');
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    getCurrentWorkshift(store: any) {
        if (store.shiftInitialTime && store.shiftFinalTime) {
            var initialTime = moment(store.shiftInitialTime).format("HH:mm");
            var finalTime = moment(store.shiftFinalTime).format("HH:mm");
            return initialTime + "h" + ' até ' + finalTime + "h";
        }
        else return "";
    }

    getEventDate(e: any) {
        return moment(e.initialDate.date).format('DD/MM/YYYY HH:mm');
    }

    openOverlay() {
        this.showInstructionOverlay = true;
    }

    fechaOverlay() {
        this.showInstructionOverlay = false;
        localStorage.setItem('SHOW_EXPLORAR_OVERLAY', 'false');
        const taa = JSON.parse(localStorage.getItem('TAA_SELECTED') || '[]');
        if (!(taa.modality.toUpperCase() == 'L' && (taa.storeId != null || taa.storeId != "null"))) {
       // this.lojaUnica()
    };
        this.verifyCommand();
        //ifthis.getStores();
        Util.syncCodePush();
    }

    refreshOverlayTimer() {
        clearInterval(this.overlayTimer);
        this.overlayTimer = undefined;
        this.overlayTimer = setTimeout(() => this.openOverlay(), 45000)
    }

    exitKiosk() {
        if (this.exitCounter > 15) {
            KioskPlugin.exitKiosk();
            Util.syncCodePush();
        }
        else {
            this.exitCounter += 1;

        }
    }

    verifyCommand() {
        const taa = JSON.parse(localStorage.getItem('TAA_SELECTED') || '[]');
        if (taa.modality.toUpperCase() == 'L' && (taa.storeId != null || taa.storeId != "null")) {
            this.$router.push('TicketPayment');
        }
        return;
    }

}
