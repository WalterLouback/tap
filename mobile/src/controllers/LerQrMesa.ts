import { Component, Prop, Vue } from 'vue-property-decorator'
import TextLink from '../components/TextLink.vue'
import DefaultButton from '../components/DefaultButtonOrd.vue'
import QRscanner from '../ts/QRscanner'
import ModalCodigo from '../components/ModalCodigo.vue';
import HeaderFluxo from '../components/HeaderFluxo.vue';
import Util from '../ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';

declare var M: any;
declare var $: any;
declare var window: any;

@Component({
    components: { DefaultButton, TextLink, ModalCodigo, HeaderFluxo, HeaderCircleButton }
})

export default class LerQrMesa extends Vue {
    showModal: boolean = false;
    errorMessage: string = 'Ocorreu um erro desconhecido. Verifique sua conexão.'

    scanner: any = new QRscanner();
    backButton: any         = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack};

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        const self = this;

        this.scanner.scan(
            function (err: any, content:string) {
            if (err) {
                self.mostraModal('Não foi possível abrir a câmera');
            }
            else {
                localStorage.setItem('deliverToBalcony', "false");
                let structures = JSON.parse(localStorage.getItem('STRUCTURES_QR') || '[]');
                let structure  = structures.find((structure:any) => structure.externalQrcode == content);
                if (!structure) {
                    self.mostraModal('QrCode inválido.');
                    return ;
                }
                let structureId = structure.id;
                localStorage.setItem('structureId', structureId); 
                if (localStorage.getItem('QR_FROM') == 'EscolherUnidade') {
                    localStorage.setItem('stores', '[]');
                    self.$router.push('Lojas');
                }
                else {
                    self.$router.go(-1);
                }
            }
        });

    }

    goBack(){
        // if(localStorage.getItem('QR_FROM')) this.$router.push(localStorage.getItem('QR_FROM')||'Lojas');
        // else this.$router.go(-1);
        this.$router.go(-1);
    }

    beforeDestroy() {
        this.scanner.stop();
    }

    mostraModal(message: string) {
        this.errorMessage = message;
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
        this.$router.go(-1);
    }

}