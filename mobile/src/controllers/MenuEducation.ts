import { Component, Prop, Vue } from 'vue-property-decorator';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderModality from '@/components/HeaderModality.vue';
import ListDefault from '@/components/ListDefault.vue';
import ButtonCard from '@/components/ButtonCard.vue';
import SectionArea from '@/components/SectionArea.vue';
import BottomNavigation from '@/components/BottomNavigation.vue';
import MenuOptions from '@/components/MenuOptions.vue';
import ModalDependents from '@/components/ModalDependents.vue';
import CleanListTwoParams from '@/components/CleanListTwoParams.vue';
import FooterLogo from '@/components/FooterLogo.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'

@Component({    
    components: { FooterLogo, DefaultButton, HeaderModality, ListDefault, ButtonCard, SectionArea, BottomNavigation, MenuOptions, ModalDependents, CleanListTwoParams, HeaderCircleButton}
})

export default class MenuEducation extends Vue {
    userData: any    = JSON.parse(localStorage.getItem('USER_DATA') || '{}');
    hasDependent: boolean = true;
    menuOptions: boolean = false;
    showDependents: boolean = false;
    profileImage: string = this.userData.IMAGE;
    dependentImage: string = localStorage.getItem('CURRENT_DEPENDENT_IMAGE') || '';
    fullName: string = this.userData.FIRST_NAME + ' ' + this.userData.LAST_NAME;
    dependents: any    = [];
    getNpf: any    = localStorage.getItem('ID_EXT') || "";
    npf: number = parseInt(this.getNpf);
    checkedUserNpf =localStorage.getItem('DEPENDENT_EXT_ID');
    incomingNpf: any;
    reportsCount: number = 0;
    reportsKey: number = 1;
    classesFiltered: any;
    hasModality: boolean = false;
    modalitiesList: any = JSON.parse(localStorage.getItem('MODALITIES_LIST') || '[]');
    backButton: any = {iconClass: 'fal', icon: 'chevron-left', callback:this.goBack};

    mounted() {
        const self = this;
        Util.setBackButtonBehavior(this.goBack);
        // localStorage.removeItem('GO_TO_ENROLL');
        localStorage.setItem('CURRENT_CLASS', '');

        this.dependents = this.userData.dependents;

        if (localStorage.getItem('GET_MODALITY_FROM_LS') !== 'true' ) {
            this.getCourses();
        }

        if (this.dependents == '') {
            this.hasDependent = false;
            localStorage.setItem('DEPENDENT_EXT_ID', this.userData.ORGANIZATION_DATA.EXTERNAL_ID);
        }

        Util.getUserReports((response: any) => {
            let currentReports : any = [];
    
            if (response.length > 0) {
                currentReports = response;

                currentReports.forEach((report:any) => {
                    if(report.status == 'P') self.reportsCount++;    
                });

                let currentReportsStorage: any = JSON.parse(localStorage.getItem('CURRENT_REPORTS') || '[]');
                let oldReportsStorage: any = JSON.parse(localStorage.getItem('OLD_REPORTS') || '[]');
        
                if(oldReportsStorage && currentReports) {
                    if (self.existsNewValues(oldReportsStorage,currentReports)) {
                        localStorage.setItem('USER_HAS_REPORTS', JSON.stringify(true));
                    } else { localStorage.setItem('USER_HAS_REPORTS', JSON.stringify(false));  localStorage.setItem('OLD_REPORTS',JSON.stringify(currentReports)); }
                }
                
                localStorage.setItem('CURRENT_REPORTS', JSON.stringify(currentReports));
                self.reportsKey ++;
    
            } else {
                currentReports = [];
                localStorage.setItem('USER_HAS_REPORTS', JSON.stringify(false));
                localStorage.setItem('CURRENT_REPORTS', JSON.stringify(currentReports));
                self.reportsKey ++;
            }
        });
    }

    existsNewValues(a: any,b: any) {
        return !!b.find((objeto: any) => !a.find((obj2: any) => obj2.id == objeto.id)) 
    }

    updated(){
        const userNpf = localStorage.getItem('DEPENDENT_EXT_ID');
        const profileImage = this.userData.IMAGE;
        const dependentName = localStorage.getItem('CURRENT_DEPENDENT_NAME');
        const dependentImage = localStorage.getItem('CURRENT_DEPENDENT_IMAGE');
        const checkedUserNpf = localStorage.getItem('DEPENDENT_EXT_ID');
        if(typeof this.incomingNpf === 'undefined'){
            this.incomingNpf = userNpf;
        }

        if (userNpf != this.incomingNpf){
            this.incomingNpf = userNpf;
            this.getCourses();
            this.profileImage = profileImage;
            this.dependentImage = dependentImage || '';
            this.checkedUserNpf = checkedUserNpf;
            let incomingId = JSON.parse(localStorage.getItem('DEPENDENT_EXT_ID') || '');
            let userId = this.userData.ORGANIZATION_DATA.EXTERNAL_ID;
        }
    }

    

    openEnrolls() {
        localStorage.setItem('GO_TO_ENROLL', 'true');
        this.$router.push('ClassEnrollList');
    }
    
    openModality() {
        localStorage.setItem('GO_TO_ENROLL', 'false');
        this.$router.push('ModalityList');
    }

    openReports() {
        this.$router.push('Reports');
    }

    openCalendar() {
        this.$router.push('ClassCalendar');
    }

    goBack(){
        this.$router.go(-2);
    }

    openMenuOptions(){
        this.menuOptions = true;
    }

    closeMenuOptions(){
        const OPENLIST = 'popInBottom';
        const CLOSELIST  = 'popOutBottom';
        const OPEN= 'popIn';
        const CLOSE  = 'popOut';

        $('#option0').removeClass(OPENLIST);
        $('#option0').addClass(CLOSELIST);
        $('#option1').removeClass(OPENLIST);
        $('#option1').addClass(CLOSELIST);
        $('#option2').removeClass(OPENLIST);
        $('#option2').addClass(CLOSELIST);
        $('#option3').removeClass(OPENLIST);
        $('#option3').addClass(CLOSELIST);
        $('#option4').removeClass(OPENLIST);
        $('#option4').addClass(CLOSELIST);
        $('#option5').removeClass(OPENLIST);
        $('#option5').addClass(CLOSELIST);
        $('#btn').removeClass(OPEN);
        $('#btn').addClass(CLOSE);

        setTimeout( () => {
            this.menuOptions = false;
        }, 600);
    }

    
    openModalDependents(){
        const OPENMODAL = 'slide-modal-open';
        const CLOSEMODAL  = 'slide-modal-close';

        $('#modalDependents').removeClass(CLOSEMODAL);
        $('#modalDependents').addClass(OPENMODAL);

        setTimeout( () => {
            this.showDependents = true;
        }, 50);
    }

    closeModalDependents(){
        const OPENMODAL = 'slide-modal-open';
        const CLOSEMODAL  = 'slide-modal-close';

        $('#modalDependents').removeClass(OPENMODAL);
        $('#modalDependents').addClass(CLOSEMODAL);

        setTimeout( () => {
            this.showDependents = false;
        }, 300);
    }

    dependentClick(idExt:any, nome: string, img: any) {
        localStorage.setItem('DEPENDENT_EXT_ID', idExt);
        localStorage.setItem('CURRENT_DEPENDENT_NAME', nome);
        localStorage.setItem('CURRENT_DEPENDENT_IMAGE', img);
        localStorage.setItem('GET_MODALITY_FROM_LS', '');
        this.closeModalDependents();
    }

    getCourses() {
        try {
            this.classesFiltered = [];
            const req: any = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || ""),
                'EXTERNAL_ID' : localStorage.getItem('DEPENDENT_EXT_ID'),
                'USER_ID_CONSULTA' : localStorage.getItem('USER_ID')
            };
            req.route = 'getCourses';
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                if (response.response.length > 0) {
                    this.hasModality = true;
                    localStorage.setItem('MODALITIES_LIST', JSON.stringify(response.response));
                    this.modalitiesList = response.response;
                    localStorage.setItem('GET_MODALITY_FROM_LS', 'true');
                    
                } else {
                    this.hasModality = false;
                    localStorage.setItem('GET_MODALITY_FROM_LS', '');
                    localStorage.setItem('MODALITIES_LIST', JSON.stringify([]));
                    this.modalitiesList = [];
                }
            };
            req.callbackError = (error: any) => {
                this.hasModality = false;
                localStorage.setItem('GET_MODALITY_FROM_LS', '')
                console.log(error);
            };
            req.loadingMessage = 'Buscando suas atividades...';
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e)
        }
    }

    
}