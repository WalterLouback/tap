import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';
import SectionTitle from '@/components/SectionTitle.vue';
import EventDetailImage from '@/components/EventDetailImage.vue';
import TextLink from '@/components/TextLink.vue';
import InputText from '@/components/InputText.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import SelectField from '@/components/SelectField.vue';
import OptionField from '@/components/OptionField.vue';
import InvisibleHeader from '@/components/InvisibleHeader.vue'
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import Orders, { StoresConfig, RecentOrdersConfig } from "../modules/orders-module/Orders";
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue'
import HeaderBackground from '@/modules/shared-components/HeaderBackground.vue'

declare var M: any;
declare var $: any;
@Component({
    components: { SectionTitle, EventDetailImage, TextLink, InputText, DefaultButton, SelectField, OptionField, InvisibleHeader, HeaderCircleButton, HeaderBackground }
})

export default class EventDetails extends Vue {
    inputValues: any;
    structures: any = [];
    events: any = [];
    eventDetail: any = JSON.parse(localStorage.getItem('EVENT') || '{}');
    ticketsEvent: any = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
    nome: any = [];
    cheapestTicket: any = {};
    isFavorited: boolean = this.isFavorite();
    componentKey: number = 0;
    backButton: any = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}
    otherButton: any = [{iconClass: 'fal', icon: 'share-alt', colorClass: 'default', callback: this.openWebShare}];
    parentEvent: any = (localStorage.getItem('PARENT_EVENT')) ? JSON.parse(localStorage.getItem('PARENT_EVENT')||'[]') : [];
    children: any = [];
    schedule: any = [];
    breadCrumbs: any = [];
    sharedEventParent: boolean = true;
    loadedMenuData: boolean = false; 

    formatDate(stringDate: string) {
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('DD [de] MMM [às] HH:mm');
    }

    getTicketByEvent() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': this.eventDetail.id
            };
            req.route = 'getTicketByEvent';
            req.callbackSuccess = function(response: any) { 
                self.ticketsEvent = response.eventTickets;
                self.findCheapestTicket();
                const allTicketsEvents = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]');
                allTicketsEvents[self.eventDetail.id] = response.eventTickets;
                localStorage.setItem('TICKETS_EVENTS', JSON.stringify(allTicketsEvents));
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                console.log(e);
            }
    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getEvent();
        this.findCheapestTicket();
        this.verifySharedEventParent();
        this.getEventData(this.eventDetail.id);
        this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
    }

    toggleFavorite() {
        const isFavorited = this.isFavorited;
        if (!isFavorited) {
            this.isFavorited = true;
            this.favoriteEvent();
        } else {
            this.isFavorited = false; 
            this.unfavoriteEvent();
        }
    }

    favoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'createFavoriteEvent';
        req.callbackSuccess = function (response: any) { 
            self.updateFavoriteList();
            self.isFavorited = true;
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();
    }

    unfavoriteEvent() {
        const self = this
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'EVENT_ID': this.eventDetail.id
        };
        req.route = 'deleteFavoriteEvent';
        req.callbackSuccess = function (response: any) { 
            self.updateFavoriteList();
            self.isFavorited = false;
        };
        req.callbackError = function(error: any) {
            
            console.log(error);
        };
        req.showLoader = false;
        req.send();
    }

    updateFavoriteList(){
        try{
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                "NRORG": localStorage.getItem("NRORG"),
                'TOKEN': localStorage.getItem("TOKEN")
            }
            req.route = 'favorite';
            req.callbackSuccess = (response:any) => {
                localStorage.setItem('FAVORITE_EVENTS_AND_STORES',JSON.stringify(response.calendar));
            }
            req.callbackError = () => {
                console.log("Error");
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e){
            console.log(e)
        }
    }

    loadEvents() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "TOKEN": localStorage.getItem('TOKEN'),
                "DATE": localStorage.getItem('SELECTED_DATE'),
                "STRUCTURES": localStorage.getItem('SELECTED_STRUCTURES') || '[]',
                "CATEGORIES": localStorage.getItem('CATEGORIES') || '[]',
                "TAGS": localStorage.getItem('SELECTED_TAGS') || '[]',
                "USER_ID": localStorage.getItem('USER_ID')
            };
            req.route = 'getEventsByOrg';
            req.callbackSuccess = function(response: any) { 
                let events = response.events;
                self.events = events;
                localStorage.setItem('EVENTS', JSON.stringify(response.events));
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    goToSecondProductMenu(){
        localStorage.setItem('SELECTED_EVENT', JSON.stringify(this.eventDetail.id));
        localStorage.setItem('EVENT_NAME_QRCODE', JSON.stringify(this.eventDetail.name));
        this.$router.push({ name: 'GetProduct' });
    }

    goToProductMenu(){
        this.$router.push({ name: 'ProductMenu' });
    }

    goToBuyEventTicket(){
        this.$router.push('/BuyEventTicket');
    }

    getEvent() {
       this.events = JSON.parse(localStorage.getItem('EVENTS') || '{}');
       this.eventDetail = JSON.parse(localStorage.getItem('EVENT') || '{}');
    }

    findCheapestTicket() {
        if (this.ticketsEvent.length > 0) {
            this.cheapestTicket = this.ticketsEvent.reduce((a: any, b: any) => { 
                if (a && b) return a.price < b.price ? a : b;
                else return a;
            });
        } else return {};
    }

    goBack() {
        if(this.parentEvent.length && localStorage.getItem('EVENT_SCHEDULE_CACHE')){
            var scheduleList = JSON.parse(localStorage.getItem('EVENT_SCHEDULE_CACHE')||'[]');
            var schedule = scheduleList.pop();
            localStorage.setItem('EVENT_SCHEDULE', JSON.stringify(schedule));
            localStorage.setItem('EVENT_SCHEDULE_CACHE', JSON.stringify(scheduleList)); 
        } 
        this.$router.go(-1);
    }

    isFavorite(){
        if (!!localStorage.getItem('FAVORITE_EVENTS_AND_STORES')){
            var favorites = JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES')||'{}');
            var  store: any = JSON.parse(localStorage.getItem('STORE') || "{}");
            // return !!JSON.parse(localStorage.getItem('FAVORITE_EVENTS_AND_STORES')||'{}').find((e:any)=>e.id == this.store.id);
            return !!favorites.find((ev:any)=>ev.id == store.id);
        }
        else return false;
    }

    goToEventDetails(){
        this.$router.push('/EventDetails');
    }

    goToStore() {
        localStorage.setItem('IS_AN_EVENT','t');
        Util.removeBackButtonBehavior();
        Orders.goToStores(new StoresConfig(this.goToEventDetails, this.eventDetail.id));
    }

    goToEventStores(){
        localStorage.setItem('IS_AN_EVENT','t');
        this.parentEvent.push(this.eventDetail);
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent))
        this.$router.push('/EventStores')
    }

    goToEventSchedule(){
        localStorage.setItem('IS_AN_EVENT','t');
        this.parentEvent.push(this.eventDetail);
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
        this.$router.push('/EventSchedule')
    }

    goToSharedStore(id:string){
        localStorage.setItem('IS_AN_EVENT', '');
        Util.removeBackButtonBehavior();
        localStorage.setItem('SHARE_ID', '')
        localStorage.setItem('SHARE_TYPE', '')
        Util.removeBackButtonBehavior();
        Orders.goToStores(new StoresConfig(this.goToEventDetails, id));
    }

    goToSharedEvent(id: string, last: boolean){
        localStorage.setItem('IS_AN_EVENT','t');
        this.parentEvent.push(this.eventDetail);
        localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent));
        this.breadCrumbs = this.parentEvent.map((obj:any)=>{return {name: obj.name, id:obj.id}});
        if (last){
            localStorage.setItem('SHARE_ID', '')
            localStorage.setItem('SHARE_TYPE', '')
        }
        this.getEventData(id);
    }

    verifySharedEventParent(){
        this.sharedEventParent = !!localStorage.getItem('SHARE_PARENT_ID');
    }

    openWebShare(){
        const w: any = window;
        var idType: string = '&et=e';
        var parentEvent: string = this.parentEvent.map((obj:any)=>{return obj.id}).join(',');
        var id: string = (parentEvent.length) ? 'p=' + parentEvent +'&id=' + this.eventDetail.id:'id=' + this.eventDetail.id;
        var suggestionType: string = 'deste evento';         
        // w.plugins.socialsharing.share(`Acho que você vai gostar `+ suggestionType + `, confira no BipFun https://bipfun.teknisa.com/open_event?`+ id + idType);
        console.log(`Acho que você vai gostar `+ suggestionType + `, confira no BipFun https://bipfun.teknisa.com/open_event?`+ id + idType);
    }

    getStoreData() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "STORE_ID": this.eventDetail.id
            };
            req.route = 'getStoreData';
            req.callbackSuccess = (response: any) => {
                // localStorage.setItem('PARENT_EVENT', JSON.stringify(response.store))
                // localStorage.setItem('EVENT_STORES', JSON.stringify(response.store.children));
                //-----TEMP para teste
                // var temp = (localStorage.getItem('STORES')) ? JSON.parse(localStorage.getItem('STORES')||'[]') : [];
                // this.children = temp.filter((obj:any)=>obj.type=='S');
                // this.schedule = temp.filter((obj:any)=>obj.type=='E');

                //-----Correto
                this.children = response.store.children.filter((obj:any)=>obj.type=='S');
                this.schedule = response.store.children.filter((obj:any)=>obj.type=='E');
                // this.parentEvent = response.store.schedule;
                localStorage.setItem('EVENT_STORES', JSON.stringify(this.children));
                localStorage.setItem('EVENT_SCHEDULE', JSON.stringify(this.schedule));
                if(localStorage.getItem('SHARE_TYPE')) this.sendToSharedEvent()
                else this.loadedMenuData = true;
                this.$forceUpdate();
                //
                
                //setTimeout(self.addEventListeners, 1);
            };
            req.callbackError = function(error: any) {
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    

    getEventData(id: any) {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "EVENT_ID": id
            };
            req.route = 'getEventData';
            req.callbackSuccess = (response: any) => {
                this.eventDetail = response.event;
                this.verifySharedEventParent()
                this.ticketsEvent = JSON.parse(localStorage.getItem('TICKETS_EVENTS') || '[]')[this.eventDetail.id] || [];
                localStorage.setItem('PARENT_EVENT', JSON.stringify(this.parentEvent))
                localStorage.setItem('EVENT', JSON.stringify(this.eventDetail));
                this.getTicketByEvent();
                this.getStoreData();
            };
            req.callbackError = function(error: any) {
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
             console.log(e); 
        }
    }    

    sendToSharedEvent(){
        
        if (localStorage.getItem('SHARE_PARENT_ID')) {
            var parentEventList:any = localStorage.getItem('SHARE_PARENT_ID')!.split(',');
            parentEventList.shift();
            localStorage.setItem('SHARE_PARENT_ID', parentEventList.join(','));
        }
        else{
            var parentEventList:any = [];
        } 

        if (parentEventList.length){
            var parentEvent = parentEventList[0];
            this.goToSharedEvent(parentEvent||'', false);
        }
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='e') {
            this.goToSharedEvent(localStorage.getItem('SHARE_ID')!, true);
        }
        else if(localStorage.getItem('SHARE_TYPE') && localStorage.getItem('SHARE_TYPE')=='s') {
            this.goToSharedStore(localStorage.getItem('SHARE_ID')!);
        }
        
    }
    
}