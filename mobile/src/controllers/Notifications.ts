
import { Component, Prop, Vue } from 'vue-property-decorator';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderRelease from '@/components/HeaderRelease.vue';
import ListDefault from '@/components/ListDefault.vue';
import ThreeOptionsListNotifications from '@/components/ThreeOptionsListNotifications.vue';
import TextLink from '@/components/TextLink.vue';
import Util from '@/ts/Util';
import moment from 'moment';
import EmptyState from '@/components/EmptyState.vue';
import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';

@Component({        
    components: { HeaderRelease, DefaultButton, ListDefault, ThreeOptionsListNotifications, TextLink, EmptyState, HeaderCircleButton}
})

export default class Notifications extends Vue {
    currentNotifications: any   = JSON.parse(localStorage.getItem('CURRENT_NOTIFICATIONS') || '[]');
    backButton: any             = {iconClass: 'fas', icon: 'chevron-left', callback:this.goBack}

    mounted(){  
        Util.setBackButtonBehavior(this.goBack);
        localStorage.setItem('USER_HAS_NOTIFICATIONS', JSON.stringify(false));
        localStorage.setItem('OLD_NOTIFICATIONS',JSON.stringify(this.currentNotifications));
    }
    
    goToNotification(notificationId: any ){
        //let clickedNotification : any =  this.currentNotifications.filter(((notifcation: any) => notifcation.id == notificationId));
        let clickedNotification = this.currentNotifications.find((surv:any) => surv.id == notificationId);
        if (clickedNotification.origin == 'survey') {
            localStorage.setItem('TITLE_SURVEY', clickedNotification.description);
            localStorage.setItem('CURRENT_SURVEY', notificationId);
            this.$router.push('Survey');
        } else if (clickedNotification.origin === 'gen_api_external') {
            localStorage.setItem('REPORT_DETAIL', JSON.stringify(clickedNotification));
            localStorage.setItem('CURRENT_REPORT', notificationId);
            if (clickedNotification.category === 1) this.$router.push('ReportsDetailsEvent');
            else this.$router.push('ReportsDetails');
        }

        // PREPARED FOR TYPE & ORDER 
        // if (clickedNotification[0].type == 'survey') {
        //     localStorage.setItem('CURRENT_SURVEY', clickedNotification[0].ref_id);
        //     this.$router.push('Survey');
        // }
        // } else if (clickedNotification[0].type == 'order') {
        //     localStorage.setItem('ORDER_ID', clickedNotification[0].ref_id+'');
        //     this.$router.push('ExtratoPedido');
        // }
    }

    formatDateTime(date: any) {
        const stringDate = date.date ? date.date : date;
        const momentDate: any = moment(stringDate);
        return momentDate.lang('pt-br').format('DD/MM - HH:mm');
    }

    goBack() {
        this.$router.go(-1);
    }
}
