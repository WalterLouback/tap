    import { Component, Prop, Vue } from 'vue-property-decorator';
    import moment from 'moment';
    import SectionArea from '@/components/SectionArea.vue';
    import TabsEvent from '@/components/TabsEvent.vue';
    import InputText from '@/components/InputText.vue';
    import DefaultButton from '@/components/DefaultButton.vue';
    import BottomNavigation from '@/components/BottomNavigation.vue';
    import SelectField from '@/components/SelectField.vue';
    import OptionField from '@/components/OptionField.vue';
    import Card from '@/components/Card.vue';
    import CardEvent from '@/components/CardEvent.vue';
    import HeaderEvents from '@/components/HeaderEvents.vue';
    import TextLink from '@/components/TextLink.vue';
    import ModalCodigo from '@/components/ModalCodigo.vue';
    import Request from '@/ts/Request';
    import Util from '@/ts/Util';
    import EmptyState from '@/components/EmptyState.vue';
    import HeaderCircleButton from '@/modules/shared-components/HeaderCircleButton.vue';
    import BannerStore from '@/modules/shared-components/BannerStore.vue'

    declare var M: any;
    declare var $: any;
    @Component({
        components: {SectionArea, TabsEvent, InputText, DefaultButton, SelectField, OptionField, Card, CardEvent, HeaderEvents, BottomNavigation, TextLink, EmptyState, ModalCodigo, HeaderCircleButton, BannerStore}
    })

    export default class Event extends Vue {

        inputValues: any
        events: any        = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        eventsToShow:  any = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        eventsToShow2: any = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        today: any      = moment()
        tomorrow: any   = moment().add(1,'d')
        monday: any     = moment().isoWeekday(0)
        friday: any     = moment().isoWeekday(5)
        weekend: any    = [
            { sex: moment().isoWeekday(5) },
            { sab: moment().isoWeekday(6) },
            { dom: moment().isoWeekday(7) }
        ]
        nextMonday: any = moment().isoWeekday(8)
        nextSunday: any = moment().isoWeekday(14)
        lastDayOfMonth: any  = moment().add('months', 1).date(0);
        lastRoute: boolean = localStorage.getItem('LAST_ROUTE') == 'Ingressos' || localStorage.getItem('SHOW_FILTERED_EVENTS') == 'true';
        filteredEventsByTicket: any =  localStorage.getItem('FILTERED_TICKET_EVENTS') || [];
        notFound: boolean = false;
        filteredEvents: any = JSON.parse(localStorage.getItem('EVENTS') || '[]');
        cameFromTickets: any = localStorage.getItem('CAME_FROM_TICKETS') == 'true';
        syncRequest: boolean = false;

        calendar: any   = {
            dates: [
                { id: '0', NAME: 'Todos', IDTAB: 'ALL'},
                { id: '1', NAME: 'Hoje', IDTAB: 'TODAY', INITIAL_DATE: this.today, FINAL_DATE: moment() },
                { id: '2', NAME: 'Amanhã', IDTAB: 'TOMORROW', INITIAL_DATE: this.tomorrow, FINAL_DATE: this.tomorrow },
                { id: '3', NAME: 'Esta semana', IDTAB: 'THIS_WEEK', INITIAL_DATE: this.monday, FINAL_DATE: this.friday },
                { id: '4', NAME: 'Próxima semana', IDTAB: 'NEXT_WEEK', INITIAL_DATE: this.nextMonday, FINAL_DATE: this.nextSunday },
                { id: '5', NAME: 'Este mês', IDTAB: 'THIS_MONTH', INITIAL_DATE: this.today, FINAL_DATE: this.lastDayOfMonth },
            ]
        }
        backButton: any = {iconClass: 'fas', icon:'chevron-left', callback:this.goBack};

        removeSpecialCharacters(s: string) {
            s = s ? s.toLowerCase() : '';
            s = s.toLowerCase();
            s = s.replace(new RegExp(/\s/g),'_');
            s = s.replace(new RegExp(/[àáâãäå]/g),'a');
            s = s.replace(new RegExp(/æ/g),'ae');
            s = s.replace(new RegExp(/ç/g),'c');
            s = s.replace(new RegExp(/[èéêë]/g),'e');
            s = s.replace(new RegExp(/[ìíîï]/g),'i');
            s = s.replace(new RegExp(/ñ/g),'n');                
            s = s.replace(new RegExp(/[òóôõö]/g),'o');
            s = s.replace(new RegExp(/œ/g),'oe');
            s = s.replace(new RegExp(/[ùúûü]/g),'u');
            s = s.replace(new RegExp(/[ýÿ]/g),'y');
            s = s.replace(new RegExp(/\W/g),'');
            return s;
        }

        beforeMount() {
            if(localStorage.getItem('LAST_ROUTE') == 'Ingressos') {
                localStorage.setItem('CAME_FROM_TICKETS', 'true');
                this.cameFromTickets = true;
            }
        }

        destroyed() {
            if(this.cameFromTickets === 'true') {
                localStorage.removeItem('CAME_FROM_TICKETS');
            }
        }

        mounted() {
            Util.setBackButtonBehavior(this.goBack);
            this.getEvents();
            var now = moment();

            moment.updateLocale('pt', {
                monthsShort : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                months : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                weekdays : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
            });

            var self = this;

            $('#search_event').keydown( function(e:any) {
                setTimeout(function() {
                    self.eventsToShow2 = self.eventsToShow.filter((event: any) => {
                        if(!self.removeSpecialCharacters(event.name).includes(self.removeSpecialCharacters($('#search_event').val()))) self.notFound = true;
                        else self.notFound = false;
                        return (
                            !$('#search_event').val() || 
                            self.removeSpecialCharacters(event.name).includes(self.removeSpecialCharacters($('#search_event').val()))
                        )
                    })
                }, 100);
            })

            if (localStorage.getItem('SELECTED_DATE') || localStorage.getItem('EVENT_CATEGORIES') || localStorage.getItem('SELECTED_STRUCTURES') || localStorage.getItem('SELECTED_EVENT_TAGS')) {
                $('#tabs').hide();
                $('#limpar-filtros').show();
            }
            else {
                $('#tabs').show();
                $('#limpar-filtros').hide();
            }


            localStorage.removeItem("CAME_FROM_TICKETS");
            localStorage.removeItem("CAME_FROM_HOME");
        }

        dayChange(day: number) {
            const lastRoute = localStorage.getItem('LAST_ROUTE');

            if(lastRoute == "Ingressos" || localStorage.getItem('SHOW_FILTERED_EVENTS') == 'true') {
                if (day == 0) {
                    this.eventsToShow = this.filteredEvents;
                    this.eventsToShow2 = this.filteredEvents;
                } else {
                    this.eventsToShow = this.filteredEvents.filter((event: any) => { 
                        return (
                            (
                                moment(event.initialDate.date) <= this.calendar.dates[day].INITIAL_DATE.endOf('day')._d ||
                                moment(event.initialDate.date) <= this.calendar.dates[day].FINAL_DATE.endOf('day')._d
                            ) &&
                            (
                                moment(event.finalDate.date) >= this.calendar.dates[day].INITIAL_DATE.startOf('day')._d ||
                                moment(event.finalDate.date) >= this.calendar.dates[day].FINAL_DATE.startOf('day')._d
                            )
                        )
                    });
                    this.eventsToShow2 = this.eventsToShow;
                }
                if(this.eventsToShow2.length == 0) this.notFound = true;
                else this.notFound = false;
            
            } else {
                if (day == 0) {
                    this.eventsToShow = this.events;
                    this.eventsToShow2 = this.events;
                } else {
                    this.eventsToShow = this.events.filter((event: any) => { 
                        return (
                            (
                                moment(event.initialDate.date) <= this.calendar.dates[day].INITIAL_DATE.endOf('day')._d ||
                                moment(event.initialDate.date) <= this.calendar.dates[day].FINAL_DATE.endOf('day')._d
                            ) &&
                            (
                                moment(event.finalDate.date) >= this.calendar.dates[day].INITIAL_DATE.startOf('day')._d ||
                                moment(event.finalDate.date) >= this.calendar.dates[day].FINAL_DATE.startOf('day')._d
                            )
                        )
                    });
                    this.eventsToShow2 = this.eventsToShow;
                }
                if(this.eventsToShow2.length == 0) this.notFound = true;
                else this.notFound = false;
            }
        }

        goTo() {
            this.$router.push('FilterEvents');
        }

        goBack(): void {
            // if (localStorage.getItem('CAME_FROM_HOME') == 'true') {
            //     localStorage.setItem('CAME_FROM_HOME', 'false');
            //     this.$router.push('HomeApp');
            // } else {
            //     localStorage.removeItem('CAME_FROM_TICKETS');
            //     this.$router.push('EventsMenu');
            // }
            this.$router.go(-1);
        }
        
        
        formatData(ID: any){
            const evento = this.events.find((event: any) => event.id == ID)
            const momentDate: any = moment(evento.initialDate.date);
            return momentDate.lang('pt-br').format('DD [de] MMMM, HH[h]mm');
        }
        
        verifyFavorite(ID: any){
            if(this.events.find((event: any) => event.id == ID).isFavorite == true) return 
            if(this.events.find((event: any) => event.id == ID).isFavorite == false) return 'favorite_border'
        }

        toggleFavorite(eventId: number) {
            localStorage.setItem('EVENT', JSON.stringify(this.events.find((event: any) => event.id == eventId)));

            var isFavorite = $('#favorite' + eventId + ' > i').html() == 'favorite';
            
            if (isFavorite) {
                $('#favorite' + eventId + ' > i').html('favorite_border'); 
                this.unfavoriteEvent(eventId);
            }
            else {   
                $('#favorite' + eventId + ' > i').html('favorite'); 
                this.favoriteEvent(eventId);
            }
        }

        favoriteEvent(eventId: number) {
            const self = this
            const req = new Request();
            const event = JSON.parse(localStorage.getItem('EVENT') || '');
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': eventId
            };
            req.route = 'createFavoriteEvent';
            req.callbackSuccess = function (response: any) { 
                event.favoriteId = eventId;

                self.showModalSync();
                self.getEvents();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.showLoader = false;
            req.send();
        }

        unfavoriteEvent(eventId: number) {
            const self = this
            const req = new Request();
            const event = JSON.parse(localStorage.getItem('EVENT') || '');
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'USER_ID': localStorage.getItem('USER_ID'),
                'EVENT_ID': eventId
            };
            req.route = 'deleteFavoriteEvent';
            req.callbackSuccess = function (response: any) { 
                event.favoriteId = null;
                self.getEvents();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.showLoader = false;
            req.send();
        }

        setValue(data: any) {
            this.inputValues[data.name] = data.value;
        }
        click(id: number){
            localStorage.setItem('EVENT', JSON.stringify(this.events.find((event: any) => event.id == id)));
            localStorage.setItem('EVENTS', JSON.stringify(this.events));
            localStorage.setItem('SELECTED_EVENT', JSON.stringify(id));
            this.$router.push({ name: "EventDetails" });
        }
        getEvents() {
            try {
                const self = this;
                const req = new Request();
                req.body = {
                    "NRORG": localStorage.getItem('NRORG'),
                    "TOKEN": localStorage.getItem('TOKEN'),
                    "DATE": localStorage.getItem('SELECTED_DATE'),
                    "STRUCTURES": localStorage.getItem('SELECTED_STRUCTURES') || '[]',
                    "CATEGORIES": localStorage.getItem('EVENT_CATEGORIES') || '[]',
                    "TAGS": localStorage.getItem('SELECTED_EVENT_TAGS') || '[]',
                    "USER_ID": (localStorage.getItem('USER_ID')) ? localStorage.getItem('USER_ID') :1,
                    "STRUCTURE_ID": localStorage.getItem('STRUCTURE_ID'),
                    "STATUS": "A"
                };
                req.route = 'getEventsByOrg';
                req.callbackSuccess = function(response: any) {
                    let events = response.events;
                    
                    self.events = events;
                    self.eventsToShow2 = self.events;
                    self.filteredEvents = self.events;
                
                    // let dia = moment(events[0].initialDate.date).lang('pt-br').format("DD [de] MMMM, hh[h]mm");
                    localStorage.setItem('EVENTS', JSON.stringify(response.events));
                    console.log(!!self.cameFromTickets)
                    if (!!self.cameFromTickets) self.filteredEvents = self.events.filter(((event:any) => !!event.ticketId));
                    localStorage.setItem('FILTERED_TICKET_EVENTS', JSON.stringify(self.filteredEvents));
                    const lastRoute = localStorage.getItem('LAST_ROUTE');

                    if(!self.eventsToShow2[0]) self.notFound = true;

                    if(lastRoute == "Ingressos" || !!self.cameFromTickets) {
                        if ( self.filteredEvents == 0) self.notFound = true;
                        else self.notFound = false;
                    } else {
                        if (!self.eventsToShow2[0]) self.notFound = true;
                        else  self.notFound = false;
                    }
                };
                req.callbackError = function(error: any) {
                    console.log(error);
                    self.notFound = true;
                }
                req.blockTouchEvents = false;
                req.send();
                } catch (e) {
                    console.log(e);
                }
        }

        showModalSync() {
            this.syncRequest = true;
        }
    
        closeModalSync() {
            this.syncRequest = false;
        }
    
        callSyncCalendar() {
            let eventDetail= JSON.parse(localStorage.getItem('EVENT') || '{}')
            
            let dtInicio = moment(eventDetail.initialDate.date).format(moment.HTML5_FMT.DATE).split('-');
            let hrInicio = moment(eventDetail.initialDate.date).format(moment.HTML5_FMT.TIME).split(':');

            let dtFinal = moment(eventDetail.finalDate.date).format(moment.HTML5_FMT.DATE).split('-');
            let hrFinal = moment(eventDetail.finalDate.date).format(moment.HTML5_FMT.TIME).split(':');

            let initialDate = new Date(parseInt(dtInicio[0]),
            parseInt(dtInicio[1]) - 1, parseInt(dtInicio[2]), parseInt(hrInicio[0]), 
            parseInt(hrInicio[1]), 0, 0);

            let finalDate = new Date(parseInt(dtFinal[0]),
                parseInt(dtFinal[1]) - 1, parseInt(dtFinal[2]), parseInt(hrFinal[0]), 
                parseInt(hrFinal[1]), 0, 0);

            let saveCalendar = {
                initialDate: initialDate,
                finalDate: finalDate,
                nameEvt: eventDetail.name,
                location: eventDetail.location,
                details: eventDetail.about
            }

            Util.syncCalendarSystem(saveCalendar);
            
            this.closeModalSync();
        }


        limparFiltros() {
            localStorage.setItem('SELECTED_DATE', '');
            localStorage.setItem('SELECTED_STRUCTURES', '');
            localStorage.setItem('EVENT_CATEGORIES', '');
            localStorage.setItem('SELECTED_STRUCTURES_FORMATTED', '');
            localStorage.setItem('EVENT_CATEGORIES_FORMATTED', '');
            localStorage.setItem('SELECTED_EVENT_TAGS', '');
            $('#limpar-filtros').fadeOut();
            $('#tabs').fadeIn('fast');
            this.getEvents();
            $('.tabs').tabs();
        }
    }