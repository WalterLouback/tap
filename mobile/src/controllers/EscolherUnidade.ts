import { Component, Prop, Vue } from 'vue-property-decorator'
import SectionArea from '../components/SectionArea.vue'
import TextLink from '../components/TextLink.vue'
import InputText from '../components/InputText.vue'
import DefaultButtonOrd from '../components/DefaultButtonOrd.vue'
import SelectField from '../components/SelectField.vue'
import OptionField from '../components/OptionField.vue'
import ModalCodigo from '../components/ModalCodigo.vue'
import HeaderFluxo from '../components/HeaderFluxo.vue'
import SelectFieldController from '../ts/SelectFieldController'
import Request from '../ts/Request'
import moment from 'moment';
import Util from '../ts/Util';

declare var M: any;
declare var $: any; 

@Component({
    components: { SectionArea, TextLink, InputText, DefaultButtonOrd, SelectField, OptionField, ModalCodigo, HeaderFluxo }
})

export default class EscolherUnidade extends Vue {
    //Declarations
    showModal: boolean = false
    //Structures
    structures: any = JSON.parse(localStorage.getItem('STRUCTURES') || '[]');

    //Define the values of the object to the request
    data() {
        return {
        }
    }

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        SelectFieldController.refresh();
        let day = moment().date();
        localStorage.setItem('LAST_DAY_ESCOLHER_UNIDADE', JSON.stringify(day));
        this.getStructures();
        localStorage.setItem('deliverToBalcony', '');
        localStorage.setItem('structureId', '');
        // try {
        //     history.replaceState({}, '', '/#/MenuPedidos');
        // } catch (e) { }
    }

    goBack() {
        if(localStorage.getItem('ENTERED_CHOOSE_STRUCTURE_FROM')) this.$router.push(localStorage.getItem('ENTERED_CHOOSE_STRUCTURE_FROM')||'EscolherUnidade');
        else this.$router.go(-1);
    }

    lerQrMesa() {
        localStorage.setItem('QR_FROM', 'EscolherUnidade');
        this.$router.push('LerQrMesa' );
    }

    buttonAvancar( id:any , name:any ) {
        localStorage.setItem('STRUCTURE_ID', id);
        localStorage.setItem('STRUCTURE_NAME', name);
        localStorage.setItem('lastStructureOfLastOrder', '');
        localStorage.setItem('stores', '[]');
        if(localStorage.getItem('ENTERED_CHOOSE_STRUCTURE_FROM')) this.$router.push(localStorage.getItem('ENTERED_CHOOSE_STRUCTURE_FROM')||'EscolherUnidade');
        else this.$router.go(-1);
    }

    mostraModal() {
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
    }

    getStructures() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                "NRORG": localStorage.getItem('NRORG'),
                "STRUCTURE_LEVEL": 0
            };
            req.route = 'getStructures';
            req.callbackSuccess = function(response: any) {
                let structures = response.structures;
                self.structures = structures;
                localStorage.setItem('STRUCTURES', JSON.stringify(structures));
                SelectFieldController.refresh();
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            }
            req.blockTouchEvents = false;
            req.send();
        }catch (e) {
            console.log(e);
        }
    }
}