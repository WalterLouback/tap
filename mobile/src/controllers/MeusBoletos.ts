import { Component, Prop, Vue } from 'vue-property-decorator';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import HeaderRelease from '@/components/HeaderRelease.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import Boleto from '@/components/Boleto.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import EmptyState from '@/components/EmptyState.vue';
import moment from 'moment';

@Component({
    components: { HeaderFluxo, Boleto, ModalCodigo, DefaultButton, HeaderRelease, EmptyState }
})

export default class MeusBoletos extends Vue {

    modal: boolean = false;
    hasNoBoletos: boolean = false;
    modalMsg: String = '';
    boletos: any = JSON.parse(localStorage.getItem('BOLETOS') || '[]');

    mounted() {
        Util.setBackButtonBehavior(this.goBack);
        this.getBills();
    }

    getBills() {
        const req  = new Request();

        req.route = 'getBillsFromUser';
        req.body  = {
            'USER_ID': localStorage.getItem('USER_ID'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'TOKEN_EXT': localStorage.getItem('TOKEN_EXT'),
            'NRORG': localStorage.getItem('NRORG')
        };
        req.callbackSuccess = (response: any) => {
            if (response.bills.length > 0) {
                this.boletos = response.bills.splice(0, 12);
            } else if (response.bills.des_erro) {
                // req.showErrorMessage('Sua sessão expirou, faça o login novamente.');
                // this.$router.push('Home');
            } else this.hasNoBoletos = true;

            localStorage.setItem('BOLETOS', JSON.stringify(this.boletos));
        };
        req.callbackError = (error: any) => {
            console.log(error);
        };
        req.send();
    }

    billDetails( billId:any ){
        localStorage.setItem('BILL_ID', billId);
        let boleto = this.boletos.find(( bill:any ) => bill.billId == billId )
        localStorage.setItem('BOLETO', JSON.stringify(boleto));
        this.$router.push('BillDetails');
    }

    goBack() {
        this.$router.go(-1);
    }

}