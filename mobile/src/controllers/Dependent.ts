import { Component, Prop, Vue } from 'vue-property-decorator';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import ListDefault from '@/components/ListDefault.vue';
import TwoOptionList from '@/components/TwoOptionList.vue';
import TitleSection from '@/components/TitleSection.vue';
import SectionArea from '@/components/SectionArea.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';
import {VMoney} from 'v-money';

@Component({
    components: { ListDefault, DefaultButton, HeaderFluxo, TwoOptionList, TitleSection, SectionArea, VMoney }
})
export default class Dependent extends Vue {
    dependent: any = JSON.parse(localStorage.getItem('DEPENDENT') || '{}');
    mainCartao: any = JSON.parse(localStorage.getItem('MAIN_CARTAO') || '{}');
    mainCreditCard: any = JSON.parse(localStorage.getItem('USER_DATA') || '{}').MAIN_CARD_ID || '';
    cards: Array<any> = [];
    LAST_NUMBER: any = localStorage.getItem('LAST_NUMBER_MAIN_CARD') || '';
    ID: String = '';

    DEPENDENT_LIMIT: any = this.dependent.DEPENDENCY_DATA ? this.dependent.DEPENDENCY_DATA.MONTHLY_LIMIT : '';
    DEPENDENT_EXPENSES_MONTH: String = '';
    DEPENDENT_MAIN_CARD: String = '';

    BALANCE: any = 0;

    money: any = {
        decimal: ',',
        thousands: '.',
        prefix: 'R$ ',
        suffix: '',
        precision: 2,
        masked: false /* doesn't work with directive */
    };

    mounted(dependent: any, cards: any) {
        Util.setBackButtonBehavior(this.goBack);
        $('#EDITAR_SALDO > button').prop('disabled', true);
        this.getMainCardId(function(response: any) {
            self.mainCreditCard = response.user.MAIN_CARD_ID;
            self.getMainCard(self.mainCreditCard);
        });
        this.getMonthExpensesFromDependent(this.dependent.USER_DATA.ID);

        /* Correção para exibição do valor na máscara */
        if (this.dependent && this.dependent.DEPENDENCY_DATA) {
            this.BALANCE = this.dependent.USER_DATA.ORGANIZATION_DATA.BALANCE * 100;
            this.DEPENDENT_LIMIT = this.dependent.DEPENDENCY_DATA.MONTHLY_LIMIT * 100;
        }

        const self = this;
        setTimeout(function() {
            M.updateTextFields();
            if(self.dependent.DEPENDENCY_DATA.CAN_USE_MAIN_CARD == true){
                $('#CARD_STATUS_'+self.dependent.USER_DATA.ID).attr('checked', 'true');
            }else{
                $('#EDIT_LIMIT_TO_'+self.dependent.USER_DATA.ID + ' > button').prop('disabled', true);
                $('#CANCEL_LIMIT_TO_'+self.dependent.USER_DATA.ID + ' > button').prop('disabled', true);
                $('#limiteDeUso').addClass('opacity-05');
            }
        }, 5);
    }

    getMainCardId(callback: any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'USER_TYPE_ID': 1,
                'TOKEN_EXT' : localStorage.getItem('TOKEN_EXT' || "")
            };
            req.route = 'getUserData';
            req.callbackSuccess = callback;
            req.callbackError = function(error: any) {
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    getMainCard(res1: any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'NRORG': localStorage.getItem('NRORG'),
            };
            req.route = 'getValidCreditCard';
            req.callbackSuccess = function(response: any) {
                self.mainCartao = {};
                self.LAST_NUMBER = '';
                self.cards = response.creditcards;
                self.mainCartao = self.cards.find((card:any) => card.ID == self.mainCreditCard);

                if(self.mainCartao && self.mainCartao.FLAG){
                    self.LAST_NUMBER = self.mainCartao.FLAG + ' - ' + '0000'.substring(0, 4 - String(self.mainCartao.LAST_NUMBERS).length) + (self.mainCartao.LAST_NUMBERS);
                    localStorage.setItem('LAST_NUMBER_MAIN_CARD', self.LAST_NUMBER);
                } else{
                    self.LAST_NUMBER = 'Nenhum cartão cadastrado';
                    localStorage.setItem('LAST_NUMBER_MAIN_CARD', self.LAST_NUMBER);
                    localStorage.setItem('MAIN_CARTAO', '{}');
                }
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    findMainCard(mainCardId: number, cards: any, cardFromParent: any=null) {
        let card = {};
        if (cardFromParent && cardFromParent.ID == mainCardId) card = cardFromParent;
        else card = cards.find((card:any) => card.ID == mainCardId);
        return card;
    }

    enableDisabledDependentCard(ID : any) {
        let DEPENDENT: any = this.dependent;
        if(DEPENDENT.DEPENDENCY_DATA.CAN_USE_MAIN_CARD == true){
            this.disableDependentForMainCard(ID);
            $('#EDIT_LIMIT_TO_'+this.dependent.USER_DATA.ID + ' > button').prop('disabled', true);
            $('#CANCEL_LIMIT_TO_'+this.dependent.USER_DATA.ID + ' > button').prop('disabled', true);
            $('#limiteDeUso').addClass('opacity-05');
        }
        if(DEPENDENT.DEPENDENCY_DATA.CAN_USE_MAIN_CARD == false){
            this.enableDependentForMainCard(ID);
            $('#EDIT_LIMIT_TO_'+this.dependent.USER_DATA.ID + ' > button').prop('disabled', false);
            $('#CANCEL_LIMIT_TO_'+this.dependent.USER_DATA.ID + ' > button').prop('disabled', false);
            $('#limiteDeUso').removeClass('opacity-05');
        }
    }

    enableDependentForMainCard(ID : any) {

        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'DEPENDENT_ID' : ID,
                'NRORG': localStorage.getItem('NRORG'),
            };
            req.route = 'enableDependentForMainCard';
            req.callbackSuccess = function(response: any) {
                if (self.dependent) self.dependent.DEPENDENCY_DATA.CAN_USE_MAIN_CARD = true
                localStorage.setItem('DEPENDENT', JSON.stringify(self.dependent));
                console.log(response);
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    disableDependentForMainCard(ID : any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'DEPENDENT_ID' : ID,
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route = 'disableDependentForMainCard';
            req.callbackSuccess = function(response: any) {
                if (self.dependent) self.dependent.DEPENDENCY_DATA.CAN_USE_MAIN_CARD = false;
                localStorage.setItem('DEPENDENT', JSON.stringify(self.dependent));
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    editDependentMonthlyLimit(ID : any) {

        try {
            const self = this
            const req = new Request();

            const val:any = $('#LIMIT_CARD_'+this.dependent.USER_DATA.ID).val();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'DEPENDENT_ID' : ID,
                'NRORG': localStorage.getItem('NRORG'),
                'MONTHLY_LIMIT': val.split(' ')[1]
            };
            req.route = 'editDependentMonthlyLimit';
            req.callbackSuccess = function(response: any) {
                let LIMIT = $('#LIMIT_CARD_'+self.dependent.USER_DATA.ID).val();
                    if (self.dependent) self.dependent.DEPENDENCY_DATA.MONTHLY_LIMIT = LIMIT;
                    localStorage.setItem('DEPENDENT', JSON.stringify(self.dependent));
                    $('#LIMIT_CARD_'+self.dependent.USER_DATA.ID).prop('disabled', true);
                    $('#EDIT_LIMIT_TO_'+self.dependent.USER_DATA.ID).removeClass('display-none').addClass('display-block');
                    $('#CANCEL_LIMIT_TO_'+self.dependent.USER_DATA.ID).removeClass('display-block').addClass('display-none');
                    $('#SAVE_LIMIT_TO_'+self.dependent.USER_DATA.ID).removeClass('display-block').addClass('display-none');
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    getMonthExpensesFromDependent(ID : any) {

        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'DEPENDENT_ID' : ID,
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route = 'getMonthExpensesFromDependent';
            req.callbackSuccess = function(response: any) {
                self.DEPENDENT_EXPENSES_MONTH = 'R$ ' + response.response.total.toFixed(2).replace('.', ',');
            };
            req.callbackError = function(error: any) {
                
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
            } catch (e) {
                
                console.log(e);
            }
    }

    editLimitMonth() {
        $('#LIMIT_CARD_'+this.dependent.USER_DATA.ID).prop('disabled', false);
        $('#EDIT_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-block').addClass('display-none');
        $('#CANCEL_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-none').addClass('display-block');
        $('#SAVE_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-none').addClass('display-block');
    }

    cancelEditLimitMonth() {
        $('#LIMIT_CARD_'+this.dependent.USER_DATA.ID).prop('disabled', true);
        $('#LIMIT_CARD_'+this.dependent.USER_DATA.ID).val(this.dependent.DEPENDENCY_DATA.MONTHLY_LIMIT);
        $('#EDIT_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-none').addClass('display-block');
        $('#CANCEL_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-block').addClass('display-none');
        $('#SAVE_LIMIT_TO_'+this.dependent.USER_DATA.ID).removeClass('display-block').addClass('display-none');
    }

    goToAddBalance(id: any) {
        localStorage.setItem('USER_RECEIVED_ID', id);
        this.$router.push({ name: 'AddBalance' });
    }

    goBack(): void {
        this.$router.go(-1);
    }


}