 
    import { Component, Prop, Vue } from 'vue-property-decorator';
    import HeaderFluxo from '@/components/HeaderFluxo.vue';
    import DefaultButton from '@/components/DefaultButton.vue';
    import ModalCodigo from '@/components/ModalCodigo.vue';
    import OpenImage from '@/components/OpenImage.vue';
    import ActivitySupportRequest from '@/components/ActivitySupportRequest.vue';
    import Request from '@/ts/Request';
    import Slick from 'vue-slick';
    import Util from '@/ts/Util';
    import moment from 'moment';
    import ImageCompressor from '@/ts/ImageCompressor';
    import ImageUploader from '@/components/ImageUploader.vue';


    @Component({
        components: {ImageUploader, HeaderFluxo, Slick, DefaultButton, ModalCodigo, ActivitySupportRequest, OpenImage}
    })

    export default class SupportRequestDetails extends Vue {
        // supportRequest:    any     = JSON.parse(localStorage.getItem('SUPPORT_REQUEST') || '{}');
        supportRequest:    any     = JSON.parse(localStorage.getItem('SUPPORT_REQUEST') || '{}');
        initialTime:       string  = moment(this.supportRequest.initialTime.date).lang('pt-br').format('DD [de] MMMM, hh[h]mm');
        activities:        any     = [];
        description:       string  = '';
        thisImage:         string  = '';
        file:              any     = [];
        showDeleteAction:  boolean = false;
        oldInputValue:     any     = $('#file').val();
        showModal:         boolean = false;
        showBtnDeleteFile: boolean = false;
        alert:             boolean = false;
        isImagem:          boolean = false;
        userId:            any     = localStorage.getItem('USER_ID'); 
        modalIcon:         string  = 'error_outline';
        modalMessage:      string  = 'Ocorreu um erro desconhecido. Verifique sua conexão.';
        OriginalHeight:    any;
        msg:               string = '';

        mounted() {
            this.getActivities(this.supportRequest.id);
            this.OriginalHeight = $("#textarea1").height();
        }

        getActivities(id: number) {
            try {
                const self = this;
                const req = new Request();
                req.body = {
                    'SUPPORT_REQUEST_ID': id,
                    'USER_ID': localStorage.getItem('USER_ID'),
                    'TOKEN': localStorage.getItem('TOKEN'),
                    'NRORG': localStorage.getItem('NRORG')
                };

                req.route  = 'getActivityRequestByNrorgAndSupportRequest';
                req.callbackSuccess = function(response: any) {
                    self.activities = response.response.activityRequest.length > 0 ? self.factoryActivities(response.response.activityRequest) : '';
                    // setTimeout(function(){
                    //     self.focusInput('lastMessage');
                    // }, 100);
                    self.focusInput('lastMessage');
                };
                req.callbackError = function(error: any) {
                }
                req.blockTouchEvents = true;
                req.send();
            } catch (e) {
            }
        }

        focusInput( elClass:any ){
            const offset = -60;
            const el: any = document.getElementsByClassName(elClass);
            setTimeout(function(){
                console.log('haai');
                el[0].scrollIntoView();
            },150);
        }

        openThisImage( ulr: string ) {
            const oi: any = OpenImage;
            (OpenImage as any).openModal('image-modal', ulr);
        }

        factoryActivities(activities: any) {
            let self = this;
            activities.forEach((element: any) => {
                element.date = moment(element.createdAt.date).lang('pt-br').format('DD [de] MMMM, hh[h]mm');
                element.side = self.userId == element.userId;
            });
            return activities;
        }

        goBack() {
            this.$router.go(-1);
        }

        readURL( input:any ) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function ( e:any ) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        previewFiles(event: any) {
            const self = this;
            const iu: any = ImageUploader;
            (ImageUploader as any).requestUploadMethod(function(f: any) {
                console.log(f);
                $('#file').val(f);
                setTimeout(function() {
                    $('#blah').attr('src', f);
                }, 500);
                self.file = f;
                self.isImagem = true;
                self.ifHasFile('Arquivo anexado');
                self.showDeleteAction = true;
                self.oldInputValue = f;
            }, () => {});
        }

        getBase64(file: any, cb: Function) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
            cb(reader.result);
            $('#blah').attr('src', String(reader.result));
            };
            reader.onerror = function (error) {
            
            };
        }

        ifHasFile( NAME: any ){
            var inputs = document.querySelectorAll( '.inputfile' );
            let self = this;
            Array.prototype.forEach.call( inputs, function( input : any ){
                var label	 = input.nextElementSibling;
                    label.innerHTML = NAME;
            });
        }

        setFile(file: string) {
            this.file = file;
        }

        cleanInputFile() {
            const self = this;
            let control = $("#file");
            control.replaceWith( control.val('') );
            this.file = [];
            // self.ifHasFile('anexar comprovante');
            self.showDeleteAction = false;
        }

        checkIfIsImage( target:any ){
            var file:any = target;
            var fileType = file.type;
            console.log(fileType);
            var validImageTypes = ["image/gif", "image/jpeg", "image/jpg", "image/png"];
            if ($.inArray(fileType, validImageTypes) < 0) {
                this.isImagem = false;
            }else  { 
                this.isImagem = true;
            }
        }

        alertModal( mensagem:string ){
            this.alert = true;
            this.msg = mensagem;
        }

        createActivity() {
            console.log(this.file);
            if(this.description == '' && this.file == '' ){
                this.alertModal('Favor digitar uma mensagem ou anexar alguma imagem.');
                return 0;
            }

            try {
                const self = this;
                const req = new Request();
                req.body = {
                    'SUPPORT_REQUEST_ID': self.supportRequest.id,
                    'USER_ID': localStorage.getItem('USER_ID'),
                    'TOKEN': localStorage.getItem('TOKEN'),
                    'NRORG': localStorage.getItem('NRORG'),
                    'ACTIVITY_REQUEST_OLD_ID': self.activities[self.activities.length - 1].id,
                    'DESCRIPTION': self.description,
                    'FILE': self.file,
                    'TYPE': 'Response'
                };

                req.route  = 'createActivityRequest';

                req.callbackSuccess = function(response: any) {
                    self.cleanInputFile();
                    self.getActivities(self.supportRequest.id);
                    self.description = '';
                    $("#textarea1").height(self.OriginalHeight);
                    // setTimeout(function(){
                    //     self.focusInput('lastMessage');
                    // }, 50);
                };

                req.callbackError = function(error: any) {
                    if (error.response.data.errorCode == 234) {
                        self.alertModal('Favor anexar apenas imagens.');
                    }else self.alertModal('Ocorreu um erro inesperado ao enviar a solicitação. Tente novamente mais tarde.');
                }

                req.blockTouchEvents = true;
                req.send();
            } catch (e) {
                console.log(e);
                let mensagem: string = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';
                // this.mostraModal(mensagem);
            }
        }
    }