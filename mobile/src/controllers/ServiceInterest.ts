import { Component, Prop, Vue } from 'vue-property-decorator';
import Util from '@/ts/Util';
import Request from '@/ts/Request';
import HeaderRelease from '@/components/HeaderRelease.vue';
import TabsHome from '@/components/TabsHome.vue';
import ListDefault from '@/components/ListDefault.vue';
import RadioChipInterest from '@/components/RadioChipInterest.vue';
import VForType from '@/components/RadioChipInterest.vue';
// import RadioChipCourts, { VForType } from '@/components/RadioChipCourts.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import moment from 'moment';

declare var DraggableCal: any;
declare var M: any;

@Component({
    components: {
        HeaderRelease,
        TabsHome,
        DraggableCal,
        ListDefault,
        RadioChipInterest,
        // RadioChipCourts,
        DefaultButton
    }
})

export default class ServiceInterest extends Vue {
    service: any = JSON.parse(localStorage.getItem('SERVICE') || '{}');
    horarios: VForType = JSON.parse(localStorage.getItem('HORARIOS') || '[]');
    dateAppointment: string = localStorage.getItem('DATE_APPOINTMENT') || '';
    dayOfWeek: any = localStorage.getItem('DAY_OF_WEEK') || -1;
    availableProfessional: any = JSON.parse(localStorage.getItem('AVAILABLE_PROFESSIONAL') || '{}');
    listProfessional: any = JSON.parse(localStorage.getItem('LIST_PROFESSIONAL') || '{}');
    checkedOption: any = localStorage.getItem('SELECTED_SCHEDULE_ID') || 0;
    MAX_DAYS: number = 120;
    checked: boolean = false;
    idTimesheet: any = localStorage.getItem('TIME_TIMESHEET_ID') || '';
    abas: any = [
        { id: 1, NAME: 'Interesse em jogar' },
        { id: 2, NAME: 'Quem está jogando' }
    ];
    hasHour: boolean = false;
    msgErro: string = '';
    chosenDay: string = localStorage.getItem('CHOSEN_DATE') || '';
    disabled = true;
    tabAtiva = localStorage.getItem('TAB_ATIVA') || '0';
    enableButton = false;

    mounted() {
        const self = this;

        let calElem = document.querySelectorAll('[selected]');
        let el: any;
        if (calElem != null) {
            el = calElem[calElem.length - 1];
        }
        if (el != null) {
            el.removeAttribute('selected');
            el.removeAttribute('style');
        }

        self.availableProfessional = [];
        self.chosenDay = '';
        self.checkedOption = 0;
        self.horarios = new VForType();
        self.enableButton = false;

        Util.setBackButtonBehavior(this.goBack);
    }

    dateSchedule(date: any) {
        this.availableProfessional = [];
        this.dateAppointment = moment(date).utc().format('DD/MM/YYYY');
        this.dayOfWeek = moment(date).weekday();
        this.horarios = new VForType();
        this.checkedOption = 0;

        this.convertChosenDate(date);

        localStorage.setItem('DATE_APPOINTMENT', this.chosenDay);
        localStorage.setItem('DAY_OF_WEEK', this.dayOfWeek);
        localStorage.setItem('SELECTED_SCHEDULE_ID', this.checkedOption);

        this.getSchedulesByTime();
    }

    getSchedulesByTime() {
        try {
            const req: any = new Request();
            req.body = {
                "EVENT_ID": this.service.EVENT_RELS[0].EVENT_ID,
                "DAY_OF_WEEK": this.dayOfWeek,
                "DATE": moment(this.chosenDay).add(1, 'month').format('DD/MM/YYYY'),
                "USER_ID": localStorage.getItem('USER_ID') || '',
                "NRORG": localStorage.getItem('NRORG'),
                "TOKEN": localStorage.getItem('TOKEN') || '',
            };
            req.route = 'getVacantTimesheetWithInterested';
            req.callbackSuccess = (response: any) => {
                if (response && response.timesTimesheet && response.timesTimesheet.hours) {
                    if (response.timesTimesheet.hours.length > 0) {
                        let dataHours = response.timesTimesheet.hours.map((elem: any) => { return { id: elem.id, schedule: elem.initialTime, interested: elem.interested + ' Interessados', disabled: this.hasInterested(elem.idsInterested) } })
                        this.horarios = new VForType(dataHours);
                        this.hasHour = true;
                    } else {
                        this.horarios = new VForType();
                        this.hasHour = false;
                        this.msgErro = "Não há horários disponiveis para este dia!"
                    }
                }
            };
            req.callbackError = (error: any) => {
                console.log(error);
            };
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    hasInterested(elem: any) {
        let idUser = localStorage.getItem('USER_ID');
        if(elem.length > 0) {
            for(let i = 0; i < elem.length; i ++) {
                if(elem[i] == idUser) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    get selectHour() {
        return ;
    }

    set selectHour(id: any) {
        localStorage.setItem('TIME_TIMESHEET_ID', id);
        this.idTimesheet = id;
        this.checkedOption = localStorage.getItem('TIME_TIMESHEET_ID'); 
    }

    relateUserToTennisCourt() {
        try {
            const req: any = new Request();
            req.body = {
                "USER_ID": localStorage.getItem('USER_ID') || '',
                "NRORG": localStorage.getItem('NRORG'),
                "TOKEN": localStorage.getItem('TOKEN') || '',
                "APPOINTMENT_DATE": this.dateAppointment,
                "SERVICE_ID": this.service.ID,
                "TIME_TIMESHEET_ID": this.idTimesheet,
                "STRUCTURE_ID": this.service.EVENT_RELS[0].STRUCTURE,
            };
            req.route = 'relateUserToTennisCourt';
            req.visibilityOptions = { 'success': false, 'error': false, 'loading': true };
            req.callbackSuccess = (response: any) => {
                this.goBack();
                req.showSuccessMessage('Seu interesse foi cadastrado com sucesso!');
            };
            req.callbackError = (error: any) => {
                req.showErrorMessage('Ocorreu um erro ao cadastrar seu interesse, tente novamente mais tarde.');
                console.log(error);
            };
            req.loadingMessage = 'Cadastrando interesse no jogo de tênis';
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e);
        }
    }

    checkProfessional(id: any) {
        if (localStorage.getItem('TAB_ATIVA') === '1') {
            this.enableButton = true;
        }
        this.checkedOption = id;
    }

    convertChosenDate(date: any) {
        let tempDate = moment(date).utc().format('YYYY-M-D');
        let temp = tempDate.split('-')
        let month = parseInt(temp[1]) - 1;
        tempDate = temp[0] + '-' + month.toString() + '-' + temp[2];
        this.chosenDay = tempDate;
        localStorage.setItem('CHOSEN_DATE', this.chosenDay);
    }

    goBack() {
        this.$router.push("ServicesMenu");
    }

    destroyed() {
        localStorage.removeItem('DATE_APPOINTMENT');
        localStorage.removeItem('CHOSEN_DATE');
        localStorage.removeItem('TAB_ATIVA');
        localStorage.removeItem('DAY_OF_WEEK');
        localStorage.removeItem('PROFESSIONAL_ID');
        localStorage.removeItem('AVAILABLE_PROFESSIONAL')
        localStorage.removeItem('LIST_PROFESSIONAL')
        localStorage.removeItem('SELECTED_SCHEDULE_ID')
        localStorage.removeItem('HORARIOS')
    }

}