import { Component, Prop, Vue } from 'vue-property-decorator';
import DefaultButton from '@/components/DefaultButton.vue';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import ListDefault from '@/components/ListDefault.vue';
import ThreeOptionDependents from '@/components/ThreeOptionDependents.vue';
import TitleSection from '@/components/TitleSection.vue';
import ModalCodigo from '@/components/ModalCodigo.vue';
import SectionArea from '@/components/SectionArea.vue';
import TextLink from '@/components/TextLink.vue';
import EmptyState from '@/components/EmptyState.vue';
import Request from '@/ts/Request';
import Util from '@/ts/Util';

declare var $: any;
@Component({
    components: { ListDefault, DefaultButton, HeaderFluxo, ThreeOptionDependents, TitleSection, ModalCodigo, TextLink, SectionArea, EmptyState }
})
export default class Dependents extends Vue {
    dependents: any = JSON.parse(localStorage.getItem('USER_DEPENDENTS') || '[]');
    emailPref: any = '';
    compareEmail: any = '';
    statusDep: String = '';
    FIRST_NAME: String = '';
    LAST_NAME: String = '';
    ID: String = '';
    showModal: boolean = false;


    mounted() {
        Util.setBackButtonBehavior(this.goBack);

        this.getUserDependents();
        const self = this;
        if (self.dependents.length > 0)
            this.emailPref = self.dependents[0].DEPENDENCY_DATA.RECEIPTS_TO;
        this.compareEmail = this.emailPref;
        this.disabledButton('SAVE_RECEIPTS_TO');
        setTimeout(function () {
            M.updateTextFields();
        }, 50);
    }

    disabledButton(ID: any) {
        const self = this;
        setTimeout(function () {
            if (self.emailPref == self.compareEmail) $('#' + ID + ' > button').prop('disabled', true);
            if (self.emailPref != self.compareEmail) $('#' + ID + ' > button').prop('disabled', false);
        }, 0);
    }

    getUserDependents() {

        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'USER_TYPE_ID': 1
            };
            req.route = 'getUserDependents';
            req.callbackSuccess = function (response: any) {
                self.dependents = response.dependents;
                console.log(response)
                if (response.dependents.length > 0) {
                    self.emailPref = response.dependents[0].DEPENDENCY_DATA.RECEIPTS_TO;
                    //self.setDependentsStatus();
                }
                //Verificar se o Dependente está ativo, se o dependente estiver irá marcar o checkbox do botão de ativar/inativar o dependente
                M.updateTextFields();
                localStorage.setItem('USER_DEPENDENTS', JSON.stringify(self.dependents));
            };
            req.callbackError = function (error: any) {

                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {

            console.log(e);
        }
    }

    setDependentsStatus() {
        const self = this;
        //Verificar se o Dependente está ativo, se o dependente estiver irá marcar o checkbox do botão de aticonst/inativar o dependente
        setTimeout(function () {
            self.dependents.forEach((dependent: any) => {
                if (dependent.DEPENDENCY_DATA.STATUS == 'A') {
                    $('#STATUS_' + dependent.USER_DATA.ID).attr('checked', 'true');
                }
                M.updateTextFields();
            });
        }, 200);
    }

    enableDisableDependent(ID: any) {
        const DEPENDENT: any = this.dependents.find((dependent: any) => dependent.USER_DATA.ID == ID);
        if (DEPENDENT.DEPENDENCY_DATA.STATUS == 'A') {
            let first = DEPENDENT.USER_DATA.FIRST_NAME;
            let last = DEPENDENT.USER_DATA.LAST_NAME;
            let msg = 'O dependente ' + first + ' ' + last + ' foi desabilitado.';
            this.disableDependent(ID);
            this.mostraModal(msg);

        }
        if (DEPENDENT.DEPENDENCY_DATA.STATUS == 'D') {
            let first = DEPENDENT.USER_DATA.FIRST_NAME;
            let last = DEPENDENT.USER_DATA.LAST_NAME;
            let msg = 'O dependente ' + first + ' ' + last + ' foi habilitado.';
            this.enableDependent(ID);
            this.mostraModal(msg);
        }
    }

    disableDependent(ID: any) {

        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'DEPENDENT_ID': ID,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || '',
            };
            req.route = 'disableDependent';
            req.callbackSuccess = function (response: any) {
                let dependent: any = self.dependents.find((dependent: any) => dependent.USER_DATA.ID == ID);
                if (dependent) dependent.DEPENDENCY_DATA.STATUS = 'D';
                localStorage.setItem('USER_DEPENDENTS', JSON.stringify(self.dependents));
            };
            req.callbackError = function (error: any) {

                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {

            console.log(e);
        }
    }

    enableDependent(ID: any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'DEPENDENT_ID': ID,
                'NRORG': localStorage.getItem('NRORG'),
                'TOKEN': localStorage.getItem('TOKEN') || ''
            };
            req.route = 'enableDependent';
            req.callbackSuccess = function (response: any) {
                let dependent: any = self.dependents.find((dependent: any) => dependent.USER_DATA.ID == ID);
                if (dependent) dependent.DEPENDENCY_DATA.STATUS = 'A';
                localStorage.setItem('USER_DEPENDENTS', JSON.stringify(self.dependents));
            };
            req.callbackError = function (error: any) {

                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {

            console.log(e);
        }
    }

    editDependentReceiptsTo(ID: any) {
        try {
            const self = this
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID') || '',
                'TOKEN': localStorage.getItem('TOKEN') || '',
                'RECEIPTS_TO': this.emailPref,
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route = 'editDependentReceiptsTo';
            req.callbackSuccess = function (response: any) {
                let dependent: any = self.dependents.find((dependent: any) => dependent.USER_DATA.ID == ID);
                if (dependent) dependent.DEPENDENCY_DATA.STATUS = 'A';
            };
            req.callbackError = function (error: any) {
                let msg = 'Ocorreu um erro para salvar o seu email.'
                self.mostraModal(msg);
                console.log(error);
            };
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {

            console.log(e);
        }
    }

    editReceiptsTo() {
        $('#SEND_EMAIL').prop('disabled', false);
        $('#EDIT_RECEIPTS_TO').removeClass('display-block').addClass('display-none');
        $('#CANCEL_RECEIPTS_TO').removeClass('display-none').addClass('display-block');
        $('#SAVE_RECEIPTS_TO').removeClass('display-none').addClass('display-block');
    }

    cancelReceiptsTo() {
        $('#SEND_EMAIL').prop('disabled', true);
        $('#EDIT_RECEIPTS_TO').removeClass('display-none').addClass('display-block');
        $('#CANCEL_RECEIPTS_TO').removeClass('display-block').addClass('display-none');
        $('#SAVE_RECEIPTS_TO').removeClass('display-block').addClass('display-none');
    }

    goBack() {
        this.$router.go(-1);
    }

    goTo(id: any) {
        localStorage.setItem('DEPENDENT', JSON.stringify(this.dependents.find((dependent: any) => dependent.USER_DATA.ID == id)));
        this.$router.push('Dependent');
    }

    mostraModal(text: string) {
        this.statusDep = text;
        this.showModal = true;
    }

    fechaModal() {
        this.showModal = false;
    }
}