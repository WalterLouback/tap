 
import { Component, Prop, Vue } from 'vue-property-decorator';
import HeaderFluxo from '@/components/HeaderFluxo.vue';
import DefaultButton from '@/components/DefaultButton.vue';
import ThreeOptionsListGeneral from '@/components/ThreeOptionsListGeneral.vue';
import Request from '@/ts/Request';
import Slick from 'vue-slick';
import Util from '@/ts/Util';
import EmptyState from '@/components/EmptyState.vue';


@Component({
    components: {HeaderFluxo, Slick, DefaultButton, ThreeOptionsListGeneral, EmptyState}
})

export default class SupportRequest extends Vue {

    supportRequests: any = [];
    hasNoSupportRequest: boolean = false;

    mounted() {
        this.getSupportRequestsByUser();
    }

    goBack() {
        this.$router.go(-1);
    }

    getSupportRequestsByUser() {
        try {
            const self = this;
            const req = new Request();
            req.body = {
                'USER_ID': localStorage.getItem('USER_ID'),
                'TOKEN': localStorage.getItem('TOKEN'),
                'NRORG': localStorage.getItem('NRORG')
            };
            req.route  = 'getSupportRequestsByUser';
            req.callbackSuccess = function(response: any) {
                self.supportRequests = response.response.supportRequests;
                if(!self.supportRequests[0]){
                    self.hasNoSupportRequest = true;
                }
                console.log(self.supportRequests); 
            };
            req.callbackError = function(error: any) {
                // self.mostraModal('Ocorreu um erro inesperado co enviar a solicitação!', 'done');

            }
            req.blockTouchEvents = false;
            req.send();
        } catch (e) {
            console.log(e);
            let mensagem: string = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';
            // this.mostraModal(mensagem);
        }
    }

    goSupportRequestDetails(supportRequest: any) {
        localStorage.setItem('SUPPORT_REQUEST', JSON.stringify(supportRequest));
		this.$router.push('SupportRequestDetails');
    }

    goNewSupportRequest() {
        this.$router.push('AddSupportRequest');
    }

}