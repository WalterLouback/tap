import { Component, Prop, Vue } from "vue-property-decorator";
import Router from "@/router";
import DefaultButton from "../components/DefaultButton.vue";
import HeaderFluxo from "../components/HeaderFluxo.vue";
import ListDefault from "../components/ListDefault.vue";
import SectionArea from "../components/SectionArea.vue";
import Util from "../ts/Util";
import ButtonCard from "../components/ButtonCard.vue";
import HeaderFilter from "../components/HeaderFilter.vue";
import Request from "../ts/Request";
import { VMoney } from "v-money";
import HeaderRelease from "../components/HeaderRelease.vue";
import BottomNavigation from "@/modules/shared-components/BottomNavigation.vue";
import LoginModal from "@/modules/shared-components/LoginModal.vue";
import HeaderCircleButton from "@/modules/shared-components/HeaderCircleButton.vue";
import ImageCardButton from "@/modules/shared-components/ImageCardButton.vue";
import Minicard from "@/modules/shared-components/Minicard.vue";
import CircleButton from "@/modules/shared-components/CircleButton.vue";
import ModalCodigo from "../components/ModalCodigo.vue";
import InputText from "@/modules/shared-components/InputText.vue"

@Component({
  props: { dependentsData2: String },
  components: {
    DefaultButton,
    HeaderFilter,
    ButtonCard,
    HeaderFluxo,
    ListDefault,
    SectionArea,
    VMoney,
    HeaderRelease,
    BottomNavigation,
    LoginModal,
    HeaderCircleButton,
    ImageCardButton,
    Minicard,
    CircleButton,
    ModalCodigo,
    InputText
  }
})
export default class Extras extends Vue {
  // dependentsData: any = JSON.parse(
  //   localStorage.getItem("DEPENDENT_DATA") || "[]"
  // );
  // dependents: any = JSON.parse(
  //   localStorage.getItem("DEPENDENT") || "[]"
  // );
  // hasDependents: Boolean = false;
  hasUser: Boolean = false;
  // carteiraDependentes: Boolean = true;
  showLoginModal: boolean = false;
  bnKey = 0;
  logo: string = localStorage.getItem('ORGANIZATION_LOGO_SMALL')||'logo-mtc-home.svg';
  billingPic:string = Util.getImgUrl('billing','.png');
  coursesPic: string = Util.getImgUrl('courses1','.jpg');
  cardsPic: string = Util.getImgUrl('cards','.jpg');
  complaintsPic: string = Util.getImgUrl('phone','.jpg');
  emailInput: string = '';
  passwordInput: string = '';
  showLogoutModal: boolean = false;
  showModal: boolean = false;
  modalMessage: string = '';

  mounted() {
    //Util.removeBackButtonBehavior();
    Util.setBackButtonBehavior(this.goBack);
    // this.getWalletFromUserAndDependents();
    // localStorage.setItem("DEPENDENT", JSON.stringify(this.dependentsData));
    // localStorage.getItem("DEPENDENT");
  }

  totemLogout(){
    var loginSettings = localStorage.getItem('LOGIN_PROPS') || ''
    localStorage.clear(); 
    localStorage.setItem('LOGIN_PROPS', loginSettings);
    this.$router.push('/LogIn');
  }

  beforeDestroy() {
    Util.removeBackButtonBehavior();
  }

  goToRegisteredCards() {
    if (localStorage.getItem("USER_ID")) this.$router.push("RegisteredCards");
    else this.openLoginModal();
  }

  goToDependents() {
    if (localStorage.getItem("USER_ID")) this.$router.push("Dependents");
    else this.openLoginModal();
  }

  goToBillings(){
    if (localStorage.getItem("USER_ID")) this.$router.push("MeusBoletos");
    else this.openLoginModal();
  }

  goToSupportRequests(){
    if (localStorage.getItem("USER_ID")) this.$router.push("SupportRequest");
    else this.openLoginModal();
  }

  goToExtrato() {
    if (localStorage.getItem("USER_ID")) this.$router.push("Extrato");
    else this.openLoginModal();
  }

  openLoginModal() {
    this.showLoginModal = true;
  }

  closeLoginModal() {
    this.showLoginModal = false;
  }

  data() {
    return {
      inputValues: {}
    };
  }

  goBack() {
    Util.removeBackButtonBehavior();
    this.$router.push('Home')
  }

  adminAuth(){
        return new Promise((resolve: Function, reject: Function) => {
            const req = new Request();
            req.route = 'loginBackoffice';
            req.body   = {
                'EMAIL': this.emailInput,
                'PASSWORD': this.passwordInput,
                'NRORG': localStorage.getItem('NRORG'),
                'USER_TYPE_ID': 4
            };
            req.callbackSuccess = ()=>{
                this.totemLogout();
            };
            req.callbackError = (error: any) => { 
                const _window: any = window;
                var mensagem: string;

                if (_window.navigator.onLine === false) mensagem = 'Sem conexão com a internet.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 1) mensagem = 'Usuário não encontrado.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 2) mensagem = 'Usuário ou senha incorretos.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 3) mensagem = 'Usuário ou senha incorretos.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 10 || error && error.response && error.response.data && error.response.data.errorCode == 11) mensagem = 'Usuário não autenticado.';
                else if (error && error.response && error.response.data && error.response.data.errorCode == 1002) mensagem = error.response.data.error;
                else mensagem = 'Ocorreu um erro desconhecido. Por favor, tente de novo mais tarde';

                this.openModal(mensagem);
            };
            req.callbackTimeout = () => { this.openModal('Não foi possível estabelecer uma conexão para completar');};
            if (!window.navigator.onLine) { this.openModal("Não é possível completar a operação offline"); }
            else req.send();
        });
  }

  openModal(msg:string ){
    this.modalMessage = msg;
    this.showModal = true;
  }

  updateEmailInput(value: string){
    this.emailInput = value; 
  }
  updatePasswordInput(value: string){
    this.passwordInput = value; 
  }

}