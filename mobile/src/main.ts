import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'materialize-css';
import 'materialize-css/dist/css/materialize.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import './stylesheet/app.scss';
import './stylesheet/components.scss';
import './stylesheet/views.scss';
import './font-awesome-imports';
import { defineCustomElements as webSocialShareInput } from 'web-social-share/dist/loader';


// import "@/thirdparty/vivify.min.css";
// import '@/thirdparty/card/card.js';
// import '@/thirdparty/card/card.css';
// import '@/thirdparty/slick/slick.css';
// import '@/thirdparty/slick/slick.js';
// import '@/thirdparty/slick/slick-theme.css';
// import '@/thirdparty/to-title-case.js';

Vue.config.productionTip = false;
Vue.config.devtools = true;

declare var $: any;

webSocialShareInput(window);

Vue.config.ignoredElements = [
  "web-social-share"
];

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');