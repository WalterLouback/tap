//General
import Vue from 'vue';
import Router from 'vue-router';

//Others
import Home from './views/Home.vue';
import Util from './ts/Util';
import Component from 'vue-class-component';
import Notifications from './views/Notifications.vue';
import Perfil from './views/Perfil.vue';
import EventDetails from './views/EventDetails.vue';
import EventStores from './views/EventStores.vue';
import EventSchedule from './views/EventSchedule.vue';
import BuyEventTicket from './views/BuyEventTicket.vue';
import NominateTicket from './views/NominateTicket.vue';
import Explorar from './views/Explorar.vue';
import MenuEvents from './views/MenuEvents.vue';
import Schedule from './views/Schedule.vue';
import Services from './views/Services.vue';
import Extras from './views/Extras.vue';
import ServicesChildren from './views/ServicesChildren.vue';
import ServiceInterest from './views/ServiceInterest.vue';
import ServicesSchedule from './views/ServicesSchedule.vue';
import ModalityDependents from './views/ModalityDependents.vue';
import MenuEducation from './views/MenuEducation.vue';
import Events from './views/Events.vue';
import SupportRequest from './views/SupportRequest.vue';
import SupportRequestDetails from './views/SupportRequestDetails.vue';
import MeusBoletos from './views/MeusBoletos.vue';
import Dependent from './views/Dependent.vue';
import Dependents from './views/Dependents.vue';
import TicketPayment from './views/TicketPayment.vue';

const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);

Vue.use(Router);
const router = new Router({
  routes: [
    {
      path: '/',
      component: Explorar
    },
    {
      path: '/Home',
      component: Home
    },
    {
      path: '/Notifications',
      component: Notifications
    },
    {
      path: '/Perfil',
      component: Perfil
    },
    {
      path: '/Explorar',
      component: Explorar
    },
    {
      path: '/MenuEvents',
      component: MenuEvents
    }
    ,
    {
      path: '/Schedule',
      component: Schedule
    },
    {
      path: '/Services',
      component: Services
    },
    {
      path: '/Extras',
      component: Extras
    },
    {
      path: '/MenuEducation',
      component: MenuEducation
    },
    {
      path: '/ModalityDependents',
      component: ModalityDependents
    },
    {
      path: '/ServicesChildren',
      component: ServicesChildren
    },
    {
      path: '/ServiceInterest',
      component: ServiceInterest
    },
    {
      path: '/ServicesSchedule',
      component: ServicesSchedule
    },
    {
      path: '/Events',
      component: Events
    },
    {
      path: '/Dependent',
      component: Dependent
    },
    {
      path: '/Dependents',
      component: Dependents
    },
    {
      path: '/MeusBoletos',
      component: MeusBoletos
    },
    {
      path: '/SupportRequest',
      component: SupportRequest
    },
    {
      path: '/SupportRequestDetails',
      component: SupportRequestDetails
    },
    {
      path: '/SupportRequestDetails',
      component: SupportRequestDetails
    },
    {
      path: '/TicketPayment',
      component: TicketPayment
    }
  ]
});
export default router;