import { Component, Prop, Vue } from 'vue-property-decorator'

declare var FCMPlugin: any;
declare var priority: any;
declare var device: any;
declare var cordova: any;

@Component({
    components: { }
})

export default class FCM extends Vue {
    
    init(onNewToken: Function, onPushOrderTapped: Function, onForegroundNotification: Function) {
        document.addEventListener("deviceready", ()=> {
            let priority: any = "high";
            if (device.platform != "Ios") {
                priority = 1;
            }

            if (typeof FCMPlugin != 'undefined') {
                FCMPlugin.onTokenRefresh(onNewToken);
                FCMPlugin.getToken(onNewToken);
                FCMPlugin.onNotification(function(data: any){
                    if(data.wasTapped){
                        console.log('notification was tapped');
                        console.log(data);
                        onPushOrderTapped(data);
                    }
                    else if (device.platform == "Android" && device.version >= "8" && device.version < "9") {
                        // se a versão do android for a Oreo, o plugin crasha
                        console.log('Android Oreo foreground notification');
                    }
                    else {
                        console.log('foreground notification');
                        console.log(data);
                        onForegroundNotification(data);
                        const plugins: any = cordova.plugins;
                        plugins.notification.local.schedule({
                            title: data.title,
                            text: data.message,
                            icon: data.icon,
                            priority: priority,
                            foreground: true
                        });
                    }
                });
            }
            else {
                console.error('Erro ao iniciar FCMPlugin');
            }
        } ,false);
    }
}