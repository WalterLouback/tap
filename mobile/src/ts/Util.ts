import { Component, Vue } from 'vue-property-decorator';
import Request from '@/ts/Request';
import moment from 'moment';
import router from '@/router';

declare var codePush: any;
declare var SyncStatus: any;

@Component
export default class Util extends Vue {
    private static listener: EventListener;
    private static listenerPedido: EventListener;
    private static listenerProdutos: EventListener;


    static setBackButtonBehavior(listener: EventListener) {
        if (!!Util.listener) document.removeEventListener("backbutton", Util.listener);
        Util.listener = listener;
        if (Util.listener !=undefined)
        document.addEventListener("backbutton", Util.listener, false);
    }

    static getBackButtonBehavior() {
       return Util.listener;
    }
    
    static removeBackButtonBehavior(){
        document.removeEventListener("backbutton", Util.listener);
    }

    static setAddPedido(listener: EventListener) {
        if (!!Util.listenerPedido) document.removeEventListener("keypress", Util.listenerPedido);
        Util.listenerPedido = listener;
        document.addEventListener("keypress", Util.listenerPedido, false);
    }
    
    static removeAddPedido(){
        document.removeEventListener("keypress", Util.listenerPedido);
    }
    static setAddProdutos(listener: EventListener) {
        if (!!Util.listenerProdutos) document.removeEventListener("keypress", Util.listenerProdutos);
        Util.listenerProdutos = listener;
        document.addEventListener("keypress", Util.listenerProdutos, false);
    }

    
    static removeAddProdutos(){
        document.removeEventListener("keypress", Util.listenerProdutos);
    }


    static addNpfMask(npf: string) {
        npf = Util.removeMasks(npf);
        const len: number = npf.length;
        if (len > 3) {
            npf = npf[0] + ' ' + npf.substring(1, len - 1) + '-' + npf[len - 1];
        }
        return npf;
    }

    static removeMasks(npf: string) {
        return npf.replace(/-/g, '').replace(/\./g, '').replace(/ /g, '');
    }

    static addCpfMask(cpf: string) {
        cpf = Util.removeMasks(cpf);
        const len: number = cpf.length;
        if (len == 11) {
            cpf = cpf.substring(0, 3) + '.' + cpf.substring(3, 6) + '.' + cpf.substring(6, 9) + '-' + cpf.substring(9, 11);
        }
        return cpf;
    }

    static onNewFCMToken(fcmToken: string) {
        const req: Request = new Request();
        req.route = 'updateFCMToken';
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'FCM_TOKEN': fcmToken
        };
        req.callbackSuccess = function (response: any) { };
        req.callbackError = function (error: any) { };
        req.showLoader = false;
        req.send();
    }

    static removeFCMToken() {
        const req: Request = new Request();
        req.route = 'updateFCMToken';
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN'),
            'USER_ID': localStorage.getItem('USER_ID'),
            'FCM_TOKEN': ""
        };
        req.callbackSuccess = function (response: any) { };
        req.callbackError = function (error: any) { };
        req.showLoader = false;
        req.send();
    }    

    static getUserData(callback?: Function) {
        const self = this
        const req = new Request();
        req.body = {
            'USER_ID': localStorage.getItem('USER_ID') || '',
            'NRORG': localStorage.getItem('NRORG'),
            'TOKEN': localStorage.getItem('TOKEN') || '',
            'USER_TYPE_ID': 1,
            'TOKEN_EXT': localStorage.getItem('TOKEN_EXT' || "")
        };
        req.route = 'getUserData';
        req.callbackSuccess = function (response: any) {
            const now: any = moment();
            localStorage.setItem('USER_DATA', JSON.stringify(response.user));
            localStorage.setItem('USER_NAME', response.user.FIRST_NAME);
            localStorage.setItem('LAST_GET_USER_DATA', now);
            if (callback) callback(response);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.send();
    }

    static getUserReports(callback: any) {
        const req = new Request();
        req.body = {
            'NRORG': localStorage.getItem('NRORG'),
            'USER_ID': localStorage.getItem('USER_ID'),
            "TOKEN": localStorage.getItem("TOKEN")
        };
        req.route = 'getReportsByUser';
        req.callbackSuccess = function (response: any) {
            if (callback) callback(response.reports);
            const now: any = moment();
            localStorage.setItem('LAST_GET_REPORTS', now);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.showLoader = true;
        req.send();
    }

    static getUserNotifications(callback: any) {

        const req = new Request();
        req.body = {
            "NRORG": localStorage.getItem('NRORG'),
            "USER_ID": localStorage.getItem("USER_ID"),
            "TOKEN": localStorage.getItem("TOKEN")
        };
        req.route = 'getNotificationsByUser';
        req.callbackSuccess = function (response: any) {
            if (callback) callback(response.notifications);
            const now: any = moment();
            localStorage.setItem('LAST_GET_NOTIFICATIONS', now);
        };
        req.callbackError = function (error: any) {
            console.log(error);
        };
        req.showLoader = true;
        req.send();
    }

    static getImgUrl(imageName: string, extension: string = ".svg") {
        var images = require.context('../assets/', false)
        return images('./' + imageName + extension)
    }

    static capitalize(text: string) {
        return text ? text
            .split(" ")
            .map(x => x[0].toUpperCase() + x.substring(1).toLowerCase())
            .join(" ") : ''
    }

    static closeKeyboard() {
        $('input').blur();
    }

    static checkUpdateReadyToInstall() {
        const now: any = moment();
        const lastResume: any = localStorage.getItem('LAST_RESUME');
        const duration: Number = Number(moment.duration(moment().diff(lastResume, 'hours')));
        const actualRoute: string = String(router.currentRoute.name);
        const restartEnabledRoutes = ['HomeApp', 'EventsMenu', 'MenuPedidos', 'ServicesMenu'];

        if (duration >= 6 && localStorage.getItem('UPDATE_INSTALLED') === 'true' && restartEnabledRoutes.includes(actualRoute)) {
            const _window: any = window;
            localStorage.removeItem('UPDATE_INSTALLED')
            localStorage.setItem('LAST_RESUME', now.toISOString());
            _window.codePush.restartApplication();
        }
        localStorage.setItem('LAST_RESUME', now.toISOString());
    }

    static logOff() {

        try {
            const req = new Request();
            req.body = {
                'NRORG': localStorage.getItem('NRORG'),
                "TOKEN_EXT": localStorage.getItem('TOKEN_EXT') || ''
            };
            req.route = 'logout';
            req.callbackSuccess = function (response: any) {
                $('.sidenav').sidenav('close');
                router.push('/LogIn');
            };
            req.callbackError = function (error: any) {
                console.log('ws error:');
                console.log(error);
            };
            req.blockTouchEvents = true;
            req.send();
        } catch (e) {
            console.log(e);
        }

        try {
            localStorage.setItem('CAME_FROM_LOGOUT', 'true');
            localStorage.setItem('CAME_FROM_FORGOT_PASSWORD', 'true');
            localStorage.setItem('USER_ID', '');
            localStorage.setItem('USER_DATA', '');
            localStorage.setItem('TOKEN', '');
            localStorage.setItem('REFRESH_TOKEN', '');
            localStorage.setItem('TOKEN_EXT', '');
            localStorage.setItem('ID_EXT', '');
            localStorage.setItem('KEY_EXT', '');
            localStorage.setItem('TOTP', '');
            localStorage.setItem('GET_MODALITY_FROM_LS', '');
            localStorage.setItem('GET_CLASS_FROM_LS', '');
            $('.sidenav').sidenav('close');
        } catch (e) {
            console.log(e);
        }
    }

    static cancelLogoff() {
        this.setBackButtonBehavior(function () {
        $('.sidenav').sidenav('close');
        })
    }

    public static moneyStringToNumber(moneyString: string) {
        if (moneyString.includes('R$ '))
            return Number.parseFloat(moneyString.split('R$ ')[1].replace('.', '').replace(',', '.'));
        else throw new Error('Impossível converter valor');
    }

    static flatten(array: any) {
        var ret: any[] = [];
        for(var i = 0; i < array.length; i++) {
            if(Array.isArray(array[i])) {
                ret = ret.concat(this.flatten(array[i]));
            } else {
                ret.push(array[i]);
            }
        }
        return ret;
    }

    static isEmpty(obj: any) {
        for (var property in obj){
            if (obj.hasOwnProperty(property)) return false;
        }
        return true;
    }

    static syncCalendarSystem(saveCalendar:any) {
        saveCalendar.details += "\n\nCriado por App Minas by Zeedhi"
        const w : any = window;

        w.plugins.calendar.createEventInteractivelyWithOptions(saveCalendar.nameEvt, saveCalendar.location, 
            saveCalendar.details, saveCalendar.initialDate, saveCalendar.finalDate, {},
        (success:any) => {
            // sucesso
            console.log(success);
        },
        (error:any) => {
            //erro
            console.log(error);
        });
        
    }

    static validate_char(c: string) {
        if ((c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57)
            || (c.charCodeAt(0) >= 65 && c.charCodeAt(0) <= 90)
            || (c.charCodeAt(0) >= 97 && c.charCodeAt(0) <= 122)) {
            return true;
        } else {
            return false;
        }
    }
    
    static validate_num(c: string) {
        if (c.charCodeAt(0) >= 48 && c.charCodeAt(0) <= 57) {
            return true;
        } else {
            return false;
        }
    }
    
    static getMaskLength(mask:any) {
        return (mask.match(/_/g) || []).length;
    }
    
    static applyMask(text: string, mask: string) {
        var validChars: string[] = [];
        var maskedText = "";  

        for (var i = 0; i < text.length; i++) {
            if (this.validate_num(text.charAt(i))) {
                validChars.push(text.charAt(i));
            }
        }
        

        for (var j = 0, i = 0; j < mask.length && i < validChars.length; j++) {
            if (mask.charAt(j) == "_") { 
                    maskedText = maskedText + validChars[i];
                    i++;
            } else {
                maskedText = maskedText + mask.charAt(j);
            }

        }

        return maskedText;
      }
    
    static getValidCharacters(text: string, numeric: boolean) {
        var finalText = '';

        for (var i = 0; i < text.length; i++) {
            if (this.validate_num(text.charAt(i)) || !numeric && this.validate_char(text.charAt(i))) {
                finalText = finalText + text.charAt(i);
            }
        }
        return finalText.toUpperCase();
    }

    static removeSpecialCharacters(s: string) {
        s = s ? s.toLowerCase() : '';
        s = s.toLowerCase();
        s = s.replace(new RegExp(/\s/g),'_');
        s = s.replace(new RegExp(/[àáâãäå]/g),'a');
        s = s.replace(new RegExp(/æ/g),'ae');
        s = s.replace(new RegExp(/ç/g),'c');
        s = s.replace(new RegExp(/[èéêë]/g),'e');
        s = s.replace(new RegExp(/[ìíîï]/g),'i');
        s = s.replace(new RegExp(/ñ/g),'n');                
        s = s.replace(new RegExp(/[òóôõö]/g),'o');
        s = s.replace(new RegExp(/œ/g),'oe');
        s = s.replace(new RegExp(/[ùúûü]/g),'u');
        s = s.replace(new RegExp(/[ýÿ]/g),'y');
        s = s.replace(new RegExp(/\W/g),'');
        return s;
    }

    static syncCodePush() {
        codePush.notifyApplicationReady();
        let message = '';
        if (typeof codePush !== 'undefined') codePush.sync((status: any) => {
          if (typeof SyncStatus !== 'undefined') {
            if (status === SyncStatus.UPDATE_INSTALLED) message = 'A atualização será concluída após a aplicação ser reiniciada';
            //else if (status === SyncStatus.UP_TO_DATE) message = 'Nenhuma atualização disponível';
            //else if (status === SyncStatus.ERROR) message = 'Erro ao atualizar aplicação';
            //else if (status === SyncStatus.IN_PROGRESS) message = 'Em progresso';
            //else if (status === SyncStatus.CHECKING_FOR_UPDATE) message = 'Buscando atualizações';
            else if (status === SyncStatus.DOWNLOADING_PACKAGE) message = 'Baixando atualização';
            else if (status === SyncStatus.INSTALLING_UPDATE) message = 'Instalando atualização';
            
            if (message) M.toast({html: message, displayLength: 1500})
          }
        })
    }

}