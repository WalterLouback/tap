import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class ImageCompressor extends Vue {

    static compressImage(file: string, MAX_WIDTH: number, MAX_HEIGHT: number, format: string, response: Function) {
        var img = new Image();
        img.src = file;
        img.onload = function() {
            var canvas = document.createElement("canvas");
            var ctx: any = canvas.getContext("2d");
    
            var width  = img.width;
            var height = img.height;

            console.log(width)
            console.log(height)
    
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
    
            console.log(width)
            console.log(height)

            canvas.width = width;
            canvas.height = height;
    
            console.log(canvas)

            ctx.drawImage(img, 0, 0, width, height);
            console.log(canvas.toDataURL("image/" + format));
            response(
                canvas.toDataURL("image/" + format)
            );
        }
    }

}