import { Component, Prop, Vue } from 'vue-property-decorator';
import axios from 'axios';
import NProgress from 'nprogress';
import moment from 'moment';

@Component
export default class Request extends Vue {

    body: any = {};
    route: string = '';
    callbackSuccess: Function = new Function();
    callbackError: Function = new Function();
    callbackTimeout: Function = new Function();
    blockTouchEvents: boolean = false;
    showLoader: boolean = true;
    timeout: number = 60000;
    // defaultUrlBase: string = "http://app2.minastc.com.br/backend_taa/index.php/";
    // defaultUrlBase: string = "http://testephp7-eattake.teknisa.com/backend_taaeattake/backend/service/index.php/";
    // defaultUrlBase: string = "http://walterloubak1.zeedhi.com/workfolder/autoatendimento/backend/service/index.php/";
    // defaultUrlBase: string = "http://marcosdiniz1.zeedhi.com/workfolder/autoatendimento/backend/service/index.php/";
    // defaultUrlBase: string = "http://rayckgoncalves1.zeedhi.com/workfolder/autoatendimento/backend/service/index.php/";
       defaultUrlBase: string = "https://rc-taa.eattake.com/backend_taaeattake/index.php/";

    urlBase: string = localStorage.getItem('URL_BASE') || this.defaultUrlBase;

    private isFirstTry: boolean = true;

    // Function which makes the request
    send(): any {
        // Defines the request url
        // http://lucasmacedo.zeedhi.com/workfolder/appCostumer/generalbackend
        // https://mtc-teste.teknisa.com/generalbackend

        const urlBackEnd = this.urlBase;
        const urlReq: string = urlBackEnd + this.route;
        const self = this;
        // Spinner
        this.startLoading();
        // Post Function
        axios.post(urlReq,
            {
                'requestType': 'Row',
                'row': this.body
            },
            {
                'timeout': this.timeout
            })
            .then(function (response: any) {
                self.finishLoading();
                const requestData: any = response.data.dataset;
                self.callbackSuccess(requestData);
            })
            .catch(function (error: any) {
                self.finishLoading();
                console.log(error);
                if (error.code === 'ECONNABORTED') { self.callbackTimeout(); return; }
                else if (error && error.response && error.response.data) {
                    const errorResp = error.response.data;
                    if (errorResp.errorCode !== 42) {
                        console.log('Error:');
                        console.log(error.response.data);
                        self.callbackError(error);
                    }
                    else {
                        console.log('Generating new session...');
                        self.generateSession();
                    }
                }
                else {
                    self.callbackError(error);
                }
            })
            .then(function () {
            }
            );
    }

    startLoading() {
        if (this.showLoader) {
            NProgress.start();
            if (this.blockTouchEvents) {
                $('body').css('pointer-events', 'none');
                $('html').css('pointer-events', 'none');
            }
        }
    }

    finishLoading() {
        NProgress.done();
        if (this.blockTouchEvents) {
            $('body').css('pointer-events', 'auto');
            $('html').css('pointer-events', 'auto');
        }
    }

    generateSession() {
        console.log('GENERATING SESSION');
        if (this.isFirstTry) {
            const self = this;
            const userId: string = localStorage.getItem('USER_ID') || '';
            const refreshTKN: string = localStorage.getItem('REFRESH_TOKEN') || '';

            const req = new Request();
            req.body = {
                'USER_ID': userId,
                'REFRESH_TOKEN': refreshTKN
            };
            req.route = 'requestToken';
            req.callbackSuccess = function (response: any) {
                localStorage.setItem('TOKEN', response.user.token);
                self.remakeRequest();
            }
            req.callbackError = function (error: any) {
                console.log('ERRO SESSION');
                console.log(error);
                self.callbackError(error);
            }
            req.send();
        }
    }

    remakeRequest() {
        this.isFirstTry = false;
        if (this.body) this.body.TOKEN = localStorage.getItem('TOKEN');
        this.send();
    }

    setUrlBaseToDefault() {
        this.urlBase = this.defaultUrlBase;
        localStorage.removeItem('URL_BASE');
    }

    setUrlBase(url: string) {
        this.urlBase = url;
        localStorage.setItem('URL_BASE', url);
    }


    getStoneToken() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + 'MTY3YTQ5YWQtZDg5Zi00M2I5LWIwZTAtZGI4OTQ2MWEzOGNk' }
        });

        instance.get('/token')
            .then((response: any) => {
                localStorage.setItem('STONE_TOKEN', response.data.token);
                localStorage.setItem('STONE_TOKEN_GENERATED_DATE', JSON.stringify(moment().toISOString()));
                if (response.data.success) this.callbackSuccess();
                else this.callbackError()
            })
            .catch((error: any) => {
                this.callbackError(error);
            })
    }

    updateStoneToken(callback = (() => { })) {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + 'MTY3YTQ5YWQtZDg5Zi00M2I5LWIwZTAtZGI4OTQ2MWEzOGNk' }
        });

        instance.get('/token')
            .then((response: any) => {
                localStorage.setItem('STONE_TOKEN', response.data.token);
                localStorage.setItem('STONE_TOKEN_GENERATED_DATE', JSON.stringify(moment().toISOString()));
                callback();
            })
            .catch((error: any) => {
                this.updateStoneToken(callback);
            })
    }

    getPosReferenceId() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });

        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getPosReferenceId)
            return;
        }
        if (elapsedTime < 24) {
            instance.post('/pos/activate-pos-link', this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.getPosReferenceId)
            return;
        }
    }

    sendStonePreTransaction() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 15000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') },
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.sendStonePreTransaction)
            return;
        }
        if (elapsedTime < 24) {
            instance.post('/pre-transaction/create', this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                    //log --- Enviou com sucesso (body, response.data) para stone
                    this.crialogTransaction("--- Enviou create com sucesso "+JSON.stringify(this.body)+JSON.stringify(response.data))
                })
                .catch((error: any) => {
                    this.callbackError(error);
                      //log --- Enviou com falha (body, error) para stone
                      this.crialogTransaction("--- Enviou create com falha "+JSON.stringify(this.body)+JSON.stringify(error))
                })

        }
        else {
            this.updateStoneToken(this.sendStonePreTransaction)
            return;
        }
    }
    crialogTransaction(data:string){
        var date =new Date()
        var log = localStorage.getItem('LOG_TRANSACTION')||""
        log =log+'\n '+date.getHours()+':'+date.getMinutes()+":"+date.getSeconds()+data;
        localStorage.setItem("LOG_TRANSACTION",log)
    }

    claimCheckCreate() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') },
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.claimCheckCreate)
            return;
        }
        if (elapsedTime < 24) {
            instance.post('/claim-check/create', this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.claimCheckCreate)
            return;
        }
    }

    getStonePreTransactionStatus() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 5000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getStonePreTransactionStatus)
            return;
        }
        if (elapsedTime < 24) {
            instance.get('/pre-transaction/single/' + this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                    this.crialogTransaction("--- Buscou com sucesso "+JSON.stringify(this.body)+JSON.stringify(response.data))
                })
                .catch((error: any) => {
                    this.callbackError(error);
                    this.crialogTransaction("--- Buscou com falha "+JSON.stringify(this.body)+JSON.stringify(error))
                })
        }
        else {
            this.updateStoneToken(this.getStonePreTransactionStatus)
            return;
        }
    }

    getStoneTransactionData() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 0,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getStoneTransactionData)
            return;
        }
        if (elapsedTime < 24) {
            instance.get('/transactions/single/pre-transaction/' + this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.getStoneTransactionData)
            return;
        }
    }

    getEstablishmentData() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getEstablishmentData)
            return;
        }
        if (elapsedTime < 24) {
            instance.get('/establishment/get-single/' + this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.getEstablishmentData)
            return;
        }
    }
    
    getTransactionDate() {
        
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getEstablishmentData)
            return;
        }
        if (elapsedTime < 24) {
            instance.post('/pre-transaction/date/' , this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.getEstablishmentData)
            return;
        }
    }
    cancelPreTransaction() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.cancelPreTransaction)
            return;
        }
        if (elapsedTime < 24) {
            instance.delete('/pre-transaction/delete/' + this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                    this.crialogTransaction("--- cancelada com sucesso "+JSON.stringify(this.body)+JSON.stringify(response.data))

                })
                .catch((error: any) => {
                    this.callbackError(error);
                    this.crialogTransaction("--- cancelada com falha "+JSON.stringify(this.body)+JSON.stringify(error))

                })
        }
        else {
            this.updateStoneToken(this.cancelPreTransaction)
            return;
        }
    }

    getAvailablePos() {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;
        if (lastStoneTokenGeneratedDateString) {
            //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        }
        else {
            this.updateStoneToken(this.getAvailablePos)
            return;
        }
        if (elapsedTime < 24) {
            instance.get('/pos/get-available-pos/' + this.body)
                .then((response: any) => {
                    const requestData: any = response.data;
                    if (response.data.success) this.callbackSuccess(requestData);
                    else this.callbackError(requestData)
                })
                .catch((error: any) => {
                    this.callbackError(error);
                })
        }
        else {
            this.updateStoneToken(this.getAvailablePos)
            return;
        }
    }

    getTicketData() {
        // removendo undefined da string na primeira chamada.
        let body = (this.body.substr(0, 9) == 'undefined') ? this.body.substr(9) : this.body;
        let externalApi = (localStorage.getItem('STORE_IP') == 'null' || localStorage.getItem('STORE_IP') == 'undefined') ? null : localStorage.getItem('STORE_IP');

        const instance = axios.create({
            baseURL: externalApi ||'http://localhost:9091/odhen-eattake/backend/service/index.php',
            timeout: 8000
        });

        instance.get('/comandas/consulta?barcode=' + body).then((response: any) => {
            const requestData: any = response.data;
            if (!response.data.error) this.callbackSuccess(requestData);
            else this.callbackError(requestData);
        }).catch((error: any) => { this.callbackError(error); });

        return
    }
    saveSale(){
        const instance = axios.create({
            baseURL: 'http://localhost:9091/odhen-interface/backend/service/index.php',
            timeout: 8000
        });

        instance.post('/sale/saveDeliverySale', this.body)
            .then((response: any) => {
                console.log(response)
                const requestData: any = response.data;
                if (!response.data.error) this.callbackSuccess(requestData);
                else this.callbackError(requestData)
            })
            .catch((error: any) => {
                this.callbackError(error);
            })
        return;

    }
    danfeCreate(){
            const instance = axios.create({
                baseURL: 'https://api.siclospag.com.br/connect-split/v1',
                timeout: 8000,
                headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') },
            });
            const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
            var elapsedTime = 0;
            if (lastStoneTokenGeneratedDateString) {
                //var lastStoneTokenGeneratedDate = Date.parse("2020-08-10T13:17:53");
                elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
            }
            else {
                this.updateStoneToken(this.sendStonePreTransaction)
                return;
            }
            if (elapsedTime < 24) {
                instance.post('danfe/create', this.body)
                    .then((response: any) => {
                        const requestData: any = response.data;
                        if (response.data.success) this.callbackSuccess(requestData);
                        else this.callbackError(requestData)
                    })
                    .catch((error: any) => {
                        this.callbackError(error);
                    })
            }
            else {
                this.updateStoneToken(this.sendStonePreTransaction)
                return;
            }
        }
    

    closeOrder(){
        const instance = axios.create({
            baseURL: 'http://localhost:9091/odhen-interface/backend/service/index.php',
            timeout: 8000
        });

        instance.post('/sale/closeOrder', this.body)
            .then((response: any) => {
                console.log(response)
                const requestData: any = response.data;
                if (!response.data.error) this.callbackSuccess(requestData);
                else this.callbackError(requestData)
            })
            .catch((error: any) => {
                this.callbackError(error);
            })
        return;

    }
    getSale(){
        const instance = axios.create({
            baseURL: 'http://localhost:9091/odhen-interface/backend/service/index.php',
            timeout: 8000
        });

        instance.post('/sale/getSale', this.body)
            .then((response: any) => {
                console.log(response)
                const requestData: any = response.data;
                if (!response.data.error) this.callbackSuccess(requestData);
                else this.callbackError(requestData)
            })
            .catch((error: any) => {
                this.callbackError(error);
            })
        return;
    }

    closeTicket() {
        let externalApi = (localStorage.getItem('STORE_IP') == 'null' || localStorage.getItem('STORE_IP') == 'undefined') ? null : localStorage.getItem('STORE_IP');
        const instance = axios.create({
            baseURL: externalApi || 'http://localhost:9091/odhen-eattake/backend/service/index.php',
            timeout: 20000
        });

        instance.post('/comandas/fechamento', this.body)
            .then((response: any) => {
                console.log(response)
                const requestData: any = response.data;
                if (!response.data.error) this.callbackSuccess(requestData);
                else this.callbackError(requestData)
            })
            .catch((error: any) => {
                this.callbackError(error);
            })
        return;

    }

    getAvailablePosPrivate() {
        const self = this;
        let establishmentId = localStorage.getItem('STONE_ESTABLISHMENT_ID');

        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });
        const lastStoneTokenGeneratedDateString: string = JSON.parse(localStorage.getItem('STONE_TOKEN_GENERATED_DATE') || '[]') || "";
        var elapsedTime = 0;

        if (lastStoneTokenGeneratedDateString) {
            elapsedTime = moment.duration(moment().diff(moment(lastStoneTokenGeneratedDateString))).asHours();
        } else {
            this.updateStoneToken(this.getAvailablePosPrivate);
            return;
        }

        if (elapsedTime < 24) {
            return new Promise((resolve: Function, reject: Function) => {
                instance.get('/pos/get-available-pos/' + establishmentId)
                    .then((response: any) => {
                        const requestData: any = response.data;
                        if (response.data.success) {
                            localStorage.setItem('AVAILABLE_POS', JSON.stringify(response.data));
                            this.callbackSuccess(requestData);
                        }
                        else {
                            this.callbackError(requestData);
                        }
                    })
                    .catch((error: any) => {
                        this.callbackError(error);
                    });
            });
        } else {
            this.updateStoneToken(this.getAvailablePosPrivate);
            return;
        }
        debugger
    }

    deactivatePos(posReferenceId: string) {
        const instance = axios.create({
            baseURL: 'https://api.siclospag.com.br/connect-split/v1',
            timeout: 8000,
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('STONE_TOKEN') }
        });

        instance.put('/pos/deactivate-pos-link/' + posReferenceId)
            .then((response: any) => {
                const requestData: any = response.data;
                if (response.data.success) this.callbackSuccess(requestData);
                else this.callbackError(requestData);
            })
            .catch((error: any) => {
                this.callbackError(error);
            });
    }
}
