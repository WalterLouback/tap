import Vue from "vue";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faBell as falBell,
  faBookOpen as falBookOpen,
  faCalendar as falCalendar,
  faCalendarAlt as falCalendarAlt,
  faCheeseburger as falCheeseburger,
  faCircle as falCircle,
  faCocktail as falCocktail,
  faDonate as falDonate,
  faGlassCheers as falGlassCheers,
  faGraduationCap as falGraduationCap,
  faGripHorizontal as falGripHorizontal,
  faHomeAlt as falHomeAlt,
  faKey as falKey,
  faListUl as falListUl,
  faMapMarkerAlt as falMapMarkerAlt,
  faMapMarkerCheck as falMapMarkerCheck,
  faMoneyCheckAlt as falMoneyCheckAlt,
  faNewspaper as falNewspaper,
  faPhone as falPhone,
  faPlusCircle as falPlusCircle,
  faQrcode as falQrcode,
  faRunning as falRunning,
  faTheaterMasks as falTheaterMasks,
  faTicketAlt as falTicketAlt,
  faUser as falUser,
  faUserGraduate as falUserGraduate,
  faUsers as falUsers,
  faUtensils as falUtensils,
  faCompass as falCompass,
  faShareAlt as falShareAlt,
  faWallet as falWallet,
  faHeart as falHeart,
  faSignOutAlt as falSignOutAlt,
  faTrashAlt  as falTrashAlt,
  faShoppingCart as falShoppingCart,
  faStopwatch as falStopWatch,
  faStoreAlt as falStoreAlt,
  faDollarSign as falDollarSign,
  faLoveseat as falLoveseat,
  faBagsShopping as falBagsShopping,
  faChevronRight as falChevronRight,
  faChevronLeft as falChevronLeft,
  faStore as falStore,
  faSparkles as falSparkles,
  faEllipsisH as falEllipsisH,
  faVolumeUp as falVolumeUp,
  faBarcodeAlt as falBarcodeAlt,
  faFile as falFile,
  faUserFriends as falUserFriends,
  faRss as falRss,
  faCreditCardBlank as falCreditCardBlank,
  faTennisBall as falTennisBall,
  faStars as falStars,
  faStar as falStar, 
  faSpa as falSpa,
  faTabletAndroidAlt as falTabletAndroidAlt,
  faExclamationCircle as falExclamationCircle,
  faGlobe as falGlobe,
  faHandPointUp as falHandPointUp,
  faSpinnerThird as falSpinnerThird,
  faSearch as falSearch,
  faTimesCircle as falTimesCircle,
  faTimes as falTimes
} from "@fortawesome/pro-light-svg-icons";

import {
  faChevronDown as farChevronDown,
  faEye as farEye,
  faEyeSlash as farEyeSlash,
  faHeart as farHeart,
  faStopwatch as farStopWatch,
  faCalendar as farCalendar,
  faCocktail as farCocktail,
  faCreditCardBlank as farCreditCardBlank,
  faRss as farRss,
  faRefrigerator as farrefrigerator

} from "@fortawesome/pro-regular-svg-icons";

import {
  faArrowDown as fasArrowDown,
  faCircle as fasCircle,
  faHeart as fasHeart,
  faCompass as fasCompass,
  faUser as fasUser,
  faWallet as fasWallet,
  faChevronLeft as fasChevronLeft,
  faSignOutAlt as fasSignOutAlt,
  faBell as fasBell,
  faHomeAlt as fasHomeAlt,
  faListUl as fasListUl,
  faQrcode as fasQrcode,
  faStar as fasStar,
  faBagsShopping as fasBagsShopping,
  faStore as fasStore,
  faCocktail as fasCocktail,
  faCalendarAlt as fasCalendarAlt,
  faUsers as fasUsers,
  faSparkles as fasSparkles,
  faEllipsisH as fasEllipsisH,
  faCreditCardBlank as fasCreditCardBlank,
  faRss as fasRss,
  faFile as fasFile,
  faLongArrowAltRight as fasLongArrowAltRight,
  faWifiSlash as fasWifiSlash
} from "@fortawesome/pro-solid-svg-icons";

library.add(
  //light
  falSpinnerThird,
  falBell,
  falBookOpen,
  falCalendar,
  falCalendarAlt,
  falCheeseburger,
  falCircle,
  falCocktail,
  falDonate,
  falGlassCheers,
  falGraduationCap,
  falGripHorizontal,
  falHomeAlt,
  falKey,
  falListUl,
  falMapMarkerAlt,
  falMapMarkerCheck,
  falMoneyCheckAlt,
  falNewspaper,
  falPhone,
  falPlusCircle,
  falQrcode,
  falRunning,
  falTheaterMasks,
  falTicketAlt,
  falTicketAlt,
  falUser,
  falUserGraduate,
  falUsers,
  falUtensils,
  falCompass,
  falShareAlt,
  falWallet,
  falHeart,
  falSignOutAlt,
  falTrashAlt,
  falShoppingCart,
  falStopWatch,
  falStoreAlt,
  falLoveseat,
  falDollarSign,
  falBagsShopping,
  falChevronRight,
  falStore,
  falSparkles,
  falEllipsisH,
  falBarcodeAlt,
  falVolumeUp,
  falUserFriends,
  falFile,
  falRss,
  falCreditCardBlank,
  falTennisBall,
  falStars,
  falStar,
  falSpa,
  falChevronLeft,
  falTabletAndroidAlt,
  falExclamationCircle,
  falGlobe,
  falHandPointUp,
  falSpinnerThird,
  falSearch, 
  falTimesCircle, 
  falTimes,  
  //regular,
  farChevronDown,
  farEye,
  farEyeSlash,
  farHeart,
  farStopWatch,
  farCalendar,
  farChevronDown,
  farCocktail,
  farCreditCardBlank,
  farrefrigerator,


  //solid,
  fasArrowDown,
  fasCircle,
  fasHeart,
  fasChevronLeft,
  fasWallet,
  fasCompass,
  fasUser,
  fasSignOutAlt,
  fasBell,
  fasHomeAlt,
  fasListUl,
  fasQrcode,
  fasStar,
  fasBagsShopping,
  fasStore,
  fasCocktail,
  fasUsers,
  fasCalendarAlt,
  fasSparkles,
  fasEllipsisH,
  fasCreditCardBlank,
  fasRss,
  fasFile,
  fasLongArrowAltRight,
  fasWifiSlash
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
