<?php
$ds = DIRECTORY_SEPARATOR;

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once __DIR__.$ds.'bootstrap.php';

$entityManager = $instanceManager->getService('entityManager');

return ConsoleRunner::createHelperSet($entityManager);