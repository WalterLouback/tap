#!/bin/bash
# git fetch && git checkout feature/BIP-15-organizar-apis

#TEKFUN_DIR=$(pwd)
# AUTH_STRING="iagosilveirateknisa:a6s36yWKycuQDc2cXsuZ"
TEKFUN_DIR="/var/www/teknisa/bipfun/backend"

mkdir ${TEKFUN_DIR}/../logs
mkdir ${TEKFUN_DIR}/src/cache
cd ${TEKFUN_DIR}

# cp service/temp_3/src/Service/Auth.php src/Service/Auth.php
cp service/temp_1/check3.txt ./service/
cp service/temp_1/routes.json ./
cp service/temp_1/src/Listeners/TokenAuthentication.php ./src/Listeners/

# sudo chown -R teknisa:teknisa ${TEKFUN_DIR}
# sudo chmod 777 -R ${TEKFUN_DIR}
# sudo chmod 777 -R ${TEKFUN_DIR}/logs
# sudo chmod 777 -R ${TEKFUN_DIR}/src/cache

# echo "Downloading vendor packages"
# curl -u ${AUTH_STRING} -O -J -L http://lucasmacedo.zeedhi.com/vfs/1/9cs3gzN88L60v2h2/workspace/appCostumer/generalbackend/vendor/?download=vendor.zip -o latest_vendor_php5.zip
# unzip -q -o latest_vendor_php5.zip
# rm latest_vendor_php5.zip
# #composer global require hirak/prestissimo
# #composer install --no-progress --profile --prefer-dist

# cd ${TEKFUN_DIR}/api

# echo "Downloading apis"
# curl -u ${AUTH_STRING} -O -J -L http://lucasmacedo.zeedhi.com/vfs/1/9cs3gzN88L60v2h2/workspace/appCostumer/generalbackend/api/?download=api.zip -o apis.zip
# unzip -q -o apis.zip
# rm apis.zip

sudo chmod 755 -R ${TEKFUN_DIR}
sudo chmod 777 -R ${TEKFUN_DIR}/src/cache