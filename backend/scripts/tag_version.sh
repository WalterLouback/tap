echo Tagging version $1

git config --global user.email "guilherme.elier@teknisa.com"
git config --global user.name "Script Runner"

# Function to create version
create_version()
{
    cd $1
    git checkout $BITBUCKET_BRANCH
    git tag -a v$2 -m "Version $2"
    git push origin v$2
    git branch $2
    git checkout $2
    git merge origin/$BITBUCKET_BRANCH --no-edit
    git push --set-upstream origin $2
    cd ..
}

# Creating version from backend
create_version autoatendimento $1
