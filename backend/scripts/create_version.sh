echo Creating version $1

git config --global user.email "guilherme.elier@teknisa.com"
git config --global user.name "Script Runner"

# Function to create version
create_version()
{
    git checkout $BITBUCKET_BRANCH
    git branch $1
    git checkout $1
    git push --set-upstream origin $1
}

# Creating version from autoatendimento
cd ../..
create_version autoatendimento $1

# Returning to the autoatendimento folder
cd backend/
