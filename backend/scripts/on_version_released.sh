echo Releasing version $1

git config --global user.email "guilherme.elier@teknisa.com"
git config --global user.name "Script Runner"

# Function to create version
create_version()
{
    cd $1
    git checkout master
    git merge origin/$2 --no-edit
    git push --set-upstream origin master
    cd ..
}


# Creating version from backend
create_version autoatendimento $1
