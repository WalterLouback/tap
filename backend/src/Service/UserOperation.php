<?php
namespace Zeedhi\ApiGeneral\Service;

use Doctrine\ORM\EntityManager;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;

use Facebook\Facebook;

abstract class UserOperation {

    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;

    /**
     * UserOperation constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment = $environment;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * @return Environment
     */
    protected function getEnvironment() {
        return $this->environment;
    }

    /**
     * @return GenUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function getCurrentUser() {
        return $this->entityManager->find(GenUser::class, $this->environment->getUserId());
    }
}