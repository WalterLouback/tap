<?php
namespace Zeedhi\ApiGeneral\Service;
// Chame o arquivo com as Classes do PHPMailer
use PHPMailer;

class EmailPhpMailer {
	
	public function sendEmail($recipient, $addressRecipient, $subject, $body, $genConfiguration) {
		// Inst�ncia a classe PHPMailer
		$mail = new PHPMailer();
		
		// Configura��o dos dados do servidor e tipo de conex�o (Estes dados voc� obtem com seu host)
		$mail->IsSMTP(); // Define que a mensagem ser� SMTP
		$mail->SMTPAuth = true; // Autentica��o (True: Se o email ser� autenticado | False: se o Email n�o ser� autenticado)
		$mail->SMTPSecure = "tls";
		$mail->Host 	= $genConfiguration->getHost(); // Endere�o do servidor SMTP
		$mail->Username = $genConfiguration->getHostFromName(); // Usu�rio do servidor SMTP
		$mail->Password = $genConfiguration->getHostPassword(); // A Senha do email indicado acima
		
		// Remetente (Identifica��o que ser� mostrada para quem receber o email)
		$mail->From 	= $genConfiguration->getHostFrom();
		$mail->FromName = $genConfiguration->getHostFrom();
		$mail->Port     = 587;
		
		$arrayEmail = explode(", ",$addressRecipient);
		
		foreach($arrayEmail as $email){
			// Destinat�rio
			$mail->AddAddress($email, $recipient);
		}
	
		// Opcional (Se quiser enviar c�pia do email)
		// $mail->AddCC('copia@dominio.com.br', 'Copia'); 
		// $mail->AddBCC('CopiaOculta@dominio.com.br', 'Copia Oculta');
	
		// Define tipo de Mensagem que vai ser enviado
		$mail->IsHTML(true); // Define que o e-mail ser� enviado como HTML
	
		// Assunto e Mensagem do email
		$mail->Subject  = $subject; // Assunto da mensagem
		$mail->Body 	= $body;
		
		$this->saveLog($addressRecipient, $subject, $body);
		
		// Envia a Mensagem
		$enviado = $mail->Send();
		
		// Verifica se o email foi enviado
		if($enviado)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function saveLog($addressRecipient, $subject, $body) {
		
		$log = "[".date('m/d/Y h:i:s', time())."] " . PHP_EOL;
		$log .= "Address Recipient: $addressRecipient " . PHP_EOL;
		$log .= "Subject: $subject " . PHP_EOL;
		$log .= "Message: $body" . PHP_EOL;
		
		$currentDirectory = getcwd();
		$currentDirectory = dirname(__FILE__);
        $currentDirectory = explode('/', $currentDirectory);
        $n = 2;
        while($n >= 0) {
            array_pop($currentDirectory);
            $n--;
        }
        $currentDirectory = implode('/', $currentDirectory) . '/logs/email/';
		
	    $path = $currentDirectory;
	    $file = date('dmY', time()) . '.txt';
	    if (!file_exists($path)) {
	        mkdir($path, 0777, true);
	    }
	    file_put_contents($path.$file, $log, FILE_APPEND);
	}
	
}
?>