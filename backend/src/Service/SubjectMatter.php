<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenActivityRequest;
use Zeedhi\ApiGeneral\Model\Entities\GenSupportRequest;
use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenSubjectMatter;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;

use Doctrine\ORM\EntityManager;

class SubjectMatter extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }

    public function createSubjectMatter($userId, $name, $responsableEmail, $nrorg) {
        $subjectMatter = new GenSubjectMatter();
        $subjectMatter->setemailResponsible($responsableEmail);
        $subjectMatter->setCreatedBy($userId);
        $subjectMatter->setSubjectMatter($name);
        $subjectMatter->setStatus('A');
        $subjectMatter->setNrorg($nrorg);

        $this->getEntityManager()->persist($subjectMatter);
        $this->getEntityManager()->flush();
       
        return $subjectMatter;
    }
    
    public function removeSubjectMatter($id) {
        $subjectMatter = $this->getEntityManager()->getRepository(GenSubjectMatter::class)->find($id);
        $subjectMatter->setStatus('I');
        $this->getEntityManager()->flush();
    }
    
    public function editSubjectMatter($id, $name, $responsableEmail, $userId) {
        $subjectMatter = $this->getEntityManager()->getRepository(GenSubjectMatter::class)->find($id);
        $subjectMatter->setSubjectMatter($name);
        $subjectMatter->setemailResponsible($responsableEmail);
        $subjectMatter->setModifiedBy($userId);
        $this->getEntityManager()->flush();
        
        return $subjectMatter;
    }
}
    