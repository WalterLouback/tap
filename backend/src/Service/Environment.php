<?php
namespace Zeedhi\ApiGeneral\Service;

interface Environment {

    public function getUserId();

    public function setUserId($userId);

}