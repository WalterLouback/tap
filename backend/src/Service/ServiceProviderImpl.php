<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\Framework\Security\OAuth\Exception;

class ServiceProviderImpl implements \Zeedhi\Framework\Security\OAuth\ServiceProvider {

	private $services = array();

	public function __construct($clientId) {
		$this->service = new ServiceImpl("Zeedhi Application {$clientId}", $clientId, "SECRET");
	}

	public function findByClientAndSecretId($clientId, $clientSecret) {
		/** @var ServiceImpl $service */
		if ($this->service->getClientId() === $clientId && $this->service->getClientSecret($clientSecret)) {
			return $this->service;
		}

        throw Exception::serviceNotFound($clientId);
	}

}