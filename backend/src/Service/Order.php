<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\BpCreditcard;
use Zeedhi\ApiGeneral\Model\Entities\BpOrder;
use Zeedhi\ApiGeneral\Model\Entities\BpProductOrder;
use Zeedhi\ApiGeneral\Model\Entities\BpProduct;
use Zeedhi\ApiGeneral\Model\Entities\BpWallet;
use Zeedhi\ApiGeneral\Model\Entities\BpEvent;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction as PlatformInstantBuyTransaction;

use Doctrine\ORM\EntityManager;

class Order extends UserOperation {

    /** @var Producer */
    private $producer;
    
    /**
     * __construct
     * 
     * @param EntityManager         $entityManager
     * @param Environment           $environment
     * @param Producer              $producer
     */
    public function __construct(EntityManager $entityManager, Environment $environment, Producer $producer) {
        parent::__construct($entityManager, $environment);
        $this->producer = $producer;
    }

    /**
     * createOrder
     * 
     * @param array  $products
     * @param int $eventId
     * @param int $selectCardId
     *
     * @return BpOrder
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    private function createOrder(array $products, $eventId, $selectCardId = null) {
        $GenUser = $this->getCurrentUser();
        $bpOrder = new BpOrder();

        $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_BALANCE);
        if(isset($selectCardId)){
            /** @var BpCreditcard $bpCard */
            $bpCard = $this->getEntityManager()->find(BpCreditcard::class, $selectCardId);
            $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_CREDITCARD);
            $bpOrder->setCreditcard($bpCard);
        }
        
        $bpEvent = $this->getEntityManager()->find(BpEvent::class, $eventId);
        $bpOrder->setEvent($bpEvent);
        
        $bpOrder->setUser($GenUser);
        $this->getEntityManager()->persist($bpOrder);

        $orderTotal = 0;
        foreach ($products as $product) {
            $productId = $product['PRODUCT_ID'];
            $productQuantity = $product['QUANTITY'];
            /** @var BpProduct $bpProduct */
            $bpProduct = $this->getEntityManager()->find(BpProduct::class, $productId);

            $bpProductOrder = new BpProductOrder();
            $bpProductOrder->setOrder($bpOrder);
            $bpProductOrder->setProduct($bpProduct);
            $bpProductOrder->setQuantity($productQuantity);
            $bpProductOrder->setTotal($bpProduct->getPrice() * $productQuantity);

            $this->getEntityManager()->persist($bpProductOrder);
            $orderTotal += $bpProductOrder->getTotal();
        }

        $bpOrder->setTotal($orderTotal);
        $this->getEntityManager()->flush();
        return $bpOrder;
    }
    
    /**
     * @param integer  $selectCardId
     * @param integer $buyingValue
     *
     * @return BpOrder
     */
    public function createOrderWallet($selectCardId, $buyingValue){
        $GenUser = $this->getCurrentUser();
        /** @var BpCreditcard $bpCard */
        $bpCard = $this->getEntityManager()->find(BpCreditcard::class, $selectCardId);
        
        $bpOrder = new BpOrder();
        $bpOrder->setUser($GenUser);
        $bpOrder->setCreditcard($bpCard);
        $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_CREDITCARD_FOR_WALLET);
        $bpOrder->setTotal($buyingValue);
        $this->getEntityManager()->persist($bpOrder);
        
        $this->getEntityManager()->flush();
        return $bpOrder;
    }
    
    /**
     * processOrder
     * 
     * @param array $products
     * @param integer $eventId
     * @param integer $selectCardId
     */
    public function processOrder(array $products, $eventId, $selectCardId = null) {
        $bpOrder = $this->createOrder($products, $eventId, $selectCardId);

        $this->producer->send($bpOrder->getId());
        return $bpOrder;
    }
    
    /**
     * @param integer  $selectCardId
     * @param integer $buyingValue
     *
     * @return BpOrder
     */
    public function processOrderWallet($selectCardId, $buyingValue) {
        $bpOrder = $this->createOrderWallet($selectCardId, $buyingValue);

        $this->producer->send($bpOrder->getId());
        return $bpOrder;
    }

    /**
     * findOrderByQrCode
     * 
     * @param string $orderQrCode
     *
     * @throws Exception Order not found.
     *
     * @return BpOrder
     */
    private function findOrderByQrCode($orderQrCode) {
        $order = $this->getEntityManager()->getRepository(BpOrder::class)->findOneBy(array("qrcode" => $orderQrCode));
        if ($order === null) {
            throw Exception::orderNotFound();
        }

        return $order;
    }
    
    /**
     * finishOrder
     * 
     * @param string $orderQrCode
     */
    public function finishOrder($orderQrCode) {
        /** @var BpOrder $order */
        $order = $this->findOrderByQrCode($orderQrCode);
        if ($order->getStatus() === BpOrder::STATUS_PAID) {
            $order->setStatus(BpOrder::STATUS_USED);
            $this->getEntityManager()->flush();
        } else {
            throw Exception::invalidOrderStatus($order->getStatus());
        }
    }
}