<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenTag;
use Zeedhi\ApiGeneral\Model\Entities\GenCategory;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\EvtTagUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTagRel;
use Zeedhi\ApiGeneral\Model\Entities\GenUserCategoryRel;

use Doctrine\ORM\EntityManager;

class Tag extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getTagsFromOrganization($nrorg) {
        $tags = $this->getEntityManager()->getRepository(GenTag::class)->findBy(['nrorg' => $nrorg]);
        return $tags;
    }
    
    public function removeTagFromUser($tagId, $userId) {
        $association = $this->getEntityManager()->getRepository(EvtTagUser::class)->findOneBy(['genTag' => $tagId, 'genUser' => $userId]);
        if ($association != NULL) {
            $this->getEntityManager()->remove($association);
            $this->getEntityManager()->flush();
        }
    }
    
    public function associateTagWithUser($tagId, $userId) {
        $tag = $this->getEntityManager()->getRepository(GenTag::class)->find($tagId);
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if ($tag != NULL) {
            $association = new EvtTagUser();
            $association->setGenTag($tag);
            $association->setGenUser($user);
            
            $this->getEntityManager()->persist($association);
            $this->getEntityManager()->flush();
        }
    }
    
    public function associateCategoryWithUser($tagIds=NULL, $categoryIds=NULL, $userId, $nrorg) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $genTags = $this->getEntityManager()->getRepository(GenUserTagRel::class)->findBy(['genUser' => $userId]);
        $genCategories = $this->getEntityManager()->getRepository(GenUserCategoryRel::class)->findBy(['genUser' => $userId]);
        
        if ($genTags != NULL) {
            foreach($genTags as $genTag) {
                $this->getEntityManager()->remove($genTag);
            }
            $this->getEntityManager()->flush();
        }
        

        if ($genCategories != NULL) {
            foreach($genCategories as $genCategory) {
                $this->getEntityManager()->remove($genCategory);
            }
            $this->getEntityManager()->flush();
        }
        
        foreach($tagIds as $tagId) {
            $tag = $this->getEntityManager()->getRepository(GenTag::class)->find($tagId);

            if ($tag != NULL) {
                $association = new GenUserTagRel();
                $association->setGenTag($tag);
                $association->setStatus('A');
                $association->setGenUser($user);
                $association->setNrorg($nrorg);
                
                $this->getEntityManager()->persist($association);
                $this->getEntityManager()->flush();
            }
        }
        
        foreach($categoryIds as $categoryId) {
            $category = $this->getEntityManager()->getRepository(GenCategory::class)->find($categoryId);

            if ($category != NULL) {
                $association = new GenUserCategoryRel();
                $association->setGenCategory($category);
                $association->setStatus('A');
                $association->setGenUser($user);
                $association->setNrorg($nrorg);
                
                $this->getEntityManager()->persist($association);
                $this->getEntityManager()->flush();
            }
        }
    }
    
    public function getPreferencesFromUser($userId, $nrorg) {
        $genTags = $this->getEntityManager()->getRepository(GenUserTagRel::class)->findBy(['genUser' => $userId]);
        $genCategories = $this->getEntityManager()->getRepository(GenUserCategoryRel::class)->findBy(['genUser' => $userId]);

        $preferences = new \stdClass;
        $categories = GenUserCategoryRel::manyToArray($genCategories);
        $tags = GenUserTagRel::manyToArray($genTags);
        
        $preferences->categories = $categories;
        $preferences->tags = $tags;

        return ($preferences);
    }
    
    public function getUserTags($userId, $nrorg) {
        $tags = $this->getEntityManager()->createQuery(
            "
            SELECT t
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenTag' t
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\EvtTagUser' tu WITH tu.genTag = t
            WHERE t.nrorg = $nrorg
            AND tu.genUser = $userId
            "
        )->getResult();
        
        return $tags;
    }
    
}