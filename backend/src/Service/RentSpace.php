<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;
use Zeedhi\ApiGeneral\Model\Entities\GenRentSpaces;
use Zeedhi\ApiGeneral\Model\Entities\GenRentSpacesHasPhotos;


use Doctrine\ORM\EntityManager;

class RentSpace extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    /**
     * @param  array $findKeyByValue
     *
     * @return GenUser
     */
    public function getRentSpacesFromOrganization($structureId, $nrorg){
        $genRentSpaces    = GenRentSpaces::class;
        
        $rentSpaces = $this->getEntityManager()->createQuery(
            "
            SELECT rs
            FROM $genRentSpaces rs
            LEFT JOIN rs.genStructure st
            WHERE rs.nrorg = '$nrorg' AND rs.status = 'A'
            "
        )->getResult();
        
        // * Percorre todos os rentSpaces e inclui
        // * os unityIds de suas respectivas unidades
        
        $data = [];
        foreach($rentSpaces as $value) {
            $value->build($this->getEntityManager(), null);
            array_push($data, $value->toArray());
        }
        
        if($structureId) {
            foreach($data as $key=>$val) {
                if($val['unityId'] != $structureId)
                    unset($data[$key]);
            }
        }
        
        return $data;
    }
    
    public function getRentSpacesFromStructure($structureId, $nrorg){
        $genRentSpaces    = GenRentSpaces::class;
    
        $structureQuery   = $structureId ? "AND st.id = '$structureId'" : "";
        
        $rentSpaces = $this->getEntityManager()->createQuery(
            "
            SELECT rs
            FROM $genRentSpaces rs
            LEFT JOIN rs.genStructure st
            WHERE rs.nrorg = '$nrorg' AND rs.status = 'A'
            $structureQuery
            "
        )->getResult();
        
        foreach($rentSpaces as $value) {
            $value->build($this->getEntityManager(), null);
        }
        
        return $rentSpaces;
    }
    
    /**
     * @param  array $findKeyByValue
     *
     * @return GenUser
     */
    public function getRentSpacesById(array $findKeyByValue){
        $rentSpacesRepository = $this->getEntityManager()->getRepository(GenRentSpaces::class)->findOneBy($findKeyByValue) ;
        $rentSpacesRepository->build($this->getEntityManager());
        
        return $rentSpacesRepository;
    }
    
    public function getGenStructure(array $findKeyByValue){
        $structure = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy($findKeyByValue) ;
        
        return $structure;
    }
    
    public function addRentSpace($userId, $nrorg, $name, $description, $maximumPersonCapacity, $linkTourVirtual, $status, $structure, $photos, $phone, $address) {
        $rentSpaces = new GenRentSpaces();
        
        if($nrorg)
            $rentSpaces->setNrorg($nrorg);
        if($userId)
            $rentSpaces->setCreatedBy($userId);
        if($userId)
            $rentSpaces->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        if($name)
            $rentSpaces->setName($name);
        if($description)
            $rentSpaces->setDescription($description);
        if($maximumPersonCapacity)
            $rentSpaces->setMaximumPersonCapacity($maximumPersonCapacity);
        if($linkTourVirtual)
            $rentSpaces->setLinkTourVirtual($linkTourVirtual);
        if($status)
            $rentSpaces->setStatus($status);
        if($structure)
            $rentSpaces->setGenStructure($structure);
        if($phone)
            $rentSpaces->setTelephone($phone);
        if($address)
            $rentSpaces->setAddress($address);
        
        $this->getEntityManager()->persist($rentSpaces);
        
        return $rentSpaces;
    }
    
    public function addPhotos($rentSpaces, $userId, $link) {
        $rentSpacesHasPhotos = new GenRentSpacesHasPhotos();
        
        $rentSpacesHasPhotos->setRentSpaces($rentSpaces);
        $rentSpacesHasPhotos->setCreatedBy($userId);
        $rentSpacesHasPhotos->setLink($link);
        
        $this->getEntityManager()->persist($rentSpacesHasPhotos);
        
        return $rentSpacesHasPhotos;
    }
    
    public function updateRentSpace($rentSpaceId, $userId, $nrorg, $name, $description, $maximumPersonCapacity, $linkTourVirtual, $status, $structure, $photos, $phone, $address) {
        $rentSpaces = $this->getEntityManager()->getRepository(GenRentSpaces::class)->find($rentSpaceId);
        
        if($nrorg)
            $rentSpaces->setNrorg($nrorg);
        if($userId)
            $rentSpaces->setModifiedBy($userId);
        if($userId)
            $rentSpaces->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        if($name)
            $rentSpaces->setName($name);
        if($description)
            $rentSpaces->setDescription($description);
        if($maximumPersonCapacity)
            $rentSpaces->setMaximumPersonCapacity($maximumPersonCapacity);
        if($linkTourVirtual)
            $rentSpaces->setLinkTourVirtual($linkTourVirtual);
        if($status)
            $rentSpaces->setStatus($status);
        if($structure)
            $rentSpaces->setGenStructure($structure);
        if($phone)
            $rentSpaces->setTelephone($phone);
        if($address)
            $rentSpaces->setAddress($address);
        
        $this->getEntityManager()->persist($rentSpaces);
        
        return $rentSpaces;
    }
    
    public function updatePhotos($rentSpaces, $userId, $link) {
        $rentSpacesHasPhotos = $this->getEntityManager()->getRepository(GenRentSpacesHasPhotos::class)->findOneBy(['rentSpaces' => $rentSpaces]);
        
        $rentSpacesHasPhotos->setRentSpaces($rentSpaces);
        $rentSpacesHasPhotos->setModifiedBy($userId);
        $rentSpacesHasPhotos->setLink($link);
        
        
        $this->getEntityManager()->persist($rentSpacesHasPhotos);
        
        return $rentSpacesHasPhotos;
    }
    
    public function deletePhotos($rentSpaceId) {
        $rentSpacesHasPhotos = $this->getEntityManager()->getRepository(GenRentSpacesHasPhotos::class)->findBy(['rentSpaces' => $rentSpaceId]);
        
        foreach ($rentSpacesHasPhotos as $photo) {
            $this->getEntityManager()->remove($photo);
        }
        
        $this->commit();
    }
    
    public function commit() {
        $this->getEntityManager()->flush();
    }
    
    public function getUserData($userId, $userTypeId, $nrorg) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->build($this->getEntityManager(), $userTypeId, $nrorg);
        
        return $user;
    }
    
    public function getOrganization($nrorg) {
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
}