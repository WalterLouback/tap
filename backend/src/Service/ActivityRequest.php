<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenActivityRequest;
use Zeedhi\ApiGeneral\Model\Entities\GenSupportRequest;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;

use Doctrine\ORM\EntityManager;

class ActivityRequest extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getActivityRequestByNrorgAndSupportRequest($nrorg, $supportRequestId) {
        return $this->getEntityManager()->getRepository(GenActivityRequest::class)->findBy(['nrorg' => $nrorg, 'genSupportRequest' => $supportRequestId]);
    }
    
    public function createActivityRequest($nrorg, $userId, $type = NULL, $description = NULL, $file = NULL, $status = NULL, $supportRequestId = NULL, $activityRequestOldId = NULL) {
        
        $activityRequest = new GenActivityRequest();
        
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if($supportRequestId != NULL) {
            $genSupportRequest = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
            $activityRequest->setGenSupportRequest($genSupportRequest);
        }
        
        if($activityRequestOldId != NULL) {
            $genActivityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestOldId);
            $activityRequest->setGenActivityRequest($genActivityRequest);
        }
        
        $activityRequest->setNrorg($nrorg);
        $activityRequest->setGenUser($genUser);
        $activityRequest->setType($type);
        $activityRequest->setDescription($description);
        $activityRequest->setFile($file);
        $activityRequest->setStatus($status);
        
        $this->getEntityManager()->persist($activityRequest);
        $this->getEntityManager()->flush();
    }
    
    public function updateActivityRequest($activityRequestId, $nrorg, $userId, $type = NULL, $description = NULL, $file = NULL, $status = NULL, $supportRequestId = NULL, $activityRequestOldId = NULL) {
        
        $activityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestId);
        
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if($supportRequestId != NULL) {
            $genSupportRequest = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
            $activityRequest->setGenSupportRequest($genSupportRequest);
        }
        
        if($activityRequestOldId !== NULL) {
            $genActivityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestOldId);
            $activityRequest->setGenActivityRequest($genActivityRequest);
        }
        
        $activityRequest->setNrorg($nrorg);
        $activityRequest->setGenUser($genUser);
        $activityRequest->setType($type);
        $activityRequest->setDescription($description);
        $activityRequest->setFile($file);
        $activityRequest->setStatus($status);
        
        $this->getEntityManager()->persist($activityRequest);
        $this->getEntityManager()->flush();
    }
    
    public function cancellationActivityRequest($activityRequestId, $userId, $nrorg) {
        
        $activityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->findOneBy(['id' => $activityRequestId, 'nrorg' => $nrorg]);
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $activityRequest->setStatus("I");
        $activityRequest->setGenUser($genUser);
        
        $this->getEntityManager()->persist($activityRequest);
        $this->getEntityManager()->flush();
    }
    
    public function getActivityRequestDescending ($nrorg, $supportRequestId) {
        $supportClass     = GenActivityRequest::class;
        $supportRequests = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $supportClass s
            WHERE s.genSupportRequest = $supportRequestId
            AND s.nrorg = $nrorg
            AND (s.status <> 'I' or s.status is null)
            ORDER BY s.createdAt
            DESC
            "
        )->getResult();
        return $supportRequests;
    }
    
    public function getFirstActivityInformation ($nrorg, $supportRequestId) {
        $supportClass     = GenActivityRequest::class;
        $supportRequests = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $supportClass s
            WHERE s.genSupportRequest = $supportRequestId
            AND s.nrorg = $nrorg
            AND (s.status <> 'I' or s.status is null)
            order by s.createdAt
            "
        );
        $supportRequests->setFirstResult(0);
        $supportRequests->setMaxResults(1);
        $data = $supportRequests->getResult();
        return $data;
    }
}
    