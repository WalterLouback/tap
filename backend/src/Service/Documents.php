<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenDocument;
use Zeedhi\ApiGeneral\Model\Entities\GenDocUserRel;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;

use Doctrine\ORM\EntityManager;

class Documents extends UserOperation {

     public function __construct(EntityManager $entityManager, Environment $environment) {
          parent::__construct($entityManager, $environment);
     }

     public function uploadDocument($nrorg, $userId, $type, $file) {
          $document = new GenDataUpdate();

          $document->setGenDocument($this->getEntityManager()->getReference(GenDocument::class, $type));
          $document->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
          $document->setUpdateStatus('P');
          $document->setDocument($file);
          $document->setNrorg($nrorg);
          $document->setCreatedBy($userId);

          $this->getEntityManager()->persist($document);
          $this->getEntityManager()->flush();
          
          return $document;
     }

     public function getDocType($nrorg) {
          return $this->getEntityManager()->getRepository(GenDocument::class)->findBy(['nrorg' => $nrorg, 'status' => 'A']);
     }

     public function getDocuments($nrorg, $userId) {
          $document = GenDocument::class;
          $dataUpdate = GenDataUpdate::class;
          $genUser = GenUser::class;
          
          $users = $this->getEntityManager()->createQuery(
               "
               SELECT dur.document, dur.updateStatus as status, doc.name, doc.type, doc.expirationDate, doc.responsibleEmail, dur.createdAt
               FROM $dataUpdate dur
               LEFT JOIN $document doc WITH dur.genDocument = doc.id
               LEFT JOIN $genUser gu WITH dur.genUser = gu.id
               WHERE dur.nrorg = $nrorg
               AND dur.genUser = $userId
               "
          )->getResult();
          return $users;
     }

     public function createDocument($expDate, $name, $status, $type, $nrorg, $userId, $email) {
          $document = new GenDocument();

          $document->setExpirationDate($expDate);
          $document->setName($name);
          $document->setStatus($status);
          $document->setType($type);
          $document->setNrorg($nrorg);
          $document->setCreatedBy($userId);
          $document->setResponsibleEmail($email);
          $this->getEntityManager()->persist($document);
          $this->getEntityManager()->flush();
          
          return $document->getId();
     }

     public function editDocument($id, $expDate, $name, $status, $type, $nrorg, $userId, $email) {
          $document = $this->getEntityManager()->getRepository(GenDocument::class)->find($id);

          $document->setExpirationDate($expDate);
          $document->setName($name);
          $document->setStatus($status);
          $document->setType($type);
          $document->setNrorg($nrorg);
          $document->setResponsibleEmail($email);
          $document->setModifiedBy($userId);

          $this->getEntityManager()->flush();
     }

     public function removeDocument($id) {
          $document = $this->getEntityManager()->getRepository(GenDocument::class)->find($id);

          $document->setStatus('I');

          $this->getEntityManager()->flush();
     }
     
     public function getUserById($userId) {
          return $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
     }
     
     public function getGenConfiguration($nrorg) {
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
}