<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenStructureType;
use Zeedhi\ApiGeneral\Model\Entities\GenAddress;
use Zeedhi\ApiGeneral\Model\Entities\EvtEvent;

use Doctrine\ORM\EntityManager;

class Structure extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    function getStructures($nrorg, $parentId, $event, $level, $initial=NULL, $max=NULL, $type=NULL) {
        $structure = 'Zeedhi\ApiGeneral\Model\Entities\GenStructure';
        
        $parentQuery = $parentId !== NULL ? "AND s.parent = $parentId" : "";
        $levelQuery  = $level    !== NULL ? "AND s.level = $level" : "";
        $eventQuery  = $event    !== NULL ? "AND s.evtEvent = $event" : "";
        $typeQuery   = $type     !== NULL ? "AND s.type = '$type'" : "";
        $nrorgQuery  = $nrorg     !== NULL ? "AND s.nrorg = '$nrorg'" : "";
            
        
        $structures  = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $structure s
            WHERE s.status = 'A'
            $nrorgQuery
            $parentQuery
            $levelQuery
            $eventQuery
            $typeQuery
            ORDER BY s.id
            "
        );
        
        if ($initial !== NULL) $structures->setFirstResult($initial);
        if ($max !== NULL) $structures->setMaxResults($max);
        
        $structures = new \Doctrine\ORM\Tools\Pagination\Paginator($structures);
        
        foreach ($structures as $structure) {
            $structure->build($this->getEntityManager());
        }
        
        return $structures;
    }
    
    function getStructureById($structureId, $nrorg) {
        $structure = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg]);
        if ($structure == NULL) throw new Exception('Structure not found', 1);
        
        $structure->build($this->getEntityManager());
        
        return $structure;
    }
    
    function getGenAddressById($addressId, $nrorg) {
        $genAddress = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['id' => $addressId, 'nrorg' => $nrorg]);

        return $genAddress;
    }
    
    public function createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg, $latitude, $longitude) {
        $structureAddress = new GenAddress();
        $structureAddress->setCreatedBy($userId);
        $this->buildGenAddress($structureAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg, $latitude, $longitude);
        $this->getEntityManager()->persist($structureAddress);
        $this->getEntityManager()->flush();
        
        return $structureAddress;
    }
    
    public function buildGenAddress($genAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg, $latitude, $longitude) {
        $genAddress->setStreet($street);
        $genAddress->setNumber($number);
        $genAddress->setNeighborhood($neighborhood);
        $genAddress->setCity($city);
        $genAddress->setProvincy($provincy);
        $genAddress->setCep($cep);
        $genAddress->setNrorg($nrorg);
        $genAddress->setLatitude($latitude);
        $genAddress->setLongitude($longitude);
    }
    
    public function updateGenAddress($genAddressId, $nrorg, $street, $number, $neighborhood, $city, $provincy, $cep, $userId, $latitude, $longitude) {
        $genAddress = $this->getGenAddressById($genAddressId, $nrorg);
        $genAddress->setModifiedBy($userId);
        $this->buildGenAddress($genAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg, $latitude, $longitude);
        $this->getEntityManager()->flush();
        return $genAddress;
    }
    
    public function updateStructure($structureId, $nrorg, $name, $description, $parent, $structureLevel, $evtEventId, $type, $status, $userId, $genAddress, $email = NULL, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId) {
        $genStructure = $this->getStructureById($structureId, $nrorg);
        $genStructure->setModifiedBy($userId);
        if($parent)   $parent = $this->getStructureById($parent, $nrorg); 
        $this->buildGenStructure($genStructure, $name, $nrorg, $description, $type, $status, $parent, $structureLevel, $evtEventId, $genAddress, $email, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId);
        $this->getEntityManager()->flush();
        return true;
    }
    
    public function buildGenStructure($structure, $name, $nrorg, $description, $type, $status, $parentId, $structureLevel, $evtEventId, $genAddress, $email = NULL, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId) {
        $structure->setName($name);
        $structure->setEmail($email);
        $structure->setNrorg($nrorg);
        $structure->setDescription($description);
        $structure->setType($type);
        $structure->setStatus($status);
        $structure->setGenAddress($genAddress);
        if ($structureLevel !== NULL) $structure->setLevel($structureLevel);
        if ($parentId != NULL) $structure->setParent($parentId);
        $structure->setMaxpersons($maxpersons); 
        $structure->setCheckAprove($checkaprove); 
        $structure->setmaxarea($maxarea); 
        $structure->setrules($rules);
        $structure->setimage($image); 
        $structure->setEvtEvent($evtEventId);
        $structure->setstructureTypeId($structureTypeId);
    }
    
    public function createStructure($name, $nrorg, $description, $type, $userId, $parent = NULL, $genAddress = NULL, $structureLevel, $evtEventId = NULL, $email = NULL, $maxpersons = NULL, $checkaprove = NULL, $maxarea = NULL, $rules = NULL, $image = NULL, $structureTypeId = NULL) {
        $structure = new GenStructure();
        $structure->setCreatedBy($userId);
        if($parent)   $parent = $this->getStructureById($parent, $nrorg); 
        $this->buildGenStructure($structure, $name, $nrorg, $description, $type, 'A', $parent, $structureLevel, $evtEventId, $genAddress, $email, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId);
        $this->getEntityManager()->persist($structure);
        $this->getEntityManager()->flush();
        
        return $structure;
    }
    
    public function inactivateStructure($structureId, $nrorg) {
        $genStructure = $this->getStructureById($structureId, $nrorg);
        $genStructure->setStatus('I');
        $this->getEntityManager()->flush();
        return $genStructure;
    }
    
    public function getPlacesStructures($nrorg, $type) {
        $structures = $this->getEntityManager()->getRepository(GenStructure::class)->findBy(['nrorg' => $nrorg, 'type' => $type]);
        
        return $structures;
    }
    
    // public function getPlacesStructures($parent, $nrorg, $type) {
    //     $structures = $this->getEntityManager()->getRepository(GenStructure::class)->findBy(['parent' => $parent, 'nrorg' => $nrorg, 'type' => $type]);
        
    //     foreach ($structures as $structure) {
    //         // $structure->setChildren($this->getPlacesStructures($structure->getId(), $nrorg, $type));
    //         $structures = array_merge($structures, $this->getPlacesStructures($structure->getId(), $nrorg, $type));
    //     }
        
    //     return $structures;
    // }


    //functions related to the type of space
    function getStructureTypeById($structureTypeId, $nrorg) {
        $structureType = $this->getEntityManager()->getRepository(GenStructureType::class)->findOneBy(['id' => $structureTypeId, 'nrorg' => $nrorg]);
        if ($structureType == NULL) throw new Exception('StructureType not found', 1);
        
        $structureType->build($this->getEntityManager());
        
        return $structureType;
    }
    
    public function createStructureType($name, $nrorg, $userId) {
        $structuretype = new GenStructureType();
        $structuretype->setCreatedBy($userId);
        $structuretype->setName($name);
        $structuretype->setNrorg($nrorg);
        $structuretype->setStatus('A');
        $this->getEntityManager()->persist($structuretype);
        $this->getEntityManager()->flush();
        return $structuretype;
    }
    
    public function updateStructureType($name, $nrorg, $userId, $structureTypeId) {
        $structuretype = $this->getStructureTypeById($structureTypeId, $nrorg);
        $structuretype->setModifiedBy($userId);
        $structuretype->setName($name);
        $structuretype->setNrorg($nrorg);
        $this->getEntityManager()->persist($structuretype);
        $this->getEntityManager()->flush();
        return true;
    }    
    
    public function inactivateStructureType($structureTypeId, $nrorg) {
        $genStructure = $this->getStructureTypeById($structureTypeId, $nrorg);
        $genStructure->setStatus('I');
        $this->getEntityManager()->flush();
        return $genStructure;
    } 
    
    function getStructuresType($nrorg) {
        $structureType = 'Zeedhi\ApiGeneral\Model\Entities\GenStructureType';
        
        $structures  = $this->getEntityManager()->createQuery("
            SELECT s
            FROM $structureType s
            WHERE s.nrorg = $nrorg
            and s.status != 'I'
            ORDER BY s.id
        ");
        
        $structures = new \Doctrine\ORM\Tools\Pagination\Paginator($structures);
        
        foreach ($structures as $structure) {
            $structure->build($this->getEntityManager());
        }
        
        return $structures;
    }
        
}