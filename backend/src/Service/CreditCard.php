<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\BpCreditcard;

use Zeedhi\PaymentPlatform\Customer;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\CreditCard as PlatformCreditCard;

use Zeedhi\Framework\ORM\DateTime;

use Doctrine\ORM\EntityManager;

class CreditCard extends UserOperation {

    /** @var InstantBuyPlatform */
    private $paymentPlatform;

    /**
     * @param EntityManager       $entityManager
     * @param Environment         $environment
     * @param InstantBuyPlatform  $paymentPlatform
     */
    public function __construct(EntityManager $entityManager, Environment $environment, InstantBuyPlatform $paymentPlatform) {
        parent::__construct($entityManager, $environment);
        $this->paymentPlatform = $paymentPlatform;
    }

    private function discoverFlag($numbers) {
        $masterPrefix = ['51','52','53','54','55'];
        switch (true) {
            case substr($numbers, 0, 1) === '4':
                return BpCreditcard::FLAG_VISA;
                break;
            case in_array(substr($numbers, 0, 2), $masterPrefix):
                return BpCreditcard::FLAG_MASTERCARD;
            default:
                throw Exception::unsupportedCreditCardFlag();
                break;
        }
    }
    
    public function addNewCreditCard($ownerName, $numbers, $expirationDateMonth, $expirationDateYear, $cvv) {
        $flag = $this->discoverFlag($numbers);
        $platformCreditCard = new PlatformCreditCard($ownerName, $numbers, $expirationDateMonth, $expirationDateYear, $cvv, $flag);
        /*$token = $this->paymentPlatform->createToken($platformCreditCard);*/
        $customer = new Customer();
        $customer->setName($ownerName);
        $customer->setId('cus_3Z4D3jqCYgC9Aa6k');
        $token = $this->paymentPlatform->createToken($platformCreditCard, $customer);
        $lastNumbers = substr($numbers, 12, 4);
        $bpCreditCard = new BpCreditCard();
        $expirationDate = DateTime::createFromFormat("m/Y", $expirationDateMonth."/".$expirationDateYear);
        $bpCreditCard->setExpirationDate($expirationDate);
        $bpCreditCard->setFlag($flag);
        $bpCreditCard->setLastNumbers($lastNumbers);
        $bpCreditCard->setToken($token['InstantBuyKey']);
        $bpCreditCard->setUser($this->getCurrentUser());
        $this->getEntityManager()->persist($bpCreditCard);
        $this->getEntityManager()->flush();

        return $bpCreditCard;
    }
}