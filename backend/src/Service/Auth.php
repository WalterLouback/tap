<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenContact;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\GenAddress;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiGeneral\Model\Entities\PayWallet;
use Zeedhi\ApiGeneral\Model\Entities\GenEditableFields;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;
use Zeedhi\ApiGeneral\Model\Entities\EvtEventSeller;

use Zeedhi\ApiGeneral\Service\EmailPhpMailer as emailPhpMailerrService;

use Zeedhi\ApiGeneral\Service\SMTP;
use PHPMailer;
use SMTP as smtpMailer;
use Exception as ExceptionPHPMailer;

use Doctrine\ORM\EntityManager;
use Facebook\Facebook;
use Facebook\Authentication\AccessToken;

use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\Cache\Type\SessionImpl;
use Zeedhi\Framework\Cache\Type\MemcachedImpl;
use Zeedhi\Framework\Cache\Type\FileImpl;

use Zeedhi\Framework\Security\OAuth\OAuthImpl;
use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\Session\Storage\NativeSession;
use Zeedhi\Framework\Session\Attribute\SimpleAttribute;

use Zeedhi\ApiGeneral\Helpers\ReplaceDql;

class Auth extends UserOperation {

    const SALT = 'BIPPOPSALT';
    const REGISTRATION_COMPLETE = 'A';
    
    /** @var Facebook */
    private $facebook;

    /**
     * Auth constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     * @param Facebook $facebook
     */
    public function __construct(EntityManager $entityManager, Environment $environment, Facebook $facebook, SMTP $smtpService) {
        parent::__construct($entityManager, $environment);
        $this->facebook = $facebook;
        $this->smtpService = $smtpService;
    }

    /**
     * cryptPassword
     * Encrypt SALT and password string with sha512
     * 
     * @param String $password
     * 
     * @return String
     */
    public function cryptPassword($password) {
        //@toDo Remover StrToUpper
        return strtoupper(hash('sha512', self::SALT.$password));
    }

    /**
     * populateEnvironment
     * Set User params to session with Environment
     * 
     * @param GenUser $user
     */
    private function populateEnvironment(GenUser $user) {
        $this->getEnvironment()->setUserId($user->getId());
    }
    
    /**
     * @param  array $findKeyByValue
     *
     * @return GenUser
     */
    public function getGenUser(array $findKeyByValue){
        $userRepository = $this->getEntityManager()->getRepository(GenUser::class);
        
        return $userRepository->findOneBy($findKeyByValue);
    }
    
    public function getGenUserByCpfOrExternalId($nrorg, $cpf = null, $npf = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        
        $queryCpf       =  $cpf != NULL ? " u.cpf = '$cpf' AND u.cpf <> ''" : "";
        $queryNpf       =  $npf != NULL ? " utr.externalId = '$npf' AND utr.externalId <> '' AND utr.nrorg = '$nrorg'" : "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u 
            FROM $genUser u 
            JOIN $genUserType utr WITH utr.genUser = u.id 
            WHERE $queryCpf $queryNpf 
            AND u.status = 'A' 
            AND utr.status = 'A'
            "
        )->getResult();
        return $users;
    }
    
    public function getGenUsersByPhone($nrorg, $phone) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $genContact     = GenContact::class;
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u 
            FROM $genUser u 
            JOIN $genUserType utr WITH utr.genUser = u.id 
            JOIN $genContact c WITH c.phone = $phone
            WHERE u.status = 'A' 
            AND utr.status = 'A'
            "
        )->getResult();
        return $users;
    }

    /**
     * login
     * Log in the user and populate Environment
     *
     * @param string $email
     * @param string $password
     *
     * @return GenUser
     *
     * @throws Exception
     */
    public function login($nrorg, $npf, $cpf, $password, $phone = null) {
        $cryptedPassword = $this->cryptPassword($password);

        if ($npf != null) {
            $users = $this->getActiveUserByExternalId($nrorg, $npf);
        } else if ($phone != null) {
            $userPhone = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['type' => 'MOBILE', 'phone' => $phone]);
            $users = ($userPhone) ? [ $userPhone->getGenUser() ] : [];
        } else {
            $users = $this->getGenUserByCpfOrExternalId($nrorg, $cpf, $npf);
        }
        
        $user = count($users) > 0 ? $users[0] : NULL;
        
        if (!isset($user) || $user == null) {
            throw Exception::loginNotFound();
        } else if (!$phone && $user->getPassword() != $cryptedPassword) {
            throw Exception::incorrectPassword();
        }
        
        $user->build($this->getEntityManager());
        $userId   = $user->getId();
            
        $clientId = "USER_ID_$userId";
        $token    = $this->generateAccessToken($clientId);
        
        $user->setToken($token);
        $this->getEntityManager()->flush();
        
        return $user;
    }    
    
    public function authenticateUser($email, $phone, $cpf, $password) {
        $genUser        = GenUser::class;
        $genContact     = GenContact::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $cpfQuery       =  $cpf != NULL ? "AND REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        $emailQuery = $email !== NULL ? "AND u.email = '$email'" : "";
        $phoneQuery = $phone !== NULL ? "AND c.phone = '$phone'" : "";
        $cryptedPassword = $this->cryptPassword($password);
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM $genUser u
            LEFT JOIN $genContact c WITH c.genUser = u
            WHERE u.status = 'A'
            AND u.password = '$cryptedPassword'
            $emailQuery
            $cpfQuery
            $phoneQuery
            "
        )->getResult();
        
        if (count($users) > 0) {
            $user     = $users[0];
            
            $user->build($this->getEntityManager());
                
            $clientId = "USER_ID_" . $user->getId();
            $token    = $this->generateAccessToken($clientId);
            
            $user->setToken($token);
            $this->getEntityManager()->flush();
            
            return $user;
        } else return NULL;
    }
    
    public function authenticateUserV1($email, $phone, $cpf, $password, $nrorg) {
        $userTypeRel    = GenUserTypeRel::class;
        $genUser        = GenUser::class;
        $genContact     = GenContact::class;
        $replaceDql     = ReplaceDql::class;

        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $cpfQuery           = $cpf != NULL ? "AND REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        $emailQuery         = $email !== NULL ? "AND u.email = '$email'" : "";
        $phoneQuery         = $phone !== NULL ? "AND c.phone = '$phone'" : "";
        $cryptedPassword    = $this->cryptPassword($password);
        
        if($nrorg !== NULL && $nrorg != 0) {
            $userTypeRelQuary   = "JOIN $userTypeRel utr WITH utr.nrorg = '$nrorg' AND u.id = utr.genUser";
            $passwordQuery      = "AND (utr.password = '$cryptedPassword' OR u.password = '$cryptedPassword')";
            $typerel            = "AND utr.genUserType = '1'";
        } else {
            $userTypeRelQuary   = "";
            $passwordQuery      = "AND u.password = '$cryptedPassword'";
            $typerel            = "";
        }
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM $genUser u
            LEFT JOIN $genContact c WITH c.genUser = u
            $userTypeRelQuary
            WHERE u.status = 'A'
            $passwordQuery
            $emailQuery
            $cpfQuery
            $phoneQuery
            $typerel
            "
        )->getResult();
        
        if (count($users) > 0) {
            $user     = $users[0];
            
            $user->build($this->getEntityManager());
            
            $clientId = "USER_ID_" . $user->getId();
            $token    = $this->generateAccessToken($clientId);
            
            $user->setToken($token);
            $this->getEntityManager()->flush();
            
            return $user;
        } else return NULL;
    }
    
    public function authenticateUserWithExternalMethod($uid, $nrorg) {
        $userTypeRel    = GenUserTypeRel::class;
        $genUser        = GenUser::class;
        $genContact     = GenContact::class;
        $replaceDql     = ReplaceDql::class;

        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $uidQuery         = "AND u.firebaseId = '$uid'";
        
        
        if($nrorg !== NULL && $nrorg != 0) {
            $userTypeRelQuary   = "JOIN $userTypeRel utr WITH utr.nrorg = '$nrorg' AND u.id = utr.genUser";
            $typerel            = "AND utr.genUserType = '1'";
        } else {
            $userTypeRelQuary   = "";
            $typerel            = "";
        }
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM $genUser u
            LEFT JOIN $genContact c WITH c.genUser = u
            $userTypeRelQuary
            WHERE u.status = 'A'
            $uidQuery
            $typerel
            "
        )->getResult();
        
        if (count($users) > 0) {
            $user     = $users[0];
            
            $user->build($this->getEntityManager());
            
            $clientId = "USER_ID_" . $user->getId();
            $token    = $this->generateAccessToken($clientId);
            
            $user->setToken($token);
            $this->getEntityManager()->flush();
            
            return $user;
        } else return NULL;
    }
    
    public function loginWB($nrorg, $npf = null, $cpf = null) {
        if($npf != null) {
            $users = $this->getActiveUserByExternalId($nrorg, $npf);
        }else {
            $users = $this->getGenUserByCpfOrExternalId($nrorg, $cpf, $npf);
        }
        
        $user = count($users) > 0 ? $users[0] : NULL;
        
        if (!isset($user) || $user == null)
            throw Exception::loginNotFound();
        
        $user->build($this->getEntityManager());
        $userId   = $user->getId();
            
        $clientId = "USER_ID_$userId";
        $token    = $this->generateAccessToken($clientId);
        
        $user->setToken($token);
        $this->getEntityManager()->flush();
        
        return $user;
    }
    
    public function getGenUserTypeRel($userId, $genUserTypeId, $nrorg) {
        if ($nrorg == 0) $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $genUserTypeId, 'status' => 'A']);
        else $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $genUserTypeId, 'nrorg' => $nrorg, 'status' => 'A']);
        return $userTypeRel;
    }
    
    public function getEvtEventSeller($userId) {
       $evtSellerType = $this->getEntityManager()->createQuery(
            "
            SELECT ee.id FROM " . EvtEventSeller::class . " ees 
            JOIN ees.evtEvent ee 
            WHERE ees.genUser = '$userId' AND ees.status = 'A'
            "
        )->getResult();
        return $evtSellerType && $evtSellerType[0] ? $evtSellerType[0]["id"] : null;
    }
    
    public function getGenUserTypeRelAdmin($userId, $genUserTypeId, $nrorg) {
        if ($nrorg == 0) $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $genUserTypeId, 'status' => 'A']);
        else $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $genUserTypeId, 'nrorg' => $nrorg, 'status' => 'A']);
        return $userTypeRel;
    }
    
    private function setupOAuth($clientId) {
		$storage = new FileImpl(__DIR__ . "/../cache", ".txt"); 
        $this->serviceProvider = new ServiceProviderImpl($clientId);
        
        $lifeTime = 10 * 60; //life time in seconds
		$this->oauth = new OAuthImpl($this->serviceProvider, $storage, time() + $lifeTime);
        
		$this->clientSecret = "SECRET";
    }
    
    public function generateAccessToken($clientId) {
		$token = "TK_$clientId";
        return $token;
    }
    
    public function checkAccessToken($token, $clientId) {
		$service = $this->oauth->checkAccess($token, $this->clientSecret);
		if ($clientId != $service->getClientId()) {
	        throw Exception::unauthorizedAction();
		}
    }
    
    /**
     * @param  string $facebookToken
     *
     * @return array FacebookUser
     */
    private function getFacebookUserData($facebookToken) {
        $response = $this->facebook->get('/me?fields=id,name,email,picture.width(700).height(700)', $facebookToken);
        return $response->getGraphUser();
    }

    /**
     * @param  object  $facebookToken
     * @param  string $facebookToken
     * @param  GenUser $user
     *
     * @return GenUser $user
     */
    private function setFacebookUserData($facebookUser, $facebookToken, GenUser $user, $facebookTokenExpiration = null){
        if($user->getId() === null){
            $this->createWallet($user);
        }
        if($facebookTokenExpiration != null){
            $user->setFacebookTokenExpDate($facebookTokenExpiration);
        }
        if ($user->getEmail() == NULL) $user->setEmail($facebookUser["email"]);
        if ($user->getFirstName() == NULL && $user->getLastName() == NULL) {
            $name = explode(" ", $facebookUser["name"]);
            $user->setFirstName(current($name));
            $user->setLastName(end($name));
        }
        if ($user->getImage() == NULL) $user->setImage($facebookUser["picture"]["url"]);
        $user->setFacebookId($facebookUser["id"]);
        $user->setFacebookToken($facebookToken);
        $user->setStatus('A');
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user;
    }
    
    private function generateTempPassword($length, $characters='0123456789') {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
     * Sends a confirmation email to the user's stored email address with a temporary
     * password to access MTC.
     */
    public function sendConfirmationEmail($user, $nrorg , $bool) {
        $userEmail      = $user["email"];
        $userNrorg      = $nrorg;
        $userId         = $user['userId'];
        $tempPassword   = $this->generateTempPassword(6);
        
        $genConfiguration   =  $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $userNrorg]);
        $genOrganization    =  $this->getEntityManager()->getRepository(GenOrganization::class)->findOneBy(['nrorg' => $userNrorg]);
        if($genOrganization == null) {
            $genOrganization    =  $this->getEntityManager()->getRepository(GenOrganization::class)->findOneBy(['nrorg' => 0]);
        }
        
        /*alteracao da senha*/
        $genUser        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $userTypeRel    = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'nrorg' => $nrorg, 'genUserType' => '1']);
        
        $genUser->setPassword($this->cryptPassword($tempPassword));
        if($userTypeRel !== null) $userTypeRel->setPassword($this->cryptPassword($tempPassword));
        // if ($bool) $genUser->setStatus('P');

        $this->getEntityManager()->flush();
        
        $subject            = "Alteracao de senha";
        $body['senha']      = $tempPassword;
        $body['email']      = $genUser->getEmail();
        $body['remete']     = $genConfiguration->getHostFrom();
        
        return $this->sendEmail($genConfiguration, $genUser->getEmail(), $genUser->getFirstName() ." ". $genUser->getLastName() , $subject, $body, $genOrganization);
        //return  ['status' => 'Message has been sent', 'password' => $body['senha']];
    }
    
    function sendEmail($genConfiguration, $to, $toName, $subject, $body, $genOrganization) {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug  = smtpMailer::DEBUG_OFF;                  // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = $genConfiguration->getHost();           // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            // $mail->SMTPSecure = 'tls';                                  // 
            $mail->Username   = $genConfiguration->getHostFromName();   // SMTP username
            $mail->Password   = $genConfiguration->getHostPassword();   // SMTP password
            
            $secure = $genConfiguration->getSecure();  
            if (empty($secure)) $secure = 'tls';
            $mail->SMTPSecure = $secure;
        
            $port = $genConfiguration->getPort();  
            if (empty($port)) $port = 587;
            $mail->Port = $port;                

            //Recipients
            $mail->setFrom($genConfiguration->getHostFrom(), $genOrganization->getName());
            $mail->addAddress($to, $toName);     // Add a recipient
            // $mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');
        
            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $this->mountBody($body);
            $mail->AltBody = '';
        
            $mail->send();
            return ['status' => 'Message has been sent', 'password' => $body['senha']];
        } catch (ExceptionPHPMailer $e) {
            return ['status' => "Message could not be sent. Mailer Error: {$mail->ErrorInfo}", 'password' => $body['senha']];
        }
    }    
    
    // function sendEmail($genConfiguration, $to, $toName, $subject, $body, $genOrganization) {
    //     $mail = new PHPMailer;
    //     //Tell PHPMailer to use SMTP
    //     $mail->isSMTP();
    //     $mail->CharSet = 'UTF-8';
    //     //Enable SMTP debugging
    //     // SMTP::DEBUG_OFF = off (for production use)
    //     // SMTP::DEBUG_CLIENT = client messages
    //     // SMTP::DEBUG_SERVER = client and server messages
    //     $mail->SMTPDebug = smtpMailer::DEBUG_OFF;
    //     //Ask for HTML-friendly debug output
    //     $mail->Debugoutput = 'html';
    //     //Set the hostname of the mail server
    //     $mail->Host = $genConfiguration->getHost();
    //     //Set the SMTP port number - likely to be 25, 465 or 587
    //     $mail->Port = 587;
    //     //Set the encryption system to use - ssl (deprecated) or tls
    //     $mail->SMTPSecure = 'tls';
    //     //Whether to use SMTP authentication
    //     $mail->SMTPAuth = true;
    //     //Username to use for SMTP authentication
    //     $mail->Username = $genConfiguration->getHostFromName();
    //     //Password to use for SMTP authentication
    //     $mail->Password = $genConfiguration->getHostPassword();
    //     //Set who the message is to be sent from
    //     $mail->setFrom($genConfiguration->getHostFrom(), $genOrganization->getName());
    //     //Set who the message is to be sent to
    //     $mail->addAddress($to, $toName);
    //     //Set the subject line
    //     $mail->Subject = $subject;
    //     //Read an HTML message body from an external file, convert referenced images to embedded,
    //     //convert HTML into a basic plain-text alternative body
    //     $mail->Body = $this->mountBody($body);
    //     //Replace the plain text body with one created manually
    //     $mail->AltBody = 'This is a plain-text message body';
    //     //Attach an image file
    //     // $mail->addAttachment('../vendor/phpmailer/phpmailer/examples/images/phpmailer_mini.png');
    //     //send the message, check for errors
    //     if (!$mail->send()) {
    //         return "Mailer Error: " . $mail->ErrorInfo;
    //     } else {
    //         return "Message sent!";
    //     }
    // }
    
    public function mountBody($body) {
        $template = 
        '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html">
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<meta http-equiv="Content-Language" content="pt-br">
		</head>
        <center><br><br>
            <table border="0" cellpadding="0" cellspacing="0" width="490">
                <tbody><tr>
                    <td bgcolor="#F1F1F2"><p><img style="width: 200px; margin-left: 20px; margin-top: 15px;"></p>
                        <p style="color: rgb(51, 51, 51); font-size: 14px; font-weight: 300; line-height: 18px; margin-right: 20px; margin-left: 20px; font-family: &quot;open sans&quot;, arial;">Seu novo codigo de acesso foi gerado com sucesso.</p>
                        <p style="color: rgb(51, 51, 51); font-size: 14px; font-weight: 600; line-height: 18px; margin-right: 20px; margin-left: 20px; font-family: &quot;open sans&quot;, arial;">Para acessar o App, utilize:<br><br>
                        </p></td>
                </tr>
                <tr>
                    <td bgcolor="#C0C0C0">
                        <p style="text-align: center; color: rgb(255, 255, 255); font-size: 22px; font-weight: 600; line-height: 18px; margin: 15px; font-family: &quot;open sans&quot;, arial;">
                        <br>'.$body['senha'].'</p></td>
                </tr>
                <tr>
                    <td bgcolor="#F1F1F2"><p style="color: rgb(51, 51, 51); font-size: 14px; font-weight: 300; line-height: 18px; margin-right: 20px; margin-left: 20px; margin-top: 20px; font-family: &quot;open sans&quot;, arial;">Em caso de duvidas, contatar nossa equipe de atendimento:<br>
                        <strong>E-mail:</strong> <a href="ouvidoria:' . $body['remete'] . '" target="_blank">' . $body['remete'] . '</a><br></p><center>------------------------------<wbr>------------------------------<wbr>-----------
                        <p style="color: rgb(0, 82, 140); font-size: 14px; font-weight: 300; line-height: 18px; margin-bottom: 60px; margin-right: 20px; margin-left: 20px; font-family: &quot;open sans&quot;, arial;">AGRADECEMOS A PREFERENCIA</p></center>
                        <p></p></td>
                </tr>
                </tbody>
            </table>
        </center>
		</html>
		';
        return $template;
    }
    
    public function validateTemporaryPassword($userId, $tempPassword) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        if ($user->getPassword() !== $this->cryptPassword($tempPassword)) {
            throw new Exception('Temporary password is incorrect!');
        }
    }

    /**
     * requestToken
     * Log in the user and populate Environment On enter 
     *
     * @param integer $userId
     *
     * @return GenUser
     *
     * @throws Exception
     */
    public function requestToken($userId, $token) {
        
        
        $user = $this->getGenUser(array("id" => $userId));
        
        if($user == null) {
            throw Exception::userNotFound();
        }
        
        if ($user->getToken() !== $token) {
            throw new Exception('Unauthorized Action', 3);
        }
        
        if($user->getFacebookToken() !== null){
            $user = $this->automaticLoginFacebook($user);
        }
        
        $clientId = "USER_ID_$userId";
        $token    = $this->generateAccessToken($clientId);

        return $token;
    }

    /**
     * getUserOrganizations
     * Log in the user and populate Environment On enter 
     *
     * @param integer $userId
     *
     * @return GenUser
     *
     * @throws Exception
     */
    public function getUserOrganizations($userId, $userTypeId, $latitude, $longitude) {
        $user = $this->getGenUser(array("id" => $userId));
        
        if($user == null) {
            throw Exception::userNotFound();
        }
        
        if($user->getFacebookToken() !== null){
            $user = $this->automaticLoginFacebook($user);
        }
        
        $user->setAssociatedOrganizations($this->getEntityManager(), $userTypeId, $latitude, $longitude);

        return $user;
    }
    
    /**
     * Links an existing account to a facebook profile.
     * 
     */
    public function linkFacebook($userId, $facebookToken) {
        $facebookUser = $this->getFacebookUserData($facebookToken);
        if(!isset($facebookUser["email"])) {
            throw Exception::invalidFacebook();
        }

        $accessToken = new AccessToken($facebookToken);
        $oauth2 = $this->facebook->getOAuth2Client();
        $longLivedToken = $oauth2->getLongLivedAccessToken($accessToken);

        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if ($user == NULL) {
            throw Exception::userNotFound();
        }
        
        $user = $this->setFacebookUserData($facebookUser, $longLivedToken->getValue(), $user, $longLivedToken->getExpiresAt());
        $user->build($this->getEntityManager());
        
        return $user;
    }
        
    /**
     * loginFacebook
     * Perform login using facebook data
     *
     * @param  string $facebookToken Facebook login token
     *
     * @return GenUser
     */
    public function loginFacebook($facebookToken) {
        $facebookUser = $this->getFacebookUserData($facebookToken);
        
        if(!isset($facebookUser["email"])) {
            throw Exception::invalidFacebook();
        }

        $accessToken = new AccessToken($facebookToken);
        $oauth2 = $this->facebook->getOAuth2Client();
        $longLivedToken = $oauth2->getLongLivedAccessToken($accessToken);
        
        $user = $this->getGenUser(array("facebookId" => $facebookUser["id"], 'status' => "A"));
     
        if ($user != null) {
            $user = $this->setFacebookUserData($facebookUser, $longLivedToken->getValue(), $user, $longLivedToken->getExpiresAt());
        }
        else throw Exception::userNotFound();
        
        $user->build($this->getEntityManager());

        return $user;
    }

    /**
     * automaticLoginFacebook
     * Perform Automatic login using facebook data
     *
     * @param  GenUser $user
     *
     * @return GenUser
     */
    public function automaticLoginFacebook(GenUser $user){
        $facebookUser = $this->getFacebookUserData($user->getFacebookToken());
        
        if($user->getFacebookTokenExpDate() < new \Datetime) {
            throw Exception::facebookTokenExpired();
        }
        
        $loggedUser = $this->setFacebookUserData($facebookUser, $user->getFacebookToken(), $user);
        
        return $loggedUser;
    }

    /**
     * logout
     * Clear environment properties
     *
     */
    public function logout() {
        $this->getEnvironment()->setUserId(null);
    }
    
    /**
     * registerIsIncomplete
     * Check if the user registration wasn't completed in the app.
     * For this, it's checking the status attribute in the GenUser table.
     * 
     */
    public function registerIsIncomplete($userId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        return $user->getStatus() !== self::REGISTRATION_COMPLETE;
    }
    
    /**
     * checkTempPassword
     * Verifies if the informed temporary password is correct.
     * 
     */
    public function checkTempPassword($userId, $password) {
        $cryptedPassword = $this->cryptPassword($password);
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findBy(['id' => $userId, 'password' => $cryptedPassword]);
        
        return $user != NULL;
    }
    
    /**
     * createPassword
     * Alters an user's status to active and alter it's password.
     * 
     */
    public function createPassword($id, $newPass) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        $user->setPassword($this->cryptPassword($newPass));
        $user->setStatus('A');
        
        $this->getEntityManager()->flush();
        
        $user->build($this->getEntityManager());
        
        return $user;
    }
    
    /**
     * createWallet
     * 
     * @param GenUser $user
     */
    private function createWallet(GenUser $user) {
        $wallet = new PayWallet();
        $wallet->setUser($user);
        $this->getEntityManager()->persist($wallet);
    }

    /**
     * @param array $newUserRow
     * @return GenUser
     */
    public function register($newUserRow) {
        $user = new GenUser();
        $user->setEmail($newUserRow['EMAIL']);
        $user->setFirstName($newUserRow['FIRST_NAME']);
        $user->setLastName($newUserRow['LAST_NAME']);
        $user->setPhone($newUserRow['PHONE']);
        $this->createWallet($user);
        if(isset($newUserRow['IMAGE'])){
            $user->setImage($newUserRow['IMAGE']);
        }
        $user->setPassword($this->cryptPassword($newUserRow['PASSWORD']));
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user;
    }

    /**
     * changePass
     * Change the password of user
     *
     * @param string $oldPassword
     * @param string $newPassword
     *
     * @throws Exception
     */
    public function changePassword($userId, $oldPassword, $newPassword) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);

        if ($oldPassword !== NULL) {
            $encryptedOldPassword = $this->cryptPassword($oldPassword);
            if ($user->getPassword() !== $encryptedOldPassword) {
                throw Exception::wrongOldPassword();
            }
        }
        
        $encryptedNewPassword = $this->cryptPassword($newPassword);
        $user->setPassword($encryptedNewPassword);
        if ($user->getResetPassword() == true)    
            $user->SetResetPassword(NULL);
        
        $this->getEntityManager()->flush();
    }
    
    /**
     * Changes the user's mobile phone number.
     * Validates if the user knows the old phone number and gets the new phone
     * number received by Facebook Account Kit validation.
     */
    public function changeMobilePhoneNumber($userId, /*$authCode, */$newPhone /*$oldPhoneNumber*/) {
        //$data = $this->facebookTokenExchange($authCode);
        //$newPhone = isset($data['phone']) ? $data['phone']['number'] : NULL;
        
        $userPhone = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['type' => 'MOBILE', 'genUser' => $userId]);
        if ($userPhone != NULL) {
            //if ($oldPhoneNumber == $userPhone->getPhone()) {
            $userPhone->setPhone($newPhone);
        } else {
            $userPhone = new GenContact();
            $userPhone->setType('MOBILE');
            $userPhone->setPhone($newPhone);
            $user      = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
            $userPhone->setGenUser($user);
            $user->setModifiedAt(new \DateTime());
            $this->getEntityManager()->persist($userPhone);
        }
        $this->getEntityManager()->flush();
        return 'Ok';
        //} else return 'Incorrect old phone number';
    }
    
    /**
     * Changes the user's email address
     */
    public function changeEmail($userId, $newEmail) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->setEmail($newEmail);
        $user->setModifiedAt(new \DateTime());
        $user->setModifiedBy($user->getId());
        
        $this->getEntityManager()->flush();
        return 'OK';
    }
    
    public function validateFacebookSmsToken($userId, $code) {
        $data = $this->facebookTokenExchange($code);
        $phone = isset($data['phone']) ? $data['phone']['number'] : NULL;
        
        if ($phone == NULL) {
            throw Exception::smsTokenValidationError();
        }
        
        $contactMethod = $this->getEntityManager()->createQuery(
            "
            SELECT c.phone
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenContact' c WITH c.genUser = u.id AND c.type = 'MOBILE'
            WHERE u.id = $userId
            "
        )->getResult()[0]['phone'];
        
        if ($phone != $contactMethod) {
            throw Exception::incorrectPhoneNumber();
        }
    }
    
    public function facebookTokenExchange($code) {
        // Initialize variables
        $app_id = '737658683293102';
        $secret = '7d43f6170c2146dc79d30204668090a1';
        $version = 'v3.1'; // 'v1.1' for example
        
        // Exchange authorization code for access token
        $token_exchange_url = 'https://graph.accountkit.com/'.$version.'/access_token?'.
          'grant_type=authorization_code'.
          '&code='.$code.
          "&access_token=AA|$app_id|$secret";
        $data = $this->doCurl($token_exchange_url);
        $user_id = $data['id'];
        $user_access_token = $data['access_token'];
        $refresh_interval = $data['token_refresh_interval_sec'];
        
        // Get Account Kit information
        $me_endpoint_url = 'https://graph.accountkit.com/'.$version.'/me?'.
          'access_token='.$user_access_token;
        $data = $this->doCurl($me_endpoint_url);
        $phone = isset($data['phone']) ? $data['phone']['number'] : '';
        $email = isset($data['email']) ? $data['email']['address'] : '';
        
        return $data;
    }

    // Method to send Get request to url
    function doCurl($url) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $data = json_decode(curl_exec($ch), true);
      curl_close($ch);
      return $data;
    }
    
    
    /*DEVIDY INÍCIO*/
    public function activateUser($user) {
        $user->setStatus('A');
        $this->getEntityManager()->flush();
        return $user;
    }
    
    public function inactiveUser($user) {
        $user->setStatus('I');
        $this->getEntityManager()->flush();
        return $user;
    }
    /*DEVIDY FINAL*/
    
    /**
     * cria um user com informações do WEBSERVICE, bem como o perfil
     */
    public function createGenUserWebService($email, $password, $nrorg, $externalId){
        /*Diff*/
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email]);
        if(empty($user)){
            $user = new GenUser();
            $user->setEmail($email);
            $user->setPassword($this->cryptPassword($password));
            $user->setStatus('A');
            
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, 1));
            $userProfile->setStatus('A');
            $userProfile->setExternalId($externalId);
            
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->persist($userProfile);
            $this->getEntityManager()->flush();
        }else {
           $userType = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $user->getId(), 'genUserType' => 1]);
           
           if(empty($userType)){
                $userProfile = new GenUserTypeRel();
                $userProfile->setGenUser($user);
                $userProfile->setNrorg($nrorg);
                $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, 1));
                $userProfile->setStatus('A');
                $userProfile->setExternalId($externalId);
                
                $this->getEntityManager()->persist($userProfile);
                $this->getEntityManager()->flush();
           }
        }
    }
    
    public function createUserTypeRel($user, $nrorg, $externalId) {
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, 1));
        $userProfile->setStatus('A');
        $userProfile->setExternalId($externalId);
        
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    public function updateGenUserWebService($email, $password){
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email, 'status' => "A"]);
        
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }
    
    public function getConfiguration($nrorg){
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
    
    public function getGenUserType($userId){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId]);
    }
    
    /*Buscar organization*/
    public function getOrganization($nrorg) {
        return $this->getEntityManager()->getRepository(GenOrganization::class)->find($nrorg);
    }
    
    public function updateGenUserEmailCellphone($userId, $email, $type, $phoneNumber){
        $user        = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['id' => $userId, 'status' => "A"]);
        $userPhone = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $userId]);
        
        $user->setEmail($email);
        $userPhone->setGenUser($user);
        $userPhone->setType('MOBILE');
        $userPhone->setPhone($phoneNumber);
       
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->persist($userPhone);
        $this->getEntityManager()->flush();
    }
    
    public function getUserFromOrganizationAndProfile($nrorg, $cpf, $npf, $email) {
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u
            WHERE (u.cpf = '$cpf' OR utr.externalId = '$npf' OR u.email = '$email') 
            AND u.status = 'A' AND utr.nrorg = '$nrorg' AND utr.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getUserIdsByEmail($email) {
        $genUser        = GenUser::class;

        $genUserIds = $this->getEntityManager()->createQuery(
            "SELECT u.id
             FROM $genUser u 
             WHERE u.email = '$email' AND u.status = 'A'   
             "
        )->getResult();
        
        return $genUserIds;
    }

    public function getActiveUserByExternalId($nrorg, $externalId) {
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u.id
            WHERE utr.externalId = '$externalId' AND u.status = 'A' AND utr.status = 'A' AND utr.genUserType = 1
            "
        )->getResult();
        
        return $users;
    }
    
    public function getActiveUserNrorgByExternalId($nrorg, $externalId) {
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u.id
            WHERE utr.externalId = '$externalId' AND u.status = 'A' AND utr.status = 'A' AND utr.genUserType = 1 
            AND utr.externalId is not null
            "
        )->getResult();
        
        return $users;
    }
    
    public function createOrUpdateGenUser($firstName, $lastName, $cpf = null, $email = null, $password = null, $image = null, $nrorg, $status, $genUser = NULL) {
        if(empty($genUser)){
        $genUser = new GenUser();
            
        } 
        $genUser->setFirstName($firstName);
        $genUser->setLastName($lastName);
        $genUser->setStatus($status);
        !empty($image)       ? $genUser->setImage($image) : "";
        !empty($cpf)         ? $genUser->setCpf($cpf) : "";
        !empty($email)       ? $genUser->setEmail($email) : "";
        !empty($password)    ? $genUser->setPassword($this->cryptPassword($password)) : "";
        
        $this->getEntityManager()->persist($genUser);
        return $genUser;
    }
    
    public function addProfileUser($user, $userTypeId, $nrorg, $status, $externalId) {
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
        $userProfile->setNrorg($nrorg);
        $userProfile->setStatus($status);
        $userProfile->setExternalId($externalId);
        
        return $this->getEntityManager()->persist($userProfile);
    }
    
    public function requestAddressChange($user, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL) {
        $typeName    = explode('_', $type)[0];

        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $user, 'status' => "A", 'nrorg' => $nrorg, 'type' => $typeName]);
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => $type]);
        
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus('A');
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $genAddress = $this->getEntityManager()->persist($address);
        return $genAddress;
    }
    
    public function addDependent($parent, $dependent, $monthlyLimit = null, $receiptsTo, $nrorg) {
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['parent' => $parent, 'dependent' => $dependent]);
        
        if ($dependency != NULL) return $dependency;
        
        if ($dependent == NULL || $parent == NULL) {
            throw Exception::userNotFound();
        }
        
        if ($receiptsTo == NULL) {
            $parent->build($this->getEntityManager());
            $receiptsTo = $parent->getEmail();
        }
        
        $dependentRel = new GenDependent();
        $dependentRel->setDependent($dependent);
        $dependentRel->setParent($parent);
        $dependentRel->setNrorg($nrorg);
        $dependentRel->setMonthlyLimit($monthlyLimit);
        $dependentRel->setReceiptsTo($receiptsTo);
        $dependentRel->setStatus('A');
        
        $this->getEntityManager()->persist($dependentRel);
        return $dependentRel;
    }
    
    public function addMobilePhoneNumber($user, $newPhone, $type) {
        $userPhone = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['type' => 'MOBILE', 'genUser' => $user]);
        if ($userPhone != NULL) {
            $userPhone->setPhone($newPhone);
        } else {
            $userPhone = new GenContact();
            $userPhone->setType('MOBILE');
            $userPhone->setPhone($newPhone);
            $userPhone->setGenUser($user);
            $user->setModifiedAt(new \DateTime());
        }
        $this->getEntityManager()->persist($userPhone);
    }
    
    public function removeMascaraCpf($cpf) {
        $chars = array(".","-");
        $newCpf = str_replace($chars,"",$cpf);
        return $newCpf;
    }
    
    public function commit(){
        $this->getEntityManager()->flush();
    } 
    
    public function loginBackOffice($email, $password) {
        $cryptedPassword = $this->cryptPassword($password);
        $user = $this->getGenUser(array("email" => $email, 'status' => 'A'));
        if ($user == null) {
            throw Exception::loginNotFound();
        } else if ($user->getPassword() != $cryptedPassword) {
            throw Exception::incorrectPassword();
        }
        
        $user->build($this->getEntityManager());
        $userId   = $user->getId();
        
        $clientId = "USER_ID_$userId";
        $token    = $this->generateAccessToken($clientId);
        
        $user->setToken($token);
        $this->getEntityManager()->flush();
        
        return $user;
    }

    function setLastGenUserToken($userId, $externalToken) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $user->setLastToken($externalToken);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    public function getGenUserByCpfOrEmail($cpf = null, $email = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $queryCpf       =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        $queryEmail     =  $email != NULL ? " u.email = '$email' " :  "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u.id userId, u.email, utr.nrorg 
            FROM $genUser u
            JOIN $genUserType utr WITH utr.genUser = u.id 
            WHERE $queryCpf $queryEmail 
            AND u.status = 'A'
            AND utr.status = 'A'
            AND utr.genUserType = '1'
            "
        )->getResult();
        return $users;
    }
    
    public function loginBackOfficeCheckingStatusdifferentofI($email, $password) {
        $cryptedPassword = $this->cryptPassword($password);
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(array("email" => $email, 'status' => 'A'));
        if ($user == null) {
            $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(array("email" => $email, 'status' => 'P'));
            if ($user == null) {
                throw Exception::loginNotFound();
            } else if ($user->getPassword() != $cryptedPassword) {
                throw Exception::incorrectPassword();
            }
        } else if ($user->getPassword() != $cryptedPassword) {
            throw Exception::incorrectPassword();
        }
        
        $user->build($this->getEntityManager());
        $userId   = $user->getId();
        
        $clientId = "USER_ID_$userId";
        $token    = $this->generateAccessToken($clientId);
        
        $user->setToken($token);
        $this->getEntityManager()->flush();
        
        return $user;
    }
        
    public function sendConfirmationEmailAdmin($users, $nrorg , $bool) {
        try {
            $user = $users[0];
            $userEmail      = $user["email"];
            $userNrorg      = $nrorg;
            $userId         = $user['userId'];
            $tempPassword   = $this->generateTempPassword(6);
            
            $genConfiguration   =  $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $userNrorg]);
            $genOrganization    =  $this->getEntityManager()->getRepository(GenOrganization::class)->findOneBy(['nrorg' => $userNrorg]);
            if($genOrganization == null) {
                $genOrganization    =  $this->getEntityManager()->getRepository(GenOrganization::class)->findOneBy(['nrorg' => 0]);
            }
            /*alteracao da senha*/
            $genUser = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $userEmail]);
            $genUser->setPassword($this->cryptPassword($tempPassword));
            // $genUser->setStatus('P');
            $genUser->SetResetPassword('true');

            $this->getEntityManager()->flush();
            
            $subject            = "Alteracao de senha";
            $body['senha']      = $tempPassword;
            $body['email']      = $genUser->getEmail();
            $body['remete']     = $genConfiguration->getHostFrom();
            
            return $this->sendEmail($genConfiguration, $genUser->getEmail(), $genUser->getFirstName() ." ". $genUser->getLastName() , $subject, $body, $genOrganization);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getGenUserByCpfOrEmailuserExistsByCpfAndReturnData($cpf = null, $email = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $queryCpf       =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        $queryEmail     =  $email != NULL ? " u.email = '$email' " :  "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u.id userId, u.email, u.cpf
            FROM $genUser u
            WHERE $queryCpf $queryEmail 
            AND u.status != 'I'  
            "
        )->getResult();
        
        return $users != null ? $users : null;
    }
    
            
}