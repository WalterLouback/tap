<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenContact;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\PayCreditcard;
use Zeedhi\ApiGeneral\Model\Entities\GenAddress;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiGeneral\Model\Entities\BpWallet;
use Zeedhi\ApiGeneral\Model\Entities\PayWallet;
use Zeedhi\ApiGeneral\Model\Entities\GenEditableFields;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;
use Zeedhi\ApiGeneral\Model\Entities\GenDocument;
use Zeedhi\ApiGeneral\Model\Entities\GenDocUserRel;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganizationUserRel;

use Zeedhi\ApiGeneral\Service\SMTP;
use Zeedhi\ApiGeneral\Helpers\ReplaceDql;

use Doctrine\ORM\EntityManager;
use Facebook\Facebook;
use Facebook\Authentication\AccessToken;

use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiGeneral\Model\Entities\SerServiceSellerRel;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\GenUser as EvtGenUser;
use Zeedhi\ApiOrders\Model\Entities\EvtSellerType;

class User extends UserOperation {
    
    /** @var Facebook */
    private $facebook;
    const SALT = 'BIPPOPSALT';
    /**
     * Auth constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     * @param Facebook $facebook
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getUserData($userId, $userTypeId, $nrorg) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->build($this->getEntityManager(), $userTypeId, $nrorg);
        
        return $user;
    }
    
    /**
     * Creates a request to update address that will appear in the backoffice 
     * waiting for approval.
     */
    public function requestAddressChange($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL, $latitude=NULL, $longitude=NULL) {
        $typeName    = explode('_', $type)[0];
        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => $type]);

        /* Checking the permission to perform this action */
        $permission  = ($type != 'DELIVERY') ? $param->getGenPermission()->getName() : 'A';
        
        switch ($permission) {
            case 'N':
                throw Exception::unauthorizedAction();
                break;
            case 'A':
                if ($old != NULL) {
                    $old->setStatus('O');
                    $this->getEntityManager()->persist($old);
                }
                // Do not break
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus($status);
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $address->setLatitude($latitude);
        $address->setLongitude($longitude);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        
        /* Creating data update request */
        if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setGenAddress($address);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $dataUpdate->setNrorg($nrorg);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        if ($type == 'DELIVERY') return ['status'=>$status, 'id'=>$address->getId()];
        else return $status;
    }
    
    /**
     * Creates a request to update user's basic information. The request will
     * appear in the backoffice and once it's accepted, the data will be transfered
     * to the original user (parent).
     */
    public function requestUserChange($id, $nrorg, $firstName, $lastName, $cpf, $document) {
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        $s1=NULL; $s2=NULL; $s3=NULL;
        if ($firstName != NULL && $user->getFirstName() != $firstName) $s1 = $this->requestFirstNameChange($id, $firstName, $nrorg, $document);
        if ($lastName != NULL && $user->getLastName() != $lastName)    $s2 = $this->requestLastNameChange($id, $lastName, $nrorg, $document);
        if ($cpf != NULL && $user->getCpf() != $cpf)                   $s3 = $this->requestCpfChange($id, $cpf, $nrorg, $document);
        
        return [$s1, $s2, $s3];
    }
    
    private function requestFirstNameChange($id, $firstName, $nrorg, $document) {
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'FIRST_NAME']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setFirstName($firstName);
            $user->setModifiedAt(new \DateTime());
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setFirstName($firstName);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    private function requestLastNameChange($id, $lastName, $nrorg, $document) {
        $param        = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'LAST_NAME']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setModifiedAt(new \DateTime());
            $user->setLastName($lastName);
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setLastName($lastName);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    private function requestCpfChange($id, $cpf, $nrorg, $document) {
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'CPF']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setModifiedAt(new \DateTime());
            $user->setCpf($cpf);
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setCpf($cpf);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    /**
     * Adds a contact method to a user.
     */
    public function addContactMethod($userId, $type, $phoneNumber) {
        $user    = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $contact = new GenContact();
        $contact->setGenUser($user);
        $contact->setType($type);
        $contact->setPhone($phoneNumber);
        
        $this->getEntityManager()->persist($contact);
        $this->getEntityManager()->flush();
        
        return $contact;
    }
    
    /**
     * Removes a contact method
     */
    public function removeContactMethod($contactMethodId, $userId) {
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->find($contactMethodId);
        
        if ($contact == NULL || $contact->getGenUser() == NULL || $contact->getGenUser()->getId() !== $userId) {
            throw Exception::contactMethodNotFound();
        }
        
        $this->getEntityManager()->remove($contact);
        $this->getEntityManager()->flush();
        
        return $contact;
    }
    
    /**
     * Verifies if the specified phone
 number matches the registered one.
     */
    public function verifyMobilePhone($userId, $phoneNumber) {
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $userId, 'type' => 'MOBILE']);
        return $phoneNumber == $contact->getPhone();
    }
    
    public function verifyPhoneInDatabase($phoneNumber) {
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['phone' => $phoneNumber, 'type' => 'MOBILE']);
        if ($contact) {
            return $contact->getGenUser();
        } else {
            return NULL;
        }
    }
    
    /**
     * getContactMethodsByCpf
     * Returns the contact methods of an user based on it's CPF.
     * 
     */
    public function getContactMethodsByCpf($cpf) {
        $contactMethods = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.email, c.phone
            FROM Zeedhi\ApiGeneral\Model\Entities\GenUser u
            LEFT JOIN Zeedhi\ApiGeneral\Model\Entities\GenContact c WITH c.genUser = u.id AND c.type = 'MOBILE'
            WHERE u.cpf = $cpf
            "
        )->getResult();
        
        return $this->factoryContactMethods($contactMethods);
    }
    
    /**
     * getContactMethodsByExternalId
     * Returns the contact methods of an user based on it's External Id.
     * 
     */
    public function getContactMethodsByExternalId($eId) {
        $contactMethods = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.email, c.phone
            FROM Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel p
            JOIN Zeedhi\ApiGeneral\Model\Entities\GenUser u WITH u.id = p.genUser
            LEFT JOIN Zeedhi\ApiGeneral\Model\Entities\GenContact c WITH c.genUser = u.id AND c.type = 'MOBILE'
            WHERE p.externalId = $eId
            "
        )->getResult();
        
        return $this->factoryContactMethods($contactMethods);
    }
    
    /**
     * factoryContactMethods
     * Formats a contact method.
     * 
     */
    private function factoryContactMethods($contactMethods) {
        if (count($contactMethods) > 0) {
            $userId      = $contactMethods[0]['id'];
            $maskedEmail = $contactMethods[0]['email'] != NULL ? GenContact::maskEmail($contactMethods[0]['email']) : NULL; 
            $maskedPhone = $contactMethods[0]['phone'] != NULL ? GenContact::maskPhone($contactMethods[0]['phone']) : NULL;
            
            $response = ['USER_ID' => $userId, 'MASKED_EMAIL' => $maskedEmail, 'EMAIL' => $contactMethods[0]['email'], 'PHONE' => $maskedPhone];
            
        } else $response = [];
        
        return $response;
    }
    
    /**
     * getUserDependents
     * Gets the dependents of an user in an organization.
     * 
     */
    public function getUserDependents($id, $nrorg) {
        $user     = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        $parentId = $user->getId();
        if ($user !== NULL) {
            $dependents = $this->getEntityManager()->createQuery(
                "
                SELECT DISTINCT d
                FROM 'Zeedhi\ApiGeneral\Model\Entities\GenDependent' d
                JOIN d.parent pr
                JOIN d.dependent dr
                JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = dr
                JOIN utr.genUserType ut
                WHERE pr.id = $parentId
                AND d.nrorg = $nrorg
                AND ut.id = 1
                AND dr.status = 'A'
                "
            )->getResult();
            foreach ($dependents as $dependent) {
                $dependent->build($this->getEntityManager(), 1, $nrorg);
            }
            return $dependents;
        } return [];
    }
    
    /**
     * addDependent
     * Link an user to another as a dependent.
     * 
     */
    public function addDependent($parentId, $dependentId, $monthlyLimit, $receiptsTo, $nrorg) {
        $parent    = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId);
        $dependent = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['parent' => $parent, 'dependent' => $dependent]);
        
        if ($dependency != NULL) return $dependency;
        
        if ($dependent == NULL || $parent == NULL) {
            throw Exception::userNotFound();
        }
        
        if ($receiptsTo == NULL) {
            $parent->build($this->getEntityManager());
            $receiptsTo = $parent->getEmail();
        }
        
        $dependentRel = new GenDependent();
        $dependentRel->setDependent($dependent);
        $dependentRel->setParent($parent);
        $dependentRel->setNrorg($nrorg);
        $dependentRel->setMonthlyLimit($monthlyLimit);
        $dependentRel->setReceiptsTo($receiptsTo);
        $dependentRel->setStatus('A');
        
        $this->getEntityManager()->persist($dependentRel);
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * removeDependent
     * Unlink a dependent from it's parent.
     * 
     */
    public function removeDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL || $user == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $this->getEntityManager()->remove($dependentRel);
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * enableDependent
     * Sets a dependent's status to enabled.
     * 
     */
    public function enableDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setStatus('A');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * disableDependent
     * Sets a dependent's status to disabled.
     * 
     */
    public function disableDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setStatus('D');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * enableDependentForMainCard
     * Sets that the dependent can use the parent's main card.
     * 
     */
    public function enableDependentForMainCard($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setCanUseMainCard('T');
        }
        
        $this->getEntityManager()->flush();

        return $dependent;
    }
    
    /**
     * disableDependentForMainCard
     * Sets that the dependent can not use the parent's main card.
     * 
     */
    public function disableDependentForMainCard($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
    
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setCanUseMainCard('F');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    public function editDependentMonthlyLimit($userId, $dependentId, $nrorg, $monthlyLimit) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
    
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setMonthlyLimit($monthlyLimit);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function editDependentReceiptsTo($userId, $dependentId=NULL, $nrorg, $receiptsTo) {
        $filter = array();
        $filter = array_merge($filter, array('nrorg' => $nrorg));
        $filter = array_merge($filter, array('parent' => $userId));
        if ($dependentId != NULL) $filter = array_merge($filter, array('dependent'   => $dependentId));

        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy($filter);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setReceiptsTo($receiptsTo);
        };
        
        $this->getEntityManager()->flush();
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $card   = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard();
        if ($card == NULL) return 0;
        $cardId = $card->getId();
        $today  = new \DateTime();
        $firstDayOfMonth   = $today->format('Y-m-d');

        $orders = $this->getEntityManager()->createQuery(
        "
        SELECT o
        FROM 'Zeedhi\ApiGeneral\Model\Entities\OrdOrder' o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createDate >= '$firstDayOfMonth'
        "
        )->getResult();
        
        $total = 0;
        
        foreach ($orders as $order) {
            if ($order->getTotal() != NULL)
                $total += $order->getTotal();
        }
        
        return $total;
    }
    
    public function setMainCreditCard($userId, $cardId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($cardId);
        
        $user->setMainCreditcard($creditCard);
        $this->getEntityManager()->flush();
    }
    
    /*CRISTIANO Integração*/
    
    /*Buscar organization*/
    public function getOrganization($nrorg) {
        $organization    = $this->getEntityManager()->getRepository(GenOrganization::class)->find($nrorg);
        
        return $organization;
    }
    
    /*cria um userDependent com informações do WEBSERVICE, bem como o perfil*/
    public function createGenUserDependentWebService($firstName, $lastName, $email = null, $password, $nrorg, $externalId = null, $cpf = null){
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $email ? $user->setEmail($email) : "";
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        $cpf ? $user->setCpf($cpf) : "";
        
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, 1));
        $userProfile->setStatus('A');
        $externalId ? $userProfile->setExternalId($externalId) : "";
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    /*atualiza um user*/
    public function updateGenUserWebService($userId, $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userTypeId){
        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmail($email);
        $user->setImage($image);
        $user->setStatus('A');
        $user->setCpf($cpf);
        $user->setModifiedAt(new \DateTime());
        
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId]);
        if (empty($userProfile) || $userProfile->getGenUserType()->getId() == 3) {
            $userProfile = new GenUserTypeRel();
        }
        
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userType   = $this->getEntityManager()->getRepository(GenUserType::class)->find($userTypeId);
        $userProfile->setGenUserType($userType);
        $userProfile->setStatus('A');
        $userProfile->setExternalId($externalId);
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    /*atualiza os telefones do user*/
    public function updateContactMethod($userId, $type, $phoneNumber) {
        
        $contact    = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
        if(!empty($contact)){
            $user    = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
            $user->setModifiedAt(new \DateTime());
            $contact->setGenUser($user);
            $contact->setType($type);
            $contact->setPhone($phoneNumber);
            
            $this->getEntityManager()->persist($contact);
            $this->getEntityManager()->flush();
            
            return $contact;
        }else {
            $this->addContactMethod($userId, $type, $phoneNumber);
        }
    }
    
    /*buscar endereços de um user*/
    public function getUserAddress($userId, $nrorg){
        return $this->getEntityManager()->getRepository(GenAddress::class)->findBy(['genUser' => $userId, 'status' => "A"]);
    }
    
    // /*buscar um user por email*/
    // public function getUserWebService($email, $externalId, $cep){
    //     return $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email]);
    // }
    
    /*buscar um user por email*/
    public function getUserWebService($cpf = NULL, $externalId = NULL){
        $queryCpf        = isset($cpf)        && $cpf        ? $cpf        : NULL;
        $queryExternalId = isset($externalId) && $externalId ? $externalId : NULL;
        
        if($queryCpf){
            $user     = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['cpf' => $queryCpf, 'status' => 'A']);
            $response = !empty($user) ? $user : NULL;
        }else {
            $userType = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['externalId' => $queryExternalId, 'status' => 'A']);
            $response = !empty($userType) ? $userType : NULL;
        }

        return $response;
    }
    
    /*cryptografa uma senha*/
    private function cryptPassword($password) {
        //@toDo Remover StrToUpper
        return strtoupper(hash('sha512', self::SALT.$password));
    }
    
    /*cria um endereço*/
    public function createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type) {
        $typeName    = explode('_', $type)[0];

        $user        = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
        
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        if(!empty($old)){
            $old->setStatus("O");
            $this->getEntityManager()->persist($old);
            $this->getEntityManager()->flush();
        }
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus($status);
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        return $status;
    }
    
    public function updateAddressProfile($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type) {
        $typeName    = explode('_', $type)[0];
        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => $type]);

        if ($old != NULL) {
            $old->setStatus('O');
            $this->getEntityManager()->persist($old);
        }
        $status = "A";
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus($status);
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    /*atualiza um endereço*/
    public function requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL) {
        $typeName    = explode('_', $type)[0];

        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus("A");
        $address->setCreateDate(new \DateTime());
        
        if(!empty($old)) {
            $old->setStatus("O");
            $this->getEntityManager()->persist($old);
        }
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
    }
    
    public function getConfiguration($nrorg) {
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
    
    public function getGenUserType($userId){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
    }

    public function getUsersFromOrganization($nrorg,$userName,$cpf,$email,$telefone,$tipoUsuario, $itemsPerPage, $page) {
        $filtro = "";
     
        if($userName){ 
            $filtro .= " AND concat(u.firstName,' ',u.lastName) like '$userName'"; 
        }
        if($cpf){ 
            $filtro .= " AND u.cpf like " . "'".$cpf. "'"; 
        }
        if($email){ 
            $filtro .= " AND u.email like " . "'".$email. "'"; 
        }
        if($telefone){ 
            $filtro .= " AND uc.phone like " . "'".$telefone. "'"; 
        }
        if($tipoUsuario){
            $filtro .= " AND utr.genUserType in " . "(".$tipoUsuario. ")";
        }

        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenContact' uc WITH uc.genUser = u
            WHERE utr.nrorg = $nrorg AND u.status = 'A'
            $filtro
            GROUP BY u.id 
            "
        );
        
         $users->setFirstResult($itemsPerPage * ($page - 1));
        if ($itemsPerPage != NULL) $users->setMaxResults($itemsPerPage);
        
        $users = new \Doctrine\ORM\Tools\Pagination\Paginator($users);
        
        foreach ($users as $user) {
            $user->build($this->getEntityManager(), NULL, $nrorg);
        }
        
        return $users;
    }
    
    public function getDataUpdateRequests($nrorg, $statuses = null, $typeDocument = null, $dateRange = null) {
        $document = GenDocument::class;
        $dateFormated = $dateRange !== null ? $this->stringToDate($dateRange) : "";
        
        if (!is_null($typeDocument))
            $docType = "'". join($typeDocument, "' OR doc.id = '") . "'";
        $docTypeQuery = $typeDocument !== NULL ? "AND (doc.id = $docType)" : "";
        
        if (!is_null($statuses))
            $status = "'". join($statuses, "' OR du.updateStatus = '") . "'";
        $statusQuery = $statuses !== NULL ? "AND (du.updateStatus = $status)" : "";
        
        if (!empty($dateFormated))
            $date = "'". join($dateFormated, "' AND du.createdAt <= '") . "'";
        $dateRangeQuery = $dateFormated !== "" ? "AND (du.createdAt >= $date)" : ""; 
        
        $requests = $this->getEntityManager()->createQuery(
            "
            SELECT du
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate' du
            LEFT JOIN $document doc WITH doc.id = du.genDocument
            WHERE du.nrorg = $nrorg
            $statusQuery
            $docTypeQuery
            $dateRangeQuery
            GROUP BY du.id
            ORDER BY du.id
            ASC
            "
        )->getResult();
        
        foreach($requests as $request) {
            $request->build($this->getEntityManager());
        }
        
        return $requests;
    }

    public function acceptDataUpdateRequest($dataUpdateId, $address, $userId) {
        $dataUpdate = $this->getEntityManager()->getRepository(GenDataUpdate::class)->find($dataUpdateId);
        
        if (isset($address['newCep'])) {
            $userCurrentAddress = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['type' => $address['type'], 'genUser' => $dataUpdate->getGenUser(), 'nrorg' => $dataUpdate->getNrorg(), 'status' => 'A']);
            if ($userCurrentAddress) {
                $userCurrentAddress->setStatus('O');
                $this->getEntityManager()->persist($userCurrentAddress);
            }
            
            $genAddress = new GenAddress();
            $genAddress->setCep($address['newCep']);
            $genAddress->setStreet($address['newStreet']);
            $genAddress->setNeighborhood($address['newNeighborhood']);
            $genAddress->setCity($address['newCity']);
            $genAddress->setProvincy($address['newState']);
            $genAddress->setNumber($address['newNumber']);
            $genAddress->setComplement($address['newComplement']);
            $genAddress->setStatus('A');
            $genAddress->setGenUser($dataUpdate->getGenUser());
            $genAddress->setNrorg($dataUpdate->getNrorg());
            $genAddress->setType($address['type']);
            $this->getEntityManager()->persist($genAddress);
            
            $dataUpdate->setGenAddress($genAddress);
        }
        $dataUpdate->setApprovedBy($this->getEntityManager()->getRepository(GenUser::class)->find($userId));
        $dataUpdate->setUpdateStatus('A');
        $this->getEntityManager()->persist($dataUpdate);
        
        $this->getEntityManager()->flush();
        return $dataUpdate;
    }
    
    public function denyDataUpdateRequest($dataUpdateId, $userId) {
        $dataUpdate = $this->getEntityManager()->getRepository(GenDataUpdate::class)->find($dataUpdateId);
        
        $dataUpdate->setApprovedBy($this->getEntityManager()->getRepository(GenUser::class)->find($userId));
        $dataUpdate->setUpdateStatus('D');
        
        $this->getEntityManager()->flush();
    }
    
    public function updateUserProfileStatus($userId, $userTypeId, $nrorg, $status, $adminId) {
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);

        if ($userProfile) $userProfile->setStatus($status);
        else {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId)); 
            $userProfile->setNrorg($nrorg);
            $userProfile->setStatus($status);
            $userProfile->setCreatedBy($adminId);
            $this->getEntityManager()->persist($userProfile);
        }
        
        $userProfile->setModifiedBy($adminId);
        $this->getEntityManager()->flush();
    }

    public function createUser($firstName, $lastName, $cpf, $email, $phone, $password, $nrorg, $userTypeId = null, $birthDate = null, $image) {
        if ($birthDate) {
            $dateArray = explode("/", $birthDate);
            $formattedBirthDate = count($dateArray) === 3 ? new \DateTime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0]) : NULL;
        }
    
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        $birthDate ? $user->setBirthDate($formattedBirthDate) : null;
        $image     ? $user->setImage($image) : null;
        
        if($nrorg !== NULL && $nrorg != 0) {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            $userProfile->setPassword($this->cryptPassword($password));
            $userProfile->setEmail($email);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setStatus('A');
            $this->getEntityManager()->persist($userProfile);
        }
        
        if ($phone) {
            $contact = new GenContact();
            $contact->setGenUser($user);
            $contact->setType('MOBILE');
            $contact->setPhone($phone);
            
            $this->getEntityManager()->persist($contact);
        }
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    public function createUserWithExternalMethod($uid, $firstName, $lastName, $email, $phone, $nrorg, $userTypeId = null, $birthDate = null, $image = null) {
        if ($birthDate) {
            $dateArray = explode("/", $birthDate);
            $formattedBirthDate = count($dateArray) === 3 ? new \DateTime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0]) : NULL;
        }
    
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setFirebaseId($uid);
        $user->setStatus('A');
        $birthDate ? $user->setBirthDate($formattedBirthDate) : null;
        $image     ? $user->setImage($image) : null;
        
        if($nrorg !== NULL && $nrorg != 0) {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            $userProfile->setEmail($email);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setStatus('A');
            $this->getEntityManager()->persist($userProfile);
        }
        
        if ($phone) {
            $contact = new GenContact();
            $contact->setGenUser($user);
            $contact->setType('MOBILE');
            $contact->setPhone($phone);
            
            $this->getEntityManager()->persist($contact);
        }
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user;
        
    }
    
    
    public function updateUser($user, $firstName, $lastName, $cpf, $email, $password, $nrorg) {
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email); 
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $user->getId(), 'genUserType' => 3, 'nrorg' => $nrorg]);
        if(empty($userProfile)) {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, GenUserType::VENDEDOR_ID));
            $userProfile->setStatus('A');
            $this->getEntityManager()->persist($userProfile);
            $this->getEntityManager()->flush();
        }
        
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }
    
    public function updateFCMToken($userId, $token, $nrorg) {
        $organizationUserRel = $this->getEntityManager()->getRepository(GenOrganizationUserRel::class)->findOneBy(['genUser' => $userId, 'nrorg' => $nrorg]);
        
        if($organizationUserRel == NULL) { 
            $organizationUserRel = new GenOrganizationUserRel(); 
            $organizationUserRel->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $organizationUserRel->setStatus("A");
            $organizationUserRel->setNrorg($nrorg);
        }
        $organizationUserRel->setFirebaseToken($token);
        
        $this->getEntityManager()->persist($organizationUserRel);
        $this->getEntityManager()->flush();
        return $organizationUserRel;
    }
    
    /**
     * Se os paramentros a seguir for diferente de null, e porque quem esta
     * usando o metodo é o createUserV1.
     * @$email
     * @$password
     */
    public function addProfileUser($userId, $userTypeId, $nrorg, $status, $adminId, $email=null, $password=null) {
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
        
        if ($userProfile) {
            $userProfile->setStatus($status);
            $userProfile->setModifiedBy($adminId);
        } else {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setNrorg($nrorg);
            $userProfile->setStatus($status);
            $userProfile->setCreatedBy($adminId);
        }
        
        if($email != null || $password != null) {
            $userProfile->setPassword($this->cryptPassword($password));
            $userProfile->setEmail($email);
        }
        
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    public function addProfileUserAdmin($userId, $userTypeId, $nrorg, $status, $adminId, $email=null, $password=null) {
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
        
        if ($userProfile) {
            $userProfile->setStatus($status);
            $userProfile->setModifiedBy($adminId);
        } else {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setNrorg($nrorg);
            $userProfile->setStatus($status);
            $userProfile->setCreatedBy($adminId);
        }
        
        if($email != null || $password != null) {
            $userProfile->setPassword($this->cryptPassword($password));
            $userProfile->setEmail($email);
        }
        
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }

    public function getGenUser($email, $cpf) {
        $user = NULL;

        // if ($email) $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email, 'status' => 'A']);
        if ($user === NULL && $cpf) $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['cpf' => $cpf, 'status' => 'A']);
        
        return $user;
    }
    
    public function getGenUserAdmin($email, $cpf) {
        $user = NULL;
        //PROCURA POR STATUS A
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email, 'status' => 'A']);
        // if ($user === NULL && $cpf) $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['cpf' => $cpf, 'status' => 'A']);
        // PROCURA POR STATUS P
        if ($user === NULL && $email ) $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email, 'status' => 'P']);
        // if ($user === NULL && $cpf) $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['cpf' => $cpf, 'status' => 'P']);
        // CASO ENCONTRE, VERIFICA SE OS EMAIL'S SAO IGUAIS
        // if ($user !== NULL && ($user->getEmail() != $email && $user->getCpf() == $cpf)) throw Exception::cpfexistAndEmailIsdifferent();
        // if ($user !== NULL && ($user->getCpf() != $cpf && $user->getEmail() == $email)) throw Exception::emailexistAndCPFIsdifferent();
        
        return $user;
    }
    
    public function getGenUserTypeWB($userId, $userTypeId, $nrorg){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
    }
    
    public function getGenUserTypeWBAdmin($userId, $userTypeId, $nrorg){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
    }
    
    /**
     * Se os paramentros a seguir for diferente de null, e porque quem esta
     * usando o metodo é o createUserV1.
     * @$email
     * @$password
     */
    public function activateUserTypeRel($genUserTypeRel, $email=null, $password=null) {
        $genUserTypeRel->setStatus('A');
        
        if($email != null || $password != null) {
            $genUserTypeRel->setPassword($this->cryptPassword($password));
            $genUserTypeRel->setEmail($email);
        }
        $this->getEntityManager()->persist($genUserTypeRel);
        $this->getEntityManager()->flush();
    }
    
    public function activateUserTypeRelAdmin($genUserTypeRel, $email=null, $password=null) {
        $genUserTypeRel->setStatus('A');
        
        if($email != null || $password != null) {
            $genUserTypeRel->setPassword($this->cryptPassword($password));
            $genUserTypeRel->setEmail($email);
        }
        $this->getEntityManager()->persist($genUserTypeRel);
        $this->getEntityManager()->flush();
    }
  
    public function getUserFromOrganizationAndProfile($cpf, $npf) {
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u.id
            WHERE ((u.cpf = '$cpf' AND u.cpf <> '') OR utr.externalId = '$npf') 
            AND u.status = 'A' AND utr.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getWalletFromUserAndDependents($userId, $nrorg) {
        $genUser = GenUser::class;
        $payWallet = PayWallet::class;
        $genDependent = GenDependent::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $response = [];
        
        $parent = $this->getEntityManager()->createQuery(
            "
            SELECT pw FROM $payWallet pw LEFT JOIN pw.genUser gu WITH gu = $userId
            JOIN $genUserTypeRel utr WITH utr.genUser = gu
            WHERE gu.status = 'A' AND utr.status = 'A'
            ")->getResult();
        
        if(!empty($parent)) {
            foreach($parent as $value) {
                array_push($response, [
                    'PARENT_ID' => $value->getGenUser()->getId(),
                    'FULL_NAME' => $value->getGenUser()->getFirstName() ."". strrchr($value->getGenUser()->getLastName(), ' '),
                    'WALLET_ID' => $value->getId(),
                    'BALANCE' => $value->getBalance()
                ]);
            }
        } else {
            $user = $this->getEntityManager()->createQuery(
                "
                SELECT gu.id, CONCAT(gu.firstName, ' ', gu.lastName) name FROM $genUser gu
                WHERE gu.status = 'A' AND  gu.id = $userId
                ")->getResult();
            
            $temp = explode(" ",$user[0]["name"]);
            $nomeNovo = $temp[0] . " " . $temp[count($temp)-1];
            
            array_push($response, [
                    'PARENT_ID' => $user[0]["id"],
                    'FULL_NAME' => $nomeNovo,
                    'WALLET_ID' => 'not found',
                    'BALANCE' => 0.00
                ]);
        }
        
        $dependents = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT (SELECT pwp FROM $genUser gu
                            LEFT JOIN $payWallet pwp WITH gu = pwp.genUser
                            WHERE gu.id = $userId) walletIdParent, dr.id dependentId, dr.firstName, dr.lastName, pw.id walletId, pw.balance
            FROM $genDependent d
            JOIN d.parent pr
            JOIN d.dependent dr
            LEFT JOIN $payWallet pw WITH d.dependent = pw.genUser
            JOIN $genUserTypeRel utr WITH utr.genUser = dr
            JOIN utr.genUserType ut
            WHERE pr.id = $userId
            AND d.nrorg = $nrorg
            AND ut.id = 1
            AND dr.status = 'A'
            AND utr.status = 'A'
            "
        )->getResult();
        
        foreach ($dependents as $key => $dependent) {
            array_push($response, [
                'WALLET_ID_PARENT' => $dependent["walletIdParent"],
                'DEPENDENT_ID' => $dependent["dependentId"],
                'FULL_NAME' => $dependent['firstName'] ."". strrchr($dependent['lastName'], ' '),
                'WALLET_ID' => $dependent['walletId'],
                'BALANCE' => $dependent["balance"]
            ]);
        }
        
        return $response;
    }
    
    public function getUserDataById($nrorg, $userId, $userTypeId) {
        $genUser        = GenUser::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.cpf, u.email, utr.externalId
            FROM $genUser u
            JOIN $genUserTypeRel utr WITH utr.genUser = u.id
            WHERE u.id = $userId AND u.status = 'A' AND utr.status = 'A' 
            AND utr.genUserType = $userTypeId AND utr.nrorg = $nrorg
            "
        )->getResult();
        
        return $user;
    }
    
    public function getUserFromOrganizationAndExternalId($nrorg, $externalId) {
        $genUser        = GenUser::class;
        $genUserTypeRel = GenUserTypeRel::class;
        $genOrganizationUserRel = GenOrganizationUserRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id userId, our.firebaseToken
            FROM $genUser u
            JOIN $genUserTypeRel utr WITH(utr.externalId = '$externalId' AND utr.nrorg = '$nrorg' AND utr.genUser = u AND utr.status = 'A')
            JOIN $genOrganizationUserRel our WITH (utr.nrorg = our.nrorg AND u = our.genUser)
            WHERE u.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getDocumentApprovalRequests($nrorg, $status) {
        $genDocument        = GenDocument::class;
        $genDocUserRel      = GenDocUserRel::class;
        
        $documents = $this->getEntityManager()->createQuery(
            "
            SELECT gdur.id, gdur.document, gd.name as documentName
            FROM $genDocUserRel gdur
            JOIN $genDocument gd WITH gdur.genDocument = gd.id
            WHERE gdur.nrorg = '$nrorg'  
            AND gdur.status = '$status'
            "
        )->getResult();
        
        return $documents;        
    }
    
    public function stringToDate($array = []) {
		$dates = [];
		
        foreach($array as $key => $string) {
            $dateFormat = str_replace('/', '-', $string);
            date('Y-m-d', strtotime($dateFormat));
    		$date = new \DateTime($dateFormat);
    		$dates[$key] = $date->format('Y-m-d');
        }
        
        return $dates;
    }
    /*cria um professional*/
    public function createProfessional($firstName, $lastName, $initialTime, $finalTime, $cpf, $email, $password, $nrorg, $phoneNumber, $serviceIds, $adminId, $eventId, $image) {
        
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        $user->setCreatedBy($adminId);
        $user->setImage($image);
        
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, GenUserType::VENDEDOR_ID));
        $userProfile->setStatus('A');
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->persist($userProfile);
        // $this->getEntityManager()->persist($SerServiceSellerRel);
        $this->getEntityManager()->flush();
        
        $contact    = $this->addContactMethod($user->getId(), 'MOBILE', $phoneNumber);
        
        $evtEvent   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        
        $seller     = $this->createEvtEventSeller($evtEvent, $nrorg, $user->getId(), $adminId);
        
        if ($serviceIds && $eventId) {
            $this->persistSerServiceSellerRel($user->getId(), $serviceIds, $initialTime, $finalTime, $nrorg, null, $adminId, $eventId);
        }
        
        $user->build($this->getEntityManager(), 3, $nrorg);
        return $user;
    }
    
    /*atualiza um professional*/
    public function updateProfessional($clientId, $firstName, $lastName, $cpf, $email, $password, $nrorg, $phoneNumber, $serviceIds, $adminId, $eventId, $image, $status, $initialTime, $finalTime) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['id' => $clientId]);
        
        $firstName  ? $user->setFirstName($firstName)   : null;
        $lastName   ? $user->setLastName($lastName)     : null;
        $cpf        ? $user->setCpf($cpf)               : null;
        $email      ? $user->setEmail($email)           : null;
        $status     ? $user->setStatus($status)         : null;
        $adminId    ? $user->setCreatedBy($adminId)     : null;
        $image      ? $user->setImage($image)           : null;
        $password   ? $user->setPassword($this->cryptPassword($password)) : null;
        
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $user->getId()]);
        $userProfile->setNrorg($nrorg);
        $userProfile->setStatus('A');
        
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->merge($userProfile);
        $this->getEntityManager()->flush();
        
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $user->getId()]);
        
        if($contact) {
            $contact->setPhone($phoneNumber);
            $this->getEntityManager()->merge($contact);
            $this->getEntityManager()->flush();
        }else {
            $contact    = $this->addContactMethod($user->getId(), 'MOBILE', $phoneNumber);
        }
        
        if ($serviceIds && $eventId) {
            $this->persistSerServiceSellerRel($user->getId(), $serviceIds, $initialTime, $finalTime, $nrorg, null, $adminId, $eventId);
        }
        
        $user->build($this->getEntityManager(), 3, $nrorg);
        
        return $user;
    }
    
    /*cria relacionamentos entre um eventSeller e varios services*/
    function persistSerServiceSellerRel($userId, $serviceIds, $initialTime, $finalTime, $nrorg, $createdBy, $modifiedBy, $eventId) {
        $seller = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $userId, 'evtEvent' => $eventId]);
        
        if(empty($seller)){
            throw new Exception('seller not found', 14125);
        }
        
        $this->removeSerServiceSellerRel($seller->getId());
        
        foreach ($serviceIds as $serviceId) {
            $this->createServiceSellerRel($seller->getId(), $serviceId, $initialTime, $finalTime, $nrorg, $createdBy, $modifiedBy);
        }
        
        $this->getEntityManager()->flush();
    }
    
    /*remove relalionamentos entre eventSeller e serService*/
    function removeSerServiceSellerRel($sellerId) {
        $serviceSellerRels = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->findBy(['evtEventSeller' => $sellerId]);
        
        foreach ($serviceSellerRels as $val) {
            $val->setStatus('I');
            $this->getEntityManager()->merge($val);
        }
        $this->getEntityManager()->flush();
    }
    
    /*cria um relacionamento entre eventSeller e serService*/
    function createServiceSellerRel($sellerId, $serviceId, $initialTime, $finalTime, $nrorg, $createdBy, $modifiedBy) {
        $serviceSellerRel = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->findOneBy(['evtEventSeller' => $sellerId, 'serService' => $serviceId]);
        if(empty($serviceSellerRel)){
            $serviceSellerRel = new SerServiceSellerRel(); 
            $serviceSellerRel->setEvtEventSeller($this->getEntityManager()->getReference(EvtEventSeller::class, $sellerId));
            $serviceSellerRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        }
        
        $serviceSellerRel->setStatus('A');
        $serviceSellerRel->setInitialTime($initialTime);
        $serviceSellerRel->setFinalTime($finalTime);
        
        if($nrorg)
            $serviceSellerRel->setNrorg($nrorg);
        if($createdBy)
            $serviceSellerRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceSellerRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($serviceSellerRel);
        
        return $serviceSellerRel;
    }
    
    /*cria um eventSeller*/
    function createEvtEventSeller($evtEvent, $nrorg, $userId, $createdBy) {
        $evtEventSeller = new EvtEventSeller();
        
        $evtEventSeller->setEvtEvent($evtEvent);
        $evtEventSeller->setNrorg($nrorg);
        $evtEventSeller->setGenUser($this->getEntityManager()->getReference(EvtGenUser::class, $userId));
        $evtEventSeller->setCreatedBy($createdBy);
        $evtEventSeller->setStatus('A');
        $evtEventSeller->setEvtSellerType($this->getEntityManager()->getReference(EvtSellerType::class, 38));
        
        $this->getEntityManager()->persist($evtEventSeller);
        $this->getEntityManager()->flush();
        
        return $evtEventSeller;
    }
    
    function getLastGenUserToken($userId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        return ($user->getLastToken() != NULL) ? $user->getLastToken() : NULL;
    }
    
    function setLastGenUserToken($userId, $externalToken) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $user->setLastToken($externalToken);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    function setSkipPreferences($userId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);

        $user->setPreferences(1);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    public function activateUser($user, $modifieldBy) {
        $user->setStatus('A');
        $user->setModifiedBy($modifieldBy);
        $this->getEntityManager()->flush();
    }
    
    public function inactiveUser($user, $modifieldBy) {
        $user->setStatus('I');
        $user->setModifiedBy($modifieldBy);
        $this->getEntityManager()->flush();
    }
    
    public function enableOrDisableUser($userId, $nrorg, $modifieldBy) {
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM " . GenUser::class . " u
            JOIN " . GenUserTypeRel::class . " utr WITH utr.genUser = u.id
            WHERE u.id = $userId AND utr.nrorg = '$nrorg'
            "
        )->getResult();
        
        foreach($users as $user) {
            if($user != NULL) {
                if($user->getStatus() == "I") {
                    $this->activateUser($user, $modifieldBy);
                }else {
                    $this->inactiveUser($user, $modifieldBy);
                }
            }else {
                throw Exception::userNotFound();
            }
        }
    }
    
    function changePassword($adminId, $userId, $password) {
        $admin = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUserType' => 4, 'genUser' => $adminId, 'status' => 'A']);
        $user  = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if ($admin === NULL) throw new \Exception('Only an admin can update user data', 1);
        if ($user === NULL) throw new \Exception('User not found', 2);
        
        $user->setPassword($this->cryptPassword($password));
        $user->setModifiedBy($adminId);
        $user->setModifiedAt(new \DateTime());
        $this->getEntityManager()->flush();
    }
    
    public function getUsersAutoComplete($partialName, $nrorg, $userId) {
        $genUser        = GenUser::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $users = $this->getEntityManager()->createQuery("
            SELECT u.id, CONCAT(u.firstName, ' ', u.lastName) as fullName, gutr.externalId
            FROM $genUser u
            JOIN $genUserTypeRel gutr WITH (gutr.genUser = u AND gutr.status = 'A')
            WHERE u.status = 'A' AND CONCAT(u.firstName, ' ', u.lastName) like '$partialName%'
            ")->setMaxResults(20)->getResult();
        
        return $users;
    }
    
    public function createUserGuest($firstName, $lastName=null , $cpf=null, $email=null, $phoneNumber=null, $adminId, $status) {
        $user = new GenUser();
        
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setStatus($status);
        $user->setCreatedBy($adminId);
        $this->getEntityManager()->persist($user);
        
        $this->getEntityManager()->flush();
        
        if($phoneNumber){
            $contact    = $this->addContactMethod($user->getId(), 'MOBILE', $phoneNumber);
        }
        
        return $user->toArray();
    }
    
    public function DeleteUser ($row) {
        try {
            $nrorg = $row['NRORG'];
            $data = $row['DATA'];
            $userId = $data['ID'];
            $query =  "DELETE FROM GEN_USER_TYPE_REL WHERE nrorg = $nrorg AND gen_User_id = $userId";    
            $this->getEntityManager()->getConnection()->executeQuery($query);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    public function getGenUserByCpfOrEmail($cpf = null, $email = null, $nrorg = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        if($nrorg != null && $nrorg != 0){
            $queryCpf   =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') AND utr.nrorg = '$nrorg'" : "";
        } else {
            $queryCpf   =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        }
        
        $queryEmail     =  $email != NULL ? " AND u.email = '$email' " :  "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u.id userId, u.email, utr.nrorg, u.cpf , u.firstName, u.lastName, u.birthDate, utr.password
            FROM $genUser u
            JOIN $genUserType utr WITH utr.genUser = u.id 
            WHERE $queryCpf $queryEmail 
            AND u.status = 'A' 
            AND utr.status = 'A'
            AND utr.genUserType = '1'
            "
        )->getResult();
        return $users != null ? $users[0] : null;
    }
    
    public function getNameByCpf($cpf = null, $email = null, $nrorg = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        if($nrorg != null && $nrorg != 0){
            $queryCpf       =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') AND utr.nrorg = '$nrorg'" : "";
        } else {
            $queryCpf       =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
        }
        
        $queryEmail     =  $email != NULL ? " AND u.email = '$email' " :  "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u.id userId, u.email, utr.nrorg, u.cpf , u.firstName, u.lastName
            FROM $genUser u
            JOIN $genUserType utr WITH utr.genUser = u.id 
            WHERE $queryCpf $queryEmail 
            AND u.status = 'A' 
            AND utr.status = 'A'
            AND utr.genUserType = '1'
            "
        )->getResult();
        return $users != null ? $users[0] : null;
    }
    
    
    public function getGenUserByCpfOrEmailuserExistsByCpfAndReturnData($cpf = null, $email = null) {
        $genUser        = GenUser::class;
        $genUserType    = GenUserTypeRel::class;
        $replaceDql     = ReplaceDql::class;
        $config         = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $queryCpf       =  $cpf != NULL ? "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') " : "";
       // $queryEmail     =  $email != NULL ? " u.email = '$email' " :  "";
        
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u.id userId, utr.nrorg, u.cpf
            FROM $genUser u
            JOIN $genUserType utr WITH utr.genUser = u.id 
            WHERE $queryCpf
            "
        )->getResult();

        return $users != null ? $users[0] : null;
    }

    public function getUsersFromOrganizationWithStatusNotInative($nrorg,$userName,$cpf,$email,$telefone,$tipoUsuario, $itemsPerPage, $page) {
        $filtro = "";
     
        if($userName){ 
            $filtro .= " AND concat(u.firstName,' ',u.lastName) like '$userName'"; 
        }
        if($cpf){ 
            $filtro .= " AND u.cpf like " . "'".$cpf. "'"; 
        }
        if($email){ 
            $filtro .= " AND u.email like " . "'".$email. "'"; 
        }
        if($telefone){ 
            $filtro .= " AND uc.phone like " . "'".$telefone. "'"; 
        }
        if($tipoUsuario){
            $filtro .= " AND utr.genUserType in " . "(".$tipoUsuario. ")";
        }

        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenContact' uc WITH uc.genUser = u
            WHERE utr.nrorg = $nrorg AND u.status != 'I'
            $filtro
            GROUP BY u.id 
            "
        );
        
         $users->setFirstResult($itemsPerPage * ($page - 1));
        if ($itemsPerPage != NULL) $users->setMaxResults($itemsPerPage);
        
        $users = new \Doctrine\ORM\Tools\Pagination\Paginator($users);
        
        foreach ($users as $user) {
            $user->build($this->getEntityManager(), NULL, $nrorg);
        }
        
        return $users;
    }
    
    public function createUserAdmin($firstName, $lastName, $cpf, $email, $phone, $password, $nrorg, $userTypeId = null, $birthDate = null, $image) {
        if ($birthDate) {
            $dateArray = explode("/", $birthDate);
            $formattedBirthDate = count($dateArray) === 3 ? new \DateTime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0]) : NULL;
        }
    
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        $birthDate ? $user->setBirthDate($formattedBirthDate) : null;
        $image     ? $user->setImage($image) : null;
        
        if($nrorg !== NULL && $nrorg != 0) {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            // $userProfile->setPassword($this->cryptPassword($password));
            // $userProfile->setEmail($email);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setStatus('A');
            $this->getEntityManager()->persist($userProfile);
        }
        
        if ($phone) {
            $contact = new GenContact();
            $contact->setGenUser($user);
            $contact->setType('MOBILE');
            $contact->setPhone($phone);
            
            $this->getEntityManager()->persist($contact);
        }
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    public function updateUserStonePOS($email, $pos){
        $user = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email]);
        $user->setPosSerialNumber($pos);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user; 
    }
    
}
