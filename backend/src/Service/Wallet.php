<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\BpCreditcard;
use Zeedhi\ApiGeneral\Model\Entities\BpWallet;
use Zeedhi\ApiGeneral\Model\Entities\BpOrder;
use Zeedhi\ApiGeneral\Service\Order as OrderService;

use Doctrine\ORM\EntityManager;

class Wallet {

    /** @var OrderService */
    private $orderService;

    /**
     * @param OrderService
     */
    public function __construct(OrderService $orderService) {
        $this->orderService = $orderService;
    }
    
    /**
     * @param integer  $selectCardId
     * @param integer $buyingValue
     *
     * @return BpOrder
     */
    public function processOrder($selectCardId, $buyingValue) {
        $bpOrder = $this->orderService->processOrderWallet($selectCardId, $buyingValue);
        return $bpOrder;
    }
   
}