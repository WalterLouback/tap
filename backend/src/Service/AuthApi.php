<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\TokenAuthApi;

use Doctrine\ORM\EntityManager;

class AuthApi extends UserOperation {

    const SALT = 'BIPPOPSALT';

    /**
     * AuthApi constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }

    public function cryptPassword($password) {
        return strtoupper(hash('sha512', self::SALT.$password));
    }
    
    public function generateAccessToken($clientId) {
//      $this->setupOAuth($clientId);
// 		$token = $this->oauth->grantAccessToken($clientId, $this->clientSecret);
		$token = "TK_$clientId";
        return $token;
    }
    
    private function setupOAuth($clientId) {
		$storage = new FileImpl(__DIR__ . "/../cache", ".txt"); 
        $this->serviceProvider = new ServiceProviderImpl($clientId);
        
        $lifeTime = 10 * 60; //life time in seconds
		$this->oauth = new OAuthImpl($this->serviceProvider, $storage, time() + $lifeTime);
        
		$this->clientSecret = "SECRET";
    }    

    public function authtoken ($row) {
        try {
            $email = $row['EMAIL'];
            $password = $row['PASSWORD'];
            if (empty($email)) return ['error', 'e-mail cant be empty'];
            // if (empty($password)) return ['error', 'password cant be empty'];
            
            $cryptedPassword = $this->cryptPassword($password);
            $user = $this->getEntityManager()->getRepository(TokenAuthApi::class)->findOneBy(array("email" => $email));
            if ($user == null) throw Exception::incorrectPassword();
            else if ($user->getPassword() != $cryptedPassword) throw Exception::incorrectPassword();
            
            // $user->build($this->getEntityManager());
            $userId   = $user->getId();
            
            $tokenInBase = $user->getToken();
            if (!empty($tokenInBase)) {
                $createdAt = $user->getCreatedAt();
                $createdAt = $createdAt->format('Y/m/d H:i');
                $date = new \DateTime();
                $Now = $date->format('Y/m/d H:i');
                // Comparando as Datas
                if ((strtotime($Now) - strtotime($createdAt)) <= 7200) return ['success', 'Token: '. $tokenInBase];
            }
            $clientId = "USER_ID_$userId";
            $token    = $this->generateAccessToken($clientId);
            $user->setToken($token);
            $user->setCreatedAt();
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
            
            return ['success', 'Token: '. $token];
        } catch (\Exception $e ) {
            return ['error', $e->getMessage()];
        }
    }

}