<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenNews;

use Doctrine\ORM\EntityManager;

class News extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getLastNews($nrorg, $max, $showNotActiveNews) {
        $notActiveNews = $showNotActiveNews ? "OR n.active = false" : "";
        
        $news = $this->getEntityManager()->createQuery(
            "
            SELECT n
            FROM Zeedhi\ApiGeneral\Model\Entities\GenNews n
            WHERE n.nrorg = $nrorg
            AND ( n.active = true
            $notActiveNews )
            ORDER BY n.active DESC, 
            n.id DESC
            "
        );
        
        $news->setFirstResult(0);
        if ($max != NULL) $news->setMaxResults($max);
        
        $news = new \Doctrine\ORM\Tools\Pagination\Paginator($news);
        
        return $news; 
    }
    
    public function addNews($title, $news, $nrorg, $active, $userId, $image) {
        $genNews = new GenNews();
        $user    = $this->getEntityManager()->getRepository(GenUser::class)->find($userId); 
        
        $genNews->setTitle($title);
        $genNews->setNews($news);
        $genNews->setNrorg($nrorg);
        $genNews->setActive($active);
        $genNews->setGenUser($user);
        $genNews->setCreatedBy($userId);
        $genNews->setImage($image);
        
        $this->getEntityManager()->persist($genNews);
        $this->getEntityManager()->flush();
    }
    
    public function updateNews($newsId, $title, $news, $nrorg, $active, $userId, $image) {
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId); 
        $genNews    = $this->getEntityManager()->getRepository(GenNews::class)->find($newsId); 
        
        $genNews->setTitle($title);
        $genNews->setNews($news);
        $genNews->setNrorg($nrorg);
        $genNews->setActive($active);
        $genNews->setGenUser($user);
        $genNews->setCreatedBy($userId);
        if ($image != NULL && $image != '') $genNews->setImage($image);
        
        $this->getEntityManager()->persist($genNews);
        $this->getEntityManager()->flush();
    }
    
    public function inactivateNews($newsId, $nrorg, $userId) {
        $genNews    = $this->getEntityManager()->getRepository(GenNews::class)->findOneBy(['id' => $newsId, 'nrorg' => $nrorg]); 
        
        $genNews->setActive(0);
        $genNews->setModifiedBy($userId);
        
        $this->getEntityManager()->flush();
    }
    
    public function removeNews($newsId, $nrorg, $userId) {
        $genNews    = $this->getEntityManager()->getRepository(GenNews::class)->findOneBy(['id' => $newsId, 'nrorg' => $nrorg]); 
        
        $this->getEntityManager()->remove($genNews);
        $this->getEntityManager()->flush();
    }
    
    public function addNewsWebService($title, $news, $nrorg, $active, $image) {
        $genNews = new GenNews();
        
        $genNews->setTitle($title);
        $genNews->setNews($news);
        $genNews->setNrorg($nrorg);
        $genNews->setActive($active);
        $genNews->setImage($image);
        
        $this->getEntityManager()->persist($genNews);
        $this->getEntityManager()->flush();
    }
    
    public function getAllNews($nrorg) {
        return $this->getEntityManager()->getRepository(GenNews::class)->findBy(['nrorg' => $nrorg]);
    }
    
}
    