<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;
use Zeedhi\ApiGeneral\Model\Entities\PayGateway;
use Zeedhi\ApiGeneral\Model\Entities\PayGatewayRel;
use Zeedhi\ApiGeneral\Model\Entities\GenTaaConfiguration;
use Zeedhi\ApiGeneral\Model\Entities\EvtEvent;
use Zeedhi\ApiGeneral\Model\Entities\GenStructure;

use Doctrine\ORM\EntityManager;

class Organization extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getOrganizationData($userId, $nrorg, $userTypeId) {
        $organization = $this->getEntityManager()->createQuery(
            "
            SELECT o, c
            FROM " . GenOrganization::class . " o
            LEFT JOIN " . GenConfiguration::class . " c WITH c.nrorg = o.nrorg
            WHERE o.nrorg = $nrorg
            "
        )->getResult();
        $this->checkIfDependentCanAccessData($userId, $nrorg);

        if (count($organization) > 0) {
            $organization[0]->setEditableFields($this->getEntityManager());
            $organization[1]->build($this->getEntityManager());
        }
        
        return $organization;
    }
    
    public function getOrganizationDataAdmin($userId, $nrorg, $userTypeId) {
        $organization = $this->getEntityManager()->createQuery(
            "
            SELECT o, c
            FROM " . GenOrganization::class . " o
            LEFT JOIN " . GenConfiguration::class . " c WITH c.nrorg = o.nrorg
            WHERE o.nrorg = $nrorg
            "
        )->getResult();
        
        $this->checkIfDependentCanAccessData($userId, $nrorg);

        if (count($organization) > 0) {
            $organization[0]->setEditableFields($this->getEntityManager());
            $organization[1]->build($this->getEntityManager());
        }
        
        return $organization;
    }
    
    public function getOrganizationsData($cpf, $userTypeId) {
        $organizations = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT o.nrorg, o.name, c.logoImage, c.logoImageSmall, 
            c.primaryColor, c.secondaryColor, c.yesColor, c.noColor, 
            c.useIntegration, c.preferenceFlow, c.fcmServerKey, c.awsS3AccessKey, 
            c.awsS3SecretKey, c.awsS3Bucket, c.host, c.hostFrom, c.hostPassword, 
            c.hostFromName, c.googleAnalyticsKey
            FROM " . GenUser::class . " u
            LEFT JOIN " . GenUserTypeRel::class . " utr WITH u.id = utr.genUser
            LEFT JOIN " . GenOrganization::class . " o WITH o.nrorg = utr.nrorg 
            LEFT JOIN " . GenConfiguration::class . " c WITH c.nrorg = o.nrorg
            WHERE u.cpf = $cpf
            "
        )->getResult();
        
        return $organizations;
    }
    
    public function getAnalyticsKey($nrorg) {
        return $this->getEntityManager()->createQuery(
            "
            SELECT O.googleAnalyticsKey
            FROM " . GenConfiguration::class . " O
            WHERE O.nrorg = $nrorg
            "
        )->getResult();
    }
    
    private function checkIfDependentCanAccessData($userId, $nrorg) {
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM " . GenUser::class . " u
            WHERE u.id = $userId 
            "
        )->getResult()[0];
        
        
        $user->build($this->getEntityManager(), null, $nrorg);

        if ($user->getDependencyStatus() != 'A') {
            throw new Exception('User can not access the system', 24);
        }
    }
    
    private function validateUserType($userId, $nrorg, $userTypeId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->build($this->getEntityManager(), null, $nrorg);
        $user->getUserProfile($userTypeId, $nrorg);
    }
    
    public function updateOrganizationGateway($nrorg, $gatewayName, $merchant, $merchantId, $adminId) {
        $datetime   = new \DateTime();

        $storesGatewayRels = $this->getEntityManager()->getRepository(PayGatewayRel::class)->findBy(['nrorg' => $nrorg, 'genConfiguration' => NULL]);

        foreach ($storesGatewayRels as $gr) {
            if ($gr->getPayGateway()) {
                $gr->getPayGateway()->setMerchantKey(NULL);
                $gr->getPayGateway()->setMerchantId(NULL);
                $gr->getPayGateway()->setGateway($gatewayName);
            }
        }
        
        $configuration    = $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
        $orgGatewayRel    = $this->getEntityManager()->getRepository(PayGatewayRel::class)->findOneBy(['genConfiguration' => $configuration]);
        
        $gateway = new PayGateway();
        $gateway->setId($datetime->getTimestamp());
        $gateway->setGateway($gatewayName);
        $gateway->setMerchantKey($merchant);
        $gateway->setMerchantId($merchantId);
        $gateway->setCreatedBy($adminId);
        $this->getEntityManager()->persist($gateway);
        
        if ($orgGatewayRel) {
            $orgGatewayRel->setPayGateway($gateway);
            $orgGatewayRel->setModifiedBy($adminId);
        } else {
            $orgGatewayRel = new PayGatewayRel();
            $orgGatewayRel->setId($datetime->getTimestamp());
            $orgGatewayRel->setGenConfiguration($configuration);
            $orgGatewayRel->setPayGateway($gateway);
            $orgGatewayRel->setNrorg($configuration->getNrorg());
            $orgGatewayRel->setCreatedBy($adminId);
            $this->getEntityManager()->persist($orgGatewayRel);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function getNrorg($nrorg) {
        $organization = $this->getEntityManager()->getRepository(GenOrganization::class)->find($nrorg);
        
        return $organization;
    }
    
    public function createConfigurationNrorg($logoImage, $logoImageSmall, $primaryColor, $secondaryColor, $yesColor, $noColor, $useIntegration, $preferenceFlow, $nrorg, $aws_s3_access_key, $aws_s3_bucket, $aws_s3_secret_key, $fcm_server_key, $host, $host_from, $host_from_name, $host_password, $googleAnalyticsKey, $appLayoutConfig, $allowExternalUsers, $keyGoogle, $urlApiExternal, $tokenApiExternal, $originApiExternal, $overlayImage) {
        $configuration = new GenConfiguration();
        
        if($logoImage)
            $configuration->setLogoImage($logoImage);
        if($logoImageSmall)
            $configuration->setLogoImageSmall($logoImageSmall);
        if($primaryColor)
            $configuration->setPrimaryColor($primaryColor);
        if($secondaryColor)
            $configuration->setSecondaryColor($secondaryColor);
        if($overlayImage)
            $configuration->setOverlayBackground($overlayImage);
        // if($yesColor)
        //     $configuration->setYesColor($yesColor);
        // if($noColor)
        //     $configuration->setNoColor($noColor);
        // if($useIntegration)
        //     $configuration->setUseIntegration($useIntegration);
        // if($preferenceFlow)
        //     $configuration->setPreferenceFlow($preferenceFlow);
        // if($nrorg)
        //     $configuration->setNrorg($nrorg);
        // if($aws_s3_access_key)
        //     $configuration->setAwsS3AccessKey($aws_s3_access_key);
        // if($aws_s3_bucket)
        //     $configuration->setAwsS3Bucket($aws_s3_bucket);
        // if($aws_s3_secret_key)
        //     $configuration->setAwsS3SecretKey($aws_s3_secret_key);
        // if($fcm_server_key)
        //     $configuration->setFcmServerKey($fcm_server_key);
        // if($host)
        //     $configuration->setHost($host);
        // if($host_from)
        //     $configuration->setHostFrom($host_from);
        // if($host_from_name)
        //     $configuration->setHostFromName($host_from_name);
        // if($host_password)
        //     $configuration->setHostPassword($host_password);
        // if($googleAnalyticsKey)
        //     $configuration->setGoogleAnalyticsKey($googleAnalyticsKey);
        // if($appLayoutConfig)
        //     $configuration->setAppLayoutConfig($appLayoutConfig);
        // if($allowExternalUsers)
        //     $configuration->setAllowExternalUsers($allowExternalUsers);
        // if($keyGoogle)
        //     $configuration->setKeyGoogle($keyGoogle);
        if($urlApiExternal)
            $configuration->seturlApiExternal($urlApiExternal);
        if($tokenApiExternal)
            $configuration->settokenApiExternal($tokenApiExternal);
        if($originApiExternal)
            $configuration->setOriginApiExternal($originApiExternal);
       
        
        $this->getEntityManager()->persist($configuration);
        $this->getEntityManager()->flush();
        return $configuration;
    }
    
    public function updateConfigurationNrorg($configuration, $logoImage, $logoImageSmall, $primaryColor, $secondaryColor, $yesColor, $noColor, $useIntegration, $preferenceFlow, $aws_s3_access_key, $aws_s3_bucket, $aws_s3_secret_key, $fcm_server_key, $host, $host_from, $host_from_name, $host_password, $googleAnalyticsKey, $appLayoutConfig , $allowExternalUsers, $keyGoogle, $urlApiExternal, $tokenApiExternal, $originApiExternal, $overlayImage) {
        
        $configuration->setLogoImage($logoImage);
        $configuration->setLogoImageSmall($logoImageSmall);
        $configuration->setPrimaryColor($primaryColor);
        $configuration->setSecondaryColor($secondaryColor);
        $configuration->setSecondaryColor($secondaryColor);
        $configuration->setOverlayBackground($overlayImage);


        // USUSARIO NAO PODERA SETAR ESTE3S DADOS PELO ADMIN, SERA CRIADO CONJUNTA A CRIACAO DA ORGANIZACAO
        // $configuration->setYesColor($yesColor);
        // $configuration->setNoColor($noColor);
        // $configuration->setUseIntegration($useIntegration);
        // $configuration->setPreferenceFlow($preferenceFlow);
        // $configuration->setAwsS3AccessKey($aws_s3_access_key);
        // $configuration->setAwsS3Bucket($aws_s3_bucket);
        // $configuration->setAwsS3SecretKey($aws_s3_secret_key);
        // $configuration->setFcmServerKey($fcm_server_key);
        // $configuration->setHost($host);
        // $configuration->setHostFrom($host_from);
        // $configuration->setHostFromName($host_from_name);
        // $configuration->setHostPassword($host_password);
        // $configuration->setGoogleAnalyticsKey($googleAnalyticsKey);
        // $configuration->setAppLayoutConfig($appLayoutConfig);
        // $configuration->setAllowExternalUsers($allowExternalUsers);
        // $configuration->setKeyGoogle($keyGoogle);
        
        $configuration->seturlApiExternal($urlApiExternal);
        $configuration->settokenApiExternal($tokenApiExternal);
        $configuration->setOriginApiExternal($originApiExternal);

        $this->getEntityManager()->persist($configuration);
        $this->getEntityManager()->flush();
        return $configuration;
    }
    
    public function getConfigurationByNrorg($nrorg) {
        $configuration = $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
        return $configuration;
    }
    
    public function getUseIntegration() {
        $genConfiguration = GenConfiguration::class;
        
        $useIntegration = $this->getEntityManager()->createQuery("
            SELECT c.useIntegration
            from $genConfiguration c
        ")->setMaxResults(1)->getSingleResult();
        
        return $useIntegration;
    }
    
    public function getOrganizationDataByURL($url) {
        $new_url = stripcslashes($url);
        $search = 'rc-' ;
        $trimmed_url = str_replace($search, '', $new_url) ;
        
        $organization = $this->getEntityManager()->createQuery(
            "
            SELECT o, c
            FROM " . GenOrganization::class . " o
            LEFT JOIN " . GenConfiguration::class . " c WITH c.nrorg = o.nrorg
            WHERE c.url = '$trimmed_url'
            "
        )->getResult();

        if (count($organization) > 0) {
            $organization[0]->setEditableFields($this->getEntityManager());
            $organization[1]->build($this->getEntityManager());
        }
        else throw new Exception('Organization not found', 24);
        return $organization;
    }
    
    public function updateOrganizationStoneID($nrorg, $stoneId){
        $configuration = $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
        $configuration->setStoneEstablishmentId($stoneId);

        $this->getEntityManager()->persist($configuration);
        $this->getEntityManager()->flush();
        return $configuration;
    }
    
    /*TAA's*/
    public function getAvailableTaa($nrorg, $structureId, $status) {
        $genTaaConfigurations = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->findBy(['nrorg' => $nrorg, 'structureId' => $structureId, 'status' => $status]);
       
        $result = [];
        
        foreach ($genTaaConfigurations as $value) {
              array_push($result, $value->toArray());  
        }
        
        return $result;
    }
    
    private function prepareDataTaa($genTaaConfiguration, $nrorg = null, $name = null, $modality = null, $posSerialNumber = null, $posReferenceId = null, $externalApiUrl = null, $externalApiId = null, $structureId = null, $storeId = null, $status = null, $createdBy = null, $modifiedBy = null) {
        
        $genTaaConfiguration->setNrorg($nrorg);
        $genTaaConfiguration->setName($name);
        $genTaaConfiguration->setModality($modality);
        $genTaaConfiguration->setPosSerialNumber($posSerialNumber);
        $genTaaConfiguration->setPosReferenceId($posReferenceId);
        $genTaaConfiguration->setExternalApiUrl($externalApiUrl);
        $genTaaConfiguration->setExternalApiId($externalApiId);
        $genTaaConfiguration->setCreatedBy($createdBy);
        $genTaaConfiguration->setModifiedBy($modifiedBy);
        $genTaaConfiguration->setStatus($status);
        $structureId ? $genTaaConfiguration->setStructureId($this->getEntityManager()->getReference(GenStructure::class, $structureId)) : $genTaaConfiguration->setStructureId(null);
        $storeId ? $genTaaConfiguration->setStoreId($this->getEntityManager()->getReference(EvtEvent::class, $storeId)) : $genTaaConfiguration->setStoreId(null);
        
        return $genTaaConfiguration;
    }
    
    public function createTaaConfiguration($nrorg = null, $name = null, $modality = null, $posSerialNumber = null, $posReferenceId = null, $externalApiUrl = null, $externalApiId = null, $structureId = null, $storeId = null, $status = null, $createdBy = null) {
        $genTaaConfiguration = new GenTaaConfiguration();
        
        $taaConfiguration = $this->prepareDataTaa($genTaaConfiguration, $nrorg, $name, $modality, $posSerialNumber, $posReferenceId, $externalApiUrl, $externalApiId, $structureId, $storeId, $status, $createdBy, null);
        
        $this->getEntityManager()->persist($taaConfiguration);
        $this->getEntityManager()->flush();
        
        return $taaConfiguration->toArray();
    }
    
    public function getTaaConfiguration($nrorg, $structureId = null, $status = null) {
        if ($status && $structureId) {
            $genTaaConfigurations = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->findBy(['nrorg' => $nrorg, 'structureId' => $structureId, 'status' => $status]);
        } elseif ($status && !$structureId) {
            $genTaaConfigurations = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->findBy(['nrorg' => $nrorg, 'status' => $status]);
        }
        else {
            $genTaaConfigurations = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->findBy(['nrorg' => $nrorg]);
        }
        
        $result = [];
        
        foreach ($genTaaConfigurations as $value) {
            array_push($result, $value->toArray());
        }
        
        return $result;
    }
    
    public function updateTaaConfiguration($taaId, $nrorg = null, $name = null, $modality = null, $posSerialNumber = null, $posReferenceId = null, $externalApiUrl = null, $externalApiId = null, $structureId = null, $storeId = null, $status = null, $modifiedBy = null) {
        $genTaaConfiguration = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->find($taaId);
        
        $taaConfiguration = $this->prepareDataTaa($genTaaConfiguration, $nrorg, $name, $modality, $posSerialNumber, $posReferenceId, $externalApiUrl, $externalApiId, $structureId, $storeId, $status, null, $modifiedBy);
        
        $this->getEntityManager()->merge($taaConfiguration);
        $this->getEntityManager()->flush();
        
        return $taaConfiguration->toArray();
    }
    
    public function getStoresToTaa($nrorg, $status = null, $storeName = null) {
        if($storeName != null) {
            $stores = $this->getEntityManager()->createQuery(
                "
                SELECT e
                FROM " . EvtEvent::class . " e
                WHERE e.TYPE = 'S' AND e.nrorg = '$nrorg' AND e.name LIKE '$storeName'
                "
            )->getResult();
        } else {
            $stores = $this->getEntityManager()->getRepository(EvtEvent::class)->findBy(['nrorg' => $nrorg, 'type' => 'S']);
        }
        $result = array();
        
        foreach($stores as $store) {
            array_push($result, $store->toArray());
        }
        
        return $result;
    }

    function updateReferenceIdOrSerialNumber($taaId, $posSerialNumber, $posReferenceId) {
        $genTaaConfiguration = $this->getEntityManager()->getRepository(GenTaaConfiguration::class)->find($taaId);

        $posSerialNumber ? $genTaaConfiguration->setPosSerialNumber($posSerialNumber) : null;
        $posReferenceId ? $genTaaConfiguration->setPosReferenceId($posReferenceId) : null;

        $this->getEntityManager()->persist($genTaaConfiguration);
        $this->getEntityManager()->flush();
        
        return $genTaaConfiguration->toArray();
    }
}