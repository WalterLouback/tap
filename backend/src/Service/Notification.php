<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenNotification;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiSurvey\Model\Entities\SurSurvey;
use Zeedhi\ApiSurvey\Model\Entities\SurUserAnswer;
use Zeedhi\ApiEvents\Model\Entities\EvtEventFavorite;

use Zeedhi\ApiGeneral\Helpers\FCM;

use Doctrine\ORM\EntityManager;

class Notification extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function setNotification($id, $userId, $nrorg) {
        $notification = $this->getEntityManager()->getRepository(GenNotification::class)->findOneBy(['id' => $id, 'genUser' => $userId, 'nrorg' => $nrorg, 'status' => "P"]);
        
        $notification->setStatus("L");
        $this->getEntityManager()->flush();
        
        return $notification;
    }
    
    public function setReport($idReport, $userId, $nrorg) {
        $report = $this->getEntityManager()->getRepository(GenNotification::class)->findOneBy(['id' => $idReport, 'genUser' => $userId, 'nrorg' => $nrorg, 'status' => "L"]);
        
        if(empty($report)) {
            $report = $this->getEntityManager()->getRepository(GenNotification::class)->findOneBy(['id' => $idReport, 'genUser' => $userId, 'nrorg' => $nrorg, 'status' => "C"]);
            $report->setStatus("L");
            $this->getEntityManager()->flush();        
        } else if($report) {
            $report->setStatus("C");
            $this->getEntityManager()->flush();
        } else {
            throw new Exception('Erro!');
        }
        return $report;
    }
    
    /* Esse método retorna notificações dos ultimos 15 dias! */
    public function getNotificationsByUser($userId, $dateTime, $nrorg) {
        $genNotification  = GenNotification::class;
        $surSurvey        = SurSurvey::class;
        $surUserAnswer    = SurUserAnswer::class;
        $evtEventFavorite = EvtEventFavorite::class;
        
        $notifications = $this->getEntityManager()->createQuery("
            SELECT gn.id, 'Sobre seu pedido' as name, gn.message as description, gn.createdBy, gn.title, gn.dateOfCommitment, gn.urlImage, gn.category, gn.externalId as externalId, gn.confirmation, gn.status, gn.originNotification as origin, gn.dateTime as createdAt, gn.dateTime as initialDate
            FROM $genNotification gn
            WHERE gn.nrorg = $nrorg
            AND gn.genUser = $userId 
            AND gn.dateTime >= '$dateTime'
            AND gn.originNotification = 'Order_Orderbackend'
            ORDER BY gn.id DESC
        ")->getResult();
        
        $surveys = $this->getEntityManager()->createQuery("
            SELECT s.id, 'Nova pesquisa de satisfação' as name, s.name as description, s.status, 'survey' as origin, s.modifiedAt as createdAt, s.modifiedAt as initialDate
            FROM $surSurvey s
            WHERE s.nrorg = $nrorg
            AND s.status = 'A'
            AND s.createdAt >= '$dateTime'
            AND s.id NOT IN (
                SELECT s2.id
                FROM $surUserAnswer ua
                JOIN ua.surQuestion q
                JOIN q.surSurvey s2
                WHERE s2.id = s.id
                AND ua.genUser = $userId
            )
            ORDER BY s.id DESC
        ")->getResult();
        
        $reports = $this->getEntityManager()->createQuery("
            SELECT gn.id, 'Novo comunicado' as name, gn.message as description, gn.status, gn.originNotification as origin, gn.dateTime as createdAt, gn.dateTime as initialDate, gn.externalId as externalId, gn.confirmation as confirmation, gn.title as title, gn.dateOfCommitment as dateOfCommitment, gn.category as category, gn.urlImage as urlImage 
            FROM $genNotification gn
            WHERE gn.nrorg = $nrorg
            AND gn.genUser = $userId
            AND gn.originNotification = 'gen_api_external'
            ORDER BY gn.id DESC
        ")->getResult();
        
        $favoritedEvents = $this->getEntityManager()->createQuery("
            SELECT gn.id, 'Está chegando!' as name, gn.message as description, gn.status, gn.originNotification as origin, gn.dateTime as createdAt, gn.dateTime as initialDate, gn.externalId as externalId, gn.confirmation as confirmation, gn.title as title, gn.dateOfCommitment as dateOfCommitment, gn.category as category, gn.urlImage as urlImage 
            FROM $genNotification gn
            WHERE gn.nrorg = $nrorg
            AND gn.genUser = $userId
            AND gn.originNotification = 'Evt_EventFavorite'
            ORDER BY gn.id DESC
        ")->getResult();
        
        $mergedArray = $this->orderNotificationsArray(array_merge($notifications, $surveys, $reports, $favoritedEvents));
        
        return $mergedArray;
    }
    public function getReportsByUser($userId, $nrorg) {
        $genNotification = GenNotification::class;
        
        $reports = $this->getEntityManager()->createQuery("
            SELECT gn.id, 'Novo comunicado' as name, gn.message as description, gn.status, gn.originNotification as origin, gn.dateTime as createdAt, gn.dateTime as initialDate, gn.externalId as externalId, gn.confirmation as confirmation, gn.title as title, gn.dateOfCommitment as dateOfCommitment, gn.category as category, gn.urlImage as urlImage 
            FROM $genNotification gn
            WHERE gn.nrorg = $nrorg
            AND gn.genUser = $userId
            AND gn.originNotification = 'gen_api_external'
            ORDER BY gn.id DESC
        ")->getResult();
        
        return $reports;
    }
    
    private function orderNotificationsArray($notificationsArray) {
        $date = array_column($notificationsArray, 'createdAt');
        array_multisort($date, SORT_DESC, $notificationsArray);
        return $notificationsArray;
    }
    
    public function inserirNotification($status, $message, $nrorg, $userId, $originNotification, 
                                            $dateTime, $notificationData, $title, $firebaseToken = null, 
                                            $externalId = null, $confirmation = null, $dateOfCommitment = null, $category = null, $urlImage = null, $userAutorId = null) {
        $notification = new GenNotification();

        if($status)
            $notification->setStatus($status);
        if($message)
            $notification->setMessage($message);
        if($nrorg)
            $notification->setNrorg($nrorg);
        if($userId) {
            $notification->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        }
        if($userAutorId) {
            $notification->setCreatedBy($userAutorId);
        }
        if($originNotification)
            $notification->setOriginNotification($originNotification);
        if($dateTime)
            $notification->setDateTime($dateTime);
        if($dateOfCommitment)
            $notification->setDateOfCommitment($dateOfCommitment);
        if($externalId != null)
            $notification->setExternalId($externalId);
        if($confirmation != null)
            $notification->setConfirmation($confirmation);
        if($title)
            $notification->setTitle($title);
        if($urlImage)
            $notification->setUrlImage($urlImage);
        if($category)
            $notification->setCategory($category);
        
        $this->getEntityManager()->persist($notification);
        $this->getEntityManager()->flush();
       
        $fcm = new FCM($nrorg, $this->getEntityManager());
        
        if($notificationData != NULL) {
            $fcm->sendNotificationToApp($firebaseToken, $title, $message, $notificationData);
        }else {
            $fcm->sendNotificationToApp($firebaseToken, $title, $message, null);
        }
        
        return $notification;
    }

}