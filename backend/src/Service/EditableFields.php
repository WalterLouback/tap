<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenPermission;
use Zeedhi\ApiGeneral\Model\Entities\GenEditableFields;

use Doctrine\ORM\EntityManager;

class EditableFields extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }

    function getEditableFields($nrorg) {
        $editableFields = $this->getEntityManager()->getRepository(GenEditableFields::class)->findBy(['nrorg' => $nrorg]);
        
        foreach ($editableFields as $ef) {
            $ef->build($this->getEntityManager());
        }
        
        return $editableFields;
    }
    
    public function getPermissions() {
        $permissions = $this->getEntityManager()->createQuery(
            'SELECT p.id, p.name
            FROM Zeedhi\ApiGeneral\Model\Entities\GenPermission p
            GROUP BY p.name
            '
        )->getResult();

        return $permissions;
    }
    
    public function getUserTypes($nrorg) {
        $userTypes = $this->getEntityManager()->createQuery(
            "SELECT u.id, u.name
            FROM Zeedhi\ApiGeneral\Model\Entities\GenUserType u
            WHERE   u.nrorg = $nrorg OR u.nrorg = 0 
            "
        )->getResult();

        return $userTypes;
    }
    
    public function createEditableField($fieldName, $permission, $userType, $userId, $nrorg) {
        $genEditableFields = new GenEditableFields();
        $genEditableFields->setFieldName($fieldName);
        $genEditableFields->setCreatedBy($userId);
        $genEditableFields->setNrorg($nrorg);
        $genEditableFields->setGenPermission($this->getEntityManager()->getReference(GenPermission::class, $permission));
        $genEditableFields->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userType));
        $this->getEntityManager()->persist($genEditableFields);
        $this->getEntityManager()->flush();
        
        return $genEditableFields;
    }
    
    public function getEditableFielById($id, $nrorg) {
        $editableField = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['id' => $id, 'nrorg' => $nrorg]);
        return $editableField; 
    }
    
        
    public function updateEditableField($genEditableFields, $fieldName, $permission, $userType, $userId, $nrorg) {
        $genEditableFields->setFieldName($fieldName);
        $genEditableFields->setModifiedBy($userId);
        $genEditableFields->setNrorg($nrorg);
        $genEditableFields->setGenPermission($this->getEntityManager()->getReference(GenPermission::class, $permission));
        $genEditableFields->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userType));
        $this->getEntityManager()->flush();
        
        return $genEditableFields;
    }
    
    public function removeEditableField($id) {
        $editableFields = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['id' => $id]);
        if ($editableFields == NULL)
            throw new \Exception('GenEditableFields not found', 1);    
        $this->getEntityManager()->remove($editableFields);
        $this->getEntityManager()->flush();

    }
}