<?php
namespace Zeedhi\ApiGeneral\Service;

use Zeedhi\ApiGeneral\Model\Entities\GenActivityRequest;
use Zeedhi\ApiGeneral\Model\Entities\GenSupportRequest;
use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenSubjectMatter;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiGeneral\Helpers\Util;

class SupportRequest extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getSupportRequestsByUser($nrorg, $genUser) {
        $supportRequests = $this->getEntityManager()->getRepository(GenSupportRequest::class)->findBy(['nrorg' => $nrorg, 'genUser' => $genUser]);
        
        foreach ($supportRequests as $supportRequest) {
            $supportRequest->build($this->getEntityManager());
        }
        
        return $supportRequests;
    }
    
    public function getSupportRequestsFilter($nrorg, $genUser, $initialTime = NULL, $finalTime = NULL, $structureId = NULL, $status = NULL, $subjectMatterId = NULL) {
        $genSupportRequest  = GenSupportRequest::class;
        $genStructure       = GenStructure::class;
        $genSubjectMatter   = GenSubjectMatter::class;
        $genActivityRequest = GenActivityRequest::class;
        
        $queryInitialTime   = ($initialTime != NULL)      ? "AND sp.initialTime >= $initialTime"          : "";
        $queryFinalTime     = ($finalTime != NULL)        ? "AND sp.finalTime >= $finalTime"              : "";
        $queryStatus        = ($status != NULL)           ? "AND sp.status = $status"                     : "";
        $queryStructure     = ($structureId != NULL)      ? "AND sp.genStructure = $structureId"          : "";
        $querySubjectMatter = ($subjectMatterId != NULL)  ? "AND sp.genSubjectMatter = $subjectMatterId"  : "";
        
        $supportRequest = $this->getEntityManager()->createQuery(
                "
                    SELECT sp
                    FROM $genSupportRequest sp
                    LEFT JOIN $genStructure s WITH s.id = sp.genStructure
                    LEFT JOIN $genSubjectMatter sr WITH sr.id = sp.genSubjectRequest
                    LEFT JOIN $genActivityRequest ar WITH sp.id = ar.genSupportRequest
                    WHERE sp.nrorg = $nrorg
                    $queryInitialTime   
                    $queryFinalTime     
                    $queryStatus        
                    $queryStructure     
                    $querySubjectMatter
                    AND sr.status = 'A'
                    ORDER BY sp.id DESC
                "
            );
        
        foreach ($supportRequests as $supportRequest) {
            $supportRequest->build($this->getEntityManager());
        }
        
        return $supportRequests;
    }
    
    public function createSupportRequest($nrorg, $userId, $userIdEmployee, $initialTime = NULL, $finalTime = NULL, $structureId = NULL, $status = NULL, $subjectMatterId = NULL) {
        $supportRequest     = new GenSupportRequest();
        
        $genUser            = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $userIdEmployee     ? $genUserEmployee = $this->getEntityManager()->getRepository(GenUser::class)->find($userIdEmployee) : NULL;
        $genStructure       = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
        $genSubjectMatter   = $this->getEntityManager()->getRepository(GenSubjectMatter::class)->find($subjectMatterId);
        
        $supportRequest->setNrorg($nrorg);
        $supportRequest->setGenUser($genUser);
        $supportRequest->setGenStructure($genStructure);
        $supportRequest->setGenSubjectMatter($genSubjectMatter);
        
        isset($genUserEmployee) ? $supportRequest->setGenUserEmployee($genUserEmployee) : NULL;
        $initialTime != NULL ? $supportRequest->setInitialTime($initialTime) : $supportRequest->setInitialTime(new \DateTime());
        $finalTime   != NULL ? $supportRequest->setFinalTime($finalTime) : NULL;
        $status      != NULL ? $supportRequest->setStatus($status) : NULL;
        
        $this->getEntityManager()->persist($supportRequest);
        $this->getEntityManager()->flush();
        
        return $supportRequest;
    }    
    
    public function updateSupportRequest($supportRequestId, $nrorg, $userId, $userIdEmployee, $initialTime = NULL, $finalTime = NULL, $structureId = NULL, $status = NULL, $subjectMatterId = NULL) {
        $supportRequest     = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
        
        $genUser            = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $genUserEmployee    = $this->getEntityManager()->getRepository(GenUser::class)->find($userIdEmployee);
        $genStructure       = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
        $genSubjectMatter   = $this->getEntityManager()->getRepository(GenSubjectMatter::class)->find($subjectMatterId);
        
        $supportRequest->setNrorg($nrorg);
        $supportRequest->setGenUser($genUser);
        $supportRequest->setGenUserEmployee($genUserEmployee);
        $supportRequest->setGenStructure($genStructure);
        $supportRequest->setGenSubjectMatter($genSubjectMatter);
        $initialTime != NULL ? $supportRequest->setInitialTime($initialTime) : NULL;
        $finalTime   != NULL ? $supportRequest->setFinalTime($finalTime) :  $supportRequest->setFinalTime(new \DateTime());
        $status      != NULL ? $supportRequest->setStatus($status) : NULL;
        
        $this->getEntityManager()->persist($supportRequest);
        $this->getEntityManager()->flush();
        
        return $supportRequest;
    }
    
    public function createActivityRequest($nrorg, $userId, $type = NULL, $description = NULL, $file = NULL, $status = NULL, $supportRequestId = NULL, $activityRequestOldId = NULL) {
        
        $activityRequest = new GenActivityRequest();

        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if($supportRequestId != NULL) {
            $genSupportRequest = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
            $activityRequest->setGenSupportRequest($genSupportRequest);
        }
        
        if($activityRequestOldId != NULL) {
            $genActivityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestOldId);
            $activityRequest->setGenActivityRequest($genActivityRequest);
        }

        $activityRequest->setNrorg($nrorg);
        $activityRequest->setGenUser($genUser);
        $type           != NULL ? $activityRequest->setType($type) : NULL;
        $description    != NULL ? $activityRequest->setDescription($description): NULL;
        $file           != NULL ? $activityRequest->setFile($file) : NULL;
        $status         != NULL ? $activityRequest->setStatus($status) : NULL;
        
        $this->getEntityManager()->persist($activityRequest);
        $this->getEntityManager()->flush();
        
        return $activityRequest;
    }
    
    public function updateActivityRequest($activityRequestId, $nrorg, $userId, $type = NULL, $description = NULL, $file = NULL, $status = NULL, $supportRequestId = NULL, $activityRequestOldId = NULL) {
        
        $activityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestId);
        
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        if($supportRequestId != NULL) {
            $genSupportRequest = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
            $activityRequest->setGenSupportRequest($genSupportRequest);
        }
        
        if($activityRequestOldId !== NULL) {
            $genActivityRequest = $this->getEntityManager()->getRepository(GenActivityRequest::class)->find($activityRequestOldId);
            $activityRequest->setGenActivityRequest($genActivityRequest);
        }
        
        $activityRequest->setNrorg($nrorg);
        $activityRequest->setGenUser($genUser);
        $activityRequest->setType($type);
        $activityRequest->setDescription($description);
        $activityRequest->setFile($file);
        $activityRequest->setStatus($status);
        
        $this->getEntityManager()->persist($activityRequest);
        $this->getEntityManager()->flush();
    }
    
    public function cancellationSupportRequest($supportRequestId, $userId, $nrorg) {
        
        $supportRequest = $this->getEntityManager()->getRepository(GenSupportRequest::class)->findOneBy(['id' => $supportRequestId, 'nrorg' => $nrorg]);
        
        $genUser        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $supportRequest->setStatus("I");
        $supportRequest->setGenUser($genUser);
        
        $this->getEntityManager()->persist($supportRequest);
        $this->getEntityManager()->flush();
    }
    
    public function getSubjectMatters($nrorg) {
        return $this->getEntityManager()->getRepository(GenSubjectMatter::class)->findBy(['nrorg' => $nrorg, 'status' => 'A']);
    }
    
    public function getSubjectMatter($subjectMatterId) {
        return $this->getEntityManager()->getRepository(GenSubjectMatter::class)->find($subjectMatterId);
    }
    
    public function getSupportRequestsByFilter($initialDate, $finalDate, $userName, $userEmployeeName, $id, $nrorg, $status) {
        $supportClass      = GenSupportRequest::class;
        $initialDateFormat = Util::stringToDate($initialDate);
        $finalDateFormat   = Util::stringToDate($finalDate);
        
        $userQuery = $userName !== NULL ? "AND (u.cpf LIKE '".$userName."')" : "";
        
        if (!is_null($userEmployeeName))
            $employeeIds = "'". join($userEmployeeName, "' OR e.id = '") . "'";
        $employeeQuery = $userEmployeeName !== NULL ? "AND (e.id = $employeeIds)" : ""; 
        
        $idQuery = $id !== NULL ? "AND (s.id LIKE '".$id."')" : "";
        
        $initialDateQuery = $initialDate !== NULL ? "AND s.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND s.createdAt <= '$finalDateFormat 23:59:59'" : "";
        
        if (!is_null($status))
            $statusReq = "'". join($status, "' OR s.status = '") . "'";
        $statusQuery = $status !== NULL ? "AND (s.status = $statusReq)" : "";
        
        $supportRequests = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $supportClass s
            JOIN s.genUser u
            LEFT JOIN s.genUserEmployee e
            WHERE s.nrorg = $nrorg
            $userQuery
            $employeeQuery
            $statusQuery
            $initialDateQuery
            $finalDateQuery
            $idQuery
            ORDER BY s.createdAt
            DESC
            "
        )->getResult();
        return $supportRequests;
    }
    
    public function finishSupportRequest($supportRequestId, $status) {
        $supportRequest     = $this->getEntityManager()->getRepository(GenSupportRequest::class)->find($supportRequestId);
        $supportRequest->setStatus($status);
        $this->getEntityManager()->flush();
    }
    
    public function getPlacesStructures($structureId, $nrorg) {
        $structures = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg]);
        
        return $structures;
    }
    
    // public function getPlacesStructures($parent, $nrorg, $type) {
    //     $structures = $this->getEntityManager()->getRepository(GenStructure::class)->findBy(['parent' => $parent, 'nrorg' => $nrorg, 'type' => $type]);
        
    //     foreach ($structures as $structure) {
    //         // $structure->setChildren($this->getPlacesStructures($structure->getId(), $nrorg, $type));
    //         $structures = array_merge($structures, $this->getPlacesStructures($structure->getId(), $nrorg, $type));
    //     }
        
    //     return GenStructure::manyToArrayOne($structures);
    // }
    
    public function getUserData($userId, $userTypeId, $nrorg) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->build($this->getEntityManager(), $userTypeId, $nrorg);
        
        return $user;
    }
    
    public function getOrganization($nrorg) {
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
}
    