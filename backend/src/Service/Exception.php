<?php
namespace Zeedhi\ApiGeneral\Service;

class Exception extends \Exception {

    const USER_NOT_FOUND = 404;
    const EMAIL_EXIST_VALIDATION_ERROR = 403;
    const EMAIL_DONT_EXIST__CPF_EXIST_VALIDATION_ERROR = 405;
    const EMAIL_EXIST__CPF_DONT_EXIST_VALIDATION_ERROR = 406;
    const INCORRECT_PASSWORD = 2; 
    const WRONG_OLD_PASSWORD = 3;
    const UNSUPPORTED_CREDIT_CARD_FLAG = 4;
    const INVALID_ORDER_STATUS = 5;
    const ORDER_NOT_FOUND = 6;
    const INSUFICIENT_CREDIT = 7;
    const INVALID_FACEBOOK = 8;
    const FACEBOOK_TOKEN_EXPIRED = 9;
    const USER_ID_NOT_FOUND = 10;
    const UNAUTHORIZED_ACTION = 11;
    const CONTACT_METHOD_NOT_FOUND = 12;
    const INCORRECT_PHONE_NUMBER = 13;
    const USER_NOT_BELONG_ORGANIZATION = 14;
    const SMS_TOKEN_VALIDATION_ERROR = 15;

    public static function loginNotFound() {
        return new static('User not found!', self::USER_NOT_FOUND);
    }

    public static function incorrectPassword() {
        return new static('Incorrect Password!', self::INCORRECT_PASSWORD);
    }
    
    public static function wrongOldPassword() {
        return new static('The current password is wrong!', self::WRONG_OLD_PASSWORD);
    }

    public static function unsupportedCreditCardFlag() {
        return new static('The credit card belongs to a non supported brand.', self::UNSUPPORTED_CREDIT_CARD_FLAG);
    }

    public static function invalidOrderStatus($orderStatus) {
        return new static("The selected order has a invalid status: {$orderStatus}", self::INVALID_ORDER_STATUS );
    }

    public static function orderNotFound() {
        return new static("Order not found!", self::ORDER_NOT_FOUND );
    }
    
    public static function balanceInsufficient() {
        return new static("Insuficient credit balance!", self::INSUFICIENT_CREDIT );
    }
    
    public static function invalidFacebook() {
        return new static('Your e-mail was not provided.', self::INVALID_FACEBOOK);
    }

    public static function facebookTokenExpired() {
        return new static('Facebook token expired.', self::FACEBOOK_TOKEN_EXPIRED);
    }
    
    public static function userNotFound() {
        return new static('User ID not found on Database!', self::USER_ID_NOT_FOUND);
    }
    
    public static function unauthorizedAction() {
        return new static('Unauthorized Action!', self::UNAUTHORIZED_ACTION);
    }
    
    public static function contactMethodNotFound() {
        return new static('Contact method not found!', self::CONTACT_METHOD_NOT_FOUND);
    }
    
    public static function incorrectPhoneNumber() {
        return new static('The provided phone number does not match the one found on Database!', self::INCORRECT_PHONE_NUMBER);
    }
    
    public static function userNotBelongOrganization() {
        return new static('User does not have the requested user profile.', self::USER_NOT_BELONG_ORGANIZATION);
    }
        
    public static function smsTokenValidationError() {
        return new static('There was a problem with Facebook\'s token validation', self::SMS_TOKEN_VALIDATION_ERROR);
    }
    
    public static function emailAlreadyInDataBase() {
        return new static('E-mail Already in Database', self::EMAIL_EXIST_VALIDATION_ERROR);
    }
    
    public static function cpfexistAndEmailIsdifferent() {
        return new static('Cpf Already in Database, but e-mail its not the same', self::EMAIL_DONT_EXIST__CPF_EXIST_VALIDATION_ERROR);
    }
    
    public static function emailexistAndCPFIsdifferent() {
        return new static('e-mail Already in Database, but cpf its not the same', self::EMAIL_EXIST__CPF_DONT_EXIST_VALIDATION_ERROR);
    }
    
    
}