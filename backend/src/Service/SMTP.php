<?php

namespace Zeedhi\ApiGeneral\Service;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Mime as Mime;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Doctrine\ORM\EntityManager;
use Util\Query;

class SMTP {

    protected $entityManager;
    protected $environment;

    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment   = $environment; 
    }
    
    public function sendConfirmationEmail($password, $to) {
        return $this->sendEmail(
                        "Email Recovery",
                        "Use $password as your temporary password to access MTC",
                        $to);
    }

    private function sendEmail($subject, $body, $to) {  
        // Get data from server
        try {
            $sender     = 'Teknisa';
            $username   = 'lucas.macedo@teknisa.com';
            $server     = '192.168.122.10';
            $password   = '1020Bippop2010';
            $port       = '25'; //old 25
            $type       = "text/html; charset=UTF-8";
            $encoding   = "UTF-8";
            $encryption = "null";
            
            // E-mail options.
            $options    = self::setOptionsEmail($server, $port, $username, $password, $encryption);
            
            $message = new Message();
            $message->addFrom('teknisa@teknisa.com', 'Teknisa');
            $message->addTo($to);
            $message->setSubject($subject);
            $message->setBody($body);
            
            $message->setEncoding('UTF-8');

            // Send e-mail.
            $transport  = new SmtpTransport();
            $transport->setOptions($options);
            $transport->send($message);
            
            return true;
        } catch (Exception $e) {
            var_dump($e);die;
        }
    }

    public function setConfigEmail(&$message, $encoding, $email_addressee, $username, $sender, $subject, $body){
        
        // -- for a while
        $name_addressee = $email_addressee; 
        // --
        $message->setEncoding($encoding);
        $message->addTo($email_addressee, $name_addressee)
            ->addFrom($username, $sender)
            ->setSubject($subject)
            ->setBody($body);
    }

    public function setOptionsEmail($server, $port, $username, $password, $encryption){ 

        return $options = new SmtpOptions(array(
            'host'              => $server,
            'port'              => $port,
            'connection_class'  => 'smtp',
            'connection_config' => array(
                'username'    => $username,
                'password'    => $password,
                'encryption'  => $encryption
            )
        ));
    }

}