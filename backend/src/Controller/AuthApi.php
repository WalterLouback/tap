<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\AuthApi as AuthApiService;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Simple;
use Zeedhi\ApiGeneral\Helpers\Util;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DTO;

class AuthApi extends Simple {

    /** @var AuthService */
    protected $authService;

    /**
     * __contruct
     *
     * @param AuthService $authService
     */
    public function __construct(AuthApiService $authService) {
        $this->authApiService = $authService;
    }

    public function authtoken (DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        $resp  = $this->authApiService->authtoken($row);
        $response->addDataSet(new DataSet("authtoken", $resp));
    }    
    
}