<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\ActivityRequest as activityRequestService;
use Zeedhi\ApiGeneral\Service\SubjectMatter as SubjectMattertService;

use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Helpers\ImageUploader;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DataSource\Manager;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;

use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;


class SubjectMatter extends Crud{
    
    protected $dataSourceName = 'subjectMatter';
    /**
     * __contruct
     *
     * @param NewsService $newsService
     */
    public function __construct(Manager $dataSourceManager, SubjectMattertService $subjectMatterService) {
        parent::__construct($dataSourceManager);
        $this->subjectMatterService = $subjectMatterService;
    }
   
   public function createSubjectMatter(DTO\Request\Row $request, DTO\Response $response) {
        $row              = $request->getRow();
        $userId           = intVal($row['USER_ID']);
        $name    = $row['SUBJECT_MATTER'];
        $responsableEmail = $row['RESPONSABLE_EMAIL'];
        $nrorg            = $row['NRORG'];
        
        $subjectMatter = $this->subjectMatterService->createSubjectMatter($userId, $name, $responsableEmail, $nrorg);
        
        $dataSet = new DataSet('subjectMatter', ['response' => 'ok', 'id' => $subjectMatter->getId()]);
    
        $response->addDataSet($dataSet);
   }
   
   public function removeSubjectMatter(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $id = $row['ID'];
       
        $this->subjectMatterService->removeSubjectMatter($id);
       
        $dataSet = new DataSet('removeSubjectMatter', ['response' => 'ok']);
    
        $response->addDataSet($dataSet);

   }
   
   public function editSubjectMatter(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        $id               = $row['ID'];
        $subjectMatter    = $row['SUBJECT_MATTER'];
        $responsableEmail = $row['RESPONSABLE_EMAIL'];
        $userId           = $row['USER_ID'];
       
        $this->subjectMatterService->editSubjectMatter($id, $subjectMatter, $responsableEmail, $userId);
       
        $dataSet = new DataSet('editSubjectMatter', ['response' => 'ok']);
    
        $response->addDataSet($dataSet);       
   }
    
}