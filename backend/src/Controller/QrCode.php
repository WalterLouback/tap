<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;

class QrCode extends Crud {
    
    protected $dataSourceName = 'qr_code';

}