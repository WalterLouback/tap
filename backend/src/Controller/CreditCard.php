<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiGeneral\Service\CreditCard as CreditCardService;

class CreditCard extends Crud {

    protected $dataSourceName = 'creditcard';
    
    /** @var \Zeedhi\ApiGeneral\Service\CreditCard */
    private $creditCardService;
    
    public function __construct(Manager $manager, CreditCardService $creditCardService) {
        parent::__construct($manager);
        $this->creditCardService = $creditCardService;
    }
    
    public function addCreditCard(DTO\Request\Row $request, DTO\Response $response) {
        $creditCardRow = $request->getRow();
        list($month, $year) = explode("/", $creditCardRow['EXPIRATION_DATE']);
        $creditCard = $this->creditCardService->addNewCreditCard(
            $creditCardRow['CARDFULLNAME'],
            $creditCardRow['NUMBERS'],
            $month,
            $year,
            $creditCardRow['CVV']
        );
        
        $response->addDataSet(new DataSet("creditcard", array(array(
            "ID" => $creditCard->getId(),
            "FLAG" => $creditCard->getFlag(),
            "EXPIRATION_DATE" => $creditCard->getExpirationDate(),
            "LAST_NUMBERS" => $creditCard->getLastNumbers()
        ))));
    }

}