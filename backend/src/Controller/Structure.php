<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Helpers\General as General;
use Zeedhi\ApiGeneral\Service\Structure as StructureService;
use Zeedhi\ApiGeneral\Service\Exception as Exception;
use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenStructureType;
use Zeedhi\ApiGeneral\Helpers\Util;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiGeneral\Helpers\ImageUploader;

class Structure {
    
    /** @var StructureService */
    protected $structureService;
    
    /** @var general */
    protected $general;
    
    /**
     * __contruct
     *
     * @param StructureService $StructureService
     */
    public function __construct(StructureService $structureService, ImageUploader $imageUploader) {
        $this->structureService = $structureService;
        $this->imageUploader = $imageUploader;
    }
    
    /**
     * getStructures
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStructures(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Obter parâmetros da requisição */
        $nrorg              = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        $event              = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $level              = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : NULL;
        $parentId           = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $initial            = isset($row['FIRST_RESULT']) ? $row['FIRST_RESULT'] : NULL;
        $max                = isset($row['MAX_RESULTS'])  ? $row['MAX_RESULTS'] : NULL;
        $type               = isset($row['TYPE'])  ? $row['TYPE'] : NULL;

        /* Obter estruturas */
        $structures         = $this->structureService->getStructures($nrorg, $parentId, $event, $level, $initial, $max, $type);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structures', GenStructure::manyToArray($structures)));
    }
    
    /**
     * getStructureById
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStructureById(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $id    = $row['STRUCTURE_ID'];
        $nrorg = $row['NRORG'];
        
        /* Obter estrutura */
        $structure = $this->structureService->getStructureById($id, $nrorg);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structure', $structure->toArray()));
    }
    
    /**
     * createStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function createStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obtêm os parametros da requisição*/
        $userId         = $row['USER_ID'];
        $name           = $row['NAME'];
        $nrorg          = $row['NRORG'];
        $description    = isset($row['DESCRIPTION']) ? $row['DESCRIPTION'] : NULL;
        $type           = $row['TYPE'];
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $structureLevel = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : 0;
        $evtEventId     = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $maxpersons = isset($row['MAXPERSONS']) ? $row['MAXPERSONS'] : null;
        $checkaprove = isset($row['CHECKAPROVE']) ? $row['CHECKAPROVE'] : null;
        $maxarea = isset($row['MAXAREA']) ? $row['MAXAREA'] : null;
        $rules = isset($row['RULES']) ? $row['RULES'] : null;
        $structureTypeId = isset($row['structureTypeId']) ? $row['structureTypeId'] : null;
  
        $ImageUploader = $this->imageUploader;
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            if (strlen($row['IMAGE']) > 500)
                $image = $ImageUploader->upload($row['IMAGE'], $nrorg);
            else    
                $image = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;;
        }

        /*Endereço da Strutura*/
        $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
        $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
        $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
        $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
        $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
        $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
        $latitude       = isset($row['LATITUDE']) ? $row['LATITUDE'] : NULL;
        $longitude      = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : NULL;
        
        /*Telefone da estrutura*/
        $phone          = isset($row['PHONE']) ? $row['PHONE'] : NULL;
        $email          = isset($row['EMAIL']) ? $row['EMAIL'] : NULL;
        
        $genAddress = NULL;
        
        /*Cria o endereço*/
        if($street || $number || $neighborhood || $city || $provincy || $cep) {
            $genAddress = $this->structureService->createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg, $latitude, $longitude);
        }
        /*Cria unidade*/
        $structure = $this->structureService->createStructure($name, $nrorg, $description, $type, $userId, $parentId, $genAddress, $structureLevel, $evtEventId, $email, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    /**
     * updateStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function updateStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obtêm os parametros da requisição*/
        $name           = $row['NAME'];
        $type           = $row['TYPE'];
        $nrorg          = $row['NRORG'];
        $status         = $row['STATUS'];
        $userId         = $row['USER_ID'];
        $description    = $row['DESCRIPTION'];
        $structureId    = $row['STRUCTURE_ID'];
        $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
        $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
        $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
        $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
        $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $genAddressId   = isset($row['ADDRESS_ID']) ? $row['ADDRESS_ID'] : NULL;
        $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
        $evtEventId     = isset($row['$EVT_EVENT_ID']) ? $row['$EVT_EVENT_ID'] : NULL;
        $structureLevel = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : NULL;
        $email          = isset($row['EMAIL']) ? $row['EMAIL'] : NULL;
        $latitude       = isset($row['LATITUDE']) ? $row['LATITUDE'] : NULL;
        $longitude      = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : NULL;
        
        $maxpersons = isset($row['MAXPERSONS']) ? $row['MAXPERSONS'] : null;
        $checkaprove = isset($row['CHECKAPROVE']) ? $row['CHECKAPROVE'] : null;
        $maxarea = isset($row['MAXAREA']) ? $row['MAXAREA'] : null;
        $rules = isset($row['RULES']) ? $row['RULES'] : null;
        $structureTypeId = isset($row['structureTypeId']) ? $row['structureTypeId'] : null;

        $ImageUploader = $this->imageUploader;
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            if (strlen($row['IMAGE']) > 500)
                $image = $ImageUploader->upload($row['IMAGE'], $nrorg);
            else    
                $image = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;;
        }
        
        // $phone          = $row['PHONE'];
        $genAddress = $genAddressId;
        /*Atualiza o endereço*/
        if(($street || $number || $neighborhood || $city || $provincy || $cep || $latitude || $longitude) && ($genAddressId)){
            $genAddress = $this->structureService->updateGenAddress($genAddressId, $nrorg, $street, $number, $neighborhood, $city, $provincy, $cep, $userId, $latitude, $longitude);
        } else if((!$genAddressId) && ($street || $number || $neighborhood || $city || $provincy || $cep || $latitude || $longitude)) {
            $genAddress = $this->structureService->createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg, $latitude, $longitude);
        }
        
        /*Obtem a unidade*/
        $structure = $this->structureService->updateStructure($structureId, $nrorg, $name, $description, $parentId, $structureLevel, $evtEventId, $type, $status, $userId, $genAddress, $email, $maxpersons, $checkaprove, $maxarea, $rules, $image, $structureTypeId);
        
        /*Atualiza a unidade*/
        

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function inactivateStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $this->structureService->inactivateStructure($row['STRUCTURE_ID'], $row['NRORG']);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    function getPlacesStructures(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $structureId    = $row['STRUCTURE_ID'];
        $nrorg          = $row['NRORG'];
        
        $structures = $this->structureService->getPlacesStructures($nrorg, "U");
        
        $response->addDataSet(new DataSet('structures', GenStructure::manyToArray($structures)));
    }
    
    // function getPlacesStructures(DTO\Request\Row $request, DTO\Response $response) {
    //     $row   = $request->getRow();
        
    //     /* Obter parâmetros da requisição */
    //     $structureId    = $row['STRUCTURE_ID'];
    //     $nrorg          = $row['NRORG'];
        
    //     $structures = $this->structureService->getPlacesStructures($structureId, $nrorg, "L");
        
    //     // $response->addDataSet(new DataSet('structure', GenStructure::manyToArray($structures)));
    //     $response->addDataSet(new DataSet('structure', GenStructure::manyToArrayOne($structures)));
    // }


    //functions related to the type of space
    public function createStructureType (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obtêm os parametros da requisição*/
        $userId         = $row['USER_ID'];
        $name           = $row['NAME'];
        $nrorg          = $row['NRORG'];        
        $structure = $this->structureService->createStructureType($name, $nrorg, $userId);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }

    public function updateStructureType(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $name           = $row['NAME'];
        $nrorg          = $row['NRORG'];
        $userId         = $row['USER_ID'];
        $structureTypeId    = $row['STRUCTURE_TYPE_ID'];
        $structure = $this->structureService->updateStructureType($name, $nrorg, $userId, $structureTypeId);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
 
    public function inactivateStructureType(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $this->structureService->inactivateStructureType($row['STRUCTURE_TYPE_ID'], $row['NRORG']);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    } 

     function getStructuresType(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Obter parâmetros da requisição */
        $nrorg              = $row['NRORG'];
        /* Obter estruturas */
        $structures         = $this->structureService->getStructuresType($nrorg);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structures', GenStructureType::manyToArray($structures)));
    }    
    
}