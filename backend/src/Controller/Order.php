<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\Order as OrderService;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;

class Order extends Crud {
    
    protected $dataSourceName = 'order';
    
    /** @var OrderService */
    private $orderService;
    
    public function __construct(Manager $dataSourceManager, OrderService $orderService) {
        parent::__construct($dataSourceManager);
        $this->orderService = $orderService;
    }

    public function processOrder(DTO\Request\Row $request, DTO\Response $response) {
        $orderRow = $request->getRow();
        $this->orderService->processOrder($orderRow['PRODUCTS_ORDER'], $orderRow['EVENT_ID'], $orderRow['CARD_ID']);
    }

    public function finishOrder(DTO\Request\Row $request, DTO\Response $response) {
        $orderRow = $request->getRow();
        $this->orderService->finishOrder($orderRow['QRCODE']);
    }
    
    public function processOrderWithBalance(DTO\Request\Row $request, DTO\Response $response) {
        $orderRow = $request->getRow();
        $this->orderService->processOrder($orderRow['PRODUCTS_ORDER'], $orderRow['EVENT_ID']);
    }
    
}