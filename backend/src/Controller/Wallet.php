<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\Wallet as WalletService;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;

class Wallet extends Crud {
    
    protected $dataSourceName = 'wallet';
    
    /** @var CreditService */
    private $walletService;
    
    public function __construct(Manager $dataSourceManager, WalletService $walletService) {
        parent::__construct($dataSourceManager);
        $this->walletService = $walletService;
    }
    
    public function processCreditPurchase(DTO\Request\Row $request, DTO\Response $response) {
        $walletRow = $request->getRow();
        $this->walletService->processOrder($walletRow['CARD_ID'], $walletRow['BALANCE']);
    }
}