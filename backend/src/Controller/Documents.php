<?php
namespace Zeedhi\ApiGeneral\Controller;
use Zeedhi\ApiGeneral\Service\Documents as documentsService;
use Zeedhi\ApiGeneral\Service\EmailPhpMailer as emailPhpMailerrService;
use Zeedhi\ApiGeneral\Model\Entities\GenDocument;
use Zeedhi\ApiGeneral\Model\Entities\GenDocUserRel;
use Zeedhi\ApiGeneral\Helpers\FileUploader;
use Zeedhi\ApiGeneral\Helpers\Util;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\Manager;
class Documents extends Crud {
    protected $dataSourceName = 'documents';
    /**
     * __contruct
     *
     * @param DocumentsService $documentsService
     * @param ImageUploader $imageUploader
     */
     public function __construct(Manager $dataSourceManager, DocumentsService $documentsService, FileUploader $fileUploader) {
          parent::__construct($dataSourceManager);
          $this->documentsService = $documentsService;
          $this->fileUploader = $fileUploader;
     }

     public function uploadDocument(DTO\Request $request, DTO\Response $response) {
          $data     = $request->getParameters();
          $row      = json_decode($data['row'], true);

          $nrorg    = $row['NRORG'];
          $userId   = $row['USER_ID'];
          $type     = $row['TYPE'];
          
          $file     = $_FILES;
          
          $fileURL  = $this->fileUploader->upload($file, $nrorg);
          
          $this->documentsService->uploadDocument($nrorg, $userId, $type, $fileURL);

          $response->addDataSet(new DataSet('response', ['status' => 'ok']));
     }

     public function getDocType(DTO\Request $request, DTO\Response $response) {
          $row = Util::getParamsAsArray($request);
          
          $nrorg      = $row['NRORG'];
          
          $documents = $this->documentsService->getDocType($nrorg);
          
          $response->addDataSet(new DataSet('response', GenDocument::manyToArray($documents)));
     }

     public function getDocuments(DTO\Request\Row $request, DTO\Response $response) { 
          $row = $request->getRow();
          
          $nrorg      = $row['NRORG'];
          $userId     = $row['USER_ID'];
          
          $documents = $this->documentsService->getDocuments($nrorg, $userId);
          
          $response->addDataSet(new DataSet('response', $documents));
     }
     
     public function createDocument(DTO\Request $request, DTO\Response $response) {
          $row = $request->getRow();

          $nrorg    = $row['NRORG'];
          $userId   = $row['USER_ID'];
          $type     = $row['TYPE'];
          $expDate  = $row['EXP_DATE'];
          $name     = $row['NAME'];
          $status   = $row['STATUS'];
          
          
          $documentId =$this->documentsService->createDocument($expDate, $name, $status, $type, $nrorg, $userId);

          $response->addDataSet(new DataSet('response', ['status' => 'ok', 'id' => $documentId]));
     }
     
     public function editDocument(DTO\Request $request, DTO\Response $response) {
          $row = $request->getRow();
          $id       = $row['ID'];
          $nrorg    = $row['NRORG'];
          $userId   = $row['USER_ID'];
          $type     = $row['TYPE'];
          $expDate  = $row['EXP_DATE'];
          $name     = $row['NAME'];
          $status   = $row['STATUS'];
          
          
          $this->documentsService->editDocument($id, $expDate, $name, $status, $type, $nrorg, $userId);

          $response->addDataSet(new DataSet('response', ['status' => 'ok']));
     }
     
     public function removeDocument(DTO\Request\Row $request, DTO\Response $response) {
          $row = $request->getRow();

          $id = $row['ID'];

          $this->documentsService->removeDocument($id);

          $response->addDataSet(new DataSet('response', ['status' => 'ok']));
     }
}
