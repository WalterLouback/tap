<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;

class TicketOrder extends Crud {

    protected $dataSourceName = 'ticket_order';

}