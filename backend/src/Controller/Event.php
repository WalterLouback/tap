<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiGeneral\Service\Environment;

class Event extends Crud {

    protected $dataSourceName = 'event';
    
    /** @var $environment Zeedhi\ApiGeneral\Service\Environment */
    private $environment;
    /** @var $urlPrefix String*/
    private $urlPrefix;

    /** @const COVERIMG_PHOTO_UPLOAD Name of field that uploads the cover photo in widget */
    const COVERIMG_PHOTO_UPLOAD = 'COVERIMG_PHOTO_UPLOAD';
    /** @const LOGOIMG_PHOTO_UPLOAD Name of field that uploads the logo photo in widget */
    const LOGOIMG_PHOTO_UPLOAD = 'LOGOIMG_PHOTO_UPLOAD';
    /** @const MAPIMG_PHOTO_UPLOAD Name of field that uploads the map photo in widget */
    const MAPIMG_PHOTO_UPLOAD = 'MAPIMG_PHOTO_UPLOAD';

    /** @const IMAGE_COVER Name of column's database that represents cover image */
    const IMAGE_COVER = 'IMAGE_COVER';
    /** @const IMAGE_LOGO Name of column's database that represents logo image */
    const IMAGE_LOGO = 'IMAGE_LOGO';
    /** @const IMAGE_MAP Name of column's database that represents map image */
    const IMAGE_MAP = 'IMAGE_MAP';
    
    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param String $urlPrefix
     */
    public function __construct(Manager $dataSourceManager, Environment $environment, $urlPrefix) {
        parent::__construct($dataSourceManager);
        $this->environment = $environment;
        $this->urlPrefix = $urlPrefix;
    }
    
    /**
     * save
     * Insert image paths before Crud save and iterate over dataset
     * 
     * @param DTO\Request\DataSet $request
     * @param DTO\Response $response
     * 
     * @return DTO\Response $response
     */
    public function save(DTO\Request\DataSet $request, DTO\Response $response) {
        $rows = $request->getDataSet()->getRows();
        $this->putImagePathFromTo($rows, self::COVERIMG_PHOTO_UPLOAD, self::IMAGE_COVER);
        $this->putImagePathFromTo($rows, self::LOGOIMG_PHOTO_UPLOAD, self::IMAGE_LOGO);
        $this->putImagePathFromTo($rows, self::MAPIMG_PHOTO_UPLOAD, self::IMAGE_MAP);

        foreach ($rows as $row) {
            $row['USER_ID'] = $this->environment->getUserId();
        }
        parent::save($request, $response);
    }

    /**
     * putImagePathFromTo
     * Put image path to field that match the database column
     * 
     * @param Zeedhi\Framework\DTO\Row $row
     * @param String $from
     * @param String $to
     * 
     * @return Void
     */
    private function putImagePathFromTo($rows, $from, $to){
        
        foreach ($rows as $row) {
            if(!empty($row[$from])){
                $row[$to] = $this->urlPrefix.$row[$from][0]['path'];
            }
        }

    }

}