<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\News as NewsService;
use Zeedhi\ApiGeneral\Model\Entities\GenNews;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Helpers\ImageUploader;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DataSource\Manager;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;

use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration;


class News extends Crud{
    
    protected $dataSourceName = 'news';
    /**
     * __contruct
     *
     * @param NewsService $newsService
     */
    public function __construct(Manager $dataSourceManager, NewsService $newsService, ImageUploader $imageUploader) {
        parent::__construct($dataSourceManager);
        $this->newsService = $newsService;
        $this->imageUploader = $imageUploader;
    }
    
    public function getLastNews(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg  = $row[GenOrganization::NRORG];
        $amount = isset($row['MAX_RESULTS']) ? $row['MAX_RESULTS'] : null;
        $showNotActiveNews = isset($row['SHOW_NOT_ACTIVE_NEWS']) ? $row['SHOW_NOT_ACTIVE_NEWS'] : false;
        
        $news   = $this->newsService->getLastNews($nrorg, $amount, $showNotActiveNews);
        
        $response->addDataSet(new DataSet('news', GenNews::manyToArray($news)));
    }
    
    /**
     * CRISTIANO
     *
     * add a news 
     */
    public function addNews(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow(); 
        
        
        $title  = $row['TITLE']; 
        $news   = $row['NEWS']; 
        $nrorg  = $row['NRORG']; 
        $active = $row['ACTIVE']; 
        $userId = $row['USER_ID']; 
        $image  = $row['IMAGE'];
        
        $news   = $this->newsService->addNews($title, $news, $nrorg, $active, $userId, $image);
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * CRISTIANO
     *
     * update a news 
     */
    public function updateNews(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow(); 
        
        $title  = $row['TITLE']; 
        $newId  = $row['NEW_ID']; 
        $news   = $row['NEWS']; 
        $nrorg  = $row['NRORG']; 
        $active = $row['ACTIVE']; 
        $userId = $row['USER_ID']; 
        $image  = $row['IMAGE'];
        
        $news   = $this->newsService->updateNews($newId, $title, $news, $nrorg, $active, $userId, $image);
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * CRISTIANO
     *
     * delete a news 
     */
    public function inactivateNews(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow(); 
        
        $news   = $this->newsService->inactivateNews($row['NEW_ID'], $row['NRORG'], $row['USER_ID']);
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function removeNews(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow(); 
        
        $news   = $this->newsService->removeNews($row['NEW_ID'], $row['NRORG'], $row['USER_ID']);
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function uploadNewsImage(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $image    = $row['IMAGE'];
        $newsId   = $row['NEWS_ID'];
        $nrorg    = $row['NRORG'];
        
        /* 
        Imagem era armazenada com nome baseado no ID da notícia
        Verificar se ainda há alguma necessidade disto na implementação do admin.
        */
        // $imageUrl = $this->imageUploader->upload($image, 'NEWS_' . $newsId);
        
        $imageUrl = $this->imageUploader->upload($image, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['imageUrl' => $imageUrl]));
    }
    
    public function removeTempImage(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $image    = $row['IMAGE_NAME'];
        
        $imageUrl = $this->imageUploader->removeImage('NEWS_' . $newsId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function getNewsWebService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorgs     = array(1, 2);
        // $consumerWSMTC = new ConsumeWebServiceMTC();
        $integration  = new Integration(new ConsumeWebServiceMTC());
        
        foreach($nrorgs as $nrorg) {
            $result = $integration->getNewsWebService($nrorg);
            $getNews = $this->newsService->getAllNews($nrorg); 

            if(!empty($result) && empty($result['res']['des_erro'])){
                if(!empty($getNews)){
                    foreach($getNews as $value) {
                        $newsId = $value->getId();
                        $userId = 1;
                        
                       $this->newsService->removeNews($newsId, $nrorg, $userId);
                    }
                }

                foreach (array_reverse($result) as $value) {
                    $externalId  = $value['id'];
                    $title       = $value['titulo'];
                    $data        = $value['data'];
                    $externalId  = $value['hora'];
                    $image       = $value['imagem'];
                    $news        = $value['noticia'];
                    $active      = 1;
                    
                   $news   = $this->newsService->addNewsWebService($title, $news, $nrorg, $active, $image);
                }
            }
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }

    
}