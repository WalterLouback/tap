<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenTag;
use Zeedhi\ApiGeneral\Model\Entities\PayCreditcard;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;/*Cristiano*/
use Zeedhi\ApiGeneral\Service\Auth as AuthService;
use Zeedhi\ApiGeneral\Service\User as UserService;
use Zeedhi\ApiGeneral\Controller\Exception;
use Zeedhi\ApiGeneral\Helpers\RijndaelCrypt;
use Zeedhi\ApiGeneral\Helpers\Util;

use Zeedhi\Framework\Controller\Simple;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;

use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration;
use Zeedhi\ApiGeneral\Listeners\RegisterLogs;


class Auth extends Simple {

    /** @var AuthService */
    protected $authService;
    /** @var ParameterBag */
    protected $parameterBag;
    /** @var $urlPrefix String*/
    private   $urlPrefix;
    /** @var $rijndaelCrypt String*/
    private   $rijndaelCrypt;
     /** @var UserService */
    protected $userService;
    
    const SALT = 'BIPPOPSALT';

    /**
     * __contruct
     *
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService, ParameterBag $parameterBag, $urlPrefix, RijndaelCrypt $rijndaelCrypt, RegisterLogs $registerLogs, UserService $userService) {
        $this->authService = $authService;
        $this->userService = $userService;
        $this->parameterBag = $parameterBag;
        $this->urlPrefix = $urlPrefix;
        $this->rijndaelCrypt = $rijndaelCrypt;
        $this->registerLogs  = $registerLogs;
    }

    /**
     * login
     * Login method
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function login(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        /* Verifica se os campos foram preenchidos */
        $status = Response::STATUS_SUCCESS;
        
        $this->checkFieldLogin($row);
        $genUserTypeId  = GenUserType::USUARIO_ID;
        
        $level = "login";
        $msg   = "requestLogin: " . json_encode($row); 
        $this->registerLogs->logMsg("Request Login: " . $msg, $level);
        
        $email      = isset($row["EMAIL"])    ? $row["EMAIL"] : NULL;
        $phone      = isset($row["PHONE"])    ? $row["PHONE"] : NULL;
        $cpf        = isset($row["CPF"])      ? $row["CPF"]   : NULL;
        $npf        = isset($row["NPF"])      ? $row["NPF"]   : NULL;
        $password   = isset($row["PASSWORD"]) ? $row["PASSWORD"] : NULL;
        $nrorg      = $row["NRORG"];
        
        $statusUserWB   = "";
        
        $webService     = $this->useIntegration($nrorg);
        
        if ($webService) {
            $organization = $this->getOrganization($nrorg);
            
            $integration  = new Integration(new ConsumeWebServiceMTC());
            
            $statusUserWB = $integration->webServiceLoginOne($organization, $email, $cpf, $npf, $password);
            if ($statusUserWB['res']['status'] != "ok") throw new Exception($statusUserWB['res']['des_erro'], 2);
            $genUsers = $this->authService->getActiveUserByExternalId($nrorg, $statusUserWB['res']['npf']);
            if ($genUsers[0]->getPassword() != $this->authService->cryptPassword($password))
                $this->authService->createPassword($genUsers[0]->getId(), $password);
            
            if(!empty($genUsers) && $this->verifyUserBelongsToOrganization($genUsers[0]->getId(), $nrorg) == NULL && $statusUserWB['res']['status'] == "ok") {
                $this->authService->addProfileUser($genUsers[0], 1, $nrorg, "A", $npf);
                $this->authService->commit();
                
                $genUsers = $this->authService->getActiveUserByExternalId($nrorg, $statusUserWB['res']['npf']);
                $user = isset($genUsers[0]) ? $genUsers[0] : null;
                $this->formatDataAndCreateOrUpdateGenUser($organization, $statusUserWB['res']['token'], $integration, $password, $nrorg, $user); 
            }
            
            if(empty($genUsers) || $genUsers[0]->getEmail() == null) {
                $integration  = new Integration(new ConsumeWebServiceMTC());
                $filial = $this->getOrganization($nrorg);
                $user = isset($genUsers[0]) ? $genUsers[0] : null;
                
                $this->formatDataAndCreateOrUpdateGenUser($filial, $statusUserWB['res']['token'], $integration, $password, $nrorg, $user); 
            }
            $user = $this->authService->login($nrorg, $statusUserWB['res']['npf'], null, $password);
            $this->authService->setLastGenUserToken($user->getId(), $statusUserWB['res']['token']);
        } else if ($npf || $cpf || $phone) {
            $user = $this->authService->login($nrorg, $npf, $cpf, $password, $phone);
        } else {
            $user = $this->authService->loginBackOffice($email, $password);
        }
        
        $genUserTypeRel = $this->authService->getGenUserTypeRel($user->getId(), $genUserTypeId, $nrorg);
        
        if(empty($genUserTypeRel)) throw new \Exception('User without profile!', 98);
        
        $dataSet     = new DataSet('user', [
            'userId'        => $user->getId(),
            'refresh_token' => $user->getToken(),
            'token_ext'     => (isset($statusUserWB['res']['status']) && ($statusUserWB['res']['status'] == 'ok')) ? $statusUserWB['res']['token'] : "",
            'key_ext'       => (isset($statusUserWB['res']['status']) && ($statusUserWB['res']['status'] == 'ok')) ? $statusUserWB['res']['chave'] : "",
            'id_ext'        => (isset($statusUserWB['res']['status']) && ($statusUserWB['res']['status'] == 'ok')) ? $statusUserWB['res']['npf'] : "",
            'val_pic'       => (isset($statusUserWB['res']['status']) && ($statusUserWB['res']['status'] == 'ok')) ? $statusUserWB['res']['dat_validade_foto'] : "",
            'val_id_ext'    => (isset($statusUserWB['res']['status']) && ($statusUserWB['res']['status'] == 'ok')) ? $statusUserWB['res']['dat_validade_cart'] : ""
        ]);
        
        $this->registerLogs->logMsg("Response Login: " . serialize($dataSet), $level);
        $response->addDataSet($dataSet);
    }
    
    public function authenticateUser(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $email      = isset($row["EMAIL"])    ? $row["EMAIL"] : NULL;
        $phone      = isset($row["PHONE"])    ? $row["PHONE"] : NULL;
        $cpf        = isset($row["CPF"])      ? $row["CPF"]   : NULL;
        $password   = $row["PASSWORD"];
        
        if (!$email && !$phone && !$cpf) throw new \Exception("User identification is required. Options: ['EMAIL', 'PHONE', 'CPF']", 1);
        
        $user       = $this->authService->authenticateUser($email, $phone, $cpf, $password);
        
        if ($user === NULL) throw new \Exception("User not found", 2);
        
        $response->addDataSet(new DataSet('user', [
            'userId'        => $user->getId(),
            'refresh_token' => $user->getToken()
        ]));
    }
    /** Este metodo deve verificar se usuario tem senha cadastrada no 
     *  perfil dele numa organizacao especifica. Senão tiver usar a senha
     *  do genUser, que e a senha padrao dele.
     */
    public function authenticateUserV1(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $email      = isset($row["EMAIL"])    ? $row["EMAIL"] : NULL;
        $phone      = isset($row["PHONE"])    ? $row["PHONE"] : NULL;
        $cpf        = isset($row["CPF"])      ? $row["CPF"]   : NULL;
        $nrorg      = isset($row["NRORG"])    ? $row["NRORG"] : NULL;
        $password   = $row["PASSWORD"];
        
        if (!$email && !$phone && !$cpf && !$password) throw new \Exception("User identification is required. Options: ['EMAIL', 'PHONE', 'CPF', 'NRORG', 'PASSWORD']", 1);
        
        $user       = $this->authService->authenticateUserV1($email, $phone, $cpf, $password, $nrorg);
        
        if ($user === NULL) throw new \Exception("User not found", 2);
        
        $response->addDataSet(new DataSet('user', [
            'userId'        => $user->getId(),
            'refresh_token' => $user->getToken()
        ]));
    }
    
    public function authenticateUserWithExternalMethod(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $uid        = isset($row["UID"]) ? $row["UID"] : NULL;
        $firstName  = isset($row["FIRST_NAME"]) ? $row["FIRST_NAME"] : NULL;
        $lastName   = isset($row["LAST_NAME"]) ? $row["LAST_NAME"] : NULL;
        $email      = isset($row["EMAIL"]) ? $row["EMAIL"] : NULL;
        $phone      = isset($row["PHONE"]) ? $row["PHONE"] : NULL;
        $nrorg      = isset($row["NRORG"]) ? $row["NRORG"] : NULL;
        $image      = isset($row["IMAGE"]) ? $row["IMAGE"] : NULL;
        $userTypeId = "1";
       
        
        if (!$uid) throw new \Exception("User identification is required.", 1);
        
        $user = $this->authService->authenticateUserWithExternalMethod($uid, $nrorg);
        
        if ($user === NULL) {
            if (!$phone)  throw new \Exception("Contact method not found!", 12);
            else {
                $this->userService->createUserWithExternalMethod($uid, $firstName, $lastName, $email, $phone, $nrorg, $userTypeId, null, $image);
                $user = $this->authService->authenticateUserWithExternalMethod($uid, $nrorg);
            }
        }
        
        $response->addDataSet(new DataSet('user', [
            'userId'        => $user->getId(),
            'refresh_token' => $user->getToken()
        ]));
    }
    
    public function verifyUserBelongsToOrganization($userId, $nrorg) {
        $genUserTypeId  = 1;
        $haveProfile    = !empty($this->authService->getGenUserTypeRel($userId, $genUserTypeId, $nrorg)) ? true : false;
        
        return $haveProfile;
    }
    
    public function checkFieldLogin($row) {
        if (!empty($row[GenUser::PASSWORD])) {
        } elseif (isset($row["CPF"]) && !empty($row["CPF"])  && !empty($row[GenUser::PASSWORD])) {
        } elseif (isset($row["NPF"]) && !empty($row["NPF"])  && !empty($row[GenUser::PASSWORD])) {
        } else { throw Exception::invalidParameters(); }
    }
    /**
     * loginBackoffice
     * LoginBackoffice method
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function loginBackoffice(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $status = Response::STATUS_SUCCESS;
        if (empty($row[GenUser::EMAIL]) && !empty($row[GenUser::PASSWORD])) {
            throw Exception::invalidParameters();
        }
        $email         = $row[GenUser::EMAIL];
        $password      = $row[GenUser::PASSWORD];
        $nrorg         = $row[GenOrganization::NRORG];
        $genUserTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : GenUserType::VENDEDOR_ID;
        $genUserTotem = isset($row['TOTEM_USER']) ? $row['TOTEM_USER'] : null;
        
        $user = $this->authService->loginBackOffice($email, $password);
        
        $userStatus = $this->authService->getGenUser(['email' => $email, 'status' => 'A']);
        $genUserTypeRel = $this->authService->getGenUserTypeRel($user->getId(), $genUserTypeId, $nrorg);
        $evtEventSeller = $this->authService->getEvtEventSeller($user->getId());
        
        if($userStatus->getStatus() === 'I') throw new \Exception('Deleted user!', 99);
        if(!$genUserTypeRel || ($evtEventSeller == NULL && !$genUserTotem)) throw new \Exception('User without profile!', 98);
        //if(!$genUserTypeRel) throw new \Exception('User without profile!', 98);
        if($genUserTypeRel->getStatus() ==! 'A') throw new \Exception('Inactive user!', 99);
        
        
        if(!$genUserTotem){
            $dataSet     = new DataSet('user', [
                'userId'        => $user->getId(),
                'refresh_token' => $user->getToken(),
            ]);
        }
        else{
            $dataSet     = new DataSet('user', [
                'userId'        => $user->getId(),
                'refresh_token' => $user->getToken(),
                'pos_serial_number' => $user->getPosSerialNumber()
                
            ]);
        }
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * @param DTO\Request  $request
     * @param DTO\Response $response
     * 
     * deprecated
     */
    public function logoutBackoffice(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $externalToken = $row['TOKEN_EXT'];
        $this->authService->logout();
        $this->parameterBag->set(GenUser::USER_ID, null);
        $this->externalLogOut($externalToken);
    }
    
    /**
     *
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function logout(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
  
        $externalToken = isset($row['TOKEN_EXT']) ? $row['TOKEN_EXT'] : NULL;
        $nrorg = $row['NRORG'];
        
        $YesNo  = $this->useIntegration($nrorg);
        
        if(isset($YesNo) && $YesNo) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        
            $branch   = $this->getOrganization($nrorg);
            $returnWs = $consumeWebServiceMTC->logoutWebService($externalToken, $branch);
            $dataSet  = new DataSet('response', [$returnWs['res']]);
        }else {
            $dataSet  = new DataSet('response', ["ok"]);
        }
        
        $this->authService->logout();
        $this->parameterBag->set(GenUser::USER_ID, null);
        
        $response->addDataSet($dataSet);
    }
    
    /**
     * requestToken
     * 
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function requestToken(DTO\Request $request, DTO\Response $response) {
        $row         = $request->getRow();
        $status      = Response::STATUS_SUCCESS;
        $userId      = $row[GenUser::USER_ID];
        $token       = $row['REFRESH_TOKEN'];
        
        $token   = $this->authService->requestToken($userId, $token);
        $dataSet = new DataSet('user', [
            'token' => $token
        ]);
    
        $response->addDataSet($dataSet);
    }
    
    /**
     * getUserOrganizations
     * 
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function getUserOrganizations(DTO\Request $request, DTO\Response $response) {
        $row         = $request->getRow();
        $status      = Response::STATUS_SUCCESS;
        $userId      = $row[GenUser::USER_ID];
        $userTypeId  = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : NULL;
        $latitude    = isset($row['LATITUDE']) ? $row['LATITUDE'] : NULL;
        $longitude   = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : NULL;
        
        $user    = $this->authService->getUserOrganizations($userId, $userTypeId, $latitude, $longitude);
        $dataSet = new DataSet('user', [
            'organizations' => $user->getAssociatedOrganizations()
        ]);
    
        $response->addDataSet($dataSet);
    }
    
    /**
     * loginFacebook
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function loginFacebook(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $status  = Response::STATUS_SUCCESS;
        if (empty($row[GenUser::FB_TOKEN])) {
            throw Exception::invalidParameters();
        }
        $facebookToken = $row[GenUser::FB_TOKEN];
        
        $user    = $this->authService->loginFacebook($facebookToken);
        $dataSet = new DataSet('user', $user->toArray());
    
        $response->addDataSet($dataSet);
    }
    
    /**
     * linkFacebook
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     public function linkFacebook(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $status  = Response::STATUS_SUCCESS;
        if (empty($row[GenUser::FB_TOKEN])) {
            throw Exception::invalidParameters();
        }
        $type          = isset($row[GenUser::AUTH_TYPE]) ? $row[GenUser::AUTH_TYPE] : NULL;
        $code          = isset($row[GenUser::AUTH_CODE]) ? $row[GenUser::AUTH_CODE] : NULL;
        $tempPass      = isset($row[GenUser::TEMP_PASSWORD]) ? $row[GenUser::TEMP_PASSWORD] : NULL;
        $userId        = $row[GenUser::USER_ID];
        $facebookToken = $row[GenUser::FB_TOKEN];
        
        if ($this->authService->registerIsIncomplete($userId)) {
            if ($type == 'PHONE') {
                $this->authService->validateFacebookSmsToken($userId, $code);
            } else if ($type == 'EMAIL') {
                $this->authService->validateTemporaryPassword($userId, $tempPass);
            } else throw new Exception('Authentication required!');
        }
        $user = $this->authService->linkFacebook($userId, $facebookToken);
        $dataSet = new DataSet('user', $user->toArray());
    
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * validateFacebookToken
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function validateFacebookToken(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $status   = Response::STATUS_SUCCESS;
        
        $authType = $row[GenUser::AUTH_TYPE];
        $authCode = $row[GenUser::AUTH_CODE];
        $userId   = $row[GenUser::USER_ID];
        
        $authenticated = false;
        
        if ($authType === GenUser::AUTH_TYPE_PHONE) {
            $authenticated = $this->authService->validateFacebookSmsToken($userId, $authCode);
        }
        $dataSet   = new DataSet('response', ['authenticated' => $authenticated]);
    
        $response->addDataSet($dataSet);
    }
    
    public function sendConfirmationEmail(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        $cpf    = $row["CPF"];
        $nrorg = $row['NRORG'];
        $bool = isset($row['PENDENT']) ? $row['PENDENT'] : false;
        $users  = $this->authService->getGenUserByCpfOrEmail($cpf, null);
        
        if (count($users)) {
            $result = $this->authService->sendConfirmationEmail($users[0], $nrorg, $bool);
        }
        else throw Exception::unauthorizedAction();
        
        $dataSet = new DataSet('response', ['status' => $result['status'], 'password' => $result['password']]);
    
        $response->addDataSet($dataSet);
    }

    public function validateTemporaryPassword(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $status = Response::STATUS_SUCCESS;
        $userId   = $row[GenUser::USER_ID];
        $tempPass = $row['TEMP_PASSWORD'];
        
        if ($this->authService->registerIsIncomplete($userId)) {
            $areEqual = $this->authService->checkTempPassword($userId, $tempPass);
        } 
        else throw Exception::unauthorizedAction();
        $dataSet = new DataSet('response', ['areEqual' => $areEqual]);
    
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * createPassword
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function createPassword(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $status = Response::STATUS_SUCCESS;
        
        $type     = $row[GenUser::AUTH_TYPE];
        $userId   = $row[GenUser::USER_ID];
        $code     = isset($row[GenUser::AUTH_CODE]) ? $row[GenUser::AUTH_CODE] : NULL;
        $tempPass = isset($row[GenUser::TEMP_PASSWORD]) ? $row[GenUser::TEMP_PASSWORD] : NULL;
        $newPass  = $row[GenUser::NEW_PASSWORD];
        
        if ($this->authService->registerIsIncomplete($userId)) {
            if ($type == 'PHONE') {
                $this->authService->validateFacebookSmsToken($userId, $code);
            } else {
                $this->authService->validateTemporaryPassword($userId, $tempPass);
            }
            $user = $this->authService->createPassword($userId, $newPass);
        } else throw Exception::unauthorizedAction();
        $dataSet  = new DataSet('user', $user->toArray());
    
        $response->addDataSet($dataSet);
        
    }

    /**
     * changePass
     * Change the password of an User
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     * @throws \Zeedhi\ApiGeneral\Service\Exception
     */
    public function changePassword(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        $status = Response::STATUS_SUCCESS;
        $userId      = $row['UPDATE_USER_ID'];
        $oldPassword = isset($row[GenUser::NEW_PASSWORD]) ? $row[GenUser::PASSWORD] : NULL;
        $newPassword = isset($row[GenUser::NEW_PASSWORD]) ? $row[GenUser::NEW_PASSWORD] : $row[GenUser::PASSWORD];
        $this->authService->changePassword($userId, $oldPassword, $newPassword);
        $dataSet  = new DataSet('response', ['success' => true]);
    
        $response->addDataSet($dataSet);
        
    }
    
    /**
     * Primeiro validar email informado pelo usuario
     * Buscar usuario ativo no banco de dados
     * Verificar de usuario existe
     * Mandar Mensagem para email do usuario encontrado
     */
    public function forgotPasswordSendEmail(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        if(filter_var($row['EMAIL'], FILTER_VALIDATE_EMAIL)) {
            $email = $row['EMAIL'];
        } else {
            throw new \Exception("invalid email", 1);
        }
        
        $genUser   = $this->authService->getGenUser(['email' => $email, 'status' => 'A']); // retorna user ativos
        
        if($genUser) {
            $dataSet  = new DataSet('response', ['msg' => "password recovery link sent to email: " . $email]);
            $response->addDataSet($dataSet);
        }else {
            throw new \Exception("User not found", 2);
        }
    }
    
    /**
     *
     * changeMobilePhoneNumber
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function changeMobilePhoneNumber(DTO\Request $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $userId   = $row[GenUser::USER_ID];
        $newEmail = $row[GenUser::EMAIL];
        /*$authCode = $row['AUTH_CODE'];*/
        /*$oldPhone = $row['OLD_PHONE'];*/
        $newPhone = $row['NEW_PHONE'];
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        
        $this->updateGenUserEmailCellPhoneWebService($nrorg, $tokenExt, $newEmail, $newPhone);
        
        $status   = $this->authService->changeMobilePhoneNumber($userId, /*$authCode, */$newPhone/*, $oldPhone*/);
        
        $response->addDataSet(new DataSet('response', ['status' => $status]));
    }
    
    /**
     *
     * changeEmail
     * @param DTO\Request  $request
     * @param DTO\Response $response
     */
    public function changeEmail(DTO\Request $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $userId   = $row[GenUser::USER_ID];
        $newEmail = $row[GenUser::EMAIL];
        $newPhone = $row['NEW_PHONE'];
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        
        $this->updateGenUserEmailCellPhoneWebService($nrorg, $tokenExt, $newEmail, $newPhone);
        
        $status   = $this->authService->changeEmail($userId, $newEmail);
        
        $response->addDataSet(new DataSet('response', ['status' => $status]));
    }

    /* Verifica se uma organização tem WebService, para isso a organização deve existir e está configurada! */
    public function useIntegration ($nrorg) {
        $organization = $this->authService->getOrganization($nrorg);
        
        if (isset($organization)) {
            
            $genConfiguration = $this->authService->getConfiguration($nrorg);
            if(isset($genConfiguration)) {
                return $genConfiguration ? $genConfiguration->getUseIntegration() : false;
            }else {
                throw Exception::organizationNotConfiguration();
            }
        }else {
            throw Exception::organizationNotFound();
        }
    }
    
    /*Valida data/hora do aplicativo com data/hora do servidor*/
    public function getTOTP(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $chave   = $row['c'];
        
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        $totp = $consumeWebServiceMTC->getTOTP($chave);
        
        $response->addDataSet(new DataSet('response', [$totp['totp']]));
    }
    
    /* Metodo que gerencia a criação de um usuario! */
    public function registerUser(DTO\Request\Row $request, DTO\Response $response){
        $row       = $request->getRow();
        
        $nrorg     = $row['NRORG'];
        $branch    = $row['BRANCH'];
        $date_birth= $row['DATE_BIRTH'];
        $imei      = $row['IMEI'];
        $cpf       = $row['CPF'];
        $npf       = $row['NPF'];
        $email     = $row['EMAIL'];
        $password  = $row['PASSWORD'];
        $password2 = $row['PASSWORD2'];
        
        $level = "registerUserWebService";
        $msg   = "nrorg: " . $nrorg . " e-mail: " . $email . " password: " . $password . " password2: " . $password2 . " npf: " . $npf . " date birth: " . $date_birth; 
        $this->registerLogs->logMsg("Request registerUserWebService: " . $msg, $level);
        
        $error     = [];
        
        $genUsers = $this->authService->getActiveUserNrorgByExternalId($nrorg, $npf);
        if(count($genUsers) > 1) {
            $response->addDataSet(new DataSet('webService', ['error' => ['status' => 'erro', 'des_erro' => 'Usuário duplicado.']]));
            return;
        }
    
        $userIds = $this->authService->getUserIdsByEmail($email); // retorna user ativos
        
        $genUser = count($genUsers) > 0 ? $genUsers[0] : NULL;
        
        
        
        // if(!empty($genUser)){
        //     foreach ($userIds as $id) {
        //         if($genUser->getId() != $id['id']) {
        //             $response->addDataSet(new DataSet('webService', ['error' => ['status' => 'erro', 'des_erro' => 'Usuário já cadastrado.']]));
        //             return ;
        //         }
        //     }
        // } else if(!empty($userIds)){
        //     $response->addDataSet(new DataSet('webService', ['error' => ['status' => 'erro', 'des_erro' => 'E-mail já cadastrado.']]));
        //     return ;
        // }
        

        $webService       = $this->useIntegration($nrorg);
        $responseArray    = [];
        $dataSet          = [];
    
        if($webService) {
            $integration  = new Integration(new ConsumeWebServiceMTC());
            
            $branches = array( "minas", "nautico");
            $returnWS = "";
            
            $row['BRANCHES'] = $branches;
            $returnWS = $integration->registerUserOnWebService($row);

            // if($returnWSInterno['res']['status'] == "ok") {
            //     $returnWS = $returnWSInterno;
            // }
            
            // $returnWS     = $integration->registerUserOnWebService($row);
            if($returnWS['res']['status'] !== "erro"){
                $this->synchronizeUserInLocaldatabase($nrorg, $email, $password, $integration, $genUser, $cpf, $npf);
                $responseArray = ['success' => $returnWS['res']];
            } else $responseArray = ['error' => $returnWS['res']];
        }
        
        $this->registerLogs->logMsg("Response registerUserWebService: " . serialize($responseArray), $level);
        
        $dataSet   = new DataSet('webService', $responseArray);
        
        $response->addDataSet($dataSet);
    }
    
    public function synchronizeUserInLocaldatabase($nrorg, $email, $password, $integration, $genUser = NULL, $cpf, $npf) {
        $organization = $this->getOrganization($nrorg);
        
        $responseLoginWB    = $integration->webServiceLogin($organization, $email, $cpf, $npf, $password);
        
        if($responseLoginWB['res']['status'] == "ok") {
            $genUser = $this->formatDataAndCreateOrUpdateGenUser($organization, $responseLoginWB['res']['token'], $integration, $password, $nrorg, $genUser);
            
        } else {
            
            throw new Exception('Falha na comunicação com a base de associados.', 9654);
        
        }    
    }
    
    public function formatDataAndCreateOrUpdateGenUser($filial, $token, $integration, $password, $nrorg, $genUser = NULL) {
        $branches = array( "minas", "nautico");
        $partnerData = "";
        foreach($branches as $branch){
            $partnerDataInterno = $integration->webServiceDadosAssociado($branch, $token);
            
            if($partnerDataInterno['res']['status'] == "ok") {
                $partnerData = $partnerDataInterno;
                $nrorg = $branch == "minas" ? 1 : 2;
            }
        }
        
        
        $fullName    = trim($partnerData['res']['nome']);
        $fullName    = explode(" ", $fullName, 2);
        
        $cpfMasc     = !empty($partnerData['res']['cpf'])    ? trim($partnerData['res']['cpf']) : NULL;
        $cpf         = $cpfMasc != NULL ? $this->removeMascaraCpf($cpfMasc) : NULL;
        
        $firstName   = $fullName[0];
        $lastName    = $fullName[1];
        $externalId  = !empty($partnerData['res']['npf'])               ? trim($partnerData['res']['npf']) : NULL;
        $email       = !empty($partnerData['res']['email'])             ? trim($partnerData['res']['email']) : NULL;
        $image       = !empty($partnerData['res']['foto_url'])          ? trim($partnerData['res']['foto_url']) : NULL;
        $phoneNumber = !empty($partnerData['res']['telefone_celular'])  ? trim($partnerData['res']['telefone_celular']) : NULL;
        $type        = !empty($partnerData['res']['telefone_celular'])  ? "MOBILE" : NULL;
        $userType    = 1;
        $status      = "A";

        $genUserResponse        = $this->authService->createOrUpdateGenUser($firstName, $lastName, $cpf, $email, $password, $image, $nrorg, $status, $genUser);
        
        if(empty($genUser)) $genUserProfile = $this->authService->addProfileUser($genUserResponse, $userType, $nrorg, $status, $externalId);
        
        $phoneNumber != NULL ? $this->authService->addMobilePhoneNumber($genUserResponse, $phoneNumber, $type) : NULL;
        
        if(!empty($partnerData['res']['dependentes']))
        
            $arrayDependents = $this->createOrUpdateDependents($partnerData, $nrorg, $genUserResponse);
        
        if(!empty($partnerData['res']['enderecos']))
            $this->createOrUpdateAddress($partnerData['res']['enderecos'], $nrorg, $genUserResponse);
        
        $this->authService->commit();
        return $genUser;
    }
    
    public function createOrUpdateDependents($partnerData, $nrorg, $genUserResponse) {
        foreach ($partnerData['res']['dependentes'] as $dependent) {
            $fullName    = trim($dependent['nome']);
            $fullName    = explode(" ", $fullName, 2);
            
            $cpfMasc     = !empty($dependent['cpf']) ? trim($dependent['cpf']) : NULL;
            $cpf         = $cpfMasc != NULL ? $this->removeMascaraCpf($cpfMasc) : NULL;
            
            $firstName   = $fullName[0];
            $lastName    = $fullName[1];
            $externalId = !empty($dependent['npf'])               ? trim($dependent['npf']) : NULL;
            $email       = NULL;
            $image       = !empty($dependent['foto_url'])          ? trim($dependent['foto_url']) : NULL;
            $phoneNumber = !empty($dependent['telefone_celular'])  ? trim($dependent['telefone_celular']) : NULL;
            $type        = !empty($partnerData['res']['telefone_celular'])  ? "MOBILE" : NULL;
            $userType    = 1;
            $status      = "A";
            
            
            $genUserDependent = $this->authService->getActiveUserNrorgByExternalId($nrorg, $externalId);
            /* cria os dependentes */
            if(empty($genUserDependent)) {
                $genUserDependent[0]   = $this->authService->createOrUpdateGenUser($firstName, $lastName, $cpf, $email, null, $image, $nrorg, $status);
                /* define o perfil */
                $genUserProfile = $this->authService->addProfileUser($genUserDependent[0], $userType, $nrorg, $status, $externalId);
                $phoneNumber != NULL ? $this->authService->addMobilePhoneNumber($genUserDependent, $phoneNumber, $type) : NULL;
            }

            /* relaciona os dependentes ao titular! */
            $genRelDependent    = $this->authService->addDependent($genUserResponse, $genUserDependent[0], 0, $genUserResponse->getEmail(), $nrorg);
        }
    }
    
    public function createOrUpdateAddress($enderecos, $nrorg, $genUser) {
        foreach ($enderecos as $key => $endereco) {
            $street         = $endereco['logradouro'];
            $neighborhood   = $endereco['bairro'];
            $city           = $endereco['cidade'];
            $provincy       = $endereco['uf'];
            $number         = $endereco['numero'];
            $complement     = $endereco['complemento'];
            $cep            = $endereco['cep'];
            $country        = $endereco['pais'];
            $status         = "A";
            $document       = NULL;
            $type           = $key === 'cobranca' ? 'BILLING' : 'CORRESPONDENCE';
            $this->authService->requestAddressChange($genUser, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document, $status);
        }
    }
    
    public function removeMascaraCpf($cpf) {
        $chars = array(".","-");
        $newCpf = str_replace($chars,"",$cpf);
        return $newCpf;
    }
    
    /*verifica a qual organization um user pertence*/
    public function getOrganization($nrorg) {
        $organization = $this->authService->getOrganization($nrorg);
        
        if(isset($organization)){
            switch ($organization->getNrorg()) {
                case 0:
                    $response = "teknisa";
                    break;
                case 1:
                    $response = "minas";
                    break;
                case 2:
                    $response = "nautico";
                    break;
                case 101:
                    $response = "pinheiros";
                    break;
            }
        }else {
            throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    public function getOrganizationOne($nrorg) {
        $organization = $this->authService->getOrganization($nrorg);
        
        if(isset($organization)){
            switch ($organization->getNrorg()) {
                case 0:
                    $response = "teknisa";
                    break;
                case 1:
                    $response = "minas";
                    break;
                case 2:
                    $response = "nautico";
                    break;
            }
        }else {
            throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    /*CRISTIANO Verificar se o usuario pertence a organizacao*/
    public function verifyIfUserBelongOrganization($email, $nrorg) {
        $user = $this->authService->getGenUser(['email' => $email, 'status' => 'A']);
        $userTypeRel = $this->authService->getGenUserType($user->getId());
        if($userTypeRel->getNrorg() !== $nrorg)
            throw Exception::userNotBelongOrganization();
    }
    
    public function updateGenUserEmailCellPhoneWebService($nrorg, $tokenExt, $email, $phoneNumber) {
        $branch = "minas";
        
        $webService  = $this->useIntegration($nrorg);
        
        if($webService) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
            
            $result = $consumeWebServiceMTC->changeData($branch, $tokenExt, $email, $phoneNumber);
            
            if($result['res']['status'] != "ok") {
                throw new Exception($result['res']['des_erro'], 1002);
            }
        }
    }
    
    public function loginBackofficeCheckingStatusdifferentofI(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();

        $status = Response::STATUS_SUCCESS;
        if (empty($row[GenUser::EMAIL]) && !empty($row[GenUser::PASSWORD])) {
            throw Exception::invalidParameters();
        }
        $email         = $row[GenUser::EMAIL];
        $password      = $row[GenUser::PASSWORD];
        $nrorg         = $row[GenOrganization::NRORG];
        $genUserTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : GenUserType::VENDEDOR_ID;
        
        
        $user = $this->authService->loginBackOfficeCheckingStatusdifferentofI($email, $password);
        // $userStatus = $this->authService->getGenUser(['email' => $email, 'status' => $user->getStatus()]);
        
        $genUserTypeRel = $this->authService->getGenUserTypeRelAdmin($user->getId(), $genUserTypeId, $nrorg);

        if($user->getStatus() === 'I') throw new \Exception('Deleted user!', 99);
        if(!$genUserTypeRel) throw new \Exception('User without profile!', 98);
        if($genUserTypeRel->getStatus() ==! 'A') throw new \Exception('Inactive user!', 99);
        
        
        
        
        $dataSet     = new DataSet('user', [
            'userId'        => $user->getId(),
            'refresh_token' => $user->getToken(),
            'pos_serial_number'=> $user->getPosSerialNumber()
        ]);
        
        
        $response->addDataSet($dataSet);
    }    

   public function sendConfirmationEmailAdmin(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        $cpf    = $row["CPF"];
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : 0;
        $email    = $row["EMAIL"];
        $bool = isset($row['PENDENT']) ? $row['PENDENT'] : false;
        $users  = $this->authService->getGenUserByCpfOrEmailuserExistsByCpfAndReturnData(null, $email);
        if (count($users)) {
            $result = $this->authService->sendConfirmationEmailAdmin($users, $nrorg, $bool);
        }
        else throw Exception::unauthorizedAction();
        
        $dataSet = new DataSet('response', ['status' => $result['status'], 'password' => $result['password']]);
    
        $response->addDataSet($dataSet);
    }
    
}