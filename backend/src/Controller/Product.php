<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;

class Product extends Crud {
    
    protected $dataSourceName = 'product';

}