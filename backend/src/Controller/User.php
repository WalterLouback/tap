<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Model\Entities\GenAddress;
use Zeedhi\ApiGeneral\Model\Entities\GenContact;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenDataUpdate;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\GenTag;
use Zeedhi\ApiGeneral\Model\Entities\PayCreditcard;
use Zeedhi\ApiGeneral\Service\User as UserService;
use Zeedhi\ApiGeneral\Service\Auth as AuthService;
use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration;

use Zeedhi\ApiGeneral\Helpers\Util;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;

use Zeedhi\ApiGeneral\Helpers\ImageUploader;

class User extends Crud {
    
    const SALT = 'BIPPOPSALT';

protected $dataSourceName = 'user';

    /**
     * __contruct
     *
     * @param AuthService $authService
     */
    public function __construct(UserService $userService, ImageUploader $imageUploader) {
        $this->userService = $userService;
        $this->imageUploader = $imageUploader;
    }
    /**
     * getUserData
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function getUserData(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row[GenOrganization::NRORG];
        $userId     = $row[GenUser::USER_ID];
        $userTypeId = $row[GenUserType::USER_TYPE_ID];
        $tokenInt   = $row['TOKEN'];
    
        $user       = $this->userService->getUserData($userId, $userTypeId, $nrorg);
        if($user->getStatus() === 'I') throw new \Exception('Inactive user!', 99);
        $arrayDataSet = "";
        $arrayDataSet = $user->toArray($userTypeId, $nrorg);
        
        $webService = $this->useIntegration($nrorg);
        
        if ($userTypeId == 1 && $webService) {
            $tokenExt   = $row['TOKEN_EXT'];
            $dependentsWebService = $this->getUserDependentsWebService($nrorg, $tokenExt);
            
            if($dependentsWebService['status'] == "erro") {
                $resposta['res'] = $dependentsWebService;
                $resposta['res']['code'] = 401;
                $dataSet = new DataSet('response', $resposta);
            } else {
                $arrayDataSet['dependents'] = ($dependentsWebService['status'] == "ok" && isset($dependentsWebService['dependentes'])) ? $dependentsWebService['dependentes'] : "";
                $dataSet = new DataSet('user', $arrayDataSet);
            }
        } else {
            $dataSet = new DataSet('user', $arrayDataSet);
        }
        
        $response->addDataSet($dataSet);
    }
    
    /**
     * requestAddressChange
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     * @throws Exception
     */
    
    public function updateAddressProfile(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row["UPDATE_USER_ID"];
        $nrorg = $row["NRORG"];
        $cep = $row["CEP"];
        $street = $row["STREET"];
        $neighborhood = $row["NEIGHBORHOOD"];
        $city = $row["CITY"];
        $provincy = $row["PROVINCY"];
        $number = intval($row["NUMBER"]);
        $complement = $row["COMPLEMENT"];
        $country = isset($row["COUNTRY"]) ? $row["COUNTRY"] : 'BRASIL';
        $type = isset($row["TYPEADDRESS"]) ? $row["TYPEADDRESS"] : "BILLING_ADDRESS";
        $tokenExt = isset($row["TOKEN"]) ? $row["TOKEN"] : NULL;
        
        $this->userService->setLastGenUserToken($userId, $tokenExt);
        
        $status = $this->userService->updateAddressProfile($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
        $dataSet = new DataSet('response', ['status' => $status]);
        
        $response->addDataSet($dataSet);
    }
    
    public function requestAddressChange(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $nrorg = $row[GenAddress::NRORG];
        $cep = $row[GenAddress::CEP];
        $street = $row[GenAddress::STREET];
        $neighborhood = $row[GenAddress::NEIGHBORHOOD];
        $city = $row[GenAddress::CITY];
        $provincy = $row[GenAddress::PROVINCY];
        $number = intval($row[GenAddress::NUMBER]);
        $complement = $row[GenAddress::COMPLEMENT];
        $country = isset($row[GenAddress::COUNTRY]) ? $row[GenAddress::COUNTRY] : 'BRASIL';
        $type = isset($row[GenAddress::TYPE]) ? $row[GenAddress::TYPE] : GenAddress::TYPE_BILLING;
        $document = isset($row[GenDataUpdate::DOCUMENT]) ? $row[GenDataUpdate::DOCUMENT] : NULL;
        $tokenExt = isset($row["TOKEN_EXT"]) ? $row["TOKEN_EXT"] : NULL;
        $latitude = isset($row["LATITUDE"]) ? $row["LATITUDE"] : NULL;
        $longitude = isset($row["LONGITUDE"]) ? $row["LONGITUDE"] : NULL;
        
        if($type == GenAddress::TYPE_CORRESPONDENCE){
            $this->requestAddressChangeWebService($tokenExt, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $type);
        }
        
        $this->userService->setLastGenUserToken($userId, $tokenExt);
        
         
        if( $type!="DELIVERY") $type == 'BILLING' ? $type = 'BILLING_ADDRESS' : $type = 'CORRESPONDENCE_ADDRESS';

        $status = $this->userService->requestAddressChange($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document, $latitude, $longitude);
        $dataSet = new DataSet('status', ['status' => $status]);
        
        $response->addDataSet($dataSet);
    }
    
    /**
     * requestUserChange
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     * @throws Exception
     */
    public function requestUserChange(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $firstName = $row[GenUser::FIRST_NAME];
        $lastName = $row[GenUser::LAST_NAME];
        $cpf = $row[GenUser::CPF];
        $nrorg = $row[GenOrganization::NRORG];
        $document = $row[GenDataUpdate::DOCUMENT];
        $status = $this->userService->requestUserChange($userId, $nrorg, $firstName, $lastName, $cpf, $document);
        $dataSet = new DataSet('status', ['status' => $status]);
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * addContactMethod
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function addContactMethod(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $type = isset($row[GenContact::CONTACT_TYPE]) ? $row[GenContact::CONTACT_TYPE] : GenContact::CONTACT_TYPE_MOBILE;
        $number = $row[GenContact::PHONE_NUMBER];
        $contact = $this->userService->addContactMethod($userId, $type, $number);
        $dataSet = new DataSet('contactMethod', $contact->toArray());
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * removeContactMethod
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function removeContactMethod(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $contactMethodId = $row[GenContact::CONTACT_METHOD_ID];
        $contact = $this->userService->removeContactMethod($contactMethodId, $userId);
        $dataSet = new DataSet('contactMethod', $contact->toArray());
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * verifyMobilePhone
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function verifyMobilePhone(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $phoneNumber = $row[GenContact::PHONE_NUMBER];
        $areEqual = $this->userService->verifyMobilePhone($userId, $phoneNumber);
        $response->addDataSet(new DataSet('response', ['ARE_EQUAL' => $areEqual]));
    }
    
    /**
     *
     * getContactMethods
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function getContactMethods(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $cpf = isset($row[GenUser::CPF]) ? $row[GenUser::CPF] : NULL;
        $npf = isset($row[GenUserTypeRel::EXTERNAL_ID]) ? $row[GenUserTypeRel::EXTERNAL_ID] : NULL;
        if ($cpf == NULL && $npf == NULL) {
            throw Exception::missingParameters();
        } else if ($cpf != NULL) {
            $contactMethods = $this->userService->getContactMethodsByCpf($cpf);
        } else {
            $contactMethods = $this->userService->getContactMethodsByExternalId($npf);
        }
        if ($contactMethods == NULL) {
            $contactMethods = [];
        }
        $response->addDataSet(new DataSet('contactMethods', $contactMethods));
    }
    
    /**
     * getUserDependents
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     */
    public function getUserDependents(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $nrorg = $row[GenOrganization::NRORG];
        
        $dependents = $this->userService->getUserDependents($userId, $nrorg);
        $userDataSet = new DataSet('dependents', GenDependent::manyToArray($dependents));
        $response->addDataSet($userDataSet);
    }
    
    /**
     * 
     * 
     */
    public function addDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $monthlyLimit = isset($row[GenDependent::MONTHLY_LIMIT]) ? $row[GenDependent::MONTHLY_LIMIT] : 200;
        $receiptsTo = isset($row[GenDependent::RECEIPTS_TO]) ? $row[GenDependent::RECEIPTS_TO] : NULL;
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->addDependent($userId, $dependentId, $monthlyLimit, $receiptsTo, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * removeDependent
     */
    public function removeDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->removeDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * enableDependent
     */
    public function enableDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->enableDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * disableDependent
     */
    public function disableDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->disableDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * enableDependentForMainCard
     */
    public function enableDependentForMainCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->enableDependentForMainCard($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * disableDependentForMainCard
     */
    public function disableDependentForMainCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->disableDependentForMainCard($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function editDependentMonthlyLimit(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $monthlyLimit = $row[GenDependent::MONTHLY_LIMIT];
        $this->userService->editDependentMonthlyLimit($userId, $dependentId, $nrorg, $monthlyLimit);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function editDependentReceiptsTo(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = isset($row[GenDependent::DEPENDENT_ID]) ? $row[GenDependent::DEPENDENT_ID] : NULL;
        $nrorg = $row[GenOrganization::NRORG];
        $receiptsTo = $row[GenDependent::RECEIPTS_TO];
        $this->userService->editDependentReceiptsTo($userId, $dependentId, $nrorg, $receiptsTo);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function getMonthExpensesFromDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $totalExpenses = $this->userService->getMonthExpensesFromDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['total' => $totalExpenses]);
        $response->addDataSet($dataSet);
    }
    
    public function setMainCreditCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $cardId = $row['CREDITCARD_ID'];
        $this->userService->setMainCreditCard($userId, $cardId);
        $dataSet = new DataSet('response', ['status' => "Ok"]);
        $response->addDataSet($dataSet);
    }
    
    /**
     * CRISTIANO Integração
     * Este metodo sincroniza o DB local com o MTC, atravez de WEBSERVICE
     */
    public function synchronizationLocalWebService($userId, $userTypeId, $nrorg, $tokenExt, $tokenInt) {
        $useIntegration = $this->useIntegration($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        if($useIntegration){
            $organization = $this->getOrganization($nrorg);
            $response = $consumeWebServiceMTC->webServiceDadosAssociado($organization, $tokenExt);
            
            $user = $this->userService->getUserData($userId, $userTypeId, $nrorg);
            if($user->getStatus() === 'I') throw new \Exception('Inactive user!', 99);
            if($response['res']['status'] == "ok") {
                $result = $this->verifyDateOfChange($response['res']['data_alteracao'], $user->getModifiedAt());

                if(empty($user->getFirstName()) || $result){
                    $this->updateGenUserWebService($userId, $nrorg, $response);
                    $this->createOrUpdateAddress($userId, $response['res'], $nrorg);
                    $arrayUserIdDependents = $this->createOrUpdateGenUserDependentWebService($userId, $response['res'], $nrorg) ;
                    if ($arrayUserIdDependents) {
                        foreach ($arrayUserIdDependents as $value) {
                            $this->userService->addDependent($userId, $value, 0, $response['res']['email'], $nrorg);
                        }
                    }
                }
            }elseif($response['res']['status'] == "erro" && $response['res']['des_erro'] == "Sua sessão expirou! Favor efetue login novamente!") {
                throw Exception::unauthenticatedUserInWebservice();
            }else {
                throw new Exception($response['res']['des_erro'], 1002);
            }
        }
    }
    
    /*verifica a qual organization um user pertence*/
    public function getOrganization($nrorg) {
        $organization = $this->userService->getOrganization($nrorg);
        
        if(isset($organization)){
            switch ($organization->getNrorg()) {
                case 0:
                    $response = "teknisa";
                    break;
                case 1:
                    $response = "minas";
                    break;
                case 2:
                    $response = "nautico";
                    break;
            }
        }else {
            throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    /* Verifica se data externa e maior que a interna!!!*/
    public function verifyDateOfChange($dataExt, $dataInt) {
        $newFormat  = str_replace("/", "-", $dataExt);
        $newDataExt = new \DateTime($newFormat);
        
        return ($newDataExt > $dataInt);
    }
    
    /*Atualiza o Usuario e contatos */
    public function updateGenUserWebService($userId, $nrorg, $response){
        unset($response['res']['enderecos']);
        unset($response['res']['dependentes']);
        $fullName    = trim($response['res']['nome']);
        $fullName    = explode(" ", $fullName, 2);
        
        $firstName   = $fullName[0];
        $lastName    = $fullName[1];
        $externalId  = trim($response['res']['npf']);
        $email       = trim($response['res']['email']);
        $image       = trim($response['res']['foto_url']);
        $phoneNumber = trim($response['res']['telefone_celular']);
        $cpf         = trim($response['res']['cpf']);
        $type        = "MOBILE";
        $userType    = 1;
        
        $this->userService->updateGenUserWebService($userId, $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userType);
        $this->userService->updateContactMethod($userId, $type, $phoneNumber);
    }
        
    /*createOrUpdate Addresses*/
    public function createOrUpdateAddress($userId, $response, $nrorg) {
        $getAddresses = $this->userService->getUserAddress($userId, $nrorg);
       /*verifica se user não tem endereço*/
        if(!empty($getAddresses)){
   
                $street         = $response['enderecos']['cobranca']['logradouro'];
                $neighborhood   = $response['enderecos']['cobranca']['bairro'];
                $city           = $response['enderecos']['cobranca']['cidade'];
                $provincy       = $response['enderecos']['cobranca']['uf'];
                $number         = $response['enderecos']['cobranca']['numero'];
                $complement     = $response['enderecos']['cobranca']['complemento'];
                $cep            = $response['enderecos']['cobranca']['cep'];
                $country        = $response['enderecos']['cobranca']['pais'];
                $type           = "BILLING";
                $status         = "A";
            /*inseri o endereco de cobrança*/
            $this->userService->requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
            
                $street         = $response['enderecos']['correspondencia']['logradouro'];
                $neighborhood   = $response['enderecos']['correspondencia']['bairro'];
                $city           = $response['enderecos']['correspondencia']['cidade'];
                $provincy       = $response['enderecos']['correspondencia']['uf'];
                $number         = $response['enderecos']['correspondencia']['numero'];
                $complement     = $response['enderecos']['correspondencia']['complemento'];
                $cep            = $response['enderecos']['correspondencia']['cep'];
                $country        = $response['enderecos']['correspondencia']['pais'];
                $type           = "CORRESPONDENCE";
                $status         = "A";
            /*inseri o endereco de correspondencia*/
            $this->userService->requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
        }else {
            if($response['enderecos']['cobranca']) {
                $street         = $response['enderecos']['cobranca']['logradouro'];
                $neighborhood   = $response['enderecos']['cobranca']['bairro'];
                $city           = $response['enderecos']['cobranca']['cidade'];
                $provincy       = $response['enderecos']['cobranca']['uf'];
                $number         = $response['enderecos']['cobranca']['numero'];
                $complement     = $response['enderecos']['cobranca']['complemento'];
                $cep            = $response['enderecos']['cobranca']['cep'];
                $country        = $response['enderecos']['cobranca']['pais'];
                $type           = "BILLING";
                $status         = "A";
                /*atualiza o endereco de cobrança*/
                $this->userService->createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
                
            }if($response['enderecos']['correspondencia']) {
                $street         = $response['enderecos']['correspondencia']['logradouro'];
                $neighborhood   = $response['enderecos']['correspondencia']['bairro'];
                $city           = $response['enderecos']['correspondencia']['cidade'];
                $provincy       = $response['enderecos']['correspondencia']['uf'];
                $number         = $response['enderecos']['correspondencia']['numero'];
                $complement     = $response['enderecos']['correspondencia']['complemento'];
                $cep            = $response['enderecos']['correspondencia']['cep'];
                $country        = $response['enderecos']['correspondencia']['pais'];
                $type           = "CORRESPONDENCE";
                $status         = "A";
                /*atualiza o endereco de correspondencia*/
                $this->userService->createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
            }
        }
    }
    
    /*create Dependents*/
    public function createOrUpdateGenUserDependentWebService($userId, $response, $nrorg) {
        $arrayUserIdDependents = "";
        /*verifica se associado não tem dependentes*/
        if(!empty($response['dependentes'])) {
            /*percorre os dependentes*/
            foreach ($response['dependentes'] as $dependent) {
                    $user        = $this->userService->getUserWebService($dependent['cpf'], $dependent['npf']);
                    $fullName    = trim($dependent['nome']);
                    $fullName    = explode(" ", $fullName, 2);
                    
                    $firstName   = $fullName[0];
                    $lastName    = $fullName[1];
                    $externalId  = $dependent['npf']                         ? trim($dependent['npf'])      : "";
                    $email       = $dependent['email'] && $dependent['email'] != $response['email'] ? trim($dependent['email'])    : "";
                    $image       = $dependent['foto_url']                    ? trim($dependent['foto_url']) : "";
                    $cpf         = $dependent['cpf']                         ? trim($dependent['cpf'])      : "";
                    $phoneNumber = "31900000000";
                    $type        = "MOBILE";
                    $password    = "123456";
                    
                  /*se o dependente não existe no db então inseri-o na table GenUser*/
                  if(!empty($user)) {
                        $userType    = 1;
                        $this->userService->updateGenUserWebService($user->getId(), $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userType);
                  }else {/*senão atualiza*/
                        $this->userService->createGenUserDependentWebService($firstName, $lastName, $email, $password, $nrorg, $externalId, $cpf);
                  }
                $dependent = $this->userService->getUserWebService($cpf, $externalId);
                if($cpf) {
                    $arrayUserIdDependents[] = $dependent->getId();
                }else {
                    $arrayUserIdDependents[] = $dependent->getGenUser()->getId();
                }
            }
           return $arrayUserIdDependents;/*retorna um array com os id dos dependentes*/
        }
    }
    
    public function useIntegration ($nrorg) {
        $organization = $this->userService->getOrganization($nrorg);
        
        if (isset($organization)) {
            
            $genConfiguration = $this->userService->getConfiguration($nrorg);
            if(isset($genConfiguration)) {
                return $genConfiguration ? $genConfiguration->getUseIntegration() : false;
            }else {
                throw Exception::organizationNotConfiguration();
            }
        }else {
            throw Exception::organizationNotFound();
        }
    }
    
    /*busca dependentes do titular*/
    public function getUserDependentsWebService ($nrorg, $tokenExt){
        $filial = $this->getOrganization($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        $response = $consumeWebServiceMTC->webServiceDadosAssociado($filial, $tokenExt);

        return $response['res'];
    }
    /*CRISTIANO Integração*/
    
    public function getUsersFromOrganization(DTO\Request $request, DTO\Response $response) {
        $row      = Util::getParamsAsArray($request);
        
        $nrorg    = $row['NRORG'];
        $userName    = isset($row['FULL_NAME']) ? $row['FULL_NAME'] : '';
        $cpf = isset($row['CPF']) ? $row['CPF'] : '';
        $email = isset($row['EMAIL']) ? $row['EMAIL'] : '';
        $telefone = isset($row['MAIN_PHONE']) ? $row['MAIN_PHONE'] : '';
        $tipoUsuario = isset($row['USR_TYPES']) ? implode(",", $row['USR_TYPES']) : '';
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 200;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        $users    = $this->userService->getUsersFromOrganization($nrorg,$userName,$cpf,$email,$telefone,$tipoUsuario,$itemsPerPage, $page);
        
        $dataSet  = new DataSet('users', GenUser::manyToArray($users, NULL, $nrorg));
        $response->addDataSet($dataSet);
    }
    
    public function getUsersFromOrganizationWithStatusNotInative(DTO\Request $request, DTO\Response $response) {
        $row      = Util::getParamsAsArray($request);
        
        $nrorg    = $row['NRORG'];
        $userName    = isset($row['FULL_NAME']) ? $row['FULL_NAME'] : '';
        $cpf = isset($row['CPF']) ? str_replace('-','',str_replace('.','',$row['CPF'])) : '';
        $email = isset($row['EMAIL']) ? $row['EMAIL'] : '';
        $telefone = isset($row['MAIN_PHONE']) ? $row['MAIN_PHONE'] : '';
        $tipoUsuario = isset($row['USR_TYPES']) ? implode(",", $row['USR_TYPES']) : '';
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 200;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 1;
        $users    = $this->userService->getUsersFromOrganizationWithStatusNotInative($nrorg,$userName,$cpf,$email,$telefone,$tipoUsuario,$itemsPerPage, $page);
        
        $dataSet  = new DataSet('users', GenUser::manyToArray($users, NULL, $nrorg));
        $response->addDataSet($dataSet);
    }

    public function getDataUpdateRequests(DTO\Request $request, DTO\Response $response) {
        $row          = Util::getParamsAsArray($request);
        $nrorg        = $row[GenOrganization::NRORG];
        $statuses     = !empty($row['STATUSES']) ? $row['STATUSES'] : ['P'];
        $typeDocument = !empty($row['TYPE_DOCUMENT']) ? $row['TYPE_DOCUMENT'] : null;
        $dateRange    = !empty($row['DATE_RANGE']["start"]) ? $row['DATE_RANGE'] : null;
        $requests     = $this->userService->getDataUpdateRequests($nrorg, $statuses, $typeDocument, $dateRange);
        $requestsData = GenDataUpdate::manyToArray($requests);
        $dataSet      = new DataSet('requests', $requestsData);
        
        $response->addDataSet($dataSet);
    }
    
    public function acceptDataUpdateRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        
        $dataUpdateId = $row['DATA_UPDATE_ID'];
        $userId       = $row['USER_ID'];
        $address      = isset($row['ADDRESS']) ? $row['ADDRESS'] : NULL;
        
        $dataUpdate = $this->userService->acceptDataUpdateRequest($dataUpdateId, $address, $userId);
        
        $lastToken  = $this->userService->getLastGenUserToken($userId);
        if($lastToken != null) {
            if (GenAddress::TYPE_BILLING == $address['type'] . "_ADDRESS") {
                $this->requestAddressChangeWebService($lastToken, $dataUpdate->getNrorg(), $address["newCep"], $address["newStreet"], $address["newNeighborhood"], $address["newCity"], $address["newState"], $address["newNumber"], $address["newComplement"], $address['type'] . "_ADDRESS");
            }
        }
        $dataSet  = new DataSet('response', ['status' => 'Ok']);
        
        $response->addDataSet($dataSet);
    }
    
    public function denyDataUpdateRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        
        $dataUpdateId = $row['DATA_UPDATE_ID'];
        $userId       = $row['USER_ID'];

        $this->userService->denyDataUpdateRequest($dataUpdateId, $userId);
        $dataSet  = new DataSet('response', ['status' => 'Ok']);
        
        $response->addDataSet($dataSet);
    }
    
    public function updateUserProfileStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();

        $adminId    = $row[GenUser::USER_ID];
        $userTypeId = $row['USER_TYPE_ID'];
        $nrorg      = $row['NRORG'];
        $status     = $row['STATUS'];
        $userId     = $row['UPDATE_USER_ID'];
        
        $this->userService->updateUserProfileStatus($userId, $userTypeId, $nrorg, $status, $adminId);
        
        $dataSet    = new DataSet('response', ['status' => 'Ok']);

        $response->addDataSet($dataSet);
    }

    public function createUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $adminId    = $row[GenUser::USER_ID];
        $firstName  = isset($row['FIRST_NAME']) ? $row['FIRST_NAME'] : "";
        $lastName   = isset($row['LAST_NAME']) ? $row['LAST_NAME'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $phone      = isset($row['PHONE']) ? $row['PHONE'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $birthDate  = isset($row['BIRTH_DATE']) ? $row['BIRTH_DATE'] : "";
        $userTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : 3;
        $status = "A";
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            $image = strlen($row['IMAGE']) > 500 ? $ImageUploader->upload($row['IMAGE'], $nrorg) : $row['IMAGE'];
        }
        
        /* Searching for the user in database */
        $user = NULL;
        // if ($phone) $user = $this->userService->verifyPhoneInDatabase($phone);
        if ($user === NULL) $user = $this->userService->getGenUser($email, $cpf);
        
        /* Checking if the user exists in database */
        if ($user === NULL) {
            /* If not, create the user with the given data */
            $this->userService->createUser($firstName, $lastName, $cpf, $email, $phone, $password, $nrorg, $userTypeId, $birthDate, $image);
        } else if ($nrorg !== NULL && $nrorg != 0) {
            /* Checking if user has the requested profile in this organization */
            $genUserTypeRel = $this->userService->getGenUserTypeWB($user->getId(), $userTypeId, $nrorg);
        
            if ($genUserTypeRel === NULL) {
                /* If not, create the profile*/
                $this->userService->addProfileUser($user->getId(), $userTypeId, $nrorg, $status, $adminId);
            } else if ($genUserTypeRel->getStatus() == 'I') {
                /* If the profile is disabled, activate it */
                $this->userService->activateUserTypeRel($genUserTypeRel);
            } else {
                /* If the profile is Ok, throw exception */
                throw Exception::userExistsInTheDatabase();
            }
        } else {
            throw Exception::userExistsInTheDatabase();
        }
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }

    public function createUserAdmin(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $createdMessage = 'Ok';        
        $adminId    = $row[GenUser::USER_ID];
        $firstName  = isset($row['FIRST_NAME']) ? $row['FIRST_NAME'] : "";
        $lastName   = isset($row['LAST_NAME']) ? $row['LAST_NAME'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $phone      = isset($row['PHONE']) ? $row['PHONE'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $birthDate  = isset($row['BIRTH_DATE']) ? $row['BIRTH_DATE'] : "";
        $userTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : 3;
        $status = "A";
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            $image = strlen($row['IMAGE']) > 500 ? $ImageUploader->upload($row['IMAGE'], $nrorg) : $row['IMAGE'];
        }
        //teknisateste11@gmail.com
        /* Searching for the user in database */
        $user = NULL;
        // if ($phone) $user = $this->userService->verifyPhoneInDatabase($phone);
        if ($user === NULL) $user = $this->userService->getGenUserAdmin($email, $cpf);
        /* Checking if the user exists in database */
        if ($user === NULL) {
            /* If not, create the user with the given data */
            $this->userService->createUserAdmin($firstName, $lastName, $cpf, $email, $phone, $password, $nrorg, $userTypeId, $birthDate, $image);
        } else if ($nrorg !== NULL && $nrorg != 0) {
            /* Checking if user has the requested profile in this organization */
            $createdMessage = 'profile Ok';
            $genUserTypeRel = $this->userService->getGenUserTypeWBAdmin($user->getId(), $userTypeId, $nrorg);
            if ($genUserTypeRel === NULL) {
                /* If not, create the profile*/
                $this->userService->addProfileUserAdmin($user->getId(), $userTypeId, $nrorg, $status, $adminId);
            } else if ($genUserTypeRel->getStatus() == 'I') {
                /* If the profile is disabled, activate it */
                $this->userService->activateUserTypeRelAdmin($genUserTypeRel);
            } else {
                /* If the profile is Ok, throw exception */
                throw Exception::userExistsInTheDatabase();
            }
        } else {
            throw Exception::userExistsInTheDatabase();
        }
        
        $dataSet = new DataSet('response', ['status' => $createdMessage]);
        $response->addDataSet($dataSet);
    }
    
    /**
     * Este metodo criar users dos apps filhos do xappa.
     */
    public function createUserV1(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $adminId    = $row[GenUser::USER_ID];
        $firstName  = isset($row['FIRST_NAME']) ? $row['FIRST_NAME'] : "";
        $lastName   = isset($row['LAST_NAME']) ? $row['LAST_NAME'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $phone      = isset($row['PHONE']) ? $row['PHONE'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $birthDate  = isset($row['BIRTH_DATE']) ? $row['BIRTH_DATE'] : "";
        $userTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : 1;
        $status = "A";
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            $image = strlen($row['IMAGE']) > 500 ? $ImageUploader->upload($row['IMAGE'], $nrorg) : $row['IMAGE'];
        }
        
        /* Searching for the user in database */
        $user = NULL;
        // if ($phone) $user = $this->userService->verifyPhoneInDatabase($phone);
        if ($user === NULL) $user = $this->userService->getGenUser($email, $cpf);
        
        /* Checking if the user exists in database */
        if ($user === NULL) {
            /* If not, create the user with the given data */
            $this->userService->createUser($firstName, $lastName, $cpf, $email, $phone, $password, $nrorg, $userTypeId, $birthDate, $image);
        } else if ($nrorg !== NULL && $nrorg != 0) {
            /* Checking if user has the requested profile in this organization */
            $genUserTypeRel = $this->userService->getGenUserTypeWB($user->getId(), $userTypeId, $nrorg);
        
            if ($genUserTypeRel === NULL) {
                /* If not, create the profile*/
                $this->userService->addProfileUser($user->getId(), $userTypeId, $nrorg, $status, $adminId, $email, $password);
            } else if ($genUserTypeRel->getStatus() == 'I') {
                /* If the profile is disabled, activate it */
                $this->userService->activateUserTypeRel($genUserTypeRel, $email, $password);
            } else {
                /* If the profile is Ok, throw exception */
                throw Exception::userExistsInTheDatabase();
            }
        } else {
            throw Exception::userExistsInTheDatabase();
        }
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }

    public function userExistsByCpf(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        
        $user = NULL;
        $user = $this->userService->getGenUserByCpfOrEmail($cpf, null, $nrorg);
        
        if ($user !== NULL) {
            throw Exception::userExistsInTheDatabase();
        }
        
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function cryptPassword($password) {
        return strtoupper(hash('sha512', self::SALT.$password));
    }
    
    public function userExistsByCpfAndReturnData(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $cpf        = isset($row['CPF']) ? $row['CPF'] : null;
        $email        = isset($row['EMAIL']) ? $row['EMAIL'] : null;
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : null;
        
        $cryptedPassword = $this->cryptPassword($password);
        
        $user = NULL;
        $user = $this->userService->getGenUserByCpfOrEmail($cpf, null);
        //$user = $this->userService->getGenUserByCpfOrEmailuserExistsByCpfAndReturnData(null, $email);
        
        if ($user == NULL) {
            throw Exception::loginNotFound();
        }
        
        $dataSet = new DataSet('response', ['status' => 'Ok', 'user' => $user, 'password' => $cryptedPassword]);
        $response->addDataSet($dataSet);
    }


    public function updateFCMToken(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId = $row['USER_ID'];
        $token  = $row['FCM_TOKEN'];
        $nrorg  = $row['NRORG'];
        
        $organizationUserRel = $this->userService->updateFCMToken($userId, $token, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok', 'Id' => $organizationUserRel->getId()]));
    }
    
    public function requestAddressChangeWebService($tokenExt, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $type) {
        $webService  = $this->useIntegration($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        
        if($webService) {
            $branchs = ["minas", "nautico"];
            $type == "CORRESPONDENCE_ADDRESS" ? $type = 2 : $type = 1;
            foreach ($branchs as $branch) {
                $returnWS = $consumeWebServiceMTC->changeAddress($branch, $tokenExt, $type, $cep, $provincy, $city, $neighborhood, $street, $number, $complement);
                
                if($returnWS['res']['status'] == "ok") {
                    break;
                }
            }
            
            if($returnWS['res']['status'] != "ok") { 
                throw new Exception($returnWS['res']['des_erro'], 1002);
            }
        }
    }
    
    /* insere um caractere em posição específica */
    function replaceByPos($str, $pos, $c){
        return substr($str, 0, $pos) . $c . substr($str, $pos);
    }
    
    public function getBillsFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $nrorg    = $row['NRORG'];
        
        if ($nrorg == "1") {
            $branch = "minas";
        } else if ($nrorg == "2") {
            $branch = "nautico";
        }
        
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        $userId   = $row['USER_ID'];
        
        $bills    = [];
        
        if ($this->useIntegration($nrorg)) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
            $bills                = $consumeWebServiceMTC->getBillsFromUser($tokenExt, $branch);
        }
        
        $response->addDataSet(new DataSet('bills', $bills));
    }
    
    public function getBillDetails(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        $userId   = $row['USER_ID'];
        $billId   = $row['BILL_ID'];
        
        $bill     = [];
        
        if ($this->useIntegration($nrorg)) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
            $bill                 = $consumeWebServiceMTC->getBillDetails($tokenExt, $billId);
        }
        
        $response->addDataSet(new DataSet('bill', $bill));
    }
    
    /**
     * getUserDependents
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     */
    public function getWalletFromUserAndDependents(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $userId     = $row[GenUser::USER_ID];
        $nrorg      = $row[GenOrganization::NRORG];
        
        $dependentsData = $this->userService->getWalletFromUserAndDependents($userId, $nrorg);
        $userDataSet    = new DataSet('dependentsData', $dependentsData);
        $response->addDataSet($userDataSet);
    }
    
    public function createProfessional(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $adminId    = $row[GenUser::USER_ID];
        $serviceIds = $row['SERVICE_IDS'];
        $eventId    = $row['EVENT_ID'];
        $fullName   = $row['FULL_NAME'];
        $phoneNumber = isset($row['PHONE_NUMBER']) ? $row['PHONE_NUMBER'] : "";
        $initialTime = isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : "";
        $finalTIme  = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $image      = isset($row['IMAGE']) ? $this->imageUploader->upload($row['IMAGE'], $nrorg) : "";
        $status     = "A";
        $userTypeId = 3;
        
        $nomeArray = explode(" ", $fullName);
        $firstName = array_shift($nomeArray);
        $lastName  = "";
        if(count($nomeArray) > 1) {
             $lastName = implode(" ", $nomeArray);
        } else $lastName = '';
        
        $user = $this->userService->getGenUser($email, $cpf);
        
        if(!empty($user)) {
            $genUserTypeRel = $this->userService->getGenUserTypeWB($user->getId(), $userTypeId, $nrorg);
            if(empty($genUserTypeRel)){
                $this->userService->addProfileUser($user->getId(), $userTypeId, $nrorg, $status, $adminId);
                throw Exception::userExistsInTheDatabase();
            } else if($genUserTypeRel->getStatus() == 'I'){
                $this->userService->activateUserTypeRel($genUserTypeRel);
            }
            $dataSet    = new DataSet('response', ['usuario_existente' => true]);
        }else {
            $user = $this->userService->createProfessional($firstName, $lastName, $initialTime, $finalTIme, $cpf, $email, $password, $nrorg, $phoneNumber, $serviceIds, $adminId, $eventId, $image);
            $dataSet    = new DataSet('response', $user->toArray());
        }
        
        $response->addDataSet($dataSet);
    }
    
    public function updateProfessional(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $ImageUploader = $this->imageUploader;
        
        $adminId    = $row[GenUser::USER_ID];
        $serviceIds = $row['SERVICE_IDS'];
        $eventId    = $row['EVENT_ID'];
        $clientId   = $row['CLIENT_ID'];
        $fullName   = $row['FULL_NAME'];
        $firstName  = substr($fullName, 0, strpos($fullName, " "));
        $lastName   = substr($fullName, strpos($fullName, " "));
        $phoneNumber= isset($row['PHONE_NUMBER']) ? $row['PHONE_NUMBER'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $image      = isset($row['IMAGE']) && strlen($row['IMAGE']) > 500 ? $ImageUploader->upload($row['IMAGE'], $nrorg) : $row['IMAGE'];
        $status     = isset($row['STATUS']) ? $row['STATUS'] : "A";
        $initialTime= isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : "";
        $finalTime  = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : "";
        $userTypeId = 3;
        
        $firstName  = strtok($fullName, ' ');
        $lastName   = strstr($fullName, ' ');
        
        $user       = $this->userService->updateProfessional($clientId, $firstName, $lastName, $cpf, $email, $password, $nrorg, $phoneNumber, $serviceIds, $adminId, $eventId, $image, $status, $initialTime, $finalTime);

        $dataSet    = new DataSet('response', $user->toArray());
        
        $response->addDataSet($dataSet);
    }
    
    public function setSkipPreferences(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        $userId = $row[GenUser::USER_ID];
        
        $this->userService->setSkipPreferences($userId);
        
        $dataSet = new DataSet('response', ['status' => "Ok"]);
        $response->addDataSet($dataSet);
    }
    
    public function saveCustomizationSamples(DTO\Request $request, DTO\Response $response) {
        $rows = Util::getParamsAsArray($request);
        $rows[0]['_id'] = explode(" ", (string)microtime())[1];
        
        $response->addDataSet(new DataSet('response', ['saved' => $rows]));
    }
    
    public function enableOrDisableUser(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        
         $userId = $row[GenUser::USER_ID];
         $nrorg = $row["NRORG"];
         $modifieldBy = $row["MODIFIELD_BY"];
         
         $this->userService->enableOrDisableUser($userId, $nrorg, $modifieldBy);
         
         $response->addDataSet(new DataSet('response', ['status' => "Ok"]));
    }

    public function changePassword(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $adminId  = $row['USER_ID'];
        $userId   = $row['UPDATE_USER_ID'];
        $password = $row['PASSWORD'];
        
        $this->userService->changePassword($adminId, $userId, $password);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function getUsersAutoComplete(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $partialName    = $row['PARTIAL_NAME'];
        $nrorg          = $row['NRORG'];
        $userId         = $row['USER_ID'];
        
        $users          = $this->userService->getUsersAutoComplete($partialName, $nrorg, $userId);
        
        $response->addDataSet(new DataSet('users', $users));
    }
    
    public function createUserGuest(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $adminId        = $row['USER_ID'];
        $fullName       = $row['FULL_NAME'];
        $phoneNumber    = isset($row['PHONE_NUMBER']) ? $row['PHONE_NUMBER'] : NULL;
        $cpf            = isset($row['CPF']) ? $row['CPF'] : NULL;
        $email          = isset($row['EMAIL']) ? $row['EMAIL'] : NULL;
        $status         = "A";
        $lastName       = NULL;
        
        $nomeArray      = explode(" ", $fullName);
        $firstName      = array_shift($nomeArray);
        
        if(count($nomeArray) > 1) {
             $lastName = implode(" ", $nomeArray);
        }
        
        $user = $this->userService->createUserGuest($firstName, $lastName, $cpf, $email, $phoneNumber, $adminId, $status);
        
        $response->addDataSet(new DataSet('response', $user));
    }
    
    public function DeleteUser (DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $result = $this->userService->DeleteUser($row);
        $response->addDataSet(new DataSet('response', ['status' => $result]));
    }
    
    public function updateUserStonePOS(DTO\Request\Row $request, DTO\Response $response) {
        
        $row        = $request->getRow();
        
        $email        = $row['EMAIL'];
        $pos       = $row['POS'];
        
        $user = $this->userService->updateUserStonePOS($email, $pos);
        $response->addDataSet(new DataSet('response', ['user' => $user->toArray()]));
    }
}
