<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Helpers\General as General;
use Zeedhi\ApiGeneral\Service\User as UserService;
use Zeedhi\ApiGeneral\Service\Notification as NotificationService;
use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration as Integration;
use Zeedhi\ApiGeneral\Service\Exception as Exception;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;


class Notification {
    
    /**
     * __contruct
     *
     * @param RentSpaceService $rentSpaceService
     * @param NotificationService $notification
     */
    public function __construct(NotificationService $notification, Integration $integration) {
        $this->notification = $notification;
        $this->Integration = $integration;
    }
    
    /* Esse método retorna notificações dos ultimos 15 dias! */
    public function getNotificationsByUser(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = $row['NRORG'];
        $userId         = $row['USER_ID'];
        $currentDate    = new \DateTime();
        
        $currentDate->sub(new \DateInterval('P15D'));
        
        $lastFortnight = $currentDate->format('Y-m-d');
        
        $notifications = $this->notification->getNotificationsByUser($userId, $lastFortnight, $nrorg);
        
        $response->addDataSet(new DataSet('notifications', $notifications));
        $response->addDataSet(new DataSet('surveys', $notifications));
    }
    
    public function getReportsByUser(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $nrorg   = $row['NRORG'];
        $userId  = $row['USER_ID'];
        
        $reports = $this->notification->getReportsByUser($userId, $nrorg);
        
        $response->addDataSet(new DataSet('reports', $reports));
    }
    
    public function setNotificationAsRead(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $id      = $row['ID'];
        $userId  = $row['USER_ID'];
        $nrorg   = $row['NRORG'];
        $arrayIdNotifications   = isset($row['ARRAY_ID_NOTIFICATIONS']) ? $row['ARRAY_ID_NOTIFICATIONS'] : NULL;
        
        if($arrayIdNotifications) {
            foreach($arrayIdNotifications as $el) {
                if($el) {
                    $reports = $this->notification->setNotification($el, $userId, $nrorg);
                }
            }
        } else {
            $reports = $this->notification->setNotification($id, $userId, $nrorg);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function setReportAsConfirmed(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $idReport = $row['REPORT_ID'];
        $userId   = $row['USER_ID'];
        $nrorg    = $row['NRORG'];
        $token    = $row['TOKEN_EXT'];

        $report = $this->notification->setReport($idReport, $userId, $nrorg);

        if(!empty($report->getExternalId())) {
            $resp = $report->getStatus() === 'C' ? 'Sim' : 'Não';
    
            $integration = new Integration(new ConsumeWebServiceMTC());
    
            $integration->sendNotificationReply($token, $report->getExternalId(), $resp);
        }        
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
}
