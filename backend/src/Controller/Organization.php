<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\Organization as OrganizationService;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DTO;
use Zeedhi\ApiGeneral\Helpers\Util;

use Zeedhi\ApiGeneral\Helpers\ImageUploader;

class Organization {
    
    /**
     * __contruct
     *
     * @param OrganizationService $organizationService
     */
    public function __construct(OrganizationService $organizationService, ImageUploader $imageUploader) {
        $this->organizationService = $organizationService;
        $this->imageUploader = $imageUploader;
    }
    
    public function getOrganizationData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId     = $row['USER_ID'];
        $nrorg      = $row['NRORG'];
        $userTypeId = $row['USER_TYPE_ID'];

        $organizationData = $this->organizationService->getOrganizationData($userId, $nrorg, $userTypeId);
        
        $organization = isset($organizationData[0]) ? $organizationData[0]->toArray() : [];
        $configs      = isset($organizationData[1]) ? $organizationData[1]->toArray() : [];
        
        $organization['CONFIGURATIONS'] = $configs;
        
        $response->addDataSet(new DataSet('organization', $organization));
    }
    
    public function getOrganizationDataAdmin(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId     = $row['USER_ID'];
        $nrorg      = $row['NRORG'];
        $userTypeId = $row['USER_TYPE_ID'];

        $organizationData = $this->organizationService->getOrganizationDataAdmin($userId, $nrorg, $userTypeId);
        
        $organization = isset($organizationData[0]) ? $organizationData[0]->toArrayAdmin() : [];
        $configs      = isset($organizationData[1]) ? $organizationData[1]->toArrayAdmin() : [];
        
        $organization['CONFIGURATIONS'] = $configs;
        
        $response->addDataSet(new DataSet('organization', $organization));
    }
    
    public function getOrganizationsData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $cpf     = $row['CPF'];
        $userTypeId = $row['USER_TYPE_ID'];

        $organizations = $this->organizationService->getOrganizationsData($cpf, $userTypeId);
        
        $response->addDataSet(new DataSet('organizations', $organizations));
    }
    
    public function getAnalyticsKey(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $nrorg  = isset($row['NRORG']) ? $row['NRORG'] : 1;
        $analyticsKey = $this->organizationService->getAnalyticsKey($nrorg);
        $response->addDataSet(new DataSet('response', $analyticsKey));
    }
    
    public function updateOrganizationGateway(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $adminId    = $row['USER_ID'];
        $nrorg      = $row['NRORG'];
        $gateway    = $row['GATEWAY'];
        $merchant   = $row['MERCHANT_KEY'];
        $merchantId = $row['MERCHANT_ID'];

        $this->organizationService->updateOrganizationGateway($nrorg, $gateway, $merchant, $merchantId, $adminId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }    
    
    public function createOrUpdateConfigurationNrorg(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $adminId            = $row['USER_ID'];
        $nrorg              = $row['NRORG'];
        $logoImage          = isset($row['LOGO_IMAGE']) && strlen($row['LOGO_IMAGE']) > 500 ? $this->imageUploader->upload($row['LOGO_IMAGE'], $nrorg) :  $row['LOGO_IMAGE'];
        $logoImageSmall     = isset($row['LOGO_IMAGE_SMALL']) && strlen($row['LOGO_IMAGE_SMALL']) > 500 ? $this->imageUploader->upload($row['LOGO_IMAGE_SMALL'], $nrorg) :  $row['LOGO_IMAGE_SMALL'];
        $primaryColor       = isset($row['PRIMARY_COLOR']) ? $row['PRIMARY_COLOR'] : null;
        $secondaryColor     = isset($row['SECONDARY_COLOR']) ? $row['SECONDARY_COLOR'] : null;
        $yesColor           = isset($row['YES_COLOR']) ? $row['YES_COLOR'] : null;
        $noColor            = isset($row['NO_COLOR']) ? $row['NO_COLOR'] : null;
        $useIntegration     = isset($row['USE_INTEGRATION']) ? $row['USE_INTEGRATION'] : null;
        $preferenceFlow     = isset($row['PREFERENCE_FLOW']) ? $row['PREFERENCE_FLOW'] : null;
        $aws_s3_access_key  = isset($row['AWS_S3_ACCESS_KEY']) ? $row['AWS_S3_ACCESS_KEY'] : null;
        $aws_s3_bucket      = isset($row['AWS_S3_BUCKET']) ? $row['AWS_S3_BUCKET'] : null;
        $aws_s3_secret_key  = isset($row['AWS_S3_SECRET_KEY']) ? $row['AWS_S3_SECRET_KEY'] : null;
        $fcm_server_key     = isset($row['FCM_SERVER_KEY']) ? $row['FCM_SERVER_KEY'] : null;
        $host               = isset($row['HOST']) ? $row['HOST'] : null;
        $host_from          = isset($row['HOST_FROM']) ? $row['HOST_FROM'] : null;
        $host_from_name     = isset($row['HOST_FROM_NAME']) ? $row['HOST_FROM_NAME'] : null;
        $host_password      = isset($row['HOST_PASSWORD']) ? $row['HOST_PASSWORD'] : null;
        $googleAnalyticsKey = isset($row['GOOGLE_ANALYTICS_KEY']) ? $row['GOOGLE_ANALYTICS_KEY'] : null;
        $appLayoutConfig    = isset($row['APP_LAYOUT_CONFIG']) ? $row['APP_LAYOUT_CONFIG'] : null;
        $allowExternalUsers = isset($row['ALLOW_EXTERNAL_USERS']) ? $row['ALLOW_EXTERNAL_USERS'] : null;
        $keyGoogle          = isset($row['KEY_GOOGLE']) ? $row['KEY_GOOGLE'] : null;
        $originApiExternal  = isset($row['ORIGIN_API_EXTERNAL']) ? $row['ORIGIN_API_EXTERNAL'] : null;  
        $tokenApiExternal   = isset($row['TOKEN_API_EXTERNAL']) ? $row['TOKEN_API_EXTERNAL'] : null;  
        $urlApiExternal     = isset($row['URL_API_EXTERNAL']) ? $row['URL_API_EXTERNAL'] : null;  
        $overlayImage       = isset($row['OVERLAY_IMAGE']) && strlen($row['OVERLAY_IMAGE']) > 500 ? $this->imageUploader->upload($row['OVERLAY_IMAGE'], $nrorg) :  $row['OVERLAY_IMAGE'];
 
        $configuration = $this->organizationService->getConfigurationByNrorg($nrorg);
        
        if(empty($configuration)){
            $result = $this->organizationService->createConfigurationNrorg($logoImage, $logoImageSmall, $primaryColor, $secondaryColor, $yesColor, $noColor, $useIntegration, $preferenceFlow, $nrorg, $aws_s3_access_key, $aws_s3_bucket, $aws_s3_secret_key, $fcm_server_key, $host, $host_from, $host_from_name, $host_password, $googleAnalyticsKey, $appLayoutConfig, $allowExternalUsers, $keyGoogle, $urlApiExternal, $tokenApiExternal, $originApiExternal, $overlayImage);
        }else {
            $result = $this->organizationService->updateConfigurationNrorg($configuration, $logoImage, $logoImageSmall, $primaryColor, $secondaryColor, $yesColor, $noColor, $useIntegration, $preferenceFlow, $aws_s3_access_key, $aws_s3_bucket, $aws_s3_secret_key, $fcm_server_key, $host, $host_from, $host_from_name, $host_password, $googleAnalyticsKey, $appLayoutConfig, $allowExternalUsers, $keyGoogle, $urlApiExternal, $tokenApiExternal, $originApiExternal, $overlayImage);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok', 'configurationId' => $result->getId()]));
    }
    
    public function getUseIntegration(DTO\Request\Row $request, DTO\Response $response) {
        $useIntegration = $this->organizationService->getUseIntegration();
        $response->addDataSet(new DataSet('response', $useIntegration));
    }
    
    public function getOrganizationDataByURL(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $url     = $row['URL'];
        
        $organizationData = $this->organizationService->getOrganizationDataByURL($url);
        
        $organization = isset($organizationData[0]) ? $organizationData[0]->toArray() : [];
        $configs      = isset($organizationData[1]) ? $organizationData[1]->toArray() : [];
        
        $organization['CONFIGURATIONS'] = $configs;
        
        $response->addDataSet(new DataSet('organization', $organization));
    }
    
    public function updateOrganizationStoneID(DTO\Request\Row $request, DTO\Response $response){
        $row = $request->getRow();
        
        $nrorg     = $row['NRORG'];
        $stoneId   = $row['STONE_ESTABLISHMENT_ID'];
         
        $organization = $this->organizationService->updateOrganizationStoneID($nrorg, $stoneId);
        $response->addDataSet(new DataSet('organization', $organization->toArray()));
    }
    
    /*TAA's*/
    public function getAvailableTaa(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        $nrorg = $row['NRORG'];
        $structureId = $row['STRUCTURE_ID'];
        $status = $row['STATUS'];
        
        $taas   = $this->organizationService->getAvailableTaa($nrorg, $structureId, $status);
        $response->addDataSet(new DataSet('taas', $taas));
    }
    
    public function createTaaConfiguration(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : null;
        $name = isset($row['NAME']) ? $row['NAME'] : null;
        $modality = isset($row['MODALITY']) ? $row['MODALITY'] : null;
        $posSerialNumber = isset($row['POS_SERIAL_NUMBER']) ? $row['POS_SERIAL_NUMBER'] : null;
        $posReferenceId = isset($row['POS_REFERENCE_ID']) ? $row['POS_REFERENCE_ID'] : null;
        $externalApiUrl = isset($row['EXTERNAL_API_URL']) ? $row['EXTERNAL_API_URL'] : null;
        $externalApiId = isset($row['EXTERNAL_API_ID']) ? $row['EXTERNAL_API_ID'] : null;
        $structureId = isset($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        $storeId = isset($row['STORE_ID']) ? $row['STORE_ID'] : null;
        $createdBy = isset($row['USER_ID']) ? $row['USER_ID'] : null;
        $status = isset($row['STATUS']) ? $row['STATUS'] : null;
        
        $taa = $this->organizationService->createTaaConfiguration($nrorg, $name, $modality, $posSerialNumber, $posReferenceId, $externalApiUrl, $externalApiId, $structureId, $storeId, $status, $createdBy);
        $response->addDataSet(new DataSet('taa', $taa));
    }
    
    public function getTaaConfiguration(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $nrorg = $row['NRORG'];
        $structureId = isset($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        $status = isset($row['status']) ? $row['status'] : null; // necessario que o parametro seja em minúsculo. (CRUD genTAA)
        $taas = $this->organizationService->getTaaConfiguration($nrorg, $structureId, $status);
        $response->addDataSet(new DataSet('taas', $taas));
    }
    
    public function updateTaaConfiguration(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $taaId = $row['ID'];
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : null;
        $name = isset($row['NAME']) ? $row['NAME'] : null;
        $modality = isset($row['MODALITY']) ? $row['MODALITY'] : null;
        $posSerialNumber = isset($row['POS_SERIAL_NUMBER']) ? $row['POS_SERIAL_NUMBER'] : null;
        $posReferenceId = isset($row['POS_REFERENCE_ID']) ? $row['POS_REFERENCE_ID'] : null;
        $externalApiUrl = isset($row['EXTERNAL_API_URL']) ? $row['EXTERNAL_API_URL'] : null;
        $externalApiId = isset($row['EXTERNAL_API_ID']) ? $row['EXTERNAL_API_ID'] : null;
        $structureId = isset($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        $storeId = isset($row['STORE_ID']) ? $row['STORE_ID'] : null;
        $modifiedBy = isset($row['USER_ID']) ? $row['USER_ID'] : null;
        $status = isset($row['STATUS']) ? $row['STATUS'] : null;
        
        $taa = $this->organizationService->updateTaaConfiguration($taaId, $nrorg, $name, $modality, $posSerialNumber, $posReferenceId, $externalApiUrl, $externalApiId, $structureId, $storeId, $status, $modifiedBy);
        $response->addDataSet(new DataSet('taa', $taa));
    }
    
    public function getStoresToTaa(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $nrorg = $row['NRORG'];
        $status = isset($row['STATUS']) ? $row['STATUS'] : null;
        $storeName = isset($row['STORE_NAME']) ?  $row['STORE_NAME'] : null;
        
        $stores = $this->organizationService->getStoresToTaa($nrorg, $status, $storeName);
        $response->addDataSet(new DataSet('stores', $stores));
    }
    
    public function updateReferenceIdOrSerialNumber(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $taaId  = $row['TAA_ID'];
        $posSerialNumber    = isset($row['POS_SERIAL_NUMBER']) ? $row['POS_SERIAL_NUMBER'] : null;
        $posReferenceId     = isset($row['POS_REFERENCE_ID']) ? $row['POS_REFERENCE_ID'] : null;
        
        $taa = $this->organizationService->updateReferenceIdOrSerialNumber($taaId, $posSerialNumber, $posReferenceId);

        $response->addDataSet(new DataSet('taa', $taa));
    }
}