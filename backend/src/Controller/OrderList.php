<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;

class OrderList extends Crud {
    
    protected $dataSourceName = 'order_list';

}