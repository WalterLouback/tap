<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Helpers\General as General;

use Zeedhi\ApiGeneral\Service\User as UserService;
use Zeedhi\ApiGeneral\Service\RentSpace as RentSpaceService;
use Zeedhi\ApiGeneral\Service\Notification as NotificationService;
use Zeedhi\ApiGeneral\Service\Exception as Exception;
use Zeedhi\ApiGeneral\Helpers\ImageUploader;
use Zeedhi\ApiGeneral\Service\EmailPhpMailer as emailPhpMailerrService;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;


class RentSpace {
    
    /** @var RentSpaceService */
    protected $rentSpaceService;
    
    /** @var general */
    protected $general;
    
    /**
     * __contruct
     *
     * @param RentSpaceService $rentSpaceService
     * @param NotificationService $notification
     */
    public function __construct(RentSpaceService $rentSpaceService, NotificationService $notification, ImageUploader $imageUploader) {
        $this->rentSpaceService = $rentSpaceService;
        $this->notification = $notification;
        $this->imageUploader = $imageUploader;
    }
    
    public function getRentSpacesFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = $row['NRORG'];
        
        $structureId    = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        
        $rentSpaces = $this->rentSpaceService->getRentSpacesFromOrganization($structureId, $nrorg);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('rentSpacesAll', $rentSpaces));
    }
    
    public function getRentSpacesFromStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = $row['NRORG'];
        
        $structureId    = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        
        $rentSpaces = $this->rentSpaceService->getRentSpacesFromStructure($structureId, $nrorg);
        
        $rentSpacesAll = [];
        foreach ($rentSpaces as $key => $value) {
            $rentSpacesAll[$key] = $value->toArrayAll();
        }
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('rentSpacesAll', $rentSpacesAll));
    }
    
    public function getRentSpacesById(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $nrorg          = $row['NRORG'];
        $rentSpacesId   = $row['RENT_SPACE_ID'];
        
        $genRentSpaces = $this->rentSpaceService->getRentSpacesById(['id' => $rentSpacesId]);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('rentSpaces', $genRentSpaces->toArrayAll()));
    }
    
    public function addRentSpace(DTO\Request\Row $request, DTO\Response $response) {
        $row                    = $request->getRow();
        
        $userId                 = isset($row['USER_ID']) && !empty($row['USER_ID']) ? $row['USER_ID'] : null;
        $nrorg                  = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG'] : null;
        $name                   = isset($row['NAME']) && !empty($row['NAME']) ? $row['NAME'] : null;
        $description            = isset($row['DESCRIPTION']) && !empty($row['DESCRIPTION']) ? $row['DESCRIPTION'] : null;
        $maximumPersonCapacity  = isset($row['MAXIMUMPERSONCAPACITY']) && !empty($row['MAXIMUMPERSONCAPACITY']) ? $row['MAXIMUMPERSONCAPACITY'] : null;
        $phone                  = isset($row['PHONE']) && !empty($row['PHONE']) ? $row['PHONE'] : null;
        $address                = isset($row['ADDRESS']) && !empty($row['ADDRESS']) ? $row['ADDRESS'] : null;
        $linkTourVirtual        = isset($row['LINKTOURVIRTUAL']) && !empty($row['LINKTOURVIRTUAL']) ? $row['LINKTOURVIRTUAL'] : null;
        $status                 = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : "A";
        $structureId            = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        // $photos                 = isset($row['PHOTOS']) && !empty($row['PHOTOS']) ? $row['PHOTOS'] : null;
        // $urlPicture             = null;
        $urlPicture             = isset($row['PHOTOS']) && !empty($row['PHOTOS']) ? $row['PHOTOS'] : null;
        
        /* updload da image e retorna array de links */
        // if($photos) {
        //     foreach ($photos as $key => $photo) {
        //         $urlPicture[$key] = $this->imageUploader($photo['PHOTO']);
        //     }
        // }
        
        /* retorna o local na unidade */
        if($structureId)
            $structure = $this->rentSpaceService->getGenStructure(['id' => $structureId, 'type' => "L"]);
        
        try {
            $rentSpaces = $this->rentSpaceService->addRentSpace($userId, $nrorg, $name, $description, $maximumPersonCapacity, $linkTourVirtual, $status, $structure, $urlPicture, $phone, $address);
            if($urlPicture) {
                if(!empty($urlPicture[0]['PHOTO1']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO1']);
                if(!empty($urlPicture[0]['PHOTO2']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO2']);
                if(!empty($urlPicture[0]['PHOTO3']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO3']);
                if(!empty($urlPicture[0]['PHOTO4']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO4']);
                if(!empty($urlPicture[0]['PHOTO5']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO5']);
            }
            
            $this->rentSpaceService->commit();
            
            $result = 'ok';
        } catch (Exception $e) {
            $result = $e->getMessage();
        }
        $response->addDataSet(new DataSet('response', ['status' => $result]));
    }
    
    public function updateRentSpace(DTO\Request\Row $request, DTO\Response $response) {
        $row                    = $request->getRow();
        
        $userId                 = isset($row['USER_ID']) && !empty($row['USER_ID']) ? $row['USER_ID'] : null;
        $nrorg                  = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG'] : null;
        $name                   = isset($row['NAME']) && !empty($row['NAME']) ? $row['NAME'] : null;
        $description            = isset($row['DESCRIPTION']) && !empty($row['DESCRIPTION']) ? $row['DESCRIPTION'] : null;
        $maximumPersonCapacity  = isset($row['MAXIMUMPERSONCAPACITY']) && !empty($row['MAXIMUMPERSONCAPACITY']) ? $row['MAXIMUMPERSONCAPACITY'] : null;
        $phone                  = isset($row['PHONE']) && !empty($row['PHONE']) ? $row['PHONE'] : null;
        $address                = isset($row['ADDRESS']) && !empty($row['ADDRESS']) ? $row['ADDRESS'] : null;
        $linkTourVirtual        = isset($row['LINKTOURVIRTUAL']) && !empty($row['LINKTOURVIRTUAL']) ? $row['LINKTOURVIRTUAL'] : null;
        $structureId            = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : null;
        $rentSpaceId            = isset($row['RENT_SPACE_ID']) && !empty($row['RENT_SPACE_ID']) ? $row['RENT_SPACE_ID'] : null;
        $status                 = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : null;
        // $photos                 = isset($row['PHOTOS']) && !empty($row['PHOTOS']) ? $row['PHOTOS'] : null;
        // $urlPicture             = null;
        $urlPicture             = isset($row['PHOTOS']) && !empty($row['PHOTOS']) ? $row['PHOTOS'] : null;
        
        /* updload da image e retorna array de links */
        // if($photos) {
        //     foreach ($photos as $key => $photo) {
        //         $urlPicture[$key] = $this->imageUploader($photo['PHOTO']);
        //     }
        // }
        
        // apagando fotos antigas
        $this->rentSpaceService->deletePhotos($rentSpaceId);
        
        /* retorna o local na unidade */
        if($structureId)
            $structure = $this->rentSpaceService->getGenStructure(['id' => $structureId, 'type' => "L"]);
        
        try {
            $rentSpaces = $this->rentSpaceService->updateRentSpace($rentSpaceId, $userId, $nrorg, $name, $description, $maximumPersonCapacity, $linkTourVirtual, $status, $structure, $urlPicture, $phone, $address);
            
            if($urlPicture) {
                if(!empty($urlPicture[0]['PHOTO1']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO1']);
                if(!empty($urlPicture[0]['PHOTO2']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO2']);
                if(!empty($urlPicture[0]['PHOTO3']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO3']);
                if(!empty($urlPicture[0]['PHOTO4']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO4']);
                if(!empty($urlPicture[0]['PHOTO5']))
                    $this->rentSpaceService->addPhotos($rentSpaces, $userId, $urlPicture[0]['PHOTO5']);
            }
                
            $this->rentSpaceService->commit();
            
            $result = 'ok';
        } catch (Exception $e) {
            $result = $e->getMessage();
        }
        $response->addDataSet(new DataSet('response', ['status' => $result]));
    }
    
    
    
    public function imageUploader($base64, $nrorg) {
        $ImageUploader = $this->imageUploader;
        $link = $ImageUploader->upload($base64, $nrorg);
        
        return $link;
    }
    
    public function requestRentSpace(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $rentSpacesId   = $row['RENT_SPACE_ID'];
        $userId         = $row['USER_ID'];
        $nrorg          = $row['NRORG'];
        
        /* loacal a que o anúcio se refere! */
        $local = $this->rentSpaceService->getRentSpacesById(['id' => $rentSpacesId]);
        
        $user = $this->rentSpaceService->getUserData($userId, 1, $nrorg)->toArray();
        
        $organization = $this->rentSpaceService->getOrganization($nrorg);
        
        $this->sendEmailResponse($local, $user, $organization);
        
        $response->addDataSet(new DataSet('response', ['response' => 'ok']));
    }
    
    public function sendEmailResponse($local, $user, $organization) {
        $sendEmail = new emailPhpMailerrService();
        
        // headers
        $recipient          = "Aluguel de espaços";
        if (!empty($local->getGenStructure()->getEmail())) {
            $addressRecipient   = $local->getGenStructure()->getEmail();
        }else {
            $addressRecipient   = $local->getGenUser()->getEmail();
        }
        
        // data
        $subject    = htmlspecialchars("Aluguel de locais");
        
        $body       = 'Associado(a) ' . $user['FIRST_NAME'] . ' ' . $user['LAST_NAME'] . ' tem interesse em alugar ' . $local->getName() . ' na unidade: ' . $local->getGenStructure()->getParent()->getName() .' e ele gostaria de saber mais informaçoes sobre a locação do espaço. </br> Celular para contato '. $user['CONTACTS'][0]['PHONE'] . ' e e-mail ' . $user['EMAIL'];
        
        $message = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		</head>
        <div style="text-align: left;">
			<h2>Ol&aacute;</h2> 
			<h3><small style="color:#2d2d2d;">Voce tem uma nova mensagem:</small></h3>
			<h4>
			  '. $body .'
			 </h4>
		</div>
		</html>
		';

        $response = $sendEmail->sendEmail($recipient, $addressRecipient, $subject, $message, $organization);
    }
}
