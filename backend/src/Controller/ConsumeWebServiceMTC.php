<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Controller\Exception;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;

class ConsumeWebServiceMTC {

    const WB_INDISPONIVEL = array('res' => array('wb'=>'I', 'status'=>'erro', 'cadastrado'=>'sim', 'des_erro'=>'Serviço indisponível no momento. Todos os dias a partir das 23:20 até às 02:00 o serviço fica indisponível para realização de backup.'));
    const WB_INDISPONIVEL_OTHER = array('res' => array('wb'=>'I', 'status'=>'erro', 'cadastrado'=>'sim', 'des_erro'=>'Serviço indisponível no momento. Por favor tente outra vez em 5 minutos.'));
    
    /* construtor da url para consumir WebService */
    public function consumesWebService($url, $parameters) {
        $data = sprintf("%s?%s", $url, http_build_query($parameters));
        
        // inicia seção CURL 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode($result, true);
        
        if($objResult == NULL)
            $objResult = $this->checkRequestTimeout();
        
        return $objResult;
    }
    
    /**
     * Faz a validação dos dados de login do associado e retorna o token em caso de
     * sucesso para ser utilizado em outras requisições do webservice. Caso o associado tenha
     * tentado efetuar o login de outro aparelho (IMEI diferente), o webservice enviará um SMS
     * com um token de ativação do aparelho, neste caso o status será “erro” e o campo  
     * “des_erro”=”validar_token|(**) *****-1234”. A mensagem de erro retornará no início a palavra
     * “validar_token” e separado por pipe o celular que recebeu o SMS com o token de ativação.
     */
    public function webServiceLogin($filial, $email, $cpf, $npf, $password) {
        $parametersExternal['f'] = $filial != NULL ? $filial : "";
        $parametersExternal['e'] = $email != NULL ? $email : "";
        $parametersExternal['c'] = $cpf != NULL ? $cpf : "";
        $parametersExternal['n'] = $npf != NULL ? $npf : "";
        $parametersExternal['s'] = $password;
        $parametersExternal['imei'] = "123456001234560";
        
        $url = "https://socio.minastc.com.br/ws/login.php";
        
        /* Consumindo a WebService e retornando */
        return $this->consumesWebService($url, $parametersExternal);
    }
    
    /* Retorna todos os dados do associado. Em caso de titular, retorna a lista de dependentes */
    public function webServiceDadosAssociado ($filial, $tokenExt) {
        $parametersExternal['f'] = $filial;
        $parametersExternal['t'] = $tokenExt;
        $url = "https://socio.minastc.com.br/ws/dados_associado.php";

        /* Consumindo a WebService e retornando */
        return $this->consumesWebService($url, $parametersExternal);
    }
    
    /* Faz a validação do token do usuário */
    public function webServiceValidarToken($tokenExt, $filial) {
        $parametersExternal['f'] = $filial;
        $parametersExternal['t'] = $tokenExt;
        $url = "https://socio.minastc.com.br/ws/validar_token.php";

        /* Consumindo a WebService e retornando */
        return $this->consumesWebService($url, $parametersExternal);
    }
    
    /* Área de logout */
    public function logoutWebService($tokenExt, $filial) {
        $parametersExternal['f'] = $filial;
        $parametersExternal['t'] = $tokenExt;
        $url = "https://socio.minastc.com.br/ws/logout.php";

        /* Consumindo a WebService e retornando */
        return $this->consumesWebService($url, $parametersExternal);
    }
    
    /* Faz o cadastro do celular e senha do associado */
    public function registerUserWebService($branch = null, $cpf = null, $npf = null, $date_birth = null, $email, $password, $password2, $imei = null) {
        
        $parametersExternal['f']    = isset($branch)     ? $branch : "";
        $parametersExternal['c']    = isset($cpf)        ? $cpf : "";
        $parametersExternal['n']    = isset($npf)        ? $npf : "";
        $parametersExternal['d']    = isset($date_birth) ? $date_birth : "";
        $parametersExternal['e']    = isset($email)      ? $email : "";
        $parametersExternal['s1']   = isset($password)   ? $password : "";
        $parametersExternal['s2']   = isset($password2)  ? $password2 : "";
        $parametersExternal['imei'] = "123456001234560"/*isset($imei)       ? $imei : ""*/;
        
        $url = "https://socio.minastc.com.br/ws/cadastro.php";
        
        $returnWS = $this->consumesWebService($url, $parametersExternal);
        
        return $returnWS;
    }
    
    
    public function getTOTP($chave) {
        $parametersExternal['c'] = $chave;
        $url = "https://socio.minastc.com.br/ws/gerar_totp.php";

        /* Consumindo a WebService e retornando */
        return $this->consumesWebService($url, $parametersExternal);
    }
    
    public function checkIfUserIsRegistered($branch, $npf, $cpf) {
        $parametersExternal['f']    = $branch;
        $parametersExternal['c']    = $cpf;
        $parametersExternal['n']    = $npf;
        
        $url = "https://socio.minastc.com.br/ws/verificar_cadastro.php";
        
        $returnWS = $this->consumesWebService($url, $parametersExternal);
        
        return $returnWS;
    }
    
    public function consumesWebServiceGET($url) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        $objResult = json_decode($result, true);
        
        if($objResult == NULL)
            $objResult = $this->checkRequestTimeout();
        
        return $objResult;
    }
    
    public function consumesWebServicePOST($url, $data) {
        $data = sprintf("%s?%s", $url, http_build_query($data));
        // inicia seção CURL 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode(substr($result, 3, strlen($result) - 1), true);

        if($objResult == NULL)
            $objResult = $this->checkRequestTimeout();
        
        return $objResult;
    }
    
    public function getNewsWebService() {
        
        $url        = "https://socio.minastc.com.br/ws/noticias.php";
        $result     = $this->consumesWebServiceGET($url);
        
        return $result;
    }
    
    public function changeData($branch, $tokenExt, $email, $phoneNumber) {
        $parametersExternal['f']       = $branch;
        $parametersExternal['t']       = $tokenExt;
        $parametersExternal['email']   = $email;
        $parametersExternal['celular'] = $phoneNumber;
        
        $url = "https://socio.minastc.com.br/ws/alterar_dados.php";
        
        $returnWS = $this->consumesWebServicePOST($url, $parametersExternal);
        
        return $returnWS;
    }
    
    /* Changes the billing address or correspondence of the logged in member. */
    public function changeAddress($branch, $tokenExt, $type, $cep, $provincy, $city, $neighborhood, $street, $number, $complement) {
        $parametersExternal['f']           = $branch;
        $parametersExternal['t']           = $tokenExt;
        $parametersExternal['tipo']        = $type; // 1 for billing and 2 for mailing
        $parametersExternal['cep']         = $cep; // format: 00000-000
        $parametersExternal['uf']          = $provincy; // uf with 2 digits: MG
        $parametersExternal['cidade']      = $city;
        $parametersExternal['bairro']      = $neighborhood;
        $parametersExternal['logradouro']  = $street;
        $parametersExternal['numero']      = $number;
        $parametersExternal['complemento'] = $complement; // complement if any. (This is the only optional field).

        $url = "https://socio.minastc.com.br/ws/alterar_endereco.php";
        
        $returnWS = $this->consumesWebServicePOST($url, $parametersExternal);
        
        return $returnWS;
    }
    
    public function getBillsFromUser($tokenExt, $branch="minas") {
        $params['t']    = $tokenExt;
        $params['f']    = $branch;
        
        $url            = 'https://socio.minastc.com.br/ws/faturas.php';
        
        $response       = $this->consumesWebService($url, $params);
        
        if (!$response || !isset($response['res']) || !isset($response['res']['faturas']))
            return $response['res'];
        
        $bills          = $response['res']['faturas'];
        
        $formattedBills = [];
        
        foreach ($bills as $index=>$bill) {
            array_push($formattedBills, [
                'finalDate' => $this->formatDate($bills[$index]['data_vencimento']),
                'value' => (float) str_replace(',', '.', str_replace('R$ ', '', $bills[$index]['valor'])),
                'formattedBarCode' => $bills[$index]['linha_formatada'],
                'barCode' => $bills[$index]['linha_digitavel'],
                'paid' => $bills[$index]['pago'] == 'sim',
                'pdfUrl' => $bills[$index]['url_boleto'] != "Indisponível" && $bills[$index]['url_boleto'] != "boleto pago"? $bills[$index]['url_boleto'] : NULL,
                'errorMessage' => isset($bills[$index]['mensagem_banco']) ? $bills[$index]['mensagem_banco'] : NULL,
                'billId' => $bills[$index]['numero']
            ]);
        }
        
        return $formattedBills;
    }
    
    public function getBillDetails($tokenExt, $billId, $branch="minas") {
        $params['t']      = $tokenExt;
        $params['f']      = $branch;
        $params['fatura'] = $billId;
        
        $url              = 'https://socio.minastc.com.br/ws/extrato.php';
        
        $response         = $this->consumesWebService($url, $params);
        
        if (!$response || !isset($response['res']) || !isset($response['res']['itens']))
            return $response['res'];
        
        $items            = $response['res']['itens'];
        
        $formattedItems   = [];

        $statusOk         = $response['res']['status'] == 'ok';
        $date             = $this->formatDate($response['res']['data_vencimento']);
        $value            = $response['res']['valor'];
        
        foreach ($items as $index=>$bill) {
            if (isset($items[$index]['associado'])) {
                array_push($formattedItems, [
                    'userName' => $items[$index]['associado'],
                    'value' => (float) str_replace(',', '.', str_replace('R$ ', '', $items[$index]['valor'])),
                    'description' => isset($items[$index]['descricao']) ? $items[$index]['descricao'] : NULL
                ]);
            } else {
                array_push($formattedItems, [
                    'userName' => $items[0][$index]['associado'],
                    'value' => (float) str_replace(',', '.', str_replace('R$ ', '', $items[0][$index]['valor'])),
                    'description' => isset($items[0][$index]['descricao']) ? $items[0][$index]['descricao'] : NULL
                ]);
            }
        }
        
        $retorno = [
            'sucess' => $statusOk,
            'date'   => $date,
            'value'  => $value,
            'items'  => $formattedItems
        ];
        
        return $retorno;
    }
    
    public function formatDate($date) {
        $arr  = explode('/', $date);
        return $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    }
    
    function checkRequestTimeout() {
        date_default_timezone_set('America/Sao_Paulo');
        $currentTime = date('H:i:s');
        
        if($currentTime >= "23:00:00" && $currentTime <= "02:00:00") {
            return self::WB_INDISPONIVEL;
        }else {
            return self::WB_INDISPONIVEL_OTHER;
        }
    }
}