<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\EditableFields as EditableFieldsService;
use Zeedhi\ApiGeneral\Service\Exception as Exception;

use Zeedhi\ApiGeneral\Model\Entities\GenEditableFields;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

class EditableFields {
    const NPF                    = 'NPF';
    const CPF                    = 'CPF';
    const LAST_NAME              = 'LAST_NAME';
    const FIRST_NAME             = 'FIRST_NAME';
    const BILLING_ADDRESS        = 'BILLING_ADDRESS';
    const CORRESPONDENCE_ADDRESS = 'CORRESPONDENCE_ADDRESS';
    /** @var StructureService */
    protected $editableFieldsService;

    /**
     * __contruct
     *
     * @param StructureService $StructureService
     */
    public function __construct(EditableFieldsService $editableFieldsService) {
        $this->editableFieldsService = $editableFieldsService;
    }
    
    /**
     * getEditableFields
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function getEditableFields(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $nrorg          = $row['NRORG'];
        $editableFields = $this->editableFieldsService->getEditableFields($nrorg);
        $response->addDataSet(new DataSet('response', ['editableFields' => GenEditableFields::manyToArray($editableFields)]));
    }
    
    public function getFields(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $nrorg          = $row['NRORG'];
        $editableFields = [['NAME' => self::CPF, 'LABEL' => 'CPF'], ['NAME' => self::NPF, 'LABEL' => 'NPF'], ['NAME' => self::FIRST_NAME, 'LABEL' => 'Primeiro Nome'], ['NAME' => self::LAST_NAME, 'LABEL' => 'Último Nome'], ['NAME' => self::BILLING_ADDRESS, 'LABEL' => 'Endereço de Cobrança'], ['NAME' => self::CORRESPONDENCE_ADDRESS, 'LABEL' => 'Endereço de Correspondência']];
        
        $response->addDataSet(new DataSet('response', ['editableFields' => $editableFields]));
    }
    
    public function getPermissions(DTO\Request\Row $request, DTO\Response $response) {
        $permissions = $this->editableFieldsService->getPermissions();
        
        $response->addDataSet(new DataSet('response', ['permissions' => $permissions]));
    }
    
    public function getUserTypes(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userTypes = $this->editableFieldsService->getUserTypes($row['NRORG']);
        
        $response->addDataSet(new DataSet('response', ['userTypes' => $userTypes]));
    }
    
    public function createEditableField(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $fieldName  = $row['FIELD_NAME'];
        $permission = $row['PERMISSION'];
        $userTypes  = $row['USER_TYPE'];
        $userId     = $row['USER_ID'];
        $nrorg      = $row['NRORG'];
        
        if(!isset($fieldName) || !isset($permission) || !isset($userTypes))
            throw new \Exception('Insufficient parameters!', 0);
       
        foreach ($userTypes as $key=>$value) {
            $responseCreateEditableFields[$key] = $this->editableFieldsService->createEditableField($fieldName, $permission, $value, $userId, $nrorg);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function updateEditableField(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $fieldName  = $row['FIELD_NAME'];
        $permission = $row['PERMISSION'];
        $userType   = $row['USER_TYPE'];
        $userId     = $row['USER_ID'];
        $nrorg      = $row['NRORG'];
        $id         = $row['ID'];
        $editableField = $this->editableFieldsService->getEditableFielById($id, $nrorg);
        
        $responseUpdateEditableField = $this->editableFieldsService->updateEditableField($editableField, $fieldName, $permission, $userType, $userId, $nrorg);

        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function removeEditableField(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $this->editableFieldsService->removeEditableField($row['ID']);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
}