<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\User as UserService;
use Zeedhi\ApiGeneral\Service\Notification as NotificationService;
use Zeedhi\ApiGeneral\Service\Organization as OrganizationService;
use Zeedhi\ApiGeneral\Service\Exception;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;

class WebService {
    
    public function __construct(UserService $userService, NotificationService $notification, OrganizationService $organizationService) {
        $this->userService   = $userService;
        $this->notification  = $notification;
        $this->organizationService = $organizationService;
    }
    
    public function sendMessageUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg              = (!empty($row['NRORG']))           ? $row['NRORG']             : null;
        $externalIdNoti     = (!empty($row['NOTIFICATION_ID'])) ? $row['NOTIFICATION_ID']   : null;
        $externalIds        = (!empty($row['NPFs']))            ? $row['NPFs']              : null;
        $message            = (!empty($row['MESSAGE']))         ? $row['MESSAGE']           : null;
        $confirmation       = (!empty($row['ACTION']))          ? $row['ACTION']            : null;
        $inputTitle         = (!empty($row['TITLE']))           ? $row['TITLE']             : null;
        $category           = (!empty($row['CATEGORY']))        ? $row['CATEGORY']          : null;  //0 - text; 1 - evento;
        $urlImage           = (!empty($row['URL_IMAGE']))       ? $row['URL_IMAGE']         : null;
        
        $dateOfCommitment   = (!empty($row['DATE_OF_COMMITMENT'])) ? $this->stringToDate($row['DATE_OF_COMMITMENT']) : null;
        $userData           = array();
        
        if($nrorg == null && $externalIds == null && $message == null) 
            throw Exception::invalidParameters();
        
        /*busca nrorg*/
        $nrorg      = $this->getOrganization($nrorg);
        
        $userAutor  = null;
        /*busca ids e firebasetokens do NPF informados e add num array*/
        foreach($externalIds as $externalId){
            $user       = $this->getUserByExternalId($nrorg, $externalId);
            
            if(!empty($user)) {
                array_push($userData,   array(
                                            "userId" => $user[0]["userId"],
                                            "userFirebaseToken" => $user[0]["firebaseToken"]
                                        ));
            }
        }
        $dataSet = array();
        
        if(!empty($userData)) {
            /*inseri as notificacoes para os usuarios*/
            foreach($userData as $user) {
                $this->insertNotification($message, $nrorg, $user['userId'], $user['userFirebaseToken'], $confirmation, $inputTitle, $dateOfCommitment, $category, $urlImage, $userAutor, $externalIdNoti);
            }
            
            $dataSet = array('success' => true, 'message' => "notificações envidas.");
        } else {
            $dataSet = array('success' => false, 'message' => "notificações não foram envidas. porque o usuário não foi encotrado.");
        }
        
        // $response->addDataSet(new DataSet('response', $dataSet));
    }
    
    public function getUserByExternalId($nrorg, $externalId) {
        
        $user       = $this->userService->getUserFromOrganizationAndExternalId($nrorg, $externalId);
        
        return $user;
    }
    
    public function getOrganization($nrorg) {
        $organization  = $this->organizationService->getNrorg($nrorg);
        
        return $organization->getNrorg();
    }
    
    public function insertNotification($message, $nrorg, $userId, $userFirebaseToken, $confirmation, $inputTitle, $dateOfCommitment, $category, $urlImage, $userAutor, $externalIdNoti=null) {
        $notificationData   = NULL;
        $originNotification = "gen_api_external";
        $title              = $inputTitle == null ? "Sobre o seus cursos" : $inputTitle;
        $status             = "P";
        $dateTime           = new \DateTime();
        
        try {
            $this->notification->inserirNotification($status, $message, $nrorg, $userId, $originNotification, $dateTime, $notificationData, $title, $userFirebaseToken, $externalIdNoti, $confirmation, $dateOfCommitment, $category, $urlImage, $userAutor);
        }catch(Exception $e) {}
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
        $newDate = new \DateTime($dateFormat);
        return $newDate;
    }
}

?>