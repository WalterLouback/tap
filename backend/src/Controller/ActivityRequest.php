<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\ActivityRequest as activityRequestService;
use Zeedhi\ApiGeneral\Service\GenSupportRequest as genSupportRequestService;
use Zeedhi\ApiGeneral\Service\GenSubjectMatter as genSubjectMatterService;

use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Helpers\ImageUploader;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DataSource\Manager;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;

use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;

use Zeedhi\ApiGeneral\Helpers\Util;

class ActivityRequest extends Crud{
    
    protected $dataSourceName = 'activityRequest';
    /**
     * __contruct
     *
     * @param NewsService $newsService
     */
    public function __construct(Manager $dataSourceManager, ActivityRequestService $activityRequestService, ImageUploader $imageUploader) {
        parent::__construct($dataSourceManager);
        $this->activityRequestService = $activityRequestService;
        $this->imageUploader = $imageUploader;
    }
    
    public function getActivityRequestByNrorgAndSupportRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $supportRequestId   = $row['SUPPORT_REQUEST_ID'];
        
        $activity           = $this->activityRequestService->getActivityRequestByNrorgAndSupportRequest($nrorg, $supportRequestId);
        
        foreach ($activity as $key => $value) {
            $activityRequest[$key] = $value->toArray();
            if ($value->getGenActivityRequest()) $activityRequest[$key]['activityRequestOld'] = $value->getGenActivityRequest()->getId();
        }
        
        $dataSet = !empty($activityRequest) ? $activityRequest : [];
        
        $response->addDataSet(new DataSet('response', ['activityRequest' => $dataSet]));
    }
    
    public function createActivityRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();

        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $type                   = isset($row['TYPE'])                    ? $row['TYPE']                     : NULL;
        $description            = isset($row['DESCRIPTION'])             ? $row['DESCRIPTION']              : NULL;
        $fileB64                = isset($row['FILE'])                    ? $row['FILE']                     : NULL;
        $status                 = isset($row['STATUS'])                  ? $row['STATUS']                   : NULL;
        $supportRequestId       = isset($row['SUPPORT_REQUEST_ID'])      ? $row['SUPPORT_REQUEST_ID']       : NULL;
        $activityRequestOldId   = isset($row['ACTIVITY_REQUEST_OLD_ID']) ? $row['ACTIVITY_REQUEST_OLD_ID']  : NULL;
        
        $ImageUploader = $this->imageUploader;
        
        $file  = $fileB64 && strlen($fileB64) > 500 ? $ImageUploader->upload($fileB64, $nrorg) :  $row['FILE'];

        $activityRequest = $this->activityRequestService->createActivityRequest($nrorg, $userId, $type, $description, $file, $status, $supportRequestId, $activityRequestOldId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function updateActivityRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $type                   = isset($row['TYPE'])                    ? $row['TYPE']                     : NULL;
        $description            = isset($row['DESCRIPTION'])             ? $row['DESCRIPTION']              : NULL;
        $fileB64                = isset($row['FILE'])                    ? $row['FILE']                     : NULL;
        $status                 = isset($row['STATUS'])                  ? $row['STATUS']                   : NULL;
        $supportRequestId       = isset($row['SUPPORT_REQUEST_ID'])      ? $row['SUPPORT_REQUEST_ID']       : NULL;
        $activityRequestId      = isset($row['ACTIVITY_REQUEST_ID'])     ? $row['ACTIVITY_REQUEST_ID']      : NULL;
        $activityRequestOldId   = isset($row['ACTIVITY_REQUEST_OLD_ID']) ? $row['ACTIVITY_REQUEST_OLD_ID']  : NULL;
        
        $ImageUploader =$this->imageUploader;
        $file = $fileB64 ? $ImageUploader->upload($fileB64, $nrorg) : NULL;
        
        $activityRequest = $this->activityRequestService->updateActivityRequest($activityRequestId, $nrorg, $userId, $type, $description, $file, $status, $supportRequestId, $activityRequestOldId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function cancellationActivityRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $activityRequestId      = $row['ACTIVITY_REQUEST_ID'];
        
        $this->activityRequestService->cancellationActivityRequest($activityRequestId, $userId, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function getFirstActivityInformation (DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $supportRequestId   = $row['SUPPORT_REQUEST_ID'];
        
        $activity           = $this->activityRequestService->getFirstActivityInformation($nrorg, $supportRequestId);
        
         foreach ($activity as $key => $value) {
            $activityRequest[$key] = $value->toArray();
        }
        
        $dataSet = !empty($activityRequest) ? $activityRequest : [];
        
        $response->addDataSet(new DataSet('response', ['activityRequest' => $dataSet]));
    }
    
    public function getActivityRequestDescending (DTO\Request $request, DTO\Response $response) {
        $row     = Util::getParamsAsArray($request);
        
        $nrorg              = $row['NRORG'];
        $supportRequestId   = $row['SUPPORT_REQUEST_ID'];
        
        $activity           = $this->activityRequestService->getActivityRequestDescending($nrorg, $supportRequestId);
        
        foreach ($activity as $key => $value) {
            $activityRequest[$key] = $value->toArray();
            if ($value->getGenActivityRequest()) $activityRequest[$key]['activityRequestOld'] = $value->getGenActivityRequest()->getId();
        }
        
        $dataSet = !empty($activityRequest) ? $activityRequest : [];
        
        $response->addDataSet(new DataSet('response',  $dataSet));
    }
    
}