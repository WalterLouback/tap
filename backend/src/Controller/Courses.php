<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenUserType;
use Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;

use Zeedhi\ApiGeneral\Service\User as UserService;

use Zeedhi\ApiGeneral\Controller\Exception;
use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration;

use Zeedhi\Framework\Controller\Simple;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;


class Courses extends Simple {

    /** @var UserService */
    protected $userService;

    /**
     * __contruct
     *
     * @param AuthService $authService
     */
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }
    
    /*
     * method que instancia class Integration
     */
    public function getIntegration() {
        $integration  = new Integration(new ConsumeWebServiceMTC());
        return $integration;
    }
    
    public function getCourses(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $inscricaoFilaEsperaModalidades = $this->getIntegration()->getCourses($row);
        $matriculaCursoModalidades      = $this->getIntegration()->getEnrollInCourse($row);

        if(isset($inscricaoFilaEsperaModalidades["res"]["modalidades"]) && isset($matriculaCursoModalidades["res"]["modalidades"])) {
            $inscricoes = $inscricaoFilaEsperaModalidades["res"]["modalidades"];
            $matriculas = $matriculaCursoModalidades["res"]["modalidades"];

            $arrayResultEqual          = [];
            $arrayResultDiffInscricao  = [];
            $arrayResultDiffMatricula  = [];
            $arrayResult               = [];
            /*faço interação na maior matriz*/
            foreach($inscricoes as $inscricao){
                /*faço interação na menor matriz*/
                foreach($matriculas as $matricula){
                    /*se tiver curso em inscrição e matricula add*/
                    if($inscricao["cod_mod_curso"] == $matricula["cod_mod_curso"]) {
                        array_push($arrayResultEqual, array_merge($inscricao, ["fila_espera" => true, "matricula" => true]));
                    }
                    /*se tiver curso em inscrição e não tiver em matricula add*/
                    elseif($inscricao["cod_mod_curso"] != $matricula["cod_mod_curso"]) {
                        array_push($arrayResultDiffInscricao, array_merge($inscricao, ["fila_espera" => true, "matricula" => false]));
                    }
                }
            }
            
             /*faço interação na maior matriz*/
            foreach($matriculas as $matricula){
                /*faço interação na menor matriz*/
                foreach($inscricoes as $inscricao){
                    /*se tiver curso em inscrição e não tiver em matricula add*/
                    if($matricula["cod_mod_curso"] != $inscricao["cod_mod_curso"]) {
                        array_push($arrayResultDiffMatricula, array_merge($matricula, ["fila_espera" => false, "matricula" => true]));
                    }
                }
            }
            
            /*união das matrizes*/
            $arrayMerge  = array_merge($arrayResultEqual, $arrayResultDiffInscricao, $arrayResultDiffMatricula);
            
            /*remove valores duplicados*/
            $arrayResult = $this->myArrayUnique($arrayMerge, SORT_REGULAR);
            
            $dataSet = new DataSet('response', $arrayResult);
        } elseif(isset($matriculaCursoModalidades["res"]["modalidades"])) {
            $dataSet = new DataSet('response', $matriculaCursoModalidades["res"]["modalidades"]);
        } else {
            $dataSet = new DataSet('response', $inscricaoFilaEsperaModalidades);
        }
     
        $response->addDataSet($dataSet);
    }
    
    public static function myArrayUnique($array,$key = "cod_mod_curso"){
        if(null === $key){
            return array_unique($array);
        }
        $keys=[];
        $ret = [];
        foreach($array as $elem){
            $arrayKey = (is_array($elem))?$elem["cod_mod_curso"]:$elem->$key;
            if(in_array($arrayKey,$keys)){
                continue;
            }
            $ret[] = $elem;
            array_push($keys,$arrayKey);
        }
        return $ret;
    }
    
    public function getUserExternalId($row) {
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : null;
        $userId     = isset($row['USER_ID_CONSULTA']) ? $row['USER_ID_CONSULTA'] : null;
        $userTypeId = isset($row['USER_TYPE_ID']) ? $row['USER_TYPE_ID'] : 1;
        
        if($nrorg == null || $userId == null) throw Exception::invalidParameters();
        
        $user       = $this->userService->getUserDataById($nrorg, $userId, $userTypeId);
        
        return $user[0]["externalId"]; 
    }
}