<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\ActivityRequest as activityRequestService;
use Zeedhi\ApiGeneral\Service\SupportRequest as supportRequestService;
use Zeedhi\ApiGeneral\Service\SubjectMatter as subjectMatterService;
use Zeedhi\ApiGeneral\Service\EmailPhpMailer as emailPhpMailerrService;

use Zeedhi\ApiGeneral\Model\Entities\GenStructure;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Helpers\ImageUploader;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DataSource\Manager;

use Zeedhi\ApiGeneral\Service\Notification as NotificationService;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;

use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;

use Zeedhi\ApiGeneral\Helpers\Util;

class SupportRequest extends Crud{
    
    protected $dataSourceName = 'supportRequest';
    /**
     * __contruct
     *
     * @param NewsService $newsService
     * @param NotificationService $notification
     */
    public function __construct(Manager $dataSourceManager, supportRequestService $supportRequestService, ImageUploader $imageUploader, NotificationService $notification) {
        parent::__construct($dataSourceManager);
        $this->supportRequestService = $supportRequestService;
        $this->imageUploader = $imageUploader;
        $this->notification = $notification;
    }
    
    public function getSupportRequestsByUser(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $nrorg   = $row['NRORG'];
        $userId  = $row['USER_ID'];
        
        $supportRequest = $this->supportRequestService->getSupportRequestsByUser($nrorg, $userId);
        
        foreach ($supportRequest as $key => $value) {
            $supportRequest[$key] = $value->toArray();
        }
        
        $dataSet = !empty($supportRequest) ? $supportRequest : [];
        
        $response->addDataSet(new DataSet('response', ['supportRequests' => $dataSet]));
    }
    
    
  public function createSupportRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $structureId            = $row['STRUCTURE_ID'];
        $subjectMatterId        = $row['SUBJECT_MATTER_ID'];
        $description            = $row['DESCRIPTION'];
        $userIdEmployee         = isset($row['USER_ID_EMPLOYEE'])   ? $row['USER_ID_EMPLOYEE']  : NULL;
        $initialTime            = isset($row['INITIAL_TIME'])       ? $row['INITIAL_TIME']      : NULL;
        $finalTime              = isset($row['FINAL_TIME'])         ? $row['FINAL_TIME']        : NULL;
        $fileB64                = isset($row['FILE'])               ? $row['FILE']              : NULL;
        $status                 = 'Novo';
        $initialTimeNew         = NULL;
        $finalTimeNew           = NULL;
        
        if($initialTime) {
            $initialTimeNew     = $this->stringToDate($initialTime);
        }
        
        if($finalTime) {
            $finalTimeNew       = $this->stringToDate($finalTime);
        }
        
        $supportRequest  = $this->supportRequestService->createSupportRequest($nrorg, $userId, $userIdEmployee, $initialTimeNew, $finalTimeNew, $structureId, $status, $subjectMatterId);
        
        try{
            $file = $fileB64 ? $this->imageUploader->upload($fileB64, $nrorg) : NULL;
        }  catch (\Exception $e) {
            throw new Exception('error upload image', $e);
        }
        
        $activityRequest = $this->supportRequestService->createActivityRequest($nrorg, $userId, NULL, $description, $file, NULL, $supportRequest->getId(), NULL);
        
        try {
            $this->sendEmailResponse($subjectMatterId, $structureId, $description, $activityRequest->getFile(), $userId, $nrorg, $supportRequest, $activityRequest->getFile());
        } catch (\Exception $e) {
            throw new Exception('error enviar e-mail', $e);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
        
    }
    
    public function updateSupportRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $userIdEmployee         = $row['USER_ID_EMPLOYEE'];
        $supportRequestId       = $row['SUPPORT_REQUEST_ID'];
        $structureId            = $row['STRUCTURE_ID'];
        $subjectMatterId        = $row['SUBJECT_MATTER_ID'];
        $description            = $row['DESCRIPTION'];
        $initialTime            = isset($row['INITIAL_TIME'])   ? $row['INITIAL_TIME']  : NULL;
        $finalTime              = isset($row['FINAL_TIME'])     ? $row['FINAL_TIME']    : NULL;
        $status                 = isset($row['STATUS'])         ? $row['STATUS']        : NULL;
        $fileB64                = isset($row['FILE'])           ? $row['FILE']              : NULL;
        $initialTimeNew         = NULL;
        $finalTimeNew           = NULL;
        $activityRequestId      = isset($row['ACTIVITY_REQUEST_ID'])        ? $row['ACTIVITY_REQUEST_ID']       : NULL;
        $activityRequestOldId   = isset($row['ACTIVITY_REQUEST_OLD_ID'])    ? $row['ACTIVITY_REQUEST_OLD_ID']   : NULL;
        
        if($initialTime) {
            $initialTimeNew     = $this->stringToDate($initialTime);
        }
        
        if($finalTime) {
            $finalTimeNew       = $this->stringToDate($finalTime);
        }
        
        $supportRequest = $this->supportRequestService->updateSupportRequest($supportRequestId, $nrorg, $userId, $userIdEmployee, $initialTimeNew, $finalTimeNew, $structureId, $status, $subjectMatterId);
        
        $ImageUploader = $this->imageUploader;
        $file = $fileB64 ? $ImageUploader->upload($fileB64, $nrorg) : NULL;
        
        if($activityRequestId) {
            $activityRequest = $this->supportRequestService->updateActivityRequest($activityRequestId, $nrorg, $userId, $type, $description, $file, $status, $supportRequest->getId(), $activityRequestOldId);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function cancellationSupportRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userId                 = $row['USER_ID'];
        $supportRequestId       = $row['SUPPORT_REQUEST_ID'];
        
        $activityRequest = $this->supportRequestService->cancellationSupportRequest($supportRequestId, $userId, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    
    public function getSubjectMatters(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg          = $row['NRORG'];
        
        $subjectMatter  = $this->supportRequestService->getSubjectMatters($nrorg);
        
         foreach ($subjectMatter as $key => $value) {
            $subjectMatter[$key] = $value->toArray();
        }
        
        $dataSet = !empty($subjectMatter) ? $subjectMatter : [];
        
        $response->addDataSet(new DataSet('response', ['subjectMatters' => $dataSet]));

    }
       
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }
    
    
    
    public function getSupportRequestsByFilter (DTO\Request $request, DTO\Response $response) {
        $row     = Util::getParamsAsArray($request);
        $initialDate   = isset($row['initialTimeStr']) && count($row['initialTimeStr']) > 0  ? $row['initialTimeStr'][0] : NULL;
        $finalDate     = isset($row['initialTimeStr']) && count($row['initialTimeStr']) > 0  ? $row['initialTimeStr'][1] : NULL;
        $userName      = isset($row['userName']) && $row['userName'] != '' ? $row['userName']   : NULL;
        $status        = isset($row['status']) && $row['status'] != '' && count($row['status']) > 0 ? $row['status']   : NULL;
        $userEmployeeName     = isset($row['userEmployeeName'])  && $row['userEmployeeName'] != '' && count($row['userEmployeeName']) > 0 ? $row['userEmployeeName']   : NULL;
        $nrorg   = $row['NRORG'];
        $id = isset($row['id']) && $row['id'] != '' ?$row['id'] : NULL;
        $support = $this->supportRequestService->getSupportRequestsByFilter($initialDate, $finalDate, $userName, $userEmployeeName, $id, $nrorg, $status);
         foreach ($support as $key => $value) {
             $supportRequest[$key] = $value->toArray();
         }
        
         $dataSet = !empty($supportRequest) ? $supportRequest : [];
        
         $response->addDataSet(new DataSet('supportRequests', $dataSet));
    }
    
    public function sendEmailResponse($subjectMatterId, $structureId, $description, $fileB64, $userId, $nrorg, $supportRequest, $file) {
        $sendEmail = new emailPhpMailerrService();
        
        $organization = $this->supportRequestService->getOrganization($nrorg);
        // config
        // $host               = "smtp.teknisa.com"; /*anbiente interno*/
        // $host               = "smtp.teknisa.com:5587";/*anbiente externo*/
        // $password           = "94218020@Melissa";
        // $from               = "cristiano.santana@teknisa.com";
        // $fromName           = "Teknisa";
        // $username           = "cristiano.santana@teknisa.com"; // Usuario do servidor de email.
        
        // headers
        $recipient          = "Ouvidoria";
        $addressRecipient   = $this->supportRequestService->getSubjectMatter($subjectMatterId)->getEmailResponsible();
        
        // data
        $subject                    = $this->supportRequestService->getSubjectMatter($subjectMatterId)->getSubjectMatter() . " Numero do Ticket: " . $supportRequest->getId();
        $structures                 = $this->supportRequestService->getPlacesStructures($structureId, $nrorg);
        $genUser                    = $this->supportRequestService->getUserData($userId, 1, $nrorg);
        
        $body['local']              = $structures->getName();
        $body['description']        = $description;
        $body['file']               = isset($fileB64) ? $fileB64 : "Não tem arquivo";
        
        $footer['initialDate']      = $supportRequest->getInitialTime()->format('Y-m-d H:i:s');
        
        $userFirstName              = $this->supportRequestService->getUserData($userId, 1, $nrorg)->getFirstName();
        $userLastName               = $this->supportRequestService->getUserData($userId, 1, $nrorg)->getLastName();
        
        $footer['remetente']        = $userFirstName . " " . $userLastName;
        $footer['emailRemetente']   = $this->supportRequestService->getUserData($userId, 1, $nrorg)->getEmail();
        $footer['npfRemetente']     = $this->supportRequestService->getUserData($userId, 1, $nrorg)->getUserProfile(1, $nrorg)->getExternalId();
        $footer['telefoneRemetente']= $this->supportRequestService->getUserData($userId, 1, $nrorg)->toArray()['CONTACTS'][0]['PHONE'];
        
        
        $message = $this->mountBody($body, $footer, $supportRequest->getId());
        // $response = $sendEmail->sendEmail($host, $username, $password, $recipient, $addressRecipient, $from, $subject, $message, $fromName);
        $response = $sendEmail->sendEmail($recipient, $addressRecipient, $subject, $message, $organization);
    }
    
    public function mountBody($body, $footer, $supportRequestId) {
        $template = 
        '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html">
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<meta http-equiv="Content-Language" content="pt-br">
		</head>
        <div style="text-align: left;">
			<h2>Ol&aacute;</h2> 
			<h3><small style="color:#2d2d2d;">Voce tem uma nova mensagem:</small></h3>
			<h3>
			  <strong> Numero do Ticket: </strong> <small style="color:#2d2d2d;"> ' . $supportRequestId . ' </small> <br/>
			  <strong> Data do Ticket: </strong> <small style="color:#2d2d2d;"> ' . $footer['initialDate'] . ' </small> <br/>
			  <strong> Local: </strong> <small style="color:#2d2d2d;"> ' . $body['local'] . ' </small>  <br/>
			  <strong> Remetente: </strong> <small style="color:#2d2d2d;"> ' . $footer['remetente'] . ' </small> <br/>
			  <strong> E-mail do Remetente: </strong> <small style="color:#2d2d2d;"> ' . $footer['emailRemetente'] . ' </small> <br/>
			  <strong> NPF do Remetente: </strong> <small style="color:#2d2d2d;"> ' . $footer['npfRemetente'] . ' </small> <br/>
			  <strong> Telefone do Remetente: </strong> <small style="color:#2d2d2d;"> ' . $footer['telefoneRemetente'] . ' </small> <br/>
			  <strong> Link do Arquivo: </strong> <small style="color:#2d2d2d;"> <a href="' . $body['file'] . '">' . $body['file'] . '</a></small> <br/><br/>
			  <strong> Mensagem: </strong> <small style="color:#2d2d2d;"> ' . $body['description'] . ' </small>
			 </h3>
		</div>
		</html>
		';
        return $template;
    }

    public function finishSupportRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg                  = $row['NRORG'];
        $userIdEmployee         = $row['USER_ID_EMPLOYEE'];
        $supportRequestId       = $row['SUPPORT_REQUEST_ID'];
        $description            = isset($row['DESCRIPTION']) ? $row['DESCRIPTION'] : "";
        $status                 = $row['STATUS'];

        $this->supportRequestService->finishSupportRequest($supportRequestId, $status);
        $activityRequest = $this->supportRequestService->createActivityRequest($nrorg, $userIdEmployee, 'Encerramento', $description, NULL, NULL, $supportRequestId, NULL);
        
        $response->addDataSet(new DataSet('response', ['response' => 'ok']));
    }
}