<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Controller\Integration as Integration;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DataSource\DataSet;


class EnrollList extends Crud {
     protected $dataSourceName = 'enroll_list';
     
    /**
     * __contruct
     *
     * @param OrganizationService $organizationService
     */
     public function __construct(Integration $integration) {
          $this->Integration = $integration;
     }
     
     public function getEnrollListV2(DTO\Request $request, DTO\Response $response) {
          $row      = $request->getRow();

          $npf      = $row["NPF"];
          $tokenExt = $row["TOKEN_EXT"];
          $cmc      = $row["CMC"];
          $nrorg    = $row["NRORG"];

          $integration = new Integration(new ConsumeWebServiceMTC());

          $enrollList = $integration->getEnrollListV2($npf, $tokenExt, $cmc, $nrorg);

          $response->addDataSet(new DataSet('enroll_list', $enrollList));
     }
}