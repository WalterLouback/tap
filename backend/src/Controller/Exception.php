<?php
namespace Zeedhi\ApiGeneral\Controller;

class Exception extends \Exception {

    const INVALID_PARAMETERS = 1;
    const MISSING_PARAMETERS = 2;
    const NO_ONE_LOGGED = 3;
    const NOT_DEPENDENT_TO_USER_IN_WEBSERVCIE = 6;    
    const ORGANIZATION_NOT_FOUND = 7;
    const ORGANIZATION_NOT_CONFIGURATION = 8;
    const USER_EXISTS_IN_THE_DATABASE = 9;
    const UNAUTHORIZED_ACTION = 10;
    const UNAUTHENTICATED_USER_IN_WEBSERVCIE = 11;
    const TOKEN_EXPIRED = 43;
    const USER_NOT_BELONG_ORGANIZATION = 14;
    const USER_NOT_FOUND = 404;

    public static function invalidParameters() {
        return new static('Invalid parameters!', self::INVALID_PARAMETERS);
    }
    
    public static function missingParameters() {
        return new static("It is missing parameters!", self::MISSING_PARAMETERS);
    }
    
    public static function noOneLogged() {
        return new static('There is no one logged!', self::NO_ONE_LOGGED);
    }
    
    public static function unauthorizedAction() {
        return new static('Unauthorized Action!', self::UNAUTHORIZED_ACTION);
    }
    
    public static function unauthenticatedUserInWebservice() {
        return new static('Unauthenticated User In WebService!', self::UNAUTHENTICATED_USER_IN_WEBSERVCIE);
    }
    
    public static function notDependent() {
        return new static('Not Dependent to User In WebService!', self::NOT_DEPENDENT_TO_USER_IN_WEBSERVCIE);
    }
    
    public static function organizationNotFound() {
        return new static('Organization Not Found!', self::ORGANIZATION_NOT_FOUND);
    }
    
    public static function organizationNotConfiguration() {
        return new static('Organization Not Configuration!', self::ORGANIZATION_NOT_CONFIGURATION);
    }
    
    public static function userExistsInTheDatabase() {
        return new static('User exists in the database!', self::USER_EXISTS_IN_THE_DATABASE);
    }
    
    public static function tokenExpired() {
        return new static('Your session has expired! Please login again!', self::TOKEN_EXPIRED);
    }
    
    /*Cristiano*/
    public static function userNotBelongOrganization() {
        return new static('User does not have the requested user profile.', self::USER_NOT_BELONG_ORGANIZATION);
    }

    public static function loginNotFound() {
        return new static('User not found!', self::USER_NOT_FOUND);
    }    
}