<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\ApiGeneral\Service\Tag as TagService;
use Zeedhi\ApiGeneral\Service\Auth as AuthService;

use Zeedhi\ApiGeneral\Model\Entities\GenOrganization;
use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenTag;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\ParameterBag;
use Zeedhi\Framework\DTO;

class Tag {
    
    /**
     * __contruct
     *
     * @param TagService $tagService
     */
    public function __construct(TagService $tagService) {
        $this->tagService = $tagService;
    }
    
    public function getTagsFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $nrorg = $row[GenOrganization::NRORG];
        
        $tags  = $this->tagService->getTagsFromOrganization($nrorg);
        
        $tags = GenTag::manyToArray($tags);
        $formattedTags = [];

        foreach($tags as $tag) {
            $formattedTags[$tag['TYPE']][] = $tag; 
        }
        
        $response->addDataSet(new DataSet('tags', array_values($formattedTags)));
    }
    
    public function addTagToUser(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $userId  = $row[GenUser::USER_ID];
        $tagId   = $row['TAG_ID'];
        
        $this->tagService->associateTagWithUser($tag, $userId);

        $response->addDataSet(new DataSet('status', ['OK']));
    }
    
    public function addPreferencesToUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $userId     = $row[GenUser::USER_ID];
        $nrorg      = $row[GenOrganization::NRORG];
        $tagId      = $row['TAG_ID'];
        $categoryId = $row['CAT_ID'];

        $this->tagService->associateCategoryWithUser($tagId, $categoryId, $userId, $nrorg);
        
        $preferences = $this->tagService->getPreferencesFromUser($userId, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['preferences' => $preferences]));
    }
    
    public function getPreferencesFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $userId     = $row[GenUser::USER_ID];
        $nrorg      = $row[GenOrganization::NRORG];

        $preferences = $this->tagService->getPreferencesFromUser($userId, $nrorg);

        $response->addDataSet(new DataSet('response', ['preferences' => $preferences]));
    }
    
    public function removeTagFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $userId  = $row[GenUser::USER_ID];
        $tagId   = $row['TAG_ID'];
        
        $this->tagService->removeTagFromUser($tagId, $userId);
        
        $response->addDataSet(new DataSet('status', ['OK']));
    }
    
    /**
     * getUserTags
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function getUserTags(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $userId   = $row[GenUser::USER_ID];
        $nrorg    = $row[GenOrganization::NRORG];
        
        $tags     = $this->tagService->getUserTags($userId, $nrorg);
        $dataSet  = new DataSet('user', ['tags' => GenTag::manyToArray($tags)]);
        
        $response->addDataSet($dataSet);
    }
    
}