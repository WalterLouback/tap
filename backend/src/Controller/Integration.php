<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;

use Zeedhi\ApiGeneral\Controller\Exception;
use Zeedhi\ApiGeneral\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiGeneral\Service\Notification as NotificationService;


use Zeedhi\ApiGeneral\Listeners\RegisterLogs;

class Integration {
    
    public function __construct(ConsumeWebServiceMTC $consumeWebServiceMTC) {
        $this->consumeWebServiceMTC = $consumeWebServiceMTC;
    }

    public function registerUserOnWebService($row){
        $nrorg     = $row['NRORG'];
        $branches  = $row['BRANCHES'];
        $cpf       = $row['CPF'];
        $npf       = $row['NPF'];
        $date_birth= $row['DATE_BIRTH'];
        $email     = $row['EMAIL'];
        $password  = $row['PASSWORD'];
        $password2 = $row['PASSWORD2'];
        $imei      = $row['IMEI'];
        
        $returnWSMinas   = $this->consumeWebServiceMTC->registerUserWebService($branches[0], $cpf, $npf, $date_birth, $email, $password, $password2, $imei);
        $returnWSNautico = $this->consumeWebServiceMTC->registerUserWebService($branches[1], $cpf, $npf, $date_birth, $email, $password, $password2, $imei);
        
        if($returnWSMinas['res']['status'] != "ok" && $returnWSNautico['res']['status'] != "ok") {
            $returnWS = $returnWSMinas;
        } elseif($returnWSMinas['res']['status'] == "ok") {
            $returnWS = $returnWSMinas;
        } elseif($returnWSNautico['res']['status'] == "ok") {
            $returnWS = $returnWSNautico;
        } else {
            $returnWS = array("error nas duas wb");
        }
        
        return $returnWS;
    }
    
    public function webServiceLogin($organization, $email, $cpf, $npf, $password) {
        
        $returnWSMinas = $this->consumeWebServiceMTC->webServiceLogin($organization, $email, $cpf, $npf, $password);
        $returnWSNautico = $this->consumeWebServiceMTC->webServiceLogin("nautico", $email, $cpf, $npf, $password);
        if($returnWSMinas['res']['status'] == "ok") {
            return $returnWSMinas;
        }else {
            return $returnWSNautico;
        }
    }
    
    public function webServiceLoginOne($organization, $email, $cpf, $npf, $password) {
        
        return $this->consumeWebServiceMTC->webServiceLogin($organization, $email, $cpf, $npf, $password);
    }
    
    public function webServiceDadosAssociado ($filial, $tokenExt) {
        $returnWS = $this->consumeWebServiceMTC->webServiceDadosAssociado ($filial, $tokenExt);
        
        return $returnWS;
    }
    
    public function getOrganization($nrorg) {
        switch ($nrorg) {
            case 0:
                $response = "teknisa";
                break;
            case 1:
                $response = "minas";
                break;
            case 2:
                $response = "nautico";
                break;
            default:
                throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    public function getNewsWebService($nrorg=null) {
        $data['f']  = $nrorg !== null ? $this->getOrganization($nrorg) : "";
        $url        = "https://socio.minastc.com.br/ws/noticias.php";
        $result     = $this->consumeWebServiceMTC->consumesWebService($url, $data);
        
        return $result;
    }
    
    public function getQueue($tokenExt, $externalId = NULL, $nrorg) {
        
        $data['f']  = $this->getOrganization($nrorg);
        $data['t']  = $tokenExt;
        $data['a']  = $externalId;
        $url        = "https://socio.minastc.com.br/ws/fila_espera.php";
        
        $result     = $this->consumeWebServiceMTC->consumesWebService($url, $data);
        
        return $result;
    }
    
    public function recoverPassword(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $url        = "https://socio.minastc.com.br/ws/recuperar_senha.php";
        
        $data['f'] = $this->getOrganization($row['BRANCH']);
        $data['e'] = $row['EMAIL'];
        
        $result = $this->consumeWebServiceMTC->consumesWebServicePOST($url, $data);
        
        if(!$result)
            throw new Exception('WebService unavailable!', 1002);
        
        $dataSet = new DataSet('response', $result['res']);
       
        $response->addDataSet($dataSet);
    }
    
    public function getClubAccess(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $url        = "https://socio.minastc.com.br/ws/acessos_ao_clube.php";
        $data['hora']  = isset($row['hora']) ? $row['hora'] : 6;
        $result     = $this->consumeWebServiceMTC->consumesWebService($url, $data);
        if ($result['res']['status'] == "ok") {
            $dataSet = new DataSet('response', $result['res']['acessos']);
        } else {
            throw new Exception('WebService unavailable!', 1002);
        }
    
        $response->addDataSet($dataSet);
    }
    
    public function getQueueWB(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $tokenExt   = $row['TOKEN_EXT'];
        $externalId = $row['EXTERNAL_ID'];
        $nrorg      = $row['NRORG'];
        
        $returnWs = $this->getQueue($tokenExt, $externalId, $nrorg);
        
        $dataSet = ($returnWs['res']['status'] == "erro") ? $returnWs['res']['des_erro'] : $returnWs['res']['fila'];
        
        $response->addDataSet(new DataSet('response', [$dataSet]));
    }
    
    public function checkIfUserIsRegistered(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $branch = $row['BRANCH'];
        $npf    = $row['NPF'];
        $cpf    = $row['CPF'];
        
        if ($branch == 1 || $branch == 2) {
            $returnWsMinas      = $this->consumeWebServiceMTC->checkIfUserIsRegistered("minas", $npf, $cpf);
            $returnWsNautico    = $this->consumeWebServiceMTC->checkIfUserIsRegistered("nautico", $npf, $cpf);
            
            if($returnWsMinas['res']['cadastrado'] == "não" && $returnWsNautico['res']['cadastrado'] == "não") {
                $dataSet = $returnWsMinas['res'];
            }else {
                $dataSet = array('res' => array('status' => 'ok', 'cadastrado' => 'sim'));
            }
        }else {
            $dataSet = array('res' => array('status' => 'ok', 'cadastrado' => 'sim'));
        }
        
        $response->addDataSet(new DataSet('response', [$dataSet]));
    }
    
    public function getEvents(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $url        = "https://socio.minastc.com.br/ws/eventos.php";
        $result     = $this->consumesWebServiceGET($url);
        
        $dataSet = new DataSet('response', $result);
     
        $response->addDataSet($dataSet);
    }
    
    public function getCourses($row) {
        $parameters['f'] = $this->getOrganization($row['NRORG']);
        $parameters['t'] = $row['TOKEN_EXT'];
        $parameters['a'] = $row['EXTERNAL_ID'];
        
        $url = "https://socio.minastc.com.br/ws/inscricao_fila_espera_modalidades.php";
        $courses = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);

        return $courses;
    }
    
    public function getEnrollInCourse($row) {
        $parameters['f'] = $this->getOrganization($row['NRORG']);
        $parameters['t'] = $row['TOKEN_EXT'];
        $parameters['a'] = $row['EXTERNAL_ID'];
        
        $url = "https://socio.minastc.com.br/ws/matricula_curso_modalidades.php";
        $courses = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        
        return $courses;
    }
    
    public function getTurmas(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["a"]   = $row["EXTERNAL_ID"];
        $parameters["cmc"] = $row["CMD"];
        
        $url = "https://socio.minastc.com.br/ws/inscricao_fila_espera_turmas.php";
        $turmas = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        $dataSet = new DataSet('response', $turmas);
     
        $response->addDataSet($dataSet);
    }
    
    public function getEnrollListV2($npf = NULL, $tokenExt, $cmc, $nrorg) {
        $data['f']   = $this->getOrganization($nrorg);
        $data["a"]   = $npf;
        $data["t"]   = $tokenExt;
        $data["cmc"] = $cmc;

        $url         = "https://socio.minastc.com.br/ws/matricula_curso_turmas_com_fila2.php";
        
        $enrollList = $this->consumeWebServiceMTC->consumesWebService($url, $data);
       
        return $enrollList;
    }
    
    public function getModalityList($tokenExt, $npf, $cmc, $nrorg) {
        $data['f']   = $this->getOrganization($nrorg);
        $data["t"]   = $tokenExt;
        $data["a"]   = $npf;
        $data["cmc"] = $cmc;
        $url         = "https://socio.minastc.com.br/ws/inscricao_fila_espera_turmas.php";
        
        $modalityList = $this->consumeWebServiceMTC->consumesWebService($url, $data);
        
        return $modalityList;
    }
    
    public function getFirstDateCourse(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["cmc"] = $row["CMC"];
        $parameters["cc"]  = $row["CC"];
        $parameters["ct"]  = $row["CT"];
        $url = "https://socio.minastc.com.br/ws/matricula_data_primeira_aula.php";
        
        $firstDataCourse = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        $dataSet = new DataSet('response', $firstDataCourse);

        $response->addDataSet($dataSet);
    }
    
    public function getFirstScheduleCourse(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["cmc"] = $row["CMC"];
        $parameters["cc"]  = $row["CC"];
        $parameters["ct"]  = $row["CT"];
        $parameters["data"]  = $row["DATA"];
        $url = "https://socio.minastc.com.br/ws/matricula_hora_primeira_aula.php";
        
        $firstScheduleCourse = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        $dataSet = new DataSet('response', $firstScheduleCourse);

        $response->addDataSet($dataSet);
    }
    
    public function getEvaluationDateCourse(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["cmc"] = $row["CMC"];
        $parameters["cc"]  = $row["CC"];
        $parameters["ct"]  = $row["CT"];
        $url = "https://socio.minastc.com.br/ws/matricula_data_avaliacao.php";
        
        $evaluationDataCourse = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        $dataSet = new DataSet('response', $evaluationDataCourse);
    
        $response->addDataSet($dataSet);
    }
    
    public function getEvaluationScheduleCourse(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["cmc"] = $row["CMC"];
        $parameters["cc"]  = $row["CC"];
        $parameters["ct"]  = $row["CT"];
        $parameters["data"]  = $row["DATA"];
        $url = "https://socio.minastc.com.br/ws/matricula_hora_avaliacao.php";
        
        $evaluationScheduleCourse = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        
        if(!is_array($evaluationScheduleCourse)) {
            $evaluationScheduleCourse = [$evaluationScheduleCourse];
        }
        
        $dataSet = new DataSet('response', $evaluationScheduleCourse);

        $response->addDataSet($dataSet);
    }
    
    public function getQuestionsParq(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $url = "https://socio.minastc.com.br/ws/matricula_perguntas_parq.php";
        
        $questionsParq = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        $dataSet = new DataSet('response', $questionsParq);
    
        $response->addDataSet($dataSet);
    }
    
    public function setPermanentEnroll(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["a"]   = isset($row["EXTERNAL_ID"]) ? $row["EXTERNAL_ID"] : null;
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["cmc"] = $row["CMC"];
        $parameters["cc"]  = $row["CC"];
        $parameters["ct"]  = $row["CT"];
        $parameters["parq"]  = isset($row["PARQ"]) ? $row["PARQ"] : null;
        $parameters["idp"]  = isset($row["IDP"]) ? $row["IDP"] : null;
        $parameters["ida"]  = isset($row["IDA"]) ? $row["IDA"] : null;
        $parameters["atest"]  =  isset($row["ATESTADO"]) ? $row["ATESTADO"] : null;
        $parameters["atest_data"]  = isset($row["DATA_ATT"]) ? $row["DATA_ATT"] : null;
        
        $url = "https://socio.minastc.com.br/ws/matricula_efetivar.php";
        
        $permanentEnroll = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        
        $dataSet = new DataSet('response', $permanentEnroll);

        $response->addDataSet($dataSet);
    }
    
    public function inscricaoEmFilaDeEspera(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters['t']   = $row['TOKEN_EXT'];
        $parameters['a']   = $row['EXTERNAL_ID'];
        $parameters['cc']  = $row['CMC'];
        $parameters['ct']  = $row['CT'];
        $url = "https://socio.minastc.com.br/ws/inscricao_fila_espera.php";
        
        
        $responseInscricao = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);

        $dataSet = new DataSet('response', $responseInscricao);
     
        $response->addDataSet($dataSet);
    }
    
    public function desistenciaDaFilaDeEspera(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["a"]   = $row["EXTERNAL_ID"];
        $parameters["cc"]  = $row["CMC"];
        $parameters["ct"]  = $row["CT"];
        
        $url = "https://socio.minastc.com.br/ws/cancelamento_fila_espera.php";
        $responseCancelamento = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        
        $dataSet = new DataSet('response', $responseCancelamento);
     
        $response->addDataSet($dataSet);    
        
    }
    
    public function desistenciaDaMatricula(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["a"]   = $row["EXTERNAL_ID"];
        $parameters["cc"]  = $row["CMC"];
        $parameters["ct"]  = $row["CT"];
        
        $url = "https://socio.minastc.com.br/ws/cancelar_matricula_curso.php";
        $responseCancelamento = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
        
        $dataSet = new DataSet('response', $responseCancelamento);
     
        $response->addDataSet($dataSet);    
        
    }
        
    public function meusCursos(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();
        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["a"]   = $row["EXTERNAL_ID"];

        $url = "https://socio.minastc.com.br/ws/matriculas_associado.php";
        
        $responseMeusCursos = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);

        $dataSet = new DataSet('response', $responseMeusCursos);
     
        $response->addDataSet($dataSet);    
        
    }
    
    public function getEventsFromRunners(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["a"]   = $row["EXTERNAL_ID"];

        $url = "https://socio.minastc.com.br/ws/eventos_corredores.php";
        
        $responseMeusCursos = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);

        $dataSet = new DataSet('response', $responseMeusCursos);
     
        $response->addDataSet($dataSet);    
        
    }
        
    public function confirmPresence(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $parameters['f']   = $this->getOrganization($row['NRORG']);
        $parameters["t"]   = $row["TOKEN_EXT"];
        $parameters["id"]   = $row["ID"];

        $url = "https://socio.minastc.com.br/ws/confirmar_presenca_corredor.php";
        
        $responseMeusCursos = $this->consumeWebServiceMTC->consumesWebService($url, $parameters);

        $dataSet = new DataSet('response', $responseMeusCursos);
     
        $response->addDataSet($dataSet);
    }
    
    public function getMagazines(DTO\Request\Row $request, DTO\Response $response) {
        $row               = $request->getRow();

        $url = "https://socio.minastc.com.br/ws/revistas.php";
        
        $responseGetMagazines = $this->consumeWebServiceMTC->consumesWebServiceGET($url);

        $dataSet = new DataSet('response', $responseGetMagazines);
     
        $response->addDataSet($dataSet);    
        
    }
        
    public function sendNotificationReply($token, $externalId, $response) {
        $parameters['t']   = $token;
        $parameters["id"]  = $externalId;
        $parameters["r"]   = $response;
        $url = "https://socio.minastc.com.br/ws/resposta_notificacao.php";
        return $this->consumeWebServiceMTC->consumesWebService($url, $parameters);
    }
    
}