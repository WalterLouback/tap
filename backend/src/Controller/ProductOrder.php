<?php
namespace Zeedhi\ApiGeneral\Controller;

use Zeedhi\Framework\Controller\Crud;

class ProductOrder extends Crud {
    
    protected $dataSourceName = 'product_order';

}