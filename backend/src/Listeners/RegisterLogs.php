<?php
namespace Zeedhi\ApiGeneral\Listeners;

class RegisterLogs {
    
    private $statusLog;
    
    public function __construct($statusLog) {
        $this->statusLog     = $statusLog;
    }
    
    public function logMsg($msg, $level) {
        if($this->statusLog){
            $file = dirname(__FILE__).'/../../logs/logs.txt';
            // variável que vai armazenar o nível do log!
            $levelStr = '';
         
            // verifica o nível do log
            switch ( $level )
            {
                case 'postDispatch':
                    $levelStr = 'After Requisition';
                    break;
                case 'preDispatch':
                    $levelStr = 'Before Requisition';
                    break;
                case 'onException':
                    $levelStr = 'On Exception';
                    break;
                case 'login':
                    $file = dirname(__FILE__).'/../../logs/logLogin.txt';
                    $levelStr = 'Route Login';
                    break;
                case 'registerUserWebService':
                    $file = dirname(__FILE__).'/../../logs/logRegisterUserWebService.txt';
                    $levelStr = 'Route registerUserWebService';
                    break;
                case 'autoTask':
                    $file = dirname(__FILE__).'/../../logs/logsAutoTask.txt';
                    $levelStr = 'Rotina de importação';
                    break;
            }
         
            // data atual
            $date = date( 'Y-m-d H:i:s' );
         
            // formata a mensagem do log
            // 1o: data atual
            // 2o: nível da mensagem (INFO, WARNING ou ERROR)
            // 3o: a mensagem propriamente dita
            // 4o: uma quebra de linha
            $msg = sprintf( "[%s] [%s]: %s%s", $date, $levelStr, $msg, PHP_EOL );
         
            // escreve o log no arquivo
            // é necessário usar FILE_APPEND para que a mensagem seja escrita no final do arquivo, preservando o conteúdo antigo do arquivo
            file_put_contents( $file, $msg, FILE_APPEND );
        }
    }
    
    public function verifyIfDirectoryExists($dir){
        $parts = explode('/', $dir);
        $file = array_pop($parts);
        $dir = '';
        foreach($parts as $part)
            if(!is_dir($dir .= "/$part")) mkdir($dir, 0777);
    }
    
    public function deleteDir($dir) {
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
            (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        rmdir($dir); 
    }
}

