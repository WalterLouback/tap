<?php
namespace Zeedhi\ApiGeneral\Listeners;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiGeneral\Service\Environment;
use Zeedhi\ApiGeneral\Service\ServiceImpl;
use Zeedhi\ApiGeneral\Service\ServiceProviderImpl;

use Zeedhi\ApiGeneral\Controller\Exception;

use Zeedhi\Framework\DTO\Request;
use Zeedhi\Framework\DTO\Response;

use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\Cache\Type\FileImpl;
use Zeedhi\Framework\Cache\Type\SessionImpl;
use Zeedhi\Framework\Security\OAuth\OAuthImpl;
use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\Session\Storage\NativeSession;
use Zeedhi\Framework\Session\Attribute\SimpleAttribute;
/*Cristiano*/
use Zeedhi\ApiGeneral\Listeners\RegisterLogs;
/*Cristiano*/
class PostDispatch extends \Zeedhi\Framework\Events\PostDispatch\Listener {
    
    
    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;
    
    public function __construct(EntityManager $entityManager, Environment $environment, RegisterLogs $registerLogs) {
        $this->entityManager = $entityManager;
        $this->environment   = $environment;
        $this->registerLogs  = $registerLogs;
    }
    
    public function postDispatch(Request $request, Response $response) {
        /*Cristiano*/
        $route = $request->getRoutePath();
        $level = "postDispatch";
        
        $this->registerLogs->logMsg("Success in this requisition: " . $route, $level);
        
        if ($route == '/login') {
            $level = "login";
            $responseJson = json_encode($response->getDataSets()[0]->getRows());
            $this->registerLogs->logMsg("Success at login: " . $responseJson, $level);
        }
    }
    
}