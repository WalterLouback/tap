<?php
namespace Zeedhi\ApiGeneral\Listeners;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiGeneral\Service\Environment;
use Zeedhi\ApiGeneral\Service\ServiceImpl;
use Zeedhi\ApiGeneral\Service\ServiceProviderImpl;

use Zeedhi\ApiGeneral\Controller\Exception;

use Zeedhi\ApiPayment\Service\PaymentMethod;

use Zeedhi\Framework\DTO\Request;

use Zeedhi\ApiGeneral\Helpers\Util;

use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\Cache\Type\FileImpl;
use Zeedhi\Framework\Cache\Type\SessionImpl;
use Zeedhi\Framework\Security\OAuth\OAuthImpl;
use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\Session\Storage\NativeSession;
use Zeedhi\Framework\Session\Attribute\SimpleAttribute;

use Zeedhi\ApiGeneral\Listeners\RegisterLogs;


class TokenAuthentication extends \Zeedhi\Framework\Events\PreDispatch\Listener {
    
    
    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;

    private $checkAccessToken;
    private $registerLogs;
    
    public function __construct(EntityManager $entityManager, Environment $environment, $checkAccessToken, RegisterLogs $registerLogs, PaymentMethod $paymentMethodService) {
        $this->entityManager = $entityManager;
        $this->environment   = $environment;
        $this->registerLogs  = $registerLogs;
        $this->checkAccessToken = $checkAccessToken;
        $this->paymentMethodService = $paymentMethodService;
        $this->ignore        = ['/recoverPassword', '/registerUserWebService', '/checkIfUserIsRegistered', '/login', '/authenticateUser', '/logout', '/loginBackoffice', '/requestToken', '/getOrganizationData', '/getLastNews', '/getTicketByEvent', '/getEventData', '/getEventChildren', 
                                '/getTagsFromOrganization', '/getContactMethods', '/verifyMobilePhone', '/getProviders', '/getProviderData',
                                'validateFacebookToken', '/createPassword', '/facebookLogin', '/sendMessageUser', '/forgotPasswordSendEmail',
                                '/getStructures', '/sendConfirmationEmail', '/validateTemporaryPassword', '/posAvailable','/updatePosStatus',
                                '/getStores', '/getStoreData', '/getCategories', '/getEventsByOrg', '/getBalance', '/addCreditCard', '/getStructureById', '/createOrderTicket', '/authenticateUserV1',
                                '/getUserTicketsByOrg', '/getNewsWebService', '/getEventsWebService', '/getBalanceFromGUID', '/getAnalyticsKey', '/getUseIntegration',  '/getStoresNearUser', '/getDeliveryData', '/getServicesByNrorg',
                                '/loginBackofficeCheckingStatusdifferentofI', '/getOrganizationDataByURL', '/authenticateUserWithExternalMethod', '/siclosEndpoint', '/updateUserStonePOS',  '/updateOrganizationStoneID', '/updateStoreRecipient', '/getLocker', '/openTheLock'];
    }
    
    private function setupOAuth($clientId) {
		$storage = new FileImpl(__DIR__ . "/../cache", ".txt");
        $serviceProvider = new ServiceProviderImpl($clientId);
        
        $lifeTime = 10 * 60; //life time in seconds
		$this->oauth = new OAuthImpl($serviceProvider, $storage, time() + $lifeTime);
        
		$this->clientSecret = "SECRET";
    }
    
    public function preDispatch(Request $request) {
        $route = $request->getRoutePath();

        if (substr($route, 0, 16) === '/picpayEndpoint~') {
            $this->picpayCallBack();
            return ;
        }
        else {
            $row   = Util::getParamsAsArray($request);
            
            if (is_null($row)) {
                $data = $request->getParameters();
                if (!isset($data['row']) && (substr($route, 0, 15) === '/siclosEndpoint' || substr($route, 0, 16) === '/updatePosStatus' || substr($route, 0, 15) === '/openTheLock') ) {
                    $row = $data; 
                }
                else $row = json_decode($data['row'], true);
            }
            
            $level = "preDispatch";
            $msg = [['request: ' => $row, 'route' => $route]];
    
            $msgJson = json_encode($msg);
            if (!in_array($route, $this->ignore)) {
                // var_dump($row);die;
                if (empty($row['TOKEN']) || empty($row['USER_ID'])) {
                    throw Exception::invalidParameters();
                }
    
                $token    = $row['TOKEN'];
                $userId   = $row['USER_ID'];
    
                $clientId = "USER_ID_$userId";
                if($this->checkAccessToken)
                    $this->checkAccessToken($token, $clientId);
    
            }
            
            $this->registerLogs->verifyIfDirectoryExists(dirname(__FILE__).'/../../logs/');
            $this->registerLogs->logMsg("Route call: " . $msgJson, $level);
        }
    }

    public function checkAccessToken($token, $clientId) {
        try {
            $this->setupOAuth($clientId);
    		$service = $this->oauth->checkAccess($token, $this->clientSecret);
    		if ($clientId != $service->getClientId()) {
    		    throw Exception::unauthorizedAction();
    		}
        } catch (\Exception $e) {
            throw new Exception('Invalid Token', 42);
        }
    }
    
    private function picpayCallBack() {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Will be a POST
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // Set the url
        $url = 'https://bipfun.teknisa.com/backend/index.php/picpayPaymentCallback';
        // $url = 'http://lucasmacedo.zeedhi.com/workfolder/appCostumer/generalbackend/service/index.php/picpayPaymentCallback';
        curl_setopt($ch, CURLOPT_URL, $url);
        
        $referenceId     = explode("~", $_SERVER["REQUEST_URI"])[1];

        $picpayTokens = $this->paymentMethodService->getPicpayTokens($referenceId);
        $xPicpayToken = $picpayTokens['xPicpayToken'];
        $xSellerToken = $picpayTokens['xSellerToken'];
        
        $body = array(
            'requestType' => "Row",
            'row' => array(
                'REFERENCE_ID' => $referenceId,
                'USER_ID' => '1',
                'X_PICPAY_TOKEN' => $xPicpayToken,
                'X_SELLER_TOKEN' => $xSellerToken,
                'TOKEN' => 'a'
            )
        );
        
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "x-picpay-token: $xPicpayToken",
            "x-seller-token: $xSellerToken"
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        var_dump($result);
    }
    
    
}