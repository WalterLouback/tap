<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenUserType extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenUserType {
    
    const USER_TYPE_ID = 'USER_TYPE_ID';
    const USUARIO_ID  = 1;
    const VENDEDOR_ID = 3;
    const ADMIN_ID    = 4;
    
}