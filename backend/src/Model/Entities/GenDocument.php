<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenDocument extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenDocument {
     
     public static function manyToArray($documentsArray) {
          $arrays = [];
          foreach ($documentsArray as $documents) {
               array_push($arrays, $documents->toArray());
          }
          return $arrays;
     }
     
     public function toArray() {
          return array(
               'ID' => $this->getId(),
               'EXPIRATION_DATE' => $this->getExpirationDate(),
               'NAME' => $this->getName(),
               'STATUS' => $this->getStatus(),
               'TYPE' => $this->getType(),
               'NRORG' => $this->getNrorg(),
               'EMAIL' => $this->getResponsibleEmail()
          );
     }
     
}