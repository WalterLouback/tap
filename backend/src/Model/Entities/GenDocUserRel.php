<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenDocUserRel extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenDocUserRel {
     
     public static function manyToArray($documentsArray) {
          $arrays = [];
          foreach ($documentsArray as $documents) {
               array_push($arrays, $documents->toArray());
          }
          return $arrays;
     }
     
     public function toArray() {
          return array(
               'ID' => $this->getId(),
               'GEN_DOCUMENT_ID' => $this->getGenDocument()->getId(),
               'STATUS' => $this->getStatus(),
               'DOCUMENT' => $this->getDocument()
          );
     }
    
}