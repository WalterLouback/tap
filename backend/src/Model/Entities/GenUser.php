<?php
namespace Zeedhi\ApiGeneral\Model\Entities;

use Zeedhi\ApiGeneral\Helpers\AcosDql;
use Zeedhi\ApiGeneral\Helpers\CosDql;
use Zeedhi\ApiGeneral\Helpers\RadiansDql;
use Zeedhi\ApiGeneral\Helpers\SinDql;
use Zeedhi\ApiGeneral\Model\Entities\GenOrganizationUserRel;

class GenUser extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenUser {

    const USER_ID  = 'USER_ID';
    const FIRST_NAME = 'FIRST_NAME';
    const LAST_NAME = 'LAST_NAME';
    const INITIAL_TIME = "INITIAL_TIME";
    const FINAL_TIME = "FINAL_TIME";
    const CPF      = 'CPF';
    const EMAIL    = 'EMAIL';
    const PASSWORD = 'PASSWORD';
    const TEMP_PASSWORD = 'TEMP_PASSWORD';
    const NEW_PASSWORD = 'NEW_PASSWORD';
    
    const FB_ID = 'FB_ID';    
    const FB_TOKEN = 'FB_TOKEN';
    
    const AUTH_TYPE = 'AUTH_TYPE';
    const AUTH_CODE = 'AUTH_CODE';
    
    const AUTH_TYPE_PHONE = 'PHONE';
    
    const IMAGE = 'IMAGE';
    
    public function build($entityManager, $userTypeId=NULL, $nrorg=NULL, $flagBuildDependencyStatus=true) {
        $this->setCreditCards($entityManager);
        $this->setUserProfiles($entityManager, $nrorg);
        $this->setContacts($entityManager);
        $this->setAssociatedOrganizations($entityManager);
        $this->setTags($entityManager);
        $this->setAddress($entityManager, $nrorg);
        $this->setDependentOf($entityManager, $nrorg);
        $this->setAvailable($entityManager);
        if($flagBuildDependencyStatus){
        $this->setDependencyStatus($entityManager, $nrorg);
        }
        $this->setOrganizationUserRels($entityManager);
    }
    
    public function getCreditCards() {
        return property_exists($this, 'creditCards') ? $this->creditCards : [];
    }
    
    public function getOrganizationUserRels() {
        return property_exists($this, 'organizationUserRels') ? $this->organizationUserRels : [];
    }
    
    public function getUserProfiles($nrorg=NULL) {
        if ($nrorg === NULL) {
            return property_exists($this, 'userProfiles') ? $this->userProfiles : [];
        } else {
            $userProfiles = property_exists($this, 'userProfiles') ? $this->userProfiles : [];
            $profilesFromOrg = [];
            foreach ($userProfiles as $profile) {
                if ($profile->getNrorg() == $nrorg && $profile->getStatus() === 'A') array_push($profilesFromOrg, $profile);
            }
            return $profilesFromOrg;
        }
    }
    
    public function getUserProfile($userTypeId, $nrorg) {
        if (property_exists($this, 'userProfiles')) {
            if ($userTypeId) {
                foreach ($this->userProfiles as $userProfile) {
                    if ($userProfile->getNrorg() == $nrorg && $userProfile->getGenUserType()->getId() == $userTypeId) {
                        return $userProfile;
                    }
                }
            }
        }
        throw new \Exception('User does not have the requested user profile.', 14);
    }
    
    public function getTags() {
        return property_exists($this, 'tags') ? $this->tags : [];
    }
    
    public function getAddress() {
        return property_exists($this, 'address') ? $this->address : [];
    }
    
    public function getContacts() {
       ///// var_dump(property_exists($this, 'contacts') );
        return property_exists($this, 'contacts') ? $this->contacts : [];
        
    }
    
    public function getAssociatedOrganizations() {
        return property_exists($this, 'associatedOrganizations') ? $this->associatedOrganizations : [];
    }
    
    public function getAvailable() {
        return property_exists($this, 'available') ? $this->available : NULL;
    }
    
    public function getDependentOf() {
        return property_exists($this, 'dependentOf') ? $this->dependentOf : NULL;
    }
    
    public function getDependencyStatus() {
        return property_exists($this, 'dependencyStatus') ? $this->dependencyStatus : NULL;
    }
    
    public function setCreditCards($entityManager) {
        $creditCards = $entityManager->getRepository(PayCreditcard::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
        $this->creditCards = $creditCards;
    }
    
    public function setOrganizationUserRels($entityManager) {
        $organizationUserRels = $entityManager->getRepository(GenOrganizationUserRel::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
        $result = GenOrganizationUserRel::manyToArray($organizationUserRels);
        $this->organizationUserRels = $result;
    }
    
    public function setUserProfiles($entityManager, $nrorg) {
        if ($nrorg !== NULL) {
            $userProfiles = $entityManager->getRepository(GenUserTypeRel::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
            $configuration = $entityManager->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
            $profileInOrganization = NULL;
            foreach ($userProfiles as $userProfile) {
                $userProfile->build($entityManager, $nrorg);
                if ($userProfile->getNrorg() == $nrorg && 
                    !!$userProfile->getGenUserType() &&
                    $userProfile->getGenUserType()->getId() === 1) {
                        $profileInOrganization = $userProfile;
                }
            }
            if (!$profileInOrganization && !!$configuration && $configuration->getAllowExternalUsers() == 1) {
                $profileInOrganization = new GenUserTypeRel();
                $profileInOrganization->setGenUser($this);
                $profileInOrganization->setNrorg($nrorg);
                $profileInOrganization->setGenUserType($entityManager->getReference(GenUserType::class, 1));
                $profileInOrganization->setStatus('A');
                $profileInOrganization->setExternalId(0);
                
                array_push($userProfiles, $profileInOrganization);
            }
            $this->userProfiles = $userProfiles;
        }
    }
    
    public function setDependentOf($entityManager, $nrorg) {
        $dependentId = $this->getId();
        
        if ($nrorg !== NULL) {
            $dependentOf = $entityManager->createQuery(
                "
                SELECT pu
                FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' pu
                JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenDependent' d WITH d.parent = pu
                JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUser' pr WITH d.dependent = pr
                WHERE pr.id = $dependentId
                AND d.id > 0
                "
            )->getResult();
            if (count($dependentOf) > 0) {
                $dependentOf[0]->setCreditcards($entityManager);
                $this->dependentOf = $dependentOf[0];
            }
        }
    }
    
    public function setDependencyStatus($entityManager, $nrorg) {
        $dependentId = $this->getId();
        if ($nrorg !== NULL) {
            $dependencyData = $entityManager->createQuery(
                "
                SELECT d
                FROM 'Zeedhi\ApiGeneral\Model\Entities\GenDependent' d 
                WHERE d.dependent = $dependentId
                AND d.nrorg = $nrorg
                "
            )->getResult();
            
            if (count($dependencyData) > 0) {
                $dependencyData[0]->build($entityManager, 1, $nrorg);
                $this->dependencyStatus = $dependencyData[0]->getStatus();
            } else $this->dependencyStatus = "A";
        }
    }
    
    public function setTags($entityManager) {
        $id = $this->getId();
        $tags = $entityManager->createQuery(
            "
            SELECT t
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenTag' t
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\EvtTagUser' tu WITH t = tu.genTag
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUser' u WITH u = tu.genUser
            WHERE tu.genUser = $id
            "
        )->getResult();
        $this->tags = $tags;
    }
    
    public function setAddress($entityManager, $nrorg) {
        $userId = $this->getId();
        if ($nrorg !== NULL) {
            $address = $entityManager->createQuery(
                "
                SELECT a
                FROM 'Zeedhi\ApiGeneral\Model\Entities\GenAddress' a
                WHERE a.genUser = $userId
                AND a.status = 'A'
                AND a.nrorg = $nrorg
                ORDER BY a.createdAt DESC
                "
            )->getResult();
            if (count($address) > 0) $this->address = $address;
        }
    }
    
    public function setContacts($entityManager) {
        $userId   = $this->getId();
        $this->contacts = $entityManager->createQuery(
            "
            SELECT c
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenContact' c
            WHERE c.genUser = $userId
            "
        )->getResult();
    }
    
    public function setAvailable($entityManager) {
        $userId = $this->getId();
        $this->available = $entityManager->createQuery(
            "
            SELECT SER.initialTime, SER.finalTime 
            FROM ".  SerServiceSellerRel::class ." SER
            INNER JOIN ". EvtEventSeller::class ." EVT WITH EVT.id = SER.evtEventSeller
            WHERE EVT.genUser = $userId AND SER.status = 'A'
            "            
        )->getResult();
    }
    
    public function setAssociatedOrganizations($entityManager, $userTypeId=1, $latitude=NULL, $longitude=NULL) {
        $config = $entityManager->getConfiguration();
        $acosDql = AcosDql::class;
        $cosDql = CosDql::class;
        $radiansDql = RadiansDql::class;
        $sinDql = SinDql::class;
        $config->addCustomDatetimeFunction('ACOS', $acosDql);
        $config->addCustomDatetimeFunction('COS', $cosDql);
        $config->addCustomDatetimeFunction('RADIANS', $radiansDql);
        $config->addCustomDatetimeFunction('SIN', $sinDql);
        
        $distanceSelectQuery  = "";
        $distanceWhereQuery   = "";
        $orderBy              = "ORDER BY p.id DESC";
        $orAllowExternalUsers = $userTypeId === 1 ? "OR c.allowExternalUsers = 1" : "";
        
        $distance             = 10; // In kilometers
        
        /* 
        -- Ordernation --
        First place: Organizations (associated to the user) that have structures at x kilometers from the user.
        Second place: Organizations (not associated to the user) that have structures at x kilometers from the user and allows
        external access.
        Third place: Organizations associated to the user.
        
        -- Notes --
        Ps: Using Haversine algorithm to determine distance between user and organizations.
        Ps2: In case the latitude and longitude were not informed, only the user profile is considered.
        */
            
        if ($latitude !== NULL && $longitude !== NULL) {
            $distanceSelectQuery = ", ( 6371 * acos(cos(radians($latitude)) * cos(radians(a.latitude) ) *   
            cos(radians(a.longitude) - radians($longitude)) + sin(radians($latitude)) * 
            sin(radians(a.latitude) ) ) ) as distance";
            
            $orderBy = "ORDER BY distance ASC, p.id DESC";
            
            $distanceWhereQuery = "AND ( ( 6371 * acos(cos(radians($latitude)) * cos(radians(a.latitude) ) *   
            cos(radians(a.longitude) - radians($longitude)) + sin(radians($latitude)) * 
            sin(radians(a.latitude) ) ) ) < $distance OR p.status = 'A')";
        }
        else $orderBy = "ORDER BY p.id DESC";

        $userId = $this->getId();
        $associatedOrganizations = $entityManager->createQuery(
            "
            SELECT DISTINCT o.nrorg, o.name, c.logoImage, c.logoImageSmall, p.id
            $distanceSelectQuery
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenOrganization' o
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenConfiguration' c WITH c.nrorg = o.nrorg
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUserTypeRel' p WITH ( p.nrorg = o.nrorg AND p.genUser = $userId AND p.genUserType = $userTypeId AND p.status = 'A' )
            LEFT JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenStructure' s WITH s.nrorg = o.nrorg
            LEFT JOIN s.genAddress a
            WHERE (
                ( p.genUser = $userId AND p.genUserType = $userTypeId AND p.status = 'A' )
                $orAllowExternalUsers
            )
            $distanceWhereQuery
            GROUP BY o.nrorg
            $orderBy
            "
        )->getResult();
        
        $this->associatedOrganizations = $associatedOrganizations;
    }
    
    public static function manyToArray($users, $userTypeId=NULL, $nrorg=NULL) {
        $arrays = [];
        foreach ($users as $user) {
            array_push($arrays, $user->toArray($userTypeId, $nrorg));
        }
        return $arrays;
    }
    
    public function isSeller($nrorg) {
        foreach ($this->getUserProfiles($nrorg) as $profile) {
            if ($profile->getGenUserType()->getId() === 3) return true;
        }
        return false;
    }
    
    public function hasAccessToApp($nrorg) {
        foreach ($this->getUserProfiles($nrorg) as $profile) {
            if ($profile->getGenUserType()->getId() === 1) return true;
        }
        return false;
    }
    
    public function hasAccessToAdmin($nrorg) {
        foreach ($this->getUserProfiles($nrorg) as $profile) {
            if ($profile->getGenUserType()->getId() === 4) return true;
        }
        return false;
    }
    
    public function toArray($userTypeId=NULL, $nrorg=NULL) {
        return array(
            'ID' => $this->getId(),
            'CPF' => $this->getCpf(),
            'FIRST_NAME' => trim($this->getFirstName()),
            'LAST_NAME' => trim($this->getLastName()),
            'FULL_NAME' => trim($this->getFirstName()) ." ". trim($this->getLastName()),
            'ADDRESS' => $this->getAddress() !== NULL ? GenAddress::manyToArray($this->getAddress()) : NULL,
            'CONTACTS' => GenContact::manyToArray($this->getContacts()),
            ///'CONTACTS' => $this->getContacts() ? GenContact::toArray().'PHONE' : NULL,
            'MAIN_PHONE' => count($this->getContacts()) > 0 ? $this->getContacts()[0]->getPhone() : '',
            'BIRTH_DATE' => $this->getBirthDate(),
            'PREFERENCES' => $this->getPreferences(),
            'EMAIL' => $this->getEmail(),
            'IMAGE' => $this->getImage(),
            'DEPENDENCY_STATUS' => $this->getDependencyStatus(),
            'DEPENDENT_OF' => $nrorg != NULL && $this->getDependentOf() ? $this->getDependentOf()->toArray() : NULL,
            'ORGANIZATION_DATA' => $userTypeId && $nrorg != NULL && $this->getUserProfile($userTypeId, $nrorg) != NULL ? $this->getUserProfile($userTypeId, $nrorg)->toArray() : NULL,
            'USER_PROFILES' => $nrorg != NULL ? GenUserTypeRel::manyToArray($this->getUserProfiles($nrorg)) : NULL,
            'MAIN_CARD_ID' => $this->getMainCreditcard() != NULL ? $this->getMainCreditcard()->getId() : NULL,
            'IS_SELLER' => $nrorg != NULL && $this->isSeller($nrorg) ? "true": "false",
            'HAS_ACCESS_TO_APP' => $nrorg != NULL && $this->hasAccessToApp($nrorg) ? "true": "false",
            'HAS_ACCESS_TO_ADMIN' => $nrorg != NULL && $this->hasAccessToAdmin($nrorg) ? "true": "false",
            'STATUS' => $this->getStatus() != NULL ? $this->getStatus() : NULL,
            'AVAILABLE' => $this->getAvailable(),
            'ORGANIZATION_USER_RELS' => $this->getOrganizationUserRels(),
            'RESET_PASSWORD' => $this->getResetPassword(),
            'POS' => $this->getPosSerialNumber()
        );
    }
    
}