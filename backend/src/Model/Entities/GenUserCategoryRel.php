<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenUserCategoryRel extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenUserCategoryRel {
    
    public static function manyToArray($categories) {
        $arrays = [];
        foreach ($categories as $category) {
          array_push($arrays, $category->toArray());
        };
        return $arrays;
    }
    
    public function toArray() {
        return array(
          'ID' => $this->getGenCategory() ? $this->getGenCategory()->getId() : NULL,
          'NAME' => $this->getGenCategory() ? $this->getGenCategory()->getName() : NULL
        );
    }
    
}