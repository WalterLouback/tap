<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenUserTagRel extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenUserTagRel {
    
    public static function manyToArray($tags) {
        $arrays = [];
        foreach ($tags as $key=>$tag) {
          $arrays[$key] = $tag->toArray();
        };
        return $arrays;
    }
    
    public function toArray() {
        return array(
          'ID' => $this->getGenTag() ? $this->getGenTag()->getId() : NULL,
          'NAME' => $this->getGenTag() ? $this->getGenTag()->getName() : NULL
        );
    }
    
}