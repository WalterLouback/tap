<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class EvtEventSeller extends \Zeedhi\ApiGeneral\Model\Entities\Base\EvtEventSeller {
    
    function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_DATA' => $this->getGenUser() ? $this->getGenUser()->toArray() : [],
            'SELLER_TYPE' => $this->getEvtSellerType() ? $this->getEvtSellerType()->toArray() : null
        );
    }
    
    
    static function manyToArray($sellers) {
        $array = [];
        foreach ($sellers as $seller) {
            array_push($array, $seller->toArray());
        }
        return $array;
    }
}