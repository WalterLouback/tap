<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenTaaConfiguration extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenTaaConfiguration {
    
    public function toArray() {
        return array(
            'nrorg' => $this->getNrorg(),
            'name' => $this->getName(),
            'modality' => $this->getModality(),
            'posSerialNumber' => $this->getPosSerialNumber(),
            'posReferenceId' => $this->getPosReferenceId(),
            'externalApiUrl' => $this->getExternalApiUrl(),
            'externalApiId' => $this->getExternalApiId(),
            'createdAt' => $this->getCreatedAt(),
            'modifiedAt' => $this->getModifiedAt(),
            'createdBy' => $this->getCreatedBy(),
            'modifiedBy' => $this->getModifiedBy(),
            'id' => $this->getId(),
            'status' => $this->getStatus(),
            'email' => $this->getEmail(),
            'structureId' => $this->getStructureId() ? $this->getStructureId()->getId() : null,
            'structure' => $this->getStructureId() ? $this->getStructureId()->getName() : null,
            'storeId' => $this->getStoreId() ? $this->getStoreId()->getId() : null,
            'store' => $this->getStoreId() ? $this->getStoreId()->getName() : null,
           
        );
    }
}