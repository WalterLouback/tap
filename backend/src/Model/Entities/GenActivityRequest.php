<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenActivityRequest extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenActivityRequest {
    
    public function toArray() { 
        return array(
            'id'            => $this->getId(),
            'type'          => $this->getType(),
            'description'   => $this->getDescription(),
            'file'          => $this->getFile(),
            'fileUrl'       => $this->getFile(),
            'status'        => $this->getStatus(),
            'nrorg'         => $this->getNrorg(),
            'userId'        => $this->getGenUser() ? $this->getGenUser()->getId() : NULL,
            'userName'      => $this->getGenUser() ? $this->getGenUser()->getFirstName() ." ". $this->getGenUser()->getLastName() : NULL,
            'parentId'      => $this->getGenActivityRequest() ? $this->getGenActivityRequest()->getId() : NULL, 
            'createdAt'     => $this->getCreatedAt(),
            'createdAtStr'  => $this->getCreatedAt() ? $this->getCreatedAt()->__toString() : NULL
        );
    }
    
}