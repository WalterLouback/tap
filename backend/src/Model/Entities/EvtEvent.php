<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class EvtEvent extends \Zeedhi\ApiGeneral\Model\Entities\Base\EvtEvent {
    
    public function toArray() {
        $array = [];
        $array['storeId'] = $this->getId();
        $array['storeName'] = $this->getName();
        
        return $array;
    }

    
}