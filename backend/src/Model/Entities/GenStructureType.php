<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenStructureType extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenStructureType {
    
    public function build($entityManager=NULL) {
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['status'] = $this->getStatus();
        return $array;
    }
    
    public static function manyToArray($structures) {
        $arrays = [];
        foreach ($structures as $structure) {
            array_push($arrays, $structure->toArray());
        }
        
        return $arrays;
    }
 
}