<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenConfiguration extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenConfiguration {
    
    public function build($entityManager) {
        $this->setGateway($entityManager);
    }
    
    public function getGateway() {
        return property_exists($this, 'gateway') ? $this->gateway : NULL;
    }
    
    public function setGateway($entityManager) {
        $gatewayRel = $entityManager->getRepository(PayGatewayRel::class)->findOneBy(['genConfiguration' => $this->getId()]);
        if ($gatewayRel) $this->gateway = $gatewayRel->getPayGateway();
    }
    
    public function toArray() {
        return array(
            'LOGO_IMAGE' => $this->getLogoImage(),
            'LOGO_IMAGE_SMALL' => $this->getLogoImageSmall(),
            'LOGO_IMAGE1' => $this->getLogoImage(),
            'LOGO_IMAGE_SMALL1' => $this->getLogoImageSmall(),
            'FILE_URL' => $this->getLogoImage(),
            'FILE_URL_SMALL' => $this->getLogoImageSmall(),
            'PRIMARY_COLOR' => $this->getPrimaryColor(),
            'SECONDARY_COLOR' => $this->getSecondaryColor(),
            'YES_COLOR' => $this->getYesColor(),
            'NO_COLOR' => $this->getNoColor(),
            'GATEWAY' => $this->getGateway() ? $this->getGateway()->toArray() : NULL,
            'USE_INTEGRATION' => $this->getUseIntegration(),
            'PREFERENCE_FLOW' => $this->getPreferenceFlow() ? $this->getPreferenceFlow() : FALSE,
            'FCM_SERVER_KEY' => $this->getFcmServerKey(),
            'AWS_S3_ACCESS_KEY' => $this->getAwsS3AccessKey(),
            'AWS_S3_SECRET_KEY' => $this->getAwsS3SecretKey(),
            'AWS_S3_BUCKET' => $this->getAwsS3Bucket(),
            'HOST' => $this->getHost(),
            'HOST_FROM' => $this->getHostFrom(),
            'HOST_PASSWORD' => $this->getHostPassword(),
            'HOST_FROM_NAME' => $this->getHostFromName(),
            'GOOGLE_ANALYTICS_KEY' => $this->getGoogleAnalyticsKey(),
            'APP_LAYOUT_CONFIG' => $this->getAppLayoutConfig() != null ? json_decode($this->getAppLayoutConfig(), true) : NULL,
            'ALLOW_EXTERNAL_USERS' => $this->getAllowExternalUsers(),
            'KEY_GOOGLE' => $this->getKeyGoogle(),
            'CONVENIENCE_FEE' => $this->getConvenienceFee(),
            'URL' => $this->getURL(),
            'TOKEN_API_EXTERNAL' => $this->gettokenApiExternal(),
            'URL_API_EXTERNAL' => $this->geturlApiExternal(),
            'ORIGIN_API_EXTERNAL' => $this->getOriginApiExternal(),
            'STONE_ESTABLISHMENT_ID' => $this->getStoneEstablishmentId(),
            'OVERLAY_IMAGE_URL' => $this->getOverlayBackground(),
            'OVERLAY_BACKGROUND' => $this->getOverlayBackground()
        );
    }
    
    public function toArrayAdmin() {
        return array(
            'LOGO_IMAGE' => $this->getLogoImage(),
            'LOGO_IMAGE_SMALL' => $this->getLogoImageSmall(),
            'LOGO_IMAGE1' => $this->getLogoImage(),
            'LOGO_IMAGE_SMALL1' => $this->getLogoImageSmall(),
            'FILE_URL' => $this->getLogoImage(),
            'FILE_URL_SMALL' => $this->getLogoImageSmall(),
            'PRIMARY_COLOR' => $this->getPrimaryColor(),
            'SECONDARY_COLOR' => $this->getSecondaryColor(),
            'TOKEN_API_EXTERNAL' => $this->gettokenApiExternal(),
            'URL_API_EXTERNAL' => $this->geturlApiExternal(),
            'ORIGIN_API_EXTERNAL' => $this->getOriginApiExternal(),
            'STONE_ESTABLISHMENT_ID' => $this->getStoneEstablishmentId(),
            'OVERLAY_BACKGROUND' => $this->getOverlayBackground(),
            'OVERLAY_IMAGE_URL' => $this->getOverlayBackground(),
        );
    }
    
}