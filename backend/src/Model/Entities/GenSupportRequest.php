<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenSupportRequest extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenSupportRequest {
    
    public function build($entityManager) {
        // $this->setActivityRequest($entityManager);
    }
    
    public function setActivityRequest($entityManager) {
        $activities = $entityManager->getRepository(GenActivityRequest::class)->findBy(['genSupportRequest' => $this->getId()]);
        
        $this->activities = $activities;
    }
    
    public function getActivityRequest() {
        return property_exists($this, 'activities') ? $this->activities : [];
    }
    
    public static function manyToArray($activities) {
        $arrays = [];
 
        foreach ($activities as $activity) {
            $array = $activity->toArray();
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'id'                     => $this->getId(),
            'initialTime'            => $this->getInitialTime(),
            'initialTimeStr'         => $this->getInitialTime() ? $this->getInitialTime()->__toString() : NULL,
            'finalTime'              => $this->getFinalTime(),
            'finalTimeStr'           => $this->getFinalTime() ? $this->getFinalTime()->__toString() : NULL,
            'status'                 => $this->getStatus(),
            'nrorg'                  => $this->getNrorg(),
            'userId'                 => $this->getGenUser() ? $this->getGenUser()->getId() : NULL,
            'userName'               => $this->getGenUser() ? $this->getGenUser()->getFirstName() ." ". $this->getGenUser()->getLastName() : NULL,
            'userImage'              => $this->getGenUser() ? $this->getGenUser()->getImage() : NULL,   
            'userEmployeeId'         => $this->getGenUserEmployee() ? $this->getGenUserEmployee()->getId() : NULL,
            'userEmployeeName'       => $this->getGenUserEmployee() ? $this->getGenUserEmployee()->getFirstName() ." ". $this->getGenUserEmployee()->getLastName() : NULL,
            'genSubjectMatter'       => $this->getGenSubjectMatter() ? $this->getGenSubjectMatter()->toArray() : NULL,
            'genSubjectMatterName'   => $this->getGenSubjectMatter() ? $this->getGenSubjectMatter()->getSubjectMatter() : NULL,
            'genSubjectMattereId'    => $this->getGenSubjectMatter() ? $this->getGenSubjectMatter()->getId() : NULL,
            'genSubjectMattereEmail' => $this->getGenSubjectMatter() ? $this->getGenSubjectMatter()->getEmailResponsible() : NULL,
            'genStructure'           => $this->getGenStructure() ? $this->getGenStructure()->toArray() : NULL,
            'genStructureName'       => $this->getGenStructure() ? $this->getGenStructure()->getName() : NULL,
            'genStructureId'         => $this->getGenStructure() ? $this->getGenStructure()->getId() : NULL
            // 'genActivityRequests'   => GenSupportRequest::manyToArray($this->getActivityRequest())
        );
    }
}