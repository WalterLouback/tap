<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class PayCreditcard extends \Zeedhi\ApiGeneral\Model\Entities\Base\PayCreditcard {
    
    public static function manyToArray($creditCards) {
        $arrays = [];
        foreach ($creditCards as $creditCard) {
            array_push($arrays, $creditCard->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'expirationDate' => $this->getExpirationDate(),
            'flag' => $this->getFlag(),
            'lastNumbers' => $this->getLastNumbers()
        );
    }

}