<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenPermission extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenPermission {
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}