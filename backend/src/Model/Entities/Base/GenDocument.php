<?php

namespace Zeedhi\ApiGeneral\Model\Entities\Base;

class GenDocument
{
    protected $id;
    
    protected $expirationDate;
    
    protected $name;
    
    protected $responsibleEmail;

    protected $status;
    
    protected $type;
    
    protected $nrorg;
    
    protected $createdBy;

    protected $modifiedBy;

    protected $createdAt;

    protected $modifiedAt;

    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;

        return $this;
    }
    
    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime();
        
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getResponsibleEmail()
    {
        return $this->responsibleEmail;
    }

    public function setResponsibleEmail($responsibleEmail)
    {
        $this->responsibleEmail = $responsibleEmail;

        return $this;
    }
   
}
