<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenDataUpdate {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $document;
    /** @var string  */
    protected $updateStatus;
    /** @var string  */
    protected $firstName;
    /** @var string  */
    protected $lastName;
    /** @var string  */
    protected $cpf;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenAddress  */
    protected $genAddress;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $approvedBy;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** */
    protected $genDocument;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getDocument() {
        return $this->document;
    }
	public function setDocument($document) {
        $this->document = $document;
    }
	public function getUpdateStatus() {
        return $this->updateStatus;
    }
	public function setUpdateStatus($updateStatus) {
        $this->updateStatus = $updateStatus;
    }
	public function getFirstName() {
        return $this->firstName;
    }
	public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }
	public function getLastName() {
        return $this->lastName;
    }
	public function setLastName($lastName) {
        $this->lastName = $lastName;
    }
	public function getCpf() {
        return $this->cpf;
    }
	public function setCpf($cpf) {
        $this->cpf = $cpf;
    }
	public function getGenAddress() {
        return $this->genAddress;
    }
	public function setGenAddress(\Zeedhi\ApiGeneral\Model\Entities\GenAddress $genAddress = NULL) {
        $this->genAddress = $genAddress;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getApprovedBy() {
        return $this->approvedBy;
    }
	public function setApprovedBy(\Zeedhi\ApiGeneral\Model\Entities\GenUser $approvedBy = NULL) {
        $this->approvedBy = $approvedBy;
    }
    public function getGenDocument() {
        return $this->genDocument;
    }

    public function setGenDocument(\Zeedhi\ApiGeneral\Model\Entities\GenDocument $genDocument = NULL) {
        $this->genDocument = $genDocument;
    }
    
}