<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenNotification {

    /** @var int  */
    protected $id;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $message;
    /** @var string  */
    protected $originNotification;
    /** @var DATE  */
    protected $dateTime;
     /** @var int  */
    protected $externalId;
    /** @var int  */
    protected $confirmation;
    /** @var string  */
    protected $title;
    /** @var \Datetime  */
    protected $dateOfCommitment;
    
    protected $category;
    
    protected $urlImage;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getMessage() {
        return $this->message;
    }
	public function setMessage($message = NULL) {
        $this->message = $message;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
    public function getOriginNotification() {
        return $this->originNotification;
    }
	public function setOriginNotification($originNotification = NULL) {
        $this->originNotification = $originNotification;
    }
    public function getDateTime() {
        return $this->dateTime;
    }
	public function setDateTime($dateTime) {
        $this->dateTime = $dateTime;
    }
    public function getExternalId()
    {
        return $this->externalId;
    }
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }
    public function getConfirmation()
    {
        return $this->confirmation;
    }
    public function setConfirmation($confirmation)
    {
        $this->confirmation = $confirmation;
        return $this;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    public function getDateOfCommitment() {
        return $this->dateOfCommitment;
    }
    public function setDateOfCommitment($dateOfCommitment) {
        $this->dateOfCommitment = $dateOfCommitment;
    }
    public function getCategory() {
        return $this->category;
    }
    public function setCategory($category) {
        $this->category = $category;
    }
    public function getUrlImage() {
        return $this->urlImage;
    }
    public function setUrlImage($urlImage) {
        $this->urlImage = $urlImage;
    }
    
    public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
}