<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenTaaConfiguration {

    protected $id = 0;

    protected $nrorg = 0;

    protected $name;

    protected $modality;

    protected $structureId;

    protected $storeId;

    protected $posSerialNumber;

    protected $posReferenceId;

    protected $externalApiUrl;
    
    protected $externalApiId;
    
    protected $modifiedAt;
    
    protected $modifiedBy;
    
    protected $createdAt;
    
    protected $createdBy;

    protected $status;
    
    protected $email;
    
    public function getNrorg(){
        return $this->nrorg ? $this->nrorg : null;
    }

    public function setNrorg($nrorg){
        $this->nrorg = $nrorg;
    }

    public function getName(){
        return $this->name ? $this->name : null;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getModality(){
        return $this->modality ? $this->modality : null;
    }

    public function setModality($modality){
        $this->modality = $modality;
    }

    public function getPosSerialNumber(){
        return $this->posSerialNumber ? $this->posSerialNumber : null;
    }

    public function setPosSerialNumber($posSerialNumber){
        $this->posSerialNumber = $posSerialNumber;
    }

    public function getPosReferenceId(){
        return $this->posReferenceId ? $this->posReferenceId : null;
    }

    public function setPosReferenceId($posReferenceId){
        $this->posReferenceId = $posReferenceId;
    }

    public function getExternalApiUrl(){
        return $this->externalApiUrl ? $this->externalApiUrl : null;
    }

    public function setExternalApiUrl($externalApiUrl){
        $this->externalApiUrl = $externalApiUrl;
    }

    public function getExternalApiId(){
        return $this->externalApiId ? $this->externalApiId : null;
    }

    public function setExternalApiId($externalApiId){
        $this->externalApiId = $externalApiId;
    }

    public function getCreatedAt(){
        return $this->createdAt ? $this->createdAt : null;
    }

    public function setCreatedAt(){
        $this->createdAt = new \DateTime();
    }

    public function getModifiedAt(){
        return $this->modifiedAt ? $this->modifiedAt : null;
    }

    public function setModifiedAt(){
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy(){
        return $this->createdBy ? $this->createdBy : null;
    }

    public function setCreatedBy($createdBy){
        $this->createdBy = $createdBy;
    }

    public function getModifiedBy(){
        return $this->modifiedBy ? $this->modifiedBy : null;
    }

    public function setModifiedBy($modifiedBy){
        $this->modifiedBy = $modifiedBy;
    }

    public function getId(){
        return $this->id ? $this->id : null;
    }

    public function getStructureId(){
        return $this->structureId ? $this->structureId : null;
    }

    public function setStructureId($structureId){
        $this->structureId = $structureId;
    }

    public function getStoreId(){
        return $this->storeId ? $this->storeId : null;
    }

    public function setStoreId($storeId){
        $this->storeId = $storeId;
    }
    public function getStatus(){
        return $this->status ? $this->status : null;
    }
	public function setStatus($status = NULL){
        $this->status = $status;
    }
    	public function setEmail($email= NULL){
        $this->email = $email;
	}
          public function getEmail(){
        return $this->email ? $this->email : null;
    }
}