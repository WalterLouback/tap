<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenDependent {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $monthlyLimit = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $receiptsTo;
    /** @var string  */
    protected $canUseMainCard;
    /** @var string  */
    protected $status;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $dependent;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $parent;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getMonthlyLimit() {
        return $this->monthlyLimit;
    }
	public function setMonthlyLimit($monthlyLimit) {
        $this->monthlyLimit = $monthlyLimit;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getCanUseMainCard() {
        return $this->canUseMainCard;
    }
	public function setCanUseMainCard($canUseMainCard) {
        $this->canUseMainCard = $canUseMainCard;
    }
	public function getDependent() {
        return $this->dependent;
    }
	public function setDependent($dependent = NULL) {
        $this->dependent = $dependent;
    }
	public function getParent() {
        return $this->parent;
    }
	public function setParent($parent = NULL) {
        $this->parent = $parent;
    }
	public function getReceiptsTo() {
        return $this->receiptsTo;
    }
	public function setReceiptsTo($receiptsTo = NULL) {
        $this->receiptsTo = $receiptsTo;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    
}