<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class OrdSelectedOption {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\OrdItemExtra  */
    protected $ordItemExtra;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\OrdOption  */
    protected $ordOption;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdItemExtra() {
        return $this->ordItemExtra;
    }
	public function setOrdItemExtra(\Zeedhi\ApiGeneral\Model\Entities\OrdItemExtra $ordItemExtra = NULL) {
        $this->ordItemExtra = $ordItemExtra;
    }
	public function getOrdOption() {
        return $this->ordOption;
    }
	public function setOrdOption(\Zeedhi\ApiGeneral\Model\Entities\OrdOption $ordOption = NULL) {
        $this->ordOption = $ordOption;
    }
}