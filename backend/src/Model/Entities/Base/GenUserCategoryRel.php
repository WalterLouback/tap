<?php

namespace Zeedhi\ApiGeneral\Model\Entities\Base;

abstract class GenUserCategoryRel
{
    protected $id;
    
    protected $status;
    
    protected $nrorg;
    
    protected $genCategory;
    
    protected $genUser;
    
    protected $createdAt;
    
    protected $modifiedAt;
    
    protected $createdBy;
    
    protected $modifiedBy;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status = NULL)
    {
        $this->status = $status;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg = NULL)
    {
        $this->nrorg = $nrorg;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt($a)
    {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGenUser()
    {
        return $this->genUser;
    }

    public function setGenUser($genUser)
    {
        $this->genUser = $genUser;
    }

    public function getGenCategory()
    {
        return $this->genCategory;
    }

    public function setGenCategory($genCategory)
    {
        $this->genCategory = $genCategory;
    }
}