<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenActivityRequest {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $status;
    /** @var string */
    protected $type;
    /** @var string */
    protected $description;
    /** @var string */
    protected $_file;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $createdBy;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenSupportRequest  */
    protected $genSupportRequest;    
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenActivityRequest  */
    protected $genActivityRequest;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
    public function getDescription() {
        return $this->description;
    }
	public function setDescription($description = NULL) {
        $this->description = $description;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    public function getFile() {
        return $this->_file;
    }
	public function setFile($_file = NULL) {
        $this->_file = $_file;
    }
    public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
    public function getGenSupportRequest() {
        return $this->genSupportRequest;
    }
	public function setGenSupportRequest(\Zeedhi\ApiGeneral\Model\Entities\GenSupportRequest $genSupportRequest = NULL) {
        $this->genSupportRequest = $genSupportRequest;
    }
    public function getGenActivityRequest() {
        return $this->genActivityRequest;
    }
	public function setGenActivityRequest(\Zeedhi\ApiGeneral\Model\Entities\GenActivityRequest $genActivityRequest = NULL) {
        $this->genActivityRequest = $genActivityRequest;
    }
}