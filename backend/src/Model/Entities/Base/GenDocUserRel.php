<?php

namespace Zeedhi\ApiGeneral\Model\Entities\Base;

class GenDocUserRel
{
    protected $id;
    
    protected $genDocument;
    
    protected $genUser;
    
    protected $status;
    
    protected $document;
    
    protected $nrorg;
    
    protected $createdBy;
    
    protected $modifiedBy;
    
    protected $createdAt;
    
    protected $modifiedAt;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getDocument()
    {
        return $this->document;
    }

    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;

        return $this;
    }
    
    public function setModifiedAt() 
    {
        $this->modifiedAt = new \DateTime();

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGenDocument()
    {
        return $this->genDocument;
    }

    public function setGenDocument($genDocument)
    {
        $this->genDocument = $genDocument;

        return $this;
    }

    public function getGenUser()
    {
        return $this->genUser;
    }

    public function setGenUser($genUser)
    {
        $this->genUser = $genUser;

        return $this;
    }
}
