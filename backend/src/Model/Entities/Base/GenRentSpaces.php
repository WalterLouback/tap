<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenRentSpaces {
    
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $description;
    /** @var string  */
    protected $linkTourVirtual;
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $maximumPersonCapacity;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $address;
    /** @var string  */
    protected $telephone;
    /** @var int  */
    protected $id;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
    public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getLinkTourVirtual() {
        return $this->linkTourVirtual;
    }
	public function setLinkTourVirtual($linkTourVirtual = NULL) {
        $this->linkTourVirtual = $linkTourVirtual;
    }
    public function getMaximumPersonCapacity() {
        return $this->maximumPersonCapacity;
    }
	public function setMaximumPersonCapacity($maximumPersonCapacity) {
        $this->maximumPersonCapacity = $maximumPersonCapacity;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getDescription() {
        return $this->description;
    }
	public function setDescription($description = NULL) {
        $this->description = $description;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
    public function getAddress() {
        return $this->address;
    }
	public function setAddress($address) {
        $this->address = $address;
    }
    public function getTelephone() {
        return $this->telephone;
    }
	public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiGeneral\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}