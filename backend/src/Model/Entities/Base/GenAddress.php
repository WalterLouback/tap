<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenAddress {
    
    
    /** @var \Datetime  */
    protected $createDate;
    /** @var string  */
    protected $nrorg = 0;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $provincy;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $city;
    /** @var string  */
    protected $neighborhood;
    /** @var string  */
    protected $street;
    /** @var string  */
    protected $cep;
    /** @var string  */
    protected $number;
    /** @var string  */
    protected $complement;
    /** @var string  */
    protected $latitude;
    /** @var string  */
    protected $longitude;
    /** @var string  */
    protected $country;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $imageDocument;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenAddress  */
    protected $old;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedAt = $modifiedBy;
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreateDate() {
        return $this->createDate;
    }
	public function setCreateDate(\Datetime $createDate) {
        $this->createDate = $createDate;
    }
	public function getProvincy() {
        return $this->provincy;
    }
	public function setProvincy($provincy = NULL) {
        $this->provincy = $provincy;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getCity() {
        return $this->city;
    }
	public function setCity($city = NULL) {
        $this->city = $city;
    }
	public function getNeighborhood() {
        return $this->neighborhood;
    }
	public function setNeighborhood($neighborhood = NULL) {
        $this->neighborhood = $neighborhood;
    }
	public function getStreet() {
        return $this->street;
    }
	public function setStreet($street = NULL) {
        $this->street = $street;
    }
	public function getCep() {
        return $this->cep;
    }
	public function setCep($cep) {
        $this->cep = $cep;
    }
	public function getNumber() {
        return $this->number;
    }
	public function setNumber($number) {
        $this->number = $number;
    }
	public function getComplement() {
        return $this->complement;
    }
	public function setComplement($complement) {
        $this->complement = $complement;
    }
	public function getLatitude() {
        return $this->latitude;
    }
	public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
	public function getLongitude() {
        return $this->longitude;
    }
	public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
	public function getCountry() {
        return $this->country;
    }
	public function setCountry($country) {
        $this->country = $country;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getImageDocument() {
        return $this->imageDocument;
    }
	public function setImageDocument($imageDocument) {
        $this->imageDocument = $imageDocument;
    }
	public function getOld() {
        return $this->old;
    }
	public function setOld(\Zeedhi\ApiGeneral\Model\Entities\GenAddress $old = NULL) {
        $this->old = $old;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}