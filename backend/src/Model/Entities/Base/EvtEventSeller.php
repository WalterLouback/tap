<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class EvtEventSeller {
    
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\EvtSellerType  */
    protected $evtSellerType;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var string  */
    protected $status;
    
	public function getCreatedAt() {
        return $this->createdAt; 
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiGeneral\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
    public function getEvtSellerType() {
        return $this->evtSellerType;
    }
	public function setEvtSellerType(\Zeedhi\ApiGeneral\Model\Entities\EvtSellerType $evtSellerType = NULL) {
        $this->evtSellerType = $evtSellerType;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
}