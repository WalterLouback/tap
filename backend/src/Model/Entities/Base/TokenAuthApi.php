<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class TokenAuthApi {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $token;
    /** @var string  */
    protected $email;
    /** @var string  */
    protected $password;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $nrorg;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getToken() {
        return $this->token;
    }
	public function setToken($token = NULL) {
        $this->token = $token;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getEmail() {
        return $this->email;
    }
	public function setEmail($email) {
        $this->email = $email;
    }
	public function getPassword() {
        return $this->password;
    }
	public function setPassword($password) {
        $this->nrorg = $password;
    }
}