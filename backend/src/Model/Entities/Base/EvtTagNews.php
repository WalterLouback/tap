<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class EvtTagNews {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenTag  */
    protected $genTag;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenNews  */
    protected $genNews;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenTag() {
        return $this->genTag;
    }
	public function setGenTag(\Zeedhi\ApiGeneral\Model\Entities\GenTag $genTag = NULL) {
        $this->genTag = $genTag;
    }
	public function getGenNews() {
        return $this->genNews;
    }
	public function setGenNews(\Zeedhi\ApiGeneral\Model\Entities\GenNews $genNews = NULL) {
        $this->genNews = $genNews;
    }
}