<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenUserTypeRel {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $genUser;
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $externalId = 0;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUserType  */
    protected $genUserType;
    /** @var string  */
    protected $status;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var string  */
    protected $password;
    /** @var string  */
    protected $email;

    public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getExternalId() {
        return $this->externalId;
    }
	public function setExternalId($externalId) {
        $this->externalId = $externalId;
    }
	public function getGenUserType() {
        return $this->genUserType;
    }
	public function setGenUserType(\Zeedhi\ApiGeneral\Model\Entities\GenUserType $genUserType = NULL) {
        $this->genUserType = $genUserType;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
    public function getPassword() {
        return $this->password;
    }
	public function setPassword($password = NULL) {
        $this->password = $password;
    }
	public function getEmail() {
        return $this->email;
    }
	public function setEmail($email = NULL) {
        $this->email = $email;
    }
}