<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenOrganization {
    
    
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $nrorg = 0;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
    protected $nickName;
    
    protected $stoneEstablishmentId; 
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
    public function getNickName()
    {
        return $this->nickName;
    }

    public function setNickName($nickName = NULL)
    {
        $this->nickName = $nickName;
    }
    
    public function getStoneEstablishmentId()
    {
        return $this->stoneEstablishmentId;
    }

    public function setStoneEstablishmentId($stoneEstablishmentId= NULL)
    {
        $this->stoneEstablishmentId = $stoneEstablishmentId;
        return $this;
    }
}