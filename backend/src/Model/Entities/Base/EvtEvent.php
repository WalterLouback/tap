<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class EvtEvent {
    
    protected $about;

    protected $initialDate;

    protected $imageCover;

    protected $imageLogo;

    protected $imageMap;

    protected $name;

    protected $nrorg;

    protected $status;

    protected $type;

    protected $finalDate;

    protected $location;

    protected $classification;

    protected $rating;

    protected $salesMethod;

    protected $externalId;

    protected $value;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $parentEvent;

    protected $genCategory;

    protected $structure;

    protected $ownerUser; 
    
    protected $codeEvent;

    protected $numberSeqEvent;
    
    protected $latitude;
    
    protected $longitude;

    protected $urlApiExternal;

    protected $tokenApiExternal;

    protected $originApiExternal;

    protected $integrationCommands;

    protected $ipStore;

    protected $loggiApiKey;

    protected $loggiShopId;

    public function getAbout()
    {
        return $this->about;
    }

    public function setAbout($about)
    {
        $this->about = $about;
    }

    public function getInitialDate()
    {
        return $this->initialDate;
    }

    public function setInitialDate($initialDate)
    {
        $this->initialDate = $initialDate;
    }

    public function getImageCover()
    {
        return $this->imageCover;
    }

    public function setImageCover($imageCover)
    {
        $this->imageCover = $imageCover;
    }

    public function getImageLogo()
    {
        return $this->imageLogo;
    }

    public function setImageLogo($imageLogo)
    {
        $this->imageLogo = $imageLogo;
    }

    public function getImageMap()
    {
        return $this->imageMap;
    }

    public function setImageMap($imageMap)
    {
        $this->imageMap = $imageMap;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg)
    {
        $this->nrorg = $nrorg;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getFinalDate()
    {
        return $this->finalDate;
    }

    public function setFinalDate($finalDate)
    {
        $this->finalDate = $finalDate;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getClassification()
    {
        return $this->classification;
    }

    public function setClassification($classification)
    {
        $this->classification = $classification;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function getSalesMethod()
    {
        return $this->salesMethod;
    }

    public function setSalesMethod($salesMethod)
    {
        $this->salesMethod = $salesMethod;
    }

    public function getExternalId()
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId)
    {
        $this->externalId = $externalId;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt  = new \DateTime();
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParentEvent()
    {
        return $this->parentEvent;
    }

    public function setParentEvent(\Zeedhi\ApiGeneral\Model\Entities\EvtEvent $parentEvent)
    {
        $this->parentEvent = $parentEvent;
    }

    public function getGenCategory()
    {
        return $this->genCategory;
    }

    public function setGenCategory(\Zeedhi\ApiGeneral\Model\Entities\GenCategory $genCategory)
    {
        $this->genCategory = $genCategory;
    }

    public function getStructure()
    {
        return $this->structure;
    }

    public function setStructure(\Zeedhi\ApiGeneral\Model\Entities\GenStructure $structure)
    {
        $this->structure = $structure;
    }

    public function getOwnerUser()
    {
        return $this->ownerUser;
    }

    public function setOwnerUser(\Zeedhi\ApiGeneral\Model\Entities\GenUser $ownerUser)
    {
        $this->ownerUser = $ownerUser;
    }

    public function getCodeEvent()
    {
        return $this->codeEvent;
    }

    public function setCodeEvent($codeEvent)
    {
        $this->codeEvent = $codeEvent;
    }

    public function getNumberSeqEvent()
    {
        return $this->numberSeqEvent;
    }

    public function setNumberSeqEvent($numberSeqEvent)
    {
        $this->numberSeqEvent = $numberSeqEvent;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    public function getUrlApiExternal()
    {
        return $this->urlApiExternal;
    }

    public function setUrlApiExternal($urlApiExternal)
    {
        $this->urlApiExternal = $urlApiExternal;
    }

    public function getTokenApiExternal()
    {
        return $this->tokenApiExternal;
    }

    public function setTokenApiExternal($tokenApiExternal)
    {
        $this->tokenApiExternal = $tokenApiExternal;
    }

    public function getOriginApiExternal()
    {
        return $this->originApiExternal;
    }

    public function setOriginApiExternal($originApiExternal)
    {
        $this->originApiExternal = $originApiExternal;
    }

    public function getIntegrationCommands()
    {
        return $this->integrationCommands;
    }

    public function setIntegrationCommands($integrationCommands)
    {
        $this->integrationCommands = $integrationCommands;
    }

    public function getIpStore()
    {
        return $this->ipStore;
    }

    public function setIpStore($ipStore)
    {
        $this->ipStore = $ipStore;
    }
   
    public function getLoggiApiKey()
    {
        return $this->loggiApiKey;
    }

    public function setLoggiApiKey($loggiApiKey)
    {
        $this->loggiApiKey = $loggiApiKey;
    }

    public function getLoggiShopId()
    {
        return $this->loggiShopId;
    }

    public function setLoggiShopId($loggiShopId)
    {
        $this->loggiShopId = $loggiShopId;
    }
}