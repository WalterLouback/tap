<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenStatus extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenStatus {
    
    const PEDIDO_NOVO = 3;
    const PEDIDO_ACEITO = 4;
    const PEDIDO_PREPARADO = 5;
    const PEDIDO_ENTREGUE = 6;

    public function toArray() {
        return array(
            "BUTTON_NAME" => $this->getButtonName(),
            "LABEL_NAME" => $this->getLabelName()
        );
    }

    public function toString() {
        return $this->getLabelName();
    }

}