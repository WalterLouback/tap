<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenNotification extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenNotification {

    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'MESSAGE' => $this->getMessage(),
            'STATUS' => $this->getStatus(),
            'EXTERNAL_ID' => $this-getExternalId(),
            'CONFIRMATION' => $this->getConfirmation(),
            'TITLE' => $this->getTitle(),
            // 'NRORG' => $this->getNrorg(),
            // 'USER_ID' => $this->getGenUser()->getId(),
            'ORIGIN_NOTIFICATION' => $this->getOriginNotification(),
            'DATE_TIME' => $this->getDateTime()->format('Y-m-d')
        );
    }
}
