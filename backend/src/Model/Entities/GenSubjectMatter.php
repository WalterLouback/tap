<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenSubjectMatter extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenSubjectMatter {
    
    public function toArray() {
        return array(
            'id'                => $this->getId(),
            'subjectMatter'     => $this->getSubjectMatter(),
            'emailResponsible'  => $this->getEmailResponsible()
        );
    }
}