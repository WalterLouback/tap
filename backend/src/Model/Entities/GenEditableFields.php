<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenEditableFields extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenEditableFields {
    
    const NPF                    = 'NPF';
    const CPF                    = 'CPF';
    const LAST_NAME              = 'LAST_NAME';
    const FIRST_NAME             = 'FIRST_NAME';
    const BILLING_ADDRESS        = 'BILLING_ADDRESS';
    const CORRESPONDENCE_ADDRESS = 'CORRESPONDENCE_ADDRESS';
    
    public function build($entityManager) {
        $this->setPermissionName($entityManager);
    }

    public function getPermissionName() { 
        return property_exists($this, 'permission') ? $this->permission : "";
    }
    
    public function setPermissionName($entityManager) {
        $paramId = $this->getId();
        $permission = $entityManager->createQuery(
            "
            SELECT pe
            FROM '\Zeedhi\ApiGeneral\Model\Entities\GenPermission' pe
            JOIN '\Zeedhi\ApiGeneral\Model\Entities\GenEditableFields' pa WITH pe.id = pa.genPermission
            WHERE pa.id = $paramId
            "
        )->getResult();
        if (count($permission) > 0) {
            $this->permission = $permission[0]->getName();
        }
    }
    
    public static function manyToArray($params) {
        $array = [];
        foreach ($params as $param) {
            array_push($array, $param->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "FIELD_NAME" => $this->getFieldName(),
            "PERMISSION" => $this->getPermissionName(),
            "USER_TYPE"  => $this->getGenUserType()->getName(),
            "ID"         => $this->getId()
        );
    }
    
}