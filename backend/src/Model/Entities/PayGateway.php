<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class PayGateway extends \Zeedhi\ApiGeneral\Model\Entities\Base\PayGateway {

    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getGateway(),
            'MERCHANT_KEY' => $this->getMerchantKey(),
            'MERCHANT_ID' => $this->getMerchantId()
        );
    }
    
}