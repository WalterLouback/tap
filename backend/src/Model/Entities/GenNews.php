<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenNews extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenNews {
    
    public static function manyToArray($newsArray) {
        $arrays = [];
        foreach ($newsArray as $news) {
            array_push($arrays, $news->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'TITLE' => $this->getTitle(),
            'NEWS' => $this->getNews(),
            'NRORG' => $this->getNrorg(),
            'ACTIVE' => $this->getActive() == '1',
            'IMAGE' => $this->getImage(),
            'FILEURL' => $this->getImage(),
            'AUTHOR_ID' => $this->getGenUser() ? $this->getGenUser()->getId() : NULL
        );
    }

    
}