<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SerServiceSellerRel extends \Zeedhi\ApiGeneral\Model\Entities\Base\SerServiceSellerRel {
    
    public function toArray($date=null) {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "STATUS" => $this->getStatus(),
            "SERVICE_ID" => $this->getSerService() ? $this->getSerService()->getId() : "",
            "EVENT_SELLER_ID" => $this->getEvtEventSeller() ? $this->getEvtEventSeller()->getId() : "",
            "SELLER_FULL_NAME" => $this->getEvtEventSeller() ? trim($this->getEvtEventSeller()->getGenUser()->getFirstName()) . " " . trim($this->getEvtEventSeller()->getGenUser()->getLastName()) : "",
            "SELLER_IMAGE" => $this->getEvtEventSeller() ? $this->getEvtEventSeller()->getGenUser()->getImage() ? $this->getEvtEventSeller()->getGenUser()->getImage() : NULL : "",
            "SELLER_EMAIL" => $this->getEvtEventSeller() ? $this->getEvtEventSeller()->getGenUser()->getEmail() : "",
            "DAYS_AVAILABLE" => $date ? $this->daysOfMonth($date) : [],
            "FINAL_TIME" => $this->getFinalTime(),
            "INITIAL_TIME" => $this->getInitialTime()
        );
    }
    
    function daysOfMonth($date) {
        date_default_timezone_set('America/Sao_Paulo');
        $newDate = new \DateTime($date);
        $newDate->sub(new \DateInterval('P1D'));
        $numero = cal_days_in_month(CAL_GREGORIAN, $newDate->format('m'), $newDate->format('y'));

        $nextDays = array();
        
        for($i=0; $i<=$numero; $i++) {
            $nextDays[$i] = ['day' => $newDate->format('w'), 'date' => $newDate->add(new \DateInterval("P1D"))->format('Y-m-d'), 'hour' => [] ];
        }
        return $nextDays;
    }
}