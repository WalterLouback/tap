<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenUserTypeRel extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenUserTypeRel {
    
    const EXTERNAL_ID = 'EXTERNAL_ID';
    
    public function build($entityManager) {
        $this->setDependents($entityManager);
        $this->setWallet($entityManager);
        $this->setUser($entityManager);
        $this->setDependentOf($entityManager);
    }
    
    public function getDependents() {
        return property_exists($this, 'dependents') ? $this->dependents : [];
    }
    
    public function getWallet() {
        return property_exists($this, 'wallet') ? $this->wallet : NULL;
    }
    
    public function getUser() {
        return property_exists($this, 'user') ? $this->user : NULL;
    }
    
    public function getDependentOf() {
        return property_exists($this, 'dependentOf') ? $this->dependentOf : NULL;
    }
    
    public function setDependents($entityManager) {
        $parentId = $this->getGenUser()->getId();
        $this->dependents = $entityManager->createQuery(
            "
            SELECT d
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenDependent' d
            JOIN d.parent pr
            JOIN d.dependent dr
            WHERE pr.id = $parentId
            "
        )->getResult();
    }
    
    public function setWallet($entityManager) {
        $userTypeRelId = $this->getId();
        $this->wallet = $entityManager->getRepository(PayWallet::class)->findOneBy(['genUser' => $this->getGenUser()->getId(), 'nrorg' => $this->getNrorg()]);
    }
    
    public function setUser($entityManager) {
        $this->user = $entityManager->getRepository(GenUser::class)->find($this->getgenUser());
    }
    
    public function setDependentOf($entityManager) {
        $dependentId = $this->getId();
        
        $dependentOf = $entityManager->createQuery(
            "
            SELECT pu
            FROM 'Zeedhi\ApiGeneral\Model\Entities\GenUser' pu
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenDependent' d WITH d.parent = pu
            JOIN 'Zeedhi\ApiGeneral\Model\Entities\GenUser' pr WITH d.dependent = pr
            WHERE pr.id = $dependentId
            AND d.id > 0
            "
        )->getResult();
        if (count($dependentOf) > 0) {
            $dependentOf[0]->setCreditcards($entityManager);
            $this->dependentOf = $dependentOf[0];
        }
    }
    
    public static function manyToArray($userProfiles) {
        $arrays = [];
        foreach ($userProfiles as $userProfile) {
            array_push($arrays, $userProfile->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_ID' => $this->getGenUser()->getId(),
            'USER_TYPE' => $this->getGenUserType()->getName(),
            'EXTERNAL_ID' => $this->getExternalId(),
            'EMAIL' => $this->getEmail(),
            'NRORG' => $this->getNrorg(),
            'BALANCE' => $this->getWallet() != NULL ? $this->getWallet()->getBalance() : NULL,
            'DEPENDENT_OF' => $this->getDependentOf() != NULL ? $this->getDependentOf()->toArray() : NULL
        );
    }
    
}