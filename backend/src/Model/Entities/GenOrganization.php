<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


class GenOrganization extends \Zeedhi\ApiGeneral\Model\Entities\Base\GenOrganization {
    
    const NRORG = 'NRORG';
    
    public function getEditableFields() {
        return property_exists($this, 'editableFields') ? $this->editableFields : [];
    }
    
    public function setEditableFields($entityManager) {
        $nrorg = $this->getNrorg();
        $editableFields = $entityManager->createQuery(
            "
            SELECT p
            FROM '\Zeedhi\ApiGeneral\Model\Entities\GenEditableFields' p
            WHERE p.nrorg = $nrorg
            "
        )->getResult();
        
        foreach ($editableFields as $field) {
            $field->setPermissionName($entityManager);
        } 
        
        $this->editableFields = $editableFields;
    }
    
    public function toArray() {
        return array(
            'NRORG' => $this->getNrorg(),
            'NAME' => $this->getName(),
            'NICK_NAME' => $this->getNickName(),
            'EDITABLE_FIELDS' => GenEditableFields::manyToArray($this->getEditableFields())
        );
    }
    
    public function toArrayAdmin() {
        return array(
            'NRORG' => $this->getNrorg(),
            'NAME' => $this->getName(),
            'NICK_NAME' => $this->getNickName(),
            // 'EDITABLE_FIELDS' => GenEditableFields::manyToArray($this->getEditableFields())
        );
    }
    
}