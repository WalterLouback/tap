<?php
namespace Zeedhi\ApiGeneral\Helpers;

use Doctrine\ORM\EntityManager;

class FileUploader {

const FILES_PATH = __DIR__.'/../../../appMinasArquivos/files/';
    
    public function __construct($awsS3) {
        $this->awsS3 = $awsS3;
    }

    public function upload($file_object, $nrorg) {
        // if (!$base64_string) return '';
        // $output_file = '../../images/'.$this->generateName() . '.' . $this->getFileExtension($base64_string);

        // $ifp = fopen( $output_file, 'wb' ); 
        // $data = explode( ',', $base64_string );
        // fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        // fclose( $ifp ); 
    
        // $output_file = 'http://'.$_SERVER['SERVER_NAME'].'/workfolder/appCostumer' . explode('..', $output_file)[2];
        // return $output_file; 
        
        return $this->awsS3->uploadFile($file_object, $nrorg);
    }
    
    private function getFileExtension($file_object) {
        $r1 = explode( ';', $file_object )[ 0 ];
        $r2 = explode( '/', $r1 )[ 1 ];
        
        return $r2;
    }
    
    private function generateName() {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen;
        
            return $uuid;
        }
    }
    
    public function removeFile($fileName) {
        unlink(FileUploader::FILES_PATH . $fileName);
    }
}