<?php
namespace Zeedhi\ApiGeneral\Helpers;

use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;

use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;

class AWSS3 {
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    private function getAwsS3ConfigurationsFromOrganization($nrorg) {
        $genConfiguration = GenConfiguration::class;
        
        $data = $this->entityManager->createQuery(
            "
            SELECT c.awsS3AccessKey, c.awsS3SecretKey, c.awsS3Bucket
            FROM $genConfiguration c
            WHERE c.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($data) == 0) throw new \Exception('Organization not found');
        
        return [
            'awsS3AccessKey' => $data[0]['awsS3AccessKey'],
            'awsS3SecretKey' => $data[0]['awsS3SecretKey'],
            'awsS3Bucket'    => $data[0]['awsS3Bucket']
        ];
    }
    
    public function uploadImage($base64, $nrorg) {
        if ($base64 == NULL || $base64 == '' || empty($base64)) return '';
        
        $configurations = $this->getAwsS3ConfigurationsFromOrganization($nrorg);
        $this->init($configurations['awsS3AccessKey'], $configurations['awsS3SecretKey']);
        
        $image_parts = explode(";base64,", $base64);
        
        if(substr($image_parts[0], 0, 11) == "data:image/"){
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
    
            $result = $this->clientS3->putObject([
                'ACL' => 'public-read',
                'Body' => $image_base64,
                'Bucket' => $configurations['awsS3Bucket'],
                'Key' => $this->generateName(),
                'ContentType' => 'image/' . $image_type,
            ]);
        } else {
            throw new \Exception('Format is not accepted!', 234);
        }
        
        return $result['ObjectURL'];
    }
    
    public function uploadFile($file, $nrorg) {
        if(isset($file['file'])){
            $configurations = $this->getAwsS3ConfigurationsFromOrganization($nrorg);
            $this->init($configurations['awsS3AccessKey'], $configurations['awsS3SecretKey']);
        
    		$temp_file_location = $file['file']['tmp_name']; 
    		$file_name = $file['file']['name'];

            $result = $this->clientS3->putObject([
                'ACL' => 'public-read',
                'SourceFile' => $temp_file_location,
                'Bucket' => $configurations['awsS3Bucket'],
                'Key' => $this->generateName() . '.' . $this->parseurl($file_name)
            ]);

            return $result['ObjectURL'];
        } else {
            throw new \Exception('Erro!', 234);
        }
    }
    
    private function parseurl($url) {
        # takes the last dot it can find and grabs the text after it
        $extension = preg_replace("#(.+)?\.(\w+)(\?.+)?#", "$2", $url) . "\n";
        
        return $extension;
    }
    
    private function generateName() {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen;
        
            return $uuid;
        }
    }
    
    private function init($accessKey, $secretKey) {
        $this->clientS3 = S3Client::factory(array(
            'key'    => $accessKey,
            'secret' => $secretKey
        ));
    }

}