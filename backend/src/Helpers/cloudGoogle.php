<?php
namespace Zeedhi\ApiGeneral\Helpers;

class cloudGoogle {
    /**
     * na class que for usar, instancie
     * use Zeedhi\ApiGeneral\Helpers\cloudGoogle;
     * e use
     * $cloudGoogle = new cloudGoogle();
     * $cloudGoogle->GetGeoCode($street, $number, $neighborhood, $city, $state, $zipCode, $keyGoogle);
     **/
    public function GetGeoCode($street, $number, $neighborhood, $city, $state, $zipCode, $keyGoogle) {
        // exemplo: 
        /*
            REQUEST

            $street = "Sindicalista Lúcio Guterres";
            $number = "95";
            $neighborhood = "Chácara Cotia";
            $city = "Contagem";
            $state = "MG";
            $zipCode = "32183-080";
            $keyGoogle = "AIzaSyDYu5ZYnWTMaQaVGiNDSBmC4xIGEpw10M8";
        */
        
        /*
            RESPONSE

            array(2) {
              ["lat"]=>
              float(-19.8629696)
              ["long"]=>
              float(-44.0383087)
            }
        */

        $geo = array();
        $address =  $street .", " . $number . ", " . $neighborhood . ", " . $city . "-" . $state . ", " . $zipCode;

        $addr = str_replace(" ", "%", $address);
        $address = $this->smartURL($addr);
        // Daqui em diante é o seu código original
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' .$address .'&key=' .$keyGoogle.'&sensor=false&region=Brasil';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-type: application/x-www-form-urlencoded',
            'charset=UTF-8' )
        );
        $contents   = curl_exec($ch);
        
        if (curl_errno($ch)) {
            $error = "Error Number:".curl_errno($ch)." Error String:".curl_error($ch);
            $contents = '';
        } else {
            curl_close($ch);
        }
        
        if (!is_string($contents) || !strlen($contents)) {
            echo "Failed to get contents.";
        }
        
        $output = json_decode($contents);
        if(count($output->results) > 0) {
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
            $geo['lat']=$lat;
            $geo['long']=$long;
            return $geo;
        }else {
            return $contents;
        }
    }
    
    function smartURL($str) {
        $str = strtolower(utf8_decode($str)); $i=1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        
        return $str;
    }
}