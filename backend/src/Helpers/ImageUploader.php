<?php
namespace Zeedhi\ApiGeneral\Helpers;

use Doctrine\ORM\EntityManager;

class ImageUploader {

    const IMAGES_PATH = __DIR__.'/../../../appMinasImagens/images/';
    
    public function __construct($awsS3) {
        $this->awsS3 = $awsS3;
    }

    public function upload($base64_string, $nrorg) {
        // if (!$base64_string) return '';
        // $output_file = '../../images/'.$this->generateName() . '.' . $this->getFileExtension($base64_string);

        // $ifp = fopen( $output_file, 'wb' ); 
        // $data = explode( ',', $base64_string );
        // fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        // fclose( $ifp ); 
    
        // $output_file = 'http://'.$_SERVER['SERVER_NAME'].'/workfolder/appCostumer' . explode('..', $output_file)[2];
        // return $output_file; 
        
        return $this->awsS3->uploadImage($base64_string, $nrorg);
    }
    
    private function getFileExtension($base64_string) {
        $r1 = explode( ';', $base64_string )[ 0 ];
        $r2 = explode( '/', $r1 )[ 1 ];
        
        return $r2;
    }
    
    private function generateName() {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen;
        
            return $uuid;
        }
    }
    
    public function removeImage($imageName) {
        unlink(ImageUploader::IMAGES_PATH . $imageName);
    }

/*    public function upload($base64_string, $imageName) {
        if (!$base64_string) return '';
    
        $extension = $this->getFileExtension($base64_string, $imageName == NULL);
        if ($imageName != NULL) {
            $output_file = 'http://appimagens.minastc.com.br/images/' . $imageName . '.' . $extension;
        } else {
            $imageName = $this->generateName();
            $output_file = 'http://appimagens.minastc.com.br/images/' . $imageName . '.' . $extension;
        }
    
        $data = explode( ',', $base64_string );
        file_put_contents(ImageUploader::IMAGES_PATH.$imageName.'.'.$extension, base64_decode($data[1]));
    
        $output_file = explode('..', $output_file)[0];
    
        return $output_file;
    }

    public function removeImage($imageName) {
        unlink(ImageUploader::IMAGES_PATH . $imageName);
    }
    
    private function getFileExtension($base64_string) {
        $r1 = explode( ';', $base64_string )[ 0 ];
        $r2 = explode( '/', $r1 )[ 1 ];
    
        return $r2;
    }
    
    private function generateName() {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid =
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen;
    
            return $uuid;
        }
    }*/

}
