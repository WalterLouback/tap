if [ -d eventsbackend ]; 
then
    cd eventsbackend
    git fetch --all
    git pull origin master
    cd ..
else
    git clone https://bitbucket.org/tekfun/eventsbackend.git
fi
if [ -d orderbackend ]; 
then
    cd orderbackend
    git fetch --all
    git pull origin master
    cd ..
else
    git clone https://bitbucket.org/tekfun/orderbackend.git
fi
if [ -d paymentbackend ]; 
then
    cd paymentbackend
    git fetch --all
    git pull origin master
    cd ..
else
    git clone https://bitbucket.org/tekfun/paymentbackend.git
fi
if [ -d surveybackend ]; 
then
    cd surveybackend
    git fetch --all
    git pull origin master
    cd ..
else
    git clone https://bitbucket.org/tekfun/surveybackend.git
fi
if [ -d servicesbackend ]; 
then
    cd servicesbackend
    git fetch --all
    git pull origin master
    cd ..
else
    git clone https://bitbucket.org/tekfun/servicesbackend.git
fi
