<?php
namespace Zeedhi\ApiSurvey\Helpers;

use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;

class AWSS3 {
    
    const ACCESS_KEY = 'AKIARJQOSNSIMQHXCDDB';
    const SECRET_KEY = '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';
    const BUCKET     = 'documentos.minastc.com.br';
    
    public function uploadImage($base64) {
        $this->init();
        
        $image_parts = explode(";base64,", $base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $result = $this->clientS3->putObject([
            'ACL' => 'public-read',
            'Body' => $image_base64,
            'Bucket' => AWSS3::BUCKET,
            'Key' => $this->generateName(),
            'ContentType' => 'image/' . $image_type,
        ]);
        
        return $result['ObjectURL'];
    }
    
    private function generateName() {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        } else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen;
        
            return $uuid;
        }
    }
    
    private function init() {
        $this->clientS3 = S3Client::factory(array(
            'key'    => AWSS3::ACCESS_KEY,
            'secret' => AWSS3::SECRET_KEY
        ));
    }

}