<?php
namespace Zeedhi\ApiSurvey\Helpers;

use Zeedhi\Framework\Session\Session;

class Environment implements \Zeedhi\ApiSurvey\Service\Environment {

    const USER_ID_KEY = 'id';

    /** @var Session Service used to store user data */
    protected $session;

    /** @var string */
    protected $userId;

    /**
     * __construct
     * 
     * @param Session $session Service user to handle session data
     */
    public function __construct(Session $session) {
        $this->session = $session;
    }
    
    public function getUserId() {
        return $this->session->get(self::USER_ID_KEY);
    }
    
    public function setUserId($userId) {
        $this->session->set(self::USER_ID_KEY, $userId);
    }
}