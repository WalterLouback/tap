<?php

namespace Zeedhi\ApiSurvey\Helpers;


class RijndaelCrypt {
    protected static function getSalt() {
        return str_pad("BipFun", 16, "\0");
    }
    
    /**
     * Encrypt.
     *
     * @param string $message Text.
     *
     * @return string
     */
    public static function encrypt($message) {
        return base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                md5(self::getSalt()),
                $message,
                MCRYPT_MODE_CFB,
                "bipFunBIPFUNbfun"
            )
        );
    }

    /**
     * Decrypt.
     *
     * @param string $text Text.
     *
     * @return string
     */
    public static function decrypt($text) {
        return mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            md5(self::getSalt()),
            base64_decode($text),
            MCRYPT_MODE_CFB,
            "bipFunBIPFUNbfun"
        );
    }
}