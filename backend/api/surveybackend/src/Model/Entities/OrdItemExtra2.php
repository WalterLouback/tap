<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

class OrdItemExtra extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdItemExtra {
    
    public function build($entityManager) {
        $this->setSelectedOptions($entityManager);
    }
    
    public function getSelectedOptions() {
        return property_exists($this, 'selectedOptions') ? $this->selectedOptions : [];
    }
    
    public function setSelectedOptions($entityManager) {
        $selectedOptions = $entityManager->getRepository(OrdSelectedOption::class)->findBy(['ordItemExtra' => $this->getId()]);
        $this->selectedOptions = $selectedOptions;
    }
    
    public static function manyToArray($arrays) {
        $extras = [];
        foreach ($arrays as $itemExtra) {
            $array = [];
            $array['id'] = $itemExtra->getOrdExtra()->getId();
            $array['name'] = $itemExtra->getOrdExtra()->getName();
            $array['selectedOptions'] = OrdSelectedOption::manyToArray($itemExtra->getSelectedOptions());
            
            array_push($extras, $array);
        }
        return $extras;
    }
    
}