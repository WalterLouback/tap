<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class OrdProductGroup extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdProductGroup {
    
    public function build($entityManager) {
        $this->setProducts($entityManager);
    }
    
    public function getProducts() {
        return property_exists($this, 'products') ? $this->products : [];
    }
    
    public function setProducts($entityManager) {
        $product = 'Zeedhi\ApiOrders\Model\Entities\OrdProduct';
        
        $id = $this->getId();
        
        $menuProducts = $entityManager->createQuery(
            "
            SELECT mp
            FROM $product p
            JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct mp WITH mp.ordProduct = p
            JOIN mp.ordProductGroup as pg
            JOIN mp.ordProduct as p2
            WHERE pg.id = $id
            ORDER BY p.name
            "
        )->getResult();
        
        foreach ($menuProducts as $menuProduct) {
            $menuProduct->build($entityManager);
        }
        
        $this->products = $menuProducts;
    }

    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['products'] = OrdProduct::manyToArray($this->getProducts());
        
        return $array;
    }
    
    public static function manyToArray($productGroups) {
        $arrays = [];
        foreach ($productGroups as $productGroup) {
            array_push($arrays, $productGroup->toArray());
        }
        return $arrays;
    }
    
}