<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class GenEditableFields extends \Zeedhi\ApiSurvey\Model\Entities\Base\GenEditableFields {
    
    public function getPermissionName() {
        return property_exists($this, 'permission') ? $this->permission : "";
    }
    
    public function setPermissionName($entityManager) {
        $paramId = $this->getId();
        $permission = $entityManager->createQuery(
            "
            SELECT pe
            FROM '\Zeedhi\ApiSurvey\Model\Entities\GenPermission' pe
            JOIN '\Zeedhi\ApiSurvey\Model\Entities\GenEditableFields' pa WITH pe.id = pa.genPermission
            WHERE pa.id = $paramId
            "
        )->getResult();
        if (count($permission) > 0) {
            $this->permission = $permission[0]->getName();
        }
    }
    
    public static function manyToArray($params) {
        $array = [];
        foreach ($params as $param) {
            array_push($array, $param->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "FIELD_NAME" => $this->getFieldName(),
            "PERMISSION" => $this->getPermissionName()
        );
    }
    
}