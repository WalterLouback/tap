<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class OrdOrder extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdOrder {

    const STATUS_PAYMENT_PENDING  = 'P';
    const STATUS_PAID             = 'A';
    const STATUS_CANCELED         = 'C';
    const STATUS_NOT_AUTHORIZED   = 'N';

    const PAYMENT_METHOD_CREDITCARD            = 'CC';
    const PAYMENT_METHOD_BALANCE               = 'BALANCE';
    const PAYMENT_METHOD_EXTERNAL_CC           = 'EXT_CC';
    const PAYMENT_METHOD_EXTERNAL_DC           = 'EXT_DC';
    const PAYMENT_METHOD_EXTERNAL_CASH         = 'EXT_CASH';
    const PAYMENT_METHOD_CREDITCARD_FOR_WALLET = 'CC_W';
    
    const DELIVER_TO_TABLE   = 'T';
    const DELIVER_TO_BALCONY = 'B';
    
    const ORDER_ITEMS    = 'ORDER_ITEMS';
    const TOTAL_VALUE    = 'TOTAL_VALUE';
    const DELIVER_TO     = 'DELIVER_TO';
    const PAYMENT_METHOD = 'PAYMENT_METHOD';

    
    const EVT_EVENT_SELLER_ID = 'EVT_EVENT_SELLER_ID';
    const WALLET_ID           = 'WALLET_ID';
    const COSTUMER_ID         = 'COSTUMER_ID';
    const USER_ID             = 'USER_ID';
    const ORDER_ID            = 'ORDER_ID';
    const ORDER_TYPE          = 'ORDER_TYPE';
    const TYPE                = 'TYPE';
    const NRORG               = 'NRORG';
    const INITIAL_DATE        = 'INITIAL_DATE';
    const FINAL_DATE          = 'FINAL_DATE';

    
    public function build($entityManager) {
        $this->setItems($entityManager);
    }
    
    public function toArray() {
        $array = [];
        
        $array['id'] = $this->getId();
        $array['createDate'] = $this->getCreatedAt();
        $array['deliverTo'] = $this->getDeliverTo();
        $array['structure'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getName() : NULL;
        $array['paymentMethod'] = $this->getPaymentMethod();
        $array['orderStatus'] = $this->getStatus() != NULL ? $this->getStatus()->toString() : NULL;
        $array['paymentStatus'] = $this->getPaymentStatus();
        $array['total'] = $this->getTotal();
        $array['nrorg'] = $this->getNrorg();
        $array['orderType'] = $this->getType();
        $array['creditCardId'] = $this->getPayCreditcard() ? $this->getPayCreditcard()->getId() : NULL;
        $array['eventId'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['walletId'] = $this->getPayWallet() != NULL ? $this->getPayWallet()->getId() : NULL;
        $array['evtEventSellerId'] = $this->getEvtEventSeller() != NULL ? $this->getEvtEventSeller()->getId() : NULL;
        $array['eventName'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getName() : NULL;
        $array['userId'] = $this->getGenUser() != NULL ? $this->getGenUser()->getId() : NULL;
        $array['items'] = OrdOrderProduct::manyToArray($this->getItems());
        
        return $array;
    }
    
    public function getItems() {
        return property_exists($this, 'items') ? $this->items : [];
    }

    public function setItems($entityManager) {
        $items = $entityManager->getRepository(OrdOrderProduct::class)->findBy(['ordOrder' => $this->getId()]);
        foreach ($items as $item) {
            $item->build($entityManager);
        }

        $this->items = $items;
    }
    
    public static function manyToArray($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public static function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        
            return $uuid;
        }
    }
    
}