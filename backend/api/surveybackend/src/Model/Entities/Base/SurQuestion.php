<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class SurQuestion {
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $note;
    /** @var string  */
    protected $type;
    /** @var int */
    protected $hasComment;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $limitQuantity;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\SurSurvey  */
    protected $surSurvey;
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getName() {
        return $this->name;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getType() {
        return $this->type;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getStatus() {
        return $this->status;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getLimitQuantity() {
        return $this->limitQuantity;
    }
    public function setLimitQuantity($limitQuantity) {
        $this->limitQuantity = $limitQuantity;
    }
    public function getHasComment() {
        return $this->hasComment;
    }
    public function setHasComment($hasComment) {
        $this->hasComment = $hasComment;
    }
    public function getNote() {
        return $this->note;
    }
    public function setNote($note) {
        $this->note = $note;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiSurvey\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiSurvey\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getSurSurvey() {
        return $this->surSurvey;
    }
	public function setSurSurvey(\Zeedhi\ApiSurvey\Model\Entities\SurSurvey $surSurvey = NULL) {
        $this->surSurvey = $surSurvey;
    }
}