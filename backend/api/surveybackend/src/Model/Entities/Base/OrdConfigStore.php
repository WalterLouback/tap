<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class OrdConfigStore {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $deliversToTable;
    /** @var string  */
    protected $deliversToBalcony;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\EvtEvent  */
    protected $event;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\OrdWorkflow  */
    protected $workflow;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getDeliversToTable() {
        return $this->deliversToTable;
    }
	public function setDeliversToTable($deliversToTable) {
        $this->deliversToTable = $deliversToTable;
    }
    public function getDeliversToBalcony() {
        return $this->deliversToBalcony;
    }
	public function setDeliversToBalcony($deliversToBalcony) {
        $this->deliversToBalcony = $deliversToBalcony;
    }
	public function getEvent() {
        return $this->event;
    }
	public function setEvent(\Zeedhi\ApiSurvey\Model\Entities\EvtEvent $event = NULL) {
        $this->event = $event;
    }
	public function getWorkflow() {
        return $this->workflow;
    }
	public function setWorkflow(\Zeedhi\ApiSurvey\Model\Entities\OrdWorkflow $workflow = NULL) {
        $this->workflow = $workflow;
    }
}