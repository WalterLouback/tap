<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class SurAnswerOption {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $type;
    /** @var int  */
    protected $nrorg;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getName() {
        return $this->name;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getType() {
        return $this->type;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
}