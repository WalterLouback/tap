<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class PayWallet {
    
    
    /** @var string  */
    protected $id;
    /** @var int  */
    protected $balance;
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getBalance() {
        return $this->balance;
    }
	public function setBalance($balance = NULL) {
        $this->balance = $balance;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiSurvey\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}