<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class SurSurvey {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $description;
    /** @var int  */
    protected $nrorg;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Datetime  */
    protected $initialDate;
    /** @var \Datetime  */
    protected $finalDate;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getDescription() {
        return $this->description;
    }
	public function setDescription($description = NULL) {
        $this->description = $description;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getInitialDate() {
        return $this->initialDate;
    }
	public function setInitialDate(\Datetime $initialDate = NULL) {
        $this->initialDate = $initialDate;
    }
    public function getFinalDate() {
        return $this->finalDate;
    }
	public function setFinalDate(\Datetime $finalDate = NULL) {
        $this->finalDate = $finalDate;
    }
}