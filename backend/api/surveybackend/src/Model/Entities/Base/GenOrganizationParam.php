<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class GenOrganizationParam {
    
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $uEditPhone;
    /** @var string  */
    protected $uEditAddress;
    /** @var string  */
    protected $uEditPersData;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getUEditPhone() {
        return $this->uEditPhone;
    }
	public function setUEditPhone($uEditPhone) {
        $this->uEditPhone = $uEditPhone;
    }
	public function getUEditAddress() {
        return $this->uEditAddress;
    }
	public function setUEditAddress($uEditAddress) {
        $this->uEditAddress = $uEditAddress;
    }
	public function getUEditPersData() {
        return $this->uEditPersData;
    }
	public function setUEditPersData($uEditPersData) {
        $this->uEditPersData = $uEditPersData;
    }
}