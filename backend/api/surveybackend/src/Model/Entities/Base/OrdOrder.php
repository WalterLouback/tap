<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class OrdOrder {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $id;
    /** @var int  */
    protected $total;
    /** @var string  */
    protected $deliverTo;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $transactionId;
    /** @var string  */
    protected $paymentStatus;
    /** @var string  */
    protected $paymentMethod;
    /** @var string  */
    protected $orderKey;
    /** @var \Datetime  */
    protected $createDate;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\EvtEventSeller  */
    protected $evtEventSeller;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\PayWallet  */
    protected $payWallet;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\PayCreditcard  */
    protected $payCreditcard;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenStatus  */
    protected $status;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getDeliverTo() {
        return $this->deliverTo;
    }
	public function setDeliverTo($deliverTo) {
        $this->deliverTo = $deliverTo;
    }

	public function getType() {
        return $this->type;
    }
	public function setType($type) {
        $this->type = $type;
    }
	public function getTransactionId() {
        return $this->transactionId;
    }
	public function setTransactionId($transactionId) {
        $this->transactionId = $transactionId;
    }
	public function getPaymentStatus() {
        return $this->paymentStatus;
    }
	public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }
	public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
    }
	public function getOrderKey() {
        return $this->orderKey;
    }
	public function setOrderKey($orderKey = NULL) {
        $this->orderKey = $orderKey;
    }
	public function getCreateDate() {
        return $this->createDate;
    }
	public function setCreateDate(\Datetime $createDate) {
        $this->createDate = $createDate;
	}
	public function getStatus() {
        return $this->status;
    }
	public function setStatus(\Zeedhi\ApiSurvey\Model\Entities\GenStatus $status = NULL) {
        $this->status = $status;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiSurvey\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEventSeller() {
        return $this->evtEventSeller;
    }
	public function setEvtEventSeller(\Zeedhi\ApiSurvey\Model\Entities\EvtEventSeller $evtEventSeller = NULL) {
        $this->evtEventSeller = $evtEventSeller;
    }
	public function getPayWallet() {
        return $this->payWallet;
    }
	public function setPayWallet(\Zeedhi\ApiSurvey\Model\Entities\PayWallet $payWallet = NULL) {
        $this->payWallet = $payWallet;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiSurvey\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getPayCreditcard() {
        return $this->payCreditcard;
    }
	public function setPayCreditcard(\Zeedhi\ApiSurvey\Model\Entities\PayCreditcard $payCreditcard = NULL) {
        $this->payCreditcard = $payCreditcard;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiSurvey\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
}