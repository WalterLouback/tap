<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class GenNews {
    
    
    /** @var string  */
    protected $active;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $news;
    /** @var string  */
    protected $image;
    /** @var string  */
    protected $title;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getActive() {
        return $this->active;
    }
	public function setActive($active = NULL) {
        $this->active = $active;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getNews() {
        return $this->news;
    }
	public function setNews($news = NULL) {
        $this->news = $news;
    }
	public function getImage() {
        return $this->image;
    }
	public function setImage($image = NULL) {
        $this->image = $image;
    }
	public function getTitle() {
        return $this->title;
    }
	public function setTitle($title) {
        $this->title = $title;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiSurvey\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}