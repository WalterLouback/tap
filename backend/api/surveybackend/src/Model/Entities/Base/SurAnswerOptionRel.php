<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class SurAnswerOptionRel {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $type;
    /** @var int  */
    protected $nrorg;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption  */
    protected $surAnswerOption;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\SurQuestion  */
    protected $surQuestion;
    
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getSurAnswerOption() {
        return $this->surAnswerOption;
    }
	public function setSurAnswerOption(\Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption $surAnswerOption = NULL) {
        $this->surAnswerOption = $surAnswerOption;
    }
	public function getSurQuestion() {
        return $this->surQuestion;
    }
	public function setSurQuestion(\Zeedhi\ApiSurvey\Model\Entities\SurQuestion $surQuestion = NULL) {
        $this->surQuestion = $surQuestion;
    }
}