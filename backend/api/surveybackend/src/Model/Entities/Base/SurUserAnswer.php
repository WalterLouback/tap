<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class SurUserAnswer {
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $answer;
    /** @var string  */
    protected $comment;
    /** @var string  */
    protected $note;
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption  */
    protected $surAnswerOption;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\SurQuestion  */
    protected $surQuestion;
    
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenUser  */
    protected $genUser;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getAnswer() {
        return $this->answer;
    }
	public function setAnswer($answer = NULL) {
        $this->answer = $answer;
    }
	public function getComment() {
        return $this->comment;
    }
	public function setComment($comment = NULL) {
        $this->comment = $comment;
    }
	public function getNote() {
        return $this->note;
    }
	public function setNote($note = NULL) {
        $this->note = $note;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiSurvey\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getSurAnswerOption() {
        return $this->surAnswerOption;
    }
	public function setSurAnswerOption(\Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption $surAnswerOption = NULL) {
        $this->surAnswerOption = $surAnswerOption;
    }
	public function getSurQuestion() {
        return $this->surQuestion;
    }
	public function setSurQuestion(\Zeedhi\ApiSurvey\Model\Entities\SurQuestion $surQuestion = NULL) {
        $this->surQuestion = $surQuestion;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    
}