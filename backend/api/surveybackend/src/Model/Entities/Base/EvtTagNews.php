<?php
namespace Zeedhi\ApiSurvey\Model\Entities\Base;


abstract class EvtTagNews {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenTag  */
    protected $evtTag;
    /** @var \Zeedhi\ApiSurvey\Model\Entities\GenNews  */
    protected $genNews;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtTag() {
        return $this->evtTag;
    }
	public function setEvtTag(\Zeedhi\ApiSurvey\Model\Entities\GenTag $evtTag = NULL) {
        $this->evtTag = $evtTag;
    }
	public function getGenNews() {
        return $this->genNews;
    }
	public function setGenNews(\Zeedhi\ApiSurvey\Model\Entities\GenNews $genNews = NULL) {
        $this->genNews = $genNews;
    }
}