<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

use Zeedhi\ApiSurvey\Helpers\General as General;

class EvtEvent extends \Zeedhi\ApiSurvey\Model\Entities\Base\EvtEvent {
    
    const EVENT_ID = 'EVENT_ID';

    public function build($entityManager) {
        $this->setMenus($entityManager);
        $this->setPaymentMethods($entityManager);
        $this->setDeliversToTable($entityManager);
        $this->setDeliversToBalcony($entityManager);
        $this->setPaymentMoment($entityManager);
        $this->isOpened($entityManager);
    }
    
    public function getMenus() {
        return property_exists($this, 'menus') ? $this->menus : [];
    }
    
    public function getPaymentMethods() {
        return property_exists($this, 'paymentMethods') ? $this->paymentMethods : [];
    }
    
    public function getDeliversToTable() {
        return property_exists($this, 'deliversToTable') ? $this->deliversToTable : 'F';
    }
    
    public function getDeliversToBalcony() {
        return property_exists($this, 'deliversToBalcony') ? $this->deliversToBalcony : 'F';
    }

    public function getPaymentMoment() {
        return property_exists($this, 'paymentMoment') ? $this->paymentMoment : NULL;
    }

    public function getMerchantKey() {
        return property_exists($this, 'merchantKey') ? $this->merchantKey : NULL;
    }
    
    public function getShiftsOfDay($date, $entityManager) {
        $workshift = 'Zeedhi\ApiSurvey\Model\Entities\GenShift';
        $storeId   = $this->getId();
        
        $dayOfWeek = strftime("%A", strtotime($date));

        $daysOfWeeksArray = array(
            'Sunday'    => 0, 'Monday' => 1, 'Tuesday'   => 2, 'Wednesday' => 3, 
            'Thursday'  => 4, 'Friday' => 5, 'Saturday'  => 6
        );

        $formatedDay = $daysOfWeeksArray[$dayOfWeek];
        
        $dayWorkshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime 
            FROM $workshift ws
            WHERE ws.evtEvent = $storeId
            AND ws.day = $formatedDay
            "
        )->getResult();
        
        return $dayWorkshifts;
    }
    
    public function isOpened($entityManager=NULL) {
        if (!$entityManager) {
            if (property_exists($this, 'opened')) return $this->opened;
            else return NULL;
        }
        
        $opened      = false;
        
        /* Obter status da loja para verificar se ela foi fechada ou aberta manualmente */
        $storeStatus = $this->getStatus();
        
        if ($storeStatus == 'O') {
            $opened  = true;
        }
        else if ($storeStatus != 'C') {
            
            /* Obter horários de funcionamento da loja no dia de hoje */
            $today        = new \DateTime();
            $formatedDate = $today->format('Y-m-d H:i:s');
            $shifts       = $this->getShiftsOfDay($formatedDate, $entityManager);
            
            /* Percorrer os turnos e verificar se horário atual se encaixa em algum */
            foreach ($shifts as $shift) {
                /* Obtendo horário atual */
                $currHours = $today->format('H');
                $currMins  = $today->format('i');
                /* Obtendo horário de início do turno */
                $inDate    = $shift['initialTime'];
                $inHours   = $inDate->format('H');
                $inMins    = $inDate->format('i');
                /* Obtendo horário de término do turno */
                $finDate   = $shift['finalTime'];
                $finHours  = $finDate->format('H');
                $finMins   = $finDate->format('i');
                
                /* Verificando se horário atual se encaixa no turno */
                if ($currHours > $inHours || ($currHours == $inHours && $currMins >= $inMins)) {
                    if ($currHours < $finHours || ($currHours == $finHours && $currMins <= $finMins)) {
                        $opened = true;
                        break;
                    }
                }
            }
        }
        
        $this->opened = $opened;
        
        return $opened;
    }
    
    public function setMenus($entityManager) {
        $menu      = 'Zeedhi\ApiSurvey\Model\Entities\OrdMenu';
        $eventMenu = 'Zeedhi\ApiSurvey\Model\Entities\EvtEventMenu';
        $storeId = $this->getId();
        $menus   = $entityManager->createQuery(
            "
            SELECT m 
            FROM $menu m
            JOIN $eventMenu em WITH em.ordMenu = m
            JOIN em.evtEvent as s 
            WHERE s.id = $storeId 
            ORDER BY m.name
            "
        )->getResult();
        
        foreach ($menus as $menu) {
            $menu->build($entityManager);
        }
        
        $this->menus = $menus;
    }
    
    public function setPaymentMethods($entityManager) {
        $storeId = $this->getId();
        $this->paymentMethods = $entityManager->createQuery(
            "
            SELECT pm
            FROM '\Zeedhi\ApiSurvey\Model\Entities\PayPaymentMethod' pm
            JOIN '\Zeedhi\ApiSurvey\Model\Entities\OrdConfigStore' c WITH c.event = $storeId
            JOIN '\Zeedhi\ApiSurvey\Model\Entities\OrdStorePayMethod' spm WITH pm.id = spm.paymentMethod
            "    
        )->getResult();
    }
    
    public function setDeliversToTable($entityManager) {
        $storeId = $this->getId();
        $this->deliversToTable = $entityManager->createQuery(
            "
            SELECT c.deliversToTable
            FROM 'Zeedhi\ApiSurvey\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->deliversToTable) > 0) $this->deliversToTable = $this->deliversToTable[0]['deliversToTable'];
    }
    
    public function setDeliversToBalcony($entityManager) {
        $storeId = $this->getId();
        $deliversToBalcony = $entityManager->createQuery(
            "
            SELECT c.deliversToBalcony
            FROM 'Zeedhi\ApiSurvey\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($deliversToBalcony) > 0) $this->deliversToBalcony = $deliversToBalcony[0]['deliversToBalcony'];
    }
    
    public function setPaymentMoment($entityManager) {
        $storeId       = $this->getId();
        $paymentMoment = $entityManager->createQuery(
            "
            SELECT pm
            FROM 'Zeedhi\ApiSurvey\Model\Entities\OrdConfigStore' c
            JOIN 'Zeedhi\ApiSurvey\Model\Entities\OrdWorkflow' w WITH c.workflow = w.id
            JOIN 'Zeedhi\ApiSurvey\Model\Entities\GenStatus' pm WITH w.paymentMoment = pm.id
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($paymentMoment) > 0) $this->paymentMoment = $paymentMoment[0];
    }
    
    public function setMerchantKey($entityManager) {
        $storeId = $this->getId();
        $merchantKey = $entityManager->createQuery(
            "
            SELECT g.merchantKey
            FROM 'Zeedhi\ApiSurvey\Model\Entities\PayGateway' g
            JOIN 'Zeedhi\ApiSurvey\Model\Entities\PayEventGatewayRel' gr WITH g.id = gr.payGateway
            WHERE gr.evtEvent = $storeId
            "
        )->getResult();
        if (count($merchantKey) > 0) $this->merchantKey = $merchantKey[0]['merchantKey'];
    }
    
    public function toArray() {
        $array = [];
        // $array['i/*d'] = $this->getId();
        // $array['about'] = $this->getAbout();
        // $array['address'] = $this->getAddress();
        // $array['initialDate'] = $this->getInitialDate();*/
        $array['id'] = $this->getId();
        $array['about'] = $this->getAbout();
        $array['initialDate'] = $this->getInitialDate();
        $array['finalDate'] = $this->getFinalDate();
        $array['imageCover'] = $this->getImageCover();
        $array['imageLogo'] = $this->getImageLogo();
        $array['imageMap'] = $this->getImageMap();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['status'] = $this->getStatus();
        $array['type'] = $this->getType();
        $array['structureId'] = $this->getStructure() != NULL ? $this->getStructure()->getId() : NULL;
        $array['parentEventId'] = $this->getParentEvent() != NULL ? $this->getParentEvent()->getId() : NULL;
        //$array['opened'] = (bool) $this->isOpened();
        
        if ($this->getMenus()) $array['menus'] = OrdMenu::manyToArray($this->getMenus());
        //if ($this->getPaymentMethods()) $array['paymentMethods'] = OrdProduct::manyToArray($this->getPaymentMethods());
        //if ($this->getPaymentMoment()) $array['paymentMoment'] = $this->getPaymentMoment()->toString();
        //if ($this->getDeliversToTable()) $array['deliversToTable'] = $this->getDeliversToTable() == 'T';
        //if ($this->getDeliversToBalcony()) $array['deliversToBalcony'] = $this->getDeliversToBalcony() == 'T';
        return $array;
    }
    
    public static function manyToArray($events) {
        $arrays = [];
        foreach ($events as $event) {
            $array = $event->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
}