<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class PayCreditcard extends \Zeedhi\ApiSurvey\Model\Entities\Base\PayCreditcard {
    
    const CREDIT_CARD_ID = 'CREDIT_CARD_ID';
    const FROM_USER = 1;
    const FROM_PARENT = 2;
    const FROM_OTHER = 0;
    
    public static function manyToArray($creditCards) {
        $arrays = [];
        foreach ($creditCards as $creditCard) {
            array_push($arrays, $creditCard->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'expirationDate' => $this->getExpirationDate(),
            'flag' => $this->getFlag(),
            'lastNumbers' => $this->getLastNumbers()
        );
    }
    
         
    public function getReceiptsToParent($entityManager, $nrorg) {
        $dependency = $entityManager->getRepository(GenDependent::class)->findOneBy(['dependent' => $this->getId(), 'nrorg' => $nrorg]);
        $parent     = $dependency->getParent();
        $cardFromParent = $parent->getMainCreditcard()->getId();
        return $dependent->getReceiptsTo();
    }
}