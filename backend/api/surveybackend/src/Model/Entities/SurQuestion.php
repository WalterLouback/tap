<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

class SurQuestion extends \Zeedhi\ApiSurvey\Model\Entities\Base\SurQuestion {
    
    public function build($entityManager) {
        $this->setAnswers($entityManager);
    }
    
    public function toArray() {
        $array = [];
        
        $array['id']            = $this->getId();
        $array['name']          = $this->getName();
        $array['type']          = $this->getType();
        $array['hasComment']    = $this->getHasComment();
        $array['status']        = $this->getStatus();
        $array['note']          = $this->getNote();
        $array['nrorg']         = $this->getNrorg();
        $array['createdAt']     = $this->getCreatedAt();
        // $array['answers']       = SurAnswerOptionRel::manyToArray($this->getAnswers());
        $array['evtEvent']      = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['genStructure']  = $this->getGenStructure() != NULL ? $this->getGenStructure()->getId() : NULL;
        
        return $array;
    }
    
    public function getAnswers() {
        return property_exists($this, 'answers') ? $this->answers : [];
    }

    public function setAnswers($entityManager) {
        $answers = $entityManager->getRepository(SurAnswerOptionRel::class)->findBy(['surQuestion' => $this->getId()]);
      
        foreach ($answers as $answer) {
            $answer->build($entityManager);
        }

        $this->Answers = $answers;
    }
    
    public static function manyToArray($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }    

}