<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class GenPermission extends \Zeedhi\ApiSurvey\Model\Entities\Base\GenPermission {
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}