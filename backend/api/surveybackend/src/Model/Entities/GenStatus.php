<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class GenStatus extends \Zeedhi\ApiSurvey\Model\Entities\Base\GenStatus {
    
    const PEDIDO_NOVO = 8;
    const PEDIDO_ENTREGUE = 9;

    public function toArray() {
        return array(
            "BUTTON_NAME" => $this->getButtonName(),
            "LABEL_NAME" => $this->getLabelName()
        );
    }

    public function toString() {
        return $this->getLabelName();
    }

}