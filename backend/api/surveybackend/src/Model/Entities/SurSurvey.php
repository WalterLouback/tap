<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

class SurSurvey extends \Zeedhi\ApiSurvey\Model\Entities\Base\SurSurvey {
    
    public function build($entityManager) {
        $this->setQuestions($entityManager);
    }
    
    public function toArray() {
        $array = [];
        
        $array['id']          = $this->getId();
        $array['name']        = $this->getName();
        $array['status']      = $this->getStatus();
        $array['description'] = $this->getDescription();
        $array['nrorg']       = $this->getNrorg();
        $array['createdAt']   = $this->getCreatedAt();
        $array['initialDate'] = $this->getInitialDate();
        $array['finalDate']   = $this->getFinalDate();
        
        return $array;
    }
    
    public function getQuestions() {
        return property_exists($this, 'questions') ? $this->questions : [];
    }

    public function setQuestions($entityManager) {
        $questions = $entityManager->getRepository(SurQuestion::class)->findBy(['surSurvey' => $this->getId()]);
        foreach ($questions as $question) {
            $question->build($entityManager);
        }

        $this->questions = $questions;
    }
    
    public static function manyToArray($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }

}