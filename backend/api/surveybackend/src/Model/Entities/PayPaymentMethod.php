<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class PayPaymentMethod extends \Zeedhi\ApiSurvey\Model\Entities\Base\PayPaymentMethod {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "PAYMENT_METHOD" => $this->getPaymentMethod(),
            "LABEL" => $this->getLabel()
        );
    }

    
}