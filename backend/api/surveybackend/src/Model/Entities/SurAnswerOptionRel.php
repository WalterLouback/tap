<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

class SurAnswerOptionRel extends \Zeedhi\ApiSurvey\Model\Entities\Base\SurAnswerOptionRel {
    public function build($entityManager) {
        $this->setAnswers($entityManager);
    }
    
    public function getAnswerOptionRels() {
        return property_exists($this, 'answerOptionRels') ? $this->answerOptionRels : [];
    }

    public function setAnswerOptionRels($entityManager) {
        $answerOptionRels = $entityManager->getRepository(SurAnswerOptionRel::class)->findBy(['surAnswer' => $this->getId()]);
        foreach ($answerOptionRels as $answerOptionRel) {
            $answerOptionRel->build($entityManager);
        }

        $this->Answers = $answerOptionRels;
    }
    
    public static function manyToArray($answerOptions) {
        $arrays = [];
        foreach ($answerOptions as $answerOption) {
            $array = $answerOption->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array = [];
        
        $array['id']                = $this->getId();
        $array['surAnswerOption']   = $this->getSurAnswerOption()->getId();
        $array['surQuestion']       = $this->getSurQuestion()->getId();
        
        return $array;
    }
    
    public function getAnswers() {
        return property_exists($this, 'answers') ? $this->answers : [];
    }

    public function setAnswers($entityManager) {
        $answers = $entityManager->getRepository(SurAnswerOptionRel::class)->findBy(['surQuestion' => $this->getId()]);
      
        foreach ($answers as $answer) {
            $answer->build($entityManager);
        }

        $this->Answers = $answers;
    }
}