<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

use Zeedhi\ApiSurvey\Helpers\General as General;

class OrdOption extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdOption {
    
    public static function manyToArray($options) {
        $arrays = [];
        foreach ($options as $option) {
            $array = $option->toArray();
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['price'] = $this->getPrice();
        
        return $array;
    }
    
}