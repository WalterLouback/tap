<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class OrdOrderProduct extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdOrderProduct {
    
    public function build($entityManager) {
        // $this->setExtras($entityManager);
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getOrdMenuProduct() != NULL ? $this->getOrdMenuProduct()->getName() : NULL;
        $array['unitaryPrice'] = $this->getOrdMenuProduct() != NULL ? $this->getOrdMenuProduct()->getPrice() : NULL;
        $array['quantity'] = $this->getQuantity();
        $array['remaining'] = $this->getQuantity();
        // $array['extras'] = OrdItemExtra::manyToArray($this->getExtras());
        $array['total'] = $this->getTotal();
        
        return $array;
    }
    
    public static function manyToArray($items) {
        $arrays = [];
        foreach ($items as $item) {
            $array = $item->toArray();
            array_push($arrays, $array);
        }
        return $arrays;
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    /* Devolve os extras dos itens enviados por parâmetro */
    function setExtras($entityManager) {
        $item   = 'Zeedhi\ApiSurvey\Model\Entities\OrdOrderProduct';
        $itemExtra = 'Zeedhi\ApiSurvey\Model\Entities\OrdItemExtra';
        $extra  = 'Zeedhi\ApiSurvey\Model\Entities\OrdExtra';
        $selectedOption = 'Zeedhi\ApiSurvey\Model\Entities\OrdSelectedOption';
        $option = 'Zeedhi\ApiSurvey\Model\Entities\OrdOption';
            
        $id = $this->getId();
            
        /* Realizando Query 
           o  = Pedido
           po = Item do Pedido
           e  = Extra
           ie = Extra do Item do Pedido
           op = Option
           so = Selected Option
        */
        $extras = $entityManager->createQuery(
            "
            SELECT ie
            FROM $itemExtra ie
            JOIN ie.ordExtra as e 
            JOIN ie.ordOrderProduct as i
            JOIN i.ordOrder as o
            WHERE i.id = $id
            ORDER BY e.id
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
            
        $this->extras = $extras;
    }

    
}