<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

use Zeedhi\ApiSurvey\Helpers\General as General;

class OrdExtra extends \Zeedhi\ApiSurvey\Model\Entities\Base\OrdExtra {
    
    public function build($entityManager) {
        $this->setOptions($entityManager);
    }
    
    public static function manyToArray($extras) {
        $arrays = [];
        foreach ($extras as $extra) {
            array_push($arrays, $extra->toArray());
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['required'] = $this->getRequired();
        $array['multiple'] = $this->getMultiple();
        $array['productId'] = $this->getOrdProduct() != NULL ? $this->getOrdProduct()->getId() : NULL;
        $array['options'] = OrdOption::manyToArray($this->getOptions());
        
        return $array;
    }
    
    public function getOptions() {
        return property_exists($this, 'options') ? $this->options : [];
    }
    
    public function setOptions($entityManager) {
        $item   = 'Zeedhi\ApiSurvey\Model\Entities\OrdOrderProduct';
        $extra  = 'Zeedhi\ApiSurvey\Model\Entities\OrdExtra';
        $option = 'Zeedhi\ApiSurvey\Model\Entities\OrdOption';
        $extras = [];
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $queryResult = $entityManager->createQuery(
            "
            SELECT op
            FROM $option op
            JOIN op.ordExtra e
            WHERE e.id = $id
            ORDER BY op.id
            "
        )->getResult();
        
        $this->options = $queryResult;
    }
    
}