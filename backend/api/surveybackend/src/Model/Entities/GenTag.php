<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class GenTag extends \Zeedhi\ApiSurvey\Model\Entities\Base\GenTag {
    
    public static function manyToArray($tags) {
        $arrays = [];
        foreach ($tags as $tag) {
            array_push($arrays, $tag->toArray());
        };
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getName(),
            'TYPE' => $this->getType(),
            'CATEGORY' => $this->getCategory()->getName(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}