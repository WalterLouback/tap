<?php
namespace Zeedhi\ApiSurvey\Model\Entities;

class SurAnswerOption extends \Zeedhi\ApiSurvey\Model\Entities\Base\SurAnswerOption {

    public static function manyToArray($answerOptions) {
        $arrays = [];
        foreach ($answerOptions as $answerOption) {
            $array = $answerOption->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array = [];
        
        $array['id']          = $this->getId();
        $array['name']        = $this->getName();
        $array['status']      = $this->getStatus();
        $array['type']        = $this->getType();
        $array['nrorg']       = $this->getNrorg();
        $array['createdAt']   = $this->getCreatedAt();
        $array['modifieldAt'] = $this->getModifiedAt();
        
        return $array;
    }
}