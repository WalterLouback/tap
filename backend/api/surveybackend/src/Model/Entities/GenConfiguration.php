<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class GenConfiguration extends \Zeedhi\ApiSurvey\Model\Entities\Base\GenConfiguration {
    
    public function toArray() {
        return array(
            'LOGO_IMAGE' => $this->getLogoImagE()
        );
    }
    
}