<?php
namespace Zeedhi\ApiSurvey\Model\Entities;


class PayGatewayRel extends \Zeedhi\ApiSurvey\Model\Entities\Base\PayGatewayRel {
    
    public static function getMerchantKeyByOrganization($nrorg, $entityManager) {

        $payGateway = '\Zeedhi\ApiSurvey\Model\Entities\PayGateway';
        $payGatewayRel = '\Zeedhi\ApiSurvey\Model\Entities\PayGatewayRel';
        $genConfiguration = 'Zeedhi\ApiSurvey\Model\Entities\GenConfiguration';

        $merchantKey = $entityManager->createQuery(
            "
            SELECT pg.merchantKey 
            FROM $genConfiguration gc 
            JOIN $payGatewayRel pgr WITH gc.id = pgr.genConfiguration
            JOIN $payGateway pg WITH pg.id = pgr.payGateway
            WHERE gc.nrorg = $nrorg 
            "
        )->getResult();

        return $merchantKey;
    }
    
    
}