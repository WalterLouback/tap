<?php
namespace Zeedhi\ApiSurvey\Service;

use Zeedhi\ApiSurvey\Model\Entities\SurSurvey;
use Zeedhi\ApiSurvey\Model\Entities\SurQuestion;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption;
use Zeedhi\ApiSurvey\Model\Entities\SurUserAnswer;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOptionRel;
use Zeedhi\ApiSurvey\Model\Entities\EvtEvent;
use Zeedhi\ApiSurvey\Model\Entities\GenStructure;
use Zeedhi\ApiSurvey\Model\Entities\GenUser;
use Zeedhi\ApiSurvey\Util\Query;

use Zeedhi\ApiSurvey\Helpers\FCM;

use Doctrine\ORM\EntityManager;

class Survey extends UserOperation {

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }
    
    public function getSurveys($nrorg, $userId) {
        $surSurvey          = SurSurvey::class;
        $surUserAnswer      = SurUserAnswer::class;
        $surQuestion        = SurQuestion::class;
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        
          
        $currentDateOld = new \DateTime();
        $currentDate    = $currentDateOld->format('Y-m-d');
        $dateQuery     = "AND survey.initialDate <= '$currentDate 23:59:55' AND survey.finalDate >= '$currentDate 01:01:05'";
        
        $surveys = $this->getEntityManager()->createQuery(
            "
            SELECT survey.id, survey.name, survey.description, survey.createdAt, survey.status, survey.initialDate, survey.finalDate
            FROM $surSurvey survey
            WHERE survey.status = 'A' AND survey.id NOT IN (
                SELECT s.id
                FROM $surSurvey s
                LEFT JOIN $surQuestion q WITH q.surSurvey = s
                LEFT JOIN $surUserAnswer ua WITH ua.surQuestion = q
                WHERE ua.genUser = $userId
            )
            $dateQuery
            ORDER by survey.id DESC
            "
        )->getResult();

        return $surveys;
    }

    public function submitAnswer($questionId, $userId, $answer = null, $answerOptionId = null, $comment = null, $nrorg = null) {
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['id' => $userId]);
        
        $surUserAnswer = new SurUserAnswer();
        
        $surUserAnswer->setSurQuestion($this->getEntityManager()->getReference(SurQuestion::class, $questionId));
        $surUserAnswer->setGenUser($genUser);
        if($answerOptionId != null) $surUserAnswer->setSurAnswerOption($this->getEntityManager()->getReference(SurAnswerOption::class, $answerOptionId));
        $surUserAnswer->setComment($comment);
        $surUserAnswer->setAnswer($answer);
        $surUserAnswer->setNrorg($nrorg ? $nrorg : null);
        
        $this->getEntityManager()->persist($surUserAnswer);
        $this->getEntityManager()->flush();
    }

    public function getSurveyBySurveyId($surveyId) {
        $surSurvey          = $this->getEntityManager()->getRepository(SurSurvey::class)->find($surveyId);
        
        return $surSurvey;
    }
    
    public function createOrUpdateSurvey($surveyId = null, $userId = null, $name = null, $status = null, $description = null, $nrorg = null, $initialDate = null, $finalDate = null) {
        $surSurvey   = $surveyId ? $this->getEntityManager()->getRepository(SurSurvey::class)->find($surveyId) : new SurSurvey();
        
        $oldStatus   = NULL;
        if($name)
            $surSurvey->setName($name);
        if($description)
            $surSurvey->setDescription($description);
        if($nrorg)
            $surSurvey->setNrorg($nrorg);
        if($surveyId != NULL)
            $surSurvey->setModifiedBy($userId);
        if($surveyId == NULL)
            $surSurvey->setCreatedBy($surveyId ? $surSurvey->getCreatedBy() : $userId);
        if($initialDate)
            $surSurvey->setInitialDate($initialDate);
        if($finalDate)
            $surSurvey->setFinalDate($finalDate);
        if($status) {
            $oldStatus = $surSurvey->getStatus();
            $surSurvey->setStatus($status);
        }
        
        $this->getEntityManager()->persist($surSurvey);
        $this->getEntityManager()->flush();
        
        try {
            if ($oldStatus != 'A' && $status == 'A') {
                $fcm = new FCM($nrorg, $this->getEntityManager());
                $fcm->sendNotificationToAllUsers(
                    "Queremos ouvir a sua opinião", 
                    "Responda a pesquisa de satisfação disponível no app.",
                    ["screen" => "notifications"]
                );
            }
        } catch (\Exception $e) { };

        return $surSurvey;
    }
    
    public function inactiveSurvey($surveyId, $userId, $status) {
        $surSurvey = $this->getSurveyBySurveyId($surveyId);
        
        $oldStatus = $surSurvey->getStatus();
        $surSurvey->setStatus($status);
        $surSurvey->setModifiedBy($userId);
        
        try {
            if ($oldStatus != 'A' && $status == 'A') {
                $fcm = new FCM($surSurvey->getNrorg(), $this->getEntityManager());
                $fcm->sendNotificationToAllUsers(
                    "Queremos ouvir a sua opinião", 
                    "Responda a pesquisa de satisfação disponível no app.",
                    ["screen" => "notifications"]
                );
            }
        } catch (\Exception $e) { };
        
        $this->getEntityManager()->persist($surSurvey);
        $this->getEntityManager()->flush();
    }
    
    public function getSurveysFromOrganization($nrorg, $status = null, $initialDate = null, $finalDate = null) {
        $surSurvey = SurSurvey::class;
        
        $initialDate    = $initialDate ? $initialDate->format('Y-m-d H:i:s'): NULL;
        $finalDate      = $finalDate ? $finalDate->format('Y-m-d H:i:s'): NULL;
        $status         = $status ? "'" . join($status, "' OR ss.status = '") . "'" : NULL;
        
        $queryStatus        = $status ? " AND (ss.status = $status)" : "";
        $queryInitialDate   = $initialDate ? " AND ss.initialDate   >= '$initialDate' " : "";
        $queryFinalDate     = $finalDate ? " AND ss.finalDate       >= '$finalDate' " : "";
        
        $surveys = $this->getEntityManager()->createQuery("
            SELECT ss
            FROM $surSurvey ss
            WHERE ss.nrorg = $nrorg
            $queryStatus
            $queryInitialDate
            $queryFinalDate
        ")->getResult();
        
        return SurSurvey::manyToArray($surveys);
    }
    
    public function getQuestions($surveyId) {
        $surQuestion = SurQuestion::class;
        $questionIds = $this->getEntityManager()->createQuery("
            SELECT surQuestion.id, surQuestion.name, surQuestion.type   
            FROM $surQuestion surQuestion
            WHERE surQuestion.surSurvey = $surveyId
        ")->getResult();
        
        return $questionIds;         
    }
    
    public function totalOfPeopleAnsweredSurvey($surveyId) {
        $surSurvey          = SurSurvey::class;
        $surUserAnswer      = SurUserAnswer::class;
        $surQuestion        = SurQuestion::class;
        $genUser = GenUser::class;
        
        $totalOfPeopleAnsweredSurvey = $this->getEntityManager()->createQuery("
            SELECT genUser.id
            FROM $surSurvey surSurvey
            JOIN $surQuestion surQuestion WITH surQuestion.surSurvey = surSurvey.id
            JOIN $surUserAnswer surUserAnswer WITH  surUserAnswer.surQuestion = surQuestion.id
            JOIN $genUser genUser WITH   genUser.id = surUserAnswer.genUser
            WHERE surSurvey.id = $surveyId
            GROUP BY surUserAnswer.genUser 
        ")->getResult();
        
        $total = count($totalOfPeopleAnsweredSurvey);
                
        return $total;        
    }
    
    public function numberOfAnswersOption($optionId) {
        $surSurvey          = SurSurvey::class;
        $surUserAnswer      = SurUserAnswer::class;
        $surQuestion        = SurQuestion::class;
        $surAnswerOptionRel = SurAnswerOptionRel::class;

        $totalOfPeopleAnsweredSurvey = $this->getEntityManager()->createQuery("
            SELECT count(surUserAnswer.surAnswerOption)
            FROM $surUserAnswer surUserAnswer
            WHERE surUserAnswer.surAnswerOption = $optionId
        ")->getResult();
        
        $total = count($totalOfPeopleAnsweredSurvey);
                
        return $totalOfPeopleAnsweredSurvey;           
    }
    
    public function getAnswerOptionsFromQuestion($questionId) {
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        $surAnswerOption    = SurAnswerOption::class;
        $surQuestion        = SurQuestion::class;
        
        $surAnswerOptions   = $this->getEntityManager()->createQuery(
            "
            SELECT  sao.id, sao.name, sao.status, sao.type, sq.id as question
            FROM  $surAnswerOption sao
            LEFT JOIN $surAnswerOptionRel saor WITH sao.id = saor.surAnswerOption
            LEFT JOIN $surQuestion sq WITH sq.id = saor.surQuestion
            WHERE sq.id = '$questionId'
            GROUP BY  sao.id
            ORDER BY sao.id ASC
            ")->getResult();
        foreach($surAnswerOptions as $key => $surAnswerOption) {
            $surAnswerOptions[$key]['total'] = $this->numberOfAnswersOption($surAnswerOption['id']);     
        var_dump($surAnswerOptions[$key]['total']); die;
            // var_dump($surAnswerOptions); die;
        }
            
        return $surAnswerOptions;
    }
    
    public function getQuestionReport($surveyId) {

    	$param = array('SUR_SURVEY_ID' => $surveyId);
		try {
			$questionReport = $this->getEntityManager()->getConnection()->fetchAll("
        SELECT
          S3.SUR_SURVEY_ID AS SURVEY_ID,
          S3.ID AS QUESTION_ID,
          S2.NAME AS OPTION_NAME,
          S2.ID AS OPTION_ID,
          S3.NAME AS QUESTION_NAME,
          COUNT(S1.SUR_ANSWER_OPTION_ID) as TOTAL,
          TOTAL.TOTAL_RESPOSTAS,
          ROUND((COUNT(S1.SUR_ANSWER_OPTION_ID) / TOTAL.TOTAL_RESPOSTAS * 100), 2) AS PORCENTAGEM
        FROM SUR_USER_ANSWER S1,
             SUR_QUESTION S3,
             SUR_ANSWER_OPTION S2,
             (SELECT
               COUNT(S1.SUR_ANSWER_OPTION_ID) TOTAL_RESPOSTAS,
               S1.SUR_QUESTION_ID,
               S1.NRORG
             FROM SUR_USER_ANSWER S1
             GROUP BY S1.SUR_QUESTION_ID,
                      S1.NRORG) TOTAL
        WHERE S1.SUR_ANSWER_OPTION_ID = S2.ID
        AND S1.NRORG = S2.NRORG
        AND S1.SUR_QUESTION_ID = TOTAL.SUR_QUESTION_ID
        AND S1.NRORG = TOTAL.NRORG
        AND S3.ID = S1.SUR_QUESTION_ID
        AND S3.NRORG = S1.NRORG
        AND S3.SUR_SURVEY_ID = :SUR_SURVEY_ID
        GROUP BY S2.NAME,
                 S3.NAME,
                 S3.ID
        ORDER BY S3.ID
    ", $param);
			
		} catch (\Exception $e) {
			throw $e;
			
		}
    	return $questionReport;
    }   
    
    public function getSurveysAnswered($nrorg, $status, $initialDate, $finalDate) {
        
    }
    
}
?>