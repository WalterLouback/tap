<?php
namespace Zeedhi\ApiSurvey\Service;

use Doctrine\ORM\EntityManager;
use Zeedhi\ApiSurvey\Model\Entities\BpUser;

use Facebook\Facebook;

abstract class UserOperation {

    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;

    /**
     * UserOperation constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * @return Environment
     */
    protected function getEnvironment() {
        return $this->environment;
    }

    /**
     * @return BpUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function getCurrentUser() {
        return $this->entityManager->find(BpUser::class, $this->environment->getUserId());
    }
}