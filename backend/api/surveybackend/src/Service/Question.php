<?php
namespace Zeedhi\ApiSurvey\Service;

use Zeedhi\ApiSurvey\Model\Entities\SurSurvey;
use Zeedhi\ApiSurvey\Model\Entities\SurQuestion;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption;
use Zeedhi\ApiSurvey\Model\Entities\SurUserAnswer;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOptionRel;
use Zeedhi\ApiSurvey\Model\Entities\EvtEvent;
use Zeedhi\ApiSurvey\Model\Entities\GenStructure;
use Zeedhi\ApiSurvey\Model\Entities\GenUser;


use Doctrine\ORM\EntityManager;

class Question extends UserOperation {

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }

    public function getSurQuestion($surveyId){
        $surSurvey          = SurSurvey::class;
        $surUserAnswer      = SurUserAnswer::class;
        $surQuestion        = SurQuestion::class;
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        $evtEvent           = EvtEvent::class;
        $genStructure       = GenStructure::class;
        
        $surQuestions = $this->getEntityManager()->createQuery(
            "
            SELECT surQuestion.id, surQuestion.name as question, surQuestion.limitQuantity as limit,surQuestion.type,  surQuestion.status, surQuestion.hasComment, surQuestion.note, event.id as eventId, event.name as eventName, structure.id as structureId, structure.name as structureName
            FROM $surQuestion surQuestion
            LEFT JOIN $genStructure structure WITH surQuestion.genStructure = structure.id
            LEFT JOIN $evtEvent event WITH surQuestion.evtEvent = event.id
            WHERE surQuestion.surSurvey = $surveyId
            "
        )->getResult();
        return $surQuestions;
    }    

    public function getAnswersByQuestionId($questionId){
        $surSurvey          = SurSurvey::class;
        $surUserAnswer      = SurUserAnswer::class;
        $surQuestion        = SurQuestion::class;
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        $surAnswerOption    = SurAnswerOption::class;
        
        $surQuestions = $this->getEntityManager()->createQuery(
            "
            SELECT surAnswerOption.id, surAnswerOption.name as answer, surAnswerOption.status, surAnswerOption.type
            FROM $surAnswerOptionRel surAnswerOptionRel
            LEFT JOIN $surAnswerOption surAnswerOption WITH surAnswerOptionRel.surAnswerOption = surAnswerOption.id
            WHERE surAnswerOptionRel.surQuestion = $questionId
            "
        )->getResult();
        
        return $surQuestions;
    }
    
    public function createOrUpdateQuestion($questionId = null, $surveyId = null, $userId = null, $status = null, $type = null, $nrorg = null, $note = null, $hasComment = null, $limitQuantity = null, $evtEventId = null, $genStructureId = null, $name = null) {
        $surQuestion   = $questionId ? $this->getEntityManager()->getRepository(SurQuestion::class)->find($questionId) : new SurQuestion();
        
        if($status)
            $surQuestion->setStatus($status);
        if($type)
            $surQuestion->setType($type);
        if($nrorg)
            $surQuestion->setNrorg($nrorg);
        if($questionId == NULL)
            $surQuestion->setCreatedBy($userId);
        if($questionId != NULL)
            $surQuestion->setModifiedBy($userId);
        if($name)
            $surQuestion->setName($name);
        if($note)
            $surQuestion->setNote($note);
        if($hasComment)
            $surQuestion->setHasComment($hasComment);
        if($limitQuantity)
            $surQuestion->setLimitQuantity($limitQuantity);
        if($evtEventId)
            $surQuestion->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $evtEventId));
        if($genStructureId)
            $surQuestion->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $genStructureId));
        if($surveyId)
            $surQuestion->setSurSurvey($this->getEntityManager()->getReference(SurSurvey::class, $surveyId));
        
        $this->getEntityManager()->persist($surQuestion);
        $this->getEntityManager()->flush();
        
        return $surQuestion;
    }
    
    public function getQuestionsFromSurvey($nrorg, $status, $surveyId) {
        $surQuestions = $this->getEntityManager()->getRepository(SurQuestion::class)->findBy(['nrorg' => $nrorg, 'status' => $status, 'surSurvey' => $surveyId]);
        
        return SurQuestion::manyToArray($surQuestions);
    }

}
?>