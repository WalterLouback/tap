<?php
namespace Zeedhi\ApiSurvey\Service;

use Zeedhi\ApiSurvey\Model\Entities\SurQuestion;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOption;
use Zeedhi\ApiSurvey\Model\Entities\SurAnswerOptionRel;


use Doctrine\ORM\EntityManager;

class AnswerOption extends UserOperation {

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }

    public function createOrUpdateAnswerOption($answerSurveyId = null, $userId = null, $name = null, $status = null, $type = null, $nrorg = null, $questionId = null, $typeRate = null) {
        $surAnswerSurvey   = $answerSurveyId ? $this->getEntityManager()->getRepository(SurAnswerOption::class)->find($answerSurveyId) : new SurAnswerOption();
        
        if($name)
            $surAnswerSurvey->setName($name);
        if($status)
            $surAnswerSurvey->setStatus($status);
        if($type)
            $surAnswerSurvey->setType($type);
        if($nrorg)
            $surAnswerSurvey->setNrorg($nrorg);
        if($answerSurveyId == NULL)
            $surAnswerSurvey->setCreatedBy($userId);
        if($answerSurveyId != NULL)
            $surAnswerSurvey->setModifiedBy($userId);
        
        $this->getEntityManager()->persist($surAnswerSurvey);
        
        if($typeRate == NULL) {
            if($answerSurveyId == null && $questionId != null) {
                $surAnswerOptionRel = new SurAnswerOptionRel();
                $surAnswerOptionRel->setSurQuestion($this->getEntityManager()->getReference(SurQuestion::class, $questionId));
                $surAnswerOptionRel->setSurAnswerOption($surAnswerSurvey);
                $surAnswerOptionRel->setNrorg($nrorg ? $nrorg : null);
                
                $this->getEntityManager()->persist($surAnswerOptionRel);
            }
        }
        
        $this->getEntityManager()->flush();
        
        return $surAnswerSurvey;
    }
    
    public function createRalationAnswerOptionAndQuestion($answerOptionId, $questionId, $nrorg) {
        $surAnswerOptionRel = new SurAnswerOptionRel();
        
        $surAnswerOptionRel->setSurQuestion($this->getEntityManager()->getReference(SurQuestion::class, $questionId));
        $surAnswerOptionRel->setSurAnswerOption($this->getEntityManager()->getReference(SurAnswerOption::class, $answerOptionId));
        $surAnswerOptionRel->setNrorg($nrorg);
        
        $this->getEntityManager()->persist($surAnswerOptionRel);
    }
    
    public function commit(){
        $this->getEntityManager()->flush();
    }
    
    public function getAnswerOptionFromOrganization($nrorg, $status) {
        $surAnswerOption = SurAnswerOption::class;
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        $surQuestion = SurQuestion::class;
        
        $surAnswerOptions = $this->getEntityManager()->createQuery(
            "
            SELECT  sao.id, sao.name, sao.status, sao.type, sao.type, sq.id as question
            FROM  $surAnswerOption sao
            LEFT JOIN $surAnswerOptionRel saor WITH sao.id = saor.surAnswerOption
            LEFT JOIN $surQuestion sq WITH sq.id = saor.surQuestion
            WHERE sao.nrorg = '$nrorg' AND sao.status = '$status'
            GROUP BY  sao.id
            ORDER BY sao.id ASC
            ")->getResult();
            
        return $surAnswerOptions;
    }
    
    public function getAnswerOptionsFromQuestion($nrorg, $status, $questionId) {
        $surAnswerOptionRel = SurAnswerOptionRel::class;
        $surAnswerOption = SurAnswerOption::class;
        $surQuestion = SurQuestion::class;
        
        $surAnswerOptions = $this->getEntityManager()->createQuery(
            "
            SELECT  sao.id, sao.name, sao.status, sao.type, sao.type, sq.id as question
            FROM  $surAnswerOption sao
            LEFT JOIN $surAnswerOptionRel saor WITH sao.id = saor.surAnswerOption
            LEFT JOIN $surQuestion sq WITH sq.id = saor.surQuestion
            WHERE sao.nrorg = '$nrorg' AND sao.status = '$status' AND sq.id = '$questionId'
            GROUP BY  sao.id
            ORDER BY sao.id ASC
            ")->getResult();
            
        return $surAnswerOptions;
    }
    
    public function getAnswerOptionRel($questionId) {
        $relations = $this->getEntityManager()->getRepository(SurAnswerOptionRel::class)->findBy(['surQuestion' => $questionId]);
        
        return $relations;
    }
    
    public function deleteRelations($answerOptionRelId) {
        $answerOptionRel = $this->getEntityManager()->getRepository(SurAnswerOptionRel::class)->find($answerOptionRelId);
        
        $this->getEntityManager()->remove($answerOptionRel);
        
        $this->getEntityManager()->flush();
    }
}
?>