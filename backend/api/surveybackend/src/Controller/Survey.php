<?php
namespace Zeedhi\ApiSurvey\Controller;

use Zeedhi\ApiSurvey\Service\Survey as SurveyService;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiSurvey\Service\Environment;

class Survey extends Crud {

    protected $dataSourceName = 'survey';
    
    const NRORG         = 'NRORG';
    const USER_ID       = 'USER_ID';
    const EVENT_ID      = 'EVENT_ID';
    const STRUCTURE_ID  = 'STRUCTURE_ID';
    const QUESTIONS     = 'QUESTIONS';
    const QUESTION_ID   = 'QUESTION_ID';
    const TAG           = 'TAG';
    const SURVEY_ID     = 'SURVEY_ID';
    const CATEGORY_ID   = 'CATEGORY_ID';
    const CATEGORIES    = 'CATEGORIES';
    const TAGS          = 'TAGS';
    const STRUCTURES    = 'STRUCTURES';

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param service $surveyService
     */
    public function __construct(Manager $dataSourceManager,  SurveyService $surveyService) {
        parent::__construct($dataSourceManager);
        $this->surveyService = $surveyService;
    }
    
    /** 
     * getSurveys
     * Obtêm pesquisas ativas que o usuário não respondeu. 
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @version 1.0
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function getSurveys(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        $nrorg    = $row[self::NRORG];
        $userId   = $row[self::USER_ID];

        $surveys  = $this->surveyService->getSurveys($nrorg, $userId);

        $response->addDataSet(new DataSet('surveys', $surveys));
    }
    
    /**
     * submitAnswers
     * Cria as respostas para a pesquisa.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @version 1.0
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function submitAnswers(DTO\Request\Row $request, DTO\Response $response) {
        $row              = $request->getRow();
        $surveyId         = $row[self::SURVEY_ID];
        $userId           = $row[self::USER_ID];
        $answerQuestions  = $row['SURVEY_ANSWERS']['questions'];
        $nrorg            = $row['NRORG'];
        
        foreach ($answerQuestions as $answerQuestion) {
            $answerOptionIds  = $answerQuestion['type'] != 'textarea' ? $answerQuestion['answer'] : NULL;
            $answer           = $answerQuestion['type'] == 'textarea' ? $answerQuestion['answer'] : NULL;
            $questionid       = $answerQuestion['id'];
            $comment          = $answerQuestion['comment'];
            
            if($answerQuestion['type'] == 'textarea'){
                $this->surveyService->submitAnswer($questionid, $userId, $answer, $answerOptionIds, $comment, $nrorg);
            } else {
                if(!is_array($answerOptionIds)) $answerOptionIds = [$answerOptionIds];
                foreach ($answerOptionIds as $answerOptionId) {
                    $this->surveyService->submitAnswer($questionid, $userId, $answer, $answerOptionId, $comment, $nrorg);
                }
            }
        } 

        $response->addDataSet(new DataSet('submitAnswers', ['status' => 'ok']));
    }
    
    /**
     * createOrUpdateSurvey
     * Cria uma pesquisa.
     * @author Cristiano Santana
     * @version 1.0
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function createOrUpdateSurvey(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $surveyId       = isset($row[self::SURVEY_ID])  ? $row[self::SURVEY_ID]                     : NULL ;
        $userId         = isset($row[self::USER_ID])    ? $row[self::USER_ID]                       : NULL ;
        $name           = isset($row['NAME'])           ? $row['NAME']                              : NULL ;
        $status         = isset($row['STATUS'])         ? $row['STATUS']                            : NULL ;
        $description    = isset($row['DESCRIPTION'])    ? $row['DESCRIPTION']                       : NULL ;
        $nrorg          = isset($row['NRORG'])          ? $row['NRORG']                             : NULL ;
        $initialDate    = isset($row['INITIAL_DATE'])   ? $this->stringToDate($row['INITIAL_DATE']) : NULL ;
        $finalDate      = isset($row['FINAL_DATE'])     ? $this->stringToDate($row['FINAL_DATE'])   : NULL ;
        
        $survey         = $this->surveyService->createOrUpdateSurvey($surveyId, $userId, $name, $status, $description, $nrorg, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok', 'surveyId' => $survey->getId()]));
    }
    
    public function updateStatusOfSurvey(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $surveyId       = isset($row[self::SURVEY_ID])  ? $row[self::SURVEY_ID] : NULL ;
        $userId         = isset($row[self::USER_ID])    ? $row[self::USER_ID]   : NULL ;
        $status         = $row['STATUS'];
        
        $this->surveyService->inactiveSurvey($surveyId, $userId, $status);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    
    public function getSurveysFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        if((isset($row['INITIAL_DATE']) && !empty($row['INITIAL_DATE'])) && (isset($row['FINAL_DATE']) && !empty($row['FINAL_DATE']))) {
            $initialDateOld = strlen($row['INITIAL_DATE']) > 11 ? substr($row['INITIAL_DATE'], 0, -9) : $row['INITIAL_DATE'];
            $finalDateOld   = strlen($row['FINAL_DATE']) > 11 ? substr($row['FINAL_DATE'], 0, -9) : $row['FINAL_DATE'];
        }
        
        $nrorg          = isset($row['NRORG'])  && !empty($row['NRORG'])    ? $row['NRORG']  : NULL ;
        $status         = isset($row['STATUS']) && !empty($row['STATUS'])   ? $row['STATUS'] : NULL ;
        $initialDate    = isset($row['INITIAL_DATE']) && !empty($row['INITIAL_DATE'])   ? $this->stringToDate($initialDateOld."00:59:00") : NULL ;
        $finalDate      = isset($row['FINAL_DATE']) && !empty($row['FINAL_DATE'])       ? $this->stringToDate($finalDateOld."23:59:00")   : NULL ;
        
        $surveys        = $this->surveyService->getSurveysFromOrganization($nrorg, $status, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet('surSurveysData', $surveys));
    }
    
    public function inactiveSurvey(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $surveyId       = isset($row[self::SURVEY_ID])  ? $row[self::SURVEY_ID] : NULL ;
        $userId         = isset($row[self::USER_ID])    ? $row[self::USER_ID]   : NULL ;
        $status         = "I";
        
        $this->surveyService->inactiveSurvey($surveyId, $userId, $status);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }
    
    public function totalOfPeoplesAnsweredSurvey(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $surveyId       = $row[self::SURVEY_ID];
        
        $total = $this->surveyService->totalOfPeopleAnsweredSurvey($surveyId);
        
        $response->addDataSet(new DataSet('totalOfPeoplesAnsweredSurvey', [$total]));
    }
    
    public function questionReport(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $surveyId       = $row[self::SURVEY_ID];
        
        $questionReport = $this->surveyService->getQuestionReport($surveyId);
        
        $response->addDataSet(new DataSet('questionReport', [$questionReport]));
    }

}   