<?php
namespace Zeedhi\ApiSurvey\Controller;

use Zeedhi\ApiSurvey\Service\AnswerOption as AnswerOptionService;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiSurvey\Service\Environment;

class AnswerOption extends Crud {

    protected $dataSourceName = 'answerOption';
    
    const NRORG         = 'NRORG';
    const USER_ID       = 'USER_ID';
    const EVENT_ID      = 'EVENT_ID';
    const STRUCTURE_ID  = 'STRUCTURE_ID';
    const QUESTIONS     = 'QUESTIONS';
    const TAG           = 'TAG';
    const SURVEY_ID     = 'SURVEY_ID';
    const CATEGORY_ID   = 'CATEGORY_ID';
    const CATEGORIES    = 'CATEGORIES';
    const TAGS          = 'TAGS';
    const STRUCTURES    = 'STRUCTURES';

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param service $answerOptionService
     */
    public function __construct(Manager $dataSourceManager,  AnswerOptionService $answerOptionService) {
        parent::__construct($dataSourceManager);
        $this->answerOptionService = $answerOptionService;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }
    
    public function createOrUpdateAnswerOption(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $questionId     = isset($row['QUESTION_ID'])        ? $row['QUESTION_ID']       : NULL;
        $answerSurveyId = isset($row['ANSWER_SURVEY_ID'])   ? $row['ANSWER_SURVEY_ID']  : NULL;
        $userId         = isset($row[self::USER_ID])        ? $row[self::USER_ID]       : NULL;
        $name           = isset($row['NAME'])               ? $row['NAME']              : NULL;
        $status         = isset($row['STATUS'])             ? $row['STATUS']            : NULL;
        $type           = isset($row['TYPE'])               ? $row['TYPE']              : NULL;
        $nrorg          = isset($row['NRORG'])              ? $row['NRORG']             : NULL;
        $numberOfStars  = isset($row['NUMBER_OF_STARS'])    ? $row['NUMBER_OF_STARS']   : NULL;
        $typeRate       = NULL;
        
        $surAnswerSurvey = $this->answerOptionService->createOrUpdateAnswerOption($answerSurveyId, $userId, $name, $status, $type, $nrorg, $questionId, $typeRate);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok', 'answerSurveyId' => $surAnswerSurvey->getId()]));
    }
    
    public function getAnswerOptionFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = isset($row['NRORG'])  && !empty($row['NRORG'])    ? $row['NRORG']  : NULL;
        $status         = "A";
        
        $answerOptionsNrorg        = $this->answerOptionService->getAnswerOptionFromOrganization($nrorg, $status);
        
        $response->addDataSet(new DataSet('answerOptionsData', $answerOptionsNrorg));
    }
    
    public function getAnswerOptionsFromQuestion(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = isset($row['NRORG'])  && !empty($row['NRORG'])    ? $row['NRORG']  : NULL;
        $questionId     = isset($row['QUESTION_ID'])  && !empty($row['QUESTION_ID'])    ? $row['QUESTION_ID']  : NULL;
        $status         = "A";
        $answerOptionsQuestion = $this->answerOptionService->getAnswerOptionsFromQuestion($nrorg, $status, $questionId);
        
        $response->addDataSet(new DataSet('answerOptionsQuestionData', $answerOptionsQuestion));
    }
}   