<?php
namespace Zeedhi\ApiSurvey\Controller;

use Zeedhi\ApiSurvey\Service\Question as QuestionService;
use Zeedhi\ApiSurvey\Service\AnswerOption as AnswerOptionService;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiSurvey\Service\Environment;

class Question extends Crud {

    protected $dataSourceName = 'survey';
    
    const NRORG         = 'NRORG';
    const USER_ID       = 'USER_ID';
    const EVENT_ID      = 'EVENT_ID';
    const STRUCTURE_ID  = 'STRUCTURE_ID';
    const QUESTIONS     = 'QUESTIONS';
    const TAG           = 'TAG';
    const SURVEY_ID     = 'SURVEY_ID';
    const CATEGORY_ID   = 'CATEGORY_ID';
    const CATEGORIES    = 'CATEGORIES';
    const TAGS          = 'TAGS';
    const STRUCTURES    = 'STRUCTURES';

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param service $questionService
     */
    public function __construct(Manager $dataSourceManager,  QuestionService $questionService, AnswerOptionService $answerOptionService) {
        parent::__construct($dataSourceManager);
        $this->questionService = $questionService;
        $this->answerOptionService = $answerOptionService;
    }
    
    /**
     * getSurQuestion
     * Obtêm os dados da pesquisa filtrando pelo surveyId.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @version 1.0
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function getSurQuestion(DTO\Request\Row $request, DTO\Response $response) {
        $row              = $request->getRow();
        $surveyId         = $row[self::SURVEY_ID];
        
        $surQuestionsData = $this->questionService->getSurQuestion($surveyId);
        
        foreach ($surQuestionsData as $key => $value) {
            $surQuestionsData[$key]['answers']   = $this->questionService->getAnswersByQuestionId($surQuestionsData[$key]['id']); 
        }
        
        $response->addDataSet(new DataSet('surQuestionsData', $surQuestionsData));
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }

    public function createOrUpdateQuestion(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $questionId     = isset($row['QUESTION_ID'])        ? $row['QUESTION_ID']       : NULL ;
        $surveyId       = isset($row['SURVEY_ID'])          ? $row['SURVEY_ID']         : NULL ;
        $userId         = isset($row[self::USER_ID])        ? $row[self::USER_ID]       : NULL ;
        $status         = isset($row['STATUS'])             ? $row['STATUS']            : NULL ;
        $type           = isset($row['TYPE'])               ? $row['TYPE']              : NULL ;
        $name           = isset($row['NAME'])               ? $row['NAME']              : NULL ;
        $nrorg          = isset($row['NRORG'])              ? $row['NRORG']             : NULL ;
        $note           = isset($row['NOTE'])               ? $row['NOTE']              : NULL ;
        $hasComment     = isset($row['HAS_COMMENT'])        ? $row['HAS_COMMENT']       : NULL ;
        $limitQuantity  = isset($row['LIMIT_QUANTITY'])     ? $row['LIMIT_QUANTITY']    : NULL ;
        $evtEventId     = isset($row['EVT_EVENT_ID'])       ? $row['EVT_EVENT_ID']      : NULL ;
        $genStructureId = isset($row['GEN_STRUCTURE_ID'])   ? $row['GEN_STRUCTURE_ID']  : NULL ;
        $numberOfStars  = isset($row['NUMBER_OF_STARS'])    ? $row['NUMBER_OF_STARS']   : NULL ;
        
        try {
            $question = $this->questionService->createOrUpdateQuestion($questionId, $surveyId, $userId, $status, $type, $nrorg, $note, $hasComment, $limitQuantity, $evtEventId, $genStructureId, $name);
            if($type == "rate"){
                $questionId = $question->getId();
                $status     = "A";
                $arrayAnswerOption = [];
                $typeRate   = TRUE;
                
                /*apaga o relacionamentos se existir*/
                $relations = $this->answerOptionService->getAnswerOptionRel($questionId);
                if(!empty($relations)) {
                    foreach($relations as $relation) {
                        $this->answerOptionService->deleteRelations($relation->getId());
                    }
                }
                
                /*cria as opções de resposta*/
                for($i=1; $i<=$numberOfStars; $i++){
                    array_push($arrayAnswerOption, $this->answerOptionService->createOrUpdateAnswerOption(NULL, $userId, $i, $status, NULL, $nrorg, $questionId, $typeRate)->getId());
                }
                
                /*cria novos relaionamentos*/
                foreach($arrayAnswerOption as $value) {
                    $this->answerOptionService->createRalationAnswerOptionAndQuestion($value, $questionId, $nrorg);
                }
                $this->answerOptionService->commit();
            }
            
            $response->addDataSet(new DataSet('response', ['status' => 'ok', 'questionId' => $question->getId()]));
        } catch (\Exception $e) {
            
            $response->addDataSet(new DataSet('error', ['msgError' => $e->getMessage(), 'codeError' => $e->getCode()]));
        }
    }
    
    public function getQuestionsFromSurvey(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $nrorg          = isset($row['NRORG'])      && !empty($row['NRORG'])        ? $row['NRORG']     : NULL;
        $surveyId       = isset($row['SURVEY_ID'])  && !empty($row['SURVEY_ID'])    ? $row['SURVEY_ID'] : NULL;
        $status         = "A";
        
        $questions        = $this->questionService->getQuestionsFromSurvey($nrorg, $status, $surveyId);
        
        $response->addDataSet(new DataSet('questionsData', $questions));
    }
    
}   