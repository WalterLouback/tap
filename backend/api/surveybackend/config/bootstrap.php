<?php
$ds = DIRECTORY_SEPARATOR;

require_once __DIR__.$ds.'..'.$ds.'vendor'.$ds.'autoload.php';
require_once __DIR__.$ds.'..'.$ds.'vendor'.$ds.'zeedhi'.$ds.'framework'.$ds.'bootstrap.php';

$instanceManager = \Zeedhi\Framework\DependencyInjection\InstanceManager::getInstance();
$instanceManager->loadFromFile(__DIR__.$ds.'..'.$ds.'services.xml');
$instanceManager->loadFromFile(__DIR__.$ds.'..'.$ds.'environment.xml');
$instanceManager->compile();