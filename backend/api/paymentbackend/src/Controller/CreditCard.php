<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiGeneral\Helpers\Environment;

use Zeedhi\ApiPayment\Service\CreditCard as CreditCardService;

class CreditCard extends Crud {

    protected $dataSourceName = 'creditcard';
    
    /** @var \Zeedhi\ApiPayment\Service\CreditCard **/
    private $creditCardService;
    
    /**
     * CreditCard constructor.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @param Manager $manager
     * @param CreditCardService $creditCardService
     */
    public function __construct(Manager $manager, CreditCardService $creditCardService) {
        parent::__construct($manager);
        $this->creditCardService = $creditCardService;
    }
    
    /**
    * addCreditCard
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param DTO\Request\Row $request
    * @return DTO\Response $response
    */  
    public function addCreditCard(DTO\Request\Row $request, DTO\Response $response) {
        $creditCardRow = $request->getRow();
        list($month, $year) = explode("/", $creditCardRow['EXPIRATION_DATE']);
        
        $creditCard = $this->creditCardService->addNewCreditCard(
            $creditCardRow['CARD_FULL_NAME'],
            $creditCardRow['NUMBERS'],
            $month,
            $year,
            isset($creditCardRow['CVV']) ? $creditCardRow['CVV'] : NULL,
            $creditCardRow['FLAG'],
            isset($creditCardRow['GEN_USER_ID']) ? $creditCardRow['GEN_USER_ID'] : NULL,
            isset($creditCardRow['NRORG']) ? $creditCardRow['NRORG'] : 0
        );

        $cards = $this->factoryCard($creditCard, isset($creditCardRow['GEN_USER_ID']) ? $creditCardRow['GEN_USER_ID'] : NULL);
        
        if (isset($creditCardRow['NRORG'])) {
            /*Verifica se organization vai usar consulta_Bin*/
            $gatewayData = $this->creditCardService->getGatewayFromOrg($creditCardRow['NRORG']);
            $flag        = $creditCardRow['FLAG'];
            $merchantKey = $gatewayData->getMerchantKey();
            $merchantId  = $gatewayData->getMerchantId();
            if(($gatewayData->getConsultBin() == "A") && $gatewayData->getGateway() == "CIELO" && ($flag == "Visa" || $flag == "Master Card" || $flag == "Elo")) {
                $cards['consulta_bin'] = $this->creditCardService->consultBin($merchantId, $merchantKey, substr($creditCardRow['NUMBERS'], 0, 6));
            }
        }
        
        $response->addDataSet(new DataSet("creditcard", $cards));
    }
    
    /**
    * deleteCC
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param DTO\Request\Row $request
    * @return DTO\Response $response    
    */  
    public function deleteCC(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $card = $this->creditCardService->getCreditCard($row['CREDITCARD_ID']);
        $creditCard = $this->creditCardService->deleteCC($card);
        $cards = $this->factoryCard($creditCard, $row['USER_ID']);

        $response->addDataSet(new DataSet("creditcard", $cards));
    }
    
    /**
    * Format the credit card 
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param PayCreditCard $creditCard
    * @return array 
    */
    public function factoryCard($creditCard, $userId) {
        $mainCard = $userId !== NULL ? $this->creditCardService->getMainCreditCard($userId) : NULL;    
        if ($mainCard != NULL) $mainCardId = $mainCard->getId();
        
        switch ($creditCard->getFlag()) {
            case 'Master':
            case 'Master Card':
                $flag = 'MasterCard';
                break;
            default:
                $flag = $creditCard->getFlag();
                break;
        }
        
        return array(
            "ID" => $creditCard->getId(),
            "FLAG" => $flag,
            "EXPIRATION_DATE" => $creditCard->getExpirationDate(),
            "STATUS" => $creditCard->getStatus(),
            "LAST_NUMBERS" => $creditCard->getlastNumbers(),
            "GEN_USER_ID" => $creditCard->getGenUser() ? $creditCard->getGenUser()->getId() : NULL,
            "CARD_FULL_NAME" => $creditCard->getCardFullName(),
            "IS_MAIN_CARD" => isset($mainCardId) ? $mainCardId == $creditCard->getId() : false
        );
}
    
    /**
    * getValidCreditCard 
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param DTO\Request\Row $request
    * @return DTO\Response $response
    */
    public function getValidCreditCard(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        $user_id = $row['USER_ID'];
        $nrorg   = isset($row['NRORG']) ? $row['NRORG'] : NULL;

        $creditCards = $this->creditCardService->getValidCreditCard($user_id);
        if ($nrorg != NULL) {
            $creditCardFromParent = $this->creditCardService->getCardFromParent($user_id, $nrorg);
        }
        
        $cards = [];
        
        foreach ($creditCards as $key => $creditCard) {
            $cards[$key] = $this->factoryCard($creditCard, $user_id);
        }
        
        $response->addDataSet(new DataSet("creditcards", $cards));
        if (isset($creditCardFromParent) && $creditCardFromParent != NULL)    
            $response->addDataSet(new DataSet("creditcardFromParent", $this->factoryCard($creditCardFromParent, $user_id)));
    }
}