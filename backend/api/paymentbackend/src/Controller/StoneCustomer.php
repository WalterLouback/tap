<?php
namespace Zeedhi\ApiPayment\Controller;

class StoneCustomer extends \Zeedhi\PaymentPlatform\Customer {
    
    protected $birthDate;

    public function setBirthDate($birthDate) {
        $this->birthDate = $birthDate;
    }
    
    public function getBirthDate() {
        return $this->birthDate;
    }

}