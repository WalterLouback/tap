<?php

namespace Zeedhi\ApiPayment\Controller;

use Doctrine\ORM\EntityManager;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction as PlatformInstantBuyTransaction;
use Zeedhi\PaymentPlatform\CreditCard as PlatformCreditCard;
use Zeedhi\PaymentPlatform\Consult\ConsultPlatform;
use Zeedhi\PaymentPlatform\Consult\Consult;
use Zeedhi\PaymentPlatform\Revoke\RevokePlatform;
use Zeedhi\PaymentPlatform\Revoke\Revoke;
use Zeedhi\PaymentPlatform\Customer;
use Zeedhi\PaymentPlatform\RequestURL;

use Zeedhi\PaymentPlatform\Implementations\MundiPagg\PaymentImpl as MundiPaggPaymentImpl;
use Zeedhi\PaymentPlatform\Implementations\Cielo\PaymentImpl as CieloPaymentImpl;

use Zeedhi\ApiPayment\Service\Environment;

use Zeedhi\ApiPayment\Model\Entities\OrdOrder;
use Zeedhi\ApiPayment\Model\Entities\PayGateway;
use Zeedhi\ApiPayment\Model\Entities\PayGatewayRel;
use Zeedhi\ApiPayment\Model\Entities\GenConfiguration;

class CieloPayment {
    
    public static function execRevoke($value, $transactionId, $entityManager) {
        $gatewayData = CieloPayment::getGatewayFromOrder($transactionId, $entityManager);
        $merchantKey = $gatewayData->getMerchantKey();
        $merchantId  = $gatewayData->getMerchantId();
        
        return CieloPayment::execPut($value * 100, $transactionId, $merchantKey, $merchantId);
    }
    
    private static function getGatewayFromOrder($transactionId, $entityManager) {
        $order = $entityManager->getRepository(OrdOrder::class)->findOneBy(['transactionId' => $transactionId]);
        $nrorg = $order !== NULL ? $order->getNrorg() : 0;
        return CieloPayment::getGatewayFromOrg($nrorg, $entityManager);
    }
    
    private static function getGatewayFromOrg($nrorg, $entityManager) {
        $payGateway       = PayGateway::class;
        $payGatewayRel    = PayGatewayRel::class;
        $genConfiguration = GenConfiguration::class;
        $gateway          = $entityManager->createQuery(
            "
            SELECT g
            FROM $payGateway g
            JOIN $payGatewayRel gr WITH gr.payGateway = g
            JOIN gr.genConfiguration c
            WHERE c.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($gateway) == 0) throw new Exception("Gateway not configured for organization!", 140);
        
        return $gateway[0];
    }
    
    private static function execPut($value, $transactionId, $merchantKey, $merchantId) {
        $url = "https://api.cieloecommerce.cielo.com.br/1/sales/$transactionId/void?amount=$value";
        $header['MerchantId']  = $merchantId;
        $header['MerchantKey'] = $merchantKey;
        $typeRequest           = "PUT";
        $body                  = "";
        
        return CieloPayment::execCurl($url, $body, $header, $typeRequest);
    }
    
    private static function execCurl($url, $body, $header, $typeRequest) {
        $data_string = json_encode($body, JSON_UNESCAPED_SLASHES);
        
        // inicia seção CURL 
        $curl = curl_init();
        $headers = [
                'Content-Type: application/json',
                'MerchantId:'       . $header['MerchantId'],
                'MerchantKey:'      . $header['MerchantKey'],
                'Content-Length:'   . strlen($data_string)
        ];
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);             
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $typeRequest);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode($result, true);
        
        return $objResult;
    }
    
}