<?php

namespace Zeedhi\ApiPayment\Controller;

use Doctrine\ORM\EntityManager;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiPayment\Controller\XappaPaymentImpl;
use Zeedhi\ApiPayment\Controller\StonePaymentImpl;

use Zeedhi\ApiPayment\Model\Entities\PayGateway;
use Zeedhi\ApiPayment\Model\Entities\PayGatewayRel;
use Zeedhi\ApiPayment\Model\Entities\PayCreditcard;
use Zeedhi\ApiPayment\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiPayment\Model\Entities\OrdOrder;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction as PlatformInstantBuyTransaction;
use Zeedhi\PaymentPlatform\CreditCard as PlatformCreditCard;
use Zeedhi\PaymentPlatform\Consult\ConsultPlatform;
use Zeedhi\PaymentPlatform\Consult\Consult;
use Zeedhi\PaymentPlatform\Revoke\RevokePlatform;
use Zeedhi\PaymentPlatform\Revoke\Revoke;
use Zeedhi\PaymentPlatform\RequestURL;

use Zeedhi\PaymentPlatform\Implementations\MundiPagg\PaymentImpl as MundiPaggPaymentImpl;
use Zeedhi\PaymentPlatform\Implementations\Cielo\PaymentImpl as CieloPaymentImpl;

use Zeedhi\ApiPayment\Service\Environment;

class Payment {
    /** $dir String Directory of file script passed by caller */
    public $scriptDirectory;
    /** @var EntityManager */
    private $entityManager;
    /** @var PaymentPlatform */
    private $paymentPlatform;
    /** @var Environment */
    private $environment;
    /** @var PaymentService */
    private $paymentService;
    /** @var String Directory of PHP */
    private $DIR_PHP;
   
    
    const VALUE = 'VALUE';
    const CREDITCARD_TOKEN = 'CREDITCARD_TOKEN';
    const CUSTOMER_ID = 'CUSTOMER_ID';
    const ORDER_REFERENCE = 'ORDER_REFERENCE';
    const GATEWAY = 'GATEWAY';
    
    /**
     * Payment constructor.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @param EntityManager $entityManager
     * @param InstantBuyPlatform $paymentPlatform
     * @param Environment   $environment
     * @param $DIR_PHP
     * @param PaymentService $paymentService
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;

    }
        
    /**
    * Receives the order_id in the request, selects the payment method and returns the updated order.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param DTO\Request\Row
    * @return DTO\Response $response
    */ 
    // public function execPaymentCreditCard(DTO\Request\Row $request, DTO\Response $response){
    public function execPaymentCreditCard($merchantKey, $merchantId, $gateway, $order){
        $value = $order->getTotal();
        $creditCardToken = $order->getPayCreditcard()->getToken();
        $customerId = $order->getPayCreditcard()->getCustomerId() ? $order->getPayCreditcard()->getCustomerId() : $order->getPayCreditcard()->getId();
        $orderReference = $order->getId();

        $this->initGateway($gateway, $merchantKey, $merchantId);
        
        $payment = $this->makePaymentTransaction($value, $creditCardToken, $customerId, $orderReference, $gateway);
        
        return $payment;
    }
    
    public function execPaymentXappa(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
    
        $value = $row['VALUE'];
        $creditCardId = $row['CREDITCARD_TOKEN'];
        $creditCard = $this->entityManager->getRepository(PayCreditcard::class)->find($creditCardId);
        $creditCardToken = $creditCard->getToken();
        $customerId = $creditCard->getCustomerId() ? $creditCard->getCustomerId() : $creditCard->getId();
        $orderReference = $row['ORDER_REFERENCE'];
        
        $gatewayConfs = $this->getGatewayFromOrg();
        $gateway = $gatewayConfs->getGateway();
        $merchantKey = $gatewayConfs->getMerchantKey();
        $merchantId = $gatewayConfs->getMerchantId();
        
        
        $this->initGateway($gateway, $merchantKey, $merchantId);
        
        $payment = $this->makePaymentTransaction($value, $creditCardToken, $customerId, $orderReference, $gateway);
        
        $dataSet = new DataSet('payment', $payment);
        
        $response->addDataSet($dataSet);
    }
    
    public function initGateway($gatewayName, $merchantKey, $merchantId="ac904eea-0d87-4eb8-a9cd-b842f7be3246") {
        $this->paymentPlatform = self::factoryGateway($gatewayName, $merchantKey, $merchantId);
    }
    
    public static function factoryGateway($gatewayName, $merchantKey, $merchantId="ac904eea-0d87-4eb8-a9cd-b842f7be3246") {
        if ($gatewayName == 'MUNDIPAGG') {
            $paymentPlatform = MundiPaggPaymentImpl::factory($merchantKey);
        } else if ($gatewayName == 'CIELO') {
            $paymentPlatform = CieloPaymentImpl::factory($merchantKey, $merchantId, CieloPaymentImpl::PRODUCTION);
        } else if ($gatewayName == 'STONE') {
            $paymentPlatform = StonePaymentImpl::factory($merchantKey);
        } else if ($gatewayName == 'XAPPAY') {
            $paymentPlatform = XappaPaymentImpl::factory($merchantKey);
        }
        
        return $paymentPlatform;
    }

    /**  
    * Instances the transaction and sends it to the gateway (mundipagg)  
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param OrdOrder $order
    * @return $transaction
    */
    private function makePaymentTransaction($value, $creditCardToken, $customerId, $orderReference, $gateway) {
        $order = $this->entityManager->getRepository(OrdOrder::class)->find($orderReference);
        $user = $order->getGenUser();
        
        $order->build($this->entityManager);
        
        $user->setContacts($this->entityManager, $order->getNrorg());
        $user->setAddress($this->entityManager, $order->getNrorg());
        $userDataArray = $user->toArray();
        $addressArray = isset($userDataArray['ADDRESS']) && count($userDataArray['ADDRESS']) > 0 ? $userDataArray['ADDRESS'][0] : NULL;
        $orderItemsArray = OrdOrderProduct::manyToArray($order->getItems());
        
        $customer = new StoneCustomer();
        $customer->setId($customerId);
        if ($userDataArray !== NULL) {
            $customer->setType("individual");
            $customer->setEmail($userDataArray['EMAIL']);
            $customer->setName($userDataArray['FIRST_NAME'] . ' ' . $userDataArray['LAST_NAME']);
            $customer->setDocument($userDataArray['CPF']);
            $customer->setBirthDate($userDataArray['BIRTH_DATE']);
            if (count($userDataArray['ADDRESS']) > 0) $customer->setAddress($userDataArray['ADDRESS'][0]);
            if (count($userDataArray['CONTACTS']) > 0) $customer->setPhone($userDataArray['CONTACTS'][0]['phone']);
        }
        $installments = '1';
        $transaction = new PlatformInstantBuyTransaction(
            $value * 100, // Convert amount to cents
            $orderReference,
            $creditCardToken,
            $customer,
            $installments
        );

        try {
            $response = $this->paymentPlatform->instantBuyTransaction($transaction, $orderItemsArray);
            if ($this->paymentPlatform instanceof CieloPaymentImpl && isset($response['captureUrl'])) {
                $this->paymentPlatform->captureTransaction($response['captureUrl'], $response['paymentId']);
            }

            if ($gateway == "MUNDIPAGG") {
                $payment['transaction_id'] = $response;
                $transaction               = $this->execConsult($payment['transaction_id']);
                $payment['status']         = $transaction["last_transaction"]['status'];
            } else if ($gateway == "CIELO") {
                $payment                   = $response;
                $payment['status']         = $response["status"] == "0" ? "captured" : $response["status"];
                $payment['transaction_id'] = isset($response['paymentId']) ? $response['paymentId'] : null;
            } else if ($gateway == "STONE") {
                $payment                   = $response;
                $payment['status']         = $response["status"] == "authorized" || $response['status'] == "paid" ? "captured" : $response["status"];
                $payment['transaction_id'] = isset($response['tid']) ? $response['tid'] : null;
            } else if ($gateway == "XAPPAY") {
                $payment                   = $response;
            }
            
            return $payment;
        } catch (\Exception $e) {
            if ($e->getCode() == 11) throw new \Exception('Unknown error in payment: ' . $e->getMessage(), 19);
            else throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
    * Receive id of transaction, sending request.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param int $id id of transaction
    * @return array $transaction
    */ 
    public function execConsult($id){
        $baseURL = RequestURL::MUNDIPAGG_CHARGES;/*Cristiano*/
        $consult = new Consult($id, $baseURL);
        $transactionId = $this->paymentPlatform->consult($consult);
        return $transactionId;
    }
    
    public function revoke($transactionId, $value, $gateway, $merchantKey, $merchantId=NULL){
        $this->initGateway($gateway, $merchantKey, $merchantId);
        
        if ($gateway == 'MUNDIPAGG') {
            $baseURL = RequestURL::MUNDIPAGG_CHARGES;/*Cristiano*/
            $revoke  = new Revoke($transactionId, $baseURL,  $value);
            $revoke  = $this->paymentPlatform->revoke($revoke); 
            $transaction = $this->execConsult($revoke);
            $rvk['status'] = $transaction['last_transaction']['status'];
            $rvk['success'] = $transaction['last_transaction']['success'];
        } else if ($gateway == 'CIELO') {
            $rvk = CieloPayment::execRevoke($value, $transactionId, $this->entityManager);
            $rvk['success'] = isset($rvk['ReturnCode']) && $rvk['ReturnCode'] == 9;
        } else if ($gateway == 'STONE') {
            $rvk = $this->paymentPlatform->execRevoke($value, $transactionId);
            $rvk['success'] = isset($rvk['status']) && $rvk['status'] == 'refunded';
        } else if ($gateway == 'XAPPAY') {
            $rvk = $this->paymentPlatform->execRevoke($value, $transactionId);
        }
        return $rvk;
    }
    
    public function execRevokeXappa(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $transactionId = $row['TRANSACTION_ID'];
        $value         = $row['VALUE'];

        $gatewayData   = $this->getGatewayFromOrg();
        $gateway       = $gatewayData->getGateway();
        $merchantKey   = $gatewayData->getMerchantKey();
        $merchantId    = $gatewayData->getMerchantId();
        
        $revoke = $this->revoke($transactionId, $value, $gateway, $merchantKey, $merchantId);
        $response->addDataSet(new DataSet('revoke', $revoke));
    }
    
    public function getGatewayFromOrg($nrorg = 0) {
        $payGateway       = PayGateway::class;
        $payGatewayRel    = PayGatewayRel::class;
        $genConfiguration = GenConfiguration::class;
        $gateway          = $this->entityManager->createQuery(
            "
            SELECT g
            FROM $payGateway g
            JOIN $payGatewayRel gr WITH gr.payGateway = g
            JOIN gr.genConfiguration c
            WHERE c.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($gateway) == 0) throw new Exception("Gateway not configured for organization!", 140);
        
        return $gateway[0];
    }

}
