<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiPayment\Service\Exception as Exception;


use Zeedhi\ApiPayment\Service\Balance as BalanceService;

class Balance extends Crud {

    protected $dataSourceName = 'creditcard';
    
    /** @var \Zeedhi\ApiPayment\Service\Balance **/
    private $balanceService;
    
    const USER_ID = 'USER_ID';
    const VALUE = 'VALUE';
    const NRORG = 'NRORG';

    /**
     * Balance constructor.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @param Manager $manager
     * @param BalanceService $balanceService
     */
    public function __construct(Manager $manager, BalanceService $balanceService) {
        parent::__construct($manager);
        $this->balanceService = $balanceService;
    }
    
    /**
    * execPaymentBalance 
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * @param DTO\Request\Row $request
    * @return DTO\Response $response
    */
    public function execPaymentBalance($value, $userId, $nrorg, $walletId=NULL) {
        $balance = $this->makePaymentTransactionWithBalance($value, $userId, $nrorg, $walletId);
        
        $wallet['RESPONSE'] = 'AUTHORIZED';
        $wallet['BALANCE'] = $balance;
        return $wallet;
    }
    
    /**
    * makePaymentTransaction 
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    */
    private function makePaymentTransactionWithBalance($value, $userId, $nrorg, $walletId=NULL){
        if ($walletId === NULL) $wallet = $this->balanceService->getWalletByUser($userId, $nrorg);
        else $wallet = $this->balanceService->getWalletById($walletId);

        if($value <= 0)
            throw Exception::invalidValue();
        if(empty($wallet))
            throw Exception::walletNotFound();
        if($wallet->getBalance() < $value) 
            throw Exception::InsufficientFunds();

        $wallet->setBalance($wallet->getBalance() - $value);
        $this->balanceService->updateAll();
        return $wallet->getBalance();
    }

}