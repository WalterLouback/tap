<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\PaymentPlatform\CreditCard;
use Zeedhi\PaymentPlatform\Customer;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction;

class StonePaymentImpl {

    /** @var string */
    private $baseUrl = 'https://api.pagar.me/1/';

    public function __construct($merchantKey) {
        $this->merchantKey = $merchantKey;
    }

    public function createToken(CreditCard $card, Customer $customer) {
        $request = array(
        	"card_number" => $card->getNumbers(),
        	"card_expiration_date" => $card->getExpDateMonth() . $card->getExpDateYear(),
        	"card_holder_name" => $card->getHolderName(),
        	"card_cvv" => $card->getCvv(),
        	"api_key" => $this->merchantKey
        );
        $responseData = $this->createRequest($request, 'cards');

        return array(
            'InstantBuyKey' => $responseData->id
        );
    }
    
    public function instantBuyTransaction(InstantBuyTransaction $instantBuyTransaction, $items = []) {
        $customer = $instantBuyTransaction->getCustomer();

        $request = array(
            "amount" => $instantBuyTransaction->getValue(),
            "capture" => false,
            "api_key" => $this->merchantKey,
            "card_id" => $instantBuyTransaction->getCreditCardToken(),
            "customer" => array(
                "external_id" => (string) $customer->getId(),
                "type" => $customer->getType(),
                "email" => $customer->getEmail(),
                "phone_numbers" => [ "+55" . preg_replace("/[^0-9]/", "", $customer->getPhone()) ],
                "birthday" => method_exists($customer, 'getBirthDate') && $customer->getBirthDate() !== NULL ? $customer->getBirthDate()->format('Y-m-d') : '1996-01-01',
                "country" => "br",
                "name" => $customer->getName(),
                "documents" => [ [ 'type' => 'cpf', 'number' => $customer->getDocument() ] ]
            ),
            "billing" => array(
                "name" => $customer->getName()
            ),
            "items" => array()
        );
        
        if ($customer->getAddress() !== NULL) {
            $request['billing']['address'] = array(
                "country" => "br",
                "state" => $customer->getAddress()['PROVINCY'],
                "city" => $customer->getAddress()['CITY'],
                "neighborhood" => $customer->getAddress()['NEIGHBORHOOD'],
                "street" => $customer->getAddress()['STREET'],
                "street_number" => (string) $customer->getAddress()['NUMBER'],
                "zipcode" => $customer->getAddress()['CEP']
            );
        } else {
            $request['billing']['address'] = array(
                "country" => "br",
                "state" => "MG",
                "city" => "Belo Horizonte",
                "neighborhood" => "Savassi",
                "street" => "Rua Pernambuco",
                "street_number" => "1000",
                "zipcode" => "30130151"
            );
        }
        
        foreach ($items as $item) {
            array_push($request['items'], array(
                "id" => (string) $item['id'],
                "title" => $item['name'],
                "unit_price" => isset($item['unitaryPrice']) ? $item['unitaryPrice'] * 100 : ( $item['total'] / $item['quantity'] ) * 100,
                "quantity" => $item['quantity'],
                "tangible" => true
              )
            );
        }
        
        $responseData = $this->createRequest($request, "transactions");

        return json_decode(json_encode($responseData), true);
    }
    
    public function execRevoke($value, $transactionId) {
        $request = array(
            "api_key" => $this->merchantKey
        );
        
        $responseData = $this->createRequest($request, "transactions/$transactionId/refund");
        
        return json_decode(json_encode($responseData), true);
    }

    private function createRequest($row, $request){
        $ch = curl_init( $this->baseUrl . "/" . $request);
        # Setup request to send json via POST.
        $payload = json_encode( $row );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        # Check response http code
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode !== 200) throw new \Exception('API Stone informa o seguinte erro: ' . $result);
        curl_close($ch);
        # Print response.
        return json_decode($result);
    }

    public static function factory($merchantKey) {
        return new self($merchantKey);
    }
}