<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\PaymentPlatform\CreditCard;
use Zeedhi\PaymentPlatform\Customer;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction;

class XappaPaymentImpl {

    /** @var string */
    private $urlRequest = 'http://lucasmacedo.zeedhi.com/workfolder/appCostumer/generalbackend-1/service/index.php';

    public function __construct($merchantKey) {
        $this->merchantKey = $merchantKey;
    }

    public function createToken(CreditCard $card, Customer $customer) {
        $request = array(
            "CUSTOMER_NAME" => $customer->getName(),
            "NUMBERS" => $card->getNumbers(),
            "CARD_FULL_NAME" => $card->getHolderName(),
            "EXPIRATION_DATE" => $card->getExpDateMonth().'/'.$card->getExpDateYear(),
            "FLAG" => $card->getFlag(),
            "CVV" => $card->getCvv(),
            "USER_ID" => "a",
            "TOKEN" => "a"
        );
        $responseData = $this->createRequest($request, 'addCreditCard');

        return array(
            'InstantBuyKey' => $responseData->dataset->creditcard->ID
        );
    }
    
    public function instantBuyTransaction(InstantBuyTransaction $instantBuyTransaction) {
        $request = array(
            "ORDER_REFERENCE" => $instantBuyTransaction->getTransactionId(),
            "VALUE" => $instantBuyTransaction->getValue() / 100,
            "CREDITCARD_TOKEN" => $instantBuyTransaction->getCreditCardToken(),
            "USER_ID" => "1",
            "TOKEN" => "a"
        );
        
        $responseData = $this->createRequest($request, "execPaymentXappa");
        
        return json_decode(json_encode($responseData->dataset->payment), true);
    }
    
    public function execRevoke($value, $transactionId) {
        $request = array(
            "TRANSACTION_ID" => $transactionId,
            "VALUE" => $value,
            "USER_ID" => "1",
            "TOKEN" => "a"
        );
        
        $responseData = $this->createRequest($request, "execRevokeXappa");
        
        return json_decode(json_encode($responseData->dataset->revoke), true);
    }

    private function createRequest($row, $request){
        $ch = curl_init( $this->urlRequest . "/" . $request);
        # Setup request to send json via POST.
        $payload = json_encode( array( "requestType" => 'Row', 'row' => $row ) );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        # Check response http code
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode !== 200) throw new \Exception('API Xappa informa o seguinte erro: ' . $result);
        curl_close($ch);
        # Print response.
        return json_decode($result);
    }

    public static function factory($merchantKey) {
        return new self($merchantKey);
    }
}