<?php
namespace Zeedhi\ApiPayment\Model\Entities;


class OrdOrderProduct extends \Zeedhi\ApiPayment\Model\Entities\Base\OrdOrderProduct {
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['quantity'] = $this->getQuantity();
        // $array['status'] = $this->getStatus();
        // $array['note'] = $this->getNote();
        $array['total'] = $this->getTotal();
        
        if ($this->getEvtEventTicket()) {
            $array['name'] = $this->getEvtEventTicket()->getName();
        }
        else if ($this->getOrdMenuProduct()) {
            $array['name'] = $this->getOrdMenuProduct()->getName();
            // $array['image'] = $this->getOrdMenuProduct()->getImage();
            $array['unitaryPrice'] = $this->getOrdMenuProduct()->getPrice();
            $array['productId'] = $this->getOrdMenuProduct()->getId();
        }
        else if ($this->getSerService()) {
            $array['name'] = $this->getSerService()->getName();
        }
        return $array;
    }
    
    public static function manyToArray($items) {
        $arrays = [];
        foreach ($items as $item) {
            $array = $item->toArray();
            array_push($arrays, $array);
        }
        return $arrays;
    }
    
}