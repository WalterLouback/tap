<?php
namespace Zeedhi\ApiPayment\Model\Entities;


class GenAddress extends \Zeedhi\ApiPayment\Model\Entities\Base\GenAddress {
    
    public static function manyToArray($addresses) {
        $array = [];
        foreach ($addresses as $address) {
            array_push($array, $address->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "CEP" => $this->getCep(),
            "STREET" => $this->getStreet(),
            "NEIGHBORHOOD" => $this->getNeighborhood(),
            "NUMBER" => $this->getNumber(),
            "COMPLEMENT" => $this->getComplement(),
            "CITY" => $this->getCity(),
            "PROVINCY" => $this->getProvincy()
        );
    }
    
}