<?php
namespace Zeedhi\ApiPayment\Model\Entities;


class GenContact extends \Zeedhi\ApiPayment\Model\Entities\Base\GenContact {
    
    public static function manyToArray($contacts) {
        $array = [];
        foreach ($contacts as $contact) {
            array_push($array, $contact->toArray());
        }
        return $array;
    }

    public function toArray() {
        return [
            'type'  => $this->getType(),
            'phone' => $this->getPhone()
        ];
    }

    
}