<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenTag {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
}