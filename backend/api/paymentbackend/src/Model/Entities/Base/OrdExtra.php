<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdExtra {
    
    
    /** @var int  */
    protected $multiple;
    /** @var int  */
    protected $required;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdProduct  */
    protected $ordProduct;

    public function getMultiple() {
        return $this->multiple;
    }
	public function setMultiple($multiple = NULL) {
        $this->multiple = $multiple;
    }
	public function getRequired() {
        return $this->required;
    }
	public function setRequired($required = NULL) {
        $this->required = $required;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdProduct() {
        return $this->ordProduct;
    }
	public function setOrdProduct(\Zeedhi\ApiPayment\Model\Entities\OrdProduct $ordProduct = NULL) {
        $this->ordProduct = $ordProduct;
    }
}