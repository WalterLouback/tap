<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenStructure {
    
    
    /** @var int  */
    protected $level;
    /** @var int  */
    protected $genStructureId;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $description;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenAddress  */
    protected $genAddress;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenStructure  */
    protected $parent;

    public function getLevel() {
        return $this->level;
    }
	public function setLevel($level = NULL) {
        $this->level = $level;
    }
	public function getGenStructureId() {
        return $this->genStructureId;
    }
	public function setGenStructureId($genStructureId = NULL) {
        $this->genStructureId = $genStructureId;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getDescription() {
        return $this->description;
    }
	public function setDescription($description = NULL) {
        $this->description = $description;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenAddress() {
        return $this->genAddress;
    }
	public function setGenAddress(\Zeedhi\ApiPayment\Model\Entities\GenAddress $genAddress = NULL) {
        $this->genAddress = $genAddress;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiPayment\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
}