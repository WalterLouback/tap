<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtEvent {
    
    
    /** @var string  */
    protected $merchantKey;
    /** @var int  */
    protected $ownerUser;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $genStructure;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $provincy;
    /** @var string  */
    protected $city;
    /** @var string  */
    protected $neighborhood;
    /** @var string  */
    protected $street;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $imageMap;
    /** @var string  */
    protected $imageLogo;
    /** @var string  */
    protected $imageCover;
    /** @var \Datetime  */
    protected $datetime;
    /** @var string  */
    protected $address;
    /** @var string  */
    protected $about;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $parentEvent;
    /** @var string  */
    protected $values;
    
	public function getValues() {
        return $this->values;
    }
    public function setValues($values = NULL) {
        $this->values = $values;
    }
    public function getMerchantKey() {
        return $this->merchantKey;
    }
	public function setMerchantKey($merchantKey = NULL) {
        $this->merchantKey = $merchantKey;
    }
	public function getOwnerUser() {
        return $this->ownerUser;
    }
	public function setOwnerUser($ownerUser = NULL) {
        $this->ownerUser = $ownerUser;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure($structure = NULL) {
        $this->genStructure = $structure;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getProvincy() {
        return $this->provincy;
    }
	public function setProvincy($provincy = NULL) {
        $this->provincy = $provincy;
    }
	public function getCity() {
        return $this->city;
    }
	public function setCity($city = NULL) {
        $this->city = $city;
    }
	public function getNeighborhood() {
        return $this->neighborhood;
    }
	public function setNeighborhood($neighborhood = NULL) {
        $this->neighborhood = $neighborhood;
    }
	public function getStreet() {
        return $this->street;
    }
	public function setStreet($street = NULL) {
        $this->street = $street;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getImageMap() {
        return $this->imageMap;
    }
	public function setImageMap($imageMap = NULL) {
        $this->imageMap = $imageMap;
    }
	public function getImageLogo() {
        return $this->imageLogo;
    }
	public function setImageLogo($imageLogo = NULL) {
        $this->imageLogo = $imageLogo;
    }
	public function getImageCover() {
        return $this->imageCover;
    }
	public function setImageCover($imageCover = NULL) {
        $this->imageCover = $imageCover;
    }
	public function getDatetime() {
        return $this->datetime;
    }
	public function setDatetime(\Datetime $datetime = NULL) {
        $this->datetime = $datetime;
    }
	public function getAddress() {
        return $this->address;
    }
	public function setAddress($address = NULL) {
        $this->address = $address;
    }
	public function getAbout() {
        return $this->about;
    }
	public function setAbout($about = NULL) {
        $this->about = $about;
    }
	public function getParentEvent() {
        return $this->parentEvent;
    }
	public function setParentEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $parentEvent = NULL) {
        $this->parentEvent = $parentEvent;
    }
}