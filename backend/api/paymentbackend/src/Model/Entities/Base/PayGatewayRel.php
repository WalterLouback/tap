<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class PayGatewayRel {
    
    
    protected $payGateway;
    protected $evtEvent;
    protected $genConfiguration;
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getPayGateway() {
        return $this->payGateway;
    }
	public function setPayGateway($payGateway = NULL) {
        $this->payGateway = $payGateway;
    }
    public function getGenConfiguration() {
        return $this->genConfiguration;
    }
	public function setGenConfiguration($genConfiguration = NULL) {
        $this->genConfiguration = $genConfiguration;
    }
    public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent($evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
}