<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenAddress {
    
    
    /** @var \Datetime  */
    protected $date;
    /** @var string  */
    protected $provincy;
    /** @var string  */
    protected $city;
    /** @var string  */
    protected $neighborhood;
    /** @var string  */
    protected $street;
    /** @var string  */
    protected $cep;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $number;
    /** @var string  */
    protected $complement;
    /** @var string  */
    protected $imageDocument;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenAddress  */
    protected $old;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUser  */
    protected $genUser;

    public function getDate() {
        return $this->date;
    }
	public function setDate(\Datetime $date) {
        $this->date = $date;
    }
	public function getProvincy() {
        return $this->provincy;
    }
	public function setProvincy($provincy = NULL) {
        $this->provincy = $provincy;
    }
	public function getCity() {
        return $this->city;
    }
	public function setCity($city = NULL) {
        $this->city = $city;
    }
	public function getNeighborhood() {
        return $this->neighborhood;
    }
	public function setNeighborhood($neighborhood = NULL) {
        $this->neighborhood = $neighborhood;
    }
	public function getStreet() {
        return $this->street;
    }
	public function setStreet($street = NULL) {
        $this->street = $street;
    }
	public function getCep() {
        return $this->cep;
    }
	public function setCep($cep) {
        $this->cep = $cep;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getNumber() {
        return $this->number;
    }
	public function setNumber($number) {
        $this->number = $number;
    }
	public function getComplement() {
        return $this->complement;
    }
	public function setComplement($complement) {
        $this->complement = $complement;
    }
	public function getImageDocument() {
        return $this->imageDocument;
    }
	public function setImageDocument($imageDocument) {
        $this->imageDocument = $imageDocument;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOld() {
        return $this->old;
    }
	public function setOld(\Zeedhi\ApiPayment\Model\Entities\GenAddress $old = NULL) {
        $this->old = $old;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiPayment\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}