<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtEventTicket {
    
    
    /** @var string  */
    protected $status;
    /** @var \Datetime  */
    protected $finalSale;
    /** @var \Datetime  */
    protected $startSale;
    /** @var int  */
    protected $amount;
    /** @var float  */
    protected $price;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;

    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getFinalSale() {
        return $this->finalSale;
    }
	public function setFinalSale(\Datetime $finalSale = NULL) {
        $this->finalSale = $finalSale;
    }
	public function getStartSale() {
        return $this->startSale;
    }
	public function setStartSale(\Datetime $startSale = NULL) {
        $this->startSale = $startSale;
    }
	public function getAmount() {
        return $this->amount;
    }
	public function setAmount($amount = NULL) {
        $this->amount = $amount;
    }
	public function getPrice() {
        return $this->price;
    }
	public function setPrice($price = NULL) {
        $this->price = $price;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}