<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdOption {
    
    
    /** @var float  */
    protected $price;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdExtra  */
    protected $ordExtra;

    public function getPrice() {
        return $this->price;
    }
	public function setPrice($price = NULL) {
        $this->price = $price;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdExtra() {
        return $this->ordExtra;
    }
	public function setOrdExtra(\Zeedhi\ApiPayment\Model\Entities\OrdExtra $ordExtra = NULL) {
        $this->ordExtra = $ordExtra;
    }
}