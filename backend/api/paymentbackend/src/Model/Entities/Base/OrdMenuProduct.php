<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdMenuProduct {
    
    
    /** @var string  */
    protected $name;
    /** @var float  */
    protected $price;
    /** @var \Datetime  */
    protected $finalTime;
    /** @var \Datetime  */
    protected $initialTime;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdMenu  */
    protected $ordMenu;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdProduct  */
    protected $ordProduct;

    public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
    public function getPrice() {
        return $this->price;
    }
	public function setPrice($price = NULL) {
        $this->price = $price;
    }
	public function getFinalTime() {
        return $this->finalTime;
    }
	public function setFinalTime(\Datetime $finalTime = NULL) {
        $this->finalTime = $finalTime;
    }
	public function getInitialTime() {
        return $this->initialTime;
    }
	public function setInitialTime(\Datetime $initialTime = NULL) {
        $this->initialTime = $initialTime;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdMenu() {
        return $this->ordMenu;
    }
	public function setOrdMenu(\Zeedhi\ApiPayment\Model\Entities\OrdMenu $ordMenu = NULL) {
        $this->ordMenu = $ordMenu;
    }
	public function getOrdProduct() {
        return $this->ordProduct;
    }
	public function setOrdProduct(\Zeedhi\ApiPayment\Model\Entities\OrdProduct $ordProduct = NULL) {
        $this->ordProduct = $ordProduct;
    }
}