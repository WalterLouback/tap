<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtTagNews {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenTag  */
    protected $evtTag;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenNews  */
    protected $genNews;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtTag() {
        return $this->evtTag;
    }
	public function setEvtTag(\Zeedhi\ApiPayment\Model\Entities\GenTag $evtTag = NULL) {
        $this->evtTag = $evtTag;
    }
	public function getGenNews() {
        return $this->genNews;
    }
	public function setGenNews(\Zeedhi\ApiPayment\Model\Entities\GenNews $genNews = NULL) {
        $this->genNews = $genNews;
    }
}