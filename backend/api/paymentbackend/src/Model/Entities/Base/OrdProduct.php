<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdProduct {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $price;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $detail;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdProductGroup  */
    protected $ordProductGroup;

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getPrice() {
        return $this->price;
    }
	public function setPrice($price) {
        $this->price = $price;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getDetail() {
        return $this->detail;
    }
	public function setDetail($detail = NULL) {
        $this->detail = $detail;
    }
	public function getOrdProductGroup() {
        return $this->ordProductGroup;
    }
	public function setOrdProductGroup(\Zeedhi\ApiPayment\Model\Entities\OrdProductGroup $ordProductGroup = NULL) {
        $this->ordProductGroup = $ordProductGroup;
    }
}