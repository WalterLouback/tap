<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class PayPaymentMethod {
    
    
    /** @var string  */
    protected $paymentMethod;
    /** @var string  */
    protected $label;
    /** @var string  */
    protected $nrorg;
    /** @var string  */
    protected $xSellerToken;
    /** @var string  */
    protected $xPicpayToken;
    /** @var int  */
    protected $id = 0;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod($paymentMethod = NULL) {
        $this->paymentMethod = $paymentMethod;
    }
    public function getXSellerToken() {
        return $this->xSellerToken;
    }
	public function setXSellerToken($xSellerToken = NULL) {
        $this->xSellerToken = $xSellerToken;
    }
    public function getXPicpayToken() {
        return $this->xPicpayToken;
    }
	public function setXPicpayToken($xPicpayToken = NULL) {
        $this->xPicpayToken = $xPicpayToken;
    }
    public function getLabel() {
        return $this->label;
    }
	public function setLabel($label = NULL) {
        $this->label = $label;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
}