<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtUserTicketRel {
    
    
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEventTicket  */
    protected $evtEventTicket;

    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
	public function getEvtEventTicket() {
        return $this->evtEventTicket;
    }
	public function setEvtEventTicket(\Zeedhi\ApiPayment\Model\Entities\EvtEventTicket $evtEventTicket = NULL) {
        $this->evtEventTicket = $evtEventTicket;
    }
}