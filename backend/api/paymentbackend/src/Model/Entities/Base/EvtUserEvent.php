<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtUserEvent {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}