<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdMenu {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
}