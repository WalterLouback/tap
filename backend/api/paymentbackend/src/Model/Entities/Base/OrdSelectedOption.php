<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdSelectedOption {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdItemExtra  */
    protected $ordItemExtra;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdOption  */
    protected $ordOption;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdItemExtra() {
        return $this->ordItemExtra;
    }
	public function setOrdItemExtra(\Zeedhi\ApiPayment\Model\Entities\OrdItemExtra $ordItemExtra = NULL) {
        $this->ordItemExtra = $ordItemExtra;
    }
	public function getOrdOption() {
        return $this->ordOption;
    }
	public function setOrdOption(\Zeedhi\ApiPayment\Model\Entities\OrdOption $ordOption = NULL) {
        $this->ordOption = $ordOption;
    }
}