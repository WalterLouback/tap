<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenContact {
    
    
    /** @var int  */
    protected $genStructure;
    /** @var string  */
    protected $phone;
    /** @var string  */
    protected $type;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;

    public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure($genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getPhone() {
        return $this->phone;
    }
	public function setPhone($phone = NULL) {
        $this->phone = $phone;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type) {
        $this->type = $type;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiPayment\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}