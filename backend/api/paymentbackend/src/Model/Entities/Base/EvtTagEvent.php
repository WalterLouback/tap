<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtTagEvent {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenTag  */
    protected $evtTag;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtTag() {
        return $this->evtTag;
    }
	public function setEvtTag(\Zeedhi\ApiPayment\Model\Entities\GenTag $evtTag = NULL) {
        $this->evtTag = $evtTag;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}