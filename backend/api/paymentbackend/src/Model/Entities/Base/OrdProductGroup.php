<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdProductGroup {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdMenu  */
    protected $ordMenu;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var int  */
    protected $parentId;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getOrdMenu() {
        return $this->ordMenu;
    }
	public function setOrdMenu(\Zeedhi\ApiPayment\Model\Entities\OrdMenu $ordMenu = NULL) {
        $this->ordMenu = $ordMenu;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
    public function getParentId() {
        return $this->parentId;
    }
	public function setParentId($parentId = NULL) {
        $this->parentId = $parentId;
    }
}