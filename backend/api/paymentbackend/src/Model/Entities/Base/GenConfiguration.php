<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenConfiguration {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $logoImage;
    /** @var string  */
    protected $logoImageSmall;
    /** @var string  */
    protected $primaryColor;
    /** @var string  */
    protected $secondaryColor;
    /** @var string  */
    protected $yesColor;
    /** @var string  */
    protected $noColor;
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $merchantId;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var boolean  */
    protected $useIntegration;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getLogoImage() {
        return $this->logoImage;
    }
	public function setLogoImage($logoImage = NULL) {
        $this->logoImage = $logoImage;
    }
	public function getLogoImageSmall() {
        return $this->logoImageSmall;
    }
	public function setLogoImageSmall($logoImageSmall = NULL) {
        $this->logoImageSmall = $logoImageSmall;
    }
	public function getPrimaryColor() {
        return $this->primaryColor;
    }
	public function setPrimaryColor($primaryColor = NULL) {
        $this->primaryColor = $primaryColor;
    }
	public function getSecondaryColor() {
        return $this->secondaryColor;
    }
	public function setSecondaryColor($secondaryColor = NULL) {
        $this->secondaryColor = $secondaryColor;
    }
	public function getYesColor() {
        return $this->yesColor;
    }
	public function setYesColor($yesColor = NULL) {
        $this->yesColor = $yesColor;
    }
	public function getNoColor() {
        return $this->noColor;
    }
	public function setNoColor($noColor = NULL) {
        $this->noColor = $noColor;
    }
    public function getUseIntegration() {
        return $this->useIntegration;
    }
	public function setUseIntegration($useIntegration = NULL) {
        $this->useIntegration = $useIntegration;
    }
	public function getMerchantKey() {
        return $this->merchantKey;
    }
	public function setMerchantKey($merchantKey = NULL) {
        $this->merchantKey = $merchantKey;
    }
	public function getMerchantId() {
        return $this->merchantId;
    }
	public function setMerchantId($merchantId = NULL) {
        $this->merchantId = $merchantId;
    }
}