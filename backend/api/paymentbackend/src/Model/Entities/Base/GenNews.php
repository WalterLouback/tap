<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenNews {
    
    
    /** @var string  */
    protected $active;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $news;
    /** @var string  */
    protected $title;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;

    public function getActive() {
        return $this->active;
    }
	public function setActive($active = NULL) {
        $this->active = $active;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getNews() {
        return $this->news;
    }
	public function setNews($news = NULL) {
        $this->news = $news;
    }
	public function getTitle() {
        return $this->title;
    }
	public function setTitle($title) {
        $this->title = $title;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}