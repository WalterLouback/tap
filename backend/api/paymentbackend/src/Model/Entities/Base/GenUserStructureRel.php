<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenUserStructureRel {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $genStructureId;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenStructureId() {
        return $this->genStructureId;
    }
	public function setGenStructureId($genStructureId = NULL) {
        $this->genStructureId = $genStructureId;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}