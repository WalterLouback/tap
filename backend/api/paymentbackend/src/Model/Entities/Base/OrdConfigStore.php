<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdConfigStore {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\PayPaymentMethod  */
    protected $payPaymentMethod;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $event;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getPayPaymentMethod() {
        return $this->payPaymentMethod;
    }
	public function setPayPaymentMethod(\Zeedhi\ApiPayment\Model\Entities\PayPaymentMethod $payPaymentMethod = NULL) {
        $this->payPaymentMethod = $payPaymentMethod;
    }
	public function getEvent() {
        return $this->event;
    }
	public function setEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $event = NULL) {
        $this->event = $event;
    }
}