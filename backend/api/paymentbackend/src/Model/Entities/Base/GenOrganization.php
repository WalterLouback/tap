<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenOrganization {
    
    
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $nrorg = 0;

    public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
}