<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenShift {
    
    
    /** @var \Datetime  */
    protected $finalTime;
    /** @var \Datetime  */
    protected $initialTime;
    /** @var string  */
    protected $day;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEventMenu  */
    protected $evtEventMenu;
    /** @var \Zeedhi\ApiPayment\Model\Entities\EvtEvent  */
    protected $evtEvent;

    public function getFinalTime() {
        return $this->finalTime;
    }
	public function setFinalTime(\Datetime $finalTime = NULL) {
        $this->finalTime = $finalTime;
    }
	public function getInitialTime() {
        return $this->initialTime;
    }
	public function setInitialTime(\Datetime $initialTime = NULL) {
        $this->initialTime = $initialTime;
    }
	public function getDay() {
        return $this->day;
    }
	public function setDay($day = NULL) {
        $this->day = $day;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtEventMenu() {
        return $this->evtEventMenu;
    }
	public function setEvtEventMenu(\Zeedhi\ApiPayment\Model\Entities\EvtEventMenu $evtEventMenu = NULL) {
        $this->evtEventMenu = $evtEventMenu;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiPayment\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}