<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtTagUser {
    
    
    /** @var int  */
    protected $genUserId;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenTag  */
    protected $evtTag;

    public function getGenUserId() {
        return $this->genUserId;
    }
	public function setGenUserId($genUserId = NULL) {
        $this->genUserId = $genUserId;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtTag() {
        return $this->evtTag;
    }
	public function setEvtTag(\Zeedhi\ApiPayment\Model\Entities\GenTag $evtTag = NULL) {
        $this->evtTag = $evtTag;
    }
}