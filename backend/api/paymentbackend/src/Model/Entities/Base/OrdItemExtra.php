<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class OrdItemExtra {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdOrderProduct  */
    protected $ordOrderProduct;
    /** @var \Zeedhi\ApiPayment\Model\Entities\OrdExtra  */
    protected $ordExtra;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdOrderProduct() {
        return $this->ordOrderProduct;
    }
	public function setOrdOrderProduct(\Zeedhi\ApiPayment\Model\Entities\OrdOrderProduct $ordOrderProduct = NULL) {
        $this->ordOrderProduct = $ordOrderProduct;
    }
	public function getOrdExtra() {
        return $this->ordExtra;
    }
	public function setOrdExtra(\Zeedhi\ApiPayment\Model\Entities\OrdExtra $ordExtra = NULL) {
        $this->ordExtra = $ordExtra;
    }
}