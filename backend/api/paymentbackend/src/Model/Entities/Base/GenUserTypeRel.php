<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class GenUserTypeRel {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $genUserId;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserType  */
    protected $genUserType;

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenUserId() {
        return $this->genUserId;
    }
	public function setGenUserId($genUserId = NULL) {
        $this->genUserId = $genUserId;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserType() {
        return $this->genUserType;
    }
	public function setGenUserType(\Zeedhi\ApiPayment\Model\Entities\GenUserType $genUserType = NULL) {
        $this->genUserType = $genUserType;
    }
}