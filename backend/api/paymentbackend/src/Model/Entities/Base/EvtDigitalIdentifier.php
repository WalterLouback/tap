<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class EvtDigitalIdentifier {
    
    
    /** @var \Datetime  */
    protected $createDate;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $qrCode;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;

    public function getCreateDate() {
        return $this->createDate;
    }
	public function setCreateDate(\Datetime $createDate = NULL) {
        $this->createDate = $createDate;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getQrCode() {
        return $this->qrCode;
    }
	public function setQrCode($qrCode) {
        $this->qrCode = $qrCode;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}