<?php
namespace Zeedhi\ApiPayment\Model\Entities\Base;


abstract class PayWallet {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var float  */
    protected $balance;
    /** @var \Zeedhi\ApiPayment\Model\Entities\GenUser  */
    protected $genUser;

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getBalance() {
        return $this->balance;
    }
	public function setBalance($balance = NULL) {
        $this->balance = $balance;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiPayment\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}