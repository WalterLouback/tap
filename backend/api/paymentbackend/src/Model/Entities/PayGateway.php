<?php
namespace Zeedhi\ApiPayment\Model\Entities;


class PayGateway extends \Zeedhi\ApiPayment\Model\Entities\Base\PayGateway {

    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getGateway(),
            'MERCHANT_KEY' => $this->getMerchantKey()
        );
    }
    
}