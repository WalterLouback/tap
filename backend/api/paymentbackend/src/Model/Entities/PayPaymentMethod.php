<?php
namespace Zeedhi\ApiPayment\Model\Entities;


class PayPaymentMethod extends \Zeedhi\ApiPayment\Model\Entities\Base\PayPaymentMethod {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "PAYMENT_METHOD" => $this->getPaymentMethod(),
            "LABEL" => $this->getLabel()
        );
    }

    
}