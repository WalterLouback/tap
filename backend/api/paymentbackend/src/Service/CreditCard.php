<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\PayCreditcard;
use Zeedhi\ApiPayment\Model\Entities\PayGateway;
use Zeedhi\ApiPayment\Model\Entities\PayGatewayRel;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration;
use Zeedhi\ApiPayment\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;

use Zeedhi\PaymentPlatform\Customer;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\CreditCard as PlatformCreditCard;

use Zeedhi\PaymentPlatform\Implementations\MundiPagg\PaymentImpl as MundiPaggPaymentImpl;
use Zeedhi\PaymentPlatform\Implementations\Cielo\PaymentImpl as CieloPaymentImpl;

use Zeedhi\ApiGeneral\Helpers\Environment;

use Zeedhi\ApiPayment\Controller\Payment as PaymentController;

use Zeedhi\Framework\ORM\DateTime;

use Doctrine\ORM\EntityManager;

class CreditCard extends UserOperation {

    /** @var InstantBuyPlatform */
    private $paymentPlatform;

    /**
     * @param EntityManager       $entityManager
     * @param Environment         $environment
     * @param InstantBuyPlatform  $paymentPlatform
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    /**
    * Instância a PlatformCreditCard, instância o Customer, cria o token e salva o cartão de crédito no banco e o token.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param string $ownerName
    * @param int $numbers
    * @param int $expirationDateMonth
    * @param int $expirationDateYear
    * @param int $cvv
    * @param string $flag
    * @param int $user_id
    * @return PayCreditcard
    */
    public function addNewCreditCard($ownerName, $numbers, $expirationDateMonth, $expirationDateYear, $cvv, $flag, $user_id, $nrorg) {
        $gatewayData = $this->getGatewayFromOrg($nrorg);
        $gatewayName = $gatewayData->getGateway();
        $merchantKey = $gatewayData->getMerchantKey();
        $merchantId  = $gatewayData->getMerchantId();

       /*
        *@author Cristiano Soares
        */
        if(($gatewayData->getZeroAuth() == "A") && $gatewayName == "CIELO" && ($flag == "Visa" || $flag == "Master Card" || $flag == "Elo")) {
            $expirationDate = $expirationDateMonth . "/20" . $expirationDateYear;
            $saveCard       = false;
            $brand          = $flag == "Master Card" ? "Master" : $flag;
            
            $resultAuth = $this->zeroAuth($merchantId, $merchantKey, $numbers, $ownerName, $expirationDate, $cvv, $saveCard, $brand);
            
            if(isset($resultAuth["Valid"]) && $resultAuth["Valid"] == false){
                throw new \Exception('The request is invalid.', 6);
            }
        }
        
        if ($user_id !== NULL && $this->creditCardExists($user_id, $merchantKey, $ownerName, $numbers, $expirationDateYear, $expirationDateMonth, $cvv, $flag))
            throw new Exception('Credit card already exists', 13);
            
        if ($gatewayName == "CIELO") $expirationDateYear = "20" . $expirationDateYear;
            
        $isFirstCard = $user_id !== NULL ? $this->userHasNoCards($user_id) : false;
        
        try {
            $this->paymentPlatform = PaymentController::factoryGateway($gatewayName, $merchantKey, $merchantId);
            $platformCreditCard = new PlatformCreditCard($ownerName, $numbers, $expirationDateMonth, $expirationDateYear, $cvv, $flag);
            $customer = new Customer();
            $customer->setName($ownerName);
    
            try {
                $token = $this->paymentPlatform->createToken($platformCreditCard, $customer);
            } catch (\Exception $e) {
                throw new \Exception("Error adding credit card. Check your card informations and try again.", 6);
            }
            
            $num = strlen($numbers) - 4;
            $lastNumbers = substr($numbers, $num, 4);
            $creditCard = new PayCreditcard();
            
            if (strlen($expirationDateYear) == 4) $expirationDate = $expirationDateMonth."/".substr($expirationDateYear, 2, 2);
            else $expirationDate = $expirationDateMonth."/".$expirationDateYear;
            
            $creditCard->setExpirationDate($expirationDate);
            $creditCard->setCardFullName($ownerName);
            $creditCard->setFlag($flag);
            $creditCard->setLastNumbers($lastNumbers);
            $creditCard->setToken($token['InstantBuyKey']);
            if (isset($token['CustomerId'])) $creditCard->setCustomerId($token['CustomerId']);
            $creditCard->setGateway($gatewayName);
            if ($user_id !== NULL) $creditCard->setGenUser($this->getEntityManager()->getReference(GenUser::class, $user_id));
            $creditCard->setStatus('A');
            $this->getEntityManager()->persist($creditCard);
            
            if ($isFirstCard) {
                $this->setMainCard($user_id, $creditCard);
            }
            
            $this->getEntityManager()->flush();
            return $creditCard;
        } catch (\Exception $e) {
            if ($e->getCode() == 11) throw new \Exception('Unknown error adding credit card: ' . $e->getMessage(), 19);
            else throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
    
    public function getGatewayFromOrg($nrorg) {
        $payGateway       = PayGateway::class;
        $payGatewayRel    = PayGatewayRel::class;
        $genConfiguration = GenConfiguration::class;
        $gateway          = $this->getEntityManager()->createQuery(
            "
            SELECT g
            FROM $payGateway g
            JOIN $payGatewayRel gr WITH gr.payGateway = g
            JOIN gr.genConfiguration c
            WHERE c.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($gateway) == 0) throw new Exception("Gateway not configured for organization!", 140);
        
        return $gateway[0];
    }
    
    public function userHasNoCards($userId) {
        /* Checking if one of the parents is sharing creditcards*/
        $dependencies = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $userId, 'canUseMainCard' => 'T']);
        foreach ($dependencies as $dependency) {
            $parent   = $dependency->getParent();
            if ($parent != NULL && $parent->getMainCreditcard() != NULL) {
                return false;
            }
        }
        /* Gets the user's own credit cards */
        $creditCards = $this->getEntityManager()->getRepository(PayCreditcard::class)->findBy(['status' => 'A', 'genUser' => $userId]);
        return count($creditCards) == 0;
    }
    
    public function setMainCard($userId, $card) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->setMainCreditcard($card);
    }
    
    public function creditCardExists($userId, $merchantKey, $ownerName, $numbers, $expirationDateYear, $expirationDateMonth, $cvv, $flag) {
        $lastNumbers  = substr($numbers, 12, 4);
        
        /* Checking if any of the parents has this credit card */
        $dependencies = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $userId, 'canUseMainCard' => 'T']);
        foreach ($dependencies as $dependency) {
            $parent   = $dependency->getParent();
            if ($parent != NULL) {
                $cc = $parent->getMainCreditcard();
                if ($cc != NULL && $cc->getLastNumbers() == $lastNumbers && $cc->getStatus() == 'A' && $cc->getFlag() == $flag) {
                    return true;
                }
            }
        }
        
        /* Checking if this user has this credit card */
        $condition    = ['genUser' => $userId, 'lastNumbers' => $lastNumbers, 'expirationDate' => $expirationDateMonth . '/'. $expirationDateYear, 'status' => 'A'];
        $cc = $this->getEntityManager()->getRepository(PayCreditcard::class)->findOneBy($condition);
        return $cc != NULL;
    }
    
    /**
    * Get the creditcard by id.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param int $creditCard_id
    * @return PayCreditcard
    */
    public function getCreditCard($creditCard_id) {
        return $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCard_id);
    }   
    
    /**
    * Altera o status do cartão de crédito(inativa)
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    *
    * @param PayCreditcard $card
    * @return PayCreditcard $card
    */
    public function deleteCC($card) {
        $card->setStatus('I');
        
        $user     = $card->getGenUser();
        $mainCard = $user->getMainCreditcard();
        
        $this->getEntityManager()->flush();
        
        if ($mainCard != NULL && $mainCard->getId() == $card->getId()) {
            if ($this->userHasNoCards($user->getId())) {
                $user->setMainCreditcard(NULL);
            } else {
                $this->setUsersNextMainCard($user);
            }
        }
        
        $this->getEntityManager()->flush();
        
        return $card;
    }
    
    public function setUsersNextMainCard($user) {
        /* Checks if user has a card from its own */
        $card = $this->getEntityManager()->getRepository(PayCreditcard::class)->findOneBy(['genUser' => $user->getId(), 'status' => 'A']);
        
        if ($card != NULL) {
            $user->setMainCreditcard($card);
        } 
        /* Else uses the parent's main card */
        else {
            $dependencies = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $user->getId(), 'canUseMainCard' => 'T']);
            foreach ($dependencies as $dependency) {
                $parent = $dependency->getParent();
                if ($parent->getMainCreditcard() != NULL) {
                    $user->setMainCreditcard($parent->getMainCreditcard());
                }
            }
        }
    }
    
    /**
    * Gets valid credit cards.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param int $order_id
    * @return PayCreditcard $creditCards
    */
    public function getValidCreditCard($user_id) {
        $filter = array('genUser' => $user_id, 'status' => 'A');
        $creditCards = $this->getEntityManager()->getRepository(PayCreditcard::class)->findBy(['genUser' => $user_id, 'status' => 'A']);
        return $creditCards; 
    }
    
    public function getCardFromParent($userId, $nrorg) {
        if ($this->canUseCardFromParent($userId, $nrorg)) {
            $parent         = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg])->getParent();
            $cardFromParent = $parent->getMainCreditcard();
            if ($cardFromParent->getGenUser() && $cardFromParent->getGenUser()->getId() == $parent->getId())
                return $cardFromParent;
        }
        else return NULL;
    }
    
    public function canUseCardFromParent($userId, $nrorg) {
        $dependency = $this->getEntityManager()->createQuery(
            "
            SELECT d
            FROM '\Zeedhi\ApiGeneral\Model\Entities\GenDependent' d
            WHERE d.dependent = $userId
            AND d.nrorg = $nrorg
            "
        )->getResult();
        if (count($dependency) > 0) {
            return $dependency[0]->getCanUseMainCard() == 'T';
        } else return false;
    }
    
    public function getMainCreditcard($userId) {
        return $this->getEntityManager()->getRepository(GenUser::class)->find($userId)->getMainCreditcard();
    }
    
    /**
    * 
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param 
    * @return 
    */
    public function getUser($user_id) {
       $user = $this->getEntityManager()->getRepository(GenUser::class)->find($user_id);
       return $user; 
    }
    
    /*
     *@author Cristiano Soares
     */
    public function consumesWebService($url, $card, $header, $typeRequest) {
        $data_string = json_encode($card, JSON_UNESCAPED_SLASHES);
        
        // inicia seção CURL 
        $curl = curl_init();
        $headers = [
                'Content-Type: application/json',
                'MerchantId:'       . $header['MerchantId'],
                'MerchantKey:'      . $header['MerchantKey'],
                'Content-Length:'   . strlen($data_string)
        ];
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);             
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $typeRequest);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode($result, true);
        
        return $objResult;
    }
    
    /*
     *@author Cristiano Soares
     */
    public function zeroAuth($merchantId, $merchantKey, $cardNumber, $holder, $expirationDate, $securityCode, $saveCard, $brand){
        $header['MerchantId']   = $merchantId;
        $header['MerchantKey']  = $merchantKey;
        $typeRequest            = "POST";
        $card['CardNumber']     = $cardNumber;
        $card['Holder']         = $holder;
        $card['ExpirationDate'] = $expirationDate;
        $card['SecurityCode']   = $securityCode;
        $card['SaveCard']       = $saveCard;
        $card['Brand']          = $brand;
       
        // $url = "https://apisandbox.cieloecommerce.cielo.com.br/1/zeroauth"; //Dev
        $url = "https://api.cieloecommerce.cielo.com.br/1/zeroauth"; //Produção
        
        return $this->consumesWebService($url, $card, $header, $typeRequest);
    }
    
    /*
     *@author Cristiano Soares
     */
    public function consultBin($merchantId, $merchantKey, $sixFirstCardNumbers) {
        $header['MerchantId']   = $merchantId;
        $header['MerchantKey']  = $merchantKey;
        $typeRequest            = "GET";
        $card                   = "{}";
        
        // $url = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/cardbin/$sixFirstCardNumbers"; //Dev
        $url = "https://apiquery.cieloecommerce.cielo.com.br/1/cardbin/$sixFirstCardNumbers"; //Produção
        return $this->consumesWebService($url, $card, $header, $typeRequest);
    }
}