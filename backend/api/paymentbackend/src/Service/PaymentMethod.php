<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\PayPaymentMethod;
use Zeedhi\ApiPayment\Model\Entities\OrdOrder;

use Doctrine\ORM\EntityManager;

class PaymentMethod {
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function getPicpayTokens($orderId) {
        $paymentMethod = PayPaymentMethod::class;
        $order = OrdOrder::class;
        
        $queryResult = $this->entityManager->createQuery(
            "
            SELECT pm.xSellerToken, pm.xPicpayToken
            FROM $paymentMethod pm
            JOIN $order o WITH o.id = '$orderId'
            WHERE pm.paymentMethod = o.paymentMethod
            "
        )->getResult();
        
        if (count($queryResult) > 0) {
            return $queryResult[0];
        } else return [];
    }
    
}