<?php
namespace Zeedhi\ApiPayment\Service;

use Doctrine\ORM\EntityManager;
use Zeedhi\ApiPayment\Model\Entities\BpUser;

use Facebook\Facebook;

use Zeedhi\ApiGeneral\Helpers\Environment as Environment;

abstract class UserOperation {

    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;

    /**
     * UserOperation constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment = $environment;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * @return Environment
     */
    protected function getEnvironment() {
        return $this->environment;
    }

    /**
     * @return BpUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function getCurrentUser() {
        return $this->entityManager->find(BpUser::class, $this->environment->getUserId());
    }
}