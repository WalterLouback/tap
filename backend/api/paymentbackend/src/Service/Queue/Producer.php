<?php
namespace Zeedhi\ApiPayment\Service\Queue;

use Zeedhi\Queue\Producer as ProducerBase;
use Zeedhi\Queue\Connection;

class Producer extends ProducerBase {
    
    public function __construct(Connection $connection, $queueName) {
        static::$queueName = $queueName;
        parent::__construct($connection);
    }

}