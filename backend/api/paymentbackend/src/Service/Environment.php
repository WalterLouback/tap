<?php
namespace Zeedhi\ApiPayment\Service;

interface Environment {

    public function getUserId();

    public function setUserId($userId);

}