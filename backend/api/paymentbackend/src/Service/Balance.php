<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\OrdOrder;
use Zeedhi\ApiPayment\Model\Entities\PayWallet;
use Zeedhi\ApiPayment\Model\Entities\GenUserTypeRel;

use Zeedhi\ApiPayment\Service\Exception as PayException;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiGeneral\Helpers\Environment as Environment;

class Balance extends UserOperation {
    
    /**
     * Payment constructor.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getWalletByUser($userId, $nrorg) {
        return $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy(array('genUser'   => $userId, 'nrorg' => $nrorg));
    }
    
    public function getWalletById($walletId) {
        return $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId);
    }
    
    /**
     * create wallet.
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     */
    public function createWallet($userTypeRelId){
        $wallet = new PayWallet();
        $wallet->setGenUserTypeRel($userTypeRelId);
        $wallet->setBalance(0);
        $this->getEntityManager()->persist($wallet);
        $this->getEntityManager()->flush();
        return $wallet;
    }    
    
    public function updateAll(){
        $this->getEntityManager()->flush();
    }
}

?>