PAYMENT FLOW:
	
Description

	The payment  API was developed to unify the implementation of Zeedhi's payment platform, the transactions with the wallet balance and the registration of the credit card along with its maintenance.
	
How to Use
	Its use is very simple, just call the route and pass the id of the order, done that the payment API takes care of the rest

	When it receive the request, the API verifies if the order exists, if it has paid status or not, if the payment method is valid, if the payment method is credit card it checks if exists a card bound to that order. Exceptions are thrown if it does not meet the specified conditions
	
	Only payments with PAYMENT_STATUS different than A are processed.

	After doing all the verifications the API processes the payment and returns the order already updated in the DB, in case it is a credit transaction to the wallet the API also updates the balance of it, the same happens when it is a transaction made with the wallet	

	When receiving the order after the function is called, what indicates if the payment was authorized is the field PAYMENT_STATUS:
		PAYMENT_STATUS = A – Transação autorizada.
		PAYMENT_STATUS = N – Transação não autorizada.)

ROUTES

	execPayment (int ORDER_ID): integer required:  To make transactions using the  API, just call the execPayment route and send the order id as a parameter.

	
	CREDITCARD:
Description
	The payment  API also takes care of credit card routines. Using  the API, you can register a card, delete (inactivate) and get the active credit cards.


How to Use
	To register a card just send a request containing the data of the card.
	To delete (inactivate) a card simply send the id of the card.
	To get a card just send the id of the genUser.
ROUTES
	addCreditCard(string CARDFULLNAME, int NUMBERS, int CVV, string FLAG, int GENUSERID): To register a card just call the route and send as parameter the data . All data is required
	deleteCC(int CREDITCARD_ID): To inactivate a card simply send the id of the card
	getValidCreditCard(int GENUSERID): To obtain the cards of a user, simply call the route passing the GENUSERID as parameter.
	
