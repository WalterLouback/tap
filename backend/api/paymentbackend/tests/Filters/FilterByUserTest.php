<?php
namespace Zeedhi\ApiPayment\Filters;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ReflectionClass;
use Doctrine\ORM\Query\FilterCollection;

use Zeedhi\ApiPayment\Model\Filters\FilterByUser;

class filterByUserTest extends \PHPUnit_Framework_TestCase {
    /** */
    protected $targetEntity;
    /** */
    protected $targetTableAlias;
    
    protected $filterByUser;
    
    protected $entityManager;
    
    public function setUp() {
        $this->targetEntity = $this->getMockBuilder(ClassMetadata::class)
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $this->targetEntity->reflClass = $this->getMockBuilder(ReflectionClass::class)
                                              ->setMethods(array('implementsInterface'))
                                              ->disableOriginalConstructor()
                                              ->getMock();
                                              
        $this->entityManager = $this->getMockBuilder(EntityManager::class)
                ->disableOriginalConstructor()
                ->getMock();
                                              
        $this->filterByUser = new FilterByUser($this->entityManager);
    }
    
    public function testNotImplementedInterface() {
        
        $this->targetEntity->reflClass->method("implementsInterface")
            ->with('\\Zeedhi\\ApiPayment\\Model\\Filters\\BelongToUser')
            ->willReturn(false);
        
        $condition = $this->filterByUser->addFilterConstraint($this->targetEntity, "targetTableAlias");
        $this->assertEquals("", $condition);
    }
    
    public function testImplementedInterfaceWithoutParameter() {
        $this->targetEntity->reflClass->method("implementsInterface")
            ->with('\\Zeedhi\\ApiPayment\\Model\\Filters\\BelongToUser')
            ->willReturn(true);
        $condition = $this->filterByUser->addFilterConstraint($this->targetEntity, "targetTableAlias");
        $this->assertEquals("1 = 2", $condition);
    }
    
    public function testImplementedInterfaceWithParameter() {
        $this->targetEntity->reflClass->method("implementsInterface")
            ->with('\\Zeedhi\\ApiPayment\\Model\\Filters\\BelongToUser')
            ->willReturn(true);

        $filterCollectionMock = $this->getMockBuilder(FilterCollection::class)
            ->setMethods(['setFiltersStateDirty'])
            ->disableOriginalConstructor()
            ->getMock();
            
        $this->entityManager->method("getFilters")->willReturn($filterCollectionMock);
            
        $this->filterByUser->setParameter(FilterByUser::USER_ID, 123);
        
        $mockConnection = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(["quote"])->getMock();
        
        $mockConnection->method("quote")->with(123, "integer")->willReturn("123");
        $this->entityManager->method("getConnection")->willReturn($mockConnection);
            
        $condition = $this->filterByUser->addFilterConstraint($this->targetEntity, "targetTableAlias");
        $this->assertEquals("targetTableAlias.USER_ID = 123", $condition);
    }
}