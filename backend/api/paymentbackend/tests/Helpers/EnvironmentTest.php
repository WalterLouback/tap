<?php
namespace Zeedhi\ApiPayment\Helpers;

use Zeedhi\ApiPayment\Helpers\Environment;

use Zeedhi\Framework\Session\Session;

class filterByUserTest extends \PHPUnit_Framework_TestCase {

    /** @var Session|\PHPUnit_Framework_MockObject_MockObject */
    protected $session;
    /** @var Environment */    
    protected $environment;
    
    public function setUp(){
        $this->session = $this->getMockBuilder(Session::class)
                              ->setMethods(array('get','set'))
                              ->disableOriginalConstructor()
                              ->getMock();

        $this->environment = new Environment($this->session);
    }
    
    public function testSetUserId() {
        $userId = 123;
        
        $this->session->expects($this->once())
            ->method('set')
            ->with($this->equalTo(Environment::USER_ID_KEY), $this->equalTo($userId));

        $this->environment->setUserId($userId);
    }
    
    public function testGetUserId() {
        $expectId = 123;
        
        $this->session->expects($this->once())
                      ->method('get')
                      ->with(Environment::USER_ID_KEY)
                      ->willReturn($expectId);

        $id = $this->environment->getUserId();
        
        $this->assertEquals($expectId, $id);
    }
}