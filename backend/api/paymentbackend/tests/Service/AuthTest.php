<?php
namespace Zeedhi\ApiPayment\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Facebook\Facebook;
use Facebook\FacebookResponse;
use Facebook\Authentication\AccessToken;
use Facebook\Authentication\OAuth2Client;

use Zeedhi\ApiPayment\Model\Entities\BpUser;
use Zeedhi\ApiPayment\Model\Entities\BpWallet;

class AuthTest extends \PHPUnit_Framework_TestCase
{

    /** @var EntityManager|\PHPUnit_Framework_MockObject_MockObject */
    protected $entityManager;
    /** @var Environment */
    protected $environment;
    /** @var Auth */
    protected $authService;
    /** @var Facebook */
    protected $facebook;
    
    public function setUp() {
        
        $this->entityManager = $this->getMockBuilder(EntityManager::class)
                                    ->setMethods(array('getRepository', 'persist', 'flush', 'find'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->environment = new SimpleEnvironmentImpl();
        
        $this->facebook = $this->getMockBuilder(Facebook::class)
                               ->setMethods(array('get', 'getOAuth2Client'))
                               ->disableOriginalConstructor()
                               ->getMock();
        
        $this->authService = new Auth($this->entityManager, $this->environment, $this->facebook);
    }
    
    public function testLogin() {
        $email = "joao.martins@teknisa.com";
        $password = '123456';

        $userRepository =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();
                                
        $this->entityManager->expects($this->once())
                            ->method('getRepository','find')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);
                            
                            
        $expectedEncryptedPassword = 'b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff';
        $expectedFilter = array(
            'email' => $email,
            'password' => $expectedEncryptedPassword
        );
        
        $user = new BpUser();
        $user->setId(1);
        $user->setFirstName('João Paulo');
        $user->setLastName('Martins');
        $user->setEmail('joao.martins@teknisa.com');
        $user->setPhone('25853696');
        $user->setImage('path/to/picture.jpg');
        $user->setPassword($expectedEncryptedPassword);

        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with($expectedFilter)
                       ->willReturn($user);
                           
        $this->authService->login($email, $password);

        $this->assertEquals(1, $this->environment->getUserId());
    }
    
    public function testLogged(){
        $userID = 18;
        $encryptedPassword = 'b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff';
        
        $userRepository =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();
                                
        $this->entityManager->expects($this->once())
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);
                 
        $user = new BpUser();
        $user->setId(18);
        $user->setFirstName('Diogenes');
        $user->setLastName('Morais');
        $user->setEmail('diorni@teknisa.com');
        $user->setPhone('25853696');
        $user->setImage('path/to/picture.jpg');
        $user->setPassword($encryptedPassword);
        
        $expectedFilter = array(
            'id' => $userID
        );

        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with($expectedFilter)
                       ->willReturn($user);
                           
        $this->authService->logged($userID);

        $this->assertEquals(18, $this->environment->getUserId());                            
    }

    public function testLoggedWithFacebookTokenNotNull(){
        $user = new BpUser();
        $user->setId(18);
        $user->setFirstName('Diogenes');
        $user->setLastName('Morais');
        $user->setEmail('diorni@teknisa.com');
        $user->setPhone('25853696');
        $user->setImage('path/img.jpg');
        $user->setFacebookId("55568");
        $user->setFacebookToken("65156456465465156464654");
        $user->setFacebookTokenExpDate(new \Datetime);
        $user->setPassword('b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff');

        $expectedUser = new BpUser();
        $expectedUser->setId(18);
        $expectedUser->setFirstName('Diogenes');
        $expectedUser->setLastName('Morais');
        $expectedUser->setEmail('diorni@teknisa.com');
        $expectedUser->setPhone('25853696');
        $expectedUser->setImage('path/img.jpg');
        $expectedUser->setFacebookId("55568");
        $expectedUser->setFacebookToken("65156456465465156464654");
        $expectedUser->setFacebookTokenExpDate(new \Datetime);
        $expectedUser->setPassword('b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff');

        $this->entityManager->expects($this->once())
                            ->method('persist')
                            ->with($user);             

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'diorni@teknisa.com',
                            'name' => 'Diogenes Morais',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "65156456465465156464654")
                       ->willReturn($facebookResponse);
        
        $userRepository =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();
                                
        $this->entityManager->expects($this->once())
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);

        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('id' => 18))
                       ->willReturn($user);
                           
        $this->authService->logged(18);

        $this->assertEquals(18, $this->environment->getUserId()); 
        $this->assertEquals($expectedUser, $user);                           
    }
    
    public function testLoggedExceptionNoOneFound() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('User ID not found on Database!');
        $this->expectExceptionCode(Exception::USER_ID_NOT_FOUND);
        
        $userRepository = $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();
                                
        $this->entityManager->expects($this->once())
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);

        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with(array("id" => 18))
                       ->willReturn(null);
        
        $this->authService->logged(18);
    }

    public function testLoginFacebook(){
        $expectedUser = new BpUser();
        $expectedUser->setFirstName('Lucas');
        $expectedUser->setLastName('Lacerda');
        $expectedUser->setEmail('lucas.lacerda@teknisa.com');
        $expectedUser->setImage('path/img.jpg');
        $expectedUser->setFacebookId("55568");
        $expectedUser->setFacebookToken("65156456465465156464654");
        $expectedUser->setFacebookTokenExpDate(new \Datetime);

        $wallet = new BpWallet();
        $wallet->setUser($expectedUser);
    
        $this->entityManager->expects($this->exactly(2))
                            ->method('persist')
                            ->withConsecutive($wallet, $expectedUser);             

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $facebookAccessToken = $this->getMockBuilder(AccessToken::class)
                                    ->setMethods(array('getValue', 'getExpiresAt'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $facebookAccessToken->expects($this->once())
                            ->method('getValue')
                            ->willReturn("65156456465465156464654");

        $facebookAccessToken->expects($this->once())
                            ->method('getExpiresAt')
                            ->willReturn(new \Datetime);
        
        $userRepositoryFacebook =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();

        $userRepositoryFacebook->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('facebookId' => '55568'));

        $userRepositoryEmail =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();

        $userRepositoryEmail->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('email' => 'lucas.lacerda@teknisa.com'));

        $this->entityManager->expects($this->exactly(2))
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturnOnConsecutiveCalls($userRepositoryFacebook, $userRepositoryEmail);

        $facebookResponseOAuth2Client = $this->getMockBuilder(OAuth2Client::class)
                                             ->setMethods(array('getLongLivedAccessToken'))
                                             ->disableOriginalConstructor()
                                             ->getMock();

        $facebookResponseOAuth2Client->expects($this->once())
                                     ->method('getLongLivedAccessToken')
                                     ->willReturn($facebookAccessToken);

        $this->facebook->expects($this->once())
                       ->method('getOAuth2Client')
                       ->willReturn($facebookResponseOAuth2Client);


        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'lucas.lacerda@teknisa.com',
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $user = $this->authService->loginFacebook("123456");
        $this->assertEquals($expectedUser, $user);
    }

    public function testLoginFacebookWithEmailAlreadyRegister(){
        $expectedUser = new BpUser();
        $expectedUser->setId(1);
        $expectedUser->setFirstName('Lucas');
        $expectedUser->setLastName('Lacerda');
        $expectedUser->setEmail('lucas.lacerda@teknisa.com');
        $expectedUser->setImage('path/img.jpg');
        $expectedUser->setFacebookId("55568");
        $expectedUser->setFacebookToken("65156456465465156464654");
        $expectedUser->setFacebookTokenExpDate(new \Datetime);

        $user = new BpUser();
        $user->setId(1);
        $user->setFirstName('Lucas');
        $user->setLastName('Lacerda');
        $user->setEmail('lucas.lacerda@teknisa.com');
        $user->setImage('path/to/picture.jpg');
    
        $this->entityManager->expects($this->once())
                            ->method('persist')
                            ->with($user);             

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $facebookAccessToken = $this->getMockBuilder(AccessToken::class)
                                    ->setMethods(array('getValue', 'getExpiresAt'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $facebookAccessToken->expects($this->once())
                            ->method('getValue')
                            ->willReturn("65156456465465156464654");

        $facebookAccessToken->expects($this->once())
                            ->method('getExpiresAt')
                            ->willReturn(new \Datetime);
        
        $userRepositoryFacebook =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();

        $userRepositoryFacebook->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('facebookId' => '55568'));

        $userRepositoryEmail =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();

        $userRepositoryEmail->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('email' => 'lucas.lacerda@teknisa.com'))
                       ->willReturn($user);

        $this->entityManager->expects($this->exactly(2))
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturnOnConsecutiveCalls($userRepositoryFacebook, $userRepositoryEmail);

        $facebookResponseOAuth2Client = $this->getMockBuilder(OAuth2Client::class)
                                             ->setMethods(array('getLongLivedAccessToken'))
                                             ->disableOriginalConstructor()
                                             ->getMock();

        $facebookResponseOAuth2Client->expects($this->once())
                                     ->method('getLongLivedAccessToken')
                                     ->willReturn($facebookAccessToken);

        $this->facebook->expects($this->once())
                       ->method('getOAuth2Client')
                       ->willReturn($facebookResponseOAuth2Client);


        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'lucas.lacerda@teknisa.com',
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $user = $this->authService->loginFacebook("123456");
        
        $this->assertEquals(1, $this->environment->getUserId());
        $this->assertEquals($expectedUser, $user);
    }

    public function testLoginFacebookUserAlreadyRegister(){   
        $user = new BpUser();
        $user->setId(1);
        $user->setFirstName('Lucas');
        $user->setLastName('Lacerda');
        $user->setEmail('lucas.lacerda@teknisa.com');
        $user->setImage('path/to/picture.jpg');
        $user->setFacebookId("55568");
        $user->setFacebookToken("369852147741258");
        $user->setFacebookTokenExpDate(new \Datetime);

        $expectedUser = new BpUser();
        $expectedUser->setId(1);
        $expectedUser->setFirstName('Lucas');
        $expectedUser->setLastName('Lacerda');
        $expectedUser->setEmail('lucas.lacerda@teknisa.com');
        $expectedUser->setImage('path/img.jpg');
        $expectedUser->setFacebookId("55568");
        $expectedUser->setFacebookToken("65156456465465156464654");
        $expectedUser->setFacebookTokenExpDate(new \Datetime);


        $this->entityManager->expects($this->once())
                            ->method('persist')
                            ->with($user);

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $facebookAccessToken = $this->getMockBuilder(AccessToken::class)
                                    ->setMethods(array('getValue', 'getExpiresAt'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $facebookAccessToken->expects($this->once())
                            ->method('getValue')
                            ->willReturn("65156456465465156464654");

        $facebookAccessToken->expects($this->once())
                            ->method('getExpiresAt')
                            ->willReturn(new \Datetime);
        
        $userRepository =  $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();

        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with(array('facebookId' => '55568'))
                       ->willReturn($user);

        $this->entityManager->expects($this->once())
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);

        $facebookResponseOAuth2Client = $this->getMockBuilder(OAuth2Client::class)
                                             ->setMethods(array('getLongLivedAccessToken'))
                                             ->disableOriginalConstructor()
                                             ->getMock();

        $facebookResponseOAuth2Client->expects($this->once())
                                     ->method('getLongLivedAccessToken')
                                     ->willReturn($facebookAccessToken);

        $this->facebook->expects($this->once())
                       ->method('getOAuth2Client')
                       ->willReturn($facebookResponseOAuth2Client);


        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'lucas.lacerda@teknisa.com',
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $user = $this->authService->loginFacebook("123456");
        
        $this->assertEquals(1, $this->environment->getUserId());
        $this->assertEquals($expectedUser, $user);
    }

    public function testLoginFacebookUserExceptionWithOutEmail(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Your e-mail was not provided.");
        $this->expectExceptionCode(Exception::INVALID_FACEBOOK);

        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $this->authService->loginFacebook("123456");
    }

    public function testAutomaticLoginFacebook(){
        $user = new BpUser();
        $user->setId(1);
        $user->setFirstName('Lucas');
        $user->setLastName('Lacerda');
        $user->setEmail('lucas.lacerda@teknisa.com');
        $user->setImage('path/img.jpg');
        $user->setFacebookId("55568");
        $user->setFacebookToken("123456");
        $user->setFacebookTokenExpDate(new \Datetime);

        $expectedUser = new BpUser();
        $expectedUser->setId(1);
        $expectedUser->setFirstName('Lucas');
        $expectedUser->setLastName('Lacerda');
        $expectedUser->setEmail('lucas.lacerda@teknisa.com');
        $expectedUser->setImage('path/img.jpg');
        $expectedUser->setFacebookId("55568");
        $expectedUser->setFacebookToken("123456");
        $expectedUser->setFacebookTokenExpDate(new \Datetime);
    
        $this->entityManager->expects($this->once())
                            ->method('persist')
                            ->with($user);             

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'lucas.lacerda@teknisa.com',
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $this->authService->automaticLoginFacebook($user);

        $this->assertEquals($expectedUser, $user);
    }

    public function testAutomaticLoginFacebookWithExceptionFacebookTokenExpired(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Facebook token expired.");
        $this->expectExceptionCode(Exception::FACEBOOK_TOKEN_EXPIRED);

        $user = new BpUser();
        $user->setFirstName('Lucas');
        $user->setLastName('Lacerda');
        $user->setEmail('lucas.lacerda@teknisa.com');
        $user->setImage('path/img.jpg');
        $user->setFacebookId("55568");
        $user->setFacebookToken("123456");
        $user->setFacebookTokenExpDate(new \Datetime('2000-01-01'));

        $facebookResponse = $this->getMockBuilder(FacebookResponse::class)
                                 ->setMethods(array('getGraphUser'))
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $facebookResponse->expects($this->once())
                          ->method('getGraphUser')
                          ->willReturn(array(
                            'id' => '55568', 
                            'email' => 'lucas.lacerda@teknisa.com',
                            'name' => 'Lucas Lacerda',
                            'picture' => array(
                                'url' => 'path/img.jpg'
                            )
                          ));

        $this->facebook->expects($this->once())
                       ->method('get')
                       ->with('/me?fields=id,name,email,picture.width(700).height(700)', "123456")
                       ->willReturn($facebookResponse);

        $this->authService->automaticLoginFacebook($user);
    }

    public function testLoginUserNotFound() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('User not found!');
        $this->expectExceptionCode(Exception::USER_NOT_FOUND);

        $email = 'foo@bar.com';
        $password = 'foo';
        
        $userRepository = $this->getMockBuilder(EntityRepository::class)
                                ->setMethods(array('findOneBy'))
                                ->disableOriginalConstructor()
                                ->getMock();
                                
        $this->entityManager->expects($this->once())
                            ->method('getRepository')
                            ->with(BpUser::class)
                            ->willReturn($userRepository);

        $expectedEncryptedPassword = '4a0ac85bfb8e892d0d825087cc1adb0d317ae77279d48e066e2742f8bc0b4f4263a7c09e495d72eb456beda288d66881318f7957687740dedd1e622cde0c2c45';
        $expectedFilter = array(
            'email' => 'foo@bar.com',
            'password' => $expectedEncryptedPassword
        );                            
        $userRepository->expects($this->once())
                       ->method('findOneBy')
                       ->with($expectedFilter)
                       ->willReturn(null);
        
        $this->authService->login($email, $password);
    }

    public function testRegister() {
        $row = array(
            'EMAIL' => 'jane.doe@teknisa.com',
            'FIRST_NAME' => 'Jane',
            'LAST_NAME' => 'Doe',
            'PHONE' => 25853696,
            'IMAGE' => 'path/to/nowhere',
            'PASSWORD' => '123456'
        );
        
        $expectedUser = new BpUser();
        $expectedUser->setEmail('jane.doe@teknisa.com');
        $expectedUser->setFirstName('Jane');
        $expectedUser->setLastName('Doe');
        $expectedUser->setPhone('25853696');
        $expectedUser->setImage('path/to/nowhere');
        $expectedUser->setPassword('b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff');
        
        $expectedWallet = new BpWallet();
        $expectedWallet->setUser($expectedUser);
        
        $this->entityManager->expects($this->exactly(2))
                            ->method('persist')
                            ->withConsecutive(
                                 $expectedWallet,
                                 $expectedUser
                             );

        $this->entityManager->expects($this->once())
                            ->method('flush');
        
        $user = $this->authService->register($row);
        
        $this->assertEquals($expectedUser, $user);
    }    

    public function testLogout() {
        $oldUserId = 123;
        $this->environment->setUserId($oldUserId);
        $this->authService->logout();
        $this->assertNull($this->environment->getUserId());
    }

    public function testChangePass() {
        $userId = 1;

        $this->environment->setUserId($userId);

        $expectedEncryptedPassword = 'b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff';

        $user = new BpUser();
        $user->setId($userId);
        $user->setFirstName('João Paulo');
        $user->setLastName('Martins');
        $user->setEmail('joao.martins@teknisa.com');
        $user->setPhone('25853696');
        $user->setImage('path/to/picture.jpg');
        $user->setPassword($expectedEncryptedPassword);

        $this->entityManager->expects($this->once())
            ->method('find')
            ->with(BpUser::class, $userId)
            ->willReturn($user);

        $oldPassword = '123456';
        $newPassword = '012345';

        $this->authService->changePassword($oldPassword, $newPassword);

        $expectedEncryptedNewPassword = '6930b741b3308ab7656a5ad12f31700fc280d1b7acd9bf4ae9373d4be27e4bd9da2e18136bc9b2408b1510e6905b9cb43d0f85c65bd0ea8cac8ce751f96dd20b';

        $this->assertEquals($expectedEncryptedNewPassword, $user->getPassword());
    }

    public function testChangePassWithWrongOldPassword() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('The current password is wrong!');
        $this->expectExceptionCode(Exception::WRONG_OLD_PASSWORD);

        $userId = 1;

        $this->environment->setUserId($userId);

        $expectedEncryptedPassword = 'b3d32f44a4478cd3f773453aa4c0ad0bd83fb668c665f90a931d013909588ce2d79932ea7cbec3605f464508716d99a0156a41c835f70d4c4de4346afda143ff';

        $user = new BpUser();
        $user->setId($userId);
        $user->setFirstName('João Paulo');
        $user->setLastName('Martins');
        $user->setEmail('joao.martins@teknisa.com');
        $user->setPhone('25853696');
        $user->setImage('path/to/picture.jpg');
        $user->setPassword($expectedEncryptedPassword);

        $this->entityManager->expects($this->once())
            ->method('find')
            ->with(BpUser::class, $userId)
            ->willReturn($user);

        $oldPassword = '12345';
        $newPassword = '012345';

        $this->authService->changePassword($oldPassword, $newPassword);
    }
    
    public function testUpdate() {
        $row = array(
            'EMAIL' => 'diorni@teknisa.com',
            'FIRST_NAME' => 'Diogenes',
            'LAST_NAME' => 'Morais',
            'PHONE' => 72178733,
            'IMAGE' => array(array('path' => 'path/to/image.jpg')),
            'PASSWORD' => 'teknisa'
        );
        
        $expectedUser = new BpUser();
        $expectedUser->setId(1);
        $expectedUser->setFirstName("Diogenes");
        $expectedUser->setLastName("Morais");
        $expectedUser->setPassword("teknisa");
        $expectedUser->setPhone(72178733);
        $expectedUser->setImage("path/to/image.jpg");
        $expectedUser->setEmail("diorni@teknisa.com");
        
        $this->entityManager->expects($this->once())
             ->method('find')
             ->with(BpUser::class)
             ->willReturn($expectedUser);  
        
        $this->entityManager->expects($this->once())
             ->method('flush');
        
        $user = $this->authService->update($row);
        $this->assertEquals($expectedUser, $user);     
             
        $this->assertEquals(1, $this->environment->getUserId());
    }
}
