<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\BpCreditcard;
use Zeedhi\ApiPayment\Model\Entities\BpWallet;
use Zeedhi\ApiPayment\Model\Entities\BpUser;
use Zeedhi\ApiPayment\Model\Entities\BpOrder;
use Zeedhi\ApiPayment\Service\Queue\Producer;
use Zeedhi\ApiPayment\Service\Order as OrderService;

use Doctrine\ORM\EntityManager;

class WalletTest extends \PHPUnit_Framework_TestCase {
    
    /** @var Wallet */
    protected $wallet;
    /** @var OrderService */
    private $orderService;

    protected function setUp() {
        $this->orderService = $this->getMockBuilder(OrderService::class)
                                   ->setMethods(array('processOrderWallet'))
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $this->wallet = new Wallet($this->orderService);
    }
    
    public function testProcessOrder() {
        $selectCardId = 1;
        $buyingValue = 100;
        
        $this->orderService->expects($this->once())
             ->method('processOrderWallet')
             ->with($selectCardId, $buyingValue);
        
        $this->wallet->processOrder($selectCardId, $buyingValue);
    }
}