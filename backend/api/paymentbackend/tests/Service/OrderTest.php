<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\BpCreditcard;
use Zeedhi\ApiPayment\Model\Entities\BpOrder;
use Zeedhi\ApiPayment\Model\Entities\BpProduct;
use Zeedhi\ApiPayment\Model\Entities\BpProductOrder;
use Zeedhi\ApiPayment\Model\Entities\BpUser;
use Zeedhi\ApiPayment\Model\Entities\BpWallet;
use Zeedhi\ApiPayment\Model\Entities\BpEvent;
use Zeedhi\ApiPayment\Service\Queue\Producer;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction as PlatformInstantBuyTransaction;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class OrderTest extends \PHPUnit_Framework_TestCase {

    /** @var Order */
    protected $order;
    /** @var EntityManager|\PHPUnit_Framework_MockObject_MockObject */
    private $entityManager;
    /** @var EntityRepository|\PHPUnit_Framework_MockObject_MockObject */
    private $orderRepository;
    /** @var SimpleEnvironmentImpl */
    private $environment;
    /** @var Producer */
    private $producer;

    protected function setUp() {
        $this->environment = new SimpleEnvironmentImpl();
        $this->environment->setUserId(1);

        $this->entityManager = $this->getMockBuilder(EntityManager::class)
            ->setMethods(['persist', 'flush', 'find', 'getRepository'])
            ->disableOriginalConstructor()
            ->getMock();
                       
        $this->producer = $this->getMockBuilder(Producer::class)
                               ->setMethods(array('send'))
                               ->disableOriginalConstructor()
                               ->getMock();

        $this->orderRepository = $this->getMockBuilder(EntityRepository::class)
            ->setMethods(['findOneBy'])
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->walletRepository = $this->getMockBuilder(EntityRepository::class)
             ->setMethods(['findOneBy'])
             ->disableOriginalConstructor()
             ->getMock();

        $this->order = new Order($this->entityManager, $this->environment, $this->producer);
    }

    public function testCreateOrder() {
        $bpUser = new BpUser();
        $bpUser->setId(1);

        $bpCard = new BpCreditcard();
        $bpCard->setUser($bpUser);
        $bpCard->setId(1);
        $bpCard->setToken('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');
        
        $bpEvent = new BpEvent();
        $bpEvent->setId(1);

        $bpProduct = new BpProduct();
        $bpProduct->setId(1);
        $bpProduct->setPrice(10);

        $this->entityManager->expects($this->exactly(4))
             ->method('find')
             ->withConsecutive(
                 array(BpUser::class, 1),
                 array(BpCreditcard::class, 1),
                 array(BpEvent::class, 1),
                 array(BpProduct::class, 1)
             )
             ->willReturnOnConsecutiveCalls($bpUser, $bpCard, $bpEvent, $bpProduct);

        $this->entityManager->expects($this->exactly(2))
             ->method('persist')
             ->withConsecutive(
                 [$this->callback(function($bpOrder) use ($bpUser, $bpCard) {
                     if ($bpOrder instanceof BpOrder) {
                         $bpOrder->setId(1);
                     }
                     
                     return $bpOrder instanceof BpOrder
                         //&& $bpOrder->getStatus() === BpOrder::STATUS_PAYMENT_PENDING
                         && $bpOrder->getCreditcard() === $bpCard
                         && $bpOrder->getUser() === $bpUser
                         && $bpOrder->getCreateDate() instanceof \DateTime;
                 })],
                 [$this->callback(function($bpProductOrder) use ($bpProduct) {
                     return $bpProductOrder instanceof BpProductOrder
                         && $bpProductOrder->getProduct() === $bpProduct
                         && $bpProductOrder->getQuantity() === 3;
                 })]
             );

        $this->entityManager->expects($this->once())
            ->method('flush');

        $products = array(array("PRODUCT_ID" => 1, "QUANTITY" => 3));

        $bpOrder = $this->order->processOrder($products, 1, 1);
        
        $this->assertEquals(30, $bpOrder->getTotal());
    }
    
    public function testCreateOrderWallet(){
        $bpUser = new BpUser();
        $bpUser->setId(1);
        
        $bpCard = new BpCreditcard();
        $bpCard->setId(1);
        $bpCard->setUser($bpUser);
        $bpCard->setToken('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');
        
        $bpWallet = new BpWallet();
        $bpWallet->setId(1);
        $bpWallet->setBalance(100);
        $bpWallet->setUser($bpUser);
        
        $this->entityManager->expects($this->exactly(2))
             ->method('find')
             ->withConsecutive(
                 array(BpUser::class, 1),
                 array(BpCreditcard::class, 1)
             )
             ->willReturnOnConsecutiveCalls($bpUser, $bpCard);
        
        $this->entityManager->expects($this->once())
            ->method('persist')
            ->withConsecutive(
                [$this->callback(function($bpOrder) use ($bpUser, $bpCard) {
                    if ($bpOrder instanceof BpOrder) {
                        $bpOrder->setId(1);
                    }

                    return $bpOrder instanceof BpOrder
                        && $bpOrder->getCreditcard() === $bpCard
                        && $bpOrder->getUser() === $bpUser
                        && $bpOrder->getPaymentMethod() === 'CC_W'
                        && $bpOrder->getTotal() === 100
                        && $bpOrder->getCreateDate() instanceof \DateTime;
                })]
            );
        
        $this->entityManager->expects($this->once())
             ->method('flush');
            
        $this->producer->expects($this->once())
             ->method('send');
        
        $bpOrder = $this->order->processOrderWallet(1, 100);
        $this->assertEquals(100,$bpOrder->getTotal());
        
    }

    public function testFinishOrder() {
        $qrCode = uniqid(1, true);

        $bpOrder = new BpOrder();
        $bpOrder->setStatus(BpOrder::STATUS_PAID);
        $bpOrder->setQrcode($qrCode);
        $bpOrder->setId(1);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(BpOrder::class)
            ->willReturn($this->orderRepository);

        $this->orderRepository->expects($this->once())
            ->method('findOneBy')
            ->with(array('qrcode' => $qrCode))
            ->willReturn($bpOrder);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->order->finishOrder($qrCode);

        $this->assertEquals(BpOrder::STATUS_USED, $bpOrder->getStatus());
    }

    public function testOrderNotFound() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Order not found!");
        $this->expectExceptionCode(Exception::ORDER_NOT_FOUND);

        $exception = Exception::orderNotFound();
        $qrCode = uniqid(1, true);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(BpOrder::class)
            ->willReturn($this->orderRepository);

        $this->orderRepository->expects($this->once())
            ->method('findOneBy')
            ->with(array('qrcode' => $qrCode))
            ->willReturn(null);

        $this->order->finishOrder($qrCode);
    }

    public function testOrderWithInvalidStatus() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("The selected order has a invalid status: U");
        $this->expectExceptionCode(Exception::INVALID_ORDER_STATUS);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(BpOrder::class)
            ->willReturn($this->orderRepository);

        $qrCode = uniqid(1, true);
        $bpOrder = new BpOrder();
        $bpOrder->setStatus(BpOrder::STATUS_USED);
        $bpOrder->setQrcode($qrCode);
        $bpOrder->setId(1);

        $this->orderRepository->expects($this->once())
            ->method('findOneBy')
            ->with(array('qrcode' => $qrCode))
            ->willReturn($bpOrder);

        $this->order->finishOrder($qrCode);
    }
    
    public function testProcessOrderWithBalance(){
        $bpUser = new BpUser();
        $bpUser->setId(1);
        
        $bpEvent = new BpEvent();
        $bpEvent->setId(1);
        
        $bpProduct = new BpProduct();
        $bpProduct->setId(1);
        $bpProduct->setPrice(10);
        
        $products = array(array("PRODUCT_ID" => 1, "QUANTITY" => 3));
        
        $bpWallet = new BpWallet();
        $bpWallet->setId(1);
        $bpWallet->setBalance(100);
        $bpWallet->setUser($bpUser);

        $this->entityManager->expects($this->exactly(3))
             ->method('find')
             ->withConsecutive(
                 array(BpUser::class, 1),
                 array(BpEvent::class, 1),
                 array(BpProduct::class, 1)
             )
             ->willReturnOnConsecutiveCalls($bpUser, $bpEvent, $bpProduct);
        
        $this->entityManager->expects($this->exactly(2))
             ->method('persist')
             ->withConsecutive(
                 [$this->callback(function($bpOrder) use ($bpUser) {
                     if ($bpOrder instanceof BpOrder) {
                         $bpOrder->setId(1);
                     }
                     
                     return $bpOrder instanceof BpOrder
                         && $bpOrder->getUser() === $bpUser
                         && $bpOrder->getCreateDate() instanceof \DateTime;
                 })],
                 [$this->callback(function($bpProductOrder) use ($bpProduct) {
                     return $bpProductOrder instanceof BpProductOrder
                         && $bpProductOrder->getProduct() === $bpProduct
                         && $bpProductOrder->getQuantity() === 3;
                 })]
             );
       
        $this->entityManager->expects($this->once())
             ->method('flush');
        
        $bpOrder = $this->order->processOrder($products, 1);
    }
}
