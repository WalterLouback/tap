<?php
namespace Zeedhi\ApiPayment\Service;

use Zeedhi\ApiPayment\Model\Entities\BpCreditcard;
use Zeedhi\ApiPayment\Model\Entities\BpUser;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\CreditCard as PlatformCreditCard;

use Doctrine\ORM\EntityManager;

class CreditCardTest extends \PHPUnit_Framework_TestCase {

    /** @var CreditCard */
    private $creditCardService;
    /** @var PaymentPlatform|\PHPUnit_Framework_MockObject_MockObject */
    private $paymentPlatform;
    /** @var EntityManager|\PHPUnit_Framework_MockObject_MockObject */
    private $entityManager;
    /** @var SimpleEnvironmentImpl */
    private $environment;

    protected function setUp() {
        $this->environment = new SimpleEnvironmentImpl();
        $this->environment->setUserId(1);

        $this->entityManager = $this->getMockBuilder(EntityManager::class)
            ->setMethods(['persist', 'flush', 'find'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->paymentPlatform = $this->getMockForAbstractClass(InstantBuyPlatform::class);

        $this->creditCardService = new CreditCard($this->entityManager, $this->environment, $this->paymentPlatform);
    }

    public function testAddNewCreditCard() {
        $this->paymentPlatform->expects($this->once())
            ->method('createToken')
            ->with($this->callback(function($platformCreditCard) {
                return $platformCreditCard instanceof PlatformCreditCard
                && $platformCreditCard->getFlag() === PlatformCreditCard::FLAG_MASTERCARD
                && $platformCreditCard->getNumbers() === '5111222233334444'
                && $platformCreditCard->getCvv() === '666'
                && $platformCreditCard->getOwnerName() === 'FULANO SILVA NETO'
                && $platformCreditCard->getExpDateMonth() === '07'
                && $platformCreditCard->getExpDateYear() === '2023';
            }))
            ->willReturn('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');

        $bpUser = new BpUser();

        $this->entityManager->expects($this->once())
            ->method('find')
            ->with(BpUser::class, 1)
            ->willReturn($bpUser);

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($this->callback(function($bpCreditCard) use ($bpUser){
                return $bpCreditCard instanceof BpCreditcard
                && $bpCreditCard->getFlag() === BpCreditcard::FLAG_MASTERCARD
                && $bpCreditCard->getLastNumbers() === '4444'
                && $bpCreditCard->getExpirationDate()->format('m/Y') === '07/2023'
                && $bpCreditCard->getToken() === '8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d'
                && $bpCreditCard->getUser() === $bpUser;
            }));

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->creditCardService->addNewCreditCard('FULANO SILVA NETO', '5111222233334444', '07', '2023', '666');
    }

    public function testAddNewVisaCreditCard() {
        $this->paymentPlatform->expects($this->once())
            ->method('createToken')
            ->with($this->callback(function($platformCreditCard) {
                return $platformCreditCard instanceof PlatformCreditCard
                && $platformCreditCard->getFlag() === PlatformCreditCard::FLAG_VISA
                && $platformCreditCard->getNumbers() === '4111222233334444'
                && $platformCreditCard->getCvv() === '666'
                && $platformCreditCard->getOwnerName() === 'FULANO SILVA NETO'
                && $platformCreditCard->getExpDateMonth() === '07'
                && $platformCreditCard->getExpDateYear() === '2023';
            }))
            ->willReturn('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');

        $bpUser = new BpUser();

        $this->entityManager->expects($this->once())
            ->method('find')
            ->with(BpUser::class, 1)
            ->willReturn($bpUser);

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($this->callback(function($bpCreditCard) use ($bpUser){
                return $bpCreditCard instanceof BpCreditcard
                && $bpCreditCard->getFlag() === BpCreditcard::FLAG_VISA
                && $bpCreditCard->getLastNumbers() === '4444'
                && $bpCreditCard->getExpirationDate()->format('m/Y') === '07/2023'
                && $bpCreditCard->getToken() === '8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d'
                && $bpCreditCard->getUser() === $bpUser;
            }));

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->creditCardService->addNewCreditCard('FULANO SILVA NETO', '4111222233334444', '07', '2023', '666');
    }

    public function testAddNewCreditCardWithUnsupportedFlag() {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('The credit card belongs to a non supported brand.');
        $this->expectExceptionCode(Exception::UNSUPPORTED_CREDIT_CARD_FLAG);
        $this->creditCardService->addNewCreditCard('FULANO SILVA NETO', '1111222233334444', '07', '2023', '666');
    }
}
