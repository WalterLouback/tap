<?php
namespace Zeedhi\ApiPayment\Service;

class SimpleEnvironmentImpl implements Environment {
    
    private $userId;
    
    public function getUserId() {
        return $this->userId;
    }
    
    public function setUserId($userId) {
        $this->userId = $userId;
    }
}