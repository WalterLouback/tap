<?php
require __DIR__."/../bootstrap.php";

try {
    $merchantKey = 'd41a5b8f-ba4b-42b3-bbc6-bc1fb72a3e5c';
    $mg = \Zeedhi\ApiPayment\Service\PaymentPlatform\MundiPagg\PaymentPlatformImpl::factorySandBox($merchantKey);

    $cc = new \Zeedhi\ApiPayment\Service\PaymentPlatform\CreditCard(
        "PAULO F C NETO",
        "1111222233334444",
        "07",
        "23",
        "666",
        \Zeedhi\ApiPayment\Service\PaymentPlatform\CreditCard::FLAG_VISA
    );

    $token = $mg->registerCreditCard($cc);
    echo 'Created card token: ';
    var_dump($token);

    $transaction = new \Zeedhi\ApiPayment\Service\PaymentPlatform\Transaction(123.45, uniqid(), $token);
    echo "\nCreating transaction with token: ";
    $transactionKey = $mg->registerTransaction($transaction);
    var_dump($transactionKey);

    echo "\nCanceling it ...: ";
    var_dump($mg->cancelTransaction($transactionKey));
} catch (\Exception $e) {
    echo 'Deu ruim!';
    var_dump($e->getMessage());
}