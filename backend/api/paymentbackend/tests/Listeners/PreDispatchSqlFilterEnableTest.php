<?php
namespace Zeedhi\ApiPayment\Listeners;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\FilterCollection;

use Zeedhi\ApiPayment\Helpers\Environment;
use Zeedhi\ApiPayment\Listeners\PreDispatchSqlFilterEnable;
use Zeedhi\ApiPayment\Model\Filters\FilterByUser;

use Zeedhi\Framework\DTO\Request;

class PreDispatchSqlFilterTest extends \PHPUnit_Framework_TestCase {
    
    protected $entityManager;
    
    protected $environment;
    
    protected $preDispatchSqlFilter;
    
    public function setUp() {
        $this->entityManager = $this->getMockBuilder(EntityManager::class)
                                    ->setMethods(array('getFilters', 'getConnection'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->environment = $this->getMockBuilder(Environment::class)
                                  ->setMethods(array('getUserId'))
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->preDispatchSqlFilter = new PreDispatchSqlFilterEnable($this->entityManager, $this->environment);
    }

    public function testPreDispatchUserNull() {
        $request = $this->getMockBuilder(Request::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        $this->environment->expects($this->once())
                          ->method('getUserId')
                          ->willReturn(null);

        $this->preDispatchSqlFilter->preDispatch($request);
        
        //it's missing check that the filters still disabled.
    }
    
    public function testPreDispatchUserNotNull() {
        $request = $this->getMockBuilder(Request::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        $this->environment->expects($this->once())
                          ->method('getUserId')
                          ->willReturn(123);

        $filterCollection = $this->getMockBuilder(FilterCollection::class)
                                 ->setMethods(array('enable', 'setFiltersStateDirty'))
                                 ->disableOriginalConstructor()
                                 ->getMock();
        
        $this->entityManager->expects($this->exactly(2))
                            ->method('getFilters')
                            ->willReturn($filterCollection);
                            
        $filter = $this->getMockBuilder(\Doctrine\ORM\Query\Filter\SQLFilter::class)
                        ->setConstructorArgs([$this->entityManager])
                        ->setMethods(array('setParameter', 'addFilterConstraint'))
                        ->getMock();

        $filterCollection->expects($this->once())
                               ->method('enable')
                               ->with(FilterByUser::NAME)
                               ->willReturn($filter);

        $filterCollection->expects($this->exactly(1))
                            ->method('setFiltersStateDirty');
                            
        $mockConnection = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(["quote"])->getMock();
        
        $mockConnection->method("quote")->with(123, "integer")->willReturn("123");
        $this->entityManager->method("getConnection")->willReturn($mockConnection);
                            
        $this->assertFalse($filter->hasParameter(FilterByUser::USER_ID));

        $this->preDispatchSqlFilter->preDispatch($request);
        
        $this->assertTrue($filter->hasParameter(FilterByUser::USER_ID));
        
        $this->assertEquals("123", $filter->getParameter(FilterByUser::USER_ID));
    }
    
    
}