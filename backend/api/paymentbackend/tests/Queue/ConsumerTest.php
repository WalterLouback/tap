<?php
namespace Zeedhi\ApiPayment\Service\Queue;

use Doctrine\ORM\EntityManager;

use Zeedhi\Queue\Connection;

use Zeedhi\Notification\API as NotificationAPI;


use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\ApiPayment\Service\SimpleEnvironmentImpl;
use Zeedhi\ApiPayment\Service\Exception;
use Zeedhi\ApiPayment\Model\Entities\BpOrder;
use Zeedhi\ApiPayment\Model\Entities\BpUser;
use Zeedhi\ApiPayment\Model\Entities\BpCreditcard;
use Zeedhi\ApiPayment\Model\Entities\BpWallet;

class ConsumerTest extends \PHPUnit_Framework_TestCase
{
    /** @var Connection */
    protected $connection;
    /** @var EntityManager */
    protected $entityManager;
    /** @var PaymentPlatform */
    protected $paymentPlatform;
    /** @var Consumer */
    protected $consumer;
    /** @var Environment */
    protected $environment;
    /** @var NotificationAPI */
    protected $notificationApi;

    public function setUp(){
        $this->connection = $this->getMockBuilder(Connection::class)
                                 ->setMethods()
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManager::class)
                                    ->setMethods(array('find', 'getRepository', 'flush'))
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->paymentPlatform = $this->getMockForAbstractClass(InstantBuyPlatform::class);
        
        $this->environment = new SimpleEnvironmentImpl();
        $this->environment->setUserId(1);

        $this->consumer = new Consumer($this->connection, $this->entityManager, $this->paymentPlatform, $this->environment, "/queue/test", "php");
    }

    public function testConsumeCreditCardMethod(){
        $bpOrder = new BpOrder();
        
        $bpUser = new BpUser();
        $bpUser->setId(1);
        
        $bpCard = new BpCreditcard();
        $bpCard->setUser($bpUser);
        $bpCard->setId(1);
        $bpCard->setToken('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');
        
        $bpOrder->setId(1);
        $bpOrder->setCreditCard($bpCard);
        $bpOrder->setUser($bpUser);
        $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_CREDITCARD);
        
        $message = $bpOrder->getId();
        
        $this->entityManager->expects($this->once())
                            ->method('find')
                            ->with(BpOrder::class, $message)
                            ->willReturn($bpOrder);

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $this->consumer->exec($message);
    }
    
    public function testConsumeBalanceMethod(){
        $bpOrder = new BpOrder();
        
        $bpUser = new BpUser();
        $bpUser->setId(1);

        $bpOrder->setId(1);
        $bpOrder->setUser($bpUser);
        $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_BALANCE);
        $bpOrder->setTotal(100);
        
        $message = $bpOrder->getId();
        
        $this->entityManager->expects($this->once())
                            ->method('find')
                            ->with(BpOrder::class, $message)
                            ->willReturn($bpOrder);
        
        $wallet = new BpWallet();
        $wallet->setId(1);
        $wallet->setBalance(100);
        $wallet->setUser($bpUser);
        
        $walletRepository = $this->getMockBuilder(EntityRepository::class)
             ->setMethods(['findOneBy'])
             ->disableOriginalConstructor()
             ->getMock();

        $expectedFilter = array(
            "user" => $bpUser->getId()
        );

        $walletRepository->expects($this->once())
                         ->method('findOneBy')
                         ->with($expectedFilter)
                         ->willReturn($wallet);

        $this->entityManager->expects($this->once())
                     ->method('getRepository')
                     ->with(BpWallet::class)
                     ->willReturn($walletRepository);

        $this->entityManager->expects($this->once())
                            ->method('flush');

        $this->consumer->exec($message);
    }
    public function testConsumeForWalletMethod(){
        $bpOrder = new BpOrder();
        
        $bpUser = new BpUser();
        $bpUser->setId(1);
        
        $bpCard = new BpCreditcard();
        $bpCard->setUser($bpUser);
        $bpCard->setId(1);
        $bpCard->setToken('8a7be2bd-e0a7-445a-b9e4-524e1f8c8f9d');
        
        $bpOrder->setId(1);
        $bpOrder->setCreditCard($bpCard);
        $bpOrder->setUser($bpUser);
        $bpOrder->setPaymentMethod(BpOrder::PAYMENT_METHOD_CREDITCARD_FOR_WALLET);
        $bpOrder->setTotal(100);
        
        $message = $bpOrder->getId();
        
        $this->entityManager->expects($this->once())
                            ->method('find')
                            ->with(BpOrder::class, $message)
                            ->willReturn($bpOrder);
        
        $wallet = new BpWallet();
        $wallet->setId(1);
        $wallet->setBalance(100);
        $wallet->setUser($bpUser);
        
        $walletRepository = $this->getMockBuilder(EntityRepository::class)
             ->setMethods(['findOneBy'])
             ->disableOriginalConstructor()
             ->getMock();

        $expectedFilter = array(
            "user" => $bpUser->getId()
        );

        $walletRepository->expects($this->once())
                         ->method('findOneBy')
                         ->with($expectedFilter)
                         ->willReturn($wallet);

        $this->entityManager->expects($this->once())
                     ->method('getRepository')
                     ->with(BpWallet::class)
                     ->willReturn($walletRepository);

        $this->entityManager->expects($this->exactly(2))
                            ->method('flush');

        $this->consumer->exec($message);
        
        $this->assertEquals(BpOrder::STATUS_CREDITED, $bpOrder->getStatus());
        $this->assertEquals(200, $wallet->getBalance());
    }
}