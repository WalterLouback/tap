<?php
namespace Zeedhi\ApiPayment\Mocks;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerFactory {

    protected static function createFromConnectionParams($dbParams) {
        $paths = array(realpath(__DIR__ . "/../../gen/entities/"));
        $config = Setup::createXMLMetadataConfiguration($paths);
        $config->setProxyDir(__DIR__.'/../../src/Proxies');
        return EntityManager::create($dbParams, $config);
    }

    public static function getEntityManager() {
        $dbParams = array(
            'driver'    => 'oci8',
            'user'      => 'ApiPayment',
            'password'  => 'ApiPayment',
            'host'      => '192.168.122.55',
            'port'      => '1521',
            'dbname'    => 'xe'
        );

        $entityManager = self::createFromConnectionParams($dbParams);
        $eventManager = $entityManager->getEventManager();
        $eventManager->addEventSubscriber(new \Doctrine\DBAL\Event\Listeners\OracleSessionInit());
        return $entityManager;
    }

}