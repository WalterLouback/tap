<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\ApiPayment\Service\Order as OrderService;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO\Request\Row;
use Zeedhi\Framework\DTO\Response;

class OrderTest extends \PHPUnit_Framework_TestCase {

    /** @var Order */
    private $orderController;
    /** @var OrderService|\PHPUnit_Framework_MockObject_MockObject */
    private $orderService;

    protected function setUp() {
        /** @var Manager|\PHPUnit_Framework_MockObject_MockObject $manager */
        $manager = $this->getMockForAbstractClass(Manager::class);

        $this->orderService = $this->getMockBuilder(OrderService::class)
            ->disableOriginalConstructor()
            ->setMethods(['processOrder', 'finishOrder','processOrderWithBalance'])
            ->getMock();

        $this->orderController = new Order($manager, $this->orderService);
    }

    public function testProcessOrder() {
        $products = array(array("PRODUCT_ID" => 1, "QUANTITY" => 3));
        $response = new Response();
        $row = array(
            'PRODUCTS_ORDER' => $products,
            'CARD_ID'        => 1,
            'EVENT_ID'       => 1
        );

        $request = new Row($row, 'POST', '/processOrder', uniqid());

        $this->orderService->expects($this->once())
            ->method('processOrder')
            ->with($products, 1);

        $this->orderController->processOrder($request, $response);
    }

    public function testFinishOrder() {
        $response = new Response();
        $row = array('QRCODE' => 1);
        $request = new Row($row, 'POST', '/processOrder', uniqid());

        $this->orderService->expects($this->once())
            ->method('finishOrder')
            ->with(1);

        $this->orderController->finishOrder($request, $response);
    }
    
    public function testProcessOrderWithBalance() {
        $response = new response();
        $products = array(
            array("PRODUCT_ID" => 1, "QUANTITY" => 3)
        );
        
        $row = array(
            'PRODUCTS_ORDER' => $products,
            'EVENT_ID'       => 1
        );
        
        $request = new Row($row, 'POST', '/processOrderWithBalance', uniqid());
        
        $this->orderService->expects($this->once())
             ->method('processOrder')
             ->with($products);
             
        $this->orderController->processOrderWithBalance($request, $response);
    }
}
