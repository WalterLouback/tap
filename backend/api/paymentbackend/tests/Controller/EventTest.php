<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO\Request;
use Zeedhi\Framework\DTO\Response;

use Zeedhi\ApiPayment\Service\SimpleEnvironmentImpl;

class EventTest extends \PHPUnit_Framework_TestCase {

    protected $dataSourceManager;
    
    protected $eventController;
    protected $environment;

    protected function setUp() {
        /** @var Manager|\PHPUnit_Framework_MockObject_MockObject $manager */
        $manager = $this->getMockForAbstractClass(Manager::class);
        
        $this->environment = new SimpleEnvironmentImpl();
        $this->environment->setUserId(1);
        
        $this->eventController = new Event($manager, $this->environment, "http://urlPrefix/");
    }
    
    public function testPutImagePathFromTo(){
        $response = new Response();
        
        $events = array( 
            array(
                'ID' => null,
                'NAME' => "Jane Doe's Party",
                'DATETIME' => '03/02/2017 10:18:00',
                'ABOUT' => 'The best party ever!',
                'ADDRESS' => 'Baker Street, 221B',
                'COVERIMG_PHOTO_UPLOAD' => array(array('path' => 'path/to/image.jpg')),
                'LOGOIMG_PHOTO_UPLOAD' => array(array('path' => 'path/to/image.jpg')),
                'MAPIMG_PHOTO_UPLOAD' => array(array('path' => 'path/to/image.jpg')),
                '__is_new' => true
            )
        );
        
        $dataSet = new \Zeedhi\Framework\DataSource\DataSet('event', $events);
        
        $request = new Request\DataSet($dataSet, 'POST', '/event/save', uniqid());
        
        $this->eventController->save($request, $response);
    }
}
