<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\ApiPayment\Service\Wallet as WalletService;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DTO\Request\Row;
use Zeedhi\Framework\DTO\Response;

class WalletTest extends \PHPUnit_Framework_TestCase {

    /** @var Wallet */
    private $walletController;
    /** @var WalletService|\PHPUnit_Framework_MockObject_MockObject */
    private $walletService;
    
    protected function setUp(){
        
        /** @var Manager|\PHPUnit_Framework_MockObject_MockObject $manager */
        $manager = $this->getMockForAbstractClass(Manager::class);
        
        $this->walletService = $this->getMockBuilder(WalletService::class)
            ->disableOriginalConstructor()
            ->setMethods(['processOrder'])
            ->getMock();

        $this->walletController = new Wallet($manager, $this->walletService);
    }
    
    public function testProcessCreditPurchase(){
        $response = new Response();
        $row = array(
            'CARD_ID' => 1,
            'BALANCE' => 100
        );
        
        $request = new Row($row, 'POST', '/processCreditPurchase', uniqid());
        
        $this->walletService->expects($this->once())
            ->method('processOrder')
            ->with(1, 100);

        $this->walletController->processCreditPurchase($request, $response);
    }
}