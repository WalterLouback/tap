<?php
namespace Zeedhi\ApiPayment\Controller;

use Zeedhi\ApiPayment\Controller\Auth as AuthController;
use Zeedhi\ApiPayment\Service\Auth as AuthService;
use Zeedhi\ApiPayment\Model\Entities\BpUser;
use Zeedhi\ApiPayment\Helpers\RijndaelCrypt as RijndaelHelper;

use Zeedhi\Framework\Routing\Router;
use Zeedhi\Framework\DTO\Request;
use Zeedhi\Framework\DTO\Request\Row;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\DataSource\ParameterBag;

class AuthTest extends \PHPUnit_Framework_TestCase {

    /** @var \Zeedhi\ApiPayment\Controller\Auth */
    protected $authController;
    /** @var \Zeedhi\ApiPayment\Service\Auth|\PHPUnit_Framework_MockObject_MockObject */
    protected $authService;
    /** @var ArrayImpl */
    protected $cache;
    /** @var ParameterBag */
    protected $parameterBag;
    /** @var $rijndaelHelper String*/
    protected $rijndaelHelper;

    public function setUp() {
        $this->authService = $this->getMockBuilder(AuthService::class)
                                    ->setMethods(array('login', 'register', 'changePassword', 'update','logout','logged'))
                                    ->disableOriginalConstructor()
                                    ->getMock();
                                    
       
        $this->rijndaelHelper = new RijndaelHelper();
        $this->cache = new ArrayImpl();
        $this->parameterBag = new ParameterBag($this->cache);
        $this->authController = new AuthController($this->authService, $this->parameterBag, "http://urlPrefix/", $this->rijndaelHelper);
    }

    /**
     * testLogin
     * test login with success
     */
    public function testLogin() {
        $row = array(
            'EMAIL' => 'joao.martins@teknisa.com',
            'PASSWORD' => '123456'
        );

        $expectedUser = new BpUser();
        $expectedUser->setId(18);
        $expectedUser->setFirstName("Joao Paulo");
        $expectedUser->setLastName("Joao Paulo");
        $expectedUser->setPassword("123456");
        $expectedUser->setEmail("joao.martins@teknisa.com");
        $expectedUser->setPhone("25853696");
        $expectedUser->setImage("path/to/picture.jpg");


        $this->authService->expects($this->once())
                          ->method('login')
                          ->with($row['EMAIL'], $row['PASSWORD'])
                          ->willReturn($expectedUser);

        $method = Router::METHOD_POST;
        $routePath = '/login';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->login($request, $response);

        $dataSets = $response->getDataSets();
        $this->assertCount(1, $dataSets);

        $dataSet = $dataSets[0];

        $this->assertEquals('user', $dataSet->getDataSourceName(), 'Incorrect dataSource name');

        $expectedUsers = array(
            array(
                'ID' => 18,
                'CRYPT_ID' => 'bec=',
                'FIRST_NAME' => 'Joao Paulo',
                'LAST_NAME' => 'Joao Paulo',
                'EMAIL' => 'joao.martins@teknisa.com',
                'PHONE' => '25853696',
                'MAIN_CARD' => NULL,
                'IMAGE' => 'path/to/picture.jpg',
                '__is_new' => false
            )
        );

        $user = $dataSet->getRows();
        $this->assertEquals($expectedUsers, $user);

        $this->assertEquals(18, $this->parameterBag->get(Auth::USER_ID));
    }
    
    public function testLogged(){
        $row = array(
            'USER_ID' => "bec="
        );

        $expectedUser = new BpUser();
        $expectedUser->setId(18);
        $expectedUser->setFirstName("Diogenes");
        $expectedUser->setLastName("Morais");
        $expectedUser->setPassword("123456");
        $expectedUser->setEmail("diorni@teknisa.com");
        $expectedUser->setPhone("25853696");
        $expectedUser->setImage("path/to/picture.jpg");
        
        $decrypt = $this->rijndaelHelper->decrypt($row['USER_ID']);
        
        $this->authService->expects($this->once())
                          ->method('logged')
                          ->with($decrypt)
                          ->willReturn($expectedUser);
                          
        $request = new Row($row, 'POST', '/logged', uniqid());
        $response = new Response();
        
        $this->authController->logged($request, $response);
        
        $dataSets = $response->getDataSets();
        $this->assertCount(1, $dataSets);

        $dataSet = $dataSets[0];
        
        $this->assertEquals('user', $dataSet->getDataSourceName(), 'Incorrect dataSource name');
        
        $expectedUsers = array(
            array(
                'ID' => 18,
                'CRYPT_ID' => 'bec=',
                'FIRST_NAME' => 'Diogenes',
                'LAST_NAME' => 'Morais',
                'EMAIL' => 'diorni@teknisa.com',
                'PHONE' => '25853696',
                'MAIN_CARD' => NULL,
                'IMAGE' => 'path/to/picture.jpg',
                '__is_new' => false
            )
        );

        $user = $dataSet->getRows();
        $this->assertEquals($expectedUsers, $user);

        $this->assertEquals($decrypt, $this->parameterBag->get(Auth::USER_ID));
    }
    
    /**
     * testLoginWithEmptyParams
     * test Login with empty params, expect Exception
     */
    public function testLoggedWithEmptyUser() {
        $this->setExpectedException(
            Exception::class,
            'There is no one logged!',
            Exception::NO_ONE_LOGGED
        );

        $row = array(
            'USER_ID' => ''
        );

        $method = Router::METHOD_POST;
        $routePath = '/logged';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->logged($request, $response);
    }

    /**
     * testLoginWithEmptyParams
     * test Login with empty params, expect Exception
     */
    public function testLoginWithEmptyParams() {
        $this->setExpectedException(
            Exception::class,
            'Invalid login parameters!',
            Exception::INVALID_LOGIN_PARAMETERS
        );

        $row = array(
            'EMAIL' => '',
            'PASSWORD' => ''
        );

        $method = Router::METHOD_POST;
        $routePath = '/login';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->login($request, $response);
    }

    /**
     * testLoginWithEmptyEmail
     * test Login with empty Email, expect Exception
     */
    public function testLoginWithEmptyEmail() {
        $this->setExpectedException(
            Exception::class,
            'Invalid login parameters!',
            Exception::INVALID_LOGIN_PARAMETERS
        );

        $row = array(
            'EMAIL' => '',
            'PASSWORD' => '123456'
        );

        $method = Router::METHOD_POST;
        $routePath = '/login';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->login($request, $response);
    }

    /**
     * testLoginWithEmptyPassword
     * test Login with empty Password, expect Exception
     */
    public function testLoginWithEmptyPassword() {
        $this->setExpectedException(
            Exception::class,
            'Invalid login parameters!',
            Exception::INVALID_LOGIN_PARAMETERS
        );

        $row = array(
            'EMAIL' => 'joao.martins@teknisa.com.br',
            'PASSWORD' => ''
        );

        $method = Router::METHOD_POST;
        $routePath = '/login';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->login($request, $response);
    }

    public function testRegister() {
        $row = array(
            'EMAIL' => 'jane.doe@teknisa.com',
            'CRYPT_ID' => '',
            'FIRST_NAME' => 'Jane',
            'LAST_NAME' => 'Doe',
            'PHONE' => 25853696,
            'PASSWORD' => '123456'
        );
        $method = Router::METHOD_POST;
        $routePath = '/register';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $expectedUser = new BpUser();
        $expectedUser->setFirstName("Jane");
        $expectedUser->setLastName("Doe");
        $expectedUser->setPassword("123456");
        $expectedUser->setPhone(25853696);
        $expectedUser->setEmail("jane.doe@teknisa.com");

        $this->authService->expects($this->once())
                          ->method('register')
                          ->with($row)
                          ->willReturn($expectedUser);

        $this->authController->register($request, $response);

        $dataSets = $response->getDataSets();
        $this->assertCount(1, $dataSets);

        $dataSet = $dataSets[0];

        $this->assertEquals('user', $dataSet->getDataSourceName(), 'Incorrect dataSource name');

        $expectedUsers = array(
            array(
                'ID' => NULL,
                'FIRST_NAME' => 'Jane',
                'LAST_NAME' => 'Doe',
                'EMAIL' => 'jane.doe@teknisa.com',
                'PHONE' => 25853696,
                'MAIN_CARD' => NULL,
                'IMAGE' => NULL,
                'CRYPT_ID' => '',
                '__is_new' => false
            )
        );

        $user = $dataSet->getRows();
        $this->assertEquals($expectedUsers, $user);
    }

    public function testChangePass(){
        $newPass = '456789';
        $oldPass = '123456';
        $row = array(
            'NEW_PASSWORD' => $newPass,
            'PASSWORD' => $oldPass
        );

        $method = Router::METHOD_POST;
        $routePath = '/register';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authService->expects($this->once())
            ->method("changePassword")
            ->with($oldPass, $newPass);

        $this->authController->changePassword($request, $response);
    }

    public function testChangePassMissingParameters(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("It is missing parameters!");
        $this->expectExceptionCode(Exception::MISSING_PARAMETERS);

        $newPass = '456789';
        $oldPass = '123456';
        $row = array(
            'NEW_PASSWORD' => $newPass,
            'WRONG_KEY_PASSWORD' => $oldPass
        );

        $method = Router::METHOD_POST;
        $routePath = '/register';
        $userId = '123456';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();

        $this->authController->changePassword($request, $response);
    }

    public function testLogout() {
        $this->expectException(\Zeedhi\Framework\Cache\Exception::class);
        $this->expectExceptionMessage('Value for key zeedhi_data_source_param_USER_ID not found in cache.');

        $this->authService->expects($this->once())
            ->method('logout');

        $this->parameterBag->set('LOGGED_USER', 1);

        $this->authController->logout(new Request(Router::METHOD_POST, '/logout', '123456'), new Response());

        $this->assertNull($this->parameterBag->get(Auth::USER_ID));
    }
    
    public function testUpdate(){
        $row = array(
            'CRYPT_ID' => '',
            'EMAIL' => 'diorni@teknisa.com',
            'FIRST_NAME' => 'Diogenes',
            'LAST_NAME' => 'Morais',
            'PHONE' => 7217873,
            'IMAGE' => array(array('path' => 'path/to/image.jpg')),
            'PASSWORD' => 'teknisa'
        );
        
        $method = Router::METHOD_POST;
        $routePath = '/register/update';
        $userId = '654321';
        $request = new Request\Row($row, $method, $routePath, $userId);
        $response = new Response();
        
        $expectedUser = new BpUser();
        $expectedUser->setFirstName("Diogenes");
        $expectedUser->setLastName("Morais");
        $expectedUser->setPassword("teknisa");
        $expectedUser->setPhone(7217873);
        $expectedUser->setImage("path/to/image.jpg");
        $expectedUser->setEmail("diorni@teknisa.com");
        
        $this->authService->expects($this->once())
                          ->method('update')
                          ->with($row)
                          ->willReturn($expectedUser);
        
        $this->authController->update($request, $response);
        
        $dataSets = $response->getDataSets();
        $this->assertCount(1, $dataSets);

        $dataSet = $dataSets[0];

        $this->assertEquals('user', $dataSet->getDataSourceName(), 'Incorrect dataSource name');
        
        $expectedUsers = array(
            array(
                'ID' => NULL,
                'CRYPT_ID' => '',
                'FIRST_NAME' => 'Diogenes',
                'LAST_NAME' => 'Morais',
                'EMAIL' => 'diorni@teknisa.com',
                'PHONE' => 7217873,
                'MAIN_CARD' => NULL,
                'IMAGE' => 'path/to/image.jpg',
                '__is_new' => false
            )
        );

        $user = $dataSet->getRows();
        $this->assertEquals($expectedUsers, $user);
        
    }

}