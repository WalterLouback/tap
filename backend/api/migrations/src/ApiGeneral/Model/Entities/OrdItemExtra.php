<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdItemExtra
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $ordOrderProduct;

    protected $ordExtra;
    
    protected $quantity;
    
    protected $total;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdOrderProduct(): ?OrdOrderProduct
    {
        return $this->ordOrderProduct;
    }

    public function setOrdOrderProduct(?OrdOrderProduct $ordOrderProduct): self
    {
        $this->ordOrderProduct = $ordOrderProduct;

        return $this;
    }

    public function getOrdExtra(): ?OrdExtra
    {
        return $this->ordExtra;
    }

    public function setOrdExtra(?OrdExtra $ordExtra): self
    {
        $this->ordExtra = $ordExtra;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(?string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(?string $total): self
    {
        $this->total = $total;

        return $this;
    }
}
