<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtEvent
{
    protected $about;

    protected $initialDate;

    protected $imageCover;

    protected $imageLogo;

    protected $imageMap;

    protected $name;

    protected $nrorg;

    protected $status;

    protected $type;

    protected $finalDate;

    protected $location;

    protected $classification;

    protected $rating;

    protected $salesMethod;

    protected $externalId;

    protected $value;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $parentEvent;

    protected $genCategory;

    protected $structure;

    protected $ownerUser; 
    
    protected $codeEvent;

    protected $numberSeqEvent;
    
    protected $latitude;
    
    protected $longitude;

    protected $urlApiExternal;

    protected $tokenApiExternal;

    protected $originApiExternal;

    protected $integrationCommands;

    protected $ipStore;

    protected $ordRecipient;

    protected $loggiApiKey;

    protected $loggiShopId;

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getInitialDate(): ?\DateTimeInterface
    {
        return $this->initialDate;
    }

    public function setInitialDate(?\DateTimeInterface $initialDate): self
    {
        $this->initialDate = $initialDate;

        return $this;
    }

    public function getImageCover(): ?string
    {
        return $this->imageCover;
    }

    public function setImageCover(?string $imageCover): self
    {
        $this->imageCover = $imageCover;

        return $this;
    }

    public function getImageLogo(): ?string
    {
        return $this->imageLogo;
    }

    public function setImageLogo(?string $imageLogo): self
    {
        $this->imageLogo = $imageLogo;

        return $this;
    }

    public function getImageMap(): ?string
    {
        return $this->imageMap;
    }

    public function setImageMap(?string $imageMap): self
    {
        $this->imageMap = $imageMap;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFinalDate(): ?\DateTimeInterface
    {
        return $this->finalDate;
    }

    public function setFinalDate(?\DateTimeInterface $finalDate): self
    {
        $this->finalDate = $finalDate;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getClassification(): ?string
    {
        return $this->classification;
    }

    public function setClassification(?string $classification): self
    {
        $this->classification = $classification;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(?string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getSalesMethod(): ?string
    {
        return $this->salesMethod;
    }

    public function setSalesMethod(?string $salesMethod): self
    {
        $this->salesMethod = $salesMethod;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentEvent(): ?self
    {
        return $this->parentEvent;
    }

    public function setParentEvent(?self $parentEvent): self
    {
        $this->parentEvent = $parentEvent;

        return $this;
    }

    public function getGenCategory(): ?GenCategory
    {
        return $this->genCategory;
    }

    public function setGenCategory(?GenCategory $genCategory): self
    {
        $this->genCategory = $genCategory;

        return $this;
    }

    public function getStructure(): ?GenStructure
    {
        return $this->structure;
    }

    public function setStructure(?GenStructure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getOwnerUser(): ?GenUser
    {
        return $this->ownerUser;
    }

    public function setOwnerUser(?GenUser $ownerUser): self
    {
        $this->ownerUser = $ownerUser;

        return $this;
    }

    public function getCodeEvent(): ?string
    {
        return $this->codeEvent;
    }

    public function setCodeEvent(?string $codeEvent): self
    {
        $this->codeEvent = $codeEvent;

        return $this;
    }

    public function getNumberSeqEvent(): ?int
    {
        return $this->numberSeqEvent;
    }

    public function setNumberSeqEvent(?int $numberSeqEvent): self
    {
        $this->numberSeqEvent = $numberSeqEvent;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getUrlApiExternal(): ?string
    {
        return $this->urlApiExternal;
    }

    public function setUrlApiExternal(?string $urlApiExternal): self
    {
        $this->urlApiExternal = $urlApiExternal;

        return $this;
    }

    public function getTokenApiExternal(): ?string
    {
        return $this->tokenApiExternal;
    }

    public function setTokenApiExternal(?string $tokenApiExternal): self
    {
        $this->tokenApiExternal = $tokenApiExternal;

        return $this;
    }

    public function getOriginApiExternal(): ?string
    {
        return $this->originApiExternal;
    }

    public function setOriginApiExternal(?string $originApiExternal): self
    {
        $this->originApiExternal = $originApiExternal;

        return $this;
    }

    public function getIntegrationCommands(): ?string
    {
        return $this->integrationCommands;
    }

    public function setIntegrationCommands(string $integrationCommands): self
    {
        $this->integrationCommands = $integrationCommands;

        return $this;
    }

    public function getIpStore(): ?string
    {
        return $this->ipStore;
    }

    public function setIpStore(string $ipStore): self
    {
        $this->ipStore = $ipStore;

        return $this;
    }

    public function getOrdRecipient(): ?OrdRecipient
    {
        return $this->ordRecipient;
    }

    public function setOrdRecipient(?OrdRecipient $ordRecipient): self
    {
        $this->ordRecipient = $ordRecipient;

        return $this;
    }
   
    public function getLoggiApiKey(): ?string
    {
        return $this->loggiApiKey;
    }

    public function setLoggiApiKey(?string $loggiApiKey): self
    {
        $this->loggiApiKey = $loggiApiKey;
        return $this;
    }

    public function getLoggiShopId(): ?string
    {
        return $this->loggiShopId;
    }

    public function setLoggiShopId(?string $loggiShopId): self
    {
        $this->loggiShopId = $loggiShopId;
    }    
}
