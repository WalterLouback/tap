<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdSplitRuleRecipient
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $ordSplitRule;

    protected $ordRecipient;

    protected $percentage;

    protected $fixedValue;
    // Y or N
    protected $remainderValue;
    // Y or N
    protected $processingValue;
    // MAIN_RECEPIENT, RECEPIENT or OWNER
    protected $type;
    
    protected $status;

    public function getPercentage()
    {
        return $this->percentage;
    }

    public function setPercentage($percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getFixedValue()
    {
        return $this->fixedValue;
    }

    public function setFixedValue($fixedValue): self
    {
        $this->fixedValue = $fixedValue;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdSplitRule(): ?OrdSplitRule
    {
        return $this->ordSplitRule;
    }

    public function setOrdSplitRule(?OrdSplitRule $ordSplitRule): self
    {
        $this->ordSplitRule = $ordSplitRule;

        return $this;
    }

    public function getOrdRecipient(): ?OrdRecipient
    {
        return $this->ordRecipient;
    }

    public function setOrdRecipient(?OrdRecipient $ordRecipient): self
    {
        $this->ordRecipient = $ordRecipient;

        return $this;
    }

    public function getRemainderValue(): ?string
    {
        return $this->remainderValue;
    }

    public function setRemainderValue(?string $remainderValue): self
    {
        $this->remainderValue = $remainderValue;

        return $this;
    }

    public function getProcessingValue(): ?string
    {
        return $this->processingValue;
    }

    public function setProcessingValue(?string $processingValue): self
    {
        $this->processingValue = $processingValue;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
