<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenConfiguration
{
    protected $nrorg;

    protected $logoImage;

    protected $logoImageSmall;

    protected $primaryColor;

    protected $secondaryColor;

    protected $yesColor;

    protected $noColor;

    protected $mesNoRegister;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $host;

    protected $hostFrom;

    protected $hostPassword;

    protected $hostFromName;

    protected $useIntegration;

    protected $fcmServerKey;
    
    protected $awsS3AccessKey;
    
    protected $awsS3SecretKey;
    
    protected $awsS3Bucket;

    protected $id;
    
    protected $preferenceFlow;
    
    protected $googleAnalyticsKey;
    
    protected $procobAuthKey;
    
    protected $allowExternalUsers;
    
    protected $appLayoutConfig;
    
    protected $keyGoogle;

    protected $convenienceFee;
    
    protected $hostPort;
    
    protected $hostSecure;

    protected $urlApiExternal;

    protected $tokenApiExternal;

    protected $originApiExternal;
    
    protected $tokenApiOdhen;
    
    protected $dbName;

    protected $loggiApiKey;

    protected $loggiShopId;
    
    protected $loggiUser;
    
    protected $loggiPassword;
    
    protected $backgroundOverlay;
    
    protected $nrorgIntegracao;
    
    protected $playDeliveryApiKey;
    
    protected $playDeliveryApiEmail;
    
    protected $authToken;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getLogoImage(): ?string
    {
        return $this->logoImage;
    }

    public function setLogoImage(?string $logoImage): self
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    public function getLogoImageSmall(): ?string
    {
        return $this->logoImageSmall;
    }

    public function setLogoImageSmall(?string $logoImageSmall): self
    {
        $this->logoImageSmall = $logoImageSmall;

        return $this;
    }

    public function getPrimaryColor(): ?string
    {
        return $this->primaryColor;
    }

    public function setPrimaryColor(?string $primaryColor): self
    {
        $this->primaryColor = $primaryColor;

        return $this;
    }

    public function getSecondaryColor(): ?string
    {
        return $this->secondaryColor;
    }

    public function setSecondaryColor(?string $secondaryColor): self
    {
        $this->secondaryColor = $secondaryColor;

        return $this;
    }

    public function getYesColor(): ?string
    {
        return $this->yesColor;
    }

    public function setYesColor(?string $yesColor): self
    {
        $this->yesColor = $yesColor;

        return $this;
    }

    public function getNoColor(): ?string
    {
        return $this->noColor;
    }

    public function setNoColor(?string $noColor): self
    {
        $this->noColor = $noColor;

        return $this;
    }

    public function getMesNoRegister(): ?string
    {
        return $this->mesNoRegister;
    }

    public function setMesNoRegister(?string $mesNoRegister): self
    {
        $this->mesNoRegister = $mesNoRegister;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getHostFrom(): ?string
    {
        return $this->hostFrom;
    }

    public function setHostFrom(?string $hostFrom): self
    {
        $this->hostFrom = $hostFrom;

        return $this;
    }

    public function getHostPassword(): ?string
    {
        return $this->hostPassword;
    }

    public function setHostPassword(?string $hostPassword): self
    {
        $this->hostPassword = $hostPassword;

        return $this;
    }

    public function getHostFromName(): ?string
    {
        return $this->hostFromName;
    }

    public function setHostFromName(?string $hostFromName): self
    {
        $this->hostFromName = $hostFromName;

        return $this;
    }

    public function getUseIntegration(): ?bool
    {
        return $this->useIntegration;
    }

    public function setUseIntegration(?bool $useIntegration): self
    {
        $this->useIntegration = $useIntegration;

        return $this;
    }

    public function getFcmServerKey(): ?string
    {
        return $this->fcmServerKey;
    }

    public function setFcmServerKey(?string $fcmServerKey): self
    {
        $this->fcmServerKey = $fcmServerKey;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAwsS3AccessKey(): ?string
    {
        return $this->awsS3AccessKey;
    }

    public function setAwsS3AccessKey(?string $awsS3AccessKey): self
    {
        $this->awsS3AccessKey = $awsS3AccessKey;

        return $this;
    }

    public function getAwsS3SecretKey(): ?string
    {
        return $this->awsS3SecretKey;
    }

    public function setAwsS3SecretKey(?string $awsS3SecretKey): self
    {
        $this->awsS3SecretKey = $awsS3SecretKey;

        return $this;
    }

    public function getAwsS3Bucket(): ?string
    {
        return $this->awsS3Bucket;
    }

    public function setAwsS3Bucket(?string $awsS3Bucket): self
    {
        $this->awsS3Bucket = $awsS3Bucket;

        return $this;
    }

    public function getPreferenceFlow(): ?bool
    {
        return $this->preferenceFlow;
    }

    public function setPreferenceFlow(?bool $preferenceFlow): self
    {
        $this->preferenceFlow = $preferenceFlow;

        return $this;
    }

    public function getGoogleAnalyticsKey(): ?string
    {
        return $this->googleAnalyticsKey;
    }

    public function setGoogleAnalyticsKey(?string $googleAnalyticsKey): self
    {
        $this->googleAnalyticsKey = $googleAnalyticsKey;

        return $this;
    }

    public function getProcobAuthKey(): ?string
    {
        return $this->procobAuthKey;
    }

    public function setProcobAuthKey(?string $procobAuthKey): self
    {
        $this->procobAuthKey = $procobAuthKey;

        return $this;
    }

    public function getAllowExternalUsers(): ?bool
    {
        return $this->allowExternalUsers;
    }

    public function setAllowExternalUsers(?bool $allowExternalUsers): self
    {
        $this->allowExternalUsers = $allowExternalUsers;

        return $this;
    }

    public function getAppLayoutConfig(): ?string
    {
        return $this->appLayoutConfig;
    }

    public function setAppLayoutConfig(?string $appLayoutConfig): self
    {
        $this->appLayoutConfig = $appLayoutConfig;

        return $this;
    }

    public function getKeyGoogle(): ?string
    {
        return $this->keyGoogle;
    }

    public function setKeyGoogle(?string $keyGoogle): self
    {
        $this->keyGoogle = $keyGoogle;

        return $this;
    }

    public function getConvenienceFee()
    {
        return $this->convenienceFee;
    }

    public function setConvenienceFee($convenienceFee): self
    {
        $this->convenienceFee = $convenienceFee;

        return $this;
    }

    public function getHostSecure(): ?string
    {
        return $this->hostSecure;
    }

    public function setHostSecure(?string $hostSecure): self
    {
        $this->hostSecure = $hostSecure;

        return $this;
    }

    public function getHostPort(): ?string
    {
        return $this->hostPort;
    }

    public function setHostPort(?string $hostPort): self
    {
        $this->hostPort = $hostPort;

        return $this;
    }

    public function getUrlApiExternal(): ?string
    {
        return $this->urlApiExternal;
    }

    public function setUrlApiExternal(?string $urlApiExternal): self
    {
        $this->urlApiExternal = $urlApiExternal;

        return $this;
    }

    public function getTokenApiExternal(): ?string
    {
        return $this->tokenApiExternal;
    }

    public function setTokenApiExternal(?string $tokenApiExternal): self
    {
        $this->tokenApiExternal = $tokenApiExternal;

        return $this;
    }

    public function getOriginApiExternal(): ?string
    {
        return $this->originApiExternal;
    }

    public function setOriginApiExternal(?string $originApiExternal): self
    {
        $this->originApiExternal = $originApiExternal;

        return $this;
    }

    public function getTokenApiOdhen(): ?string
    {
        return $this->tokenApiOdhen;
    }

    public function setTokenApiOdhen(?string $tokenApiOdhen): self
    {
        $this->tokenApiOdhen = $tokenApiOdhen;

        return $this;
    }

    public function getDbName(): ?string
    {
        return $this->dbName;
    }

    public function setDbName(?string $dbName): self
    {
        $this->dbName = $dbName;

        return $this;
    }
    public function getLoggiApiKey(): ?string
    {
        return $this->loggiApiKey;
    }

    public function setLoggiApiKey(?string $loggiApiKey): self
    {
        $this->loggiApiKey = $loggiApiKey;
        return $this;
    }

    public function getLoggiShopId(): ?string
    {
        return $this->loggiShopId;
    }

    public function setLoggiShopId(?string $loggiShopId): self
    {
        $this->loggiShopId = $loggiShopId;
    }

    public function getLoggiPassword(): ?string
    {
        return $this->loggiPassword;
    }

    public function setLoggiPassword(?string $loggiPassword): self
    {
        $this->loggiPassword = $loggiPassword;

        return $this;
    }

    public function getLoggiUser(): ?string
    {
        return $this->loggiUser;
    }

    public function setLoggiUser(?string $loggiUser): self
    {
        $this->loggiUser = $loggiUser;

        return $this;
    }

    public function getBackgroundOverlay(): ?string
    {
        return $this->backgroundOverlay;
    }

    public function setBackgroundOverlay(?string $backgroundOverlay): self
    {
        $this->backgroundOverlay = $backgroundOverlay;

        return $this;
    }

    public function getNrorgIntegracao(): ?string
    {
        return $this->nrorgIntegracao;
    }

    public function setNrorgIntegracao(?string $nrorgIntegracao): self
    {
        $this->nrorgIntegracao = $nrorgIntegracao;

        return $this;
    }

    public function getPlayDeliveryApiKey(): ?string
    {
        return $this->playDeliveryApiKey;
    }

    public function setPlayDeliveryApiKey(?string $playDeliveryApiKey): self
    {
        $this->playDeliveryApiKey = $playDeliveryApiKey;

        return $this;
    }

    public function getPlayDeliveryApiEmail(): ?string
    {
        return $this->playDeliveryApiEmail;
    }

    public function setPlayDeliveryApiEmail(?string $playDeliveryApiEmail): self
    {
        $this->playDeliveryApiEmail = $playDeliveryApiEmail;

        return $this;
    }

    public function getAuthToken(): ?string
    {
        return $this->authToken;
    }

    public function setAuthToken(?string $authToken): self
    {
        $this->authToken = $authToken;

        return $this;
    }

}
