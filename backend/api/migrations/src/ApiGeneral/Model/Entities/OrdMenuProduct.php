<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdMenuProduct
{
    protected $initialTime;

    protected $finalTime;

    protected $price;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $status;

    protected $estimatedTime;

    protected $nrorg;

    protected $name;

    protected $detail;

    protected $image;

    protected $amount;

    protected $id;

    protected $ordProduct;

    protected $ordMenu;

    protected $ordProductGroup;

    protected $ordination;
    
    protected $featured;
    
    protected $originalPrice;

    public function getInitialTime(): ?\DateTimeInterface
    {
        return $this->initialTime;
    }

    public function setInitialTime(?\DateTimeInterface $initialTime): self
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime(): ?\DateTimeInterface
    {
        return $this->finalTime;
    }

    public function setFinalTime(?\DateTimeInterface $finalTime): self
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEstimatedTime(): ?int
    {
        return $this->estimatedTime;
    }

    public function setEstimatedTime(?int $estimatedTime): self
    {
        $this->estimatedTime = $estimatedTime;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(?string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdProduct(): ?OrdProduct
    {
        return $this->ordProduct;
    }

    public function setOrdProduct(?OrdProduct $ordProduct): self
    {
        $this->ordProduct = $ordProduct;

        return $this;
    }

    public function getOrdMenu(): ?OrdMenu
    {
        return $this->ordMenu;
    }

    public function setOrdMenu(?OrdMenu $ordMenu): self
    {
        $this->ordMenu = $ordMenu;

        return $this;
    }

    public function getOrdProductGroup(): ?OrdProductGroup
    {
        return $this->ordProductGroup;
    }

    public function setOrdProductGroup(?OrdProductGroup $ordProductGroup): self
    {
        $this->ordProductGroup = $ordProductGroup;

        return $this;
    }

    public function getOrdination(): ?int
    {
        return $this->ordination;
    }

    public function setOrdination(?int $ordination): self
    {
        $this->ordination = $ordination;
        return $this;
    }

    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(?bool $featured): self
    {
        $this->featured = $featured;
        return $this;
    }

    public function getOriginalPrice(): ?float
    {
        return $this->originalPrice;
    }

    public function setOriginalPrice(?float $originalPrice): self
    {
        $this->originalPrice = $originalPrice;

        return $this;
    }
}
