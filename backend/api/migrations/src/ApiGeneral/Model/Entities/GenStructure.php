<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenStructure
{
    protected $name;

    protected $nrorg;

    protected $description;

    protected $type;

    protected $structureLevel;

    protected $email;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $status;

    protected $externalQrcode;

    protected $id;

    protected $evtEvent;

    protected $genAddress;

    protected $parent;

    protected $unidadeId;
    
    protected $integrationCommands;
    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStructureLevel(): ?int
    {
        return $this->structureLevel;
    }

    public function setStructureLevel(?int $structureLevel): self
    {
        $this->structureLevel = $structureLevel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getExternalQrcode(): ?string
    {
        return $this->externalQrcode;
    }

    public function setExternalQrcode(?string $externalQrcode): self
    {
        $this->externalQrcode = $externalQrcode;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getGenAddress(): ?GenAddress
    {
        return $this->genAddress;
    }

    public function setGenAddress(?GenAddress $genAddress): self
    {
        $this->genAddress = $genAddress;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getUnidadeId(): ?string
    {
        return $this->unidadeId;
    }

    public function setUnidadeId(?string $unidadeId): self
    {
        $this->unidadeId = $unidadeId;

        return $this;
    }

    public function getIntegrationCommands(): ?bool
    {
        return $this->integrationCommands;
    }

    public function setIntegrationCommands(?bool $integrationCommands): self
    {
        $this->integrationCommands = $integrationCommands;

        return $this;
    }
}
