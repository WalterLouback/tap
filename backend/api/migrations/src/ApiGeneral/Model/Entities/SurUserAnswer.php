<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SurUserAnswer
{
    protected $comment;

    protected $note;

    protected $answer;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $surQuestion;

    protected $surAnswerOption;

    protected $genUser;

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurQuestion(): ?SurQuestion
    {
        return $this->surQuestion;
    }

    public function setSurQuestion(?SurQuestion $surQuestion): self
    {
        $this->surQuestion = $surQuestion;

        return $this;
    }

    public function getSurAnswerOption(): ?SurAnswerOption
    {
        return $this->surAnswerOption;
    }

    public function setSurAnswerOption(?SurAnswerOption $surAnswerOption): self
    {
        $this->surAnswerOption = $surAnswerOption;

        return $this;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }
}
