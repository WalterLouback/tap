<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenTaaConfiguration
{

    protected $id = 0;

    protected $nrorg = 0;

    protected $name;

    protected $modality;

    protected $structureId;

    protected $storeId;

    protected $posSerialNumber;

    protected $posReferenceId;

    protected $externalApiUrl;
    
    protected $externalApiId;
    
    protected $modifiedAt;
    
    protected $modifiedBy;
    
    protected $createdAt;
    
    protected $createdBy;
    
    protected $status;
    
    protected $email;

    public function getNrorg(): ?string
    {
        return $this->nrorg;
    }

    public function setNrorg(string $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getModality(): ?string
    {
        return $this->modality;
    }

    public function setModality(string $modality): self
    {
        $this->modality = $modality;

        return $this;
    }

    public function getPosSerialNumber(): ?string
    {
        return $this->posSerialNumber;
    }

    public function setPosSerialNumber(string $posSerialNumber): self
    {
        $this->posSerialNumber = $posSerialNumber;

        return $this;
    }

    public function getPosReferenceId(): ?string
    {
        return $this->posReferenceId;
    }

    public function setPosReferenceId(string $posReferenceId): self
    {
        $this->posReferenceId = $posReferenceId;

        return $this;
    }

    public function getExternalApiUrl(): ?string
    {
        return $this->externalApiUrl;
    }

    public function setExternalApiUrl(string $externalApiUrl): self
    {
        $this->externalApiUrl = $externalApiUrl;

        return $this;
    }

    public function getExternalApiId(): ?string
    {
        return $this->externalApiId;
    }

    public function setExternalApiId(string $externalApiId): self
    {
        $this->externalApiId = $externalApiId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?string $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStructureId(): ?GenStructure
    {
        return $this->structureId;
    }

    public function setStructureId(?GenStructure $structureId): self
    {
        $this->structureId = $structureId;

        return $this;
    }

    public function getStoreId(): ?EvtEvent
    {
        return $this->storeId;
    }

    public function setStoreId(?EvtEvent $storeId): self
    {
        $this->storeId = $storeId;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
    
}
