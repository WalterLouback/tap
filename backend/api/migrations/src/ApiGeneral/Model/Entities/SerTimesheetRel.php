<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SerTimesheetRel
{
    protected $id;
    
    protected $serServiceSeller;
    
    protected $serTimesheet;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;
    
    protected $evtEvent;
    
    protected $serService;
    
    protected $ordConfigStoreDeliversId;

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?string
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?string $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerTimesheet(): ?SerTimesheet
    {
        return $this->serTimesheet;
    }

    public function setSerTimesheet(?SerTimesheet $serTimesheet): self
    {
        $this->serTimesheet = $serTimesheet;

        return $this;
    }

    public function getSerServiceSeller(): ?SerServiceSellerRel
    {
        return $this->serServiceSeller;
    }

    public function setSerServiceSeller(?SerServiceSellerRel $serServiceSeller): self
    {
        $this->serServiceSeller = $serServiceSeller;

        return $this;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }
    
    public function getSerService(): ?SerService
    {
        return $this->serService;
    }

    public function setSerService(?SerService $serService): self
    {
        $this->serService = $serService;

        return $this;
    }

    public function getOrdConfigStoreDeliversId(): ?string
    {
        return $this->ordConfigStoreDeliversId;
    }

    public function setOrdConfigStoreDeliversId(?string $ordConfigStoreDeliversId): self
    {
        $this->ordConfigStoreDeliversId = $ordConfigStoreDeliversId;

        return $this;
    }
}