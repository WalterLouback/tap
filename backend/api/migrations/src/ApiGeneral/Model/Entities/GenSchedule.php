<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenSchedule
{
    protected $id;
    
    protected $serService;
    
    protected $genStructure;
    
    protected $serServiceSeller;
    
    protected $initialDate;
    
    protected $finalDate;
    
    protected $type;
    
    protected $appointment;
    
    protected $nrorg;
    
    protected $createdBy;

    protected $modifiedBy;

    protected $createdAt;

    protected $modifiedAt;
    
    protected $status;
    
    protected $serTimeTimesheet;
    
    protected $genSchedule;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInitialDate(): ?\DateTimeInterface
    {
        return $this->initialDate;
    }

    public function setInitialDate(?\DateTimeInterface $initialDate): self
    {
        $this->initialDate = $initialDate;

        return $this;
    }

    public function getFinalDate(): ?\DateTimeInterface
    {
        return $this->finalDate;
    }

    public function setFinalDate(?\DateTimeInterface $finalDate): self
    {
        $this->finalDate = $finalDate;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAppointment(): ?string
    {
        return $this->appointment;
    }

    public function setAppointment(?string $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getSerService(): ?SerService
    {
        return $this->serService;
    }

    public function setSerService(?SerService $serService): self
    {
        $this->serService = $serService;

        return $this;
    }

    public function getSerServiceSeller(): ?SerServiceSellerRel
    {
        return $this->serServiceSeller;
    }

    public function setSerServiceSeller(?SerServiceSellerRel $serServiceSeller): self
    {
        $this->serServiceSeller = $serServiceSeller;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSerTimeTimesheet(): ?SerTimeTimesheet
    {
        return $this->serTimeTimesheet;
    }

    public function setSerTimeTimesheet(?SerTimeTimesheet $serTimeTimesheet): self
    {
        $this->serTimeTimesheet = $serTimeTimesheet;

        return $this;
    }

    public function getGenStructure(): ?GenStructure
    {
        return $this->genStructure;
    }

    public function setGenStructure(?GenStructure $genStructure): self
    {
        $this->genStructure = $genStructure;

        return $this;
    }

    public function getGenSchedule(): ?self
    {
        return $this->genSchedule;
    }

    public function setGenSchedule(?self $genSchedule): self
    {
        $this->genSchedule = $genSchedule;

        return $this;
    }
}
