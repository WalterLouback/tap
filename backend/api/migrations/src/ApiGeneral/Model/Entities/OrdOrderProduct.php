<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdOrderProduct
{
    protected $quantity;

    protected $price;

    protected $total;

    protected $ordOrderId;

    protected $remaining;

    protected $note;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $evtEventTicket;

    protected $ordMenuProduct;
    
    protected $serService;
    
    protected $status;
    
    protected $itemIdentifier;

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getOrdOrderId(): ?string
    {
        return $this->ordOrderId;
    }

    public function setOrdOrderId(?string $ordOrderId): self
    {
        $this->ordOrderId = $ordOrderId;

        return $this;
    }

    public function getRemaining(): ?int
    {
        return $this->remaining;
    }

    public function setRemaining(?int $remaining): self
    {
        $this->remaining = $remaining;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEventTicket(): ?EvtEventTicket
    {
        return $this->evtEventTicket;
    }

    public function setEvtEventTicket(?EvtEventTicket $evtEventTicket): self
    {
        $this->evtEventTicket = $evtEventTicket;

        return $this;
    }

    public function getOrdMenuProduct(): ?OrdMenuProduct
    {
        return $this->ordMenuProduct;
    }

    public function setOrdMenuProduct(?OrdMenuProduct $ordMenuProduct): self
    {
        $this->ordMenuProduct = $ordMenuProduct;

        return $this;
    }

    public function getSerService(): ?SerService
    {
        return $this->serService;
    }

    public function setSerService(?SerService $serService): self
    {
        $this->serService = $serService;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getItemIdentifier(): ?int
    {
        return $this->itemIdentifier;
    }

    public function setItemIdentifier(?int $itemIdentifier): self
    {
        $this->itemIdentifier = $itemIdentifier;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
