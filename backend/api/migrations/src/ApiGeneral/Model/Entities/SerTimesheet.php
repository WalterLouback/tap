<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SerTimesheet
{
    protected $id;
    
    protected $name;

    protected $time;

    protected $initialTime;

    protected $finalTime;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;
    
    protected $seasonalDate;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getInitialTime(): ?string
    {
        return $this->initialTime;
    }

    public function setInitialTime(?string $initialTime): self
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime(): ?string
    {
        return $this->finalTime;
    }

    public function setFinalTime(?string $finalTime): self
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?string
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?string $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeasonalDate(): ?string
    {
        return $this->seasonalDate;
    }

    public function setSeasonalDate(?string $seasonalDate): self
    {
        $this->seasonalDate = $seasonalDate;

        return $this;
    }
}