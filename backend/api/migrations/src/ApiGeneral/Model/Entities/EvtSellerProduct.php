<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtSellerProduct
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $status;

    protected $id;

    protected $ordMenuProduct;

    protected $evtEventSeller;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdMenuProduct(): ?OrdMenuProduct
    {
        return $this->ordMenuProduct;
    }

    public function setOrdMenuProduct(?OrdMenuProduct $ordMenuProduct): self
    {
        $this->ordMenuProduct = $ordMenuProduct;

        return $this;
    }

    public function getEvtEventSeller(): ?EvtEventSeller
    {
        return $this->evtEventSeller;
    }

    public function setEvtEventSeller(?EvtEventSeller $evtEventSeller): self
    {
        $this->evtEventSeller = $evtEventSeller;

        return $this;
    }
}
