<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenNotification
{
    protected $message;

    protected $dateTime;

    protected $originNotification;

    protected $status;

    protected $nrorg;

    protected $id;

    protected $genUser;
    
    protected $confirmation;
    
    protected $externalId;
    
    protected $title;
    
    protected $dateOfCommitment;
    
    protected $category;
    
    protected $urlImage;
    
    protected $createdAt;
    
    protected $createdBy;
    
    protected $modifiedAt;
    
    protected $modifiedBy;

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(?\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getOriginNotification(): ?string
    {
        return $this->originNotification;
    }

    public function setOriginNotification(?string $originNotification): self
    {
        $this->originNotification = $originNotification;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }

    public function getConfirmation()
    {
        return $this->confirmation;
    }

    public function setConfirmation($confirmation): self
    {
        $this->confirmation = $confirmation;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
    
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(): self
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(): self
    {
        $this->modifiedAt = new \DateTime();

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getUrlImage(): ?string
    {
        return $this->urlImage;
    }

    public function setUrlImage(?string $urlImage): self
    {
        $this->urlImage = $urlImage;

        return $this;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function setCategory(?int $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDateOfCommitment(): ?\DateTimeInterface
    {
        return $this->dateOfCommitment;
    }

    public function setDateOfCommitment(?\DateTimeInterface $dateOfCommitment): self
    {
        $this->dateOfCommitment = $dateOfCommitment;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }
}
