<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenUserRelationship
{
    protected $id;
    protected $relationship;
    protected $nrorg;
    protected $firstBusinessPartner;
    protected $secondBusinessPartner;
    protected $createdAt;
    protected $createdBy;
    protected $modifiedAt;
    protected $modifiedBy;

    public function getRelationship(): ?string
    {
        return $this->relationship;
    }

    public function setRelationship(?string $relationship): self
    {
        $this->relationship = $relationship;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstBusinessPartner(): ?GenBusinessPartner
    {
        return $this->firstBusinessPartner;
    }

    public function setFirstBusinessPartner(?GenBusinessPartner $firstBusinessPartner): self
    {
        $this->firstBusinessPartner = $firstBusinessPartner;

        return $this;
    }

    public function getSecondBusinessPartner(): ?GenBusinessPartner
    {
        return $this->secondBusinessPartner;
    }

    public function setSecondBusinessPartner(?GenBusinessPartner $secondBusinessPartner): self
    {
        $this->secondBusinessPartner = $secondBusinessPartner;

        return $this;
    }
}
