<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtUserTicketRel
{
    protected $nrorg;

    protected $status;

    protected $paidForUser;

    protected $ownerName;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $guid;

    protected $id;

    protected $evtEventTicket;

    protected $genUser;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPaidForUser(): ?int
    {
        return $this->paidForUser;
    }

    public function setPaidForUser(?int $paidForUser): self
    {
        $this->paidForUser = $paidForUser;

        return $this;
    }

    public function getOwnerName(): ?string
    {
        return $this->ownerName;
    }

    public function setOwnerName(?string $ownerName): self
    {
        $this->ownerName = $ownerName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(?string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEventTicket(): ?EvtEventTicket
    {
        return $this->evtEventTicket;
    }

    public function setEvtEventTicket(?EvtEventTicket $evtEventTicket): self
    {
        $this->evtEventTicket = $evtEventTicket;

        return $this;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }
}
