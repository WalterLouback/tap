<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtEventTicket
{
    protected $name;

    protected $price;

    protected $amount;

    protected $startSale;

    protected $finalSale;

    protected $status;

    protected $nrorg;
    
    protected $allowExternalUsers;
    
    protected $minPurchaseAmount;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $evtEvent;
    
    protected $codeTypeTicket;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getStartSale(): ?\DateTimeInterface
    {
        return $this->startSale;
    }

    public function setStartSale(?\DateTimeInterface $startSale): self
    {
        $this->startSale = $startSale;

        return $this;
    }

    public function getFinalSale(): ?\DateTimeInterface
    {
        return $this->finalSale;
    }

    public function setFinalSale(?\DateTimeInterface $finalSale): self
    {
        $this->finalSale = $finalSale;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getCodeTypeTicket(): ?string
    {
        return $this->codeTypeTicket;
    }

    public function setCodeTypeTicket(?string $codeTypeTicket): self
    {
        $this->codeTypeTicket = $codeTypeTicket;

        return $this;
    }

    public function getAllowExternalUsers(): ?int
    {
        return $this->allowExternalUsers;
    }

    public function setAllowExternalUsers(?int $allowExternalUsers): self
    {
        $this->allowExternalUsers = $allowExternalUsers;

        return $this;
    }

    public function getMinPurchaseAmount(): ?int
    {
        return $this->minPurchaseAmount;
    }

    public function setMinPurchaseAmount(?int $minPurchaseAmount): self
    {
        $this->minPurchaseAmount = $minPurchaseAmount;

        return $this;
    }
}
