<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class PayGateway
{
    protected $gateway;

    protected $merchantKey;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $zeroAuth;

    protected $consultaBin;

    protected $merchantId;

    protected $id;
    
    protected $ordSplitRule;

    protected $ordRecipient;

    public function getGateway(): ?string
    {
        return $this->gateway;
    }

    public function setGateway(?string $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getMerchantKey(): ?string
    {
        return $this->merchantKey;
    }

    public function setMerchantKey(?string $merchantKey): self
    {
        $this->merchantKey = $merchantKey;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getZeroAuth(): ?string
    {
        return $this->zeroAuth;
    }

    public function setZeroAuth(?string $zeroAuth): self
    {
        $this->zeroAuth = $zeroAuth;

        return $this;
    }

    public function getConsultaBin(): ?string
    {
        return $this->consultaBin;
    }

    public function setConsultaBin(?string $consultaBin): self
    {
        $this->consultaBin = $consultaBin;

        return $this;
    }

    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }

    public function setMerchantId(?string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdSplitRule(): ?OrdSplitRule
    {
        return $this->ordSplitRule;
    }

    public function setOrdSplitRule(?OrdSplitRule $ordSplitRule): self
    {
        $this->ordSplitRule = $ordSplitRule;

        return $this;
    }

    public function getOrdRecipient(): ?OrdRecipient
    {
        return $this->ordRecipient;
    }

    public function setOrdRecipient(?OrdRecipient $ordRecipient): self
    {
        $this->ordRecipient = $ordRecipient;

        return $this;
    }
}
