<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdSelectedOption
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $ordItemExtra;

    protected $ordOption;
    
    protected $price;
    
    protected $selectedQuantity;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdItemExtra(): ?OrdItemExtra
    {
        return $this->ordItemExtra;
    }

    public function setOrdItemExtra(?OrdItemExtra $ordItemExtra): self
    {
        $this->ordItemExtra = $ordItemExtra;

        return $this;
    }

    public function getOrdOption(): ?OrdOption
    {
        return $this->ordOption;
    }

    public function setOrdOption(?OrdOption $ordOption): self
    {
        $this->ordOption = $ordOption;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSelectedQuantity(): ?string
    {
        return $this->selectedQuantity;
    }

    public function setSelectedQuantity(?string $selectedQuantity): self
    {
        $this->selectedQuantity = $selectedQuantity;

        return $this;
    }
}
