<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdWorkflow
{
    protected $nrorg;

    protected $initialStatus;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $paymentMoment;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getInitialStatus(): ?int
    {
        return $this->initialStatus;
    }

    public function setInitialStatus(?int $initialStatus): self
    {
        $this->initialStatus = $initialStatus;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaymentMoment(): ?GenStatus
    {
        return $this->paymentMoment;
    }

    public function setPaymentMoment(?GenStatus $paymentMoment): self
    {
        $this->paymentMoment = $paymentMoment;

        return $this;
    }
}
