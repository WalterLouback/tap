<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdProductGroup
{
    protected $name;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $evtEvent;

    protected $ordMenu;

    protected $ordination;
    
    protected $parentId;
    
    protected $image;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getOrdMenu(): ?OrdMenu
    {
        return $this->ordMenu;
    }

    public function setOrdMenu(?OrdMenu $ordMenu): self
    {
        $this->ordMenu = $ordMenu;

        return $this;
    }

    public function getOrdination(): ?int
    {
        return $this->ordination;
    }

    public function setOrdination(?int $ordination): self
    {
        $this->ordination = $ordination;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(?int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
