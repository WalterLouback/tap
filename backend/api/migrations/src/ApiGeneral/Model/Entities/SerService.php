<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SerService
{
    protected $name;

    protected $status;

    protected $decription;
    
    protected $cancellationFine;
    
    protected $cancellationTime;
    
    protected $time;
    
    protected $evtEvent;
    
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;
    
    protected $parentId;
    
    protected $price;
    
    protected $color;
    
    protected $icon;
    
    protected $image;
    
    protected $type;
    
    protected $statusapp;
    
    protected $futureDaysToSchedule;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDecription(): ?string
    {
        return $this->decription;
    }

    public function setDecription(?string $decription): self
    {
        $this->decription = $decription;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCancellationFine()
    {
        return $this->cancellationFine;
    }

    public function setCancellationFine($cancellationFine): self
    {
        $this->cancellationFine = $cancellationFine;

        return $this;
    }

    public function getCancellationTime(): ?string
    {
        return $this->cancellationTime;
    }

    public function setCancellationTime(?string $cancellationTime): self
    {
        $this->cancellationTime = $cancellationTime;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(?string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getParentId(): ?self
    {
        return $this->parentId;
    }

    public function setParentId(?self $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatusapp(): ?string
    {
        return $this->statusapp;
    }

    public function setStatusapp(?string $statusapp): self
    {
        $this->statusapp = $statusapp;

        return $this;
    }

    public function getFutureDaysToSchedule(): ?int
    {
        return $this->futureDaysToSchedule;
    }

    public function setFutureDaysToSchedule(?int $futureDaysToSchedule): self
    {
        $this->futureDaysToSchedule = $futureDaysToSchedule;

        return $this;
    }

}
