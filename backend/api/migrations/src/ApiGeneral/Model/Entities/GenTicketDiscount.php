<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenTicketDiscount
{
    protected $id;
    
    protected $cupomCode;

    protected $nrorg;

    protected $type;

    protected $valMin;

    protected $valCupom;

    protected $initialDate;

    protected $finalDate;
    
    protected $status;
    
    protected $isActive;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCupomCode(): ?string
    {
        return $this->cupomCode;
    }

    public function setCupomCode(string $cupomCode): self
    {
        $this->cupomCode = $cupomCode;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValCupom(): ?float
    {
        return $this->valCupom;
    }

    public function setValCupom(float $valCupom): self
    {
        $this->valCupom = $valCupom;

        return $this;
    }

    public function getValMin(): ?float
    {
        return $this->valMin;
    }

    public function setValMin(float $valMin): self
    {
        $this->valMin = $valMin;

        return $this;
    }

    public function getInitialDate(): ?\DateTimeInterface
    {
        return $this->initialDate;
    }

    public function setInitialDate(\DateTimeInterface $initialDate): self
    {
        $this->initialDate = $initialDate;

        return $this;
    }

    public function getFinalDate(): ?\DateTimeInterface
    {
        return $this->finalDate;
    }

    public function setFinalDate(\DateTimeInterface $finalDate): self
    {
        $this->finalDate = $finalDate;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    public function setIsActive(int $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
    
}
