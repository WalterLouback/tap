<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenShift
{
    protected $day;

    protected $initialTime;

    protected $finalTime;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $evtEvent;

    protected $evtEventMenu;

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(?string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getInitialTime(): ?\DateTimeInterface
    {
        return $this->initialTime;
    }

    public function setInitialTime(?\DateTimeInterface $initialTime): self
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime(): ?\DateTimeInterface
    {
        return $this->finalTime;
    }

    public function setFinalTime(?\DateTimeInterface $finalTime): self
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getEvtEventMenu(): ?EvtEventMenu
    {
        return $this->evtEventMenu;
    }

    public function setEvtEventMenu(?EvtEventMenu $evtEventMenu): self
    {
        $this->evtEventMenu = $evtEventMenu;

        return $this;
    }
}
