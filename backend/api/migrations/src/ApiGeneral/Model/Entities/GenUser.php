<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenUser
{
    protected $firstName;

    protected $lastName;

    protected $status;

    protected $email;

    protected $cpf;

    protected $image;

    protected $password;

    protected $facebookId;

    protected $facebookToken;

    protected $facebookTokenExpDate;

    protected $mainCreditcardId;

    protected $token;

    protected $firebaseToken;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;
    
    protected $lastToken;
    
    protected $birthDate;
    
    protected $preferences;
    
    protected $firebaseId;

    protected $resetpassword;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getFacebookToken(): ?string
    {
        return $this->facebookToken;
    }

    public function setFacebookToken(?string $facebookToken): self
    {
        $this->facebookToken = $facebookToken;

        return $this;
    }

    public function getFacebookTokenExpDate(): ?\DateTimeInterface
    {
        return $this->facebookTokenExpDate;
    }

    public function setFacebookTokenExpDate(?\DateTimeInterface $facebookTokenExpDate): self
    {
        $this->facebookTokenExpDate = $facebookTokenExpDate;

        return $this;
    }

    public function getMainCreditcardId(): ?int
    {
        return $this->mainCreditcardId;
    }

    public function setMainCreditcardId(?int $mainCreditcardId): self
    {
        $this->mainCreditcardId = $mainCreditcardId;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getFirebaseToken(): ?string
    {
        return $this->firebaseToken;
    }

    public function setFirebaseToken(?string $firebaseToken): self
    {
        $this->firebaseToken = $firebaseToken;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastToken(): ?string
    {
        return $this->lastToken;
    }

    public function setLastToken(?string $lastToken): self
    {
        $this->lastToken = $lastToken;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPreferences(): ?int
    {
        return $this->preferences;
    }

    public function setPreferences(?int $preferences): self
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function getFirebaseId(): ?string
    {
        return $this->firebaseId;
    }

    public function setFirebaseId(?string $firebaseId): self
    {
        $this->firebaseId = $firebaseId;
        
        return $this;
    }
        
    public function getResetpassword(): ?string
    {
        return $this->resetpassword;
    }

    public function setResetpassword(?string $resetpassword): self
    {
        $this->resetpassword = $resetpassword;

        return $this;
    }
}
