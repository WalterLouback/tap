<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenActivityRequest
{
    protected $type;

    protected $description;

    protected $file;

    protected $status;

    protected $genSupportRequestId;

    protected $genUserId;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $genActivityRequestOld;

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGenSupportRequestId(): ?int
    {
        return $this->genSupportRequestId;
    }

    public function setGenSupportRequestId(?int $genSupportRequestId): self
    {
        $this->genSupportRequestId = $genSupportRequestId;

        return $this;
    }

    public function getGenUserId(): ?int
    {
        return $this->genUserId;
    }

    public function setGenUserId(?int $genUserId): self
    {
        $this->genUserId = $genUserId;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenActivityRequestOld(): ?self
    {
        return $this->genActivityRequestOld;
    }

    public function setGenActivityRequestOld(?self $genActivityRequestOld): self
    {
        $this->genActivityRequestOld = $genActivityRequestOld;

        return $this;
    }
}
