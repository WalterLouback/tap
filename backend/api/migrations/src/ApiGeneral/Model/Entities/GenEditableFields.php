<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenEditableFields
{
    protected $fieldName;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $genPermission;

    protected $genUserType;

    public function getFieldName(): ?string
    {
        return $this->fieldName;
    }

    public function setFieldName(?string $fieldName): self
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    public function getNrorg(): ?float
    {
        return $this->nrorg;
    }

    public function setNrorg(?float $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenPermission(): ?GenPermission
    {
        return $this->genPermission;
    }

    public function setGenPermission(?GenPermission $genPermission): self
    {
        $this->genPermission = $genPermission;

        return $this;
    }

    public function getGenUserType(): ?GenUserType
    {
        return $this->genUserType;
    }

    public function setGenUserType(?GenUserType $genUserType): self
    {
        $this->genUserType = $genUserType;

        return $this;
    }
}
