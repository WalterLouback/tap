<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenContact
{
    protected $type;

    protected $phone;

    protected $email;

    protected $foneWhatsapp;

    protected $foneFixo;
    
    protected $isVisibleToUser;
    
    protected $isWhatsapp;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $evtEvent;

    protected $genStructure;

    protected $genUser;

    protected $genBusinessPartner;

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getGenStructure(): ?GenStructure
    {
        return $this->genStructure;
    }

    public function setGenStructure(?GenStructure $genStructure): self
    {
        $this->genStructure = $genStructure;

        return $this;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getGenBusinessPartner(): ?GenBusinessPartner
    {
        return $this->genBusinessPartner;
    }

    public function setGenBusinessPartner(?GenBusinessPartner $genBusinessPartner): self
    {
        $this->genBusinessPartner = $genBusinessPartner;

        return $this;
    }

    public function getIsVisibleToUser(): ?string
    {
        return $this->isVisibleToUser;
    }

    public function setIsVisibleToUser(?string $isVisibleToUser): self
    {
        $this->isVisibleToUser = $isVisibleToUser;

        return $this;
    }

    public function getIsWhatsapp(): ?string
    {
        return $this->isWhatsapp;
    }

    public function setIsWhatsapp(?string $isWhatsapp): self
    {
        $this->isWhatsapp = $isWhatsapp;

        return $this;
    }

    public function getFoneWhatsapp(): ?string
    {
        return $this->foneWhatsapp;
    }

    public function setFoneWhatsapp(?string $foneWhatsapp): self
    {
        $this->foneWhatsapp = $foneWhatsapp;

        return $this;
    }

    public function getFoneFixo(): ?string
    {
        return $this->foneFixo;
    }

    public function setFoneFixo(?string $foneFixo): self
    {
        $this->foneFixo = $foneFixo;

        return $this;
    }
}
