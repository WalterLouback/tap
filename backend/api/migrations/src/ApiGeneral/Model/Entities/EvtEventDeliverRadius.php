<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtEventDeliverRadius
{
    private $nrorg;

    private $latitude;

    private $longitude;

    private $createdBy;

    private $modifiedBy;

    private $createdAt;

    private $modifiedAt;

    private $id;

    private $evtEventDeliver;

    public function getNrorg(): ?string
    {
        return $this->nrorg;
    }

    public function setNrorg(?string $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?string $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?string
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?string $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEvtEventDeliver(): ?EvtEventDeliver
    {
        return $this->evtEventDeliver;
    }

    public function setEvtEventDeliver(?EvtEventDeliver $evtEventDeliver): self
    {
        $this->evtEventDeliver = $evtEventDeliver;

        return $this;
    }
}
