<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SurAnswerOptionRel
{
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $surAnswer;

    protected $surQuestion;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurAnswer(): ?SurAnswerOption
    {
        return $this->surAnswer;
    }

    public function setSurAnswer(?SurAnswerOption $surAnswer): self
    {
        $this->surAnswer = $surAnswer;

        return $this;
    }

    public function getSurQuestion(): ?SurQuestion
    {
        return $this->surQuestion;
    }

    public function setSurQuestion(?SurQuestion $surQuestion): self
    {
        $this->surQuestion = $surQuestion;

        return $this;
    }
}
