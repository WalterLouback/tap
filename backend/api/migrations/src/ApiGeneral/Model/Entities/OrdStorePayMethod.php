<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdStorePayMethod
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $ordConfigStore;

    protected $paymentMethod;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdConfigStore(): ?OrdConfigStore
    {
        return $this->ordConfigStore;
    }

    public function setOrdConfigStore(?OrdConfigStore $ordConfigStore): self
    {
        $this->ordConfigStore = $ordConfigStore;

        return $this;
    }

    public function getPaymentMethod(): ?PayPaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PayPaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }
}
