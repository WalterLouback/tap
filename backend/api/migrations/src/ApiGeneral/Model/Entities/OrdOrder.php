<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdOrder
{
    protected $createDate;

    protected $transactionId;

    protected $paymentMethod;

    protected $total;
    
    protected $troco;

    protected $nrorg;

    protected $paymentStatus;

    protected $deliverTo;

    protected $orderType;

    protected $note;

    protected $walletId;

    protected $orderIdentifier;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $payCreditcard;

    protected $evtEvent;

    protected $ordCashier;

    protected $status;

    protected $genStructure;

    protected $genUser;

    protected $evtEventSeller;

    protected $nsu;
    
    protected $creditCardBrand;
    
    protected $creditCardNumbers;
    
    protected $orderSheet;
    
    protected $cardHolderName;
    
    protected $deliveryAddress;
    
    protected $deliveryFee;
    
    protected $convenienceFee;

    protected $OAReponse;

    protected $OAsuccess;

    protected $orderSerTimesheetId;

    protected $orderSerTimeTimesheetId;

    protected $orderConfigStoreDeliversId;
    
    protected $orderScheduledDate;
    
    protected $timezone;
    
    protected $loggiResponse;
    
    protected $loggiPk;
    
    protected $responseLog;
    
    protected $requestLog;
    
    protected $fromTaa;
    
    protected $barCode;
    
    protected $cupomId;
    
    protected $discount;
    
    protected $playDeliveryResponse;
    
    protected $body_save_order_external_api;
    
    protected $posStatus;
    
    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(?\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setTransactionId(?string $transactionId): self
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getDeliverTo(): ?string
    {
        return $this->deliverTo;
    }

    public function setDeliverTo(?string $deliverTo): self
    {
        $this->deliverTo = $deliverTo;

        return $this;
    }

    public function getOrderType(): ?string
    {
        return $this->orderType;
    }

    public function setOrderType(?string $orderType): self
    {
        $this->orderType = $orderType;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getWalletId(): ?string
    {
        return $this->walletId;
    }

    public function setWalletId(?string $walletId): self
    {
        $this->walletId = $walletId;

        return $this;
    }

    public function getOrderIdentifier(): ?int
    {
        return $this->orderIdentifier;
    }

    public function setOrderIdentifier(?int $orderIdentifier): self
    {
        $this->orderIdentifier = $orderIdentifier;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPayCreditcard(): ?PayCreditcard
    {
        return $this->payCreditcard;
    }

    public function setPayCreditcard(?PayCreditcard $payCreditcard): self
    {
        $this->payCreditcard = $payCreditcard;

        return $this;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getOrdCashier(): ?OrdCashier
    {
        return $this->ordCashier;
    }

    public function setOrdCashier(?OrdCashier $ordCashier): self
    {
        $this->ordCashier = $ordCashier;

        return $this;
    }

    public function getStatus(): ?GenStatus
    {
        return $this->status;
    }

    public function setStatus(?GenStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGenStructure(): ?GenStructure
    {
        return $this->genStructure;
    }

    public function setGenStructure(?GenStructure $genStructure): self
    {
        $this->genStructure = $genStructure;

        return $this;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }

    public function getEvtEventSeller(): ?EvtEventSeller
    {
        return $this->evtEventSeller;
    }

    public function setEvtEventSeller(?EvtEventSeller $evtEventSeller): self
    {
        $this->evtEventSeller = $evtEventSeller;

        return $this;
    }

    public function getNsu(): ?string
    {
        return $this->nsu;
    }

    public function setNsu(?string $nsu): self
    {
        $this->nsu = $nsu;

        return $this;
    }

    public function getCreditCardBrand(): ?string
    {
        return $this->creditCardBrand;
    }

    public function setCreditCardBrand(?string $creditCardBrand): self
    {
        $this->creditCardBrand = $creditCardBrand;

        return $this;
    }

    public function getCreditCardNumbers(): ?string
    {
        return $this->creditCardNumbers;
    }

    public function setCreditCardNumbers(?string $creditCardNumbers): self
    {
        $this->creditCardNumbers = $creditCardNumbers;

        return $this;
    }

    public function getOrderSheet(): ?string
    {
        return $this->orderSheet;
    }

    public function setOrderSheet(?string $orderSheet): self
    {
        $this->orderSheet = $orderSheet;

        return $this;
    }

    public function getCardHolderName(): ?string
    {
        return $this->cardHolderName;
    }

    public function setCardHolderName(?string $cardHolderName): self
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    public function getDeliveryAddress(): ?GenAddress
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?GenAddress $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    public function getDeliveryFee()
    {
        return $this->deliveryFee;
    }

    public function setDeliveryFee($deliveryFee): self
    {
        $this->deliveryFee = $deliveryFee;

        return $this;
    }

    public function getConvenienceFee()
    {
        return $this->convenienceFee;
    }

    public function setConvenienceFee($convenienceFee): self
    {
        $this->convenienceFee = $convenienceFee;

        return $this;
    }

    public function getOAReponse(): ?string
    {
        return $this->OAReponse;
    }

    public function setOAReponse(?string $OAReponse): self
    {
        $this->OAReponse = $OAReponse;

        return $this;
    }

    public function getOAsuccess(): ?string
    {
        return $this->OAsuccess;
    }

    public function setOAsuccess(?string $OAsuccess): self
    {
        $this->OAsuccess = $OAsuccess;

        return $this;
    }

    public function getOrderSerTimesheetId(): ?string
    {
        return $this->orderSerTimesheetId;
    }

    public function setOrderSerTimesheetId(?string $orderSerTimesheetId): self
    {
        $this->orderSerTimesheetId = $orderSerTimesheetId;

        return $this;
    }

    public function getOrderSerTimeTimesheetId(): ?string
    {
        return $this->orderSerTimeTimesheetId;
    }

    public function setOrderSerTimeTimesheetId(?string $orderSerTimeTimesheetId): self
    {
        $this->orderSerTimeTimesheetId = $orderSerTimeTimesheetId;

        return $this;
    }

    public function getOrderConfigStoreDeliversId(): ?string
    {
        return $this->orderConfigStoreDeliversId;
    }

    public function setOrderConfigStoreDeliversId(?string $orderConfigStoreDeliversId): self
    {
        $this->orderConfigStoreDeliversId = $orderConfigStoreDeliversId;

        return $this;
    }

    public function getOrderScheduledDate(): ?string
    {
        return $this->orderScheduledDate;
    }

    public function setOrderScheduledDate(?string $orderScheduledDate): self
    {
        $this->orderScheduledDate = $orderScheduledDate;

        return $this;
    }

    public function getTroco(): ?string
    {
        return $this->troco;
    }

    public function setTroco(?string $troco): self
    {
        $this->troco = $troco;

        return $this;
    }

    public function getLoggiResponse(): ?string
    {
        return $this->loggiResponse;
    }

    public function setLoggiResponse(?string $loggiResponse): self
    {
        $this->loggiResponse = $loggiResponse;

        return $this;
    }

    public function getLoggiPk(): ?string
    {
        return $this->loggiPk;
    }

    public function setLoggiPk(?string $loggiPk): self
    {
        $this->loggiPk = $loggiPk;

        return $this;
    }

    public function getResponseLog(): ?string
    {
        return $this->responseLog;
    }

    public function setResponseLog(?string $responseLog): self
    {
        $this->responseLog = $responseLog;

        return $this;
    }

    public function getRequestLog(): ?string
    {
        return $this->requestLog;
    }

    public function setRequestLog(?string $requestLog): self
    {
        $this->requestLog = $requestLog;

        return $this;
    }

    public function getFromTaa(): ?string
    {
        return $this->fromTaa;
    }

    public function setFromTaa(string $fromTaa): self
    {
        $this->fromTaa = $fromTaa;

        return $this;
    }

    public function getBarCode(): ?string
    {
        return $this->barCode;
    }

    public function setBarCode(?string $barCode): self
    {
        $this->barCode = $barCode;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(?string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getCupomId(): ?int
    {
        return $this->cupomId;
    }

    public function setCupomId(?int $cupomId): self
    {
        $this->cupomId = $cupomId;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getPlayDeliveryResponse(): ?string
    {
        return $this->playDeliveryResponse;
    }

    public function setPlayDeliveryResponse(?string $playDeliveryResponse): self
    {
        $this->playDeliveryResponse = $playDeliveryResponse;

        return $this;
    }

    public function getBodySaveOrderExternalApi(): ?string
    {
        return $this->body_save_order_external_api;
    }

    public function setBodySaveOrderExternalApi(?string $body_save_order_external_api): self
    {
        $this->body_save_order_external_api = $body_save_order_external_api;

        return $this;
    }

    public function getPosStatus(): ?string
    {
        return $this->posStatus;
    }

    public function setPosStatus(?string $posStatus): self
    {
        $this->posStatus = $posStatus;

        return $this;
    }

}
