<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class PayCreditcard
{
    protected $expirationDate;

    protected $flag;

    protected $lastNumbers;

    protected $token;

    protected $status;

    protected $customerId;

    protected $gateway;

    protected $cardFullName;
    
    protected $currentLimit;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $genUser;
    
    protected $payWallet;
    
    protected $responseLog;

    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?string $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getFlag(): ?string
    {
        return $this->flag;
    }

    public function setFlag(?string $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    public function getLastNumbers(): ?int
    {
        return $this->lastNumbers;
    }

    public function setLastNumbers(?int $lastNumbers): self
    {
        $this->lastNumbers = $lastNumbers;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getGateway(): ?string
    {
        return $this->gateway;
    }

    public function setGateway(?string $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getCardFullName(): ?string
    {
        return $this->cardFullName;
    }

    public function setCardFullName(?string $cardFullName): self
    {
        $this->cardFullName = $cardFullName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenUser(): ?GenUser
    {
        return $this->genUser;
    }

    public function setGenUser(?GenUser $genUser): self
    {
        $this->genUser = $genUser;

        return $this;
    }

    public function getCurrentLimit()
    {
        return $this->currentLimit;
    }

    public function setCurrentLimit($currentLimit): self
    {
        $this->currentLimit = $currentLimit;

        return $this;
    }

    public function getPayWallet(): ?PayWallet
    {
        return $this->payWallet;
    }

    public function setPayWallet(?PayWallet $payWallet): self
    {
        $this->payWallet = $payWallet;

        return $this;
    }

    public function getResponseLog(): ?string
    {
        return $this->responseLog;
    }

    public function setResponseLog(?string $responseLog): self
    {
        $this->responseLog = $responseLog;

        return $this;
    }
}
