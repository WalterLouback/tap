<?php
namespace Zeedhi\ApiGeneral\Model\Entities;

abstract class OrdConfigStoreDelivers {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $typeName;
    /** @var string  */
    protected $typeDelivers;
    /** @var string  */
    protected $allowsScheduling;
    /** @var int  */
    protected $minValue;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $event;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $maxScheduling;
    /** @var string  */
    protected $schedulingUntil;
    
	public function getCreatedAt() {
              return $this->createdAt;
          }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
              return $this->modifiedAt;
          }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
              return $this->createdBy;
          }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
              return $this->modifiedBy;
          }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
              $this->id = $id;
          }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
              $this->nrorg = $nrorg;
          }
    public function getTypeDelivers() {
        return $this->typeDelivers;
    }
	public function setTypeDelivers($typeDelivers) {
              $this->typeDelivers = $typeDelivers;
          }
    public function getTypeName() {
        return $this->typeName;
    }
	public function setTypeName($typeName) {
              $this->typeName = $typeName;
          }
	public function getEvent() {
              return $this->event;
          }
	public function setEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $event = NULL) {
              $this->event = $event;
          }
    public function getMinValue() {
        return $this->minValue;
    }
    public function setMinValue($minValue) {
        $this->minValue = $minValue;
    }
    public function getAllowsScheduling() {
        return $this->allowsScheduling;
    }
    public function setAllowsScheduling($allowsScheduling) {
        $this->allowsScheduling = $allowsScheduling;
    }
    public function getMaxScheduling() {
        return $this->maxScheduling;
    }
    public function setMaxScheduling($maxScheduling) {
        $this->maxScheduling = $maxScheduling;
    }

    public function getSchedulingUntil(): ?string
    {
        return $this->schedulingUntil;
    }

    public function setSchedulingUntil(string $schedulingUntil): self
    {
        $this->schedulingUntil = $schedulingUntil;

        return $this;
    }
    
    
}