<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdRecipient
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $externalId;

    protected $evtEvent;

    protected $bankName;

    protected $bankCode;

    protected $bankAgency;

    protected $bankAgencyVerifyDigit;

    protected $bankAccount;

    protected $bankAccountType;

    protected $bankAccountVerifyDigit;

    protected $anticipatableVolumePercentage;

    protected $automaticAnticipationEnabled;

    protected $transferDay;

    protected $transferEnabled;

    protected $transferInterval;

    protected $payGateway;
    // individual or company
    protected $holderType;

    protected $holderDocumentNumber;

    protected $holderName;

    protected $status;

    protected $description;

    protected $email;

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getBankCode(): ?string
    {
        return $this->bankCode;
    }

    public function setBankCode(?string $bankCode): self
    {
        $this->bankCode = $bankCode;

        return $this;
    }

    public function getBankAgency(): ?string
    {
        return $this->bankAgency;
    }

    public function setBankAgency(?string $bankAgency): self
    {
        $this->bankAgency = $bankAgency;

        return $this;
    }

    public function getBankAgencyVerifyDigit(): ?string
    {
        return $this->bankAgencyVerifyDigit;
    }

    public function setBankAgencyVerifyDigit(?string $bankAgencyVerifyDigit): self
    {
        $this->bankAgencyVerifyDigit = $bankAgencyVerifyDigit;

        return $this;
    }

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(?string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getBankAccountType(): ?string
    {
        return $this->bankAccountType;
    }

    public function setBankAccountType(?string $bankAccountType): self
    {
        $this->bankAccountType = $bankAccountType;

        return $this;
    }

    public function getBankAccountVerifyDigit(): ?string
    {
        return $this->bankAccountVerifyDigit;
    }

    public function setBankAccountVerifyDigit(?string $bankAccountVerifyDigit): self
    {
        $this->bankAccountVerifyDigit = $bankAccountVerifyDigit;

        return $this;
    }

    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }

    public function setDocumentNumber(?string $documentNumber): self
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getAnticipatableVolumePercentage()
    {
        return $this->anticipatableVolumePercentage;
    }

    public function setAnticipatableVolumePercentage($anticipatableVolumePercentage): self
    {
        $this->anticipatableVolumePercentage = $anticipatableVolumePercentage;

        return $this;
    }

    public function getAutomaticAnticipationEnabled(): ?int
    {
        return $this->automaticAnticipationEnabled;
    }

    public function setAutomaticAnticipationEnabled(?int $automaticAnticipationEnabled): self
    {
        $this->automaticAnticipationEnabled = $automaticAnticipationEnabled;

        return $this;
    }

    public function getTransferDay(): ?int
    {
        return $this->transferDay;
    }

    public function setTransferDay(?int $transferDay): self
    {
        $this->transferDay = $transferDay;

        return $this;
    }

    public function getTransferEnabled(): ?int
    {
        return $this->transferEnabled;
    }

    public function setTransferEnabled(?int $transferEnabled): self
    {
        $this->transferEnabled = $transferEnabled;

        return $this;
    }

    public function getTransferInterval(): ?string
    {
        return $this->transferInterval;
    }

    public function setTransferInterval(?string $transferInterval): self
    {
        $this->transferInterval = $transferInterval;

        return $this;
    }

    public function getPayGateway(): ?PayGateway
    {
        return $this->payGateway;
    }

    public function setPayGateway(?PayGateway $payGateway): self
    {
        $this->payGateway = $payGateway;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getHolderType(): ?string
    {
        return $this->holderType;
    }

    public function setHolderType(?string $holderType): self
    {
        $this->holderType = $holderType;

        return $this;
    }

    public function getHolderDocumentNumber(): ?string
    {
        return $this->holderDocumentNumber;
    }

    public function setHolderDocumentNumber(?string $holderDocumentNumber): self
    {
        $this->holderDocumentNumber = $holderDocumentNumber;

        return $this;
    }

    public function getHolderName(): ?string
    {
        return $this->holderName;
    }

    public function setHolderName(?string $holderName): self
    {
        $this->holderName = $holderName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
