<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SurQuestion
{
    protected $name;

    protected $type;

    protected $hasComment;

    protected $note;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $status;

    protected $limitQuantity;

    protected $id;

    protected $evtEvent;

    protected $genStructure;

    protected $surSurvey;
    
    protected $serServiceSeller;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getHasComment(): ?int
    {
        return $this->hasComment;
    }

    public function setHasComment(?int $hasComment): self
    {
        $this->hasComment = $hasComment;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getLimitQuantity(): ?int
    {
        return $this->limitQuantity;
    }

    public function setLimitQuantity(?int $limitQuantity): self
    {
        $this->limitQuantity = $limitQuantity;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getGenStructure(): ?GenStructure
    {
        return $this->genStructure;
    }

    public function setGenStructure(?GenStructure $genStructure): self
    {
        $this->genStructure = $genStructure;

        return $this;
    }

    public function getSurSurvey(): ?SurSurvey
    {
        return $this->surSurvey;
    }

    public function setSurSurvey(?SurSurvey $surSurvey): self
    {
        $this->surSurvey = $surSurvey;

        return $this;
    }

    public function getSerServiceSeller(): ?SerServiceSellerRel
    {
        return $this->serServiceSeller;
    }

    public function setSerServiceSeller(?SerServiceSellerRel $serServiceSeller): self
    {
        $this->serServiceSeller = $serServiceSeller;

        return $this;
    }
}
