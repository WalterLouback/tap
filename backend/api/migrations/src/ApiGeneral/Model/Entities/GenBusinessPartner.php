<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenBusinessPartner
{
    protected $id;
    protected $nrorg;
    protected $document;
    protected $documentType;
    protected $type;
    protected $lastName;
    protected $image;
    protected $firstName;
    protected $birthDate;
    protected $email;
    protected $procobAlertaMonitore;
    protected $procobNumParticipacoes;
    protected $uf;
    protected $nationality;
    protected $death;
    protected $publicExposuresCount;
    protected $gender;
    protected $zodiacSign;
    protected $situacaoReceita;
    protected $situacaoReceitaData;
    protected $otherBirthDates;
    protected $otherNameSpellings;
    protected $createdAt;
    protected $createdBy;
    protected $modifiedAt;
    protected $modifiedBy;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function setDocument(?string $document): self
    {
        $this->document = $document;

        return $this;
    }

    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }

    public function setDocumentType(?string $documentType): self
    {
        $this->documentType = $documentType;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getProcobAlertaMonitore(): ?int
    {
        return $this->procobAlertaMonitore;
    }

    public function setProcobAlertaMonitore(?int $procobAlertaMonitore): self
    {
        $this->procobAlertaMonitore = $procobAlertaMonitore;

        return $this;
    }

    public function getProcobNumParticipacoes(): ?int
    {
        return $this->procobNumParticipacoes;
    }

    public function setProcobNumParticipacoes(?int $procobNumParticipacoes): self
    {
        $this->procobNumParticipacoes = $procobNumParticipacoes;

        return $this;
    }

    public function getUf(): ?string
    {
        return $this->uf;
    }

    public function setUf(?string $uf): self
    {
        $this->uf = $uf;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getDeath(): ?int
    {
        return $this->death;
    }

    public function setDeath(?int $death): self
    {
        $this->death = $death;

        return $this;
    }

    public function getPublicExposuresCount(): ?int
    {
        return $this->publicExposuresCount;
    }

    public function setPublicExposuresCount(?int $publicExposuresCount): self
    {
        $this->publicExposuresCount = $publicExposuresCount;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getZodiacSign(): ?string
    {
        return $this->zodiacSign;
    }

    public function setZodiacSign(?string $zodiacSign): self
    {
        $this->zodiacSign = $zodiacSign;

        return $this;
    }

    public function getSituacaoReceita(): ?string
    {
        return $this->situacaoReceita;
    }

    public function setSituacaoReceita(?string $situacaoReceita): self
    {
        $this->situacaoReceita = $situacaoReceita;

        return $this;
    }

    public function getSituacaoReceitaData(): ?\DateTimeInterface
    {
        return $this->situacaoReceitaData;
    }

    public function setSituacaoReceitaData(?\DateTimeInterface $situacaoReceitaData): self
    {
        $this->situacaoReceitaData = $situacaoReceitaData;

        return $this;
    }

    public function getOtherBirthDates(): ?string
    {
        return $this->otherBirthDates;
    }

    public function setOtherBirthDates(?string $otherBirthDates): self
    {
        $this->otherBirthDates = $otherBirthDates;

        return $this;
    }

    public function getOtherNameSpellings(): ?string
    {
        return $this->otherNameSpellings;
    }

    public function setOtherNameSpellings(?string $otherNameSpellings): self
    {
        $this->otherNameSpellings = $otherNameSpellings;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
