<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdConfigStore
{
    protected $deliversToTable;

    protected $deliversToBalcony;
    
    protected $deliversToAddress;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $event;

    protected $workflow;
    
    protected $minValue;
    
    protected $storeCod;
    
    protected $urlIntegration;
    
    protected $urlQrcode;
    
    protected $gorjeta;
    
    protected $timerTaa;
    
    protected $askName;
    
    protected $acceptVoucher;
    
    protected $lockId;
    
    protected $establishmentId;
    
    protected $segmented;

    public function getDeliversToTable(): ?string
    {
        return $this->deliversToTable;
    }

    public function setDeliversToTable(?string $deliversToTable): self
    {
        $this->deliversToTable = $deliversToTable;

        return $this;
    }

    public function getDeliversToBalcony(): ?string
    {
        return $this->deliversToBalcony;
    }

    public function setDeliversToBalcony(?string $deliversToBalcony): self
    {
        $this->deliversToBalcony = $deliversToBalcony;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?EvtEvent
    {
        return $this->event;
    }

    public function setEvent(?EvtEvent $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getWorkflow(): ?OrdWorkflow
    {
        return $this->workflow;
    }

    public function setWorkflow(?OrdWorkflow $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }

    public function getDeliversToAddress(): ?string
    {
        return $this->deliversToAddress;
    }

    public function setDeliversToAddress(?string $deliversToAddress): self
    {
        $this->deliversToAddress = $deliversToAddress;

        return $this;
    }

    public function getMinValue()
    {
        return $this->minValue;
    }

    public function setMinValue($minValue): self
    {
        $this->minValue = $minValue;

        return $this;
    }

    public function getStoreCod()
    {
        return $this->storeCod;
    }

    public function setStoreCod($storeCod): self
    {
        $this->storeCod = $storeCod;

        return $this;
    }

    public function getUrlIntegration()
    {
        return $this->urlIntegration;
    }

    public function setUrlIntegration($urlIntegration): self
    {
        $this->urlIntegration = $urlIntegration;

        return $this;
    }

    public function getGorjeta(): ?int
    {
        return $this->gorjeta;
    }

    public function setGorjeta(?int $gorjeta): self
    {
        $this->gorjeta = $gorjeta;

        return $this;
    }

    public function getTimerTaa(): ?string
    {
        return $this->timerTaa;
    }

    public function setTimerTaa(?string $timerTaa): self
    {
        $this->timerTaa = $timerTaa;

        return $this;
    }

    public function getUrlQrcode(): ?string
    {
        return $this->urlQrcode;
    }

    public function setUrlQrcode(?string $urlQrcode): self
    {
        $this->urlQrcode = $urlQrcode;

        return $this;
    }
    
    public function getAskName(): ?bool
    {
        return $this->askName;
    }

    public function setAskName(?bool $askName): self
    {
        $this->askName = $askName;

        return $this;
    }
    
    public function getLockId(): ?string
    {
        return $this->lockId;
    }

    public function setLockId(?string $lockId): self
    {
        $this->lockId = $lockId;

        return $this;
    }

    public function getEstablishmentId(): ?string
    {
        return $this->establishmentId;
    }

    public function setEstablishmentId(?string$establishmentId): self
    {
        $this->establishmentId = $establishmentId;

        return $this;
    }

    public function getSegmented(): ?bool
    {
        return $this->segmented;
    }

    public function setSegmented(?bool $segmented): self
    {
        $this->segmented = $segmented;

        return $this;
    }

    public function getAcceptVoucher(): ?bool
    {
        return $this->acceptVoucher;
    }

    public function setAcceptVoucher(?bool $acceptVoucher): self
    {
        $this->acceptVoucher = $acceptVoucher;

        return $this;
    }
}
