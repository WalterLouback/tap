<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdOrderRecipient
{
    protected $id;
    protected $percentage;
    protected $fixedValue;
    protected $nrorg;
    protected $createdAt;
    protected $modifiedAt;
    protected $createdBy;
    protected $modifiedBy;
    protected $ordOrder;
    protected $ordRecipient;

    public function getPercentage()
    {
        return $this->percentage;
    }

    public function setPercentage($percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getFixedValue()
    {
        return $this->fixedValue;
    }

    public function setFixedValue($fixedValue): self
    {
        $this->fixedValue = $fixedValue;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrdOrder(): ?OrdOrder
    {
        return $this->ordOrder;
    }

    public function setOrdOrder(?OrdOrder $ordOrder): self
    {
        $this->ordOrder = $ordOrder;

        return $this;
    }

    public function getOrdRecipient(): ?OrdRecipient
    {
        return $this->ordRecipient;
    }

    public function setOrdRecipient(?OrdRecipient $ordRecipient): self
    {
        $this->ordRecipient = $ordRecipient;

        return $this;
    }
}
