<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenDependent
{
    protected $monthlyLimit;

    protected $receiptsTo;

    protected $status;

    protected $nrorg;

    protected $canUseMainCard;

    protected $receiveNotification;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;

    protected $dependent;

    protected $parent;

    public function getMonthlyLimit()
    {
        return $this->monthlyLimit;
    }

    public function setMonthlyLimit($monthlyLimit): self
    {
        $this->monthlyLimit = $monthlyLimit;

        return $this;
    }

    public function getReceiptsTo(): ?string
    {
        return $this->receiptsTo;
    }

    public function setReceiptsTo(?string $receiptsTo): self
    {
        $this->receiptsTo = $receiptsTo;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCanUseMainCard(): ?string
    {
        return $this->canUseMainCard;
    }

    public function setCanUseMainCard(?string $canUseMainCard): self
    {
        $this->canUseMainCard = $canUseMainCard;

        return $this;
    }

    public function getReceiveNotification(): ?int
    {
        return $this->receiveNotification;
    }

    public function setReceiveNotification(?int $receiveNotification): self
    {
        $this->receiveNotification = $receiveNotification;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDependent(): ?GenUser
    {
        return $this->dependent;
    }

    public function setDependent(?GenUser $dependent): self
    {
        $this->dependent = $dependent;

        return $this;
    }

    public function getParent(): ?GenUser
    {
        return $this->parent;
    }

    public function setParent(?GenUser $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
