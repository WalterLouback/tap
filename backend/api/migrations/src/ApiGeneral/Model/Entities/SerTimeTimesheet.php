<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SerTimeTimesheet
{
    protected $id;
    
    protected $initialTime;

    protected $finalTime;
    
    protected $serDayTimesheetValues;
    
    protected $serTimesheet;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;
    
    protected $type;

    protected $maxScheduling;

    public function getInitialTime(): ?string
    {
        return $this->initialTime;
    }

    public function setInitialTime(?string $initialTime): self
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime(): ?string
    {
        return $this->finalTime;
    }

    public function setFinalTime(?string $finalTime): self
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?string
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?string $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerTimesheet(): ?SerTimesheet
    {
        return $this->serTimesheet;
    }

    public function setSerTimesheet(?SerTimesheet $serTimesheet): self
    {
        $this->serTimesheet = $serTimesheet;

        return $this;
    }

    public function getSerDayTimesheetValues(): ?string
    {
        return $this->serDayTimesheetValues;
    }

    public function setSerDayTimesheetValues(?string $serDayTimesheetValues): self
    {
        $this->serDayTimesheetValues = $serDayTimesheetValues;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMaxScheduling(): ?string
    {
        return $this->maxScheduling;
    }

    public function setMaxScheduling(?string $maxScheduling): self
    {
        $this->maxScheduling = $maxScheduling;

        return $this;
    }
}