<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdLockOpening
{
    protected $nrorg;

    protected $id;

    protected $lockId;
    
    protected $doorNumber;
    
    protected $status;
    
    protected $ordOrder;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    public function getLockId(): ?int
    {
        return $this->lockId;
    }

    public function setLockId(?int $lockId): self
    {
        $this->lockId = $lockId;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?string $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrdOrder(): ?OrdOrder
    {
        return $this->ordOrder;
    }

    public function setOrdOrder(?OrdOrder $ordOrder): self
    {
        $this->ordOrder = $ordOrder;

        return $this;
    }

    public function getDoorNumber(): ?string
    {
        return $this->doorNumber;
    }

    public function setDoorNumber(?string $doorNumber): self
    {
        $this->doorNumber = $doorNumber;

        return $this;
    }
}
