<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdExtra
{
    protected $name;

    protected $required;

    protected $multiple;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $limitQuantity;

    protected $limitQuantityMin;

    protected $id;

    protected $ordMenuProduct;

    protected $ordination;
    
    protected $integrationCode;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRequired(): ?int
    {
        return $this->required;
    }

    public function setRequired(?int $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function getMultiple(): ?int
    {
        return $this->multiple;
    }

    public function setMultiple(?int $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getLimitQuantity(): ?int
    {
        return $this->limitQuantity;
    }

    public function setLimitQuantity(?int $limitQuantity): self
    {
        $this->limitQuantity = $limitQuantity;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdMenuProduct(): ?OrdMenuProduct
    {
        return $this->ordMenuProduct;
    }

    public function setOrdMenuProduct(?OrdMenuProduct $ordMenuProduct): self
    {
        $this->ordMenuProduct = $ordMenuProduct;

        return $this;
    }

    public function getOrdination(): ?int
    {
        return $this->ordination;
    }

    public function setOrdination(?int $ordination): self
    {
        $this->ordination = $ordination;

        return $this;
    }

    public function getLimitQuantityMin(): ?int
    {
        return $this->limitQuantityMin;
    }

    public function setLimitQuantityMin(?int $limitQuantityMin): self
    {
        $this->limitQuantityMin = $limitQuantityMin;

        return $this;
    }

    public function getIntegrationCode(): ?string
    {
        return $this->integrationCode;
    }

    public function setIntegrationCode(?string $integrationCode): self
    {
        $this->integrationCode = $integrationCode;

        return $this;
    }
}
