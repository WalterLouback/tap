<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenStatusUserType
{
    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $evtSellerType;

    protected $genStatus;

    protected $genUserType;

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtSellerType(): ?EvtSellerType
    {
        return $this->evtSellerType;
    }

    public function setEvtSellerType(?EvtSellerType $evtSellerType): self
    {
        $this->evtSellerType = $evtSellerType;

        return $this;
    }

    public function getGenStatus(): ?GenStatus
    {
        return $this->genStatus;
    }

    public function setGenStatus(?GenStatus $genStatus): self
    {
        $this->genStatus = $genStatus;

        return $this;
    }

    public function getGenUserType(): ?GenUserType
    {
        return $this->genUserType;
    }

    public function setGenUserType(?GenUserType $genUserType): self
    {
        $this->genUserType = $genUserType;

        return $this;
    }
}
