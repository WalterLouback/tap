<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class GenUserBenefit
{
    protected $id;
    protected $number;
    protected $retirementDate;
    protected $type;
    protected $retired;
    protected $inss;
    protected $workAccident;
    protected $sicknessWithdrawal;
    protected $loan;
    protected $bankName;
    protected $bankAgency;
    protected $bankAddress;
    protected $bankNumber;
    protected $bankComplement;
    protected $bankNeighborhood;
    protected $bankCep;
    protected $bankCity;
    protected $bankUf;
    protected $nrorg;
    protected $genUser;
    protected $genBusinessPartner;
    protected $createdAt;
    protected $modifiedAt;
    protected $createdBy;
    protected $modifiedBy;

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getRetirementDate(): ?int
    {
        return $this->retirementDate;
    }

    public function setRetirementDate(?int $retirementDate): self
    {
        $this->retirementDate = $retirementDate;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRetired(): ?int
    {
        return $this->retired;
    }

    public function setRetired(?int $retired): self
    {
        $this->retired = $retired;

        return $this;
    }

    public function getInss(): ?int
    {
        return $this->inss;
    }

    public function setInss(?int $inss): self
    {
        $this->inss = $inss;

        return $this;
    }

    public function getWorkAccident(): ?int
    {
        return $this->workAccident;
    }

    public function setWorkAccident(?int $workAccident): self
    {
        $this->workAccident = $workAccident;

        return $this;
    }

    public function getSicknessWithdrawal(): ?int
    {
        return $this->sicknessWithdrawal;
    }

    public function setSicknessWithdrawal(?int $sicknessWithdrawal): self
    {
        $this->sicknessWithdrawal = $sicknessWithdrawal;

        return $this;
    }

    public function getLoan(): ?int
    {
        return $this->loan;
    }

    public function setLoan(?int $loan): self
    {
        $this->loan = $loan;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getBankAgency(): ?string
    {
        return $this->bankAgency;
    }

    public function setBankAgency(?string $bankAgency): self
    {
        $this->bankAgency = $bankAgency;

        return $this;
    }

    public function getBankAddress(): ?string
    {
        return $this->bankAddress;
    }

    public function setBankAddress(?string $bankAddress): self
    {
        $this->bankAddress = $bankAddress;

        return $this;
    }

    public function getBankNumber(): ?string
    {
        return $this->bankNumber;
    }

    public function setBankNumber(?string $bankNumber): self
    {
        $this->bankNumber = $bankNumber;

        return $this;
    }

    public function getBankComplement(): ?string
    {
        return $this->bankComplement;
    }

    public function setBankComplement(?string $bankComplement): self
    {
        $this->bankComplement = $bankComplement;

        return $this;
    }

    public function getBankNeighborhood(): ?string
    {
        return $this->bankNeighborhood;
    }

    public function setBankNeighborhood(?string $bankNeighborhood): self
    {
        $this->bankNeighborhood = $bankNeighborhood;

        return $this;
    }

    public function getBankCep(): ?string
    {
        return $this->bankCep;
    }

    public function setBankCep(?string $bankCep): self
    {
        $this->bankCep = $bankCep;

        return $this;
    }

    public function getBankCity(): ?string
    {
        return $this->bankCity;
    }

    public function setBankCity(?string $bankCity): self
    {
        $this->bankCity = $bankCity;

        return $this;
    }

    public function getBankUf(): ?string
    {
        return $this->bankUf;
    }

    public function setBankUf(?string $bankUf): self
    {
        $this->bankUf = $bankUf;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenBusinessPartner(): ?GenBusinessPartner
    {
        return $this->genBusinessPartner;
    }

    public function setGenBusinessPartner(?GenBusinessPartner $genBusinessPartner): self
    {
        $this->genBusinessPartner = $genBusinessPartner;

        return $this;
    }
}
