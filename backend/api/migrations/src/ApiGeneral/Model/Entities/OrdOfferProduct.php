<?php
namespace Zeedhi\ApiGeneral\Model\Entities;


abstract class OrdOfferProduct {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $amount = 0;
    /** @var float  */
    protected $price = 0.0;
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\OrdMenuProduct  */
    protected $ordMenuProduct;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\OrdMenuProduct  */
    protected $ordOffer;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getAmount() {
        return $this->amount;
    }
	public function setAmount($amount) {
        $this->amount = $amount;
    }
	public function getPrice() {
        return $this->price;
    }
	public function setPrice($price) {
        $this->price = $price;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getOrdMenuProduct() {
        return $this->ordMenuProduct;
    }
	public function setOrdMenuProduct(\Zeedhi\ApiGeneral\Model\Entities\OrdMenuProduct $ordMenuProduct = NULL) {
        $this->ordMenuProduct = $ordMenuProduct;
    }
	public function getOrdOffer() {
        return $this->ordOffer;
    }
	public function setOrdOffer(\Zeedhi\ApiGeneral\Model\Entities\OrdMenuProduct $ordOffer = NULL) {
        $this->ordOffer = $ordOffer;
    }
}