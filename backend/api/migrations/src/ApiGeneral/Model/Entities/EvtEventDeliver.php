<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class EvtEventDeliver {
    
    protected $id;
    
    protected $distanceKm;
    
    protected $price;
    
    protected $nrorg;
    
    protected $createdBy;
    
    protected $modifiedBy;
    
    protected $createdAt;
    
    protected $modifiedAt;
    
    protected $evtEvent;
    
    protected $startKm;

    protected $endKm;

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getDistanceKm(): ?string
    {
        return $this->distanceKm;
    }

    public function setDistanceKm(?string $distanceKm): self
    {
        $this->distanceKm = $distanceKm;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?string
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?string $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getEndKm(): ?string
    {
        return $this->endKm;
    }

    public function setEndKm(?string $endKm): self
    {
        $this->endKm = $endKm;

        return $this;
    }

    public function getStartKm(): ?string
    {
        return $this->startKm;
    }

    public function setStartKm(?string $startKm): self
    {
        $this->startKm = $startKm;

        return $this;
    }
}
