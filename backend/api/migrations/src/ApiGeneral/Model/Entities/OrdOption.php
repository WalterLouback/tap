<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class OrdOption
{
    protected $name;

    protected $price;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $nrorg;

    protected $id;

    protected $ordExtra;
    
    protected $active;

    protected $ordination;
    
    protected $integrationCode;


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdExtra(): ?OrdExtra
    {
        return $this->ordExtra;
    }

    public function setOrdExtra(?OrdExtra $ordExtra): self
    {
        $this->ordExtra = $ordExtra;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(?int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getOrdination(): ?int
    {
        return $this->ordination;
    }

    public function setOrdination(?int $ordination): self
    {
        $this->ordination = $ordination;

        return $this;
    }

    public function getIntegrationCode(): ?string
    {
        return $this->integrationCode;
    }

    public function setIntegrationCode(?string $integrationCode): self
    {
        $this->integrationCode = $integrationCode;

        return $this;
    }
}
