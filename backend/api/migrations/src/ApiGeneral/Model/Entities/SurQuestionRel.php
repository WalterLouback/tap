<?php

namespace Zeedhi\ApiGeneral\Model\Entities;

class SurQuestionRel
{
    protected $id;

    protected $status;
    
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $surQuestion;

    protected $evtEvent;
    
    protected $evtEventSeller;
    
    protected $serService;

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?int $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerService(): ?SerService
    {
        return $this->serService;
    }

    public function setSerService(?SerService $serService): self
    {
        $this->serService = $serService;

        return $this;
    }

    public function getEvtEvent(): ?EvtEvent
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(?EvtEvent $evtEvent): self
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getEvtEventSeller(): ?EvtEventSeller
    {
        return $this->evtEventSeller;
    }

    public function setEvtEventSeller(?EvtEventSeller $evtEventSeller): self
    {
        $this->evtEventSeller = $evtEventSeller;

        return $this;
    }

    public function getSurQuestion(): ?SurQuestion
    {
        return $this->surQuestion;
    }

    public function setSurQuestion(?SurQuestion $surQuestion): self
    {
        $this->surQuestion = $surQuestion;

        return $this;
    }
}
