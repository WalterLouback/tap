<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200326193904 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER ADD DELIVERY_ADDRESS BIGINT DEFAULT NULL, CHANGE NOTE NOTE VARCHAR(1000) DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER ADD CONSTRAINT FK_79E261BE8AF48DD7 FOREIGN KEY (DELIVERY_ADDRESS) REFERENCES GEN_ADDRESS (ID)');
        $this->addSql('CREATE INDEX IDX_79E261BE8AF48DD7 ON ORD_ORDER (DELIVERY_ADDRESS)');
        $this->addSql('ALTER TABLE ORD_CONFIG_STORE ADD DELIVERS_TO_ADDRESS VARCHAR(1) DEFAULT \'F\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_CONFIG_STORE DROP DELIVERS_TO_ADDRESS');
        $this->addSql('ALTER TABLE ORD_ORDER DROP FOREIGN KEY FK_79E261BE8AF48DD7');
        $this->addSql('DROP INDEX IDX_79E261BE8AF48DD7 ON ORD_ORDER');
        $this->addSql('ALTER TABLE ORD_ORDER DROP DELIVERY_ADDRESS, CHANGE NOTE NOTE VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
