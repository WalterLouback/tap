<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519152420 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        // $this->addSql('UPDATE GEN_CONFIGURATION SET URL = "https://blackbeef.eattake.com" WHERE NRORG = 2847 AND ID = 19');
        // $this->addSql('UPDATE GEN_CONFIGURATION SET URL = "https://casagrao.eattake.com" WHERE NRORG = 8 AND ID = 1');
        // $this->addSql('UPDATE GEN_CONFIGURATION SET URL = "https://eattake.com" WHERE NRORG = 13 AND ID = 17');
        

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL DROP FOREIGN KEY FK_7634DC7B3589CB8B');
        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL CHANGE ORD_ORDER_ID ORD_ORDER_ID BIGINT DEFAULT NULL');
        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL ADD CONSTRAINT FK_7634DC7B3589CB8B FOREIGN KEY (ORD_ORDER_ID) REFERENCES ORD_ORDER (ORDER_IDENTIFIER)');
    }
}
