<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190812140213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE GEN_SCHEDULE_STRUCTURE_REL');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE ADD SER_SERVICE_ID BIGINT DEFAULT NULL, ADD SER_SERVICE_SELLER_ID BIGINT DEFAULT NULL');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D72C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (id)');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D7E2359285 FOREIGN KEY (SER_SERVICE_SELLER_ID) REFERENCES SER_SERVICE_SELLER_REL (id)');
        // $this->addSql('CREATE INDEX IDX_4C80C6D7E2359285 ON GEN_SCHEDULE (SER_SERVICE_SELLER_ID)');
        // $this->addSql('CREATE INDEX SER_SERVICE_IDX ON GEN_SCHEDULE (SER_SERVICE_ID)');
        // $this->addSql('CREATE INDEX SER_SERVICE_SELLER_IDX ON GEN_SCHEDULE (SER_SERVICE_ID)');
        $this->addSql('ALTER TABLE ORD_ORDER ADD SER_SERVICE_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER ADD CONSTRAINT FK_79E261BE2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (id)');
        $this->addSql('CREATE INDEX SER_SERVICE_FK_idx ON ORD_ORDER (SER_SERVICE_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('CREATE TABLE GEN_SCHEDULE_STRUCTURE_REL (NRORG BIGINT DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_STRUCTURE_ID BIGINT DEFAULT NULL, GEN_SCHEDULE_ID BIGINT DEFAULT NULL, INDEX GEN_SCHE_STRU_REL__SCHED_FK (GEN_SCHEDULE_ID), INDEX GEN_SCHE_STRU_REL__STRUC_FK (GEN_STRUCTURE_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE_STRUCTURE_REL ADD CONSTRAINT FK_C2D3D653075114B FOREIGN KEY (GEN_SCHEDULE_ID) REFERENCES GEN_SCHEDULE (ID)');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE_STRUCTURE_REL ADD CONSTRAINT FK_C2D3D659106090D FOREIGN KEY (GEN_STRUCTURE_ID) REFERENCES GEN_STRUCTURE (ID)');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D72C71C207');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D7E2359285');
        // $this->addSql('DROP INDEX IDX_4C80C6D7E2359285 ON GEN_SCHEDULE');
        // $this->addSql('DROP INDEX SER_SERVICE_IDX ON GEN_SCHEDULE');
        // $this->addSql('DROP INDEX SER_SERVICE_SELLER_IDX ON GEN_SCHEDULE');
        // $this->addSql('ALTER TABLE GEN_SCHEDULE DROP SER_SERVICE_ID, DROP SER_SERVICE_SELLER_ID');
        $this->addSql('ALTER TABLE ORD_ORDER DROP FOREIGN KEY FK_79E261BE2C71C207');
        $this->addSql('DROP INDEX SER_SERVICE_FK_idx ON ORD_ORDER');
        $this->addSql('ALTER TABLE ORD_ORDER DROP SER_SERVICE_ID');
    }
}
