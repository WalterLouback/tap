<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407190955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '253', PAYMENT_MOMENT = '253' WHERE ID = '80' and NRORG = 1869");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '254' WHERE ID = '253' and NRORG = 1869");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '255' WHERE ID = '254' and NRORG = 1869");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '256' WHERE ID = '255' and NRORG = 1869");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '257' WHERE ID = '256' and NRORG = 1869");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
      
    }
}
