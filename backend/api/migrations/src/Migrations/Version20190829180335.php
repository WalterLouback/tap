<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190829180335 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402E2C71C207');
        $this->addSql('DROP INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD EVT_EVENT_ID BIGINT DEFAULT NULL, CHANGE ser_service_id SER_SERVICE_SELLER_REL_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402E5B38904C FOREIGN KEY (SER_SERVICE_SELLER_REL_ID) REFERENCES SER_SERVICE_SELLER_REL (ID)');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402EB4A15433 FOREIGN KEY (EVT_EVENT_ID) REFERENCES EVT_EVENT (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_SELLER_REL_IDX ON SER_TIMESHEET_REL (SER_SERVICE_SELLER_REL_ID)');
        $this->addSql('CREATE INDEX EVT_EVENT_IDX ON SER_TIMESHEET_REL (EVT_EVENT_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402E5B38904C');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402EB4A15433');
        $this->addSql('DROP INDEX SER_SERVICE_SELLER_REL_IDX ON SER_TIMESHEET_REL');
        $this->addSql('DROP INDEX EVT_EVENT_IDX ON SER_TIMESHEET_REL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD SER_SERVICE_ID BIGINT DEFAULT NULL, DROP SER_SERVICE_SELLER_REL_ID, DROP EVT_EVENT_ID');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402E2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL (SER_SERVICE_ID)');
    }
}
