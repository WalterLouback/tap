<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200529194158 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'criei o campo oldTotal para guardar o valor anterior de uma order';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT CHANGE QUANTITY QUANTITY NUMERIC(8, 3) DEFAULT NULL');
        // $this->addSql('ALTER TABLE ORD_ORDER ADD OLD_TOTAL NUMERIC(8, 2) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER DROP OLD_TOTAL');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT CHANGE QUANTITY QUANTITY BIGINT DEFAULT NULL');
    }
}
