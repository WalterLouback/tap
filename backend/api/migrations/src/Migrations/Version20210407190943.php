<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407190943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        #$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        #$this->addSql('INSERT INTO.`ORD_PRODUCT` (`DETAIL`, `NAME`, `PRICE`, `ORD_PRODUCT_GROUP_ID`, `NRORG`, `IMAGE`, `STATUS`, `EVT_EVENT_ID`, `EXTERNAL_ID`) VALUES ("CHOC NESTLE PO 200G 2 FR","CHOC NESTLE PO 200G DOIS FRADES 50% CACAU",13.90,2755,1869,"https://rmcweb.gruporcarvalho.com.br/api/v1/ecommerce/products/44187/images","A",1575,"44187")');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
      
    }
}
