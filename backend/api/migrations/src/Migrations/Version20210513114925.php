<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210513114925 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'CREATE TABLE ORD_LOCK_OPENING';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ORD_LOCK_OPENING (LOCK_ID BIGINT DEFAULT NULL, NRORG BIGINT DEFAULT NULL, STATUS VARCHAR(10) DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, ORD_ORDER_ID VARCHAR(36) DEFAULT NULL, INDEX IDX_655637743589CB8B (ORD_ORDER_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ORD_LOCK_OPENING ADD CONSTRAINT FK_655637743589CB8B FOREIGN KEY (ORD_ORDER_ID) REFERENCES ORD_ORDER (ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
