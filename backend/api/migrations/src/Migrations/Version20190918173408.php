<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190918173408 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD GEN_STRUCTURE_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D79106090D FOREIGN KEY (GEN_STRUCTURE_ID) REFERENCES GEN_STRUCTURE (ID)');
        $this->addSql('CREATE INDEX GEN_STRUCTURE_IDX ON GEN_SCHEDULE (GEN_STRUCTURE_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D79106090D');
        $this->addSql('DROP INDEX GEN_STRUCTURE_IDX ON GEN_SCHEDULE');
        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP GEN_STRUCTURE_ID');
    }
}
