<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504210434 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Achocolatados e Biscoitos", 1389, 38, "current_timestamp", "current_timestamp", 512, 133)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (6.5, current_timestamp, "A", 0, 14, "Bisc. Calipso Chocolate 130 g", "Bisc. Nestle Calipso Chocolate 130 g", "https://s3.amazonaws.com/documentos.minastc.com.br/E8662F22-E6A2-", 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (4.49, current_timestamp, "A", 0, 14, "Bisc. Nestle Prestigio 140 g", "", "https://s3.amazonaws.com/documentos.minastc.com.br/52D708E7-E670-", 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (6.5, "current_timestamp”, "A”, 0, 14, "Bisc. Calipso Chocolate 130 g" ,"Bisc. Nestlé Calipso Chocolate 130 g" ,"https://s3.amazonaws.com/documentos.minastc.com.br/E8662F22-E6A2-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (2.99, "current_timestamp”, "A”, 0, 14, "Bisc. Nestlé Negresco "0", 140 g" ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/8DB77509-4E0C-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (2.9, "current_timestamp”, "A”, 0, 14, "Bisc. Aymoré Maria 200 g " ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/4992D817-BDF2-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (2.65, "current_timestamp”, "A”, 0, 14, "Bisc. Aymoré Cream Cracker 200 g " ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/688E2748-F1EE-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (9.9, "current_timestamp”, "A”, 0, 14, "Feijão Preto Premium Pink 1 kg" ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/1821C4A2-00E7-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (10.5, "current_timestamp”, "A”, 0, 14, "Feijão Roxo Premium Pink 1 kg" ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/2BFC9F18-B674-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (6.44, "current_timestamp”, "A”, 0, 14, "Arroz Branco Prato Fino 1 kg" ,"Arroz Branco Tipo 1 Prato Fino 1 kg" ,"https://s3.amazonaws.com/documentos.minastc.com.br/069B1E4E-D84D-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (2.2, "current_timestamp”, "A”, 0, 14, "Sal Refinado Globo 1 kg" ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/3FD80573-B86E-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (9.38, "current_timestamp”, "A”, 0, 14, "Massa Farfalle Renata 500 g" ,"Massa Farfalle Grano Duro Renata 500 g" ,"https://s3.amazonaws.com/documentos.minastc.com.br/772A6A6B-A4A2-" , 1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (7.99, "current_timestamp”, "A”, 0, 14, "Massa Penne Renata 500 g" ,"Massa Penne Grano Duro Renata 500 g" ,"https://s3.amazonaws.com/documentos.minastc.com.br/BB988666-0F2F-" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (7.35, "current_timestamp”, "A”, 0, 14, "Nescau 2.0 400 g " ,"" ,"https://s3.amazonaws.com/documentos.minastc.com.br/451D0BF5-4E96-" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (9.3, "current_timestamp”, "A”, 0, 14, "Mini PÃ£o de Queijo de Milho" ,"Produto Congelado, Contém10 unidades, peso aproximado 270 g." ,"https://s3.amazonaws.com/documentos.minastc.com.br/591D1BF1-6410-" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (7.48, "current_timestamp”, "A”, 0, 14, "Empadinha " ,"Produto Congelado, Peso aproximado de 0,110 g" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (15.66, "current_timestamp”, "A”, 0, 14, "Mini Coxinha de Frango" ,"Produto Congelado, Peso aproximado de 0,382 g" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (12.92, "current_timestamp”, "A”, 0, 14, "Mini Kibe Comum" ,"Produto Congelado, Peso aproximado 0,302" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (9.98, "current_timestamp”, "A”, 0, 14, "Mini Risole de Milho" ,"Produto Congelado, Peso Aproximado de 0,232 g" ,"https://s3.amazonaws.com/documentos.minastc.com.br/E6B6FBD6-82B9-" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (10.95, "current_timestamp”, "A”, 0, 14, "Linguiça Tradicional 500 g" ,"Boi Branco" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (13.5, "current_timestamp”, "A”, 0, 14, "Carne MoÍda 500 g" ,"Boi Branco" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (27.00, "current_timestamp”, "A”, 0, 14, "Maça de Peito 1 kg" ,"Boi Branco" ,"" ,1, 38, 134)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `DETAIL`, `IMAGE`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (30.00, "current_timestamp”, "A”, 0, 14, "Churrasco 1 kg" ,"Boi Branco" ,"" ,1, 38, 134)');
        
        
    


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('CREATE TABLE GEN_STRUCTURE_TYPE (NRORG BIGINT DEFAULT NULL, NAME VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ID BIGINT AUTO_INCREMENT NOT NULL, STATUS VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD MAXPERSONS INT DEFAULT NULL, ADD MAXAREA INT DEFAULT NULL, ADD CHECKAPROVE VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, ADD RULES VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD IMAGE VARCHAR(2000) DEFAULT NULL COLLATE utf8_unicode_ci, ADD GEN_STRUCTURE_TYPE_ID BIGINT DEFAULT NULL');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD CONSTRAINT FK_STRUCTURETYPEID FOREIGN KEY (GEN_STRUCTURE_TYPE_ID) REFERENCES GEN_STRUCTURE_TYPE (ID)');
        // $this->addSql('CREATE INDEX FK_STRUCTURETYPEID ON GEN_STRUCTURE (GEN_STRUCTURE_TYPE_ID)');
        // $this->addSql('ALTER TABLE ORD_MENU_PRODUCT ADD FEATURED TINYINT(1) DEFAULT NULL');
        //$this->addSql('ALTER TABLE ORD_ORDER DROP DELIVERY_FEE');
        // $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD EXTERNAL_ID VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci');
        //$this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE SER_SERVICE ADD USER_SCHEDULING_LIMIT INT DEFAULT NULL, ADD DEPEDENT_SCHEDULING_LIMIT INT DEFAULT NULL');
    }
}
