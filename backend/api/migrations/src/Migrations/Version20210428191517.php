<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428191517 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_API_KEY = "d351df24fc58b444166b43fd2056b32dae42a621" WHERE ID = 1567 AND NRORG = "3114"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "97141" WHERE ID = 1567 AND NRORG = "3189"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_API_KEY = "d351df24fc58b444166b43fd2056b32dae42a621" WHERE ID = 1568 AND NRORG = "3114"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "97141" WHERE ID = 1568 AND NRORG = "3189"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_API_KEY = "3dc093d92731335d5dd91a43a04a0b084871e679" WHERE ID = 1569 AND NRORG = "3114"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "113057" WHERE ID = 1569 AND NRORG = "3114"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_API_KEY = "2824d2730edfe28aa261cfb2be8fc41c196c914b" WHERE ID = 1566 AND NRORG = "3114"');
        //$this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "112740" WHERE ID = 1566 AND NRORG = "3114"');
        //$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_API_KEY = "d351df24fc58b444166b43fd2056b32dae42a621" WHERE ID = 51 AND NRORG = "3114"');
        //$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_SHOP_ID = "97141" WHERE ID = 51 AND NRORG = "3114"');
        //$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_USER = "riccoburger.gerenciaasasul@gmail.com" WHERE ID = 51 AND NRORG = "3114"');
        //$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_PASSWORD = "Memoravel20" WHERE ID = 51 AND NRORG = "3114"');

    }

    public function down(Schema $schema) : void
    {
      
    }
}
