<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905160440 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_DATA_UPDATE ADD GEN_DOCUMENT_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_DATA_UPDATE ADD CONSTRAINT FK_9B55A8835741ABA9 FOREIGN KEY (GEN_DOCUMENT_ID) REFERENCES GEN_DOCUMENT (ID)');
        $this->addSql('CREATE INDEX IDX_9B55A8835741ABA9 ON GEN_DATA_UPDATE (GEN_DOCUMENT_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_DATA_UPDATE DROP FOREIGN KEY FK_9B55A8835741ABA9');
        $this->addSql('DROP INDEX IDX_9B55A8835741ABA9 ON GEN_DATA_UPDATE');
        $this->addSql('ALTER TABLE GEN_DATA_UPDATE DROP GEN_DOCUMENT_ID');
    }
}
