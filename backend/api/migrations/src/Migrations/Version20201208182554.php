<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208182554 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
                //$this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média) - 284g de Feijão em embalagem de 500ml", 9.5, 2014, 1)');
        
        /*$this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2013, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2013, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2014, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2014, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2015, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2015, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2016, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2016, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2017, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2017, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2018, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2018, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2019, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2019, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2020, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2020, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2021, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2021, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2022, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2022, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2024, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2024, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2025, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2025, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2026, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2026, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2027, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2027, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2028, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2028, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2029, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2029, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2030, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2030, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2031, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2031, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2032, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2032, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2033, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2033, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2034, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2034, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2035, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2035, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2036, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2036, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção grande) - 615g de Arroz em embalagem de 1 litro", 19, 2037, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz  Branco (porção média) - 315g de Arroz em embalagem de 500 ml", 11, 2037, 1)');
        
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2299, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2299, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2300, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2300, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2302, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2302, 1)');
        
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2303, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2303, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2304, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2304, 1)');
        
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Feijão (porção média)", 10, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Arroz Branco (porção média)", 11, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção média)", 10, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Farofa (porção pequena)", 8, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Torrada ao Alho e Manteiga (porção média)", 15, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Cebolete (porção média)", 6, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho de Cebolete (porção pequena)", 5, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção média)", 9, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Vinagrete (porção pequena)", 7, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção média)", 16, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Barbecue (porção pequena)", 10, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção média)", 17, 2305, 1)');
        $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`,`ACTIVE`) VALUES ("Molho Tártaro (porção pequena)", 16, 2305, 1)');
        
        */
        
        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
