<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200515183217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ORD_ORDER_STATUS_REL (CREATE_DATE DATETIME DEFAULT CURRENT_TIMESTAMP, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, NRORG BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_STATUS_ID BIGINT DEFAULT NULL, ORD_ORDER_ID BIGINT DEFAULT NULL, INDEX ORD_ORDER_STATUS_ORD_ORDER_FK (ORD_ORDER_ID), INDEX ORD_ORDER_STATUS_GEN_STATUS_FK (GEN_STATUS_ID), UNIQUE INDEX ORD_ORDER_STATUS_REL_PK (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL ADD CONSTRAINT FK_7634DC7B5A1AB2A1 FOREIGN KEY (GEN_STATUS_ID) REFERENCES GEN_STATUS (ID)');
        $this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL ADD CONSTRAINT FK_7634DC7B3589CB8B FOREIGN KEY (ORD_ORDER_ID) REFERENCES ORD_ORDER (ORDER_IDENTIFIER)');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ORD_ORDER_STATUS_REL');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
