<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119172110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION ADD PREFERENCE_FLOW TINYINT(1) DEFAULT 0');
        $this->addSql('ALTER TABLE PAY_PAYMENT_METHOD CHANGE X_SELLER_TOKEN X_SELLER_TOKEN VARCHAR(255) DEFAULT NULL, CHANGE X_PICPAY_TOKEN X_PICPAY_TOKEN VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION DROP PREFERENCE_FLOW');
        $this->addSql('ALTER TABLE PAY_PAYMENT_METHOD CHANGE X_SELLER_TOKEN X_SELLER_TOKEN VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE X_PICPAY_TOKEN X_PICPAY_TOKEN VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
