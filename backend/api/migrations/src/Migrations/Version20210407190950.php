<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407190950 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PET SHOP",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("ELETRINHO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("ARTIGOS PARA FESTA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("DESCATAVEIS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("FRUTAS SECAS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("DESCARTAVEIS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("IOGURTES",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MANTEIGA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("HIDROGENADOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("LIMPEZA DOMESTICA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("EMBALAGEM",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("ELETRICO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PET SHOP",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("REQUEIJOES",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("HIGIENE",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("CUIDADOS COM O BEBE",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("INFLAMAVEIS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("ACESSÓRIOS PET",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("DIVERSOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("ELETROPORTÁTEIS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("FRALDAS DESCARTÁVEL",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("GUARDANAPOS E PAPEL TOALHAS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("HIGIENE PET",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("IOGURTE E BEBIDA LÁCTEA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("LASANHA E PRATO PRONTO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("LEITE DO COCO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MAIONESE",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MANTEIGA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MARGARINA E GORDURA VEGETAL",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MILHO PIPOCA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MOLHO DIVERSOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("MULTIUSO E LIMPADOR",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("OVOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PANOS E FLANELAS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PAPEL ALUMINÍO E FILME PVC",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PAPEL HIGIÊNICO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("PILHA E BATERIA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("RACÃO CÃES",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("RACÃO GATOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("REQUEIJAO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SABAO EM BARRA E LÍQUIDO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SABONETE ÍNTIMO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SABONETES",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SACO PARA LIXO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SARDINHAS E ATUM",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("SHAMPOO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("TAÇA VINHO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("TAÇAS PARA CHAMPAGNE",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("TEMPERO E CALDO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("TIGELA",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("UTENSÍLIOS PLÁSTICOS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("VASO",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("VELA,LÂMPADAS E LANTERNAS",1575,653)');
        $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`) VALUES ("VINAGRE E AGRIN",1575,653)');
        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
      
    }
}
