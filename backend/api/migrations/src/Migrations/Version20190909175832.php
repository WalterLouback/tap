<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190909175832 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SUR_QUESTION ADD SER_SERVICE_SELLER_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE SUR_QUESTION ADD CONSTRAINT FK_3C35ED4CE2359285 FOREIGN KEY (SER_SERVICE_SELLER_ID) REFERENCES SER_SERVICE_SELLER_REL (ID)');
        $this->addSql('CREATE INDEX fk_question_service_seller ON SUR_QUESTION (SER_SERVICE_SELLER_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SUR_QUESTION DROP FOREIGN KEY FK_3C35ED4CE2359285');
        $this->addSql('DROP INDEX fk_question_service_seller ON SUR_QUESTION');
        $this->addSql('ALTER TABLE SUR_QUESTION DROP SER_SERVICE_SELLER_ID');
    }
}
