<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428191524 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('INSERT INTO GEN_ADDRESS (`CEP`, `STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE`) VALUES ("36500-000", "Avenida Beira Rio", "Centro", "UBÁ", "MG", "A", "2897", "1605", "1150", "BRASIL", "DELIVERY", "-21.11694729083381", "-42.94710189739188")');

    }

    public function down(Schema $schema) : void
    {
      
    }
}
