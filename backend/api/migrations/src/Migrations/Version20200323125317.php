<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323125317 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration utilizado para inserir organização Casa Grão no Xappa */
        //$this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(8, "Casa Grão")');
        //$this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(8, 0)');
        //$this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 8, 1)');
        //$this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 8), ("R", 8), ("N", 8)');
        //$this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 8), ("CPF", 1, 3, 8), ("FIRST_NAME", 1, 3, 8), ("LAST_NAME", 1, 3, 8), ("BILLING_ADDRESS", 1, 2, 8), ("CORRESPONDENCE_ADDRESS", 1, 1, 8)');
        //$this->addSql('INSERT INTO `GEN_USER_TYPE_REL`(`GEN_USER_ID`,`GEN_USER_TYPE_ID`,`NRORG`,`STATUS`) VALUES (2,1,8,"A"), (2,4,8,"A")');
        //$this->addSql('INSERT INTO `ORD_WORKFLOW`(`NRORG`,`INITIAL_STATUS`, `PAYMENT_MOMENT`) VALUES (8,8,9)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
