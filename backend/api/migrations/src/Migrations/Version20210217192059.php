<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210217192059 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

         //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Em preparo', 'Novo', '#7ac95b', '3251')");
         //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#7ac95b', '3251')");
         //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#7ac95b', '3251')");
         //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '3251')");


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
