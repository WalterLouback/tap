<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504210436 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,191,12,current_timestamp,current_timestamp,"A",60,14,"Suco Nat C.Grão 1 l","Suco Natural sem adição de açúcar, Casa Grão","https://s3.amazonaws.com/documentos.minastc.com.br/5E6EF217-4579-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,192,5.5,current_timestamp,current_timestamp,"A",60,14,"Coca Cola Lata Trad. 350 ml","","https://s3.amazonaws.com/documentos.minastc.com.br/0252D3E0-58B9-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,193,5.5,current_timestamp,current_timestamp,"A",60,14,"Coca Cola Lata Zero 350 ml","","https://s3.amazonaws.com/documentos.minastc.com.br/E3FDD819-F3D5-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,194,9.5,current_timestamp,current_timestamp,"A",60,14,"Coca Cola Trad. 2 l","","https://s3.amazonaws.com/documentos.minastc.com.br/9BBD8985-C7BE-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,195,9.5,current_timestamp,current_timestamp,"A",0,14,"Coca Cola Zero 2 l","","https://s3.amazonaws.com/documentos.minastc.com.br/8F2D51E4-31C5-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,199,3.5,current_timestamp,current_timestamp,"A",60,14,"Suco D.Vale Uva 290 ml ","Suco Dell Vale Uva Lata","https://s3.amazonaws.com/documentos.minastc.com.br/5D4D422B-FCDB-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,200,3.5,current_timestamp,current_timestamp,"A",60,14,"Suco D.Vale Manga 290 ml","Suco Dell Vale Manga Lata","https://s3.amazonaws.com/documentos.minastc.com.br/69459662-30E6-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,201,3.5,current_timestamp,current_timestamp,"A",60,14,"Suco D.Vale Pêssego 290 ml","Suco Dell Vale Pêssego Lata","https://s3.amazonaws.com/documentos.minastc.com.br/4C2332D1-4951-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,202,9.5,current_timestamp,current_timestamp,"A",60,14,"Suco Dell Vale Uva 1 l","","https://s3.amazonaws.com/documentos.minastc.com.br/6FD28CC8-7A89-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,203,9.5,current_timestamp,current_timestamp,"A",60,14,"Suco Dell Vale Pêssego 1 l","","https://s3.amazonaws.com/documentos.minastc.com.br/D76B8FCD-C01B-",141,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,205,7.9,current_timestamp,current_timestamp,"A",60,14,"Cerveja Heineken 330 ml","Cerveja Long Neck","https://s3.amazonaws.com/documentos.minastc.com.br/CC787E5E-00B5-",140,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,206,6.9,current_timestamp,current_timestamp,"A",60,14,"Cerveja Stella Artois 275 ml","","https://s3.amazonaws.com/documentos.minastc.com.br/BCACDD73-C412-",140,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,207,6.9,current_timestamp,current_timestamp,"A",60,14,"Cerveja Budweiser 330 ml","Cerveja Long Neck","https://s3.amazonaws.com/documentos.minastc.com.br/72A90B82-D847-",140,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,209,28.9,current_timestamp,current_timestamp,"A",60,14,"Vinho Origem Carbernet Sauvig","Vinho Origem Carbernet Sauvignon Tinto Seco 187 ml - Casa Valduga","https://s3.amazonaws.com/documentos.minastc.com.br/B8C18A98-A921-",142,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,210,69.9,current_timestamp,current_timestamp,"A",60,14,"Vinho Nero Celebration Rose","Vinho Nero Celebration Rose Brut 750 ml - Casa Valduga","https://s3.amazonaws.com/documentos.minastc.com.br/51EED30B-9594-",142,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,211,79.9,current_timestamp,current_timestamp,"A",60,14,"Espumante Arte Brut Bran","Espumante Arte Brut Bran 750 ml - Casa Valduga","https://s3.amazonaws.com/documentos.minastc.com.br/11AAA0D6-3FF2-",142,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,270,79.9,current_timestamp,current_timestamp,"A",60,14,"Vinho Origem Carbernet Sauvig","Vinho Origem Carbernet Sauvignon Tinto Seco 750 ml - Casa Valduga","https://s3.amazonaws.com/documentos.minastc.com.br/B05E8369-6F4D-",142,0,NULL)');
        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_MENU_ID`,`ORD_PRODUCT_ID`,`PRICE`,`CREATED_AT`,`MODIFIED_AT`,`STATUS`,`ESTIMATED_TIME`,`NRORG`,`NAME`,`DETAIL`,`IMAGE`,`ORD_PRODUCT_GROUP_ID`,`AMOUNT`,`ORDINATION`) VALUES (38,288,79.9,current_timestamp,current_timestamp,"A",60,14,"Espumante Arte Brut Rosé","Espumante Arte Brut Rosé 750 ml - Casa Valduga","https://s3.amazonaws.com/documentos.minastc.com.br/330D4F53-F96F-",142,0,NULL)');

        
    


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('CREATE TABLE GEN_STRUCTURE_TYPE (NRORG BIGINT DEFAULT NULL, NAME VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ID BIGINT AUTO_INCREMENT NOT NULL, STATUS VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD MAXPERSONS INT DEFAULT NULL, ADD MAXAREA INT DEFAULT NULL, ADD CHECKAPROVE VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, ADD RULES VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD IMAGE VARCHAR(2000) DEFAULT NULL COLLATE utf8_unicode_ci, ADD GEN_STRUCTURE_TYPE_ID BIGINT DEFAULT NULL');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD CONSTRAINT FK_STRUCTURETYPEID FOREIGN KEY (GEN_STRUCTURE_TYPE_ID) REFERENCES GEN_STRUCTURE_TYPE (ID)');
        // $this->addSql('CREATE INDEX FK_STRUCTURETYPEID ON GEN_STRUCTURE (GEN_STRUCTURE_TYPE_ID)');
        // $this->addSql('ALTER TABLE ORD_MENU_PRODUCT ADD FEATURED TINYINT(1) DEFAULT NULL');
        //$this->addSql('ALTER TABLE ORD_ORDER DROP DELIVERY_FEE');
        // $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD EXTERNAL_ID VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci');
        //$this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE SER_SERVICE ADD USER_SCHEDULING_LIMIT INT DEFAULT NULL, ADD DEPEDENT_SCHEDULING_LIMIT INT DEFAULT NULL');
    }
}
