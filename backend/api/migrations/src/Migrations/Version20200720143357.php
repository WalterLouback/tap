<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200720143357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'update evtEventMenu';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        // $this->addSql('UPDATE EVT_EVENT_MENU SET ORD_MENU_ID = 45 WHERE ID = 46 AND EVT_EVENT_ID = 1398');
        // $this->addSql('UPDATE EVT_EVENT_MENU SET ORD_MENU_ID = NULL WHERE ID = 70 AND EVT_EVENT_ID = 1398');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
