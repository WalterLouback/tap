<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200902111423 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        //$this->addSql('UPDATE GEN_CONFIGURATION SET APP_LAYOUT_CONFIG = \'{"homeLayout":"feed","slideNav":[{"id":"13","name":"Perfil","icon":["user"],"user":[]},{"id":"11","name":"Sobre","icon":["info-circle"],"click":[]}],"BottomNavigation":[{"id":"8","name":"In�cio","icon":["home"]},{"id":"5","name":"Comprar","icon":["utensils"]},{"id":"10","name":"Pedidos","icon":["list-ul"]},{"id":"6","name":"Carteira","icon":["wallet"]}]}\' WHERE NRORG = 1558 AND ID = 22;');


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
