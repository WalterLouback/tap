<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420152656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration para corrigir a estrutura da loja divino fogão*/
        $this->addSql('ALTER TABLE SER_SERVICE ADD USER_SCHEDULING_LIMIT INT DEFAULT NULL');
        $this->addSql('ALTER TABLE SER_SERVICE ADD DEPEDENT_SCHEDULING_LIMIT INT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD MAXPERSONS INT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD MAXAREA INT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD CHECKAPROVE varchar(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD RULES varchar(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD IMAGE varchar(2000) DEFAULT NULL');
        $this->addSql('CREATE TABLE GEN_STRUCTURE_TYPE (NRORG BIGINT DEFAULT NULL, NAME VARCHAR(100) DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, PRIMARY KEY(ID))DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD GEN_STRUCTURE_TYPE_ID bigint DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_STRUCTURE ADD CONSTRAINT FK_STRUCTURETYPEID FOREIGN KEY (GEN_STRUCTURE_TYPE_ID) REFERENCES GEN_STRUCTURE_TYPE (ID)');
        $this->addSql('ALTER TABLE GEN_STRUCTURE_TYPE ADD STATUS varchar(1) DEFAULT NULL'); 
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_PRODUCT DROP EXTERNAL_ID');
    }
}
