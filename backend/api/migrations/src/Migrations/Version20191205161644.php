<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191205161644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE PAY_WALLET_CARD (STATUS VARCHAR(255) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, SERIAL_NUMBER VARCHAR(45) DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, WALLET_ID VARCHAR(36) DEFAULT NULL, INDEX IDX_19CF6860DAA78FC (WALLET_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE PAY_WALLET_CARD ADD CONSTRAINT FK_19CF6860DAA78FC FOREIGN KEY (WALLET_ID) REFERENCES PAY_WALLET (ID)');
        $this->addSql('ALTER TABLE ORD_ORDER CHANGE ORDER_IDENTIFIER ORDER_IDENTIFIER BIGINT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE PAY_WALLET_CARD');
        $this->addSql('ALTER TABLE ORD_ORDER CHANGE ORDER_IDENTIFIER ORDER_IDENTIFIER BIGINT DEFAULT NULL');
    }
}
