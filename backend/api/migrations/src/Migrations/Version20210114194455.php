<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210114194455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('CREATE TABLE GEN_TICKET_DISCOUNT (NRORG INT NOT NULL, CUPOM_CODE VARCHAR(255) NOT NULL, TYPE INT NOT NULL, VAL_CUPOM DOUBLE PRECISION NOT NULL, VAL_MIN DOUBLE PRECISION NOT NULL, INITIAL_DATE DATETIME NOT NULL, FINAL_DATE DATETIME NOT NULL, STATUS VARCHAR(255) NOT NULL, ID INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Saladinha da casa (salada de repolho roxo, cenoura e tomate)", 1, 3463, 1, 1, "9.01.10.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Ovo na chapa", 1, 3463, 1, 2, "9.01.10.160.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cebola fritinha", 1, 3463, 1, 3, "9.01.10.150.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Azeitona", 2, 3463, 1, 4, "9.01.10.175.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Creme de pequi", 2, 3463, 1, 5, "9.01.10.170.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Requeijão cremoso", 2, 3463, 1, 6, "9.01.30.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Guariroba", 2, 3463, 0, 7, "9.01.30.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cheddar cremoso", 2, 3463, 1, 8, "9.01.10.050.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Bacon", 3, 3463, 1, 9, "9.01.10.130.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Muçarela", 3, 3463, 1, 10, "9.01.30.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Saladinha da casa (salada de repolho roxo, cenoura e tomate)", 1, 3464, 1, 1, "9.01.10.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Ovo na chapa", 1, 3464, 1, 2, "9.01.10.160.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cebola fritinha", 1, 3464, 1, 3, "9.01.10.150.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Azeitona", 2, 3464, 1, 4, "9.01.10.175.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Creme de pequi", 2, 3464, 1, 5, "9.01.10.170.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Requeijão cremoso", 2, 3464, 1, 6, "9.01.30.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Guariroba", 2, 3464, 0, 7, "9.01.30.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cheddar cremoso", 2, 3464, 1, 8, "9.01.10.050.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Bacon", 3, 3464, 1, 9, "9.01.10.130.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Muçarela", 3, 3464, 1, 10, "9.01.30.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Saladinha da casa (salada de repolho roxo, cenoura e tomate)", 1, 3465, 1, 1, "9.01.10.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Ovo na chapa", 1, 3465, 1, 2, "9.01.10.160.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cebola fritinha", 1, 3465, 1, 3, "9.01.10.150.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Azeitona", 2, 3465, 1, 4, "9.01.10.175.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Creme de pequi", 2, 3465, 1, 5, "9.01.10.170.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Requeijão cremoso", 2, 3465, 1, 6, "9.01.30.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Guariroba", 2, 3465, 0, 7, "9.01.30.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Cheddar cremoso", 2, 3465, 1, 8, "9.01.10.050.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Bacon", 3, 3465, 1, 9, "9.01.10.130.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Muçarela", 3, 3465, 1, 10, "9.01.30.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 0, 3385, 1, 1, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 0, 3385, 1, 2, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açucar - Lata", 0, 3385, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 0, 3385, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 0, 3385, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 0, 3385, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 0, 3385, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3461, 1, 1, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3461, 1, 2, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3461, 1, 3, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3461, 1, 4, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Kuat Guaraná - Lata", 5.99, 3461, 1, 5, "9.16.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus", 5.99, 3461, 1, 6, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3461, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite Lemon Fresh", 5.99, 3461, 1, 8, "9.30.01.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 3.99, 3461, 1, 10, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3461, 1, 9, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Colombina garrafa session IPA 355 ml", 9.99, 3461, 1, 12, "9.35.02.007.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Colombina garrafa pilsen puro malte 355 ml", 9.99, 3461, 1, 11, "9.35.02.006.00")');
        
        
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3468, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3468, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3468, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3468, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3468, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3468, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3468, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3468, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3468, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3468, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3468, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3469, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3469, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3469, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3469, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3472, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3472, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3472, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3472, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3472, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3472, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3472, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3472, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3472, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3472, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3472, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3473, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3473, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3473, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3473, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3476, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3476, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3476, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3476, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3476, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3476, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3476, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3476, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3476, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3476, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3476, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3477, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3477, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3477, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3477, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3480, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3480, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3480, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3480, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3480, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3480, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3480, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3480, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3480, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3480, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3480, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3481, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3481, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3481, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3481, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3484, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3484, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3484, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3484, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3484, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3484, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3484, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3484, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3484, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3484, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3484, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3485, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3485, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3485, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3485, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3488, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3488, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3488, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3488, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3488, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3488, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3488, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3488, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3488, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3488, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3488, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3489, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3489, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3489, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3489, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3492, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3492, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3492, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3492, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3492, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3492, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3492, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3492, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3492, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3492, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3492, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3493, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3493, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3493, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3493, 1, 4, "9.05.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água sem gás - 500 ml", 3.99, 3496, 1, 1, "9.30.01.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola - Lata", 5.99, 3496, 1, 3, "9.16.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Coca Cola sem açúcar - Lata", 5.99, 3496, 1, 4, "9.16.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Laranja - Lata", 5.99, 3496, 1, 5, "9.16.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Fanta Guaraná - Lata", 5.99, 3496, 1, 6, "9.16.05.040.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Sprite - Lata", 5.99, 3496, 1, 7, "9.16.05.025.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Schweppes Citrus - Lata", 5.99, 3496, 1, 8, "9.16.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Laranja Prats - 330 ml", 6.99, 3496, 1, 9, "9.20.05.010.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Limão Prats - 330 ml", 6.99, 3496, 1, 10, "9.20.05.020.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Suco de Uva Prats - 330 ml", 6.99, 3496, 1, 11, "9.20.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("Água com gás - 500 ml", 4.99, 3496, 1, 2, "9.30.01.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis banana com canela", 5.99, 3497, 1, 1, "9.05.05.001.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis churros", 5.99, 3497, 1, 2, "9.05.05.015.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis brigadeiro", 5.99, 3497, 1, 3, "9.05.05.005.00")');
        // $this->addSql('INSERT INTO `ORD_OPTION` (`NAME`, `PRICE`, `ORD_EXTRA_ID`, `ACTIVE`, `ORDINATION`, `INTEGRATION_CODE`) VALUES ("03 mini pastéis Romeu e Julieta", 5.99, 3497, 1, 4, "9.05.05.025.00")');

    }

    public function down(Schema $schema) : void
    {
    }
}
