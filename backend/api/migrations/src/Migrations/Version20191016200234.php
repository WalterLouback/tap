<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191016200234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE GEN_USER_TAG_REL (STATUS VARCHAR(15) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_USER_ID BIGINT DEFAULT NULL, GEN_TAG_ID BIGINT DEFAULT NULL, INDEX IDX_7852D1BB4B3BFC8A (GEN_USER_ID), INDEX IDX_7852D1BB21C638B2 (GEN_TAG_ID), UNIQUE INDEX GEN_USER_TAG_REL_PK (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE GEN_USER_CATEGORY_REL (STATUS VARCHAR(15) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_USER_ID BIGINT DEFAULT NULL, GEN_CATEGORY_ID BIGINT DEFAULT NULL, INDEX IDX_A83212324B3BFC8A (GEN_USER_ID), INDEX IDX_A832123286384E7C (GEN_CATEGORY_ID), UNIQUE INDEX GEN_USER_CATEGORY_REL_PK (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE GEN_USER_TAG_REL ADD CONSTRAINT FK_7852D1BB4B3BFC8A FOREIGN KEY (GEN_USER_ID) REFERENCES GEN_USER (ID)');
        $this->addSql('ALTER TABLE GEN_USER_TAG_REL ADD CONSTRAINT FK_7852D1BB21C638B2 FOREIGN KEY (GEN_TAG_ID) REFERENCES GEN_TAG (ID)');
        $this->addSql('ALTER TABLE GEN_USER_CATEGORY_REL ADD CONSTRAINT FK_A83212324B3BFC8A FOREIGN KEY (GEN_USER_ID) REFERENCES GEN_USER (ID)');
        $this->addSql('ALTER TABLE GEN_USER_CATEGORY_REL ADD CONSTRAINT FK_A832123286384E7C FOREIGN KEY (GEN_CATEGORY_ID) REFERENCES GEN_CATEGORY (ID)');

        $this->addSql('ALTER TABLE SER_SERVICE CHANGE TYPE TYPE VARCHAR(45) DEFAULT NULL');

        $this->addSql('ALTER TABLE GEN_USER ADD PREFERENCES INT DEFAULT NULL');

        $this->addSql('ALTER TABLE EVT_EVENT ADD CODE_EVENT VARCHAR(100) DEFAULT NULL, ADD NUMBER_SEQ_EVENT INT DEFAULT NULL');
        $this->addSql('ALTER TABLE EVT_EVENT_TICKET ADD CODE_TYPE_TICKET VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE GEN_USER_TAG_REL');
        $this->addSql('DROP TABLE GEN_USER_CATEGORY_REL');

        $this->addSql('ALTER TABLE SER_SERVICE CHANGE TYPE TYPE VARCHAR(10) DEFAULT NULL COLLATE utf8_unicode_ci');

        $this->addSql('ALTER TABLE GEN_USER DROP PREFERENCES');

        $this->addSql('ALTER TABLE EVT_EVENT DROP CODE_EVENT, DROP NUMBER_SEQ_EVENT');
        $this->addSql('ALTER TABLE EVT_EVENT_TICKET DROP CODE_TYPE_TICKET');
    }
}
