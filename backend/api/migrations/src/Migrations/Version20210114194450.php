<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210114194450 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('CREATE TABLE GEN_TICKET_DISCOUNT (NRORG INT NOT NULL, CUPOM_CODE VARCHAR(255) NOT NULL, TYPE INT NOT NULL, VAL_CUPOM DOUBLE PRECISION NOT NULL, VAL_MIN DOUBLE PRECISION NOT NULL, INITIAL_DATE DATETIME NOT NULL, FINAL_DATE DATETIME NOT NULL, STATUS VARCHAR(255) NOT NULL, ID INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        
    }

    public function down(Schema $schema) : void
    {
    }
}
