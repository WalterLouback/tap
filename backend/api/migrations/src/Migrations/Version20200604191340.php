<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200604191340 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Tabela para relacionar o usuario a organizacao colocando por enquando o firebaseToken';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE GEN_ORGANIZATION_USER_REL (NRORG BIGINT DEFAULT NULL, STATUS VARCHAR(10) DEFAULT NULL, FIREBASE_TOKEN VARCHAR(152) DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_USER_ID BIGINT DEFAULT NULL, INDEX GEN_ORGANIZATION_USER_REL_FK (GEN_USER_ID), UNIQUE INDEX GEN_ORGANIZATION_USER_REL_PK (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE GEN_ORGANIZATION_USER_REL ADD CONSTRAINT FK_3A231BE84B3BFC8A FOREIGN KEY (GEN_USER_ID) REFERENCES GEN_USER (ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE GEN_ORGANIZATION_USER_REL');
    }
}
