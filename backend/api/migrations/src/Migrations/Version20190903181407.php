<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190903181407 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD SER_SERVICE_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD CONSTRAINT FK_D9B4A0392C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('CREATE INDEX ORDER_PRODUCT_SER_SERVICE_FK ON ORD_ORDER_PRODUCT (SER_SERVICE_ID)');
        $this->addSql('ALTER TABLE ORD_ORDER DROP FOREIGN KEY FK_79E261BE2C71C207');
        $this->addSql('DROP INDEX SER_SERVICE_FK_idx ON ORD_ORDER');
        $this->addSql('ALTER TABLE ORD_ORDER ADD ORDER_SHEET VARCHAR(100) DEFAULT NULL, DROP SER_SERVICE_ID');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER ADD SER_SERVICE_ID BIGINT DEFAULT NULL, DROP ORDER_SHEET');
        $this->addSql('ALTER TABLE ORD_ORDER ADD CONSTRAINT FK_79E261BE2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_FK_idx ON ORD_ORDER (SER_SERVICE_ID)');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT DROP FOREIGN KEY FK_D9B4A0392C71C207');
        $this->addSql('DROP INDEX ORDER_PRODUCT_SER_SERVICE_FK ON ORD_ORDER_PRODUCT');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT DROP SER_SERVICE_ID');
    }
}
