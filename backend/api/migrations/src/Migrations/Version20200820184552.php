<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820184552 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '20', PAYMENT_MOMENT = '20' WHERE ID = '8' and  nrorg = 8");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = NULL WHERE ID = '22' and  nrorg = 8");
        //$this->addSql("UPDATE ORD_WORKFLOW SET NRORG = NULL WHERE ID = '9' and  nrorg = 8");
        
    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
