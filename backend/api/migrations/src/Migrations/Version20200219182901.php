<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219182901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('UPDATE GEN_CONFIGURATION SET APP_LAYOUT_CONFIG = \'{"homeLayout":"classic","slideNav":[{"id":"0","name":"Perfil","icon":["user"],"child":[]},{"id":"1","name":"Dependentes","icon":["users"],"child":[]},{"id":"2","name":"Carteira","icon":["wallet"],"child":[]},{"id":"3","name":"Documentos","icon":["file"],"child":[]},{"id":"4","name":"Fale com o RH","icon":["volume"],"child":[]},{"id":"6","name":"Pesquisa de Satisfação","icon":["clipboard"],"child":[]},{"id":"7","name":"Mais Informações","icon":["plus-square"],"child":[{"id":"10","name":"Trocar de Empresa","icon":["sync"]},{"id":"11","name":"Sobre","icon":["info-circle"],"click":[]}]},{"id":"8","name":"Sair","icon":["sign-out-alt"],"child":[]}],"BottomNavigation":[{"id":"0","name":"Início","icon":["home"]},{"id":"1","name":"Compras","icon":["store"]},{"id":"3","name":"Serviços","icon":["sparkles"]},{"id":"4","name":"Agenda","icon":["calendar-alt"]}]}\' WHERE NRORG = 1 AND ID > 0;');
        // $this->addSql('UPDATE GEN_ORGANIZATION SET NAME = \'Teknisa\' WHERE NRORG = 1 AND ID > 0;');
        // Foi utilizado somente para aplicat update no bipfun, não faz parte do migration
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
