<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201125171126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE GEN_TAA_CONFIGURATION (NRORG BIGINT DEFAULT NULL, NAME VARCHAR(45) DEFAULT NULL, MODALITY VARCHAR(1) DEFAULT NULL, POS_SERIAL_NUMBER VARCHAR(45) DEFAULT NULL, POS_REFERENCE_ID VARCHAR(45) DEFAULT NULL, EXTERNAL_API_URL VARCHAR(100) DEFAULT NULL, EXTERNAL_API_ID VARCHAR(45) DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, STRUCTURE_ID BIGINT DEFAULT NULL, STORE_ID BIGINT DEFAULT NULL, INDEX IDX_4E188EE6884B0F7B (STRUCTURE_ID), INDEX IDX_4E188EE64EEC72BD (STORE_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE GEN_TAA_CONFIGURATION ADD CONSTRAINT FK_4E188EE6884B0F7B FOREIGN KEY (STRUCTURE_ID) REFERENCES GEN_STRUCTURE (ID)');
        $this->addSql('ALTER TABLE GEN_TAA_CONFIGURATION ADD CONSTRAINT FK_4E188EE64EEC72BD FOREIGN KEY (STORE_ID) REFERENCES EVT_EVENT (ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
