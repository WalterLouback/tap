<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191209153824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("segunda", "0", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("terca", "1", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("domingo", "6", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("quarta", "2", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("quinta", "3", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("sexta", "4", "A", "1")');
        $this->addSql('INSERT INTO SER_DAY_TIMESHEET (`NAME`, `VALUE`,`STATUS`,`NRORG`) VALUES ("sabado", "5", "A", "1")');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
