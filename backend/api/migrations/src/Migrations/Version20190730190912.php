<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190730190912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_NOTIFICATION ADD URL_IMAGE VARCHAR(255) DEFAULT NULL, ADD CATEGORY INT DEFAULT NULL, ADD DATE_OF_COMMITMENT DATETIME DEFAULT NULL, ADD CREATED_AT DATETIME DEFAULT NULL, ADD MODIFIED_AT DATETIME DEFAULT NULL, ADD CREATED_BY BIGINT DEFAULT NULL, ADD MODIFIED_BY BIGINT DEFAULT NULL, CHANGE MESSAGE MESSAGE TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_NOTIFICATION DROP URL_IMAGE, DROP CATEGORY, DROP DATE_OF_COMMITMENT, DROP CREATED_AT, DROP MODIFIED_AT, DROP CREATED_BY, DROP MODIFIED_BY, CHANGE MESSAGE MESSAGE VARCHAR(1500) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
