<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826202908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE SER_TIMESHEET (INITIAL_TIME VARCHAR(255) DEFAULT NULL, FINAL_TIME VARCHAR(255) DEFAULT NULL, STATUS VARCHAR(10) DEFAULT NULL, TYPES VARCHAR(10) DEFAULT NULL, DAY INT DEFAULT NULL, NRORG BIGINT DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, SER_SERVICE_ID BIGINT DEFAULT NULL, SER_SERVICE_SELLER_REL_ID BIGINT DEFAULT NULL, INDEX SER_TIMESHEET__SERVICE_SELLER_REL_FK (SER_SERVICE_SELLER_REL_ID), INDEX SER_TIMESHEET__SERVICE_FK (SER_SERVICE_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE SER_TIMESHEET ADD CONSTRAINT FK_AE5686682C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('ALTER TABLE SER_TIMESHEET ADD CONSTRAINT FK_AE5686685B38904C FOREIGN KEY (SER_SERVICE_SELLER_REL_ID) REFERENCES SER_SERVICE_SELLER_REL (ID)');
        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD SER_TIMESHEET_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D761BE9060 FOREIGN KEY (SER_TIMESHEET_ID) REFERENCES SER_TIMESHEET (ID)');
        $this->addSql('CREATE INDEX SER_TIMESHEET_IDX ON GEN_SCHEDULE (SER_TIMESHEET_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D761BE9060');
        $this->addSql('DROP TABLE SER_TIMESHEET');
        $this->addSql('DROP INDEX SER_TIMESHEET_IDX ON GEN_SCHEDULE');
        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP SER_TIMESHEET_ID');
    }
}
