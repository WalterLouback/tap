<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210217192056 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE GEN_STATUS SET NEXT = 10 WHERE ID = 10");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 10 WHERE ID = 19");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 12 WHERE ID = 11");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 13 WHERE ID = 12");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 14 WHERE ID = 14");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 15 WHERE ID = 13");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 16 WHERE ID = 34");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 17 WHERE ID = 16");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 18 WHERE ID = 17");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 21 WHERE ID = 22");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 21 WHERE ID = 49");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 22 WHERE ID = 239");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 32 WHERE ID = 36");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 38 WHERE ID = 37");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 39 WHERE ID = 38");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 49 WHERE ID = 50");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 53 WHERE ID = 54");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 54 WHERE ID = 55");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 55 WHERE ID = 56");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 58 WHERE ID = 57");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 59 WHERE ID = 58");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 60 WHERE ID = 59");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 62 WHERE ID = 61");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 63 WHERE ID = 62");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 64 WHERE ID = 63");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 147 WHERE ID = 146");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 148 WHERE ID = 147");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 149 WHERE ID = 148");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 151 WHERE ID = 150");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 152 WHERE ID = 151");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 153 WHERE ID = 152");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 155 WHERE ID = 154");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 156 WHERE ID = 155");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 157 WHERE ID = 156");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 159 WHERE ID = 158");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 160 WHERE ID = 159");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 161 WHERE ID = 160");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 163 WHERE ID = 162");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 164 WHERE ID = 163");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 165 WHERE ID = 164");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 167 WHERE ID = 166");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 168 WHERE ID = 167");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 169 WHERE ID = 168");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 171 WHERE ID = 170");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 172 WHERE ID = 171");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 173 WHERE ID = 172");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 175 WHERE ID = 174");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 176 WHERE ID = 175");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 177 WHERE ID = 176");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 179 WHERE ID = 178");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 180 WHERE ID = 179");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 181 WHERE ID = 180");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 183 WHERE ID = 182");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 184 WHERE ID = 183");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 185 WHERE ID = 184");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 187 WHERE ID = 186");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 188 WHERE ID = 187");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 189 WHERE ID = 188");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 191 WHERE ID = 190");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 192 WHERE ID = 191");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 193 WHERE ID = 192");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 195 WHERE ID = 194");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 196 WHERE ID = 195");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 197 WHERE ID = 196");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 199 WHERE ID = 198");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 200 WHERE ID = 199");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 201 WHERE ID = 200");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 203 WHERE ID = 202");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 204 WHERE ID = 203");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 205 WHERE ID = 204");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 207 WHERE ID = 206");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 208 WHERE ID = 207");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 209 WHERE ID = 208");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 211 WHERE ID = 210");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 212 WHERE ID = 211");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 213 WHERE ID = 212");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 215 WHERE ID = 214");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 216 WHERE ID = 215");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 217 WHERE ID = 216");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 219 WHERE ID = 218");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 220 WHERE ID = 219");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 221 WHERE ID = 220");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 223 WHERE ID = 222");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 224 WHERE ID = 223");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 225 WHERE ID = 224");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 227 WHERE ID = 226");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 228 WHERE ID = 227");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 229 WHERE ID = 228");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 231 WHERE ID = 230");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 232 WHERE ID = 231");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 233 WHERE ID = 232");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 235 WHERE ID = 234");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 236 WHERE ID = 235");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 237 WHERE ID = 236");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 239 WHERE ID = 20");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 239 WHERE ID = 23");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 240 WHERE ID = 8");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 240 WHERE ID = 52");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 240 WHERE ID = 238");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 241 WHERE ID = 240");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 242 WHERE ID = 244");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 244 WHERE ID = 241");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 245 WHERE ID = 35");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 246 WHERE ID = 245");
        $this->addSql("UPDATE GEN_STATUS SET NEXT = 247 WHERE ID = 246");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
