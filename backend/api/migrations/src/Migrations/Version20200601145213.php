<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200601145213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'adicionei fields TRANSACTION_ID e TYPE na tabela ORD_ORDER_STATUS_REL para facilitar o estorno de uma order';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL ADD TRANSACTION_ID VARCHAR(255) DEFAULT NULL, ADD TYPE VARCHAR(20) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL DROP TRANSACTION_ID, DROP TYPE');
    }
}
