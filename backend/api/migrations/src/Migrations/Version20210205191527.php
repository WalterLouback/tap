<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210205191527 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('UPDATE GEN_USER SET POS_SERIAL_NUMBER = "6N015403" WHERE ID = "1496"');
        // $this->addSql('UPDATE GEN_USER SET POS_SERIAL_NUMBER = "6G942552" WHERE ID = "1107"');



    }

    public function down(Schema $schema) : void
    {
       
    }
}
