<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407190954 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        //$this->addSql('INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ("Aceitar", "Novo Pedido", "#7ac95b", "1869")');
        //$this->addSql('INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ("Pronto para Entrega", "Em Separação", "#7ac95b", "1869")');
        //$this->addSql('INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ("Sair para Entrega", "Separado", "#7ac95b", "1869")');
        //$this->addSql('INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ("Finalizar", "Em Entrega", "#7ac95b", "1869")');
        //$this->addSql('INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, "Fechado", "#7ac95b", "1869")');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
      
    }
}
