<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190918162207 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIME_TIMESHEET DROP FOREIGN KEY FK_B5AF9EA148307711');
        $this->addSql('DROP INDEX SER_DAY_TIMESHEET_IDX ON SER_TIME_TIMESHEET');
        $this->addSql('ALTER TABLE SER_TIME_TIMESHEET ADD SER_DAY_TIMESHEET_VALUE VARCHAR(45) DEFAULT NULL, DROP SER_DAY_TIMESHEET_ID');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIME_TIMESHEET ADD SER_DAY_TIMESHEET_ID BIGINT DEFAULT NULL, DROP SER_DAY_TIMESHEET_VALUE');
        $this->addSql('ALTER TABLE SER_TIME_TIMESHEET ADD CONSTRAINT FK_B5AF9EA148307711 FOREIGN KEY (SER_DAY_TIMESHEET_ID) REFERENCES SER_DAY_TIMESHEET (ID)');
        $this->addSql('CREATE INDEX SER_DAY_TIMESHEET_IDX ON SER_TIME_TIMESHEET (SER_DAY_TIMESHEET_ID)');
    }
}
