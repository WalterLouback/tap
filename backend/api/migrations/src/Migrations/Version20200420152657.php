<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420152657 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration para atualizar taxas de frete */
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET DISTANCE_KM = '0.000 ate 0.500' WHERE ID = 1");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET PRICE = '0.00' WHERE ID = 1");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET DISTANCE_KM = '0.501 ate 1.000' WHERE ID = 2");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET PRICE = '5.00' WHERE ID = 2");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET DISTANCE_KM = '1.001 ate 3.000' WHERE ID = 3");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET PRICE = '8.00' WHERE ID = 3");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET DISTANCE_KM = '3.001 ate 6.000' WHERE ID = 4");
        // $this->addSql("UPDATE EVT_EVENT_DELIVER SET PRICE = '10.00' WHERE ID = 4");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_PRODUCT DROP EXTERNAL_ID');
    }
}
