<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201023133432 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add field requestLog at the table ordOrder';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 317 WHERE ID = 856');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 326 WHERE ID = 857');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 327 WHERE ID = 858');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 328 WHERE ID = 859');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 329 WHERE ID = 861');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 330 WHERE ID = 862');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 331 WHERE ID = 863');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 332 WHERE ID = 864');
           $this->addSql('UPDATE GEN_STRUCTURE SET NAME = 333 WHERE ID = 865');
           
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE ORD_ORDER DROP REQUEST_LOG');
    }
}
