<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323103138 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

       #$this->addSql("INSERT INTO GEN_ADDRESS (`STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE` ) VALUES ('SHCN CL 316 BL d', 'ASA NORTE', 'BRASILIA', 'DF', 'A', '3189', '1556', '3', 'BRASIL', 'DELIVERY', '15.737818156320088','-47.897198474307494')");





    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
