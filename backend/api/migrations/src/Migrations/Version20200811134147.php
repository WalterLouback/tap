<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811134147 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1038')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1038')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1038')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1038')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1148')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1148')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1148')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1148')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1157')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1157')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1157')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1157')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1366')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1378')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1378')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1378')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1378')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1380')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1380')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1380')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1380')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1490')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1490')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1490')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1490')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2028')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2028')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2028')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2028')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1366')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1366')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2105')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2105')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2105')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2105')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2442')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2442')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2442')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2442')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2476')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2476')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2476')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2476')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2486')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2486')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2486')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2486')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2560')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2560')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2560')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2560')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2562')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2562')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2562')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2562')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2624')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2624')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2624')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2624')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2658')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2658')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2658')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2658')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2751')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2751')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2751')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2751')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2818')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2818')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2818')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2818')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2861')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2861')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2861')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2861')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2960')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2960')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2960')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2960')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2980')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2080')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2980')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2980')");
        
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2994')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2994')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2994')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2994')");
        
        

    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
