<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202153355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'inserir s/n nos genUsers 1472 e 1473';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE GEN_USER SET POS_SERIAL_NUMBER = "6M548907" WHERE ID = "1472"');
        $this->addSql('UPDATE GEN_USER SET POS_SERIAL_NUMBER = "6C197133" WHERE ID = "1473"');

    }

    public function down(Schema $schema) : void
    {
    }
}
