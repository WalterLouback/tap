<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200709170551 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'alteracoes referente ao split payment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_SPLIT_RULE_RECEIPIENT ADD REMAINDER_VALUE NUMERIC(9, 2) DEFAULT NULL, ADD PROCESSING_FEE NUMERIC(9, 2) DEFAULT NULL, ADD TYPE NUMERIC(9, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_SPLIT_RULE ADD NAME VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE PAY_GATEWAY ADD ORD_SPLIT_RULE_ID BIGINT DEFAULT NULL, CHANGE ZERO_AUTH ZERO_AUTH VARCHAR(2) DEFAULT NULL, CHANGE CONSULTA_BIN CONSULTA_BIN VARCHAR(2) DEFAULT NULL');
        $this->addSql('ALTER TABLE PAY_GATEWAY ADD CONSTRAINT FK_8661EA0A579E18DA FOREIGN KEY (ORD_SPLIT_RULE_ID) REFERENCES ORD_SPLIT_RULE (ID)');
        $this->addSql('CREATE INDEX IDX_8661EA0A579E18DA ON PAY_GATEWAY (ORD_SPLIT_RULE_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
