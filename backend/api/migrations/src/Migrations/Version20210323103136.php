<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323103136 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        #$this->addSql('UPDATE EVT_EVENT SET LOGGI_API_KEY = "eeff96155a2cad6302252eaf636b8dc01813414f" WHERE ID = 1556 AND NRORG = "3189"');
        #$this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "64809" WHERE ID = 1556 AND NRORG = "3189"');
        #$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_API_KEY = "eeff96155a2cad6302252eaf636b8dc01813414f" WHERE ID = 71 AND NRORG = "3189"');
        #$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_SHOP_ID = "7053" WHERE ID = 71 AND NRORG = "3189"');
        #$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_USER = "costacurta@outlook.com" WHERE ID = 71 AND NRORG = "3189"');
        #$this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_PASSWORD = "Mensagi00" WHERE ID = 71 AND NRORG = "3189"');





    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
