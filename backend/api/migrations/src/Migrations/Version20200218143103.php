<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218143103 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION ADD APP_LAYOUT_CONFIG TEXT DEFAULT NULL, DROP HOME_LAYOUT');
        $this->addSql('ALTER TABLE SER_SERVICE_SELLER_REL ADD FINAL_TIME VARCHAR(255) DEFAULT NULL, ADD INITIAL_TIME VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION ADD HOME_LAYOUT TEXT DEFAULT NULL COLLATE utf8_unicode_ci, DROP APP_LAYOUT_CONFIG');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE SER_SERVICE_SELLER_REL DROP FINAL_TIME, DROP INITIAL_TIME');
    }
}
