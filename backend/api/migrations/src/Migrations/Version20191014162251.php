<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191014162251 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE PAY_WALLET ADD EVT_EVENT_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE PAY_WALLET ADD CONSTRAINT FK_31D17CF3B4A15433 FOREIGN KEY (EVT_EVENT_ID) REFERENCES EVT_EVENT (ID)');
        $this->addSql('CREATE INDEX IDX_31D17CF3B4A15433 ON PAY_WALLET (EVT_EVENT_ID)');
        $this->addSql('ALTER TABLE ORD_OFFER_PRODUCT ADD PRICE NUMERIC(10, 0) NOT NULL, ADD ORD_OFFER_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_OFFER_PRODUCT ADD CONSTRAINT FK_8EC6CF7BEBD0D25D FOREIGN KEY (ORD_OFFER_ID) REFERENCES ORD_MENU_PRODUCT (ID)');
        $this->addSql('CREATE INDEX IDX_8EC6CF7BEBD0D25D ON ORD_OFFER_PRODUCT (ORD_OFFER_ID)');
        
        $this->addSql('ALTER TABLE PAY_PAYMENT_METHOD ADD X_SELLER_TOKEN VARCHAR(255) DEFAULT NULL, ADD X_PICPAY_TOKEN VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_USER ADD BIRTH_DATE DATETIME DEFAULT NULL');
        
        $this->addSql('ALTER TABLE ORD_ORDER ADD CARD_HOLDER_NAME VARCHAR(100) DEFAULT NULL');

        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD ITEM_IDENTIFIER INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_OFFER_PRODUCT DROP FOREIGN KEY FK_8EC6CF7BEBD0D25D');
        $this->addSql('DROP INDEX IDX_8EC6CF7BEBD0D25D ON ORD_OFFER_PRODUCT');
        $this->addSql('ALTER TABLE ORD_OFFER_PRODUCT DROP PRICE, DROP ORD_OFFER_ID');
        $this->addSql('ALTER TABLE PAY_WALLET DROP FOREIGN KEY FK_31D17CF3B4A15433');
        $this->addSql('DROP INDEX IDX_31D17CF3B4A15433 ON PAY_WALLET');
        $this->addSql('ALTER TABLE PAY_WALLET DROP EVT_EVENT_ID');

        $this->addSql('ALTER TABLE PAY_PAYMENT_METHOD DROP X_SELLER_TOKEN, DROP X_PICPAY_TOKEN');

        $this->addSql('ALTER TABLE GEN_USER DROP BIRTH_DATE');

        $this->addSql('ALTER TABLE ORD_ORDER DROP CARD_HOLDER_NAME');

        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT DROP ITEM_IDENTIFIER');
    }
}
