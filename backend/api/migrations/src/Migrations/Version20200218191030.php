<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218191030 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD SER_SERVICE_ID BIGINT DEFAULT NULL');
        // $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402E2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL (SER_SERVICE_ID)');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402E2C71C207');
        $this->addSql('DROP INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP SER_SERVICE_ID');
    }
}
