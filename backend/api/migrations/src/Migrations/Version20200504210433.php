<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504210433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Achocolatados e Biscoitos", 1389, 38, "current_timestamp", "current_timestamp", 512, 133)');
        // $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Massas", 1389, 38, "current_timestamp", "current_timestamp", 513, 133)');
        // $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Mantimenos", 1389, 38, "current_timestamp", "current_timestamp", 514, 133)');
        // $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Congelados", 1389, 38, "current_timestamp", "current_timestamp", 515, 133)');
        // $this->addSql('INSERT INTO `ORD_PRODUCT_GROUP` (`NAME`,`EVT_EVENT_ID`,`ORD_MENU_ID`,`CREATED_AT`,`MODIFIED_AT`,`ORDINATION`,`PARENT_ID`) VALUES ("Açougue", 1389, 38, "current_timestamp", "current_timestamp", 516, 133)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('CREATE TABLE GEN_STRUCTURE_TYPE (NRORG BIGINT DEFAULT NULL, NAME VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ID BIGINT AUTO_INCREMENT NOT NULL, STATUS VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD MAXPERSONS INT DEFAULT NULL, ADD MAXAREA INT DEFAULT NULL, ADD CHECKAPROVE VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, ADD RULES VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD IMAGE VARCHAR(2000) DEFAULT NULL COLLATE utf8_unicode_ci, ADD GEN_STRUCTURE_TYPE_ID BIGINT DEFAULT NULL');
        // $this->addSql('ALTER TABLE GEN_STRUCTURE ADD CONSTRAINT FK_STRUCTURETYPEID FOREIGN KEY (GEN_STRUCTURE_TYPE_ID) REFERENCES GEN_STRUCTURE_TYPE (ID)');
        // $this->addSql('CREATE INDEX FK_STRUCTURETYPEID ON GEN_STRUCTURE (GEN_STRUCTURE_TYPE_ID)');
        // $this->addSql('ALTER TABLE ORD_MENU_PRODUCT ADD FEATURED TINYINT(1) DEFAULT NULL');
        //$this->addSql('ALTER TABLE ORD_ORDER DROP DELIVERY_FEE');
        // $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD EXTERNAL_ID VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci');
        //$this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE SER_SERVICE ADD USER_SCHEDULING_LIMIT INT DEFAULT NULL, ADD DEPEDENT_SCHEDULING_LIMIT INT DEFAULT NULL');
    }
}
