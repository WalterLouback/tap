<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200902111419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Pão Caseiro Com Presunto E Queijo", 5.00, 1241, "Y021500900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Pão De Forma Integral Com Peito De Peru E Queijo", 5, 1241, "Y021501000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Baguete Frango Grelhado E Mostarda", 10.5, 1241, "Y021501100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sand. Baguete Carne Seca Com Requeijão", 10.5, 1241, "Y021501200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Baguete Frango E Ricota Ervas", 10.5, 1241, "Y021501300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Baguete Frango Grelhado", 10.5, 1241, "Y021501400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Peito Frango Defumado E Cream Cheese", 9.5, 1241, "Y021501500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Mussarela Light  ", 9.5, 1241, "Y021501600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Peito Peru E Queijo", 9.5, 1241, "Y021501700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Lombo E Provolone Com Mel", 9.5, 1241, "Y021501800")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Cremoso Frango", 8, 1241, "Y021502400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Cremoso Salpicão", 8, 1241, "Y021502500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sanduíche Natural Cremoso Atum", 8, 1241, "Y021502600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bolo De Churros   ", 4.5, 1241, "Y021603000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bolo De Brigadeiro   ", 4.5, 1241, "Y021603100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bolo De Leite Em Pó", 4.5, 1241, "Y021605700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Cerveja Heineken", 6.5, 1241, "Y030102300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Cerveja Amstel", 5.5, 1241, "Y030102400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Cerveja Heineken", 6.5, 1241, "Y030102500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Cerveja Heineken", 5.5, 1241, "Y030102600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola Zero", 3.3, 1241, "Y030104400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola", 4.5, 1241, "Y030900400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola Zero", 2.3, 1241, "Y030900500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola   ", 3.3, 1241, "Y030901100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Uva", 6.5, 1241, "Y030901200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Uva", 5.5, 1241, "Y030901300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Schweppes Citrus", 5.5, 1241, "Y030901400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Schweppes Tonica", 4.5, 1241, "Y030901500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Guaraná", 5.5, 1241, "Y030901600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola", 7.5, 1241, "Y030901700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Soda Limonada", 4.5, 1241, "Y030902200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sukita Uva", 4.5, 1241, "Y030902700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Laranja", 5.5, 1241, "Y030903400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola", 8.8, 1241, "Y030903600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sprite", 4.5, 1241, "Y030903700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Uva", 3.3, 1241, "Y030903900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Guaraná ", 3.3, 1241, "Y030904100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Coca-Cola Café Espresso   ", 3.3, 1241, "Y030904200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Fanta Laranja", 3.3, 1241, "Y030904600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sprite Original", 3.3, 1241, "Y030904700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Água Mineral (Crystal) Sem Gás", 4.5, 1241, "Y031100600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Água De Coco", 3.9, 1241, "Y031100700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Água Mineral Sem Gás   ", 5.5, 1241, "Y031101000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Água Mineral (Crystal) Comgás ", 4.5, 1241, "Y031101300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Suco Maçã Natural One   ", 5, 1241, "Y031302900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Suco Uva Natural One   ", 5, 1241, "Y031303000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Suco De Laranja Xandô", 5.9, 1241, "Y031306700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Suco De Laranja Natural One", 15.2, 1241, "Y031309000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Chá Pronto Verde Laranja Com Gengibre", 5.5, 1241, "Y031501900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Icetea Pêssego Zero", 5.5, 1241, "Y031502000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Icetea Limão Zero", 5.5, 1241, "Y031502100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Icetea Pêssego", 5.5, 1241, "Y031502200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Iso Drink Água De Coco", 16.9, 1241, "Y050705100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Amendoim Sem Pele", 7.7, 1241, "Y050910000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Amendoim Mendorato", 1.6, 1241, "Y050913600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Passatempo", 3.5, 1241, "Y051202800")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Calipso Chocolate Ao Leite", 8.5, 1241, "Y051203100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Integral Aveia E Mel ", 6, 1241, "Y051203301")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Integral Morango E Cereais", 4.8, 1241, "Y051203400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Salgado Integral Gergelim", 5, 1241, "Y051203500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Club Social Integral", 1.2, 1241, "Y051206600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Negresco    ", 3.9, 1241, "Y051206900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Club Social", 1.2, 1241, "Y051207200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Integral Leite E Mel", 4.8, 1241, "Y051208100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Cacau E Cereais   ", 4.8, 1241, "Y051208200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Passatempo Leite", 3.5, 1241, "Y051208300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Biscoito Maisena", 3.5, 1241, "Y051208900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bono Chocolate", 2.29, 1241, "Y051220000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Toddynho", 3.2, 1241, "Y051700600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Paçoca Rolha Pacoquita  ", 0.6, 1241, "Y051901600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Pingo De Leite", 0.6, 1241, "Y051902500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Alfajor Doce Leite Cobertura  Chocolate Meio Amargo", 2.75, 1241, "Y051902700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Alfajor Doce De Leite Cobertura Chocolate Ao Leite", 2.75, 1241, "Y051902900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Ouro Branco  ", 1.6, 1241, "Y052604000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sonho De Valsa  ", 1.6, 1241, "Y052604100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Charge", 3, 1241, "Y052604200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Diamante Negro  ", 2.5, 1241, "Y052604300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Laka", 2.5, 1241, "Y052604400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Lancy", 2.9, 1241, "Y052604500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Sensação", 3.3, 1241, "Y052604700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Kit Kat", 4.2, 1241, "Y052605100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Chocolate Moça", 3.3, 1241, "Y052609200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Classic Ao Leite", 2.75, 1241, "Y052609900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Snickers  ", 3.3, 1241, "Y052610000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("5star  ", 3.3, 1241, "Y052610100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Galak", 2.75, 1241, "Y052610200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Alpino", 2.75, 1241, "Y052610300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bis Xtra  ", 4.5, 1241, "Y052610400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Fresh Intense  ", 2.5, 1241, "Y056005900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Canela  ", 2.5, 1241, "Y056006000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Herbal Fresh  ", 2.5, 1241, "Y056006100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Hortelã  ", 2.5, 1241, "Y056006200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Melancia  ", 2.5, 1241, "Y056006300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Menta  ", 2.5, 1241, "Y056006400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Morango  ", 2.5, 1241, "Y056006500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Trident Tutti Frutti  ", 2.5, 1241, "Y056006600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Drops Halls Extra Forte 34 Gr - Un", 2, 1241, "Y056006700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Drops Halls Menta  34 Gr - Un", 2, 1241, "Y056006900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Drops Halls Uva Verde 34 Gr - Un", 2, 1241, "Y056007100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Cereja   ", 2.4, 1241, "Y056009100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Extra Forte   ", 2.4, 1241, "Y056009200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Melancia   ", 2.4, 1241, "Y056009300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Menta   ", 2.4, 1241, "Y056009400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Morango   ", 2.4, 1241, "Y056009500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Halls Uva Verde ", 2.4, 1241, "Y056009700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Mini Paleta Chocomaltine  ", 5.5, 1241, "Y060304300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Mini Paleta Paçoca  ", 5.5, 1241, "Y060304700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Mini Paleta Prestígio  ", 5.5, 1241, "Y060304900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Pão De Queijo Com Catupiry", 4, 1241, "Y060605700")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Bolo De Pote Chocolate Com Brigadeiro", 8, 1241, "Y070100600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Frango A Parmegiana Com Arroz E Batata Sauté", 15.99, 1241, "Y705006800")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Strogonoff De Frango Com Arroz E Batata Palha", 12.99, 1241, "Y705006900")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Feijoada Com Virado De Couve E Arroz", 13.99, 1241, "Y705007000")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Frango Ao Molho De Bacon Com Arroz E Vagem", 11.99, 1241, "Y705007200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Filé De Frango Com Spaguetti Integral", 9.99, 1241, "Y705007300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Calabresa Acebolada, Arroz E Feijão Preto", 12.99, 1241, "Y705007400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Curry De Grão De Bico, Arroz E Mix De Legumes", 13.99, 1241, "Y705007500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Feijoada Com Virado De Couve E Arroz", 20.89, 1241, "Y705008500")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Picadinho - Carne De Panela, Arroz Com Feijão E Farofa", 16.99, 1241, "Y705008600")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Virado À Paulista", 17.9, 1241, "Y705009300")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Moqueca Baiana", 14.9, 1241, "Y705009400")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Marmita  Clássica", 15.26, 1241, "Y705100100")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Marmita Especial", 17.9, 1241, "Y705100200")');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`NAME`, `PRICE`, `NRORG`, `EXTERNAL_ID`) VALUES ("Marmita Fitness", 14.9, 1241, "Y705100300")');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
