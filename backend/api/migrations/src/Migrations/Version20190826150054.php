<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826150054 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402EE2359285');
        $this->addSql('DROP INDEX SER_SERVICE_SELLER_IDX ON SER_TIMESHEET_REL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL CHANGE ser_service_seller_id SER_SERVICE_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402E2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL (SER_SERVICE_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE SER_TIMESHEET_REL DROP FOREIGN KEY FK_E9D3402E2C71C207');
        $this->addSql('DROP INDEX SER_SERVICE_IDX ON SER_TIMESHEET_REL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL CHANGE ser_service_id SER_SERVICE_SELLER_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD CONSTRAINT FK_E9D3402EE2359285 FOREIGN KEY (SER_SERVICE_SELLER_ID) REFERENCES SER_SERVICE_SELLER_REL (ID)');
        $this->addSql('CREATE INDEX SER_SERVICE_SELLER_IDX ON SER_TIMESHEET_REL (SER_SERVICE_SELLER_ID)');
    }
}
