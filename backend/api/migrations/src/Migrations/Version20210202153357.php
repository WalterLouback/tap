<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202153357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'inserir s/n nos genUsers 1472 e 1473';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('UPDATE GEN_CONFIGURATION SET STONE_ESTABLISHMENT_ID = "f478c4b6-35ee-4775-b9e6-c6bb20103e1b" WHERE NRORG = "1218"');
        

    }

    public function down(Schema $schema) : void
    {
    }
}
