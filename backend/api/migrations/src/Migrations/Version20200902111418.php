<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200902111418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('INSERT INTO `ORD_MENU_PRODUCT` (`ORD_PRODUCT_ID`, `PRICE`, `CREATED_AT`, `STATUS`, `ESTIMATED_TIME`, `NRORG`, `NAME`, `AMOUNT`, `ORD_MENU_ID`, `ORD_PRODUCT_GROUP_ID`) VALUES (3119, 3.5, current_timestamp, "A", 0, 1241, "Pão de Queijo", 1, 226, 914)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
