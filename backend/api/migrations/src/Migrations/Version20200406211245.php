<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200406211245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration utilizado para inserir organização Casa Grão no Xappa */
         //$this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(11, "Teknisa")');
         //$this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(11, 0)');
         //$this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 11, 1)');
         //$this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 11), ("R", 11), ("N", 11)');
         //$this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 11), ("CPF", 1, 3, 11), ("FIRST_NAME", 1, 3, 11), ("LAST_NAME", 1, 3, 11), ("BILLING_ADDRESS", 1, 2, 11), ("CORRESPONDENCE_ADDRESS", 1, 1, 11)');
         //$this->addSql('INSERT INTO `GEN_USER_TYPE_REL`(`GEN_USER_ID`,`GEN_USER_TYPE_ID`,`NRORG`,`STATUS`) VALUES (2,1,11,"A"), (2,4,11,"A")');
         //$this->addSql('INSERT INTO `ORD_WORKFLOW`(`NRORG`,`INITIAL_STATUS`, `PAYMENT_MOMENT`) VALUES (11,8,9)');
         //$this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(12, "The Black Beef ")');
         //$this->addSql('UPDATE GEN_ORGANIZATION SET NAME = \'Buteko\' WHERE NRORG = 1;');
         //$this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(12, 0)');
         //$this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 12, 1)');
         //$this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 12), ("R", 12), ("N", 12)');
         //$this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 12), ("CPF", 1, 3, 12), ("FIRST_NAME", 1, 3, 12), ("LAST_NAME", 1, 3, 12), ("BILLING_ADDRESS", 1, 2, 12), ("CORRESPONDENCE_ADDRESS", 1, 1, 12)');
         //$this->addSql('INSERT INTO `GEN_USER_TYPE_REL`(`GEN_USER_ID`,`GEN_USER_TYPE_ID`,`NRORG`,`STATUS`) VALUES (2,1,12,"A"), (2,4,12,"A")');
         //$this->addSql('INSERT INTO `ORD_WORKFLOW`(`NRORG`,`INITIAL_STATUS`, `PAYMENT_MOMENT`) VALUES (12,8,9)');

        //$this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
