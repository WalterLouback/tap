<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190809135941 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE SER_SERVICE_EVENT_REL (NRORG BIGINT DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, EVT_EVENT_ID BIGINT DEFAULT NULL, SER_SERVICE_ID BIGINT DEFAULT NULL, INDEX SER_SERVICE_EVENT_REL__EVENT_FK (EVT_EVENT_ID), INDEX SER_SERVICE_EVENT_REL__SERVICE_FK (SER_SERVICE_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE SER_SERVICE_EVENT_REL ADD CONSTRAINT FK_12D749ECB4A15433 FOREIGN KEY (EVT_EVENT_ID) REFERENCES EVT_EVENT (id)');
        $this->addSql('ALTER TABLE SER_SERVICE_EVENT_REL ADD CONSTRAINT FK_12D749EC2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE SER_SERVICE_EVENT_REL');
    }
}
