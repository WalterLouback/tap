<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200406143336 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
        $this->addSql('ALTER TABLE ORD_CONFIG_STORE ADD MIN_VALUE NUMERIC(8, 2) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_CONFIG_STORE DROP MIN_VALUE');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
