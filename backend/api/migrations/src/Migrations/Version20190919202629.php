<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190919202629 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ORD_OFFER_PRODUCT (AMOUNT INT NOT NULL, NRORG INT NOT NULL, CREATED_AT DATETIME NOT NULL, MODIFIED_AT DATETIME NOT NULL, CREATED_BY INT NOT NULL, MODIFIED_BY INT NOT NULL, ID INT AUTO_INCREMENT NOT NULL, ORD_MENU_PRODUCT_ID BIGINT DEFAULT NULL, INDEX IDX_8EC6CF7B435E45EC (ORD_MENU_PRODUCT_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ORD_OFFER_PRODUCT ADD CONSTRAINT FK_8EC6CF7B435E45EC FOREIGN KEY (ORD_MENU_PRODUCT_ID) REFERENCES ORD_MENU_PRODUCT (ID)');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD STATUS VARCHAR(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ORD_OFFER_PRODUCT');
        $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT DROP STATUS');
    }
}
