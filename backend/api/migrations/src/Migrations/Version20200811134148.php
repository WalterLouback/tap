<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811134148 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '146', PAYMENT_MOMENT = '146' WHERE ID = '30' and  nrorg = 1038");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '147' WHERE ID = '146' and  nrorg = 1038");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '148' WHERE ID = '147' and  nrorg = 1038");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '149' WHERE ID = '148' and  nrorg = 1038");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '150', PAYMENT_MOMENT = '150' WHERE ID = '29' and  nrorg = 1148");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '151' WHERE ID = '150' and  nrorg = 1148");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '152' WHERE ID = '151' and  nrorg = 1148");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '153' WHERE ID = '152' and  nrorg = 1148");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '154', PAYMENT_MOMENT = '154' WHERE ID = '17' and  nrorg = 1157");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '155' WHERE ID = '154' and  nrorg = 1157");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '156' WHERE ID = '155' and  nrorg = 1157");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '157' WHERE ID = '156' and  nrorg = 1157");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '158', PAYMENT_MOMENT = '158' WHERE ID = '31' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '159' WHERE ID = '158' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '160' WHERE ID = '159' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '161' WHERE ID = '160' and  nrorg = 1366");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '162', PAYMENT_MOMENT = '162' WHERE ID = '41' and  nrorg = 1378");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '163' WHERE ID = '162' and  nrorg = 1378");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '164' WHERE ID = '163' and  nrorg = 1378");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '165' WHERE ID = '164' and  nrorg = 1378");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '166', PAYMENT_MOMENT = '166' WHERE ID = '28' and  nrorg = 1380");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '167' WHERE ID = '166' and  nrorg = 1380");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '168' WHERE ID = '167' and  nrorg = 1380");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '169' WHERE ID = '168' and  nrorg = 1380");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '170', PAYMENT_MOMENT = '171' WHERE ID = '37' and  nrorg = 1490");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '171' WHERE ID = '170' and  nrorg = 1490");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '172' WHERE ID = '171' and  nrorg = 1490");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '173' WHERE ID = '172' and  nrorg = 1490");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '174', PAYMENT_MOMENT = '174' WHERE ID = '20' and  nrorg = 2028");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '175' WHERE ID = '174' and  nrorg = 2028");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '176' WHERE ID = '175' and  nrorg = 2028");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '177' WHERE ID = '176' and  nrorg = 2028");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '178', PAYMENT_MOMENT = '179' WHERE ID = '31' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '179' WHERE ID = '178' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '180' WHERE ID = '179' and  nrorg = 1366");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '181' WHERE ID = '180' and  nrorg = 1366");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '182', PAYMENT_MOMENT = '183' WHERE ID = '16' and  nrorg = 2105");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '183' WHERE ID = '182' and  nrorg = 2105");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '184' WHERE ID = '183' and  nrorg = 2105");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '185' WHERE ID = '184' and  nrorg = 2105");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '186', PAYMENT_MOMENT = '187' WHERE ID = '25' and  nrorg = 2442");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '187' WHERE ID = '186' and  nrorg = 2442");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '188' WHERE ID = '187' and  nrorg = 2442");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '189' WHERE ID = '188' and  nrorg = 2442");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '190', PAYMENT_MOMENT = '191' WHERE ID = '26' and  nrorg = 2476");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '191' WHERE ID = '190' and  nrorg = 2476");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '192' WHERE ID = '191' and  nrorg = 2476");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '193' WHERE ID = '192' and  nrorg = 2476");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '194', PAYMENT_MOMENT = '195' WHERE ID = '40' and  nrorg = 2486");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '195' WHERE ID = '194' and  nrorg = 2486");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '196' WHERE ID = '195' and  nrorg = 2486");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '197' WHERE ID = '196' and  nrorg = 2486");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '198', PAYMENT_MOMENT = '199' WHERE ID = '38' and  nrorg = 2560");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '199' WHERE ID = '198' and  nrorg = 2560");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '200' WHERE ID = '199' and  nrorg = 2560");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '201' WHERE ID = '200' and  nrorg = 2560");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '202', PAYMENT_MOMENT = '203' WHERE ID = '19' and  nrorg = 2562");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '203' WHERE ID = '202' and  nrorg = 2562");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '204' WHERE ID = '203' and  nrorg = 2562");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '205' WHERE ID = '204' and  nrorg = 2562");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '206', PAYMENT_MOMENT = '207' WHERE ID = '27' and  nrorg = 2624");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '207' WHERE ID = '206' and  nrorg = 2624");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '208' WHERE ID = '207' and  nrorg = 2624");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '209' WHERE ID = '208' and  nrorg = 2624");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '210', PAYMENT_MOMENT = '211' WHERE ID = '21' and  nrorg = 2658");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '211' WHERE ID = '210' and  nrorg = 2658");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '212' WHERE ID = '211' and  nrorg = 2658");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '213' WHERE ID = '212' and  nrorg = 2658");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '214', PAYMENT_MOMENT = '215' WHERE ID = '23' and  nrorg = 2751");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '215' WHERE ID = '214' and  nrorg = 2751");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '216' WHERE ID = '215' and  nrorg = 2751");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '217' WHERE ID = '216' and  nrorg = 2751");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '218', PAYMENT_MOMENT = '219' WHERE ID = '24' and  nrorg = 2818");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '219' WHERE ID = '218' and  nrorg = 2818");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '220' WHERE ID = '219' and  nrorg = 2818");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '221' WHERE ID = '220' and  nrorg = 2818");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '222', PAYMENT_MOMENT = '223' WHERE ID = '33' and  nrorg = 2861");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '223' WHERE ID = '222' and  nrorg = 2861");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '224' WHERE ID = '223' and  nrorg = 2861");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '225' WHERE ID = '224' and  nrorg = 2861");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '226', PAYMENT_MOMENT = '226' WHERE ID = '39' and  nrorg = 2960");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '227' WHERE ID = '226' and  nrorg = 2960");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '228' WHERE ID = '227' and  nrorg = 2960");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '229' WHERE ID = '228' and  nrorg = 2960");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '230', PAYMENT_MOMENT = '230' WHERE ID = '32' and  nrorg = 2980");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '231' WHERE ID = '230' and  nrorg = 2980");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '232' WHERE ID = '231' and  nrorg = 2980");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '233' WHERE ID = '232' and  nrorg = 2980");
        
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '234', PAYMENT_MOMENT = '235' WHERE ID = '36' and  nrorg = 2994");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '235' WHERE ID = '234' and  nrorg = 2994");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '236' WHERE ID = '235' and  nrorg = 2994");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '237' WHERE ID = '236' and  nrorg = 2994");
        
        

    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
