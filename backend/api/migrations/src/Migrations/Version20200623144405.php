<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200623144405 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION ADD URL_API_EXTERNAL VARCHAR(255) DEFAULT NULL, ADD TOKEN_API_EXTERNAL VARCHAR(255) DEFAULT NULL, ADD ORIGIN_API_EXTERNAL VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE EVT_EVENT ADD URL_API_EXTERNAL VARCHAR(255) DEFAULT NULL, ADD TOKEN_API_EXTERNAL VARCHAR(255) DEFAULT NULL, ADD ORIGIN_API_EXTERNAL VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER ADD OA_INTEGRATION_RESPONSE VARCHAR(255) DEFAULT NULL, ADD OA_SUCCESS VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
