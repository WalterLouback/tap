<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200401133141 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration para organização Vale COVID-19 */
        $this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(10, "Vale COVID-19")');
        $this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(10, 0)');
        $this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 10, 1)');
        $this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 10), ("R", 10), ("N", 10)');
        $this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 10), ("CPF", 1, 3, 10), ("FIRST_NAME", 1, 3, 10), ("LAST_NAME", 1, 3, 10), ("BILLING_ADDRESS", 1, 2, 10), ("CORRESPONDENCE_ADDRESS", 1, 1, 10)');
        $this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 10), ("CPF", 1, 3, 10), ("FIRST_NAME", 1, 3, 10), ("LAST_NAME", 1, 3, 10), ("BILLING_ADDRESS", 1, 2, 10), ("CORRESPONDENCE_ADDRESS", 1, 1, 10)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        
    }
}
