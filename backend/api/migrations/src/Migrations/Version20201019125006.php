<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201019125006 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'adicionei campo responseLog nas tabelas ord_order e pay_creditcard';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE PAY_CREDITCARD ADD RESPONSE_LOG LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_ORDER ADD RESPONSE_LOG LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER DROP RESPONSE_LOG');
        $this->addSql('ALTER TABLE PAY_CREDITCARD DROP RESPONSE_LOG');
    }
}
