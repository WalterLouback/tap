<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200803115637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ORD_SPLIT_RULE_RECIPIENT (PERCENTAGE NUMERIC(9, 2) DEFAULT NULL, FIXED_VALUE NUMERIC(9, 2) DEFAULT NULL, REMAINDER_VALUE VARCHAR(5) DEFAULT NULL, PROCESSING_FEE VARCHAR(5) DEFAULT NULL, TYPE VARCHAR(100) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, STATUS VARCHAR(1) DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, ORD_SPLIT_RULE_ID BIGINT DEFAULT NULL, ORD_RECIPIENT_ID BIGINT DEFAULT NULL, INDEX IDX_509F25AA579E18DA (ORD_SPLIT_RULE_ID), INDEX IDX_509F25AA235A537F (ORD_RECIPIENT_ID), UNIQUE INDEX ORD_SPLIT_RULE_RECIPIENT_PK (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ORD_SPLIT_RULE_RECIPIENT ADD CONSTRAINT FK_509F25AA579E18DA FOREIGN KEY (ORD_SPLIT_RULE_ID) REFERENCES ORD_SPLIT_RULE (ID)');
        $this->addSql('ALTER TABLE ORD_SPLIT_RULE_RECIPIENT ADD CONSTRAINT FK_509F25AA235A537F FOREIGN KEY (ORD_RECIPIENT_ID) REFERENCES ORD_RECIPIENT (ID)');
        // $this->addSql('DROP TABLE ORD_SPLIT_RULE_RECEIPIENT');
        $this->addSql('ALTER TABLE ORD_RECIPIENT ADD DESCRIPTION VARCHAR(250) DEFAULT NULL, ADD EMAIL VARCHAR(250) DEFAULT NULL, CHANGE HOLDER_NAME HOLDER_NAME VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
