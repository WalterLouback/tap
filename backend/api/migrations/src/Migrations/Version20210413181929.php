<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210413181929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE EVT_EVENT_DELIVER_RADIUS (NRORG BIGINT DEFAULT NULL, LATITUDE VARCHAR(100) DEFAULT NULL, LONGITUDE VARCHAR(100) DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT VARCHAR(255) DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, EVT_EVENT_DELIVER_ID BIGINT DEFAULT NULL, INDEX EVT_EVENT_DELIVER_RADIUS_FK (EVT_EVENT_DELIVER_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE EVT_EVENT_DELIVER_RADIUS ADD CONSTRAINT FK_E9C0DD5BDFF3B185 FOREIGN KEY (EVT_EVENT_DELIVER_ID) REFERENCES EVT_EVENT_DELIVER (ID)');
    }

    public function down(Schema $schema) : void
    {
       
    }
}
