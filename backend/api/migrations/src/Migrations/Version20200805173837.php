<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200805173837 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add relacionamento de recipient em evtEvent e payGateway';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE EVT_EVENT ADD ORD_RECIPIENT_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE EVT_EVENT ADD CONSTRAINT FK_C1DFE02B235A537F FOREIGN KEY (ORD_RECIPIENT_ID) REFERENCES ORD_RECIPIENT (ID)');
        $this->addSql('CREATE INDEX IDX_C1DFE02B235A537F ON EVT_EVENT (ORD_RECIPIENT_ID)');
        $this->addSql('ALTER TABLE PAY_GATEWAY ADD ORD_RECIPIENT_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE PAY_GATEWAY ADD CONSTRAINT FK_8661EA0A235A537F FOREIGN KEY (ORD_RECIPIENT_ID) REFERENCES ORD_RECIPIENT (ID)');
        $this->addSql('CREATE INDEX IDX_8661EA0A235A537F ON PAY_GATEWAY (ORD_RECIPIENT_ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
