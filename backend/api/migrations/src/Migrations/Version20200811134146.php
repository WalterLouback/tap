<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811134146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '57', PAYMENT_MOMENT = '57' WHERE ID = '34' and  nrorg = 2016");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '58' WHERE ID = '57' and  nrorg = 2016");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '59' WHERE ID = '58' and  nrorg = 2016");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '60' WHERE ID = '59' and  nrorg = 2016");
        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '61', PAYMENT_MOMENT = '61' WHERE ID = '18' and  nrorg = 1558");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '62' WHERE ID = '61' and  nrorg = 1558");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '63' WHERE ID = '62' and  nrorg = 1558");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '64' WHERE ID = '63' and  nrorg = 1558");

    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
