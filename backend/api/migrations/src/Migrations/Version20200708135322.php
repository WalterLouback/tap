<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200708135322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adicao de tabela e colunas ao banco';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ORD_CONFIG_STORE_DELIVERS (event_id BIGINT DEFAULT NULL, TYPE_NAME VARCHAR(255) NOT NULL, TYPE_DELIVERS VARCHAR(255) NOT NULL, ALLOWS_SCHEDULING VARCHAR(255) NOT NULL, NRORG INT NOT NULL, MAX_SCHEDULING INT NOT NULL, MIN_VALUE NUMERIC(8, 2) DEFAULT NULL, CREATED_AT DATETIME NOT NULL, MODIFIED_AT DATETIME NOT NULL, CREATED_BY INT NOT NULL, MODIFIED_BY INT NOT NULL, ID INT AUTO_INCREMENT NOT NULL, INDEX IDX_45BB605171F7E88B (event_id), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ORD_CONFIG_STORE_DELIVERS ADD CONSTRAINT FK_45BB605171F7E88B FOREIGN KEY (event_id) REFERENCES EVT_EVENT (ID)');
        $this->addSql('ALTER TABLE SER_TIME_TIMESHEET ADD MAX_SCHEDULING BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE SER_TIMESHEET_REL ADD ORD_CONFIG_STORE_DELIVERS_ID BIGINT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
