<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200305161050 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD GEN_SCHEDULE_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D73075114B FOREIGN KEY (GEN_SCHEDULE_ID) REFERENCES GEN_SCHEDULE (ID)');
        $this->addSql('CREATE INDEX GEN_SCHEDULE_ID_IDX ON GEN_SCHEDULE (GEN_SCHEDULE_ID)');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D73075114B');
        $this->addSql('DROP INDEX GEN_SCHEDULE_ID_IDX ON GEN_SCHEDULE');
        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP GEN_SCHEDULE_ID');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
