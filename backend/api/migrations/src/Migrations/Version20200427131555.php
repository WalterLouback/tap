<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200427131555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE GEN_USER_TYPE_REL ADD PASSWORD VARCHAR(128) DEFAULT NULL, ADD EMAIL VARCHAR(255) DEFAULT NULL');
        /*Comentei as alterações a seguir porque nao excluimos nada do bd*/
        // $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT DROP EXTERNAL_ID');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
        // $this->addSql('ALTER TABLE ORD_MENU_PRODUCT DROP FEATURED');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_USER_TYPE_REL DROP PASSWORD, DROP EMAIL');
        // $this->addSql('ALTER TABLE ORD_MENU_PRODUCT ADD FEATURED TINYINT(1) DEFAULT NULL');
        // $this->addSql('ALTER TABLE ORD_ORDER_PRODUCT ADD EXTERNAL_ID VARCHAR(200) DEFAULT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
