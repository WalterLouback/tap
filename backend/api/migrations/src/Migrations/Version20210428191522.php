<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428191522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('UPDATE GEN_CONFIGURATION SET PLAY_DELIVERY_EMAIL = "eattake@teknisa.com" WHERE ID = 44 AND NRORG = 1378');
        $this->addSql('UPDATE GEN_CONFIGURATION SET PLAY_DELIVERY_API_KEY = "eP_Gei21Cgz4pxzYLXhS" WHERE ID = 35 AND NRORG = 1378');
        $this->addSql('UPDATE GEN_CONFIGURATION SET AUTH_TOKEN = "fcBDKyGv6t3BksZuCEgDQw" WHERE ID = 44 AND NRORG = 1378');
        
    }

    public function down(Schema $schema) : void
    {
      
    }
}
