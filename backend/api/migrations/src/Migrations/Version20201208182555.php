<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208182555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        

        // $this->addSql("INSERT INTO GEN_SHIFT (`DAY`, `INITIAL_TIME`, `FINAL_TIME`, `EVT_EVENT_ID`, `CREATED_AT`) VALUES ('0', '2020-11-06 07:30:00', '2020-11-06 19:00:00', '1531', '2020-11-06 15:13:33')");
       //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2016')");


        
        
        
        
        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
