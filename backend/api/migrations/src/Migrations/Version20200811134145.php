<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811134145 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '2016')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '2016')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '2016')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2016')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Confirmar', 'Novo', '#e74c3c', '1558')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#f1c926', '1558')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#0300c9', '1558')");
        //$this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '1558')");

    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
