<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519152418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

       
        //$this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(2847, "The Black Beef")');
        //$this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(2847, 0)');
        //$this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 2847, 1)');
        //$this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 2847), ("R", 2847), ("N", 2847)');
        //$this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 2847), ("CPF", 1, 3, 2847), ("FIRST_NAME", 1, 3, 2847), ("LAST_NAME", 1, 3, 2847), ("BILLING_ADDRESS", 1, 2, 2847), ("CORRESPONDENCE_ADDRESS", 1, 1, 2847)');
        //$this->addSql('INSERT INTO `GEN_USER_TYPE_REL`(`GEN_USER_ID`,`GEN_USER_TYPE_ID`,`NRORG`,`STATUS`) VALUES (2,1,2847,"A"), (2,4,2847,"A")');
        //$this->addSql('INSERT INTO `ORD_WORKFLOW`(`NRORG`,`INITIAL_STATUS`, `PAYMENT_MOMENT`) VALUES (2847,8,9)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL DROP FOREIGN KEY FK_7634DC7B3589CB8B');
        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL CHANGE ORD_ORDER_ID ORD_ORDER_ID BIGINT DEFAULT NULL');
        //$this->addSql('ALTER TABLE ORD_ORDER_STATUS_REL ADD CONSTRAINT FK_7634DC7B3589CB8B FOREIGN KEY (ORD_ORDER_ID) REFERENCES ORD_ORDER (ORDER_IDENTIFIER)');
    }
}
