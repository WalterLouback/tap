<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420152658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration para atualizar taxas de frete */
        // $this->addSql("DELETE FROM EVT_EVENT_DELIVER WHERE ID = 5");
        // $this->addSql("DELETE FROM EVT_EVENT_DELIVER WHERE ID = 6");
        
        // $this->addSql("INSERT INTO EVT_EVENT_DELIVER(NRORG, DISTANCE_KM, PRICE, EVT_EVENT_ID) VALUES (13, '0.000 ate 0.500', '0.000', 1388)");
        // $this->addSql("INSERT INTO EVT_EVENT_DELIVER(NRORG, DISTANCE_KM, PRICE, EVT_EVENT_ID) VALUES (13, '0.501 ate 1.000', '5.000', 1388)");
        // $this->addSql("INSERT INTO EVT_EVENT_DELIVER(NRORG, DISTANCE_KM, PRICE, EVT_EVENT_ID) VALUES (13, '1.001 ate 3.000', '8.000', 1388)");
        // $this->addSql("INSERT INTO EVT_EVENT_DELIVER(NRORG, DISTANCE_KM, PRICE, EVT_EVENT_ID) VALUES (13, '3.001 ate 6.000', '10.000', 1388)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_PRODUCT DROP EXTERNAL_ID');
    }
}
