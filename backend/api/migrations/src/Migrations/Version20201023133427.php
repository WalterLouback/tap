<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201023133427 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add field requestLog at the table ordOrder';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('UPDATE GEN_CONFIGURATION SET DB_NAME = "homolog" WHERE ID = 47');
        //$this->addSql('INSERT INTO `ORD_PRODUCT` (`DETAIL`, `NAME`, `PRICE`, `NRORG`, `ESTIMATED_TIME`, `IMAGE`, `EXTERNAL_ID`) VALUES ("(Arroz, batata frita, salada de alface e tomate) (Valido para refeição no SALÃO)", "Filé de frango à milanesa com molho barbecue", 18.9, 1378, 15, "", "9.01.25.245.00")');
        
        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE ORD_ORDER DROP REQUEST_LOG');
    }
}
