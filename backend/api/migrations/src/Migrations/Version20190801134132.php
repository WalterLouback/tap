<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190801134132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE GEN_DOC_USER_REL (STATUS VARCHAR(255) DEFAULT NULL, DOCUMENT VARCHAR(255) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, GEN_DOCUMENT_ID BIGINT DEFAULT NULL, GEN_USER_ID BIGINT DEFAULT NULL, INDEX IDX_D0A4C9965741ABA9 (GEN_DOCUMENT_ID), INDEX IDX_D0A4C9964B3BFC8A (GEN_USER_ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE GEN_DOCUMENT (EXPIRATION_DATE DATETIME DEFAULT NULL, NAME VARCHAR(255) DEFAULT NULL, STATUS VARCHAR(255) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, TYPE VARCHAR(255) DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE GEN_DOC_USER_REL ADD CONSTRAINT FK_D0A4C9965741ABA9 FOREIGN KEY (GEN_DOCUMENT_ID) REFERENCES GEN_DOCUMENT (id)');
        $this->addSql('ALTER TABLE GEN_DOC_USER_REL ADD CONSTRAINT FK_D0A4C9964B3BFC8A FOREIGN KEY (GEN_USER_ID) REFERENCES GEN_USER (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_DOC_USER_REL DROP FOREIGN KEY FK_D0A4C9965741ABA9');
        $this->addSql('DROP TABLE GEN_DOC_USER_REL');
        $this->addSql('DROP TABLE GEN_DOCUMENT');
    }
}
