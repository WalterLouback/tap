<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version2021042819152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('UPDATE GEN_STRUCTURE SET PARENT_ID = NULL WHERE ID = 1145 AND NRORG = 17');
        $this->addSql('UPDATE EVT_EVENT SET STATUS = "A" WHERE ID = 1606 AND NRORG = 13');


    }

    public function down(Schema $schema) : void
    {
      
    }
}
