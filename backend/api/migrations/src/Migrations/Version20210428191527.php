<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428191527 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_API_KEY = null WHERE ID = 51 AND NRORG = "3114"');
	    $this->addSql('UPDATE GEN_CONFIGURATION SET LOGGI_SHOP_ID = null WHERE ID = 51 AND NRORG = "3114"');
	    
    }

    public function down(Schema $schema) : void
    {
      
    }
}
