<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200617200753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION ADD HOST_SECURE VARCHAR(255) DEFAULT NULL, ADD HOST_PORT VARCHAR(255) DEFAULT NULL');
        // $this->addSql("INSERT INTO gen_configuration ( NRORG, LOGO_IMAGE, LOGO_IMAGE_SMALL, PRIMARY_COLOR, SECONDARY_COLOR, YES_COLOR, NO_COLOR, MES_NO_REGISTER, CREATED_AT, MODIFIED_AT, CREATED_BY, MODIFIED_BY, HOST, HOST_FROM, HOST_PASSWORD, HOST_FROM_NAME, HOST_PORT, HOST_SECURE, USE_INTEGRATION, FCM_SERVER_KEY, ID, AWS_S3_ACCESS_KEY, AWS_S3_SECRET_KEY, AWS_S3_BUCKET, PREFERENCE_FLOW, GOOGLE_ANALYTICS_KEY, PROCOB_AUTH_KEY, ALLOW_EXTERNAL_USERS, APP_LAYOUT_CONFIG, KEY_GOOGLE, CONVENIENCE_FEE) VALUES (0, 'https://i.ibb.co/m65bB2L/download.png', 'https://i.ibb.co/jLgxmR6/logo-teknisa.png', '#0067AB', '#0067AB', '#7ac95b', '#e74c3c', NULL, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 0, 0, 'smtp.teknisa.com', 'eattake@teknisa.com', 'Teknisa9558?', 'eattake', 5587, 'tls', '0', NULL, '0', NULL, NULL, NULL, '0', 'UA-144027546-2', NULL, '1', '{\"homeLayout\":\"classic\",\"slideNav\":[{\"id\":\"0\",\"name\":\"Perfil\",\"icon\":[\"user\"],\"child\":[]},{\"id\":\"1\",\"name\":\"Dependentes\",\"icon\":[\"users\"],\"child\":[]},{\"id\":\"2\",\"name\":\"Carteira\",\"icon\":[\"wallet\"],\"child\":[]},{\"id\":\"3\",\"name\":\"Documentos\",\"icon\":[\"file\"],\"child\":[]},{\"id\":\"4\",\"name\":\"Fale com o RH\",\"icon\":[\"volume\"],\"child\":[]},{\"id\":\"6\",\"name\":\"Pesquisa de Satisfação\",\"icon\":[\"clipboard\"],\"child\":[]},{\"id\":\"7\",\"name\":\"Mais Informações\",\"icon\":[\"plus-square\"],\"child\":[{\"id\":\"10\",\"name\":\"Trocar de Empresa\",\"icon\":[\"sync\"]},{\"id\":\"11\",\"name\":\"Sobre\",\"icon\":[\"info-circle\"],\"click\":[]}]},{\"id\":\"8\",\"name\":\"Sair\",\"icon\":[\"sign-out-alt\"],\"child\":[]}],\"BottomNavigation\":[{\"id\":\"0\",\"name\":\"Início\",\"icon\":[\"home\"]},{\"id\":\"1\",\"name\":\"Compras\",\"icon\":[\"store\"]},{\"id\":\"3\",\"name\":\"Serviços\",\"icon\":[\"sparkles\"]},{\"id\":\"4\",\"name\":\"Agenda\",\"icon\":[\"calendar-alt\"]}]}', NULL, '3.00')");
        // $this->addSql("INSERT INTO GEN_ORGANIZATION (NAME, CREATED_AT,MODIFIED_AT,CREATED_BY,MODIFIED_BY, NRORG, NICK_NAME) VALUES ('TEKNISA SOFTWARE LTDA', CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),0,0, 0, 'TEKNISA')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
