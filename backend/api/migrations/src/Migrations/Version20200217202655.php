<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217202655 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION CHANGE HOME_LAYOUT HOME_LAYOUT TEXT DEFAULT NULL');
        /**
         * O JSON a seguir deve ser inserido no campo criado acima.
         * `HOME_LAYOUT` = '\"{\"slideNav\": [{\"id\": \"0\", \"name\": \"Perfil\", \"icon\": [\"user\"],\n\"child\": []}, { \"id\": \"1\", \"name\": \"Dependentes\", \"icon\": [\"users\"], \"child\": [] },\n{ \"id\": \"2\", \"name\": \"Carteira\", \"icon\": [\"wallet\"], \"child\": [] }, { \"id\": \"3\", \"name\": \"Documentos\",\n\"icon\": [\"file\"], \"child\": [] }, { \"id\": \"4\", \"name\": \"Ouvidoria\", \"icon\": [\"volume\"], \"child\": [] },\n{ \"id\": \"5\", \"name\": \"Boletos\", \"icon\": [\"file-alt\"], \"child\": [] }, { \"id\": \"6\", \"name\": \"Pesquisa de Satisfação\",\n\"icon\": [\"clipboard\"], \"child\": [] }, { \"id\": \"7\", \"name\": \"Mais Informações\", \"icon\": [\"plus-square\"],\n\"child\": [{ \"id\": \"0\", \"name\": \"Pessoas no Clube\", \"icon\": [\"umbrella\"] },  {  \"id\": \"1\", \"name\": \"Trocar de Clube\",\n\"icon\": [\"sync\"] }, { \"id\": \"2\", \"name\": \"Sobre\", \"icon\": [\"info-circle\"], \"click\": [] } ] }, { \"id\": \"8\", \"name\": \"Sair\",\n\"icon\": [\"sign-out-alt\"], \"child\": [] } ], \"BottomNavigation\": [{ \"id\": \"0\", \"name\": \"Início\",\"icon\": [\"home\"] }, \n{\"id\": \"1\", \"name\": \"Pedidos\", \"icon\": [\"utensils\"] }, {\"id\": \"2\", \"name\": \"Eventos\", \"icon\": [\"compass\"]}, \n{\"id\": \"3\", \"name\": \"Serviços\", \"icon\": [\"sparkles\"]}, {\"id\": \"4\", \"name\": \"Agenda\",\"icon\": [\"calendar-alt\"] } ] }\"'
         */
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_CONFIGURATION CHANGE HOME_LAYOUT HOME_LAYOUT VARCHAR(250) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
