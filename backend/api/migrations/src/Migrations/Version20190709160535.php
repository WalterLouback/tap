<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190709160535 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_CASHIER_MOVEMENT CHANGE data_time MOVEMENT_DATE DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_CASHIER CHANGE opennig OPENNING DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_CASHIER CHANGE openning OPENNIG DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_CASHIER_MOVEMENT CHANGE movement_date DATA_TIME DATETIME DEFAULT NULL');
    }
}
