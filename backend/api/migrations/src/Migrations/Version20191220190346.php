<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191220190346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_RECIPIENT ADD ANTICIPATABLE_VOLUME_PERCENTAGE NUMERIC(9, 2) DEFAULT NULL, ADD AUTOMATIC_ANTICIPATION_ENABLED INT DEFAULT NULL, ADD TRANSFER_DAY INT DEFAULT NULL, ADD TRANSFER_ENABLED INT DEFAULT NULL, ADD TRANSFER_INTERVAL VARCHAR(255) DEFAULT NULL, ADD PAY_GATEWAY_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE ORD_RECIPIENT ADD CONSTRAINT FK_A188802F5B180EA2 FOREIGN KEY (PAY_GATEWAY_ID) REFERENCES PAY_GATEWAY (ID)');
        // $this->addSql('CREATE INDEX IDX_A188802F5B180EA2 ON ORD_RECIPIENT (PAY_GATEWAY_ID)');
        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE ORD_RECIPIENT DROP FOREIGN KEY FK_A188802F5B180EA2');
        // $this->addSql('DROP INDEX IDX_A188802F5B180EA2 ON ORD_RECIPIENT');
        $this->addSql('ALTER TABLE ORD_RECIPIENT DROP ANTICIPATABLE_VOLUME_PERCENTAGE, DROP AUTOMATIC_ANTICIPATION_ENABLED, DROP TRANSFER_DAY, DROP TRANSFER_ENABLED, DROP TRANSFER_INTERVAL, DROP PAY_GATEWAY_ID');
    }
}
