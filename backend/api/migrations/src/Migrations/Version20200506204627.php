<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200506204627 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "0.000 ate 0.100", 0.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "0.101 ate 0.500", 3.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "0.501 ate 1.000", 5.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "1.001 ate 3.000", 8.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "3.001 ate 5.000", 10.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "5.001 ate 10.000", 12.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "10.001 ate 15.000", 15.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "15.001 ate 20.000", 18.00, 1389)');
        //$this->addSql('INSERT INTO `EVT_EVENT_DELIVER` (`NRORG`, `DISTANCE_KM`, `PRICE`, `EVT_EVENT_ID`) VALUES (14, "20.001 ate 25.000", 20.00, 1389)');
  

        
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ORD_OPTION DROP ACTIVE');
        //$this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
    }
}
