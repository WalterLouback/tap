<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210428191519 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "97141" WHERE ID = 1567 AND NRORG = "3114"');
        $this->addSql('UPDATE EVT_EVENT SET LOGGI_SHOP_ID = "97141" WHERE ID = 1568 AND NRORG = "3114"');
    }

    public function down(Schema $schema) : void
    {
      
    }
}
