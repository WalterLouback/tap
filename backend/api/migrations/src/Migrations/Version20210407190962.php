<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407190962 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('INSERT INTO GEN_ADDRESS (`CEP`, `STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE`) VALUES ("70353-530", "SCLS 306 BLOCO C", "ASA SUL", "BRASÍLIA", "DF", "A", "3114", "1567", "28", "BRASIL", "DELIVERY", "-15.811129356484273", "-47.901708231272806")');
        //$this->addSql('INSERT INTO GEN_ADDRESS (`CEP`, `STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE`) VALUES ("70353-530", "SCLS 306 BLOCO C", "ASA SUL", "BRASÍLIA", "DF", "A", "3114", "1568", "28", "BRASIL", "DELIVERY", "-15.811129356484273", "-47.901708231272806")');
        //$this->addSql('INSERT INTO GEN_ADDRESS (`CEP`, `STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE`) VALUES ("70844-530", "SCLN 206 BLOCO C", "ASA NORTE", "BRASÍLIA", "DF", "A", "3114","1566", "15", "BRASIL", "DELIVERY", "-15.769808913797203", "-47.879314260964584")');
        //$this->addSql('INSERT INTO GEN_ADDRESS (`CEP`, `STREET`, `NEIGHBORHOOD`, `CITY`, `PROVINCY`, `STATUS`, `NRORG`, `EVT_EVENT_ID`, `STREET_NUMBER`, `COUNTRY`, `TYPE`, `LATITUDE`, `LONGITUDE`) VALUES ("71680-120", "SMDB CONJUNTO 12", "LAGO SUL", "BRASÍLIA", "DF","A", "3114", "1569", "2", "BRASIL", "DELIVERY", "-15.857790101675064", "-47.83146795911487")');

    }



    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
      
    }
}
