<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190827195659 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP FOREIGN KEY FK_4C80C6D761BE9060');
        $this->addSql('DROP INDEX SER_TIMESHEET_IDX ON GEN_SCHEDULE');
        $this->addSql('ALTER TABLE GEN_SCHEDULE DROP SER_TIMESHEET_ID');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD SER_TIMESHEET_ID BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE GEN_SCHEDULE ADD CONSTRAINT FK_4C80C6D761BE9060 FOREIGN KEY (SER_TIMESHEET_ID) REFERENCES SER_TIMESHEET (ID)');
        $this->addSql('CREATE INDEX SER_TIMESHEET_IDX ON GEN_SCHEDULE (SER_TIMESHEET_ID)');
    }
}
