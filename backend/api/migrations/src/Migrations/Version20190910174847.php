<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190910174847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE SUR_QUESTION_REL (STATUS VARCHAR(15) DEFAULT NULL, NRORG BIGINT DEFAULT NULL, CREATED_AT DATETIME DEFAULT NULL, MODIFIED_AT DATETIME DEFAULT NULL, CREATED_BY BIGINT DEFAULT NULL, MODIFIED_BY BIGINT DEFAULT NULL, ID BIGINT AUTO_INCREMENT NOT NULL, SER_SERVICE_ID BIGINT DEFAULT NULL, EVT_EVENT_ID BIGINT DEFAULT NULL, EVT_EVENT_SELLER_ID BIGINT DEFAULT NULL, SUR_QUESTION_ID BIGINT DEFAULT NULL, INDEX fk_question_rel_question1 (SUR_QUESTION_ID), INDEX fk_question_rel_event1 (EVT_EVENT_ID), INDEX fk_question_rel_event_seller1 (EVT_EVENT_SELLER_ID), INDEX fk_question_rel_service1 (SER_SERVICE_ID), UNIQUE INDEX ID_UNIQUE (ID), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE SUR_QUESTION_REL ADD CONSTRAINT FK_D2D0968A2C71C207 FOREIGN KEY (SER_SERVICE_ID) REFERENCES SER_SERVICE (ID)');
        $this->addSql('ALTER TABLE SUR_QUESTION_REL ADD CONSTRAINT FK_D2D0968AB4A15433 FOREIGN KEY (EVT_EVENT_ID) REFERENCES EVT_EVENT (ID)');
        $this->addSql('ALTER TABLE SUR_QUESTION_REL ADD CONSTRAINT FK_D2D0968A84275088 FOREIGN KEY (EVT_EVENT_SELLER_ID) REFERENCES EVT_EVENT_SELLER (ID)');
        $this->addSql('ALTER TABLE SUR_QUESTION_REL ADD CONSTRAINT FK_D2D0968AC48FC40 FOREIGN KEY (SUR_QUESTION_ID) REFERENCES SUR_QUESTION (ID)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE SUR_QUESTION_REL');
    }
}
