<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420152655 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Migration para criara organização EATPOINT */
        // $this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(13, "EATPOINT")');
        // $this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(13, 0)');
        // $this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 13, 1)');
        // $this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 13), ("R", 13), ("N", 13)');
        // $this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 13), ("CPF", 1, 3, 13), ("FIRST_NAME", 1, 3, 13), ("LAST_NAME", 1, 3, 13), ("BILLING_ADDRESS", 1, 2, 13), ("CORRESPONDENCE_ADDRESS", 1, 1, 13)');
        // $this->addSql('INSERT INTO `GEN_USER_TYPE_REL`(`GEN_USER_ID`,`GEN_USER_TYPE_ID`,`NRORG`,`STATUS`) VALUES (2,1,13,"A"), (2,4,13,"A")');
        // $this->addSql('INSERT INTO `ORD_WORKFLOW`(`NRORG`,`INITIAL_STATUS`, `PAYMENT_MOMENT`) VALUES (13,8,9)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE ORD_ORDER_RECIPIENT CHANGE ID ID VARCHAR(36) NOT NULL COLLATE utf8_unicode_ci');
        // $this->addSql('ALTER TABLE ORD_PRODUCT DROP EXTERNAL_ID');
    }
}
