<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200326193905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /* Organização Praia Clube */
        // $this->addSql('INSERT INTO `GEN_ORGANIZATION`(`NRORG`,`NAME`)VALUES(9, "Praia Clube")');
        // $this->addSql('INSERT INTO `GEN_CONFIGURATION`(`NRORG`, `USE_INTEGRATION`)VALUES(9, 0)');
        // $this->addSql('INSERT INTO `GEN_TAG`(`NAME`, `TYPE`, `NRORG`, `GEN_CATEGORY_ID`) VALUES ("Geral", "E", 9, 1)');
        // $this->addSql('INSERT INTO `GEN_PERMISSION` (`NAME`, `NRORG`) VALUES ("A", 9), ("R", 9), ("N", 9)');
        // $this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 9), ("CPF", 1, 3, 9), ("FIRST_NAME", 1, 3, 9), ("LAST_NAME", 1, 3, 9), ("BILLING_ADDRESS", 1, 2, 9), ("CORRESPONDENCE_ADDRESS", 1, 1, 9)');
        // $this->addSql('INSERT INTO `GEN_EDITABLE_FIELDS` (`FIELD_NAME`, `GEN_USER_TYPE_ID`, `GEN_PERMISSION_ID`, `NRORG`) VALUES ("NPF", 1, 3, 9), ("CPF", 1, 3, 9), ("FIRST_NAME", 1, 3, 9), ("LAST_NAME", 1, 3, 9), ("BILLING_ADDRESS", 1, 2, 9), ("CORRESPONDENCE_ADDRESS", 1, 1, 9)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
