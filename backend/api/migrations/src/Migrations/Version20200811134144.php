<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200811134144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Em preparo', 'Novo', '#7ac95b', '2847')");
        // $this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Pronto', 'Em preparo', '#7ac95b', '2847')");
        // $this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES ('Entregue', 'Pronto', '#7ac95b', '2847')");
        // $this->addSql("INSERT INTO GEN_STATUS (`BUTTON_NAME`, `LABEL_NAME`, `COLOR`, `NRORG`) VALUES (NULL, 'Entregue', '#7ac95b', '2847')");

        //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '56', PAYMENT_MOMENT = '56' WHERE ID = '15' and  nrorg = 2847");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '53' WHERE ID = '54' and  nrorg = 2847");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '54' WHERE ID = '55' and  nrorg = 2847");
        //$this->addSql("UPDATE GEN_STATUS SET NEXT = '55' WHERE ID = '56' and  nrorg = 2847");

    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
