<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210217192060 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

         //$this->addSql("UPDATE ORD_WORKFLOW SET INITIAL_STATUS = '249', PAYMENT_MOMENT = '249' WHERE ID = '67' and  nrorg = 3251");
         //$this->addSql("UPDATE GEN_STATUS SET NEXT = '250' WHERE ID = '249' and  nrorg = 3251");
         //$this->addSql("UPDATE GEN_STATUS SET NEXT = '251' WHERE ID = '250' and  nrorg = 3251");
         //$this->addSql("UPDATE GEN_STATUS SET NEXT = '252' WHERE ID = '251' and  nrorg = 3251");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
