# instalar pacotes
composer install

# instalar symfony
composer create-project -s stable symfony/skeleton new-project

# instalar pacotes
    composer install

# gerar new entity 
    bin/console make:entity

# gerar entity apartir do banco de dados
    php bin/console doctrine:mapping:import "Zeedhi\ApiGeneral\Model\Entities" xml --path=src/ApiGeneral/Model/Entities

# gerar getrs e setrs
    php bin/console make:entity --regenerate Zeedhi

# criar migration
    php bin/console make:migration

# aplicar migrations
    php bin/console doctrine:migrations:migrate

# Link da documentação
    https://symfony.com/doc/current/doctrine/reverse_engineering.html

# criar novo DB
    php bin/console doctrine:database:create

# gerar models apartir entity
    php bin/console doctrine:generate:entities --path=src/Entity