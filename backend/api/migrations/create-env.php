<?php
  $xmldata = simplexml_load_file("../../environment.xml") or die("Failed to load");

  $user   = (string) $xmldata->parameters[0]->parameter[0]->parameter[1];
  $pass   = (string) $xmldata->parameters[0]->parameter[0]->parameter[2];
  $host   = (string) $xmldata->parameters[0]->parameter[0]->parameter[3];
  $port   = (string) $xmldata->parameters[0]->parameter[0]->parameter[4];
  $schema = (string) $xmldata->parameters[0]->parameter[0]->parameter[5];
  // $pass = "b1pfun%40pp_";
  file_put_contents(".env-bipfun", "DATABASE_URL=mysql://$user:$pass@$host:$port/$schema", FILE_APPEND);
?>