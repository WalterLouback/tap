*** Projeto Migrations ***

*** PROCESSO DE ALTERAÇÃO DO BANCO DE DADOS ***

*** Você pode gerar o file.orm.xml apartir do db e depois realizar os passos seguintes ***
*** NÃO USE ISSO! ***
# gerar entity apartir do banco de dados
    php bin/console doctrine:mapping:import "Zeedhi\ApiGeneral\Model\Entities" xml --path=src/ApiGeneral/Model/Entities

*** Crie o file.orm.xml da nova tabela ou adicione ou altere um campo no file.orm.xml de uma tabela existente ***

*** Em caso de criação de tabela rode o comando a seguir para gerar getrs e setrs ***
*** Em caso de alteração de tabela ADD o variavel ao file.php correspondente ao file.orm.xml rode o comando a seguir para ADD getrs e setrs da variavel ***
OBS: ATENÇÂO REMOVER A PASTA MigrationsProducao, PARA RODAR ESSES COMANDOS
# gerar getrs e setrs
    php bin/console make:entity --regenerate Zeedhi

*** Rode o comando a seguir para criar o arquivo que contem o sql para criar ou alterar o DB ***
# criar migration
    php bin/console make:migration

*** Aplique a ultima alteração ou as ultimas se forem mais de uma com o comando a seguir ***
# aplicar migrations
    php bin/console doctrine:migrations:migrate

# CUIDADO COM O COMANDO A SEGUIR
# desfazer migrations
    php bin/console doctrine:migrations:migrate prev
    
*** O doctrine cria uma tabela no db que guarda as migrations já aplicadas TABLE: migration_versions ***