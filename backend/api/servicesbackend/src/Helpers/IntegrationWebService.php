<?php
namespace Zeedhi\ApiServices\Helpers;

class IntegrationWebService {
    
    public function consumesWebService($url, $parameters) {
        $data = sprintf("%s?%s", $url, http_build_query($parameters));
        // inicia seção CURL 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode($result, true);

        return $objResult;
    }
    
}