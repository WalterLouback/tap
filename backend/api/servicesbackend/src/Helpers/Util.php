<?php
namespace Zeedhi\ApiServices\Helpers;

use Zeedhi\Framework\DTO\Request;

class Util {
    public static function getParamsAsArray($obj) {
        if($obj instanceof Request\DataSet) {
            // returns An Array
            return $obj->getDataSet()->getRows();
        } elseif ($obj instanceof Request\Row) {
            return $obj->getRow();
        } elseif ($obj instanceof Request\Filter) {
            $arr = array();
            foreach ($obj->getFilterCriteria()->getConditions() as $condition) {
                $arr[ $condition['columnName'] ] = $condition['value'];  
            }
            return $arr;
        }
    }
    public static function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
}    