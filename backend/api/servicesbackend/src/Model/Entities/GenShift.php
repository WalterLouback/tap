<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenShift extends \Zeedhi\ApiServices\Model\Entities\Base\GenShift {
    
    function toArray() {
        return array(
            'ID' => $this->getId(),
            'DAY' => $this->getDay(),
            'INITIAL_TIME' => $this->getInitialTime(),
            'FINAL_TIME' => $this->getFinalTime()
        );
    }
    
    static function manyToArray($shifts) {
        $array = [];
        foreach ($shifts as $shift) {
            array_push($array, $shift->toArray());
        }
        return $array;
    }
    
}