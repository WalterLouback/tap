<?php
namespace Zeedhi\ApiServices\Model\Entities;


class EvtEventSeller extends \Zeedhi\ApiServices\Model\Entities\Base\EvtEventSeller {
    
    function build($entityManager, $nrorg) {
        $this->getGenUser()->build($entityManager, $nrorg);
    }
    
    function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_DATA' => $this->getGenUser() ? $this->getGenUser()->toArray() : [],
            'SELLER_TYPE' => $this->getEvtSellerType() ? $this->getEvtSellerType()->toArray() : []
        );
    }
    
    static function manyToArray($sellers) {
        $array = [];
        foreach ($sellers as $seller) {
            array_push($array, $seller->toArray());
        }
        return $array;
    }

}