<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenUserType extends \Zeedhi\ApiServices\Model\Entities\Base\GenUserType {
    
    const USER_TYPE_ID = 'USER_TYPE_ID';
    const USUARIO_ID  = 1;
    const VENDEDOR_ID = 3;
    const ADMIN_ID    = 4;
    
}