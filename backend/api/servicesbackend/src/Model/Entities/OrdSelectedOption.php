<?php
namespace Zeedhi\ApiServices\Model\Entities;


class OrdSelectedOption extends \Zeedhi\ApiServices\Model\Entities\Base\OrdSelectedOption {
    
    public function toArray() {
        return $this->getOrdOption()->toArray();
    }
    
    public static function manyToArray($arrays) {
        $options = [];
        foreach ($arrays as $option) {
            array_push($options, $option->toArray());
        }
        return $options;
    }
    
}