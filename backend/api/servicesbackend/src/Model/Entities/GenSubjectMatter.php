<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenSubjectMatter extends \Zeedhi\ApiServices\Model\Entities\Base\GenSubjectMatter {
    
    public function toArray() {
        return array(
            'id'                => $this->getId(),
            'subjectMatter'     => $this->getSubjectMatter(),
            'emailResponsible'  => $this->getEmailResponsible()
        );
    }
}