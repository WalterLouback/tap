<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenAddress extends \Zeedhi\ApiServices\Model\Entities\Base\GenAddress {
    
    const NRORG        = 'NRORG';
    const CEP          = 'CEP';
    const STREET       = 'STREET';
    const NUMBER       = 'NUMBER';
    const COMPLEMENT   = 'COMPLEMENT';
    const COUNTRY      = 'COUNTRY';
    const NEIGHBORHOOD = 'NEIGHBORHOOD';
    const CITY         = 'CITY';
    const PROVINCY     = 'PROVINCY';
    const TYPE         = 'TYPE';
    
    const TYPE_BILLING        = 'BILLING_ADDRESS';
    const TYPE_CORRESPONDENCE = 'CORRESPONDENCE_ADDRESS';
    
    public static function manyToArray($addresses) {
        $array = [];
        foreach ($addresses as $address) {
            array_push($array, $address->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "CEP" => $this->getCep(),
            "STREET" => $this->getStreet(),
            "NEIGHBORHOOD" => $this->getNeighborhood(),
            "NUMBER" => $this->getNumber(),
            "COMPLEMENT" => $this->getComplement(),
            "COUNTRY" => $this->getCountry(),
            "CITY" => $this->getCity(),
            "PROVINCY" => $this->getProvincy(),
            "NRORG" => $this->getNrorg(),
            "TYPE" => $this->getType(),
            "MODIFIED_AT" => $this->getModifiedAt(),
            "CREATED_AT" => $this->getCreatedAt()
        );
    }
    
}