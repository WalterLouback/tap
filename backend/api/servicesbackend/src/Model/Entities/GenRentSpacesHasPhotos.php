<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenRentSpacesHasPhotos extends \Zeedhi\ApiServices\Model\Entities\Base\GenRentSpacesHasPhotos {
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['urlPicture'] = $this->getLink();
        
        return $array;
    }
    
    public static function manyToArray($photos) {
        $arrays = [];
        foreach ($photos as $photo) {
            array_push($arrays, $photo->toArray());
        }
        return $arrays;
    }
}