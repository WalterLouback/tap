<?php
namespace Zeedhi\ApiServices\Model\Entities;


class PayPaymentMethod extends \Zeedhi\ApiServices\Model\Entities\Base\PayPaymentMethod {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "PAYMENT_METHOD" => $this->getPaymentMethod(),
            "LABEL" => $this->getLabel(),
            "X_PICPAY_TOKEN" => $this->getXPicpayToken(),
            "X_SELLER_TOKEN" => $this->getXSellerToken()
        );
    }

    
}