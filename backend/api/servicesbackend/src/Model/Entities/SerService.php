<?php

namespace Zeedhi\ApiServices\Model\Entities;

class SerService extends \Zeedhi\ApiServices\Model\Entities\Base\SerService {
    
    public function toArray($serviceEventRels = NULL) {
        $events = [];
        
        if($serviceEventRels) {
            foreach($serviceEventRels as $value) {
                array_push($events, [
                        'EVENT_ID' => $value->getEvtEvent()->getId(),
                        'EVENT_NAME' => $value->getEvtEvent()->getName(),
                        'STRUCTURE' => $value->getEvtEvent()->getStructure()->getId()
                    ]);
            }
        }
        
        return array(
            "ID" => $this->getId(),
            "NAME"   => $this->getName(),
            "DECRIPTION"   => $this->getDecription(),
            "DECRIPTION1"   => $this->getDecription(),
            "STATUS"   => $this->getStatus(),
            "CANCELLATION_FINE"   => $this->getCancellationFine(),
            "CANCELLATION_TIME"   => $this->getCancellationTime(),
            "NRORG"   => $this->getNrorg(),
            "TIME"   => $this->getTime(),
            // "CREATED_BY"   => $this->getCreatedBy(),
            // "MODIFIED_BY"   => $this->getModifiedBy(),
            // "CREATED_AT"   => $this->getCreatedAt(),
            // "MODIFIED_AT"   => $this->getModifiedAt(),
            "EVENT_RELS"   => $serviceEventRels ? $events : "",
            "PARENT_ID" => $this->getParentId() ? $this->getParentId()->getId() : "",
            "PRICE" => $this->getPrice() ? $this->getPrice() : "",
            "COLOR" => $this->getColor(),
            "ICON" => $this->getIcon(),
            "IMAGE" => $this->getImage(),
            "TYPE" => $this->getType(),
            "STATUS_APP" => $this->getStatusapp(),
            "FUTURE_DAYS_TO_SCHEDULE" => $this->getFutureDaysToSchedule(),
            "USER_SCHEDULING_LIMIT" => $this->getUserSchedulingLimit(),
            "DEPEDENT_SCHEDULING_LIMIT" => $this->getDependentSchedulingLimit()
        );
    }
}