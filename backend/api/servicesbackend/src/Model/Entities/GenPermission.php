<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenPermission extends \Zeedhi\ApiServices\Model\Entities\Base\GenPermission {
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}