<?php
namespace Zeedhi\ApiServices\Model\Entities;


class OrdOrderProduct extends \Zeedhi\ApiServices\Model\Entities\Base\OrdOrderProduct {
    
    public function build($entityManager) {
        $this->setExtras($entityManager);
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['quantity'] = $this->getQuantity();
        $array['status'] = $this->getStatus();
        $array['note'] = $this->getNote();
        $array['total'] = $this->getTotal();
        
        if($this->getOrdMenuProduct()) {
            $array['name'] = $this->getOrdMenuProduct()->getName();
            $array['image'] = $this->getOrdMenuProduct()->getImage();
            $array['unitaryPrice'] = $this->getOrdMenuProduct()->getPrice();
            $array['productId'] = $this->getOrdMenuProduct()->getId();
            $array['extras'] = OrdItemExtra::manyToArray($this->getExtras());
        }
        
        if($this->getSerService()){
            $array['serviceId'] = $this->getSerService()->getId();
        }
        return $array;
    }
    
    public static function manyToArray($items) {
        $arrays = [];
        foreach ($items as $item) {
            $array = $item->toArray();
            array_push($arrays, $array);
        }
        return $arrays;
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    /* Devolve os extras dos itens enviados por parâmetro */
    function setExtras($entityManager) {
        $item   = 'Zeedhi\ApiServices\Model\Entities\OrdOrderProduct';
        $itemExtra = 'Zeedhi\ApiServices\Model\Entities\OrdItemExtra';
        $extra  = 'Zeedhi\ApiServices\Model\Entities\OrdExtra';
        $selectedOption = 'Zeedhi\ApiServices\Model\Entities\OrdSelectedOption';
        $option = 'Zeedhi\ApiServices\Model\Entities\OrdOption';
            
        $id = $this->getId();
            
        /* Realizando Query 
           o  = Pedido
           po = Item do Pedido
           e  = Extra
           ie = Extra do Item do Pedido
           op = Option
           so = Selected Option
        */
        $extras = $entityManager->createQuery(
            "
            SELECT ie
            FROM $itemExtra ie
            JOIN ie.ordExtra as e 
            JOIN ie.ordOrderProduct as i
            JOIN i.ordOrder as o
            WHERE i.id = $id
            ORDER BY e.id
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
            
        $this->extras = $extras;
    }

    
}