<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenConfiguration extends \Zeedhi\ApiServices\Model\Entities\Base\GenConfiguration {
    
    public function build($entityManager) {
        $this->setGateway($entityManager);
    }
    
    public function getGateway() {
        return property_exists($this, 'gateway') ? $this->gateway : NULL;
    }
    
    public function setGateway($entityManager) {
        $gatewayRel = $entityManager->getRepository(PayGatewayRel::class)->findOneBy(['genConfiguration' => $this->getId()]);
        if ($gatewayRel) $this->gateway = $gatewayRel->getPayGateway();
    }
    
    public function toArray() {
        return array(
            'LOGO_IMAGE' => $this->getLogoImage(),
            'LOGO_IMAGE_SMALL' => $this->getLogoImageSmall(),
            'PRIMARY_COLOR' => $this->getPrimaryColor(),
            'SECONDARY_COLOR' => $this->getSecondaryColor(),
            'YES_COLOR' => $this->getYesColor(),
            'NO_COLOR' => $this->getNoColor(),
            'GATEWAY' => $this->getGateway() ? $this->getGateway()->toArray() : NULL,
            'USE_INTEGRATION' => $this->getUseIntegration()
        );
    }
    
}