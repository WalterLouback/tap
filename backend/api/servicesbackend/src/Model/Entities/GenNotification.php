<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenNotification extends \Zeedhi\ApiServices\Model\Entities\Base\GenNotification {

    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'MESSAGE' => $this->getMessage(),
            'STATUS' => $this->getStatus(),
            'CONFIRMATION' => $this->getConfirmation(),
            'ORIGIN_NOTIFICATION' => $this->getOriginNotification(),
            'DATE_TIME' => $this->getDateTime()->format('Y-m-d')
            // 'USER_ID' => $this->getGenUser()->getId()
        );
    }
}
