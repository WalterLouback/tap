<?php

namespace Zeedhi\ApiServices\Model\Entities;

class SerServiceStructureRel extends \Zeedhi\ApiServices\Model\Entities\Base\SerServiceStructureRel {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "CREATED_BY" => $this->getCreatedBy(),
            "MODIFIED_BY" => $this->getModifiedBy(),
            "CREATED_AT" => $this->getCreatedAt(),
            "MODIFIED_AT" => $this->getModifiedAt(),
            "GEN_STRUCTURE" => $this->getGenStructure()->getId(),
            "SER_SERVICE" => $this->getSerService()->getId(),
            "STATUS" => $this->getStatus()
        );
    }
}