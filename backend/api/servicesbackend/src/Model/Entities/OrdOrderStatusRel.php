<?php
namespace Zeedhi\ApiServices\Model\Entities;


class OrdOrderStatusRel extends \Zeedhi\ApiServices\Model\Entities\Base\OrdOrderStatusRel {
    
    public function toArray() {
        $array = [];
        
        $array['id'] = $this->getId();
        $array['orderId'] = $this->getOrdOrder()->getOrderIdentifier();
        $array['status'] = $this->getGenStatus()->getLabelName();
        $array['createDate'] = $this->getCreateDate();
        $array['nrorg'] = $this->getNrorg();
        $array['total'] = $this->getTotal();
        
        return $array;
    }
    
}