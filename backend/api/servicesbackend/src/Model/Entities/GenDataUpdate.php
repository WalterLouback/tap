<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenDataUpdate extends \Zeedhi\ApiServices\Model\Entities\Base\GenDataUpdate {
    
    const DOCUMENT = 'DOCUMENT';
    
    public function build($entityManager) {
        $this->getGenUser()->build($entityManager, $this->getNrorg());
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'user' => $this->getGenUser()->toArray(),
            'updateStatus' => $this->getUpdateStatus(),
            'approvedBy' => $this->getApprovedBy(),
            'address' => $this->getGenAddress()->toArray(),
            'document' => $this->getDocument(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'cpf' => $this->getCpf(),
            'nrorg' => $this->getNrorg(),
            'createdAt' => $this->getCreatedAt()
        );
    }
    
    public static function manyToArray($dataUpdates) {
        $array = [];
        foreach ($dataUpdates as $du) {
            array_push($array, $du->toArray());
        }
        
        return $array;
    }
    
}