<?php

namespace Zeedhi\ApiServices\Model\Entities;


class SerTimesheetRel extends  \Zeedhi\ApiServices\Model\Entities\Base\SerTimesheetRel {

    public function toArrayId() {
        return array(
            "SER_TIMESHEET_ID" => $this->getSerTimesheet()->getId()
        );
    }
}