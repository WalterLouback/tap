<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenNews extends \Zeedhi\ApiServices\Model\Entities\Base\GenNews {
    
    public static function manyToArray($newsArray) {
        $arrays = [];
        foreach ($newsArray as $news) {
            array_push($arrays, $news->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'TITLE' => $this->getTitle(),
            'NEWS' => $this->getNews(),
            'NRORG' => $this->getNrorg(),
            'ACTIVE' => $this->getActive() == '1',
            'IMAGE' => $this->getImage(),
            'AUTHOR_ID' => $this->getGenUser() ? $this->getGenUser()->getId() : NULL
        );
    }

    
}