<?php

namespace Zeedhi\ApiServices\Model\Entities;

class GenScheduleUserRel extends \Zeedhi\ApiServices\Model\Entities\Base\GenScheduleUserRel {
    
    public function toArray($entityManager) {
        return array(
            "ID" => $this->getId(),            
            "NRORG" => $this->getNrorg(),
            "STATUS" => $this->getStatus(),
            "CLIENT_ID" => $this->getGenUser() ? $this->getGenUser()->getId() : NULL,
            "CLIENT_FULL_NAME" => $this->getGenUser() ? $this->getGenUser()->getFirstName() . " " . $this->getGenUser()->getLastName() : NULL,
            "EXTERNAL_ID" => $this->getGenUser() ? $this->getExternalId($entityManager, $this->getGenUser()->getId()) : NULL,
            "CLIENT_PHONE" => $this->getGenUser() ? $this->getContacts($entityManager, $this->getGenUser()->getId()) : NULL,
            "CLIENT_EMAIL" => $this->getGenUser() ? $this->getGenUser()->getEmail() : NULL,
            "SCHEDULE_ID" => $this->getGenSchedule() ? $this->getGenSchedule()->getId() : NULL,
            "LOCATION" => $this->getGenSchedule()->getGenStructure() ? $this->getGenSchedule()->getGenStructure()->getName() : NULL,
            "SCHEDULE_INITIAL_DATE" => $this->getGenSchedule()->getInitialDate() && $this->getGenSchedule()->getSerTimeTimesheet() ? $this->getGenSchedule()->getInitialDate()->format('Y-m-d') . " " . $this->getGenSchedule()->getSerTimeTimesheet()->getInitialTime() : NULL,
            "SERVICES" => $this->getGenSchedule() ? $this->getServices($entityManager, $this->getGenUser()->getId(), $this->getNrorg(), $this->getGenSchedule()->getId()) : [],
            "TOTAL_PRICE" => $this->getAmountServices($entityManager, $this->getGenUser()->getId(), $this->getNrorg(), $this->getGenSchedule()->getId())
        );
    }
    
    public function getContacts($entityManager, $userId) {
        $genContact = GenContact::class;
        
        $contact = $this->contacts = $entityManager->createQuery(
            "
            SELECT c.phone
            FROM  $genContact c
            WHERE c.genUser = $userId
            "
        )->getResult();
        return $contact ? $contact[0]["phone"] : "";
    }
    
    public function getExternalId($entityManager, $userId) {
        $genUserType = GenUserTypeRel::class;
        
        $externalId = $this->contacts = $entityManager->createQuery(
            "
            SELECT gut.externalId
            FROM  $genUserType gut
            WHERE gut.genUser = $userId
            "
        )->getResult();
        return $externalId ? $externalId[0]["externalId"] : "";
    }
    
    public function getServices($entityManager, $userId, $nrorg, $scheduleId) {
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $services = $entityManager->createQuery("
                SELECT ss.id, ss.name, ss.time, ss.price, ss.image, sss.id serviceSellerId, ees.id sellerId, 
                ss.cancellationTime, ss.cancellationFine, CONCAT(gu.firstName, ' ', gu.lastName) fullName 
                FROM $genScheduleUserRel gsur
                LEFT JOIN gsur.genSchedule gs WITH gsur.genSchedule = '$scheduleId'
                LEFT JOIN gs.serServiceSeller sss
                LEFT JOIN sss.evtEventSeller ees
                LEFT JOIN ees.genUser gu
                LEFT JOIN gs.serService ss
                WHERE gsur.genUser = '$userId' AND gsur.genSchedule = '$scheduleId'")->getResult();
        $amount = 0;
        $data   = [];
        foreach($services as $i => $service) {
            $data[$i] = array(
                            'ID' => $service['id'], 
                            'NAME' => $service['name'], 
                            'TIME' => $service['time'], 
                            'PRICE' => $service['price'], 
                            'IMAGE' => $service['image'],
                            'SELLER_ID' => $service['sellerId'], 
                            'CANCELLATION_TIME' => $service['cancellationTime'],
                            'CANCELLATION_FINE' => $service['cancellationFine'],
                            'PROFESSIONAL_ID' => $service['serviceSellerId'], 
                            'PROFESSIONAL' => $service['fullName']
                        );   
            $amount += $service['price'];
        }
        
        return $data;
    }
    
    public function getAmountServices($entityManager, $userId, $nrorg, $scheduleId) {
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $services = $entityManager->createQuery("
                SELECT ss.id, ss.name, ss.time, ss.price, ees.id sellerId,  CONCAT(gu.firstName, ' ', gu.lastName) fullName FROM $genScheduleUserRel gsur
                LEFT JOIN gsur.genSchedule gs WITH gsur.genSchedule = '$scheduleId'
                LEFT JOIN gs.serServiceSeller sss
                LEFT JOIN sss.evtEventSeller ees
                LEFT JOIN ees.genUser gu
                LEFT JOIN gs.serService ss
                WHERE gsur.genUser = '$userId' AND gsur.genSchedule = '$scheduleId'")->getResult();
        $amount = 0;
        foreach($services as $i => $service) {
            $data[$i] = array(
                'ID' => $service['id'], 
                'NAME' => $service['name'], 
                'TIME' => $service['time'], 
                'PRICE' => $service['price'], 
                'PROFESSIONAL_ID' => $service['sellerId'], 
                'PROFESSIONAL' => $service['fullName']
            );   
            $amount += $service['price'];
        }
        
        return $amount;
    }
}