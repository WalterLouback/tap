<?php
namespace Zeedhi\ApiServices\Model\Entities;


class OrdMenu extends \Zeedhi\ApiServices\Model\Entities\Base\OrdMenu {
    
    public function build($entityManager, $eventId=NULL) {
        $this->setProductGroups($entityManager);
        $this->setDaysOfWeek($eventId, $entityManager);
        $this->isCurrentWorkshift($eventId, $entityManager);
    }
    
    public function getProductGroups() {
        return property_exists($this, 'productGroups') ? $this->productGroups : [];
    }
    
    public function getDaysOfWeek() {
        return property_exists($this, 'daysOfWeek') ? $this->daysOfWeek : [];
    }

    public function setProductGroups($entityManager) {
        $productGroup  = 'Zeedhi\ApiServices\Model\Entities\OrdProductGroup';
        $menuId        = $this->getId();
        
        $productGroups = $entityManager->createQuery(
            "
            SELECT pg
            FROM $productGroup pg
            JOIN pg.ordMenu as m
            WHERE m.id = $menuId
            ORDER BY pg.name
            "
        )->getResult();
        
        foreach ($productGroups as $productGroup) {
            $productGroup->build($entityManager);
        }
        
        $this->productGroups = $productGroups;
    }

    public function getShiftsOfDay() {
        return property_exists($this, 'dayWorkshifts') ? $this->dayWorkshifts : [];
    }

    public function setShiftsOfDay($date, $eventId, $entityManager) {
        $workshift = 'Zeedhi\ApiServices\Model\Entities\GenShift';
        $menuId   = $this->getId();
        
        $dayWorkshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime, ws.day
            FROM $workshift ws
            JOIN ws.evtEventMenu em
            JOIN em.evtEvent e
            WHERE em.ordMenu = $menuId
            AND e.id = $eventId
            "
        )->getResult();

        $this->dayWorkshifts = $dayWorkshifts;
    }
    
    public function isCurrentWorkshift($eventId=NULL, $entityManager=NULL) {
        if (!$entityManager) {
            if (property_exists($this, 'isCurrentWorkshift')) return $this->isCurrentWorkshift;
            else return NULL;
        }
        
        $isCurrentWorkshift     = false;
        
        /* Obter horários de funcionamento da loja no dia de hoje */
        $today        = new \DateTime();
        $todayClone   = clone $today;
        
        /* Definindo os turnos do dia */
        $formatedDate = $todayClone->format('Y-m-d H:i:s');
        $this->setShiftsOfDay($formatedDate, $eventId, $entityManager);
        $shifts       = $this->getShiftsOfDay();
        
        /* Adicionando fuso horário */
        $today->sub(new \DateInterval('PT2H'));
        
        /* Percorrer os turnos e verificar se horário atual se encaixa em algum */
        foreach ($shifts as $shift) {
            /* Obtendo horário atual */
            $currHours = $today->format('H');
            $currMins  = $today->format('i');
            /* Obtendo horário de início do turno */
            $inDate    = clone $shift['initialTime'];
            $inDate->sub(new \DateInterval('PT2H'));
            $inHours   = $inDate->format('H');
            $inMins    = $inDate->format('i');
            /* Obtendo horário de término do turno */
            $finDate   = clone $shift['finalTime'];
            $finDate->sub(new \DateInterval('PT2H'));
            $finHours  = $finDate->format('H');
            $finMins   = $finDate->format('i');
            
            /* Verificando se horário atual se encaixa no turno */
            if ($currHours > $inHours || ($currHours == $inHours && $currMins >= $inMins)) {
                if ($currHours < $finHours || ($currHours == $finHours && $currMins <= $finMins)) {
                    $isCurrentWorkshift = true;
                    break;
                }
            }
        }
        
        $this->isCurrentWorkshift = $isCurrentWorkshift;
        
        return $isCurrentWorkshift;
    }
    
    public function workshiftIsAvailable($eventId, $day, $initialDate, $finalDate, $entityManager) {
        /* Get the workshifts of the other menus of this day */
        $menuId     = $this->getId();
        $workshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime, ws.day
            FROM 'Zeedhi\ApiServices\Model\Entities\GenShift' ws
            JOIN ws.evtEventMenu em
            JOIN em.evtEvent e
            WHERE em.ordMenu <> $menuId
            AND e.id = $eventId
            AND ws.day = $day
            "
        )->getResult();
        
        /* Subtract the timezone from the dates */
        $iDClone      = clone $initialDate;
        $fDClone      = clone $finalDate;
        
        $initialDate  = $iDClone->sub(new \DateInterval('PT2H'));
        $finalDate    = $fDClone->sub(new \DateInterval('PT2H'));
        
        if ($initialDate >= $finalDate) {
            throw new \Exception('Final date must be higher than initial date', 8);
        }
        
        /* Iterate through the shifts and check if there are collisions */
        foreach ($workshifts as $shift) {
            $sInitialDate = $shift['initialTime']->sub(new \DateInterval('PT2H'));
            $sFinalDate   = $shift['finalTime']->sub(new \DateInterval('PT2H'));
            
            if ($initialDate >= $sInitialDate && $initialDate <= $sFinalDate) {
                throw new \Exception('Workshift unavailable', 9);
            }
            else if ($sInitialDate >= $initialDate && $sInitialDate <= $finalDate) {
                throw new \Exception('Workshift unavailable', 9);
            }
        }
    }
    
    public function setDaysOfWeek($eventId, $entityManager) {
        $eventMenu  = $entityManager->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $eventId, 'ordMenu' => $this->getId()]);
            if ($eventMenu != NULL) {
            $shifts     = $entityManager->getRepository(GenShift::class)->findBy(['evtEventMenu' => $eventMenu->getId()]);
            $daysOfWeek = [];
            
            foreach ($shifts as $shift) {
                $dayOfWeek = $shift->getDay();
                if (!in_array($dayOfWeek, $daysOfWeek)) {
                    array_push($daysOfWeek, $dayOfWeek);
                }
            }
            
            $this->daysOfWeek = $daysOfWeek;
        }
    }
    
    public function cloneProductGroups($targetMenu, $entityManager) {
        foreach ($this->getProductGroups() as $pg) {
            $newPg = $pg->getClone($entityManager);
            $newPg->setOrdMenu($targetMenu);
            
            foreach ($pg->getProducts() as $product) {
                $product->getClone($entityManager, $newPg);
            }
        }
    }
    
    public function toArray() {
        $array = [];
        $array['id']  = $this->getId();
        $array['name']  = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['isCurrentWorkshift'] = $this->isCurrentWorkshift();
        $array['daysOfWeek'] = $this->getDaysOfWeek();
        $array['shiftsOfDay'] = $this->getShiftsOfDay();
        $array['productGroups'] = OrdProductGroup::manyToArray($this->getProductGroups());

        return $array;
    }
    
    public static function manyToArray($menus) {
        $arrays = [];
        foreach ($menus as $menu) {
            array_push($arrays, $menu->toArray());
        }
        return $arrays;
    }
    
}