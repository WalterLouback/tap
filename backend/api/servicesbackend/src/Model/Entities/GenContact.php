<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenContact extends \Zeedhi\ApiServices\Model\Entities\Base\GenContact {
    
    const CONTACT_METHOD_ID = 'CONTACT_METHOD_ID';
    
    const CONTACT_TYPE = 'TYPE';
    const CONTACT_TYPE_MOBILE = 'MOBILE';
    
    const PHONE_NUMBER = 'PHONE_NUMBER';
    
    /**
     * maskEmail
     * Masks an email address in the format: l***o@teknisa.com
     * 
     */
    public static function maskEmail($email) {
        $leftSide    = explode('@', $email)[0];
        $rightSide   = explode('@', $email)[1];
        
        $maskedLeftSide  = substr($leftSide, 0, 1) . '***' . substr($leftSide, count($leftSide) - 2, 1);
        
        return $maskedLeftSide . '@' . $rightSide;
    }
    
    /**
     * maskPhone
     * Masks a phone number in the format: +5531*****1902
     * 
     */
    public static function maskPhone($phone) {
        return substr($phone, 0, 5) . '*****' . substr($phone, count($phone) - 4 - 1, 4);
    }
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "TYPE" => $this->getType(),
            "PHONE" => $this->getPhone()
        );
    }
    
    public static function manyToArray($contacts) {
        $arrays = [];
        if ($contacts != NULL) {
            foreach ($contacts as $contact) {
                array_push($arrays, $contact->toArray());
            }
        }
        return $arrays;
    }
    
}