<?php
namespace Zeedhi\ApiServices\Model\Entities;

class GenUser extends \Zeedhi\ApiServices\Model\Entities\Base\GenUser {

    const USER_ID  = 'USER_ID';
    const FIRST_NAME = 'FIRST_NAME';
    const LAST_NAME = 'LAST_NAME';
    const CPF      = 'CPF';
    const EMAIL    = 'EMAIL';
    const PASSWORD = 'PASSWORD';
    const TEMP_PASSWORD = 'TEMP_PASSWORD';
    const NEW_PASSWORD = 'NEW_PASSWORD';
    
    const FB_ID = 'FB_ID';    
    const FB_TOKEN = 'FB_TOKEN';
    
    const AUTH_TYPE = 'AUTH_TYPE';
    const AUTH_CODE = 'AUTH_CODE';
    
    const AUTH_TYPE_PHONE = 'PHONE';
    
    const IMAGE = 'IMAGE';
    
    public function build($entityManager, $userTypeId=NULL, $nrorg=NULL) {
        $this->setCreditCards($entityManager);
        $this->setUserProfiles($entityManager, $nrorg);
        $this->setContacts($entityManager);
        $this->setAssociatedOrganizations($entityManager);
        $this->setTags($entityManager);
        $this->setAddress($entityManager, $nrorg);
        $this->setDependentOf($entityManager, $nrorg);
        $this->setDependencyStatus($entityManager, $nrorg);
    }
    
    public function getCreditCards() {
        return property_exists($this, 'creditCards') ? $this->creditCards : [];
    }
    
    public function getUserProfiles($nrorg=NULL) {
        if ($nrorg == NULL) {
            return property_exists($this, 'userProfiles') ? $this->userProfiles : [];
        } else {
            $profilesFromOrg = [];
            foreach ($this->userProfiles as $profile) {
                if ($profile->getNrorg() == $nrorg && $profile->getStatus() == 'A') array_push($profilesFromOrg, $profile);
            }
            return $profilesFromOrg;
        }
    }
    
    public function getUserProfile($userTypeId, $nrorg) {
        if (property_exists($this, 'userProfiles')) {
            if ($userTypeId) {
                foreach ($this->userProfiles as $userProfile) {
                    if ($userProfile->getNrorg() == $nrorg && $userProfile->getGenUserType()->getId() == $userTypeId) {
                        return $userProfile;
                    }
                }
            }
        }
        throw new \Exception('User does not have the requested user profile.', 14);
    }
    
    public function getTags() {
        return property_exists($this, 'tags') ? $this->tags : [];
    }
    
    public function getAddress() {
        return property_exists($this, 'address') ? $this->address : [];
    }
    
    public function getContacts() {
        return property_exists($this, 'contacts') ? $this->contacts : [];
    }
    
    public function getAssociatedOrganizations() {
        return property_exists($this, 'associatedOrganizations') ? $this->associatedOrganizations : [];
    }
    
    public function getDependentOf() {
        return property_exists($this, 'dependentOf') ? $this->dependentOf : NULL;
    }
    
    public function getDependencyStatus() {
        return property_exists($this, 'dependencyStatus') ? $this->dependencyStatus : NULL;
    }
    
    public function setCreditCards($entityManager) {
        $creditCards = $entityManager->getRepository(PayCreditcard::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
        $this->creditCards = $creditCards;
    }
    
    public function setUserProfiles($entityManager, $nrorg) {
        $userProfiles = $entityManager->getRepository(GenUserTypeRel::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
        foreach ($userProfiles as $userProfile) {
            $userProfile->build($entityManager, $nrorg);
        }
        $this->userProfiles = $userProfiles;
    }
    
    public function setDependentOf($entityManager, $nrorg) {
        $dependentId = $this->getId();
        
        if ($nrorg != NULL) {
            $dependentOf = $entityManager->createQuery(
                "
                SELECT pu
                FROM 'Zeedhi\ApiServices\Model\Entities\GenUser' pu
                JOIN 'Zeedhi\ApiServices\Model\Entities\GenDependent' d WITH d.parent = pu
                JOIN 'Zeedhi\ApiServices\Model\Entities\GenUser' pr WITH d.dependent = pr
                WHERE pr.id = $dependentId
                AND d.id > 0
                "
            )->getResult();
            if (count($dependentOf) > 0) {
                $dependentOf[0]->setCreditcards($entityManager);
                $this->dependentOf = $dependentOf[0];
            }
        }
    }
    
    public function setDependencyStatus($entityManager, $nrorg) {
        $dependentId = $this->getId();
        
        if ($nrorg != NULL) {
            $dependencyData = $entityManager->createQuery(
                "
                SELECT d
                FROM 'Zeedhi\ApiServices\Model\Entities\GenDependent' d 
                WHERE d.dependent = $dependentId
                AND d.nrorg = $nrorg
                "
            )->getResult();
            
            if (count($dependencyData) > 0) {
                $dependencyData[0]->build($entityManager, $nrorg);
                $this->dependencyStatus = $dependencyData[0]->getStatus();
            } else $this->dependencyStatus = "A";
        }
    }
    
    public function setTags($entityManager) {
        $id = $this->getId();
        $tags = $entityManager->createQuery(
            "
            SELECT t
            FROM 'Zeedhi\ApiServices\Model\Entities\GenTag' t
            JOIN 'Zeedhi\ApiServices\Model\Entities\EvtTagUser' tu WITH t = tu.genTag
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenUser' u WITH u = tu.genUser
            WHERE tu.genUser = $id
            "
        )->getResult();
        $this->tags = $tags;
    }
    
    public function setAddress($entityManager, $nrorg) {
        $userId = $this->getId();
        if ($nrorg != NULL) {
            $address = $entityManager->createQuery(
                "
                SELECT a
                FROM 'Zeedhi\ApiServices\Model\Entities\GenAddress' a
                WHERE a.genUser = $userId
                AND a.status = 'A'
                AND a.nrorg = $nrorg
                "
            )->getResult();
            if (count($address) > 0) $this->address = $address;
        }
    }
    
    public function setContacts($entityManager) {
        $userId   = $this->getId();
        $this->contacts = $entityManager->createQuery(
            "
            SELECT c
            FROM 'Zeedhi\ApiServices\Model\Entities\GenContact' c
            WHERE c.genUser = $userId
            "
        )->getResult();
    }
    
    public function setAssociatedOrganizations($entityManager, $userTypeId=null) {
        $userId = $this->getId();
        $userTypeQuery = $userTypeId != null ? "AND p.genUserType = $userTypeId" : "";
        $associatedOrganizations = $entityManager->createQuery(
            "
            SELECT DISTINCT o.nrorg, o.name
            FROM 'Zeedhi\ApiServices\Model\Entities\GenOrganization' o
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenUserTypeRel' p WITH p.nrorg = o.nrorg
            WHERE p.genUser = $userId
            AND p.status = 'A'
            $userTypeQuery
            "
        )->getResult();
        $this->associatedOrganizations = $associatedOrganizations;
    }
    
    public static function manyToArray($users, $userTypeId=NULL, $nrorg=NULL) {
        $arrays = [];
        foreach ($users as $user) {
            array_push($arrays, $user->toArray($userTypeId, $nrorg));
        }
        return $arrays;
    }
    
    public function hasAccessToApp($nrorg) {
        foreach ($this->getUserProfiles($nrorg) as $profile) {
            if ($profile->getGenUserType()->getId() == 1) return true;
        }
        return false;
    }
    
    public function hasAccessToAdmin($nrorg) {
        foreach ($this->getUserProfiles($nrorg) as $profile) {
            if ($profile->getGenUserType()->getId() == 4) return true;
        }
        return false;
    }
    
    public function toArray($userTypeId=NULL, $nrorg=NULL) {
        return array(
            'ID' => $this->getId(),
            'CPF' => $this->getCpf(),
            'FIRST_NAME' => $this->getFirstName(),
            'LAST_NAME' => $this->getLastName(),
            'FULL_NAME' => $this->getFirstName() ." ". $this->getLastName(),
            'ADDRESS' => $this->getAddress() != NULL ? GenAddress::manyToArray($this->getAddress()) : NULL,
            'CONTACTS' => GenContact::manyToArray($this->getContacts()),
            'EMAIL' => $this->getEmail(),
            'IMAGE' => $this->getImage(),
            'DEPENDENCY_STATUS' => $this->getDependencyStatus(),
            'DEPENDENT_OF' => $nrorg != NULL && $this->getDependentOf() ? $this->getDependentOf()->toArray() : NULL,
            'ORGANIZATION_DATA' => $userTypeId && $nrorg != NULL && $this->getUserProfile($userTypeId, $nrorg) != NULL ? $this->getUserProfile($userTypeId, $nrorg)->toArray() : NULL,
            'USER_PROFILES' => $nrorg != NULL ? GenUserTypeRel::manyToArray($this->getUserProfiles($nrorg)) : NULL,
            'MAIN_CARD_ID' => $this->getMainCreditcard() != NULL ? $this->getMainCreditcard()->getId() : NULL,
            'HAS_ACCESS_TO_APP' => $nrorg != NULL && $this->hasAccessToApp($nrorg) ? "true": "false",
            'HAS_ACCESS_TO_ADMIN' => $nrorg != NULL && $this->hasAccessToAdmin($nrorg) ? "true": "false"
        );
    }
    
}