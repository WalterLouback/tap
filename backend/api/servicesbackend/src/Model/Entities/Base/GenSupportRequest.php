<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenSupportRequest {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $status;
    /** @var \Datetime  */
    protected $initialTime;
    /** @var \Datetime  */
    protected $finalTime;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $createdBy;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUser  */
    protected $genUserEmployee;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenSubjectMatter  */
    protected $genSubjectMatter;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenActivityRequest  */
    protected $genActivityRequest;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getInitialTime() {
        return $this->initialTime;
    }
	public function setInitialTime(\Datetime $initialTime = NULL) {
        $this->initialTime = $initialTime;
    }
    public function getFinalTime() {
        return $this->finalTime;
    }
	public function setFinalTime(\Datetime $finalTime = NULL) {
        $this->finalTime = $finalTime;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiServices\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
    public function getGenUserEmployee() {
        return $this->genUserEmployee;
    }
	public function setGenUserEmployee(\Zeedhi\ApiServices\Model\Entities\GenUser $genUserEmployee = NULL) {
        $this->genUserEmployee = $genUserEmployee;
    }
    public function getGenSubjectMatter() {
        return $this->genSubjectMatter;
    }
	public function setGenSubjectMatter(\Zeedhi\ApiServices\Model\Entities\GenSubjectMatter $genSubjectMatter = NULL) {
        $this->genSubjectMatter = $genSubjectMatter;
    }
    public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiServices\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
    public function getGenActivityRequest() {
        return $this->genActivityRequest;
    }
	public function setGenActivityRequest(\Zeedhi\ApiServices\Model\Entities\GenActivityRequest $genActivityRequest = NULL) {
        $this->genActivityRequest = $genActivityRequest;
	}
}