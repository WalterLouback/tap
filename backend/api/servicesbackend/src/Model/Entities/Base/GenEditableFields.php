<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenEditableFields {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $fieldName;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUserType  */
    protected $genUserType;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenPermission  */
    protected $genPermission;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getFieldName() {
        return $this->fieldName;
    }
	public function setFieldName($fieldName) {
        $this->fieldName = $fieldName;
    }
	public function getGenUserType() {
        return $this->genUserType;
    }
	public function setGenUserType(\Zeedhi\ApiServices\Model\Entities\GenUserType $genUserType = NULL) {
        $this->genUserType = $genUserType;
    }
	public function getGenPermission() {
        return $this->genPermission;
    }
	public function setGenPermission(\Zeedhi\ApiServices\Model\Entities\GenPermission $genPermission = NULL) {
        $this->genPermission = $genPermission;
    }
}