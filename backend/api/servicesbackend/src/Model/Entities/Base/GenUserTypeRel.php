<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenUserTypeRel {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\Apervices\Model\Entities\GenUser  */
    protected $genUser;
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $externalId = 0;
    /** @var \Zeedhi\Apervices\Model\Entities\GenUserType  */
    protected $genUserType;
    
    protected $status;

    /** @var \Datetime  */
     protected $createdAt;
     /** @var int  */
     protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
     protected $modifiedBy;

    public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiServices\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getExternalId() {
        return $this->externalId;
    }
	public function setExternalId($externalId) {
        $this->externalId = $externalId;
    }
	public function getGenUserType() {
        return $this->genUserType;
    }
	public function setGenUserType(\Zeedhi\ApiServices\Model\Entities\GenUserType $genUserType = NULL) {
        $this->genUserType = $genUserType;
    }
    
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
}