<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerTimesheet
{
    protected $id;
    
    protected $name;

    protected $time;

    protected $initialTime;

    protected $finalTime;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function getInitialTime()
    {
        return $this->initialTime;
    }

    public function setInitialTime($initialTime)
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime()
    {
        return $this->finalTime;
    }

    public function setFinalTime($finalTime)
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $dt = new \DateTime();
        $this->modifiedAt = $dt->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}