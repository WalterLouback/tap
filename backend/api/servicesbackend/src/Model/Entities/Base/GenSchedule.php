<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class GenSchedule
{
    protected $id;
    
    protected $serService;
    
    protected $serTimeTimesheet;
    
    protected $serServiceSeller;
    
    protected $genStructure;
    
    protected $initialDate;
    
    protected $finalDate;
    
    protected $type;
    
    protected $appointment;
    
    protected $nrorg;
    
    protected $createdBy;

    protected $modifiedBy;

    protected $createdAt;

    protected $modifiedAt;
    
    protected $status;
    
    protected $genSchedule;

    public function getNrorg() {
        return $this->nrorg;
    }

    public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy() {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
        return $this;
    }

    public function getModifiedAt() {
        return $this->modifiedAt;
    }

    public function setModifiedAt($modifiedAt) {
        $this->modifiedAt = new \DateTime();
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function getInitialDate() {
        return $this->initialDate;
    }

    public function setInitialDate($initialDate) {
        $this->initialDate = $initialDate;

        return $this;
    }

    public function getFinalDate() {
        return $this->finalDate;
    }

    public function setFinalDate($finalDate) {
        $this->finalDate = $finalDate;

        return $this;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    public function getAppointment() {
        return $this->appointment;
    }

    public function setAppointment($appointment) {
        $this->appointment = $appointment;

        return $this;
    }

    public function getSerService() {
        return $this->serService;
    }

    public function setSerService(\Zeedhi\ApiServices\Model\Entities\SerService $serService = NULL) {
        $this->serService = $serService;

        return $this;
    }

    public function getSerServiceSeller() {
        return $this->serServiceSeller;
    }

    public function setSerServiceSeller(\Zeedhi\ApiServices\Model\Entities\SerServiceSellerRel $serServiceSeller = NULL) {
        $this->serServiceSeller = $serServiceSeller;

        return $this;
    }
    
    public function getSerTimeTimesheet() {
        return $this->serTimeTimesheet;
    }

    public function setSerTimeTimesheet(\Zeedhi\ApiServices\Model\Entities\SerTimeTimesheet $serTimeTimesheet = NULL) {
        $this->serTimeTimesheet = $serTimeTimesheet;

        return $this;
    }
    
    public function getGenStructure() {
        return $this->genStructure;
    }

    public function setGenStructure(\Zeedhi\ApiServices\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;

        return $this;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
	public function setStatus($status = NULL) {
        $this->status = $status;
    }

    public function getGenSchedule() {
        return $this->genSchedule;
    }
    public function setGenSchedule(\Zeedhi\ApiServices\Model\Entities\GenSchedule $genSchedule = NULL) {
        $this->genSchedule = $genSchedule;
    }
}
