<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenStatusUserType {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenStatus  */
    protected $genStatus;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUserType  */
    protected $genUserType;
    /** @var \Zeedhi\ApiServices\Model\Entities\EvtSellerType  */
    protected $evtSellerType;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getGenStatus() {
        return $this->genStatus;
    }
    public function setGenStatus(\Zeedhi\ApiServices\Model\Entities\GenStatus $genStatus = NULL) {
        $this->genStatus = $genStatus;
    }
    public function getGenUserType() {
        return $this->genUserType;
    }
    public function setGenUserType(\Zeedhi\ApiServices\Model\Entities\GenUserType $genUserType = NULL) {
        $this->genUserType = $genUserType;
    }
    public function getEvtSellerType() {
        return $this->evtSellerType;
    }
    public function setEvtSellerType(\Zeedhi\ApiServices\Model\Entities\EvtSellerType $evtSellerType = NULL) {
        $this->evtSellerType = $evtSellerType;
    }
}