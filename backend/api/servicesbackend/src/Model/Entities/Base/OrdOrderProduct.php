<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class OrdOrderProduct {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $total;
    /** @var int  */
    protected $quantity;
    /** @var \Zeedhi\ApiServices\Model\Entities\OrdMenuProduct  */
    protected $ordMenuProduct;
    /** @var \Zeedhi\ApiServices\Model\Entities\OrdOrder  */
    protected $ordOrder;
    /** @var \Zeedhi\ApiServices\Model\Entities\EvtEventTicket  */
    protected $evtEventTicket;
    /** @var string */
    protected $note;
    /** @var \Zeedhi\ApiServices\Model\Entities\SerService  */
    protected $serService;
    /** @var string */
    protected $status;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
	public function getNote() {
        return $this->note;
    }
    public function setNote($note = NULL) {
        $this->note = $note;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getQuantity() {
        return $this->quantity;
    }
	public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
	public function getOrdMenuProduct() {
        return $this->ordMenuProduct;
    }
	public function setOrdMenuProduct(\Zeedhi\ApiServices\Model\Entities\OrdMenuProduct $ordMenuProduct = NULL) {
        $this->ordMenuProduct = $ordMenuProduct;
    }
	public function getOrdOrder() {
        return $this->ordOrder;
    }
	public function setOrdOrder(\Zeedhi\ApiServices\Model\Entities\OrdOrder $ordOrder = NULL) {
        $this->ordOrder = $ordOrder;
    }
    public function getSerService() {
        return $this->serService;
    }
	public function setSerService(\Zeedhi\ApiServices\Model\Entities\SerService $serService = NULL) {
        $this->serService = $serService;
	}
    public function getEvtEventTicket() {
        return $this->evtEventTicket;
    }
	public function setEvtEventTicket(\Zeedhi\ApiServices\Model\Entities\EvtEventTicket $evtEventTicket = NULL) {
        $this->evtEventTicket = $evtEventTicket;
	}
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
}