<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenSubjectMatter {

    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $subjectMatter;
    /** @var string  */
    protected $emailResponsible;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $status;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;

	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getSubjectMatter() {
        return $this->subjectMatter;
    }
	public function setSubjectMatter($subjectMatter = NULL) {
        $this->subjectMatter = $subjectMatter;
    }
	public function getEmailResponsible() {
        return $this->emailResponsible;
    }
	public function setEmailResponsible($emailResponsible = NULL) {
        $this->emailResponsible = $emailResponsible;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
}