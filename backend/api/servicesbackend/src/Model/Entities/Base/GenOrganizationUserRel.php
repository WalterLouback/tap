<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class GenOrganizationUserRel {

    protected $id;

    protected $nrorg;

    protected $status;

    protected $genUser;
    
    protected $firebaseToken;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    public function getNrorg() {
        return $this->nrorg;
    }

    public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status = NULL) {
        $this->status = $status;
    }

    public function getFirebaseToken() {
        return $this->firebaseToken;
    }

    public function setFirebaseToken($firebaseToken = NULL) {
        $this->firebaseToken = $firebaseToken;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }

    public function getModifiedAt() {
        return $this->modifiedAt;
    }

    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy = NULL) {
        $this->createdBy = $createdBy;
    }

    public function getModifiedBy() {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy = NULL) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }

    public function getGenUser() {
        return $this->genUser;
    }

    public function setGenUser(\Zeedhi\ApiServices\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
}
