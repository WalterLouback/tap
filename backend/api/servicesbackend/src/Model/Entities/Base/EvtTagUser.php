<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class EvtTagUser {
    
    
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUser  */
    protected $genUser;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenTag  */
    protected $genTag;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiServices\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenTag() {
        return $this->genTag;
    }
	public function setGenTag(\Zeedhi\ApiServices\Model\Entities\GenTag $genTag = NULL) {
        $this->genTag = $genTag;
    }
}