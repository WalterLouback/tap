<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerTimesheetRel
{
    protected $id;
    
    protected $serServiceSeller;
    
    protected $serTimesheet;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;
    
    protected $evtEvent;
    
    protected $serService;
    
    protected $ordConfigStoreDeliversId;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $dt = new \DateTime();
        $this->modifiedAt = $dt->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSerServiceSeller()
    {
        return $this->serServiceSeller;
    }

    public function setSerServiceSeller(\Zeedhi\ApiServices\Model\Entities\SerServiceSellerRel $serServiceSeller)
    {
        $this->serServiceSeller = $serServiceSeller;

        return $this;
    }

    public function getSerTimesheet()
    {
        return $this->serTimesheet;
    }

    public function setSerTimesheet(\Zeedhi\ApiServices\Model\Entities\SerTimesheet $serTimesheet)
    {
        $this->serTimesheet = $serTimesheet;

        return $this;
    }
    
    public function getEvtEvent()
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent)
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }
    public function getSerService()
    {
        return $this->serService;
    }

    public function setSerService(\Zeedhi\ApiServices\Model\Entities\SerService $serService)
    {
        $this->serService = $serService;

        return $this;
    }

    public function getOrdConfigStoreDeliversId()
    {
        return $this->ordConfigStoreDeliversId;
    }

    public function setOrdConfigStoreDeliversId($ordConfigStoreDeliversId)
    {
        $this->ordConfigStoreDeliversId = $ordConfigStoreDeliversId;

        return $this;
    }    
    
}