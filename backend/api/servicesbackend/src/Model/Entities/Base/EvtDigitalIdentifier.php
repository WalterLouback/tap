<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class EvtDigitalIdentifier {
    
    
    /** @var \Datetime  */
    protected $createDate;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $qrCode;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getCreateDate() {
        return $this->createDate;
    }
	public function setCreateDate(\Datetime $createDate = NULL) {
        $this->createDate = $createDate;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getQrCode() {
        return $this->qrCode;
    }
	public function setQrCode($qrCode) {
        $this->qrCode = $qrCode;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiServices\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}