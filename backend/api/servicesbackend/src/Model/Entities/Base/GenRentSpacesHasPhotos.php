<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenRentSpacesHasPhotos {
    
    /** @var int  */
    protected $id;
    /** @var string  */
    protected $_link;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenRentSpaces  */
    protected $rentSpaces;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
    public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getLink() {
        return $this->_link;
    }
	public function setLink($_link = NULL) {
        $this->_link = $_link;
    }
	public function getRentSpaces() {
        return $this->rentSpaces;
    }
	public function setRentSpaces(\Zeedhi\ApiServices\Model\Entities\GenRentSpaces $genRentSpaces = NULL) {
        $this->rentSpaces = $genRentSpaces;
    }
}