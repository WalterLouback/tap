<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;


class OrdOrderStatusRel
{
    protected $id;
    
    protected $createDate;
    
    protected $createdAt;
    
    protected $modifiedAt;
    
    protected $createdBy;
    
    protected $modifiedBy;
    /*Id da organização*/
    protected $nrorg;
    /*\Zeedhi\ApiServices\Model\Entities\GenStatus FOREIGN KEY*/
    protected $genStatus;
    /*\Zeedhi\ApiServices\Model\Entities\OrdOrder FOREIGN KEY*/
    protected $ordOrder;
    /*total da Order*/
    protected $total;
    /*Id da transação de pagamento*/
    protected $transactionId;
    /*tipo de registro: PAYMENT OR OUTHERS*/
    protected $type;
    
    
    public function getCreateDate() {
        return $this->createDate;
    }

    public function setCreateDate($createDate = NULL) {
        $this->createDate = $createDate;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    
    public function getModifiedAt() {
        return $this->modifiedAt;
    }
    
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy = NULL) {
        $this->createdBy = $createdBy;
    }

    public function getModifiedBy() {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy = NULL) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getNrorg() {
        return $this->nrorg;
    }

    public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }

    public function getId() {
        return $this->id;
    }

    public function getGenStatus() {
        return $this->genStatus;
    }

    public function setGenStatus(\Zeedhi\ApiServices\Model\Entities\GenStatus $genStatus = NULL) {
        $this->genStatus = $genStatus;
    }

    public function getOrdOrder() {
        return $this->ordOrder;
    }

    public function setOrdOrder(\Zeedhi\ApiServices\Model\Entities\OrdOrder $ordOrder = NULL) {
        $this->ordOrder = $ordOrder;
    }
    
    public function getTotal()  {
        return $this->total;
    }
    
    public function setTotal($total = NULL) {
        $this->total = $total;
    }
    
    public function getTransactionId() {
        return $this->transactionId;
    }

    public function setTransactionId($transactionId = NULL) {
        $this->transactionId = $transactionId;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type = NULL) {
        $this->type = $type;
    }
}