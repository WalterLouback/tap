<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerServiceStructureRel
{
    protected $genStructure;
    
    protected $serService;
    
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;
    
    protected $status;

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime();

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGenStructure()
    {
        return $this->genStructure;
    }

    public function setGenStructure(\Zeedhi\ApiGeneral\Model\Entities\GenStructure $genStructure = NULL)
    {
        $this->genStructure = $genStructure;

        return $this;
    }

    public function getSerService()
    {
        return $this->serService;
    }

    public function setSerService(\Zeedhi\ApiServices\Model\Entities\SerService $serService = NULL)
    {
        $this->serService = $serService;

        return $this;
    }
    
    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
