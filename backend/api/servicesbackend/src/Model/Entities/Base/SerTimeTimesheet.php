<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerTimeTimesheet
{
    protected $id;
    
    protected $initialTime;

    protected $finalTime;
    
    protected $serDayTimesheetValues;
    
    protected $serTimesheet;
    
    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $type;

    protected $maxScheduling;
    
    public function getInitialTime()
    {
        return $this->initialTime;
    }

    public function setInitialTime($initialTime)
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime()
    {
        return $this->finalTime;
    }

    public function setFinalTime($finalTime)
    {
        $this->finalTime = $finalTime;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $dt = new \DateTime();
        $this->modifiedAt = $dt->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSerTimesheet()
    {
        return $this->serTimesheet;
    }

    public function setSerTimesheet(\Zeedhi\ApiServices\Model\Entities\SerTimesheet $serTimesheet)
    {
        $this->serTimesheet = $serTimesheet;

        return $this;
    }
    
    public function getSerDayTimesheetValues()
    {
        return $this->serDayTimesheetValues;
    }

    public function setSerDayTimesheetValues($serDayTimesheetValues)
    {
        $this->serDayTimesheetValues = $serDayTimesheetValues;

        return $this;
    }
    
    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
    
    public function getMaxScheduling()
    {
        return $this->maxScheduling;
    }

    public function setMaxScheduling($maxScheduling)
    {
        $this->maxScheduling = $maxScheduling;

        return $this;
    }
}