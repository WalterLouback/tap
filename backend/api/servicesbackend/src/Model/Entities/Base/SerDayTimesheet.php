<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerDayTimesheet
{
    protected $id;
    
    protected $name;

    protected $values;

    protected $status;

    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        $this->values = $values;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $dt = new \DateTime();
        $this->modifiedAt = $dt->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}