<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class OrdItemExtra {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiServices\Model\Entities\OrdOrderProduct  */
    protected $ordOrderProduct;
    /** @var \Zeedhi\ApiServices\Model\Entities\OrdExtra  */
    protected $ordExtra;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdOrderProduct() {
        return $this->ordOrderProduct;
    }
	public function setOrdOrderProduct(\Zeedhi\ApiServices\Model\Entities\OrdOrderProduct $ordOrderProduct = NULL) {
        $this->ordOrderProduct = $ordOrderProduct;
    }
	public function getOrdExtra() {
        return $this->ordExtra;
    }
	public function setOrdExtra(\Zeedhi\ApiServices\Model\Entities\OrdExtra $ordExtra = NULL) {
        $this->ordExtra = $ordExtra;
    }
}