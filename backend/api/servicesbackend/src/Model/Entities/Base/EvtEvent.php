<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class EvtEvent {
    
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $imageMap;
    /** @var string  */
    protected $imageLogo;
    /** @var string  */
    protected $imageCover;
    /** @var \Datetime  */
    protected $initialDate;
    /** @var \Datetime  */
    protected $finalDate;
    /** @var string  */
    protected $location;
    /** @var string  */
    protected $salesMethod;
    /** @var string  */
    protected $rating;
    /** @var string  */
    protected $classification;
    /** @var string  */
    protected $about;
    /** @var \Zeedhi\ApiServices\Model\Entities\EvtEvent  */
    protected $parentEvent;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenUser  */
    protected $ownerUser;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenCategory  */
    protected $genCategory;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $externalId;
    /** @var string  */
    protected $values;
    
	public function getValues() {
        return $this->values;
    }
    public function setValues($values = NULL) {
        $this->values = $values;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getOwnerUser() {
        return $this->ownerUser;
    }
	public function setOwnerUser(\Zeedhi\ApiServices\Model\Entities\GenUser $ownerUser = NULL) {
        $this->ownerUser = $ownerUser;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getStructure() {
        return $this->genStructure;
    }
	public function setStructure(\Zeedhi\ApiServices\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getCategory() {
        return $this->genCategory;
    }
	public function setCategory(\Zeedhi\ApiServices\Model\Entities\GenCategory $genCategory = NULL) {
        $this->genCategory = $genCategory;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getImageMap() {
        return $this->imageMap;
    }
	public function setImageMap($imageMap = NULL) {
        $this->imageMap = $imageMap;
    }
	public function getImageLogo() {
        return $this->imageLogo;
    }
	public function setImageLogo($imageLogo = NULL) {
        $this->imageLogo = $imageLogo;
    }
	public function getImageCover() {
        return $this->imageCover;
    }
	public function setImageCover($imageCover = NULL) {
        $this->imageCover = $imageCover;
    }
	public function getInitialDate() {
        return $this->initialDate;
    }
	public function setInitialDate(\Datetime $initialDate = NULL) {
        $this->initialDate = $initialDate;
    }
    public function getFinalDate() {
        return $this->finalDate;
    }
	public function setFinalDate(\Datetime $finalDate = NULL) {
        $this->finalDate = $finalDate;
    }
	public function getLocation() {
        return $this->location;
    }
	public function setLocation($location = NULL) {
        $this->location = $location;
    }
	public function getSalesMethod() {
        return $this->salesMethod;
    }
	public function setSalesMethod($salesMethod = NULL) {
        $this->salesMethod = $salesMethod;
    }
	public function getRating() {
        return $this->rating;
    }
	public function setRating($rating = NULL) {
        $this->rating = $rating;
    }
	public function getClassification() {
        return $this->classification;
    }
	public function setClassification($classification = NULL) {
        $this->classification = $classification;
    }
    public function getAbout() {
        return $this->about;
    }
	public function setAbout($about = NULL) {
        $this->about = $about;
    }
	public function getParentEvent() {
        return $this->parentEvent;
    }
	public function setParentEvent(\Zeedhi\ApiServices\Model\Entities\EvtEvent $parentEvent = NULL) {
        $this->parentEvent = $parentEvent;
    }
    public function getExternalId() {
        return $this->externalId;
    }
    public function setExternalId($externalId) {
        $this->externalId = $externalId;
    }
}