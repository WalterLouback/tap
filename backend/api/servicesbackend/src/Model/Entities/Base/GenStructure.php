<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenStructure {
    
    
    /** @var int  */
    protected $level;
    /** @var int  */
    protected $externalQrcode;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenStructure  */
    protected $parent;
    /** @var \Zeedhi\ApiServices\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $description;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $email;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenAddress  */
    protected $genAddress;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedAt = $modifiedBy;
    }
    public function getLevel() {
        return $this->level;
    }
	public function setLevel($level = NULL) {
        $this->level = $level;
    }
    public function getExternalQrcode() {
        return $this->externalQrcode;
    }
	public function setExternalQrcode($externalQrcode) {
        $this->externalQrcode = $externalQrcode;
    }
	public function getParent() {
        return $this->parent;
    }
	public function setParent(\Zeedhi\ApiGeneral\Model\Entities\GenStructure $parent = NULL) {
        $this->parent = $parent;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getDescription() {
        return $this->description;
    }
	public function setDescription($description = NULL) {
        $this->description = $description;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getEmail() {
        return $this->email;
    }
	public function setEmail($email) {
        $this->email = $email;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenAddress() {
        return $this->genAddress;
    }
	public function setGenAddress(\Zeedhi\ApiGeneral\Model\Entities\GenAddress $genAddress = NULL) {
        $this->genAddress = $genAddress;
    }
}