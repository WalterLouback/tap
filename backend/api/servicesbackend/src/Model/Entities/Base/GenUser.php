<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class GenUser {
    
    
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $mainCreditcard;
    /** @var string  */
    protected $cpf;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $password;
    /** @var string  */
    protected $token;
    /** @var string  */
    protected $lastName;
    /** @var string  */
    protected $image;
    /** @var string  */
    protected $firstName;
    /** @var \Datetime  */
    protected $facebookTokenExpDate;
    /** @var string  */
    protected $facebookToken;
    /** @var string  */
    protected $facebookId;
    /** @var string  */
    protected $email;
    /** @var string  */
    protected $firebaseToken;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    /*public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }*/
    public function setModifiedAt($modifiedAt) {
        $this->modifiedAt = $modifiedAt;
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getCustomerId() {
        return $this->customerId;
    }
	public function setCustomerId($customerId = NULL) {
        $this->customerId = $customerId;
    }
	public function getMainCreditcard() {
        return $this->mainCreditcard;
    }
	public function setMainCreditcard($mainCreditcard = NULL) {
        $this->mainCreditcard = $mainCreditcard;
    }
	public function getCpf() {
        return $this->cpf;
    }
	public function setCpf($cpf = NULL) {
        $this->cpf = $cpf;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getPassword() {
        return $this->password;
    }
	public function setPassword($password = NULL) {
        $this->password = $password;
    }
	public function getToken() {
        return $this->token;
    }
	public function setToken($token = NULL) {
        $this->token = $token;
    }
	public function getLastName() {
        return $this->lastName;
    }
	public function setLastName($lastName) {
        $this->lastName = $lastName;
    }
	public function getImage() {
        return $this->image;
    }
	public function setImage($image = NULL) {
        $this->image = $image;
    }
	public function getFirstName() {
        return $this->firstName;
    }
	public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }
	public function getFacebookTokenExpDate() {
        return $this->facebookTokenExpDate;
    }
	public function setFacebookTokenExpDate(\Datetime $facebookTokenExpDate = NULL) {
        $this->facebookTokenExpDate = $facebookTokenExpDate;
    }
	public function getFacebookToken() {
        return $this->facebookToken;
    }
	public function setFacebookToken($facebookToken = NULL) {
        $this->facebookToken = $facebookToken;
    }
	public function getFirebaseToken() {
        return $this->firebaseToken;
    }
	public function setFirebaseToken($firebaseToken = NULL) {
        $this->firebaseToken = $firebaseToken;
    }
	public function getFacebookId() {
        return $this->facebookId;
    }
	public function setFacebookId($facebookId = NULL) {
        $this->facebookId = $facebookId;
    }
	public function getEmail() {
        return $this->email;
    }
	public function setEmail($email) {
        $this->email = $email;
    }
}