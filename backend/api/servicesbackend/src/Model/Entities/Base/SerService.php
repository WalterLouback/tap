<?php

namespace Zeedhi\ApiServices\Model\Entities\Base;

class SerService
{
    protected $name;

    protected $status;

    protected $decription;
    
    protected $cancellationFine;
    
    protected $cancellationTime;
    
    protected $time;
    
    protected $evtEvent;
    
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;
    
    protected $parentId;
    
    protected $price;
    
    protected $color;

    protected $icon;
    
    protected $image;
    
    protected $type;
    
    protected $statusapp;
    
    protected $futureDaysToSchedule;
    
    protected $userSchedulingLimit;
    
    protected $dependentSchedulingLimit;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name=NULL)
    {
        $this->name = $name;

        return $this;
    }

    public function getDecription()
    {
        return $this->decription;
    }

    public function setDecription($decription=NULL)
    {
        $this->decription = $decription;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status=NULL)
    {
        $this->status = $status;

        return $this;
    }

    public function getCancellationFine()
    {
        return $this->cancellationFine;
    }

    public function setCancellationFine($cancellationFine=NULL)
    {
        $this->cancellationFine = $cancellationFine;

        return $this;
    }

    public function getCancellationTime()
    {
        return $this->cancellationTime;
    }

    public function setCancellationTime($cancellationTime=NULL)
    {
        $this->cancellationTime = $cancellationTime;

        return $this;
    }

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg=NULL)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time=NULL)
    {
        $this->time = $time;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy=NULL)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy=NULL)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $dt = new \DateTime();
        $this->modifiedAt = $dt->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEvtEvent()
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent = NULL)
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function setParentId(\Zeedhi\ApiServices\Model\Entities\SerService $parentId = NULL)
    {
        $this->parentId = $parentId;

        return $this;
    }
    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price=NULL)
    {
        $this->price = $price;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color=NULL)
    {
        $this->color = $color;

        return $this;
    }
    
    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon=NULL)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image=NULL)
    {
        $this->image = $image;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type=NULL)
    {
        $this->type = $type;

        return $this;

    }

    public function getStatusapp()
    {
        return $this->statusapp;
    }

    public function setStatusapp($statusapp)
    {
        $this->statusapp = $statusapp;

        return $this;
    }
    
    public function getFutureDaysToSchedule()
    {
        return $this->futureDaysToSchedule;
    }

    public function setFutureDaysToSchedule($futureDaysToSchedule)
    {
        $this->futureDaysToSchedule = $futureDaysToSchedule;

        return $this;
    }
    
    public function getUserSchedulingLimit()
    {
        return $this->userSchedulingLimit;
    }

    public function setUserSchedulingLimit($userSchedulingLimit)
    {
        $this->userSchedulingLimit = $userSchedulingLimit;

        return $this;
    }
    
    public function getDependentSchedulingLimit()
    {
        return $this->dependentSchedulingLimit;
    }

    public function setDependentSchedulingLimit($dependentSchedulingLimit)
    {
        $this->dependentSchedulingLimit = $dependentSchedulingLimit;

        return $this;
    }
    
    
}
