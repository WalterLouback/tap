<?php
namespace Zeedhi\ApiServices\Model\Entities\Base;


abstract class EvtTagEvent {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiServices\Model\Entities\GenTag  */
    protected $genTag;
    /** @var \Zeedhi\ApiServices\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var int  */
    protected $nrorg;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenTag() {
        return $this->genTag;
    }
	public function setGenTag(\Zeedhi\ApiServices\Model\Entities\GenTag $genTag = NULL) {
        $this->genTag = $genTag;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiServices\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
}