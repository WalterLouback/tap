<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenStatus extends \Zeedhi\ApiServices\Model\Entities\Base\GenStatus {
    
    const PEDIDO_NOVO = 3;
    const PEDIDO_ACEITO = 4;
    const PEDIDO_PREPARADO = 5;
    const PEDIDO_ENTREGUE = 6;

    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "BUTTON_NAME" => $this->getButtonName(),
            "LABEL_NAME" => $this->getLabelName(),
            "COLOR" => $this->getColor(),
            "HAS_NEXT" => $this->getNext() != NULL,
            "NEXT" => $this->getNext() != NULL ? $this->getNext()->toString() : NULL
        );
    }
    
    public static function manyToArray($statusArray) {
        $arr = [];
        foreach ($statusArray as $status) {
            array_push($arr, $status->toArray());
        }
        
        return $arr;
    }

    public function toString() {
        return $this->getLabelName();
    }
}