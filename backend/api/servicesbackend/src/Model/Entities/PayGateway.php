<?php
namespace Zeedhi\ApiServices\Model\Entities;


class PayGateway extends \Zeedhi\ApiServices\Model\Entities\Base\PayGateway {

    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getGateway(),
            'MERCHANT_KEY' => $this->getMerchantKey()
        );
    }
    
}