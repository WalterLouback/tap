<?php

namespace Zeedhi\ApiServices\Model\Entities;

use Zeedhi\ApiServices\Model\Entities\SerTimesheet;

class SerTimesheet extends  \Zeedhi\ApiServices\Model\Entities\Base\SerTimesheet {
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "INITIAL_TIME" => $this->getInitialTime(),
            "FINAL_TIME" => $this->getFinalTime(),
            "STATUS" => $this->getStatus()
        );
    }
}