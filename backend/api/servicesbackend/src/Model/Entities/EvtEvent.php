<?php
namespace Zeedhi\ApiServices\Model\Entities;

use Zeedhi\ApiServices\Helpers\General as General;
use Zeedhi\ApiServices\Model\Entities\PayGateway;
use Zeedhi\ApiServices\Model\Entities\GenShift;
use Zeedhi\ApiServices\Model\Entities\OrdMenu;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;
use Zeedhi\ApiServices\Model\Entities\EvtEventMenu;
use Zeedhi\ApiServices\Model\Entities\EvtEventSeller;
use Zeedhi\ApiServices\Model\Entities\OrdConfigStore;
use Zeedhi\ApiServices\Model\Entities\PayPaymentMethod;

class EvtEvent extends \Zeedhi\ApiServices\Model\Entities\Base\EvtEvent {
    
    public function build($entityManager, $userId=NULL) {
        $this->setPaymentMethods($entityManager);
        $this->setPaymentMoment($entityManager);
        $this->setContactMethods($entityManager);
        $this->setWorkshifts($entityManager);
        $this->setMerchantKey($entityManager);
        if ($userId) $this->setSellerData($entityManager, $userId);
    }
    
    public function getMenus() {
        return property_exists($this, 'menus') ? $this->menus : [];
    }
    
    public function getPaymentMethods() {
        return property_exists($this, 'paymentMethods') ? $this->paymentMethods : [];
    }
    
    public function getDeliversToTable() {
        return property_exists($this, 'deliversToTable') ? $this->deliversToTable : 'F';
    }
    
    public function getDeliversToBalcony() {
        return property_exists($this, 'deliversToBalcony') ? $this->deliversToBalcony : 'F';
    }

    public function getPaymentMoment() {
        return property_exists($this, 'paymentMoment') ? $this->paymentMoment : NULL;
    }

    public function getMerchantKey() {
        return property_exists($this, 'merchantKey') ? $this->merchantKey : NULL;
    }
    
    public function getMerchantId() {
        return property_exists($this, 'merchantId') ? $this->merchantId : NULL;
    }

    public function getContactMethods() {
        return property_exists($this, 'contactMethods') ? $this->contactMethods : [];
    }
    
    function getWorkshifts() {
        return property_exists($this, 'workshifts') ? $this->workshifts : [];
    }
    
    function getSellerData() {
        return property_exists($this, 'sellerData') ? $this->sellerData : NULL;
    }
    
    function setWorkshifts($entityManager) {
        $storeId = $this->getId();
        $this->workshifts = $entityManager->createQuery(
            "
            SELECT s
            FROM " . GenShift::class ." s
            WHERE s.evtEvent = $storeId
            "
        )->getResult();
    }
    
    public function getShiftsOfDay($date, $entityManager) {
        $workshift = GenShift::class;
        $storeId   = $this->getId();
        
        $dayOfWeek = strftime("%A", strtotime($date));

        $daysOfWeeksArray = array(
            'Sunday'    => 0, 'Monday' => 1, 'Tuesday'   => 2, 'Wednesday' => 3, 
            'Thursday'  => 4, 'Friday' => 5, 'Saturday'  => 6
        );

        $formatedDay = $daysOfWeeksArray[$dayOfWeek];
        
        $dayWorkshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime 
            FROM $workshift ws
            WHERE ws.evtEvent = $storeId
            AND ws.day = $formatedDay
            "
        )->getResult();
        
        return $dayWorkshifts;
    }
    
    public function isOpened($entityManager=NULL) {
        if (!$entityManager) {
            if (property_exists($this, 'opened')) return $this->opened;
            else return NULL;
        }
        
        $opened      = false;
        
        /* Obter status da loja para verificar se ela foi fechada ou aberta manualmente */
        $storeStatus = $this->getStatus();
        
        if ($storeStatus == 'O') {
            $opened  = true;
        }
        else if ($storeStatus != 'C') {
            
            /* Obter horários de funcionamento da loja no dia de hoje */
            $today        = new \DateTime();
            $todayClone   = new \DateTime();
            
            /* Definindo os turnos do dia */
            $formatedDate = $todayClone->format('Y-m-d H:i:s');
            $shifts       = $this->getShiftsOfDay($formatedDate, $entityManager);
            /* Adicionando fuso horário */
            $today->sub(new \DateInterval('PT2H'));
            
            /* Percorrer os turnos e verificar se horário atual se encaixa em algum */
            foreach ($shifts as $shift) {
                /* Obtendo horário atual */
                $currHours = $today->format('H');
                $currMins  = $today->format('i');
                /* Obtendo horário de início do turno */
                $inDate    = clone $shift['initialTime'];
                $inDate->sub(new \DateInterval('PT2H'));
                $inHours   = $inDate->format('H');
                $inMins    = $inDate->format('i');
                /* Obtendo horário de término do turno */
                $finDate   = clone $shift['finalTime'];
                $finDate->sub(new \DateInterval('PT2H'));
                $finHours  = $finDate->format('H');
                $finMins   = $finDate->format('i');
                
                /* Verificando se horário atual se encaixa no turno */
                if ($currHours > $inHours || ($currHours == $inHours && $currMins >= $inMins)) {
                    if ($currHours < $finHours || ($currHours == $finHours && $currMins <= $finMins)) {
                        $opened = true;
                        break;
                    }
                }
            }
        }
        
        $this->opened = $opened;
        
        return $opened;
    }
    
    public function setMenus($entityManager) {
        $menu      = 'Zeedhi\ApiServices\Model\Entities\OrdMenu';
        $eventMenu = 'Zeedhi\ApiServices\Model\Entities\EvtEventMenu';
        $storeId = $this->getId();
        $menus   = $entityManager->createQuery(
            "
            SELECT m 
            FROM $menu m
            JOIN $eventMenu em WITH em.ordMenu = m
            JOIN em.evtEvent as s 
            WHERE s.id = $storeId 
            ORDER BY m.name
            "
        )->getResult();
        
        $currentWorkshift    = [];
        $notCurrentWorkshift = [];
        
        foreach ($menus as $menu) {
            $menu->build($entityManager, $this->getId());
            array_push($currentWorkshift, $menu);
            if ($menu->isCurrentWorkshift() == true) {
            } else {
                array_push($notCurrentWorkshift, $menu);
            }
        }
        
        $menus = [ 'currentWorkshift'    => $currentWorkshift,
                   'notCurrentWorkshift' => $notCurrentWorkshift ];
        
        $this->menus = $menus;
    }
    
    public function setPaymentMethods($entityManager) {
        $storeId = $this->getId();
        $this->paymentMethods = $entityManager->createQuery(
            "
            SELECT pm
            FROM " . PayPaymentMethod::class . " pm
            JOIN '\Zeedhi\ApiServices\Model\Entities\OrdConfigStore' c WITH c.event = $storeId
            JOIN '\Zeedhi\ApiServices\Model\Entities\OrdStorePayMethod' spm WITH pm.id = spm.paymentMethod
            JOIN spm.ordConfigStore c2
            WHERE c.id = c2.id
            "    
        )->getResult();
    }
    
    public function setContactMethods($entityManager) {
        $storeId = $this->getId();
        $this->contactMethods = $entityManager->createQuery(
            "
            SELECT c
            FROM " . GenContact::class . " c
            JOIN c.evtEvent s
            WHERE s.id = $storeId
            "
        )->getResult();
    }
    
    public function setDeliversToTable($entityManager) {
        $storeId = $this->getId();
        $this->deliversToTable = $entityManager->createQuery(
            "
            SELECT c.deliversToTable
            FROM " . OrdConfigStore::class . " c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->deliversToTable) > 0) $this->deliversToTable = $this->deliversToTable[0]['deliversToTable'];
    }
    
    public function setDeliversToBalcony($entityManager) {
        $storeId = $this->getId();
        $deliversToBalcony = $entityManager->createQuery(
            "
            SELECT c.deliversToBalcony
            FROM " . OrdConfigStore::class . " c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($deliversToBalcony) > 0) $this->deliversToBalcony = $deliversToBalcony[0]['deliversToBalcony'];
    }
    
    public function setPaymentMoment($entityManager) {
        $storeId       = $this->getId();
        
        /* If store has its own workflow */
        $paymentMoment = $this->getPaymentMomentFromEventWorkflow($entityManager, $storeId);
        if (count($paymentMoment) > 0) $this->paymentMoment = $paymentMoment[0];
        else {
            /* Else get organization's workflow */
            $paymentMoment = $this->getPaymentMomentFromOrganizationWorkflow($entityManager, $this->getNrorg());
            if (count($paymentMoment) > 0) $this->paymentMoment = $paymentMoment[0];
        }
    }
    
    private function getPaymentMomentFromEventWorkflow($entityManager, $storeId) {
        return $entityManager->createQuery(
            "
            SELECT pm
            FROM ". OrdConfigStore::class ." c
            JOIN 'Zeedhi\ApiServices\Model\Entities\OrdWorkflow' w WITH c.workflow = w.id
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenStatus' pm WITH w.paymentMoment = pm.id
            WHERE c.event = $storeId
            "
        )->getResult();
    }
    
    private function getPaymentMomentFromOrganizationWorkflow($entityManager, $nrorg) {
        return $entityManager->createQuery(
            "
            SELECT pm
            FROM 'Zeedhi\ApiServices\Model\Entities\OrdWorkflow' w
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenStatus' pm WITH w.paymentMoment = pm.id
            WHERE w.nrorg = $nrorg
            "
        )->getResult();
    }
    
    public function setMerchantKey($entityManager) {
        $payGateway = PayGateway::class;
        $payGatewayRel = PayGatewayRel::class;
        
        $storeId = $this->getId();
        $merchantKey = $entityManager->createQuery(
            "
            SELECT g.merchantKey, g.merchantId
            FROM $payGateway g
            JOIN $payGatewayRel gr WITH g.id = gr.payGateway
            WHERE gr.evtEvent = $storeId
            "
        )->getResult();
        if (count($merchantKey) > 0) {
            $this->merchantKey = $merchantKey[0]['merchantKey'];
            $this->merchantId = $merchantKey[0]['merchantId'];
        }
    }
    
    public function setSellerData($entityManager, $userId) {
        $evtEventSeller = EvtEventSeller::class;
        $evtEvent = EvtEvent::class;
        
        $storeId = $this->getId();
        $sellerData = $entityManager->createQuery(
            "
            SELECT es
            FROM $evtEventSeller es
            JOIN $evtEvent e WHERE es.evtEvent = e
            JOIN es.genUser u
            WHERE e.id = $storeId
            AND u.id = $userId
            "
        )->getResult();
        if (count($sellerData) > 0) {
            $this->sellerData = $sellerData[0];
        }
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['about'] = $this->getAbout();
        $array['type'] = $this->getType();
        // $array['address'] = $this->getAddress();
        $array['imageCover'] = $this->getImageCover();
        $array['imageLogo'] = $this->getImageLogo();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['status'] = $this->getStatus();
        $array['structureId'] = $this->getStructure() != NULL ? $this->getStructure()->getId() : NULL;
        $array['structureName'] = $this->getStructure() != NULL ? $this->getStructure()->getName() : NULL;
        $array['ownerUserId'] = $this->getOwnerUser() != NULL ? $this->getOwnerUser()->getId() : NULL;
        $array['parentEventId'] = $this->getParentEvent() != NULL ? $this->getParentEvent()->getId() : NULL;
        $array['opened'] = (bool) $this->isOpened();
        $array['sellerData'] = $this->getSellerData() ? $this->getSellerData()->toArray() : NULL;
        $array['workshifts'] = GenShift::manyToArray($this->getWorkshifts());
        if ($this->getMerchantKey()) $array['merchantKey'] = $this->getMerchantKey();
        if ($this->getMerchantId()) $array['merchantId'] = $this->getMerchantId();
        
        if ($this->getMenus()) {
            $array['menus'] = [
                'currentWorkshift'    => OrdMenu::manyToArray($this->getMenus()['currentWorkshift']),
                'notCurrentWorkshift' => OrdMenu::manyToArray($this->getMenus()['notCurrentWorkshift'])
            ];
        }
        if (count($this->getContactMethods()) > 0) $array['contactMethods'] = GenContact::manyToArray($this->getContactMethods());
        $array['paymentMethods'] = OrdProduct::manyToArray($this->getPaymentMethods());
        if ($this->getPaymentMoment()) $array['paymentMoment'] = $this->getPaymentMoment()->toString();
        return $array;
    }
    
    public static function manyToArray($events) {
        $arrays = [];
        foreach ($events as $event) {
            $array = $event->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
}