<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenStructure extends \Zeedhi\ApiServices\Model\Entities\Base\GenStructure {
    
    public function build($entityManager=NULL) {
        if ($entityManager != NULL) $this->setContactMethods($entityManager);
        $this->setTopLevelStructure();
    }
    
    public function getContactMethods() {
        return property_exists($this, 'contactMethods') ? $this->contactMethods : [];
    }
    
    public function getTopLevelStructure() {
        return property_exists($this, 'topLevelStructure') ? $this->topLevelStructure : NULL;
    }
    
    public function setContactMethods($entityManager) {
        $structureId = $this->getId();
        $this->contactMethods = $entityManager->createQuery(
            "
            SELECT c
            FROM \Zeedhi\ApiServices\Model\Entities\GenContact c
            JOIN c.genStructure s
            WHERE s.id = $structureId
            "
        )->getResult();
    }
    
    public function setTopLevelStructure() {
        $topLevelStructure = $this;
        
        while (($parent = $topLevelStructure->getParent()) != NULL) {
            $topLevelStructure = $parent;
        }
        
        if ($topLevelStructure != $this)
            $this->topLevelStructure = $topLevelStructure;
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['description'] = $this->getDescription();
        $array['level'] = $this->getLevel();
        $array['externalQrcode'] = $this->getExternalQrcode();
        $array['contactMethods'] = $this->getContactMethods();
        $array['parentId'] = $this->getParent() != NULL ? $this->getParent()->getId() : NULL;
        $array['address'] = $this->getGenAddress() != NULL ? $this->getGenAddress()->getId() : NULL;
        $array['email'] = $this->getEmail();
        $array['addressData'] = $this->getGenAddress() != NULL ? $this->getGenAddress()->toArray() : NULL;
        $array['children'] = GenStructure::manyToArray($this->getChildren());
        if ($this->getTopLevelStructure() != NULL)
            $array['topLevelStructure'] = $this->getTopLevelStructure()->toArray();
        
        return $array;
    }
    
    public static function manyToArray($structures) {
        $arrays = [];
        foreach ($structures as $structure) {
            array_push($arrays, $structure->toArray());
        }
        
        return $arrays;
    }
    
    public function toArrayOne() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['parentId'] = $this->getParent() != NULL ? $this->getParent()->getId() : NULL;
        
        return $array;
    }
    
    public static function manyToArrayOne($structures) {
        $arrays = [];
        foreach ($structures as $structure) {
            array_push($arrays, $structure->toArrayOne());;
        }
        
        return $arrays;
    }
    
    public function setChildren($arrayStructure) {
        $this->children = $arrayStructure;
    }
    
    public function getChildren() {
        return property_exists($this, 'children') ? $this->children : [];
    }
    
}