<?php

namespace Zeedhi\ApiServices\Model\Entities;

class GenScheduleStructureRel extends \Zeedhi\ApiServices\Model\Entities\Base\GenScheduleStructureRel {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "STRUCTURE_ID" => $this->getGenStructure()->getId(),
            "SCHEDULE_ID" => $this->getGenSchedule()->getId(),
            "CREATED_BY" => $this->getCreatedBy(),
            "MODIFIED_BY" => $this->getModifiedBy(),
            "CREATED_AT" => $this->getCreatedAt(),
            "MODIFIED_AT" => $this->getModifiedAt()
        );
    }
}