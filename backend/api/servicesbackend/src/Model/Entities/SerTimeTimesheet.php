<?php

namespace Zeedhi\ApiServices\Model\Entities;

use Zeedhi\ApiServices\Model\Entities\GenSchedule;
use Zeedhi\ApiServices\Model\Entities\GenScheduleUserRel;
use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiServices\Model\Entities\SerServiceSellerRel;
use Zeedhi\ApiServices\Model\Entities\SerTimeTimesheet;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;

class SerTimeTimesheet extends \Zeedhi\ApiServices\Model\Entities\Base\SerTimeTimesheet {
    
    protected $timesheet = [];
    
    public function build($entityManager, $eventId=null, $dayOfWeek, $date, $nrorg, $serTimesheetIds, $sellerId = null) {
        return array(
                'hours' => $this->getTimesheet($entityManager, $dayOfWeek, $eventId, $date, $nrorg, $serTimesheetIds)
        );
    }
    
    public function buildInterested($entityManager, $eventId=null, $dayOfWeek, $date, $nrorg, $serTimesheetIds, $sellerId = null) {
        return array(
                'hours' => $this->getVacantTimesheetWithInterested($entityManager, $dayOfWeek, $eventId, $date, $nrorg, $serTimesheetIds)
        );
    }
    
    public function getTimesheet($entityManager, $dayOfWeek, $eventId=null, $date, $nrorg, $serTimesheetIds) {
        $serTimeTimesheet = SerTimeTimesheet::class;
        
        $querySerTimesheetIds = ' IN (';
        
        foreach($serTimesheetIds as $index => $serTimesheetId) {
            $querySerTimesheetIds = $querySerTimesheetIds . $serTimesheetId["SER_TIMESHEET_ID"];      

            if($index + 1 == sizeof($serTimesheetIds)) {
                $querySerTimesheetIds = $querySerTimesheetIds . ")";
            } else {
                $querySerTimesheetIds = $querySerTimesheetIds . ",";
            }
        }
        date_default_timezone_set('America/Sao_Paulo');
        $today = date('Y-m-d');
        $time  = date('H:i:s');
        
        if($date == $today) {
            $arrayTimes = $entityManager->createQuery("
                SELECT stt
                FROM $serTimeTimesheet stt
                WHERE stt.serTimesheet $querySerTimesheetIds
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%'
                AND stt.status = 'A' AND stt.initialTime >= '$time'
            ")->getResult();
        } else {
            $arrayTimes = $entityManager->createQuery("
                SELECT stt
                FROM $serTimeTimesheet stt
                WHERE stt.serTimesheet $querySerTimesheetIds
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%'
                AND stt.status = 'A'
            ")->getResult();
        }
        $data = array();
        
        foreach($arrayTimes as $arrayTime) {
            array_push($data, $arrayTime->toArray());
        }
        
        return $data;
    }
    
    public function getVacantTimesheetWithInterested($entityManager, $dayOfWeek, $eventId=null, $date, $nrorg, $serTimesheetIds) {
        $serTimeTimesheet = SerTimeTimesheet::class;
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $querySerTimesheetIds = ' IN (';
        
        foreach($serTimesheetIds as $index => $serTimesheetId) {
            $querySerTimesheetIds = $querySerTimesheetIds . $serTimesheetId["SER_TIMESHEET_ID"];      

            if($index + 1 == sizeof($serTimesheetIds)) {
                $querySerTimesheetIds = $querySerTimesheetIds . ")";
            } else {
                $querySerTimesheetIds = $querySerTimesheetIds . ",";
            }
        }
        date_default_timezone_set('America/Sao_Paulo');
        $today = date('Y-m-d');
        $time  = date('H:i:s');
        
        if($date == $today) {
            $arrayTimes = $entityManager->createQuery("
                SELECT stt.id, stt.initialTime, stt.finalTime,
                    (   SELECT COUNT(gs.id) FROM $genSchedule gs
                        LEFT JOIN gs.serTimeTimesheet stt2
                        WHERE gs.serTimeTimesheet = stt.id AND gs.initialDate LIKE '$date%' AND gs.status <> 'I'
                    ) as interested
                FROM $serTimeTimesheet stt
                WHERE stt.serTimesheet $querySerTimesheetIds
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%'
                AND stt.status = 'A' AND stt.initialTime >= '$time'
            ")->getResult();
        } else {
            $arrayTimes = $entityManager->createQuery("
                SELECT stt.id, stt.initialTime, stt.finalTime,
                    (   SELECT COUNT(gs.id) FROM $genSchedule gs
                        LEFT JOIN gs.serTimeTimesheet stt2
                        WHERE gs.serTimeTimesheet = stt.id AND gs.initialDate LIKE '$date%' AND gs.status <> 'I'
                    ) as interested
                FROM $serTimeTimesheet stt
                WHERE stt.serTimesheet $querySerTimesheetIds
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%'
                AND stt.status = 'A'
            ")->getResult();
        }
        
        $data = array();
        
        foreach($arrayTimes as $key => $arrayTime) {
            $result = $entityManager->createQuery("
                SELECT gu.id FROM $genSchedule gs 
                LEFT JOIN $genScheduleUserRel gsur WITH gs.id = gsur.genSchedule
                LEFT JOIN gsur.genUser gu
                LEFT JOIN gs.serTimeTimesheet stt2
                WHERE gs.serTimeTimesheet = " . $arrayTime["id"] . " AND gs.initialDate LIKE '$date%' AND gs.status <> 'I'
            ")->getResult();
            
            $arrayTimes[$key]['idsInterested'] = !empty($result) ? array_map(function ($el){ return $el['id']; }, $result)  : [];
        }
        
        return $arrayTimes;
    }
    
    public function getSellers($entityManager, $serviceId, $nrorg, $date, $arrayTimes) {
        $serServiceSeller = SerServiceSellerRel::class;
        $genSchedule = GenSchedule::class;
 
        foreach($arrayTimes as $time) {
            
            $sellers = $entityManager->createQuery("
                SELECT sss FROM $serServiceSeller sss
                WHERE sss.serService = '$serviceId' 
                AND sss.status = 'A' AND sss.nrorg = '$nrorg'
                AND sss.id NOT IN (
                    SELECT sss1.id FROM $genSchedule gs
                    JOIN gs.serServiceSeller sss1
                    JOIN gs.serTimeTimesheet stt WITH stt.id = ". $time["id"] . "
                    WHERE gs.serService = '$serviceId'
                    AND gs.initialDate = '$date' AND gs.nrorg = '$nrorg'
                )
            ")->getResult();
        }

        return !empty($sellers) ? $this->manyToArray($sellers) : NULL;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'initialTime' => $this->getInitialTime(),
            'finalTime' => $this->getFinalTime(),
            'type' => $this->getType(),
            'max_scheduling' => getMaxScheduling()
        );
    }
    
    public static function manyToArray($data) {
        $arrays = [];
        foreach ($data as $value) {
            $array = $value->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
}