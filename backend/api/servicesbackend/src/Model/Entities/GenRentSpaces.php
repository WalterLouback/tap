<?php
namespace Zeedhi\ApiServices\Model\Entities;


class GenRentSpaces extends \Zeedhi\ApiServices\Model\Entities\Base\GenRentSpaces {
    
    public function build($entityManager, $nrorg=NULL) {
        $this->setUnidadeById($entityManager);
        $this->setRentSpacesHasPhotos($entityManager);
    }
    
    public function toArray() {
        $array = [];
        $array['id']                    = $this->getId() ? $this->getId() : "";
        $array['name']                  = $this->getName() ? $this->getName() : $this->getGenStructure()->getName();
        $array['phone']                 = $this->getTelephone() ? $this->getTelephone() : "";
        $array['address']               = $this->getAddress() ? $this->getAddress() : "";
        $array['maximumPersonCapacity'] = $this->getMaximumPersonCapacity() ? $this->getMaximumPersonCapacity() : "";
        $array['unityId']               = $this->getUnidadeById() ? $this->getUnidadeById()->getId() : "";
        $array['unityName']             = $this->getUnidadeById() ? $this->getUnidadeById()->getName() : "";
        $array['photos']                = $this->getRentSpacesHasPhotos() ? $this->getRentSpacesHasPhotos() : "";
        
        return $array;
    }
    
    public function toArrayAll() {
        $array = [];
        $array['id']                    = $this->getId() ? $this->getId() : "";
        $array['name']                  = $this->getName() ? $this->getName() : $this->getGenStructure()->getName();
        $array['phone']                 = $this->getTelephone() ? $this->getTelephone() : "";
        $array['address']               = $this->getAddress() ? $this->getAddress() : "";
        $array['description']           = $this->getDescription() ? $this->getDescription() : $this->getGenStructure()->getDescription();
        $array['maximumPersonCapacity'] = $this->getMaximumPersonCapacity() ? $this->getMaximumPersonCapacity() : "";
        $array['linkTourVirtual']       = $this->getLinkTourVirtual() ? $this->getLinkTourVirtual() : "";
        $array['status']                = $this->getStatus() ? $this->getStatus() : "";
        $array['unityId']               = $this->getUnidadeById() ? $this->getUnidadeById()->getId() : "";
        $array['unityName']             = $this->getUnidadeById() ? $this->getUnidadeById()->getName() : "";
        $array['localId']               = $this->getGenStructure() ? $this->getGenStructure()->getId() : "";
        $array['localName']             = $this->getGenStructure() ? $this->getGenStructure()->getName() : "";
        $array['responsibleEmail']      = $this->getGenStructure()->getEmail() ? $this->getGenStructure()->getEmail() : $this->getGenUser()->getEmail();
        $array['photos']                = $this->getRentSpacesHasPhotos() ? $this->getRentSpacesHasPhotos() : "";
        
        return $array;
    }
    
    public static function manyToArray($photos) {
        $arrays = [];
        foreach ($photos as $photo) {
            array_push($arrays, $photos->toArray());
        }
        return $arrays;
    }
    /* Busca fotos do espaço */
    public function setRentSpacesHasPhotos($entityManager) {
        $photos = $entityManager->getRepository(GenRentSpacesHasPhotos::class)->findBy(['rentSpaces' => $this->getId()]);
        
        $this->photos = GenRentSpacesHasPhotos::manyToArray($photos);
    }
    /* Retorna fotos do espaço */
    public function getRentSpacesHasPhotos() {
        return property_exists($this, 'photos') ? $this->photos : [];
    }
    /* Busca unidade que o espaço pertence */
    public function setUnidadeById($entityManager) {
        $unidade = $entityManager->getRepository(GenStructure::class)->findOneBy(['id' => $this->getGenStructure()->getParent(), 'nrorg' => $this->getNrorg(), 'type' => 'U']);
        
        $this->unidade = $unidade;
    }
    /* Retora unidade que o espaço pertence */
    public function getUnidadeById() {
        return property_exists($this, 'unidade') ? $this->unidade : [];
    }

}