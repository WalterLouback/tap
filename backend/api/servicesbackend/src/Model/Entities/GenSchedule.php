<?php

namespace Zeedhi\ApiServices\Model\Entities;

use Zeedhi\ApiServices\Model\Entities\GenScheduleUserRel;

class GenSchedule extends \Zeedhi\ApiServices\Model\Entities\Base\GenSchedule {
    
    public function toArray($entityManager) {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "TYPE" => $this->getType(),
            "STATUS" => $this->getStatus(),
            "STRUCTURE_ID" => $this->getGenStructure() ? $this->getGenStructure()->getId() : "",
            "STRUCTURE_NAME" => $this->getGenStructure() ? $this->getGenStructure()->getName() : "",
            "INITIAL_DATE" => $this->getInitialDate() ? $this->getInitialDate()->format('Y-m-d'): "",
            "INITIAL_TIME" => $this->getSerTimeTimesheet() ? $this->getSerTimeTimesheet()->getInitialTime() : "",
            "FINAL_DATE" => $this->getFinalDate() ? $this->getFinalDate()->format('Y-m-d') : "",
            "FINAL_TIME" => $this->getSerTimeTimesheet() ? $this->getSerTimeTimesheet()->getFinalTime() : "",
            "APPOINTMENT" => $this->getAppointment(),
            "SERVICES" => [
                'ID' => $this->getSerService() ? $this->getSerService()->getId() : "",
                'TIPO' => $this->getSerService() ? $this->getSerService()->getName() : "",
                'PRICE' => $this->getSerService() ? $this->getSerService()->getPrice(): ""
            ],
            "SERVICE_SELLER" => [ 
                "ID" => $this->getSerServiceSeller() ? $this->getSerServiceSeller()->getId() : "",
                "FULL_NAME" => $this->getSerServiceSeller() ? $this->getSerServiceSeller()->toArray()["SELLER_FULL_NAME"] : null,
                "SELLER_ID" => $this->getSerServiceSeller() ? $this->getSerServiceSeller()->toArray()["EVENT_SELLER_ID"] : null
            ],
            "CLIENT" => $this->getClient($entityManager, $this->getId()),
            "TIME_TIMESHEET_ID" => $this->getSerTimeTimesheet() ? (int) $this->getSerTimeTimesheet()->getId() : null,
            "GEN_SCHEDULE_ID" => $this->getGenSchedule() ? $this->getGenSchedule()->getId() : null
        );
    }
    
    public function getClient($entityManager, $scheduleId) {
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $user =  $entityManager->createQuery(
            "
            SELECT DISTINCT gu.id ID, CONCAT(gu.firstName, ' ', gu.lastName) NAME
            FROM $genScheduleUserRel gsur
            LEFT JOIN gsur.genUser gu  
            WHERE gsur.genSchedule = $scheduleId
            "
        )->getResult();
        
        return $user ? $user[0] : [];
    }
}
