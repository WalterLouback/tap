<?php

namespace  Zeedhi\ApiServices\Model\Entities;

class SerServiceEventRel extends \Zeedhi\ApiServices\Model\Entities\Base\SerServiceEventRel
{
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "STATUS" => $this->getStatus(),
            "EVENT_ID" => !empty($this->getEvtEvent()) ? $this->getEvtEvent()->getId() : null,
            "EVENT_NAME" => !empty($this->getEvtEvent()) ? $this->getEvtEvent()->getName() : null,
            "STRUCTURE_ID" => !empty($this->getEvtEvent()) ? $this->getEvtEvent()->getStructure()->getId() : null,
            "STRUCTURE_NAME" => !empty($this->getEvtEvent()) ? $this->getEvtEvent()->getStructure()->getName() : null,
            "SERVICE_ID" => $this->getSerService()->getId(),
            "CREATED_BY" => $this->getCreatedBy(),
            "MODIFIED_BY" => $this->getModifiedBy(),
            "CREATED_AT" => $this->getCreatedAt(),
            "MODIFIED_AT" => $this->getModifiedAt()
        );
    }
}
