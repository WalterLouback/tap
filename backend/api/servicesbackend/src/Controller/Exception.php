<?php
namespace Zeedhi\ApiServices\Controller;

class Exception extends \Exception {

    const MISSING_NAME = 0;
    const INVALID_PARAMETERS = 1;
    const MISSING_PARAMETERS = 2;
    const NO_ONE_LOGGED = 3;
    const UNAUTHORIZED_ACTION = 4;
    const USER_NOT_FOUND = 12309;

    public static function missingName() {
        return new static('Missing name property', self::MISSING_NAME);
    }
    
     public static function invalidParameters() {
        return new static('Invalid parameters!', self::INVALID_PARAMETERS);
    }
    
    public static function missingParameters() {
        return new static("It is missing parameters!", self::MISSING_PARAMETERS);
    }
    
    public static function noOneLogged() {
        return new static('There is no one logged!', self::NO_ONE_LOGGED);
    }
    
    public static function unauthorizedAction() {
        return new static('Unauthorized Action!', self::UNAUTHORIZED_ACTION);
    }
    
    /**
    * stringifyParameters
    * 
    * @param Array<String> $params
    * 
    */
    public static function stringifyParameters($params) {
        return join(", ", $params);
    }
    
    public static function invalidParametersException($functionName, $expectedParameters) {
        return new static('Invalid parameters to function \'' . $functionName . '\'. Expected ' . self::stringifyParameters($expectedParameters) . '.');
    }
    
    public static function integrityException($functionName, $attribute) {
        return new static('An integrity issue happened at function \'' . $functionName . '\' with ' . $attribute);
    }
    
    public static function unsuportedPaymentMethod() {
        return new static('The required payment method is not suported');
    }
    
    public static function loginNotFound() {
        return new static('User not found!', self::USER_NOT_FOUND);
    }
}