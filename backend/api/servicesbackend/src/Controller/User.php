<?php
namespace Zeedhi\ApiServices\Controller;

use Zeedhi\ApiServices\Model\Entities\GenAddress;
use Zeedhi\ApiServices\Model\Entities\GenContact;
use Zeedhi\ApiServices\Model\Entities\GenOrganization;
use Zeedhi\ApiServices\Model\Entities\GenDataUpdate;
use Zeedhi\ApiServices\Model\Entities\GenDependent;
use Zeedhi\ApiServices\Model\Entities\GenUser;
use Zeedhi\ApiServices\Model\Entities\GenUserType;
use Zeedhi\ApiServices\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiServices\Model\Entities\GenTag;
use Zeedhi\ApiServices\Model\Entities\PayCreditcard;
use Zeedhi\ApiServices\Service\User as UserService;
use Zeedhi\ApiServices\Service\Auth as AuthService;
use Zeedhi\ApiServices\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiServices\Controller\Integration;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;

class User extends Crud {

protected $dataSourceName = 'user';

    /**
     * __contruct
     *
     * @param AuthService $authService
     */
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }
    /**
     * getUserData
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    public function getUserData(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row[GenOrganization::NRORG];
        $userId     = $row[GenUser::USER_ID];
        $userTypeId = $row[GenUserType::USER_TYPE_ID];
        $tokenInt   = $row['TOKEN'];
    
        $user       = $this->userService->getUserData($userId, $userTypeId, $nrorg);
        if($user->getStatus() === 'I') throw new \Exception('Inactive user!', 99);
        
        $arrayDataSet = "";
        $arrayDataSet = $user->toArray($userTypeId, $nrorg);
        
        if ($userTypeId != 4) {
            $tokenExt   = $row['TOKEN_EXT'];
            $webService = $this->useIntegration($nrorg);
            // $dependentsWebService = $this->getUserDependentsWebService($nrorg, $tokenExt);
            // $arrayDataSet['dependents'] = ($dependentsWebService['status'] == "ok" && isset($dependentsWebService['dependentes'])) ? $dependentsWebService['dependentes'] : "";
        }
        
        $dataSet       = new DataSet('user', $arrayDataSet);
        $response->addDataSet($dataSet);
    }
    
    /**
     * requestAddressChange
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     * @throws Exception
     */
    public function requestAddressChange(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $nrorg = $row[GenAddress::NRORG];
        $cep = $row[GenAddress::CEP];
        $street = $row[GenAddress::STREET];
        $neighborhood = $row[GenAddress::NEIGHBORHOOD];
        $city = $row[GenAddress::CITY];
        $provincy = $row[GenAddress::PROVINCY];
        $number = intval($row[GenAddress::NUMBER]);
        $complement = $row[GenAddress::COMPLEMENT];
        $country = isset($row[GenAddress::COUNTRY]) ? $row[GenAddress::COUNTRY] : 'BRASIL';
        $type = isset($row[GenAddress::TYPE]) ? $row[GenAddress::TYPE] : GenAddress::TYPE_BILLING;
        $document = isset($row[GenDataUpdate::DOCUMENT]) ? $row[GenDataUpdate::DOCUMENT] : NULL;
        $tokenExt = isset($row["TOKEN_EXT"]) ? $row["TOKEN_EXT"] : NULL;

        $this->requestAddressChangeWebService($tokenExt, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $type);
        
        $status = $this->userService->requestAddressChange($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document);
        $dataSet = new DataSet('status', ['status' => $status]);
        $response->addDataSet($dataSet);
    }
    
    /**
     * requestUserChange
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     * @throws Exception
     */
    public function requestUserChange(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $firstName = $row[GenUser::FIRST_NAME];
        $lastName = $row[GenUser::LAST_NAME];
        $cpf = $row[GenUser::CPF];
        $nrorg = $row[GenOrganization::NRORG];
        $document = $row[GenDataUpdate::DOCUMENT];
        $status = $this->userService->requestUserChange($userId, $nrorg, $firstName, $lastName, $cpf, $document);
        $dataSet = new DataSet('status', ['status' => $status]);
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * addContactMethod
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function addContactMethod(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $type = isset($row[GenContact::CONTACT_TYPE]) ? $row[GenContact::CONTACT_TYPE] : GenContact::CONTACT_TYPE_MOBILE;
        $number = $row[GenContact::PHONE_NUMBER];
        $contact = $this->userService->addContactMethod($userId, $type, $number);
        $dataSet = new DataSet('contactMethod', $contact->toArray());
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * removeContactMethod
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function removeContactMethod(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $contactMethodId = $row[GenContact::CONTACT_METHOD_ID];
        $contact = $this->userService->removeContactMethod($contactMethodId, $userId);
        $dataSet = new DataSet('contactMethod', $contact->toArray());
        $response->addDataSet($dataSet);
    }
    
    /**
     *
     * verifyMobilePhone
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function verifyMobilePhone(DTO\Request $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $phoneNumber = $row[GenContact::PHONE_NUMBER];
        $areEqual = $this->userService->verifyMobilePhone($userId, $phoneNumber);
        $response->addDataSet(new DataSet('response', ['ARE_EQUAL' => $areEqual]));
    }
    
    /**
     *
     * getContactMethods
     * @param DTO\Request $request
     * @param DTO\Response $response
     */
    public function getContactMethods(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $cpf = isset($row[GenUser::CPF]) ? $row[GenUser::CPF] : NULL;
        $npf = isset($row[GenUserTypeRel::EXTERNAL_ID]) ? $row[GenUserTypeRel::EXTERNAL_ID] : NULL;
        if ($cpf == NULL && $npf == NULL) {
            throw Exception::missingParameters();
        } else if ($cpf != NULL) {
            $contactMethods = $this->userService->getContactMethodsByCpf($cpf);
        } else {
            $contactMethods = $this->userService->getContactMethodsByExternalId($npf);
        }
        if ($contactMethods == NULL) {
            $contactMethods = [];
        }
        $response->addDataSet(new DataSet('contactMethods', $contactMethods));
    }
    
    /**
     * getUserDependents
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     */
    public function getUserDependents(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $nrorg = $row[GenOrganization::NRORG];
        
        $dependents = $this->userService->getUserDependents($userId, $nrorg);
        $userDataSet = new DataSet('dependents', GenDependent::manyToArray($dependents));
        $response->addDataSet($userDataSet);
    }
    
    /**
     * 
     * 
     */
    public function addDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $monthlyLimit = isset($row[GenDependent::MONTHLY_LIMIT]) ? $row[GenDependent::MONTHLY_LIMIT] : 200;
        $receiptsTo = isset($row[GenDependent::RECEIPTS_TO]) ? $row[GenDependent::RECEIPTS_TO] : NULL;
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->addDependent($userId, $dependentId, $monthlyLimit, $receiptsTo, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * removeDependent
     */
    public function removeDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->removeDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * enableDependent
     */
    public function enableDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->enableDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * disableDependent
     */
    public function disableDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->disableDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * enableDependentForMainCard
     */
    public function enableDependentForMainCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->enableDependentForMainCard($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    /**
     * disableDependentForMainCard
     */
    public function disableDependentForMainCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $dependent = $this->userService->disableDependentForMainCard($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function editDependentMonthlyLimit(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $monthlyLimit = $row[GenDependent::MONTHLY_LIMIT];
        $this->userService->editDependentMonthlyLimit($userId, $dependentId, $nrorg, $monthlyLimit);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function editDependentReceiptsTo(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = isset($row[GenDependent::DEPENDENT_ID]) ? $row[GenDependent::DEPENDENT_ID] : NULL;
        $nrorg = $row[GenOrganization::NRORG];
        $receiptsTo = $row[GenDependent::RECEIPTS_TO];
        $this->userService->editDependentReceiptsTo($userId, $dependentId, $nrorg, $receiptsTo);
        $dataSet = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function getMonthExpensesFromDependent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $dependentId = $row[GenDependent::DEPENDENT_ID];
        $nrorg = $row[GenOrganization::NRORG];
        $totalExpenses = $this->userService->getMonthExpensesFromDependent($userId, $dependentId, $nrorg);
        $dataSet = new DataSet('response', ['total' => $totalExpenses]);
        $response->addDataSet($dataSet);
    }
    
    public function setMainCreditCard(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $userId = $row[GenUser::USER_ID];
        $cardId = $row['CREDITCARD_ID'];
        $this->userService->setMainCreditCard($userId, $cardId);
        $dataSet = new DataSet('response', ['status' => "Ok"]);
        $response->addDataSet($dataSet);
    }
    
    /**
     * CRISTIANO Integração
     * Este metodo sincroniza o DB local com o MTC, atravez de WEBSERVICE
     */
    public function synchronizationLocalWebService($userId, $userTypeId, $nrorg, $tokenExt, $tokenInt) {
        $useIntegration = $this->useIntegration($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        if($useIntegration){
            $organization = $this->getOrganization($nrorg);
            $response = $consumeWebServiceMTC->webServiceDadosAssociado($organization, $tokenExt);
            
            $user = $this->userService->getUserData($userId, $userTypeId, $nrorg);
            if($user->getStatus() === 'I') throw new \Exception('Inactive user!', 99);
            if($response['res']['status'] == "ok") {
                $result = $this->verifyDateOfChange($response['res']['data_alteracao'], $user->getModifiedAt());

                if(empty($user->getFirstName()) || $result){
                    $this->updateGenUserWebService($userId, $nrorg, $response);
                    $this->createOrUpdateAddress($userId, $response['res'], $nrorg);
                    $arrayUserIdDependents = $this->createOrUpdateGenUserDependentWebService($userId, $response['res'], $nrorg) ;
                    if ($arrayUserIdDependents) {
                        foreach ($arrayUserIdDependents as $value) {
                            $this->userService->addDependent($userId, $value, 0, $response['res']['email'], $nrorg);
                        }
                    }
                }
            }elseif($response['res']['status'] == "erro" && $response['res']['des_erro'] == "Sua sessão expirou! Favor efetue login novamente!") {
                throw Exception::unauthenticatedUserInWebservice();
            }else {
                throw new Exception($response['res']['des_erro'], 1002);
            }
        }
    }
    
    /*verifica a qual organization um user pertence*/
    public function getOrganization($nrorg) {
        $organization = $this->userService->getOrganization($nrorg);
        
        if(isset($organization)){
            switch ($organization->getNrorg()) {
                case 0:
                    $response = "teknisa";
                    break;
                case 1:
                    $response = "minas";
                    break;
                case 2:
                    $response = "nautico";
                    break;
            }
        }else {
            throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    /* Verifica se data externa e maior que a interna!!!*/
    public function verifyDateOfChange($dataExt, $dataInt) {
        $newFormat  = str_replace("/", "-", $dataExt);
        $newDataExt = new \DateTime($newFormat);
        
        return ($newDataExt > $dataInt);
    }
    
    /*Atualiza o Usuario e contatos */
    public function updateGenUserWebService($userId, $nrorg, $response){
        unset($response['res']['enderecos']);
        unset($response['res']['dependentes']);
        $fullName    = trim($response['res']['nome']);
        $fullName    = explode(" ", $fullName, 2);
        
        $firstName   = $fullName[0];
        $lastName    = $fullName[1];
        $externalId  = trim($response['res']['npf']);
        $email       = trim($response['res']['email']);
        $image       = trim($response['res']['foto_url']);
        $phoneNumber = trim($response['res']['telefone_celular']);
        $cpf         = trim($response['res']['cpf']);
        $type        = "MOBILE";
        $userType    = 1;
        
        $this->userService->updateGenUserWebService($userId, $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userType);
        $this->userService->updateContactMethod($userId, $type, $phoneNumber);
    }
        
    /*createOrUpdate Addresses*/
    public function createOrUpdateAddress($userId, $response, $nrorg) {
        $getAddresses = $this->userService->getUserAddress($userId, $nrorg);
       /*verifica se user não tem endereço*/
        if(!empty($getAddresses)){
   
                $street         = $response['enderecos']['cobranca']['logradouro'];
                $neighborhood   = $response['enderecos']['cobranca']['bairro'];
                $city           = $response['enderecos']['cobranca']['cidade'];
                $provincy       = $response['enderecos']['cobranca']['uf'];
                $number         = $response['enderecos']['cobranca']['numero'];
                $complement     = $response['enderecos']['cobranca']['complemento'];
                $cep            = $response['enderecos']['cobranca']['cep'];
                $country        = $response['enderecos']['cobranca']['pais'];
                $type           = "BILLING";
                $status         = "A";
            /*inseri o endereco de cobrança*/
            $this->userService->requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
            
                $street         = $response['enderecos']['correspondencia']['logradouro'];
                $neighborhood   = $response['enderecos']['correspondencia']['bairro'];
                $city           = $response['enderecos']['correspondencia']['cidade'];
                $provincy       = $response['enderecos']['correspondencia']['uf'];
                $number         = $response['enderecos']['correspondencia']['numero'];
                $complement     = $response['enderecos']['correspondencia']['complemento'];
                $cep            = $response['enderecos']['correspondencia']['cep'];
                $country        = $response['enderecos']['correspondencia']['pais'];
                $type           = "CORRESPONDENCE";
                $status         = "A";
            /*inseri o endereco de correspondencia*/
            $this->userService->requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
        }else {
            if($response['enderecos']['cobranca']) {
                $street         = $response['enderecos']['cobranca']['logradouro'];
                $neighborhood   = $response['enderecos']['cobranca']['bairro'];
                $city           = $response['enderecos']['cobranca']['cidade'];
                $provincy       = $response['enderecos']['cobranca']['uf'];
                $number         = $response['enderecos']['cobranca']['numero'];
                $complement     = $response['enderecos']['cobranca']['complemento'];
                $cep            = $response['enderecos']['cobranca']['cep'];
                $country        = $response['enderecos']['cobranca']['pais'];
                $type           = "BILLING";
                $status         = "A";
                /*atualiza o endereco de cobrança*/
                $this->userService->createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
                
            }if($response['enderecos']['correspondencia']) {
                $street         = $response['enderecos']['correspondencia']['logradouro'];
                $neighborhood   = $response['enderecos']['correspondencia']['bairro'];
                $city           = $response['enderecos']['correspondencia']['cidade'];
                $provincy       = $response['enderecos']['correspondencia']['uf'];
                $number         = $response['enderecos']['correspondencia']['numero'];
                $complement     = $response['enderecos']['correspondencia']['complemento'];
                $cep            = $response['enderecos']['correspondencia']['cep'];
                $country        = $response['enderecos']['correspondencia']['pais'];
                $type           = "CORRESPONDENCE";
                $status         = "A";
                /*atualiza o endereco de correspondencia*/
                $this->userService->createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type);
            }
        }
    }
    
    /*create Dependents*/
    public function createOrUpdateGenUserDependentWebService($userId, $response, $nrorg) {
        $arrayUserIdDependents = "";
        /*verifica se associado não tem dependentes*/
        if(!empty($response['dependentes'])) {
            /*percorre os dependentes*/
            foreach ($response['dependentes'] as $dependent) {
                    $user        = $this->userService->getUserWebService($dependent['cpf'], $dependent['npf']);
                    $fullName    = trim($dependent['nome']);
                    $fullName    = explode(" ", $fullName, 2);
                    
                    $firstName   = $fullName[0];
                    $lastName    = $fullName[1];
                    $externalId  = $dependent['npf']                         ? trim($dependent['npf'])      : "";
                    $email       = $dependent['email'] && $dependent['email'] != $response['email'] ? trim($dependent['email'])    : "";
                    $image       = $dependent['foto_url']                    ? trim($dependent['foto_url']) : "";
                    $cpf         = $dependent['cpf']                         ? trim($dependent['cpf'])      : "";
                    $phoneNumber = "31900000000";
                    $type        = "MOBILE";
                    $password    = "123456";
                    
                  /*se o dependente não existe no db então inseri-o na table GenUser*/
                  if(!empty($user)) {
                        $userType    = 1;
                        $this->userService->updateGenUserWebService($user->getId(), $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userType);
                  }else {/*senão atualiza*/
                        $this->userService->createGenUserDependentWebService($firstName, $lastName, $email, $password, $nrorg, $externalId, $cpf);
                  }
                $dependent = $this->userService->getUserWebService($cpf, $externalId);
                if($cpf) {
                    $arrayUserIdDependents[] = $dependent->getId();
                }else {
                    $arrayUserIdDependents[] = $dependent->getGenUser()->getId();
                }
            }
           return $arrayUserIdDependents;/*retorna um array com os id dos dependentes*/
        }
    }
    
    public function useIntegration ($nrorg) {
        $organization = $this->userService->getOrganization($nrorg);
        
        if (isset($organization)) {
            
            $genConfiguration = $this->userService->getConfiguration($nrorg);
            if(isset($genConfiguration)) {
                return $genConfiguration ? $genConfiguration->getUseIntegration() : false;
            }else {
                throw Exception::organizationNotConfiguration();
            }
        }else {
            throw Exception::organizationNotFound();
        }
    }
    
    /*busca dependentes do titular*/
    public function getUserDependentsWebService ($nrorg, $tokenExt){
        $filial = $this->getOrganization($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        $response = $consumeWebServiceMTC->webServiceDadosAssociado($filial, $tokenExt);
        
        return $response['res'];
    }
    /*CRISTIANO Integração*/
    
    public function getUsersFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $nrorg    = $row['NRORG'];
        $users    = $this->userService->getUsersFromOrganization($nrorg);
        
        $dataSet  = new DataSet('users', GenUser::manyToArray($users, NULL, $nrorg));
        $response->addDataSet($dataSet);
    }
    
    public function getDataUpdateRequests(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $nrorg    = $row[GenOrganization::NRORG];
        
        $requests = $this->userService->getDataUpdateRequests($nrorg);
        $dataSet  = new DataSet('requests', GenDataUpdate::manyToArray($requests));
        
        $response->addDataSet($dataSet);
    }
    
    public function acceptDataUpdateRequest(DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        
        $dataUpdateId = $row['DATA_UPDATE_ID'];
        $userId       = $row['USER_ID'];
        $address      = isset($row['ADDRESS']) ? $row['ADDRESS'] : NULL;
        
        $this->userService->acceptDataUpdateRequest($dataUpdateId, $address, $userId);
        $dataSet  = new DataSet('response', ['status' => 'Ok']);
        
        $response->addDataSet($dataSet);
    }
    
    public function updateUserProfileStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $adminId    = $row[GenUser::USER_ID];
        $userTypeId = $row['USER_TYPE_ID'];
        $nrorg      = $row['NRORG'];
        $status     = $row['STATUS'];
        $userId     = $row['UPDATE_USER_ID'];
        
        $this->userService->updateUserProfileStatus($userId, $userTypeId, $nrorg, $status, $adminId);
        
        $dataSet    = new DataSet('response', ['status' => 'Ok']);

        $response->addDataSet($dataSet);
    }

    public function createUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $adminId    = $row[GenUser::USER_ID];
        $firstName  = isset($row['FIRST_NAME']) ? $row['FIRST_NAME'] : "";
        $lastName   = isset($row['LAST_NAME']) ? $row['LAST_NAME'] : "";
        $cpf        = isset($row['CPF']) ? $row['CPF'] : "";
        $email      = isset($row['EMAIL']) ? $row['EMAIL'] : "";
        $password   = isset($row['PASSWORD']) ? $row['PASSWORD'] : "";
        $nrorg      = isset($row['NRORG']) ? $row['NRORG'] : "";
        $status = "A";
        $userTypeId = 3;
        
        $user = $this->userService->getGenUser($email);
        if(!empty($user)) {
            $genUserTypeRel = $this->userService->getGenUserTypeWB($user->getId(), 3);
            if(empty($genUserTypeRel)){
                $this->userService->addProfileUser($user->getId(), $userTypeId, $nrorg, $status, $adminId);
                throw Exception::userExistsInTheDatabase();
            } else if($genUserTypeRel->getStatus() == 'I'){
                $this->userService->activateUserTypeRel($genUserTypeRel);
            }
        }else {
            $this->userService->createUser($firstName, $lastName, $cpf, $email, $password, $nrorg);
        }
        
        $dataSet    = new DataSet('response', ['status' => 'Ok']);
        $response->addDataSet($dataSet);
    }
    
    public function updateFCMToken(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId = $row['USER_ID'];
        $token  = $row['FCM_TOKEN'];
        $nrorg  = $row['NRORG'];
        
        $organizationUserRel = $this->userService->updateFCMToken($userId, $token, $nrorg);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok', 'Id' => $organizationUserRel->getId()]));
    }
    
    public function requestAddressChangeWebService($tokenExt, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $type) {
        $webService  = $this->useIntegration($nrorg);
        $consumeWebServiceMTC = new ConsumeWebServiceMTC();
        
        if($webService) {
            $branch = "minas";
            $type == "CORRESPONDENCE_ADDRESS" ? $type = 2 : $type = 1;
            
            $returnWS = $consumeWebServiceMTC->changeAddress($branch, $tokenExt, $type, $cep, $provincy, $city, $neighborhood, $street, $number, $complement);
            
            if($returnWS['res']['status'] != "ok") { throw new Exception($returnWS['res']['des_erro'], 1002); }
        }
    }
    
    /* insere um caractere em posição específica */
    function replaceByPos($str, $pos, $c){
        return substr($str, 0, $pos) . $c . substr($str, $pos);
    }
    
    public function getBillsFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        $userId   = $row['USER_ID'];
        
        $bills    = [];
        
        if ($this->useIntegration($nrorg)) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
            $bills                = $consumeWebServiceMTC->getBillsFromUser($tokenExt);
        }
        
        $response->addDataSet(new DataSet('bills', $bills));
    }
    
    public function getBillDetails(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $tokenExt = $row['TOKEN_EXT'];
        $nrorg    = $row['NRORG'];
        $userId   = $row['USER_ID'];
        $billId   = $row['BILL_ID'];
        
        $bill     = [];
        
        if ($this->useIntegration($nrorg)) {
            $consumeWebServiceMTC = new ConsumeWebServiceMTC();
            $bill                 = $consumeWebServiceMTC->getBillDetails($tokenExt, $billId);
        }
        
        $response->addDataSet(new DataSet('bill', $bill));
    }
    
    /**
     * getUserDependents
     *
     * @param DTO\Request\Row $request
     * @param DTO\Response $response
     *
     */
    public function getDependentsData(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $userId     = $row[GenUser::USER_ID];
        $nrorg      = $row[GenOrganization::NRORG];
        
        $dependentsData = $this->userService->getDependentsData($userId, $nrorg);
        $userDataSet    = new DataSet('dependentsData', $dependentsData);
        $response->addDataSet($userDataSet);
    }
    
    public function getUserFromOrganizationAndProfile(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg  = $row['NRORG'];
        $cpf    = isset($row['CPF']) & !empty($row['CPF']) ? $row['CPF'] : NULL;
        $npf    = isset($row['NPF']) & !empty($row['NPF']) ? $row['NPF'] : NULL;
        $email  = isset($row['EMAIL']) & !empty($row['EMAIL']) ? $row['EMAIL'] : NULL;
        
        $user   = $this->userService->getUserFromOrganizationAndProfileOne($nrorg, $cpf, $npf, $email);
        
        $dataSet       = new DataSet('user', $user);
        $response->addDataSet($dataSet);
    }
}

