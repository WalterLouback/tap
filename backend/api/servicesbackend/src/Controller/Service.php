<?php
namespace Zeedhi\ApiServices\Controller;

use Zeedhi\ApiServices\Service\Service as ServiceService;
use Zeedhi\ApiServices\Helpers\General as General;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;
use Zeedhi\ApiServices\Model\Entities\EvtEventSeller;

use Zeedhi\ApiServices\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiServices\Controller\Integration;
use Zeedhi\ApiServices\Controller\Exception;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;

use Zeedhi\ApiGeneral\Helpers\ImageUploader;

class Service extends Crud {
    
    const STORE_ID            = 'STORE_ID';
    const STORE_ABOUT         = 'ABOUT';
    const STORE_IMAGE_COVER   = 'IMAGE_COVER';
    const STORE_IMAGE_LOGO    = 'IMAGE_LOGO';
    const STORE_NAME          = 'NAME';
    const STORE_STATUS        = 'STATUS';
    const STORE_OWNER_ID      = 'OWNER_ID';
    const STORE_OWNER_EMAIL   = 'OWNER_USER_EMAIL';
    
    const STRUCTURE_ID        = 'STRUCTURE_ID';
    const DELIVERS_TO_TABLE   = 'DELIVERS_TO_TABLE';
    const DELIVERS_TO_BALCONY = 'DELIVERS_TO_BALCONY';
    
    const SELLER_TYPE_ID      = 'SELLER_TYPE_ID';
    
    public function __construct(ServiceService $serviceService, ImageUploader $imageUploader) {
        $this->serviceService = $serviceService;
        $this->imageUploader = $imageUploader;
    }
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SERVICE*/
    /*ok*/
    public function getServicesByNrorg(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $userId     = $row['USER_ID'];
        $type       = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE'] : NULL;
        $eventId    = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        
        $services  = $this->serviceService->getServicesByNrorg($nrorg, $type, $eventId);
        
        $dataSet       = new DataSet('services', $services);
        $response->addDataSet($dataSet);
    }
    
    public function getServicesByNrorgBackOffice(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $userId     = $row['USER_ID'];
        $type       = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE'] : NULL;
        $eventId    = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        
        $services  = $this->serviceService->getServicesByNrorgBackOffice($nrorg, $type, $eventId);
        
        $dataSet       = new DataSet('services', $services);
        $response->addDataSet($dataSet);
    }
    
    public function getServicesChildrenByNrorg(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $userId     = $row['USER_ID'];
        $eventId    = $row['EVENT_ID'];
        $type       = $row['TYPE'];
        
        $servicesChildren  = $this->serviceService->getServicesChildrenByNrorgAndEvent($nrorg, $eventId, $type);
        
        $dataSet       = new DataSet('services', $servicesChildren);
        $response->addDataSet($dataSet);
    }
    
    public function createService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['USER_ID']) && !empty($row['USER_ID'])){
            $userId         = $row['USER_ID'];
            $createdBy      = $row['USER_ID'];
            $modifiedBy     = NULL;
        }
        
        $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        $name           = isset($row['NAME']) && !empty($row['NAME']) ? $row['NAME']: NULL;
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS']: NULL;
        $decription     = isset($row['DECRIPTION']) && !empty($row['DECRIPTION']) ? $row['DECRIPTION']: NULL;
        $cancellationFine = isset($row['CANCELLATION_FINE']) && !empty($row['CANCELLATION_FINE']) ? $row['CANCELLATION_FINE']: NULL;
        $cancellationTime = isset($row['CANCELLATION_TIME']) && !empty($row['CANCELLATION_TIME']) ? $row['CANCELLATION_TIME']: NULL;
        $time           = isset($row['TIME']) && !empty($row['TIME']) ? $row['TIME']: NULL;
        $eventId        = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID']: NULL;
        $parentId       = isset($row['PARENT_ID']) && !empty($row['PARENT_ID']) ? $row['PARENT_ID']: NULL;
        $color          = isset($row['COLOR']) && !empty($row['COLOR']) ? $row['COLOR']: NULL;
        $price          = isset($row['PRICE']) && !empty($row['PRICE']) ? $row['PRICE']: NULL;
        $icon           = isset($row['ICON']) && !empty($row['ICON']) ? $row['ICON']: NULL;
        // $image          = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;
        $type           = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE']: NULL;
        $statusApp      = isset($row['STATUS_APP']) && !empty($row['STATUS_APP']) ? $row['STATUS_APP']: NULL;
        $futureDaysToSchedule  = isset($row['FUTURE_DAYS_TO_SCHEDULE']) && !empty($row['FUTURE_DAYS_TO_SCHEDULE']) ? $row['FUTURE_DAYS_TO_SCHEDULE']: NULL;
        $userSchedulingLimit = isset($row['USER_SCHEDULING_LIMIT']) && !empty($row['USER_SCHEDULING_LIMIT']) ? $row['USER_SCHEDULING_LIMIT']: NULL;
        $dependentSchedulingLimit = isset($row['DEPEDENT_SCHEDULING_LIMIT']) && !empty($row['DEPEDENT_SCHEDULING_LIMIT']) ? $row['DEPEDENT_SCHEDULING_LIMIT']: NULL;
        
        $ImageUploader = $this->imageUploader;
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            if (strlen($row['IMAGE']) > 500)
                $image = $ImageUploader->upload($row['IMAGE'], $nrorg);
            else    
                $image = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;;
        }
        
        $service   = $this->serviceService->createService($name, $status, $time, $eventId, $decription, $cancellationFine, 
                                                            $cancellationTime, $nrorg, $createdBy, $modifiedBy, $parentId, $color, 
                                                            $price, $icon, $image, $type, $statusApp, $futureDaysToSchedule,
                                                            $userSchedulingLimit, $dependentSchedulingLimit);
        
        $dataSet       = new DataSet('response', array('serviceId' => $service->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function updateService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['USER_ID']) && !empty($row['USER_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $userId         = $row['USER_ID'];
            $serviceId      = $row['SERVICE_ID'];
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
        }
        
        $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        $name           = isset($row['NAME']) && !empty($row['NAME']) ? $row['NAME']: NULL;
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS']: NULL;
        $decription     = isset($row['DECRIPTION']) && !empty($row['DECRIPTION']) ? $row['DECRIPTION']: NULL;
        $cancellationFine = isset($row['CANCELLATION_FINE']) && !empty($row['CANCELLATION_FINE']) ? $row['CANCELLATION_FINE']: NULL;
        $cancellationTime = isset($row['CANCELLATION_TIME']) && !empty($row['CANCELLATION_TIME']) ? $row['CANCELLATION_TIME']: NULL;
        $time           = isset($row['TIME']) && !empty($row['TIME']) ? $row['TIME']: NULL;
        $eventId        = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID']: NULL;
        $parentId       = isset($row['PARENT_ID']) && !empty($row['PARENT_ID']) ? $row['PARENT_ID']: NULL;
        $color          = isset($row['COLOR']) && !empty($row['COLOR']) ? $row['COLOR']: NULL;
        $price          = isset($row['PRICE']) && !empty($row['PRICE']) ? $row['PRICE']: NULL;
        $icon           = isset($row['ICON']) && !empty($row['ICON']) ? $row['ICON']: NULL;
        // $image          = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;
        $type           = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE']: NULL;
        $statusApp      = isset($row['STATUS_APP']) && !empty($row['STATUS_APP']) ? $row['STATUS_APP']: NULL;
        $futureDaysToSchedule  = isset($row['FUTURE_DAYS_TO_SCHEDULE']) && !empty($row['FUTURE_DAYS_TO_SCHEDULE']) ? $row['FUTURE_DAYS_TO_SCHEDULE']: NULL;
        $userSchedulingLimit = isset($row['USER_SCHEDULING_LIMIT']) && !empty($row['USER_SCHEDULING_LIMIT']) ? $row['USER_SCHEDULING_LIMIT']: NULL;
        $dependentSchedulingLimit = isset($row['DEPEDENT_SCHEDULING_LIMIT']) && !empty($row['DEPEDENT_SCHEDULING_LIMIT']) ? $row['DEPEDENT_SCHEDULING_LIMIT']: NULL;

        $ImageUploader = $this->imageUploader;
        $image      = NULL;
        if (isset($row['IMAGE'])) {
            if (strlen($row['IMAGE']) > 500)
                $image = $ImageUploader->upload($row['IMAGE'], $nrorg);
            else    
                $image = isset($row['IMAGE']) && !empty($row['IMAGE']) ? $row['IMAGE']: NULL;;
        }
        $service   = $this->serviceService->updateService($serviceId, $name, $status, $time, $eventId, $decription, 
                                                            $cancellationFine, $cancellationTime, $nrorg, $createdBy, $modifiedBy, 
                                                            $parentId, $color, $price, $icon, $image, $type, $statusApp, $futureDaysToSchedule,
                                                            $userSchedulingLimit, $dependentSchedulingLimit);
        
        $dataSet       = new DataSet('response', array('serviceId' => $service->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function changeStatusOfService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $userId         = $row['USER_ID'];
        $serviceId      = $row['SERVICE_ID'];
        $nrorg          = $row['NRORG'];
        $status         = $row['STATUS'];
        
        $service   = $this->serviceService->removeService($serviceId, $status, $userId);
        
        $dataSet       = new DataSet('response', array('status' => $service->getStatus()));
        $response->addDataSet($dataSet);
    }
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SERVICE_SELLER_REL*/
    public function createServiceSellerRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['SELLER_ID']) && !empty($row['SELLER_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $sellerId       = $row['SELLER_ID'];
            $createdBy      = $row['USER_ID'];
            $modifiedBy     = NULL;
            $serviceId      = $row['SERVICE_ID'];
            $nrorg          = $row['NRORG'];
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceSellerRel   = $this->serviceService->createServiceSellerRel($sellerId, $serviceId, $nrorg, $createdBy, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('serviceSellerRelId' => $serviceSellerRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function updateServiceSellerRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['SERVICE_SELLER_REL_ID']) && !empty($row['SERVICE_SELLER_REL_ID'])) {
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
            $nrorg          = $row['NRORG'];
            $sellerId       = isset($row['SELLER_ID']) && !empty($row['SELLER_ID']) ? $row['SELLER_ID'] : NULL;
            $serviceId      = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
            $serviceSellerRelId = $row['SERVICE_SELLER_REL_ID'];
            $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : " ";
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceSellerRel   = $this->serviceService->updateServiceSellerRel($serviceSellerRelId, $sellerId, $serviceId, $nrorg, $createdBy, $modifiedBy, $status);
        
        $dataSet            = new DataSet('response', array('serviceSellerRelId' => $serviceSellerRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function getServiceSellerRelsBySeller(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $sellerId   = $row['SELLER_ID'];
        
        $serviceSellerRels  = $this->serviceService->getServiceSellerRelsBySeller($sellerId, $nrorg);
        
        $data       = array();
        
        foreach($serviceSellerRels as $serviceSellerRel) {
            array_push($data, $serviceSellerRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceSellerRels', $data);
        $response->addDataSet($dataSet);
    }
    
    public function getServiceSellerRelsByService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $serviceId  = $row['SERVICE_ID'];
        $eventId    = $row['EVENT_ID'];
        
        $serviceSellerRels  = $this->serviceService->getServiceSellerRelsByService($serviceId, $eventId, $nrorg);
        
        $data       = array();
        
        foreach($serviceSellerRels as $serviceSellerRel) {
            array_push($data, $serviceSellerRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceSellerRels', $data);
        $response->addDataSet($dataSet);
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SERVICE_STRUCTURE_REL*/
    public function createServiceStructureRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $structureId    = $row['STRUCTURE_ID'];
            $createdBy      = $row['USER_ID'];
            $serviceId      = $row['SERVICE_ID'];
            $nrorg          = $row['NRORG'];
            $modifiedBy     = NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceStructureRel = $this->serviceService->createServiceStructureRel($structureId, $serviceId, $nrorg, $createdBy, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('serviceStructureRelId' => $serviceStructureRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function updateServiceStructureRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $structureId    = $row['STRUCTURE_ID'];
            $createdBy      = $row['USER_ID'];
            $serviceId      = $row['SERVICE_ID'];
            $nrorg          = $row['NRORG'];
            $modifiedBy     = NULL;
            $serviceStructureRelId = $row['SERVICE_STRUCTURE_REL_ID'];
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceStructureRel = $this->serviceService->updateServiceStructureRel($serviceStructureRelId, $structureId, $serviceId, $nrorg, $createdBy, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('serviceStructureRelId' => $serviceStructureRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function getServiceStructureRelsByStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $structureId = $row['STRUCTURE_ID'];
        
        $serviceStructureRels  = $this->serviceService->getServiceStructureRelsByStructure($structureId, $nrorg);
        
        $data       = array();
        
        foreach($serviceStructureRels as $serviceStructureRel) {
            array_push($data, $serviceStructureRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceStructureRels', $data);
        $response->addDataSet($dataSet);
    }
    
    public function getServiceStructureRelsByService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $serviceId  = $row['SERVICE_ID'];
        
        $serviceStructureRels  = $this->serviceService->getServiceStructureRelsByService($serviceId, $nrorg);
        
        $data       = array();
        
        foreach($serviceStructureRels as $serviceStructureRel) {
            array_push($data, $serviceStructureRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceStructureRels', $data);
        $response->addDataSet($dataSet);
    }
    /*FIM*/
    
    /*METODOS AUXILIARES*/
    /*converte uma string em uma object DateTime*/
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }
    
    /*pegar data atual menos 1 dia*/
    public function getCurretDate(){
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d ' . '23:59:55');
        $newDate    = new \DateTime($date);
        $newDate->sub(new \DateInterval('P1D'));
        
        return $newDate;
    }
    
    public function getEventsFromOrganizationAndType(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        $nrorg       = $row['NRORG'];
        $type        = $row['TYPE'];
        
        $events = $this->serviceService->getEventsFromOrganizationAndType($nrorg, $type);
        
        $response->addDataSet(new DataSet('events', $events));
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SERVICE_EVENT_REL*/
    /*Adicionei paramentros, é necessario alterar o service admin*/
    public function createServiceEventRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $eventId        = $row['EVENT_ID'];
            $createdBy      = $row['USER_ID'];
            $modifiedBy     = NULL;
            $serviceId      = $row['SERVICE_ID'];
            $nrorg          = $row['NRORG'];
            $initialTime    = isset($row['INITIAL_TIME']) && !empty($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : NULL;
            $finalTime      = isset($row['FINAL_TIME']) && !empty($row['FINAL_TIME']) ? $row['FINAL_TIME'] : NULL;
            $interval       = isset($row['INTERVAL']) && !empty($row['INTERVAL']) ? $row['INTERVAL'] : NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceEventRel   = $this->serviceService->createServiceEventRel($eventId, $serviceId, $nrorg, $createdBy, $modifiedBy, $initialTime, $finalTime, $interval);
        
        $dataSet            = new DataSet('response', array('serviceEventRelId' => $serviceEventRel->getId()));
        $response->addDataSet($dataSet);
    }
    /*Adicionei paramentros, é necessario alterar o service admin*/
    public function updateServiceEventRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) && isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID'])){
            $eventId        = $row['EVENT_ID'];
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
            $serviceId      = $row['SERVICE_ID'];
            $nrorg          = $row['NRORG'];
            $serviceEventRelId = $row['SERVICE_EVENT_REL_ID'];
            $initialTime    = isset($row['INITIAL_TIME']) && !empty($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : " ";
            $finalTime      = isset($row['FINAL_TIME']) && !empty($row['FINAL_TIME']) ? $row['FINAL_TIME'] : " ";
            $interval       = isset($row['INTERVAL']) && !empty($row['INTERVAL']) ? $row['INTERVAL'] : " ";
            $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $serviceEventRel   = $this->serviceService->updateServiceEventRel($serviceEventRelId, $eventId, $serviceId, $nrorg, $createdBy, $modifiedBy, $initialTime, $finalTime, $interval, $status);
        
        $dataSet            = new DataSet('response', array('serviceEventRelId' => $serviceEventRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function getServiceEventRelsByEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $eventId    = $row['EVENT_ID'];
        
        $serviceEventRels  = $this->serviceService->getServiceEventRelsByEvent($eventId, $nrorg);
        
        $data       = array();
        
        foreach($serviceEventRels as $serviceEventRel) {
            array_push($data, $serviceEventRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceEventRels', $data);
        $response->addDataSet($dataSet);
    }
    
    public function getServiceEventRelsByService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $serviceId  = $row['SERVICE_ID'];
        
        $serviceEventRels  = $this->serviceService->getServiceEventRelsByService($serviceId, $nrorg);
        
        $data       = array();
        
        foreach($serviceEventRels as $serviceEventRel) {
            array_push($data, $serviceEventRel->toArray());
        }
        
        $dataSet       = new DataSet('serviceEventRels', $data);
        $response->addDataSet($dataSet);
    }
    /*FIM*/
    
    
    function getProvidersAssociatedToUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $userId = $row['USER_ID'];
        $nrorg  = $row['NRORG'];
        
        /* Obter as lojas */
        $providers = $this->serviceService->getProvidersAssociatedToUser($userId, $nrorg);
        
        $response->addDataSet(new DataSet('services', $providers));
    }
    
    function getProviders(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parametros da requisicao */
        $nrorg       = isset($row[General::NRORG])        ? $row[General::NRORG] : NULL;
        $ownerUser   = isset($row[General::PROFILE_ID])   ? $row[General::PROFILE_ID] : NULL;
        $structureId = isset($row[General::STRUCTURE_ID]) ? $row[General::STRUCTURE_ID] : NULL;
        $parentId    = isset($row[General::PARENT_ID])    ? $row[General::PARENT_ID]    : NULL;
        $storeName   = isset($row[General::STORE_NAME])   ? $row[General::STORE_NAME]   : NULL;
        $initial     = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max         = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $showAll     = isset($row['SHOW_ALL'])            ? $row['SHOW_ALL']   : NULL;
        
        /* Obter as lojas */
        $stores = $this->serviceService->getProviders($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll, $initial, $max);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('stores', EvtEvent::manyToArray($stores)));
    }
    
    function getProviderData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parametros sao validos */
        $expectedParameters = [General::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getStoreData', $expectedParameters);
        }
        
        /* Obter parametros da requisicao */
        $storeId = $row[General::STORE_ID];
        
        /* Instanciar Loja */
        $store = new EvtEvent();
        $store->setId($storeId);
        
        /* Obter os produtos e os metodos de pagamento */
        $store          = $this->serviceService->getProviderData($store);
        
        if ($store == NULL) {
            throw new Exception('The requested store doest not exist!', 484);
        }
        
        $response->addDataSet(new DataSet('store', $store->toArray()));
    }
    
    function updateProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID, self::STORE_ABOUT, self::STORE_NAME];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('updateStore', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId    = $row[self::STORE_ID];
        $name       = $row[self::STORE_NAME];
        $about      = $row[self::STORE_ABOUT];
        $type       = isset($row['TYPE']) ? $row['TYPE'] : NULL;
        $imageCover = isset($row[self::STORE_IMAGE_COVER]) ? $row[self::STORE_IMAGE_COVER] : NULL;
        $imageLogo  = isset($row[self::STORE_IMAGE_LOGO]) ? $row[self::STORE_IMAGE_LOGO] : NULL;

        /* Alterar loja */
        $store = $this->serviceService->updateProvider($storeId, $about, $imageCover, $imageLogo, $name, $type);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('store', $store->toArray()));
    }
    
    function createProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        try {
            $name              = $row[self::STORE_NAME];
            $about             = $row[self::STORE_ABOUT];
            $structureId       = $row[self::STRUCTURE_ID];
            $ownerEmail        = $row[self::STORE_OWNER_EMAIL];
            $imageCover        = isset($row[self::STORE_IMAGE_COVER]) ? $row[self::STORE_IMAGE_COVER] : NULL;
            $imageLogo         = isset($row[self::STORE_IMAGE_LOGO]) ? $row[self::STORE_IMAGE_LOGO] : NULL;
            $status            = isset($row[self::STORE_STATUS]) ? $row[self::STORE_STATUS] : 'A';
            $adminId           = $row['USER_ID'];
            $type              = isset($row['TYPE']) ? $row['TYPE'] : NULL;
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 2);
        }
        
        /* Criar loja */
        $store = $this->serviceService->createProvider($name, $about, $structureId, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $type);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function getEventsFromOrganizationAndTypeVersionAdminBackend(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        $nrorg       = $row['NRORG'];
        $type        = $row['TYPE'];
        
        $events = $this->serviceService->getEventsFromOrganizationAndTypeVersionAdminBackend($nrorg, $type);
        
        $response->addDataSet(new DataSet('events', $events));
    }
    
    function getEventSellersFromProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $storeId = $row["STORE_ID"];
        
        $sellers = $this->serviceService->getEventSellersFromProvider($storeId);
        
        $response->addDataSet(new DataSet('users', EvtEventSeller::manyToArray($sellers)));
    }
    
    function unlinkUserFromProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $storeId = $row["STORE_ID"];
        $userId  = $row['REMOVED_USER_ID'];
        $adminId = $row['USER_ID'];
        
        $this->serviceService->unlinkUserFromProvider($storeId, $userId, $adminId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
}

