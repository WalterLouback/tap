<?php
namespace Zeedhi\ApiServices\Controller;

use Zeedhi\ApiServices\Helpers\General as General;

use Zeedhi\ApiServices\Service\Order as OrderService;
use Zeedhi\ApiServices\Service\Exception as Exception;

use Zeedhi\ApiServices\Model\Entities\OrdOption;
use Zeedhi\ApiServices\Model\Entities\OrdProduct;
use Zeedhi\ApiServices\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiServices\Model\Entities\OrdOrder;
use Zeedhi\ApiServices\Model\Entities\OrdItemExtra;
use Zeedhi\ApiServices\Model\Entities\OrdExtra;
use Zeedhi\ApiServices\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiServices\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;
use Zeedhi\ApiServices\Model\Entities\EvtEventSeller;
use Zeedhi\ApiServices\Model\Entities\PayCreditcard;
use Zeedhi\ApiServices\Model\Entities\GenStatus;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiServices\Helpers\Util;

class Order {
    
    /** @var orderService */
    protected $orderService;
    
    /** @var general */
    protected $general;
    
    const ORDER_ID           = 'ORDER_ID';
    const ORDER_ITEMS        = 'ORDER_ITEMS';
    const ORDER_STATUS       = 'ORDER_STATUS';
    
    /**
     * __contruct
     *
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService, General $general) {
        $this->orderService = $orderService;
        $this->general      = $general;
    }
    
    /**
     * finishOrder
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function finishOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[General::ORDER_ITEMS];
        $localValue       = $row[General::TOTAL_VALUE];
        $deliverTo        = $row[General::DELIVER_TO];
        $userId           = $row[General::USER_ID];
        $storeId          = $row[General::STORE_ID];
        $nrorg            = $row[General::NRORG];
        $paymentMethod    = $row[General::PAYMENT_METHOD];
        $structureId      = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID]   : NULL;
        $creditCardId     = isset($row[General::CREDIT_CARD_ID]) ? $row[General::CREDIT_CARD_ID] : NULL;
        $note             = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrder($paymentMethod, $status, $localValue, $deliverTo, $note, $userId, $storeId, $nrorg, $creditCardId, $structureId, false);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true, true);
        /* Making financial transaction */
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId);
        /* Sending response */
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'status' => $order->getStatus()->toString(),
            'paymentStatus' => $order->getPaymentStatus()
        ]));
    }
    
    function finishOrderWithExternalId(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[General::ORDER_ITEMS];
        $localValue       = $row[General::TOTAL_VALUE];
        $deliverTo        = $row[General::DELIVER_TO];
        $sellerId         = $row[General::USER_ID];
        $storeId          = $row[General::STORE_ID];
        $nrorg            = $row[General::NRORG];
        $clientNPF        = $row['EXTERNAL_ID'];
        $structureId      = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID]   : NULL;
        $note             = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderWithExternalId($status, $localValue, $deliverTo, $note, $sellerId, $storeId, $nrorg, $structureId, $clientNPF, false);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true, false);
        /* Making financial transaction */
        $this->orderService->makePaymentTransaction($order);
        /* Sending response */
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'status' => $order->getStatus()->toString(),
            'paymentStatus' => $order->getPaymentStatus()
        ]));
    }
    
    function addItemsToOrderSheet(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        sleep(5);
        $userId = $row['USER_ID'];
        $eventId = $row['EVENT_ID'];
        $orderId = $row['ORDER_ID'];
        $products = $row['PRODUCTS'];
        $totalValue = $row['TOTAL_VALUE'];
        
        if(!isset($products[0])) {
            $products = [$products];
        }
        $orderOld   = $this->orderService->getOrder($orderId);
        $order      = $this->orderService->addItemsToOrderSheet($userId, $eventId, $orderOld, $products, $totalValue);
        $response->addDataSet(new DataSet('order', $order));
    }
    
    function updateItemToOrderSheet(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId         = $row['USER_ID'];
        $orderId        = $row['ORDER_ID'];
        $orderProductId = $row['ORDER_PRODUCT_ID'];
        $totalValue     = $row['TOTAL_VALUE'];
        $quantity       = $row['QUANTITY'];
        
        $orderOld   = $this->orderService->getOrder($orderId);
        
        if(!empty($orderOld->toArray()["items"])) {
            foreach($orderOld->toArray()["items"] as $el) {
                if($el["id"] == $orderProductId) {
                    $totalValueOld  = $el["total"];
                }
            }
        }
        
        $order      = $this->orderService->updateItemToOrderSheet($userId, $orderOld, $orderProductId, $quantity, $totalValue, $totalValueOld);
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
     
     /**
     * getOrdersFromUser
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $initial       = isset($row[General::FIRST_RESULT])   ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        
        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUser($userId, $structureId, $storeId, $nrorg, $initial, $max);

        $response->addDataSet(new DataSet('orders', OrdOrder::manyToArray($orders)));
    }
    
    function getOrdersFromUserFilter(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['FINAL_DATE'])   ? $row['FINAL_DATE']     : NULL;
        $finalDate     = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE']   : NULL;
        $status        = isset($row['ORDER_STATUS']) ? $row['ORDER_STATUS']   : NULL;

        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUserFilter($storeId, $userId, $nrorg, $initialDate, $max, $finalDate, $status);
        $orderFactory   = OrdOrder::manyToArray($orders);
        $factoredOrders = [];
        foreach($orderFactory as $order) {
            $result = $order;
            $result['initial_status'] = $this->orderService->getOrdWorkflow($order['storeId'])->getInitialStatus()->getId();
            array_push($factoredOrders, $result);
        }
        
        $response->addDataSet(new DataSet('orders', $factoredOrders));
    }
    
    /**
     * getOrdersReport
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    
    function getOrdersReport(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersReport', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID]) && $row[General::STRUCTURE_ID] != '' && count($row[General::STRUCTURE_ID]) > 0  ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID]) && $row[General::STORE_ID] != '' && count($row[General::STORE_ID]) > 0 ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $orderTypes    = isset($row['ORDER_TYPES']) ? $row['ORDER_TYPES'] : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) && $row['INITIAL_DATE'] != ''  ? $row['INITIAL_DATE']     : NULL;
        $finalDate     = isset($row['FINAL_DATE']) && $row['FINAL_DATE'] != '' ? $row['FINAL_DATE']   : NULL;
        $orderStatus   = isset($row['ORDER_STATUS']) && $row['ORDER_STATUS'] != '' && count($row['ORDER_STATUS']) > 0 ? $row['ORDER_STATUS'] : NULL;
        $deliverTo     = isset($row['DELIVER_TO'])  && $row['DELIVER_TO'] != '' ? $row['DELIVER_TO']   : NULL;
        $paymentStatus = isset($row['PAYMENT_STATUS']) && $row['PAYMENT_STATUS'] != '' && count($row['PAYMENT_STATUS']) > 0 ? $row['PAYMENT_STATUS'] : NULL;
        $paymentMethod = isset($row['PAYMENT_METHOD'])  && $row['PAYMENT_METHOD'] != '' ? $row['PAYMENT_METHOD']   : NULL;
        $sellerIds     = isset($row['SELLER_IDS'])  && $row['SELLER_IDS'] != '' ? $row['SELLER_IDS']   : NULL;
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 200;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        $statusIds     = NULL;

        /* Get orders */
        $orders         = $this->orderService->getOrdersReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethod, $orderTypes, $sellerIds, $itemsPerPage, $page);
        $orderFactory   = OrdOrder::manyToArrayReport($orders);
        
        $dataSourceName = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? 'getOrdersReport' : 'orders';
        $response->addDataSet(new DataSet($dataSourceName, $orderFactory));
    }
    
     /**
     * getOrdersFromStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromStore(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::STORE_ID, General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $storeId       = $row[General::STORE_ID];
        $userId        = $row[General::USER_ID];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $initialTime   = isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : NULL;
        $finalDate     = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        $finalTime     = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : NULL;
        $cashier       = isset($row['CASHIER']) ? $row['CASHIER'] : NULL;
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromStore($storeId, $userId, $initial, $max, $initialDate, $initialTime, $finalDate, $finalTime, $cashier);
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
     /**
     * getOrdersFromEvent
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromEvent(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::STORE_ID, General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $storeId       = $row[General::STORE_ID];
        $userId        = $row[General::USER_ID];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $order         = isset($row['ORDER']) ? $row['ORDER'] : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $initialTime   = isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : NULL;
        $finalDate     = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        $finalTime     = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : NULL;
        $cashier       = isset($row['CASHIER']) ? $row['CASHIER'] : NULL;
        
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromEvent($storeId, $userId, $order, $initial, $max, $initialDate, $initialTime, $finalDate, $finalTime, $cashier);
        
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
    function setOrderServiceToNextStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId        = $row[General::ORDER_ID];
        $paymentMethod  = isset($row["PAYMENT_METHOD"]) && !empty($row["PAYMENT_METHOD"]) ? $row["PAYMENT_METHOD"] : NULL;
        $clientId       = isset($row["CLIENT_ID"]) && !empty($row["CLIENT_ID"]) ? $row["CLIENT_ID"] : NULL;
        
        $status = $this->orderService->setOrderToNextStatus($orderId, $paymentMethod, $clientId);

        $response->addDataSet(new DataSet("response", [ 'status' => $status, 'transaction' => 'ok' ]));
    }
    
    function cancelOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId = $row[General::ORDER_ID];
        $cancellationReason = ['id' => "", 'message' => ""];
        if(isset($row['CANCELLATION_ID'])){
            $cancellationReason['id']      = $row['CANCELLATION_ID'];
            $cancellationReason['message'] = $row['MESSAGE_CANCELLATION'];
        }
        
        $this->orderService->cancelOrder($orderId, $cancellationReason);
        
        $response->addDataSet(new DataSet("response", [ 'status' => 'Ok']));
    }
     
     /**
     * getOrderData
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrderData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::ORDER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrderData', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $orderId = $row[General::ORDER_ID];
        $userId  = $row[General::USER_ID];
        
        /* Get data from the order */
        $order   = $this->orderService->getOrderData($orderId, $userId);
        $orderData = $order != NULL ? $order->toArray() : [];
        
        $response->addDataSet(new DataSet('order', $orderData));
    }
    
    function getOrdersFromUserInProgress(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
    
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['FINAL_DATE'])   ? $row['FINAL_DATE']     : NULL;
        $finalDate     = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE']   : NULL;
        
        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUserInProgress($storeId, $userId, $nrorg, $initialDate, $max, $finalDate);
        $orderFactory   = OrdOrder::manyToArray($orders);
        $factoredOrders = [];
        
        foreach($orderFactory as $order) {
            $result = $order;
            $result['initial_status'] = $this->orderService->getOrdWorkflow($order['storeId'])->getInitialStatus()->getId();
            array_push($factoredOrders, $result);
        }
        
        $response->addDataSet(new DataSet('orders', $factoredOrders));
    }
    
    function startOrderService(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Getting the request's parameters */
        $userId          = $row[General::USER_ID];
        $total           = $row[General::TOTAL_VALUE];
        $eventId         = $row[General::EVENT_ID];
        $nrorg           = $row[General::NRORG];
        $paymentMethod   = $row[General::PAYMENT_METHOD];
        $creditCardId    = isset($row[General::CREDIT_CARD_ID]) ? $row[General::CREDIT_CARD_ID] : NULL;
        $note            = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $clientId        = $row[General::CLIENT_ID];
        $orderSheet      = $row[General::ORDER_SHEET];
        $services        = $row['SERVICES'];
        
        /*verifica se existe uma order para uma comanda*/
        $existOrderSheet = $this->orderService->getOrderByOrderSheet($clientId, $orderSheet, $nrorg);
        
        if(!empty($existOrderSheet)) {
            $order  = $existOrderSheet;
            $msg    = "there is a record for this user with this order sheet!";
        } else {
            /* Instantiating the order without flushing */
            $order = $this->orderService->createOrderService($paymentMethod, $total, $note, $clientId, $eventId, $nrorg, $creditCardId, true, $orderSheet, $userId);
            $orderProduct = $this->orderService->associateServicesOrder($order, $services, true);
            $msg    = "order of service created with success!";
            /* Making financial transaction */
            $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId);
            /* Sending response */
        }
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'msg' => $msg
        ]));
    }
    
    function getOrdersFromServicesByDay(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::EVENT_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $eventId       = $row[General::EVENT_ID];
        $userId        = $row[General::USER_ID];
        $dateTime      = $row["DATETIME"];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromServicesByDay($eventId, $userId, $dateTime, $initial, $max);
        
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
    function removeOrderProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId    = $row["ORDER_ID"];
        $nrorg      = $row["NRORG"];
        $productId  = $row["PRODUCT_ID"];
        
        $order      = $this->orderService->getOrder($orderId);
        
        $result = $this->orderService->removeOrderProduct($order->getId(), $productId);
        
        $response->addDataSet(new DataSet("response", [ 'status' => 'Ok', 'order' => $result]));
    }
    
    function startOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
       
        $userId          = $row[General::USER_ID];
        $eventId         = $row[General::EVENT_ID];
        $nrorg           = $row[General::NRORG];
        $clientId        = $row[General::CLIENT_ID];
        $orderSheet      = $row[General::ORDER_SHEET];
         
        $existOrderSheet = $this->orderService->getOrderByOrderSheet($clientId, $orderSheet, $nrorg);/*verifica se existe uma order para uma comanda*/
         
        if(!empty($existOrderSheet)) {
           $order  = $existOrderSheet;
           $msg    = "there is a record for this user with this order sheet!";
        } else {
           $sellerId = $this->orderService->getSeller($userId, $eventId);
           $order = $this->orderService->createOrderService(null, null, null, $clientId, $eventId, $nrorg, null, true, $orderSheet, $sellerId);/* Instantiating the order without flushing */
           $msg    = "order of service created with success!";
        }
        
        $response->addDataSet(new DataSet('order', [ 'id' => $order->getId(), 'msg' => $msg ]));
     }
    
    function getStoreSalesStatistics(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $storeId     = $row['STORE_ID'];
        $initialDate = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $finalDate   = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        
        $statistics  = $this->orderService->getStoreSalesStatistics($storeId, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet("statistics", $statistics));
    }
}