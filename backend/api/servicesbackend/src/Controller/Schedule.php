<?php
namespace Zeedhi\ApiServices\Controller;

use Zeedhi\ApiServices\Service\Schedule as ScheduleService;
use Zeedhi\ApiServices\Service\Order as OrderService;
use Zeedhi\ApiServices\Service\Notification as NotificationService;

use Zeedhi\ApiServices\Controller\ConsumeWebServiceMTC;
use Zeedhi\ApiServices\Controller\Integration;
use Zeedhi\ApiServices\Controller\Exception;
use Zeedhi\ApiServices\Controller\Order;
use Zeedhi\ApiServices\Helpers\Util;

use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;

class Schedule extends Crud {

    public function __construct(ScheduleService $scheduleService, OrderService $orderService, NotificationService $notificationService) {
        $this->scheduleService = $scheduleService;
        $this->orderService = $orderService;
        $this->notificationService = $notificationService;
    }
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SCHEDULE*/
    public function getSchedulesByNrorgAndService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $userId     = $row['USER_ID'];
        $serviceId  = $row['SERVICE_ID'];
        
        $schedules  = $this->scheduleService->getSchedulesByNrorgAndService($serviceId, $nrorg);
        
        $dataSet       = new DataSet('schedules', $schedules);
        $response->addDataSet($dataSet);
    }
    
    public function getSchedulesByNrorgAndServiceSeller(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $userId             = $row['USER_ID'];
        $serviceSellerId    = $row['SERVICE_SELLER_ID'];
        
        $schedules  = $this->scheduleService->getSchedulesByNrorgAndServiceSeller($serviceSellerId, $nrorg);
        
        $dataSet       = new DataSet('schedules', $schedules);
        $response->addDataSet($dataSet);
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SCHEDULE_USER_REL*/
    public function confirmSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if(isset($row['USER_ID']) && !empty($row['USER_ID'])) {
            $modifiedBy         = NULL;
            $userId             = $row['USER_ID'];
            $createdBy          = $row['USER_ID'];
            $nrorg              = $row['NRORG'];
            $serviceId          = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
            $serviceSellerId    = isset($row['SERVICE_SELLER_ID']) && !empty($row['SERVICE_SELLER_ID']) ? $row['SERVICE_SELLER_ID'] : NULL;
            $timeTimesheetId    = isset($row['TIME_TIMESHEET_ID']) && !empty($row['TIME_TIMESHEET_ID']) ? $row['TIME_TIMESHEET_ID'] : NULL;
            $type               = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE']: NULL;
            $appointment        = isset($row['APPOINTMENT']) && !empty($row['APPOINTMENT']) ? $row['APPOINTMENT']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $time               = $this->scheduleService->getTimeTimeSheet($timeTimesheetId);
        
        $initialDate        = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getInitialTime());
        $finalDate          = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getFinalTime());
        
        /*cria o registro na agenda*/
        $schedule   = $this->scheduleService->createSchedule($initialDate, $finalDate, $type, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timeTimesheetId);
        
        $scheduleId         = $schedule->getId();
        /*associa o registro criado anteriormente ao usuario*/
        $scheduleUserRel    = $this->scheduleService->scheduleUser($userId, $scheduleId, $nrorg, $createdBy, $modifiedBy);
        $msg = ['Successful scheduling'];
        
        $dataSet            = new DataSet('response', array('message' => $msg, 'scheduleId' => $scheduleId));
        $response->addDataSet($dataSet);
    }
    
    public function createSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
    
        $modifiedBy         = NULL;
        $type               = NULL;
        $userId             = $row['CLIENT_ID'];
        $createdBy          = isset($row['USER_ID']) && !empty($row['USER_ID']) ? $row['USER_ID'] : NULL;
        $nrorg              = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG'] : NULL;
        $serviceId          = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
        $serviceSellerId    = isset($row['SERVICE_SELLER_ID']) && !empty($row['SERVICE_SELLER_ID']) ? $row['SERVICE_SELLER_ID'] : NULL;
        $appointment        = isset($row['APPOINTMENT']) && !empty($row['APPOINTMENT']) ? $row['APPOINTMENT'] : NULL;
        $structureId        = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : NULL;
        
        if(!empty($row['TIME_TIMESHEET_ID'])) {
            if (!is_array($row['TIME_TIMESHEET_ID'])) $row['TIME_TIMESHEET_ID'] = [ $row['TIME_TIMESHEET_ID'] ]; 
            foreach($row['TIME_TIMESHEET_ID'] as $timeTimesheetId) {
                $timesheetId    = intval($timeTimesheetId);
                $time           = $this->scheduleService->getTimeTimeSheet($timesheetId);
                
                $initialDate    = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getInitialTime());
                $finalDate      = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getFinalTime());
        
                // $thereIsOccurrence = $this->scheduleService->getScheduleUserRelsByUser($userId, $serviceSellerId, $timesheetId, $nrorg);
                $thereIsOccurrence = $this->scheduleService->checkIfThereIsSchedule($nrorg, $serviceId, $timesheetId, $serviceSellerId, $initialDate->format('Y-m-d H:i:s'));
                
                if($thereIsOccurrence == 0) {
                    /*cria o registro na agenda*/
                    $schedule   = $this->scheduleService->createSchedule($initialDate, $finalDate, $type, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timesheetId, $structureId);

                    $scheduleId     = $schedule->getId();
                    /*associa o registro criado anteriormente ao usuario*/
                    $scheduleUserRel    = $this->scheduleService->scheduleUser($userId, $scheduleId, $nrorg, $createdBy, $modifiedBy);
                    $msg = array('message' => 'Successful scheduling', 'scheduleId' => $scheduleId, 'code' => 9548);
                } else {
                    $msg = array('message' => 'There is a schedule for this user at this time and with this professional', 'code' => 9549);
                }
                
                $dataSet            = new DataSet('response', $msg);
                $response->addDataSet($dataSet);
            }
        }else {
            throw Exception::invalidParameters();
        }
    }
    
    public function updateSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
    
        $serviceSellerId    = $row['SERVICE_SELLER_ID'];
        $scheduleId         = $row['SCHEDULE_ID'];
        $modifiedBy         = NULL;
        $userId             = isset($row['CLIENT_ID']) && !empty($row['CLIENT_ID']) ? $row['CLIENT_ID'] : NULL;
        $createdBy          = isset($row['USER_ID']) && !empty($row['USER_ID']) ? $row['USER_ID'] : NULL;
        $nrorg              = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG'] : NULL;
        $serviceId          = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
        $type               = isset($row['TYPE']) && !empty($row['TYPE']) ? $row['TYPE'] : NULL;
        $appointment        = isset($row['APPOINTMENT']) && !empty($row['APPOINTMENT']) ? $row['APPOINTMENT'] : NULL;
        $structureId        = isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID']) ? $row['STRUCTURE_ID'] : NULL;
        
        if(isset($row['TIME_TIMESHEET_ID']) && !empty($row['TIME_TIMESHEET_ID'])) {
            $timeTimesheetId    = intval($row['TIME_TIMESHEET_ID']);
            $time               = $this->scheduleService->getTimeTimeSheet($timeTimesheetId);
            
            $initialDate        = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getInitialTime());
            $finalDate          = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getFinalTime());
            
            $schedule = $this->scheduleService->updateSchedule($scheduleId, $initialDate, $finalDate, $type, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timeTimesheetId);
        }
        
        $schedule = $this->scheduleService->updateSchedule($scheduleId, null, null, $type, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, null);
        
        if(!empty($schedule)) {
            $msg = array('message' => 'Successful scheduling', 'scheduleId' => $scheduleId, 'code' => 9548);
        } else {
            $msg = array('message' => 'There is a schedule for this user at this time and with this professional', 'code' => 9549);
        }
        
        $dataSet = new DataSet('response', $msg);
        $response->addDataSet($dataSet);
    }
    
    public function cancelSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if((isset($row['USER_ID']) && !empty($row['USER_ID'])) && 
            (isset($row['SCHEDULE_ID']) && !empty($row['SCHEDULE_ID'])) &&
            (isset($row['DATE_APPOINTMENT']) && !empty($row['DATE_APPOINTMENT']))  && 
            (isset($row['SERVICE_NAME']) && !empty($row['SERVICE_NAME']))) {
                
            $userId          = $row['USER_ID'];
            $createdBy       = NULL;
            $modifiedBy      = $row['USER_ID'];
            $scheduleId      = $row['SCHEDULE_ID'];
            $dateAppointment = $row['DATE_APPOINTMENT'];
            $serviceName     = $row['SERVICE_NAME'];
            $nrorg           = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $result = $this->checkIfServicehasCancellationFee($scheduleId);
        
        if($result['hasFee'] === true) {
            
            // if($this->applyCancellationFee($result['initialDate'], $result['time'])) {
            //     /*COBRAR TAXA DE CANCELAMENTO*/ 
            // }
            $msg = ['Successful cancellation'];
        }else {
            $msg = ['successful cancellation at no charge'];
            
        }
        $scheduleUserRel    = $this->scheduleService->cancelSchedule($userId, $scheduleId, $nrorg, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('message' => $msg));
        $response->addDataSet($dataSet);
        
        $notificationData = ['scheduleId', $scheduleId];
        $status = "P";
        $title = "Seu agendamento do dia ". $dateAppointment ." foi cancelado";
        $message = "O seu ". $serviceName ." foi cancelada.";
        $originNotification = "Cancel_Schedule";
        $datetime = new \DateTime();
        // $firebaseToken = $scheduleUserRel->getGenUser()->getFirebaseToken();
        
        $genUser = $this->orderService->getUserByIdAndNrorg($userId, $scheduleUserRel->getNrorg());
        
        $this->notificationService->inserirNotification($status, $message, $nrorg, $genUser[0]["userId"], $originNotification, $datetime, $notificationData, $title, $genUser[0]["firebaseToken"]);
        
    }
    
    public function cancelScheduleBackoffice(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if((isset($row['CLIENT_ID']) && !empty($row['CLIENT_ID'])) && (isset($row['SCHEDULE_ID']) && !empty($row['SCHEDULE_ID']))){
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
            $userId         = $row['CLIENT_ID'];
            $scheduleId     = $row['SCHEDULE_ID'];
            $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $result = $this->checkIfServicehasCancellationFee($scheduleId);
        
        if($result['hasFee'] === true) {
            
            // if($this->applyCancellationFee($result['initialDate'], $result['time'])) {
            //     /*COBRAR TAXA DE CANCELAMENTO*/
            // }
            $msg = ['Successful cancellation'];
        }else {
            $msg = ['successful cancellation at no charge'];
            
        }
        $scheduleUserRel    = $this->scheduleService->cancelSchedule($userId, $scheduleId, $nrorg, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('message' => $msg));
        $response->addDataSet($dataSet);
    }
    
    public function checkInSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if( !empty($row['CLIENT_ID']) && !empty($row['SCHEDULE_ID']) && !empty($row["ORDER_SHEET"]) ){
            $userId         = $row['CLIENT_ID'];
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
            $scheduleId     = $row['SCHEDULE_ID'];
            $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        /*verifica se data do check-in e igual a data de entrada.*/
        $this->makeSureCheckinDateEqualsScheduledDate($scheduleId);
        
        $scheduleUserRels = $this->scheduleService->getScheduleUserRelsByUser($userId, null, null, $nrorg, $scheduleId);
        
        $row['SELLER_ID']   = $scheduleUserRels['SERVICES'][0]["SELLER_ID"];
        $row["TOTAL_VALUE"] = $scheduleUserRels["TOTAL_PRICE"];
        $row['NOTE']        = NULL;
        $row['SERVICES']    = [];
        
        foreach($scheduleUserRels['SERVICES'] as $service) {
            array_push($row['SERVICES'], ["ID" => $service["ID"], "QUANT" => 1]);
        }
        
        $order      = $this->startOrderService($row);
        
        if(strlen($order["id"]) > 30 && $order['code'] == 12567) {
            
            $scheduleUserRel    = $this->scheduleService->checkInSchedule($userId, $scheduleId, $nrorg, $modifiedBy);
            
            if($scheduleUserRel !== NULL){
                $msg        = ['Check-in confirmed'];
                $dataSet    = new DataSet('response', array('message' => $msg, 'order' => $order));
            }else {
                $msg        = ['Check in already done'];
                $dataSet    = new DataSet('response', array('message' => $msg, 'order' => $order));
            }
                
        } elseif ($order['code'] == 12500) {
            $msg        = ['Unable to check in because order exists'];
            $dataSet    = new DataSet('response', array('error' => $msg, 'order' => $order));
        }
        
        $response->addDataSet($dataSet);
    }
    
    /*ORD_ORDER Inicio*/
    function startOrderService($row) {
        
        /* Getting the request's parameters */
        $sellerId        = $row['SELLER_ID'];
        $clientId        = $row["CLIENT_ID"];
        $eventId         = $row["EVENT_ID"];
        $nrorg           = $row["NRORG"];
        
        $total           = $row["TOTAL_VALUE"];
        $note            = isset($row['NOTE']) && !empty($row['NOTE']) ? $row['NOTE'] : "";
        $orderSheet      = $row["ORDER_SHEET"];
        $services        = $row['SERVICES'];
        
        /*verifica se existe uma order para uma comanda*/
        $existOrderSheet = $this->orderService->getOrderByOrderSheet($clientId, $orderSheet, $nrorg);
        
        if($existOrderSheet) {
            $order  = $existOrderSheet[0];
            $msg    = "there is a record for this user with this order sheet!";
            $code   = 12500;
        } else {
            /* Instantiating the order without flushing */
            $order = $this->orderService->createOrderService(null, $total, $note, $clientId, $eventId, $nrorg, null, false, $orderSheet, $sellerId);
            $orderProduct = $this->orderService->associateServicesOrder($order, $services, true);
            $msg    = "order of service created with success!";
            $code   = 12567;
        }
        /* Sending response */
        return array('id' => $order->getId(), 'msg' => $msg, 'code' => $code);
    }
    /*ORD_ORDER Fim*/
    
    public function checkIfServicehasCancellationFee($scheduleId) {
        
        $schedule = $this->scheduleService->getScheduleBySchedule($scheduleId);
        
        if($schedule->getSerService()->getCancellationTime() == null || $schedule->getSerService()->getCancellationTime() == 0) {
            $data = ['hasFee' => false];
        }else {
            $data = [
                'hasFee' => true, 
                'time' => $schedule->getSerService()->getCancellationTime(), 
                'fee' => $schedule->getSerService()->getCancellationFine(), 
                'initialDate' => $schedule->getInitialDate()
            ];
        }
        
        return $data;
    }
    
    public function applyCancellationFee($initialDate, $cancellationTime) {
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d H:i:s');
        $curretDate = new \DateTime($date);
        
        $interval   = $this->calculateIntervalBetweenDate($curretDate, $initialDate);
        
        return intval($cancellationTime) - $interval >= 0 ? true : false;
    }
    
    public function getScheduleUserRelsByUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $userId             = $row['USER_ID'];
        $serviceSellerId    = isset($row['SERVICE_SELLER_ID']) && !empty($row['SERVICE_SELLER_ID']) ? $row['SERVICE_SELLER_ID'] : null;
        $timeTimesheetId    = isset($row['TIME_TIMESHEET_ID']) && !empty($row['TIME_TIMESHEET_ID']) ? $row['TIME_TIMESHEET_ID'] : null;
        
        $scheduleUserRels  = $this->scheduleService->getScheduleUserRelsByUser($userId, $serviceSellerId, $timeTimesheetId, $nrorg);
        
        
        $dataSet       = new DataSet('ScheduleUserRels', $scheduleUserRels);
        $response->addDataSet($dataSet);
    }
    
    public function getAllSchedulesByUser(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $userId             = $row['USER_ID'];
        
        $scheduleUserRels  = $this->scheduleService->getAllSchedulesByUser($userId, $nrorg);
        
        $dataSet       = new DataSet('ScheduleUserRels', $scheduleUserRels);
        $response->addDataSet($dataSet);
    }
    
    public function getScheduleUserRelsBySchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $scheduleId         = $row['SCHEDULE_ID'];
        
        $scheduleUserRels  = $this->scheduleService->getScheduleUserRelsBySchedule($scheduleId, $nrorg);
        
        $dataSet       = new DataSet('ScheduleUserRels', $scheduleUserRels);
        $response->addDataSet($dataSet);
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SCHEDULE_STRUCTURE_REL*/
    public function createScheduleStructureRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if((isset($row['STRUCTURE_ID']) && !empty($row['STRUCTURE_ID'])) && (isset($row['SCHEDULE_ID']) && !empty($row['SCHEDULE_ID']))){
            $structureId    = $row['STRUCTURE_ID'];
            $createdBy      = $row['USER_ID'];
            $modifiedBy     = NULL;
            $scheduleId     = $row['SCHEDULE_ID'];
            $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $scheduleStructureRel    = $this->scheduleService->createScheduleStructureRel($structureId, $scheduleId, $nrorg, $createdBy, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('scheduleStructureRelId' => $scheduleStructureRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    public function updateScheduleStructureRel(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        if((isset($row['USER_ID']) && !empty($row['USER_ID'])) && (isset($row['SCHEDULE_ID']) && !empty($row['SCHEDULE_ID'])) && (isset($row['SCHEDULE_STRUCTURE_REL_ID']) && !empty($row['SCHEDULE_STRUCTURE_REL_ID']))){
            $structureId    = $row['STRUCTURE_ID'];
            $createdBy      = NULL;
            $modifiedBy     = $row['USER_ID'];
            $scheduleId     = $row['SCHEDULE_ID'];
            $scheduleStructureRelId = $row['SCHEDULE_STRUCTURE_REL_ID'];
            $nrorg          = isset($row['NRORG']) && !empty($row['NRORG']) ? $row['NRORG']: NULL;
        } else {
            throw Exception::invalidParameters();
        }
        
        $scheduleStructureRel    = $this->scheduleService->updateScheduleStructureRel($scheduleStructureRelId, $structureId, $scheduleId, $nrorg, $createdBy, $modifiedBy);
        
        $dataSet            = new DataSet('response', array('scheduleStructureRelId' => $scheduleStructureRel->getId()));
        $response->addDataSet($dataSet);
    }
    
    /*INICIO TIMESHEET*/
    public function getVacantTimesheet(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();

        $eventId    = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $serviceId  = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
        $dayOfWeek  = $row['DAY_OF_WEEK'];
        $date       = $this->stringToDate($row['DATE']);
        $nrorg      = $row['NRORG'];
        
        $timesheetIds = $this->scheduleService->getTimesheetId($eventId, $serviceId);
        ;
        $timeTimesheet = $this->scheduleService->getVacantTimesheet($eventId, $dayOfWeek, $date->format('Y-m-d'), $nrorg, $timesheetIds);
        
        $dataSet    = new DataSet('timesTimesheet', $timeTimesheet);
        $response->addDataSet($dataSet);
    }
    
    public function getVacantTimesheetWithInterested(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();

        $eventId    = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $serviceId  = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
        $dayOfWeek  = $row['DAY_OF_WEEK'];
        $date       = $this->stringToDate($row['DATE']);
        $nrorg      = $row['NRORG'];
        
        $timesheetIds = $this->scheduleService->getTimesheetId($eventId, $serviceId);

        $timeTimesheet = $this->scheduleService->getVacantTimesheetWithInterested($eventId, $dayOfWeek, $date->format('Y-m-d'), $nrorg, $timesheetIds);
        
        $dataSet    = new DataSet('timesTimesheet', $timeTimesheet);
        $response->addDataSet($dataSet);
    }
    
    /*FIM*/
    
    /*METODOS QUE BUSCA OS PROFISSIONAIS*/
    public function getProfessionalByHour(DTO\Request\Row $request, DTO\Response $response){
        $row = $request->getRow();
        
        $eventId = $row['EVENT_ID'];
        $serviceId = $row['SERVICE_ID'];
        $nrorg = $row['NRORG'];
        $timeTimesheetId = $row['TIME_TIMESHEET_ID'];
        $dataAppointment = $this->stringToDate($row['DATE_APPOINTMENT']);
        
        $professionals = $this->scheduleService->getDataProfessionalByEvent($eventId, $nrorg);
        
        $availableProfessional = $this->scheduleService->getAvailableProfessionalsByHour($professionals, $serviceId, $timeTimesheetId, $dataAppointment);

        $dataSet    = new DataSet('professional', $availableProfessional);
        $response->addDataSet($dataSet);
    }
    
    public function getVacantProfessional (DTO\Request\Row $request, DTO\Response $response){
        $row = $request->getRow();
        
        $eventId = $row['EVENT_ID'];
        $serviceId = $row['SERVICE_ID'];
        $nrorg = $row['NRORG'];
        $date = isset($row['DATE']) && !empty($row['DATE']) ? $this->stringToDate($row['DATE']) : $this->getDate();
        
        $return = $this->scheduleService->getProfessionalsByService($eventId, $serviceId, $nrorg, $date->format('Y-m-d'));
        
        $professionals = $return[0];
        $unavailable = $return[1];
        
        $dataSet    = new DataSet('professional', [$professionals, $unavailable]);
        $response->addDataSet($dataSet);
        
    }
    
    public function getHourByProfessional (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $professionalId = $row['PROFESSIONAL_ID'];
        $dateAppointment = $this->stringToDate($row['DATE_APPOINTMENT']);
        $dayOfWeek = $row['DAY_OF_WEEK'];
        
        $hours = $this->scheduleService->getHourByProfessional($professionalId, $dateAppointment, $dayOfWeek);
        
        $dataSet    = new DataSet('hours', $hours);
        $response->addDataSet($dataSet);
    }
    
    /*FIM*/
    
    public function getUserFromOrganizationAndProfile(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg  = $row['NRORG'];
        $cpf    = isset($row['CPF']) & !empty($row['CPF']) ? $row['CPF'] : NULL;
        $npf    = isset($row['NPF']) & !empty($row['NPF']) ? $row['NPF'] : NULL;
        $email  = isset($row['EMAIL']) & !empty($row['EMAIL']) ? $row['EMAIL'] : NULL;
        
        $user   = $this->scheduleService->getUserFromOrganizationAndProfile($nrorg, $cpf, $npf, $email);
        
        $dataSet       = new DataSet('user', $user);
        $response->addDataSet($dataSet);
    }
    
    public function getGeneralServiceSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        $nrorg              = $row['NRORG'];
        $eventId            = $row['EVENT_ID'];
        $type               = $row['TYPE'];
        $date               = $row['DATE'];
        $serviceIds         = isset($row['SERVICE_IDS']) && !empty($row['SERVICE_IDS']) ? $row['SERVICE_IDS'] : NULL;
        $serviceSellerIds   = isset($row['SERVICE_SELLER_IDS']) && !empty($row['SERVICE_SELLER_IDS']) ? $row['SERVICE_SELLER_IDS'] : NULL;
        
        $schedules  = $this->scheduleService->getGeneralServiceSchedule($eventId, $type, $serviceSellerIds, $serviceIds, $date);
        
        $dataSet       = new DataSet('schedules', $schedules);
        $response->addDataSet($dataSet);
    }
    
    /*METODOS AUXILIARES*/
    /*converte uma string em uma object DateTime*/
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }

    /*pegar data atual menos 1 dia*/
    public function getCurretDate(){
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d ' . '23:59:55');
        $newDate    = new \DateTime($date);
        $newDate->sub(new \DateInterval('P1D'));
        
        return $newDate;
    }
    
    /*calcula intervalo entre datas qm horas*/
    public function calculateIntervalBetweenDate($curretDate, $finalDate) {
        $interval = $curretDate->diff($finalDate);
        
        $intervalFormat = explode(":", $interval->format('%D:%H:%I:%S'));
        
        return ($intervalFormat[0] * 24) + $intervalFormat[1];
    }
    /*FIM*/
    
    
    public function getTimesheetByProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $providerId     = $row['EVENT_ID'];
        $nrorg          = $row['NRORG'];
        $timesheetId    = $row['TIMESHEET_ID'];
        
        $timeTimesheet  = $this->scheduleService->getTimesheetByProvider($providerId, $nrorg, $timesheetId);
        
        $dataSet    = new DataSet('timeTimesheet', $timeTimesheet);
        $response->addDataSet($dataSet);
    }
    
    public function getTimesheetsByProvider(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $serviceId  = isset($row['SERVICE_ID']) && !empty($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
        $providerId = isset($row['EVENT_ID']) && !empty($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $nrorg      = $row['NRORG'];
        
        $timesheets  = $this->scheduleService->getTimesheetsByProvider($providerId, $nrorg, $serviceId);
        
        $dataSet    = new DataSet('timesheets', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    public function getTimesheetsByService(DTO\Request $request, DTO\Response $response) {
        $row        = Util::getParamsAsArray($request);
        
        $serviceId  = $row['SERVICE_ID'];
        $nrorg      = $row['NRORG'];
        $status     = "A";
        
        $timesheets  = $this->scheduleService->getTimesheetsByService($serviceId, $nrorg, $status);
        
        $dataSet    = new DataSet('timesheets', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    public function getTimes(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $times  = $this->scheduleService->getTimes();
        
        $dataSet    = new DataSet('times', $times);
        $response->addDataSet($dataSet);
    }
    
    public function createTimesheet(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $providerId     = $row['EVENT_ID'];
        $nrorg          = $row['NRORG'];
        $initialTime    = $row['INITIAL_TIME'];
        $finalTime      = $row['FINAL_TIME'];
        $time           = $row['TIME'];
        $name           = $row['NAME'];
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : NULL;
        
        $timesheet  = $this->scheduleService->createTimesheet($providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name);
        
        $dataSet    = new DataSet('timesheet', ['id' => $timesheet->getId(), 'status' => "success"]);
        $response->addDataSet($dataSet);
    }
    
    public function updateTimesheet(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $timesheetId    = $row['TIMESHEET_ID'];
        $providerId     = $row['EVENT_ID'];
        $nrorg          = $row['NRORG'];
        $initialTime    = $row['INITIAL_TIME'];
        $finalTime      = $row['FINAL_TIME'];
        $time           = $row['TIME'];
        $name           = $row['NAME'];
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : NULL;
        
        $timesheet  = $this->scheduleService->updateTimesheet($timesheetId, $providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name);
        if($timesheet != NULL) {
            $dataSet    = new DataSet('timesheet', ['id' => $timesheet->getId(), 'status' => "success"]);
        }else {
            $dataSet    = new DataSet('timesheet', ['status' => "fail"]);
        }
        $response->addDataSet($dataSet);
    }
    
    public function getTimesheetsByNrorg(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        
        $timesheets  = $this->scheduleService->getTimesheetsByNrorg($nrorg);
        
        $dataSet    = new DataSet('timesheets', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    public function removeTimesheetOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $timesheetId      = $row['TIMESHEET_ID'];
        
        $this->scheduleService->removeTimesheetOrganization($timesheetId);
        
        $dataSet    = new DataSet('timesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
    public function removeTimeTimesheetOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $timetimesheetId      = $row['TIME_TIMESHEET_ID'];
        
        $this->scheduleService->removeTimeTimesheetOrganization($timetimesheetId);
        
        $dataSet    = new DataSet('timeTimesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
    public function createTimeTimesheetOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $timesheetId    = $row["TIMESHEET_ID"];
        $times          = $row["TIMES"];
        $dayTimesheetId = isset($row["DAY_OF_WEEK"]) && !empty($row["DAY_OF_WEEK"]) ? $row["DAY_OF_WEEK"] : NULL; 
        
        foreach($times as $time){
            $this->scheduleService->createTimeTimesheetOrganization($time["initialTime"], $time["finalTime"], $nrorg, $dayTimesheetId, $timesheetId);
        }
        
        $dataSet    = new DataSet('timesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
    public function getDayOfWeek(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        
        $daysOfWeek  = $this->scheduleService->getDayOfWeek($nrorg);
        
        $dataSet    = new DataSet('daysOfWeek', $daysOfWeek);
        $response->addDataSet($dataSet);
    }
    
    public function associateTimesheetWithDay(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $days           = $row["DAYS"];
        $timesheetId    = $row["TIMESHEET_ID"];
        
        /*aqui eu pego o id dos registros a serem associados*/
        $timeTimesheetIds = $this->scheduleService->getTimeTimesheetIds($nrorg, $timesheetId);
        
        foreach($timeTimesheetIds as $timeTimesheetId) {
            foreach($days as $day){
                $this->scheduleService->associateTimesheetWithDay($timeTimesheetId, $day);
            }
        }
        
        $dataSet    = new DataSet('timeTimesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
    public function createTimeTimesheet(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $timesheetId    = $row["TIMESHEET_ID"];
        $daysOfWeek     = implode(",", $row["DAYS_OF_WEEK"]);
        $initialTime    = $row["INITIAL_TIME"];
        $finalTime      = $row["FINAL_TIME"];
        $status         = $row["STATUS"];
        $type           = isset($row["TYPE"]) && !empty($row["TYPE"]) ? $row["TYPE"] : '0';
        $max_scheduling = isset($row['max_scheduling']) ? $row['max_scheduling'] : NULL;
        
        $isValidInterval = $this->scheduleService->checkIntervalTimesheet($nrorg, $initialTime, $finalTime, $timesheetId);
        
        if($isValidInterval) {
            
            $existTimeSheet = $this->scheduleService->checkExistingTimesheet($nrorg, $initialTime, $finalTime, $timesheetId, $daysOfWeek, $status, $type);
            
            if(!$existTimeSheet) {
                $this->scheduleService->createTimeTimesheetOrganization($initialTime, $finalTime, $nrorg, $daysOfWeek, $timesheetId, $status, $type , $max_scheduling);
                
                $dataSet    = new DataSet('timesheet', ['status' => 'success']);
            } else {
                $dataSet    = new DataSet('timesheet', ['status' => 'error', 'message' => 'Horário já reservado.']);
            }
        } else {
            $dataSet    = new DataSet('timesheet', ['status' => 'error', 'message' => ['Horário está fora do intervalo deste quadro de horário.']]);
        }
        
        $response->addDataSet($dataSet);
    }
    
    public function updateTimeTimesheet(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $timetimesheetId    = $row["TIME_TIMESHEET_ID"];
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $timesheetId    = $row["TIMESHEET_ID"];
        $daysOfWeek     = implode(",", $row["DAYS_OF_WEEK"]);
        $initialTime    = $row["INITIAL_TIME"];
        $finalTime      = $row["FINAL_TIME"];
        $status         = $row["STATUS"];
        $type           = $row["TYPE"];
        $max_scheduling = isset($row['max_scheduling']) ? $row['max_scheduling'] : NULL;
        
        $this->scheduleService->updateTimeTimesheetOrganization($timetimesheetId, $initialTime, $finalTime, $nrorg, $daysOfWeek, $timesheetId, $status, $type, $max_scheduling);
        
        $dataSet    = new DataSet('timesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
    public function createTimesheetRel(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $eventId        = isset($row["EVENT_ID"]) ? $row["EVENT_ID"] : null ;
        $serviceId      = isset($row["SERVICE_ID"]) ? $row["SERVICE_ID"] : null ;
        $status         = $row["STATUS"];
        $timesheetIds   = $row["TIMESHEET_IDs"];
        
        foreach($timesheetIds as $timesheetId) {
            $result  = $this->scheduleService->createTimesheetRel($timesheetId, $eventId, $nrorg, $status, $userId, $serviceId);
        }
        if($result) {
            $this->scheduleService->myCommit();
            $dataSet    = new DataSet('timesheetRel', ['status' => 'success']);
        }else {
            $dataSet    = new DataSet('timesheetRel', ['error' => [$result]]);
        }
        
        $response->addDataSet($dataSet);
    }
    
    public function removeTimesheetRel(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $userId         = $row["USER_ID"];
        $nrorg          = $row["NRORG"];
        $eventId        = isset($row["EVENT_ID"]) ? $row["EVENT_ID"] : null ;
        $serviceId      = isset($row["SERVICE_ID"]) ? $row["SERVICE_ID"] : null ;
        $status         = "I";
        $timesheetId    = $row["TIMESHEET_ID"];
        
        $result  = $this->scheduleService->removeTimesheetRel($timesheetId, $eventId, $nrorg, $status, $userId, $serviceId);
        $this->scheduleService->myCommit();
        
        $dataSet    = new DataSet('timesheetRel', ['status' => 'success']);
        
        $response->addDataSet($dataSet);
    }
    
    public function relateUserToTennisCourt(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $type           = "INTEREST";
        $nrorg          = $row['NRORG'];
        $serviceId      = $row['SERVICE_ID'];
        $structureId    = $row['STRUCTURE_ID'];
        $externalId     = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : NULL;
        
        if($externalId == NULL && isset($row['USER_ID']) && !empty($row['USER_ID'])) {
            $userId         = $row['USER_ID'];
            $createdBy      = $row['USER_ID'];
        }else {
            $user   = $this->scheduleService->getUserByExternalId($externalId);
            if($user !== NULL && isset($row['TIME_TIMESHEET_ID'])){
                $initialDate1 = $this->stringToDate($row['APPOINTMENT_DATE']);
                $hasInterest = $this->scheduleService->makeSureTheUserHasShownInterestAtTheTimetime($user[0]['id'], $serviceId, $initialDate1->format('Y-m-d'), $row['TIME_TIMESHEET_ID']);
                if($hasInterest) {
                    throw new Exception('user has already marked interest!', 154);
                }
                $userId = $user[0]['id'];
                $createdBy = $user[0]['id'];
            }else {
                throw Exception::loginNotFound();
            }
        }
        
        if(!empty($row['TIME_TIMESHEET_ID'])) {
            $timeTimesheetId    = intval($row['TIME_TIMESHEET_ID']);
            $time               = $this->scheduleService->getTimeTimeSheet($timeTimesheetId);
            
            $initialDate        = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getInitialTime());
            $finalDate          = $this->stringToDate($row['APPOINTMENT_DATE'] . " ". $time->getFinalTime());
        }else {
            throw Exception::invalidParameters();
        }
        
        $thereIsOccurrence = $this->scheduleService->checkIfThereIsSchedule($nrorg, $serviceId, $timeTimesheetId, NULL, $initialDate->format('Y-m-d H:i:s'));
        
        if($thereIsOccurrence == 0) {
            /*cria o registro na agenda*/
            $schedule   = $this->scheduleService->createSchedule($initialDate, $finalDate, $type, NULL, $nrorg, $createdBy, NULL, $serviceId, NULL, $timeTimesheetId, $structureId);
            
            /*associa o registro criado anteriormente ao usuario*/
            $scheduleUserRel    = $this->scheduleService->scheduleUser($userId, $schedule->getId(), $nrorg, $createdBy, NULL);
            
            $scheduleOfUser = $this->scheduleService->getScheduleBySchedule1($schedule->getId());
            $msg = array('message' => 'Successful scheduling', 'scheduleId' => $schedule->getId(), 'code' => 9548, 'scheduleOfUser' => $scheduleOfUser);
        } else {
            $msg = array('message' => 'There is a schedule for this user at this time and with this professional', 'code' => 9549);
        }
        
        $dataSet            = new DataSet('response', $msg);
        $response->addDataSet($dataSet);
    }
    
    public function getTennisCourtsAndTimes(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        if($row['DATE']) {
            $eventId    = $row['EVENT_ID'];
            $serviceId  = isset($row['SERVICE_ID']) ? $row['SERVICE_ID'] : NULL;
            $date       = $row['DATE'];
            $dayOfWeek  = $row['DAYS_OF_WEEK'];
            $data = $this->scheduleService->getTennisCourtsAndTimes($eventId, $date, $dayOfWeek, $serviceId);
        }else {
            throw Exception::invalidParameters();
        }
        
        $dataSet            = new DataSet('response', $data);
        $response->addDataSet($dataSet);
    }
    
    public function getDate() {
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d');
        $newDate    = new \DateTime($date);
        
        return $newDate;
    }
    
    public function makeSureCheckinDateEqualsScheduledDate($scheduleId) {
        $today = $this->getDate();
        
        $schedule = $this->scheduleService->getScheduleBySchedule($scheduleId);
        
        if($schedule->getInitialDate()->format('Y-m-d') != $today->format('Y-m-d')) {
            throw new Exception('check-in date other than scheduled date!', 485);
        }
    }
    
    // CONTROLLERS - CRUD DAS CONFIGURAÇÕES DOS DIAS/HORARIOS DA AGENDA
    public function createConfigSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $providerId     = $row['EVENT_ID'];
        $nrorg          = $row['NRORG'];
        $initialTime    = $row['INITIAL_TIME'];
        $finalTime      = $row['FINAL_TIME'];
        $time           = $row['TIME'];
        $name           = $row['NAME'];
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : NULL;
        
        $timesheet  = $this->scheduleService->createTimesheet($providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name);
        
        $dataSet    = new DataSet('timesheet', ['id' => $timesheet->getId(), 'status' => "success"]);
        $response->addDataSet($dataSet);
    }
    
    public function getConfigSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        
        $timesheets  = $this->scheduleService->getTimesheetsByNrorg($nrorg);
        
        $dataSet    = new DataSet('timesheets', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    public function updateConfigSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $timesheetId    = $row['TIMESHEET_ID'];
        $providerId     = $row['EVENT_ID'];
        $nrorg          = $row['NRORG'];
        $initialTime    = $row['INITIAL_TIME'];
        $finalTime      = $row['FINAL_TIME'];
        $time           = $row['TIME'];
        $name           = $row['NAME'];
        $status         = isset($row['STATUS']) && !empty($row['STATUS']) ? $row['STATUS'] : NULL;
        
        $timesheet  = $this->scheduleService->updateTimesheet($timesheetId, $providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name);
        if($timesheet != NULL) {
            $dataSet    = new DataSet('timesheet', ['id' => $timesheet->getId(), 'status' => "success"]);
        }else {
            $dataSet    = new DataSet('timesheet', ['status' => "fail"]);
        }
        $response->addDataSet($dataSet);
    }
    
    public function removeConfigSchedule(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $timesheetId      = $row['TIMESHEET_ID'];
        
        $this->scheduleService->removeTimesheetOrganization($timesheetId);
        
        $dataSet    = new DataSet('timesheet', ['status' => 'success']);
        $response->addDataSet($dataSet);
    }
    
     /*
      ----- SERVICE MODALITIES TIMESHEET-----
    */    
    public function getAllTimesheetByProviderForTypeModalities(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $timeTimesheet  = $this->scheduleService->getAllTimesheetByProviderForTypeModalities($row);
        $dataSet    = new DataSet('timeTimesheet', $timeTimesheet);
        $response->addDataSet($dataSet);
    }  
    
    public function getTimesheetByProviderForTypeModalities(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $timeTimesheet  = $this->scheduleService->getTimesheetByProviderForTypeModalities($row);
        $dataSet    = new DataSet('timeTimesheet', $timeTimesheet);
        $response->addDataSet($dataSet);
    }
    
    public function createTimeTimesheetForTypeModalities(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $timesheets  = $this->scheduleService->createTimeTimesheetForTypeModalities($row);
        $dataSet    = new DataSet('timesheet', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    public function removeTimeTimesheetOrganizationForTypeModalities(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $timesheets  = $this->scheduleService->removeTimeTimesheetOrganizationForTypeModalities($row);
        $dataSet    = new DataSet('timesheet', $timesheets);
        $response->addDataSet($dataSet);
    }
    
    
}

