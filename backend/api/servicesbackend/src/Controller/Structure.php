<?php
namespace Zeedhi\ApiServices\Controller;

use Zeedhi\ApiServices\Helpers\General as General;

use Zeedhi\ApiServices\Service\Structure as StructureService;
use Zeedhi\ApiServices\Service\Exception as Exception;

use Zeedhi\ApiServices\Model\Entities\GenStructure;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

class Structure {
    
    /** @var StructureService */
    protected $structureService;
    
    /** @var general */
    protected $general;
    
    /**
     * __contruct
     *
     * @param StructureService $StructureService
     */
    public function __construct(StructureService $structureService) {
        $this->structureService = $structureService;
    }
    
    /**
     * getStructures
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStructures(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $nrorg              = $row['NRORG'];
        $event              = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $level              = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : NULL;
        $parentId           = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $initial            = isset($row['FIRST_RESULT']) ? $row['FIRST_RESULT'] : NULL;
        $max                = isset($row['MAX_RESULTS'])  ? $row['MAX_RESULTS'] : NULL;
        $type               = isset($row['TYPE'])  ? $row['TYPE'] : NULL;

        /* Obter estruturas */
        $structures         = $this->structureService->getStructures($nrorg, $parentId, $event, $level, $initial, $max, $type);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structures', GenStructure::manyToArray($structures)));
    }
    
    /**
     * getStructureById
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStructureById(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $id    = $row['STRUCTURE_ID'];
        $nrorg = $row['NRORG'];
        
        /* Obter estrutura */
        $structure = $this->structureService->getStructureById($id, $nrorg);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structure', $structure->toArray()));
    }
    
    /**
     * createStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function createStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obtêm os parametros da requisição*/
        $userId         = $row['USER_ID'];
        $name           = $row['NAME'];
        $nrorg          = $row['NRORG'];
        $description    = isset($row['DESCRIPTION']) ? $row['DESCRIPTION'] : NULL;
        $type           = $row['TYPE'];
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $structureLevel = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : 0;
        $evtEventId     = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;

        /*Endereço da Strutura*/
        $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
        $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
        $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
        $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
        $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
        $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
        
        /*Telefone da estrutura*/
        $phone          = isset($row['PHONE']) ? $row['PHONE'] : NULL;
        $email          = isset($row['EMAIL']) ? $row['EMAIL'] : NULL;
        
        $genAddress = NULL;
        
        /*Cria o endereço*/
        if($street || $number || $neighborhood || $city || $provincy || $cep) {
            $genAddress = $this->structureService->createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg);
        }
        /*Cria unidade*/
        $structure = $this->structureService->createStructure($name, $nrorg, $description, $type, $userId, $parentId, $genAddress, $structureLevel, $evtEventId, $email);

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    /**
     * updateStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function updateStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obtêm os parametros da requisição*/
        $name           = $row['NAME'];
        $type           = $row['TYPE'];
        $nrorg          = $row['NRORG'];
        $status         = $row['STATUS'];
        $userId         = $row['USER_ID'];
        $description    = $row['DESCRIPTION'];
        $structureId    = $row['STRUCTURE_ID'];
        $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
        $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
        $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
        $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
        $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : NULL;
        $genAddressId   = isset($row['ADDRESS_ID']) ? $row['ADDRESS_ID'] : NULL;
        $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
        $evtEventId     = isset($row['$EVT_EVENT_ID']) ? $row['$EVT_EVENT_ID'] : NULL;
        $structureLevel = isset($row['STRUCTURE_LEVEL']) ? $row['STRUCTURE_LEVEL'] : NULL;
        $email          = isset($row['EMAIL']) ? $row['EMAIL'] : NULL;
        // $phone          = $row['PHONE'];
        $genAddress = $genAddressId;
        /*Atualiza o endereço*/
        if(($street || $number || $neighborhood || $city || $provincy || $cep) && ($genAddressId)){
            $genAddress = $this->structureService->updateGenAddress($genAddressId, $nrorg, $street, $number, $neighborhood, $city, $provincy, $cep, $userId);
        } else if((!$genAddressId) && ($street || $number || $neighborhood || $city || $provincy || $cep)) {
            $genAddress = $this->structureService->createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg);   
        }
        
        /*Obtem a unidade*/
        $structure = $this->structureService->updateStructure($structureId, $nrorg, $name, $description, $parentId, $structureLevel, $evtEventId, $type, $status, $userId, $genAddress, $email);
        
        /*Atualiza a unidade*/
        

        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function inactivateStructure(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $this->structureService->inactivateStructure($row['STRUCTURE_ID'], $row['NRORG']);
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    function getPlacesStructures(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $structureId    = $row['STRUCTURE_ID'];
        $nrorg          = $row['NRORG'];
        
        $structures = $this->structureService->getPlacesStructures($nrorg, "U");
        
        $response->addDataSet(new DataSet('structures', GenStructure::manyToArray($structures)));
    }
    
    function getStructuresWithService(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $nrorg  = $row['NRORG'];
        $level  = "0";
        $status = "A";
        
        $structures = $this->structureService->getStructuresWithService($nrorg, $level, $status);
        
        $response->addDataSet(new DataSet('structures', $structures));
    }
}