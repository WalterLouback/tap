<?php
namespace Zeedhi\ApiServices\Service;

use Zeedhi\ApiServices\Model\Entities\OrdOption;
use Zeedhi\ApiServices\Model\Entities\OrdProduct;
use Zeedhi\ApiServices\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiServices\Model\Entities\OrdConfigStore;
use Zeedhi\ApiServices\Model\Entities\OrdWorkflow;
use Zeedhi\ApiServices\Model\Entities\OrdOrder;
use Zeedhi\ApiServices\Model\Entities\OrdOrderRO;
use Zeedhi\ApiServices\Model\Entities\OrdOrderStatusRel;
use Zeedhi\ApiServices\Model\Entities\OrdItemExtra;
use Zeedhi\ApiServices\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiServices\Model\Entities\OrdExtra;
use Zeedhi\ApiServices\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiServices\Model\Entities\GenStatus;
use Zeedhi\ApiServices\Model\Entities\GenUser;
use Zeedhi\ApiServices\Model\Entities\GenDependent;
use Zeedhi\ApiServices\Model\Entities\GenStructure;
use Zeedhi\ApiServices\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;
use Zeedhi\ApiServices\Model\Entities\EvtEventSeller;
use Zeedhi\ApiServices\Model\Entities\PayCreditcard;
use Zeedhi\ApiServices\Model\Entities\PayPaymentMethod;
use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiServices\Model\Entities\GenStatusUserType;
use Zeedhi\ApiServices\Model\Entities\GenOrganizationUserRel;

use Zeedhi\ApiServices\Helpers\DateFormatDql;

use Zeedhi\ApiServices\Service\Notification;

use Zeedhi\ApiServices\Helpers\FCM;

use Zeedhi\ApiPayment\Controller\Payment;

use Doctrine\ORM\EntityManager;

class Order extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment, Payment $payment, Notification $notification) {
        parent::__construct($entityManager, $environment);
        $this->paymentApi   = $payment;
        $this->notification = $notification;
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $cardId = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard()->getId();
        $today  = new \DateTime();
        $firstDayOfMonth   = $today->format('Y-m-d');
        $total = $this->getEntityManager()->createQuery(
        "
        SELECT SUM(o.total) as totalValue
        FROM ". OrdOrderRO::class ." o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createdAt >= '$firstDayOfMonth'
        "
        )->getResult()[0]['totalValue'];
        
        return $total;
    }
    
    function verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $orderValue, $nrorg) {
        if ($paymentMethod != OrdOrder::PAYMENT_METHOD_CREDITCARD || $orderValue == 0) return ;
        
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg]);
        
        if ($dependency == NULL) return ;
        
        $parent  = $dependency->getParent();
        
        if ($parent == NULL) return ;
        
        if ($parent->getMainCreditcard() == NULL) return ;
        
        if ($parent->getMainCreditcard()->getId() != $creditcardId) return ;
        
        $limit       = $dependency->getMonthlyLimit();
        $totalExpent = $this->getMonthExpensesFromDependent($parent->getId(), $userId, $nrorg);
        $canExpend   = $limit - $totalExpent;

        if ($canExpend < $orderValue) throw new Exception("User's monthly limit has exceeded! Limit is $limit, current is $totalExpent", 12);
    }
    
    /* Creates new Order with the passed data */
    function createOrder($paymentMethod, $statusId, $total, $deliverTo, $note, $userId, $eventId, $nrorg, $creditcardId, $structureId, $canFlush=true) {
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $total, $nrorg);
        
        /* Getting necessary entities to set to the order */
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        
        $initialStatus  = $this->getInitialStatus($eventId, $nrorg);
        
        if ($user == NULL) {
            throw new Exception('User not found!');
        }
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setDeliverTo($deliverTo);
        $order->setStatus($initialStatus);
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEvent($event);
        $order->setNote($note);
        $order->setType('ORDER_STORE');
        $order->setNrorg($nrorg);
        if ($structureId  !== NULL) {
            $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
            $order->setGenStructure($structure);
        }
        if ($creditcardId !== NULL) {
            $creditcard     = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId);
            $order->setPayCreditcard($creditcard);
        }
        //$order->setCreateDate(new \DateTime());
        
        //$order->getPaymentMoment()->getId();
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        $this->saveOrderStatus(new \DateTime(), $order->getOrderIdentifier());
        return $order;
    }
    
    /* Creates new Order with the passed data */
    function createOrderWithExternalId($statusId, $total, $deliverTo, $note, $sellerId, $eventId, $nrorg, $structureId, $clientNPF, $canFlush=true) {
        $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['externalId' => $clientNPF]);
        if ($userTypeRel == NULL) {
            throw new Exception('User not found!',19);
        }
        
        $user = $userTypeRel->getGenUser();
        
        /* Getting necessary entities to set to the order */
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $seller         = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $user, 'evtEvent' => $event]);
        
        $initialStatus  = $this->getInitialStatus($eventId, $nrorg);
        
        if ($user == NULL) {
            throw new Exception('User not found!',19);
        }
        if ($user->getMainCreditcard() == NULL) {
            throw new Exception('User\'s main card not found', 20);
        }
        
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($user->getId(), OrdOrder::PAYMENT_METHOD_CREDITCARD, $user->getMainCreditcard()->getId(), $total, $nrorg);
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(OrdOrder::PAYMENT_METHOD_CREDITCARD);
        $order->setDeliverTo($deliverTo);
        $order->setStatus($initialStatus);
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEvent($event);
        $order->setEvtEventSeller($seller);
        $order->setNote($note);
        $order->setType('ORDER_STORE');
        $order->setNrorg($nrorg);
        $order->setPayCreditcard($user->getMainCreditcard());
        if ($structureId  !== NULL) {
            $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
            $order->setGenStructure($structure);
        }
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function getInitialStatus($storeId, $nrorg) {
        /* Check if store has workflow configured */
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore !== NULL ? $configStore->getWorkflow() : NULL;
        if ($workflow != NULL) {
            return $workflow->getInitialStatus();
        }
        /* If not, get organization's default workflow */
        else {
            return $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg])->getInitialStatus();
        }
        return NULL;
    }
    
    function getPaymentMoment($storeId, $nrorg) {
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore !== NULL ? $configStore->getWorkflow() : NULL;
        if ($workflow != NULL) {
            return $workflow->getPaymentMoment();
        } else {
            return $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg])->getPaymentMoment();
        }
        
        return NULL;
    }
    
    /* Creates ProductOrders with the received items */
    function createProductOrders($order, $orderItems, $removeAmount=false, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($orderItems as $item) {
            /* Getting the product referent to the item */
            $product = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($item['productId']);
            if ($removeAmount && $product->getAmount() != NULL) $product->setAmount($product->getAmount() - $item['quant']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setQuantity($item['quant']);
            $po->setTotal($product->getPrice() * $item['quant']);
            $po->setOrdMenuProduct($product);
            $po->setOrdOrder($order);
            $po->setNote($item['note']);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            /* In case any extra is associated to the item */
            if (isset($item['extras'])) {
                $this->createItemExtras($product, $po, $item['extras']);
            }
            
            array_push($productOrders, $po);
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $productOrders;
    }
    
    /* Creates extras and options of a product */
    function createItemExtras($product, $productOrder, $itemExtras) {
        $extrasTotalPrice = 0;

        /* Iterating through the extras of the item sent by the front-end */
        foreach ($itemExtras as $e) {
            /* Getting Extra to set to ItemExtra */
            $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($e['extraId']);
            
            /* Instantiating ItemExtra and setting attributes */
            $itemExtra = new OrdItemExtra();
            $itemExtra->setOrdOrderProduct($productOrder);
            $itemExtra->setOrdExtra($extra);
            
            /* Storing ItemExtra in the database */
            $this->getEntityManager()->persist($itemExtra);
            
            /* Iterating through the selected options of this ItemExtra */
            foreach ($e['selectedOptions'] as $o) {
                /* Getting Option to set to SelectedOption */
                $option = $this->getEntityManager()->getRepository(OrdOption::class)->find($o['id']);
                
                /* Instantiating SelectedOption and setting attributes */
                $selectedOption = new OrdSelectedOption();
                $selectedOption->setOrdOption($option);
                $selectedOption->setOrdItemExtra($itemExtra);
                
                /* Storing SelectedOption in database */
                $this->getEntityManager()->persist($selectedOption);
            }
        }


        $productOrder->setTotal($productOrder->getTotal() + ($extrasTotalPrice * $productOrder->getQuantity()));
    }
    
    /* Gets an user's orders in an organization */
    function getOrdersFromUser($userId, $structureId, $storeId, $nrorg, $initial=NULL, $max=NULL) {
        $order      = OrdOrderRO::class;
        $nrorgQuery = $nrorg !== NULL ? "AND o.nrorg = $nrorg" :  "";
        $storeQuery = $storeId !== NULL ? "AND s.id = $storeId" : "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            WHERE o.genUser = $userId
            AND o.type = 'ORDER_STORE' 
            $nrorgQuery
            $storeQuery
            AND ( o.paymentStatus = 'A' OR o.paymentStatus = 'P' OR o.paymentStatus = 'R')
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets a store's orders */
    function getOrdersFromStore($storeId, $userId, $initial=NULL, $max=NULL, $initialDate=NULL, $initialTime=NULL, $finalDate=NULL, $finalTime=NULL, $cashier=NULL ) {
        $order          = OrdOrderRO::class;
        $item           = OrdOrderProduct::class;
        $product        = OrdProduct::class;
        $extra          = OrdExtra::class;
        $option         = OrdOption::class;
        $user           = GenUser::class;
        $userType       = GenUserType::class;
        $eventSeller    = EvtEventSeller::class;
        $statusUserType = GenStatusUserType::class;
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat   = $this->stringToDate($finalDate);
        
        $initialDateQuery = "";
        if ($initialDate !== NULL) {
            $initialDateQuery = "AND o.createdAt >= '$initialDateFormat";
            $initialDateQuery .= $initialTime ? " $initialTime:00'" : " 00:00:00'";
        }
        
        $finalDateQuery = "";
        if ($finalDate !== NULL) {
            $finalDateQuery = "AND o.createdAt <= '$finalDateFormat";
            $finalDateQuery .= $finalTime ? " $finalTime:59'" : " 23:59:59'";
        }
        $cashierQuery = $cashier ? "AND c.id = $cashier" : "";
        

        /* Buscando pedidos (o) do tipo ORDER_STORE realizados nesta loja (s)
           cujo status (st) possa ser visualizado por esse usuário ($userId)
           com determinado sellerType (seltype) nesta loja. */
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o, st
            FROM $order o
            LEFT JOIN o.evtEvent s 
            LEFT JOIN o.status st
            LEFT JOIN $eventSeller es WITH ( es.genUser = $userId and es.evtEvent = s )
            LEFT JOIN es.evtSellerType seltype
            LEFT JOIN $statusUserType sut WITH sut.genStatus = st.id
            LEFT JOIN sut.evtSellerType seltype_2
            LEFT JOIN o.ordCashier c
            WHERE o.evtEvent = $storeId
            $initialDateQuery
            $finalDateQuery
            $cashierQuery
            AND ( s.type = 'E' OR seltype.id = seltype_2.id )
            AND ( st.next is not null or st.id is null )
            ORDER BY st.id, o.createdAt
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets a store's orders */
    function getOrdersFromEvent($storeId, $userId, $ordering="desc", $initial=NULL, $max=NULL, $initialDate=NULL, $initialTime=NULL, $finalDate=NULL, $finalTime=NULL, $cashier=NULL ) {
        $order          = OrdOrderRO::class;
        $item           = OrdOrderProduct::class;
        $product        = OrdProduct::class;
        $extra          = OrdExtra::class;
        $option         = OrdOption::class;
        $user           = GenUser::class;
        $userType       = GenUserType::class;
        $eventSeller    = EvtEventSeller::class;
        $statusUserType = GenStatusUserType::class;
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat   = $this->stringToDate($finalDate);
        
        $initialDateQuery = "";
        if ($initialDate !== NULL) {
            $initialDateQuery = "AND o.createDate >= '$initialDateFormat";
            $initialDateQuery .= $initialTime ? " $initialTime:00'" : " 00:00:00'";
        }
        
        $finalDateQuery = "";
        if ($finalDate !== NULL) {
            $finalDateQuery = "AND o.createDate <= '$finalDateFormat";
            $finalDateQuery .= $finalTime ? " $finalTime:59'" : " 23:59:59'";
        }
        $cashierQuery = $cashier ? "AND c.id = $cashier" : "";

        /* Buscando pedidos (o) do tipo ORDER_STORE realizados nesta loja (s)
           cujo status (st) possa ser visualizado por esse usuário ($userId)
           com determinado sellerType (seltype) nesta loja. */
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o, st
            FROM $order o
            LEFT JOIN o.evtEvent s 
            LEFT JOIN o.status st
            LEFT JOIN $eventSeller es WITH ( es.genUser = $userId and es.evtEvent = s )
            LEFT JOIN es.evtSellerType seltype
            LEFT JOIN $statusUserType sut WITH sut.genStatus = st.id
            LEFT JOIN sut.evtSellerType seltype_2
            LEFT JOIN o.ordCashier c
            WHERE o.evtEvent = $storeId
            $initialDateQuery
            $finalDateQuery
            $cashierQuery
            AND o.paymentStatus = 'A'
            ORDER BY o.createDate $ordering
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets the informations related to an order */
    function getOrderData($orderId, $userId) {
        if (is_integer($orderId) || is_numeric($orderId)) {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy(['orderIdentifier' => $orderId]);
        } else {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->find($orderId);
        }
        if ($order !== NULL) $order->build($this->getEntityManager());
        return $order;
    }
    
    /* Checks how to procede with the payment of an order and after that changes its status */
    function makePaymentTransaction($order) {
        if ($order->getPaymentStatus() == 'A') return ;
        
        $paymentMethod = $order->getPaymentMethod();
        $creditCardId  = $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL;
        
        $requiresPaymentMethod = $this->requiresPaymentApi($paymentMethod);
        $isPaymentMoment = $this->isPaymentMoment($order->getEvtEvent()->getId(), $order->getStatus()->getId(), $order->getNrorg());

        /* If this payment method is managed by the system */
        if ($requiresPaymentMethod) {
            /* Send the order data to the payment API */
            $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
            $orderId    = $order->getId();
            $value      = $order->getTotal();
            $ccToken    = $creditCard->getToken();
            $customerId = $creditCard->getCustomerId();
            $gateway    = $creditCard->getGateway();
            $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
            $merchant   = $order->getEvtEvent()->getMerchantKey();
            $merchantId = $order->getEvtEvent()->getMerchantId();
            
            $payment    = $this->paymentApi->execPaymentCreditCard($merchant, $merchantId, $gateway, $order);
            
            /* Getting data from the payment */
            $status     = $payment['status'];
            $order->setTransactionId($payment['transaction_id']);
            
            /* Set the payment status */
            $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
            
            /* If the payment was not successful */
            if ($status !== 'captured') {
                throw new Exception('Payment not successful. Got status \'' . $status . '\'', 7);
            }
        } else {
            /* If the payment will not be managed by the system, set status Ok */
            $order->setPaymentStatus('A');
        } 
        
        $this->getEntityManager()->flush();
    } 
    
    /* Checks if it's necessary to call the payment API for this payment method */
    function requiresPaymentApi($paymentMethod) {
        return $paymentMethod == OrdOrder::PAYMENT_METHOD_CREDITCARD || $paymentMethod == OrdOrder::PAYMENT_METHOD_BALANCE;
    }

    /* Checks the store parametrization and returns if the payment should happen at this status */
    function isPaymentMoment($eventId, $status, $nrorg) {
        $isPaymentMoment = $this->getPaymentMoment($eventId, $nrorg)->getId() == $status;

        return $isPaymentMoment;
    }
    
    function setOrderToNextStatus($orderId, $paymentMethod=null, $clientId=null) {
        
        if($paymentMethod !== NULL) {
            $this->addPaymentMethodOrderService($orderId, $paymentMethod, $clientId, $canFlush=true);
        }
        
        $order         = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy([ 'orderIdentifier' => $orderId ]);
        $user          = $this->getUserByIdAndNrorg($order->getGenUser()->getId(), $order->getNrorg());
        $currentStatus = $order->getStatus();
        $nextStatus    = $currentStatus->getNext();

        if ($nextStatus != NULL) {
            $order->setStatus($nextStatus);
            
            $storeName  = $order->getEvtEvent()->getName();
            $statusName = $nextStatus->getLabelName();
            
            $this->makePaymentTransaction($order);
            $this->getEntityManager()->flush();
            $message    = $nextStatus->getNext() != NULL ? "Sua compra em $storeName obteve status $statusName." : "Sua compra em $storeName foi concluída.";

            
            $notificationData = ['orderId' => $orderId];
            
            $title = "Sobre sua compra em $storeName";
            $originNotification = "Order_Orderbackend";
            $status             = "P";
            $dateTime           = new \DateTime();
            
            if ($user !== NULL) {
                try {
                    $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user[0]["userId"], $originNotification, $dateTime, $notificationData, $title, $user[0]["firebaseToken"]);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
            /*Verifica se o valor da order mudou para estornar um pagamento e realizar outro.*/
            $this->checkForChangesInTheOrder($order);
            $this->saveOrderStatus($dateTime, $order->getOrderIdentifier());
            return $order->getStatus()->getId();
        }
    }
    
    function cancelOrder($orderId, $cancellationReason = NULL) {
        if (is_integer($orderId)) {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy(['orderIdentifier' => $orderId]);
        } else {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->find($orderId);
        }
        if ($order == NULL) return ;
        $cancelStatus = $this->getEntityManager()->getRepository(GenStatus::class)->find(7);
        
        /* If the payment was made via credit card, exec revoke */
        if ($order->getPaymentStatus() == 'A' && $order->getPaymentMethod() == OrdOrder::PAYMENT_METHOD_CREDITCARD) {
            /* Getting the gateway and the merchant key from the store */
            $creditCard = $order->getPayCreditcard();
            $gateway    = $creditCard->getGateway();
            $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
            $merchant   = $order->getEvtEvent()->getMerchantKey();
            $merchantId = $order->getEvtEvent()->getMerchantId();
            
            /* Executing revoke */
            try {
                $revokeStatus = $this->paymentApi->revoke($order->getTransactionId(), $order->getTotal(), $gateway, $merchant, $merchantId);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                throw new Exception('Unable to execute revoke', 11);
            }
            if ($revokeStatus['success'] != true) {
                throw new Exception('Unable to execute revoke', 11);
            }
            
            $this->createRevokeOrder($order);
        }
        
        /* Set the cancelled status */
        $order->setStatus($cancelStatus);
        
        /* If the order was a balance purchase, remove balance */
        if ($order->getType() == OrdOrderRO::PAYMENT_TYPE_WALLET) {
            if ($this->startsWith($order->getPaymentMethod(), 'CC_W') || !$this->startsWith($order->getPaymentMethod(), 'BALANCE')) {
                $wallet = $order->getWallet();
                $balance = $wallet->getBalance();
                if ($balance >= $order->getTotal()) $wallet->setBalance($balance - $order->getTotal());
                else $wallet->setBalance(0);
            }
        }
        
        /* If the order was paid with balance, return the balance to the wallet */
        if ($this->startsWith($order->getPaymentMethod(), 'BALANCE')) {
            $wallet = $order->getWallet();
            if ($wallet) $wallet->setBalance($wallet->getBalance() + $order->getTotal());
        }
        
        /* If the order was paid with picpay, execute revoke */
        if ($this->startsWith($order->getPaymentMethod(), 'PICPAY') && $order->getPaymentStatus() == 'A') {
            $referenceId = $order->getId();
            $pm = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $order->getPaymentMethod()]);
            $picpayTransaction = $this->makeCurl("https://appws.picpay.com/ecommerce/public/payments/$referenceId/status", 'GET', [], $pm->getXPicpayToken(), $pm->getXSellerToken());
            
            if (!isset($picpayTransaction['authorizationId']) || !isset($picpayTransaction['status']))
                throw new \Exception('Error while executing picpay revoke', 11);
            if ($picpayTransaction['status'] == 'refunded')
                throw new \Exception('Order already refunded', 11);
            
            $cancellationResult = $this->makeCurl("https://appws.picpay.com/ecommerce/public/payments/$referenceId/cancellations", 'POST', array('authorizationId' => $picpayTransaction['authorizationId']), $pm->getXPicpayToken(), $pm->getXSellerToken());
            
            if (!isset($cancellationResult['cancellationId']))
                throw new \Exception('Error while executing picpay revoke', 11);
        }
        
        $this->getEntityManager()->flush();
        
        $storeName  = $order->getEvtEvent()->getName();
        $user          = $this->getUserByIdAndNrorg($order->getGenUser()->getId(), $order->getNrorg());
        
        if(empty($cancellationReason) || $cancellationReason['id'] == 3) {
            $message    = "Sua compra em $storeName foi cancelada.";
        }else {
            $requestCancel = $cancellationReason['message'];
            $message   = "Infelizmente a loja $storeName teve que cancelar sua compra pelo motivo $requestCancel";
        }
        
        $notificationData = ['orderId' => $orderId];
        
        $title = "Sobre sua compra em $storeName";
        $originNotification = "Order_Orderbackend";
        $status             = "P";
        $dateTime           = new \DateTime();
        
        if ($user !== NULL) {
            try {
                $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user[0]["userId"], $originNotification, $dateTime, $notificationData, $title, $user[0]["firebaseToken"]);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
        
        $this->saveOrderStatus($dateTime, $order->getOrderIdentifier());
    }
    
    public function makeCurl($url, $method, $body, $xPicpayToken, $xSellerToken) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "x-picpay-token: $xPicpayToken",
            "x-seller-token: $xSellerToken"
        ));
        // Will be a POST
        if ($method === "POST") curl_setopt($ch, CURLOPT_POST, 1);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($body) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        return json_decode($result, true);
    }
    
    public function saveOrderStatus($dateTime, $orderId) {
       $orderStatus = new OrdOrderStatusRel();
       $order       = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['orderIdentifier' => $orderId]);
       
       $orderStatus->setNrorg($order->getNrorg());
       $orderStatus->setCreateDate($dateTime);
       $orderStatus->setOrdOrder($order);
       $orderStatus->setGenStatus($order->getStatus());
       $orderStatus->setTotal($order->getTotal());
       
       $this->getEntityManager()->persist($orderStatus);
       $this->getEntityManager()->flush();
    }
    
    private function createRevokeOrder($originalOrder) {
        $cancelStatus = $this->getEntityManager()->getRepository(GenStatus::class)->find(7);
        
        $revokeOrder = new OrdOrder();
        $revokeOrder->setId(OrdOrder::getGUID());
        $revokeOrder->setPaymentMethod($originalOrder->getPaymentMethod());
        $revokeOrder->setDeliverTo($originalOrder->getDeliverTo());
        $revokeOrder->setStatus($cancelStatus);
        $revokeOrder->setTotal($originalOrder->getTotal());
        $revokeOrder->setGenUser($originalOrder->getGenUser());
        $revokeOrder->setEvtEvent($originalOrder->getEvtEvent());
        $revokeOrder->setNote($originalOrder->getNote());
        $revokeOrder->setType('ORDER_STORE');
        $revokeOrder->setNrorg($originalOrder->getNrorg());
        $revokeOrder->setGenStructure($originalOrder->getGenStructure());
        $revokeOrder->setPayCreditcard($originalOrder->getPayCreditcard());
        $revokeOrder->setPaymentStatus('R');
        
        /* Storing revoke order in database */
        $this->getEntityManager()->persist($revokeOrder);
    }
    
    function getOrdWorkflow($storeId) {
        /* Check if store has workflow configured */
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore->getWorkflow();
        
        if ($workflow == NULL) {
            $workflow = $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $configStore->getNrorg()]);
        }
        
        return $workflow;
    }
    
    function getOrdersFromUserFilter($storeId, $userId, $nrorg, $initialDate=NULL, $max=NULL, $finalDate=NULL, $statusIds=NULL) {
        $order          = OrdOrderRO::class;
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);
        
        $status = join($statusIds, "' OR st.labelName = '");
        $statusQuery = $statusIds !== NULL ? "AND (st.labelName = '$status')" : "";

        $storeQuery       = $storeId !== NULL ? "AND s.id  = $storeId" : "";
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            JOIN o.status st
            WHERE o.nrorg = $nrorg
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    function getOrdersReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods, $orderTypes, $sellerIds, $itemsPerPage = 100, $page = 1) {
        $order = OrdOrderRO::class;
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);
        
        $typesOrder = "";
        if (!is_null($orderTypes) && count($orderTypes) > 0) {
            $typesOrder = "AND (";
            foreach ($orderTypes as $orderType) {
                if ($typesOrder != "AND (") $typesOrder .= " or ";
                switch ($orderType) {
                    case 'PRODUCTS':
                        $typesOrder .= "(o.type = 'ORDER_STORE' or o.type = 'ORDER_EVENT' or o.type = 'ORDER_EVENT_SELLER' or (o.type = 'WALLET' AND o.paymentMethod = 'BALANCE'))";
                        break;
                    case 'BALANCE_PURCHASE':
                        $typesOrder .= "(o.type = 'WALLET' AND o.paymentMethod <> 'BALANCE')";
                        break;
                    case 'WALLET_TRANSFER':
                        $typesOrder .= "(o.type = 'WALLET_TRANSFER')";
                        break;
                    case 'SERVICES':
                        $typesOrder .= "(o.type = 'ORDER_SERVICE')";
                        break;
                    case 'TICKETS':
                        $typesOrder .= "(o.type = 'TICKET')";
                        break;
                }
            }
            $typesOrder .= ")";
        }
        
        if (!is_null($orderStatus))
            $statusOrder = join($orderStatus, ' OR st.id = ');
        $statusQuery = $orderStatus !== NULL ? "AND (st.id = $statusOrder)" : "AND (st.id is null or st.id <> 7)";
        
        if (!is_null($paymentStatus))
            $statusPayment = "'". join($paymentStatus, "' OR o.paymentStatus = '") . "'";
        $paymentStatusQuery = $paymentStatus !== NULL ? "AND (o.paymentStatus = $statusPayment)" : ""; 
        
        $paymentMethodQuery = "";
        if(!is_null($paymentMethods) && count($paymentMethods) == 1) {
            $methodPayment = $paymentMethods[0] == 'E' ? "o.paymentMethod like 'EXT%' " : "o.paymentMethod not like 'EXT%' ";
            $paymentMethodQuery = " AND $methodPayment";
        } 
        
        if (!is_null($paymentMethods))
            $paymentMethodsJoin = "'". join($paymentMethods, "' OR o.paymentMethod like '") . "'";
        $paymentMethodsQuery = $paymentMethods !== NULL ? "AND (o.paymentMethod like $paymentMethodsJoin)" : ""; 
            
        if (!is_null($storeId))
            $storeIds = join($storeId, ' OR s.id = ');
        $storeQuery = $storeId !== NULL ? "AND (s.id = $storeIds)" : "";     
        
        if (!is_null($sellerIds))
            $sellerIdsJoin = join($sellerIds, ' OR es.genUser = ');
        $sellerQuery = $sellerIds !== NULL ? "AND (es.genUser = $sellerIdsJoin)" : "";     
        
        if (!is_null($structureId))
            $structureIds = join($structureId, ' OR sr.id = ');
        $structureQuery = $structureId !== NULL ? "AND (sr.id = $structureIds)" : "";  
           
        $deliverToQuery = $deliverTo !== NULL ? "AND (o.deliverTo = '$deliverTo')" : "";
        
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            LEFT JOIN o.evtEvent s
            LEFT JOIN o.status st
            LEFT JOIN o.genStructure sr
            LEFT JOIN o.evtEventSeller es
            WHERE o.nrorg = $nrorg
            AND o.paymentStatus <> 'R'
            $structureQuery
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            $paymentMethodsQuery
            $paymentStatusQuery
            $deliverToQuery
            $typesOrder
            $sellerQuery
            ORDER BY o.createdAt DESC
            "
        ); 
        
        if($page){
            $orders->setFirstResult($itemsPerPage * ($page - 1));
        }else {
            $orders->setFirstResult(0);
        }
        
        if ($itemsPerPage != NULL) $orders->setMaxResults($itemsPerPage);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
    function getStatus($labelName) {
        $status = $this->getEntityManager()->getRepository(GenStatus::class)->findOneBy(['labelName' => $labelName]);
        
        return $status;
    }
    
    function getOrdersFromUserInProgress($storeId, $userId, $nrorg, $initialDate=NULL, $max=NULL, $finalDate=NULL) {
        $order              = OrdOrderRO::class;
        
        $initialDateFormat  = $this->stringToDate($initialDate);
        $finalDateFormat    = $this->stringToDate($finalDate);
        
        $storeQuery       = $storeId        !== NULL ? "AND s.id  = $storeId" : "";
        $initialDateQuery = $initialDate    !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate      !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            JOIN o.status st
            WHERE o.nrorg = $nrorg AND o.genUser = $userId AND st.next is not null AND o.type = 'ORDER_STORE' 
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    private function startsWith ($string, $startString) { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    /* Creates new Order with the passed data */
    function createOrderService($paymentMethod=null, $total, $note, $clientId, $eventId, $nrorg, $creditCardId=null, $canFlush=true, $orderSheet, $sellerId) {
        /* Getting necessary entities to set to the order */
        $seller         = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(["id" => $sellerId]);
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($clientId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        
        if ($user == NULL) {
            throw new Exception('User not found!');
        }
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        
        $order->setId(OrdOrder::getGUID());
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEventSeller($seller);
        $order->setEvtEvent($event);
        $order->setNote($note);
        $order->setType('ORDER_SERVICE');
        $order->setPaymentStatus('P');
        $order->setNrorg($nrorg);
        $order->setOrderSheet($orderSheet);
        $order->setCreatedBy($seller->getGenUser() ->getId());
        $order->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $event->getStructure()->getId()));
        $order->setStatus($this->getEntityManager()->getReference(GenStatus::class, 31));
        
        $paymentMethod  !== NULL ? $order->setPaymentMethod($paymentMethod) : NULL;
        $creditCardId   !== NULL ? $order->setPayCreditcard($this->getEntityManager()->getReference(PayCreditcard::class, $creditCardId)) : NULL;
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function addPaymentMethodOrderService($orderId, $paymentMethod, $clientId, $canFlush=true) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(["orderIdentifier" => $orderId, "paymentStatus" => "P"]);
        
        if($order == NULL) {
            throw new Exception('Order not found!',5);
        }
        
        if($paymentMethod == "CC") {
            
            $user1 = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(["id" => $clientId, "status" => "A"]);
            if($user1 && $user1->getMainCreditcard()) {
                $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($user1->getMainCreditcard());
            }else {
                $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->findOneBy(["genUser" => $clientId, "status" => "A"]);
            }
            
            if($creditCard == NULL) {
                throw new Exception('Inactive card!',53125);
            }
            $order->setPayCreditcard($this->getEntityManager()->getReference(PayCreditcard::class, $creditCard->getId()));
        }
        
        $order->setPaymentMethod($paymentMethod);
        // $order->setPaymentStatus("A");
        
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function addItemsToOrderSheet($userId, $eventId, $order, $products, $totalValue) {
        foreach ($products as $product) {
            $po = new OrdOrderProduct();
            $po->setQuantity($product['quant']);
            $po->setTotal($product['totalItem']);
            $po->setOrdOrder($order);
            $po->setOrdMenuProduct($this->getEntityManager()->getReference(OrdMenuProduct::class, $product['productId']));
            if (isset($product['note'])) $po->setNote($product['note']);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
        }
        
        $order->setTotal(floatval($order->getTotal()) + floatval($totalValue));
        $this->getEntityManager()->persist($order);
        
        $this->getEntityManager()->flush();
        $order->build($this->getEntityManager(), false);
        return $order->toArray();
    }
    
    /**
     * @var $totalValue     => quantidade * preco unitario
     * @var $totalValueOld  => quantidade anterior * preco unitario
     *
     */
    function updateItemToOrderSheet($userId, $order, $orderProductId, $quantity, $totalValue, $totalValueOld) {
        $ordOrderProduct = $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findOneBy(['id' => $orderProductId]);
        
        $ordOrderProduct->setQuantity($quantity);
        $ordOrderProduct->setTotal($totalValue);
        $ordOrderProduct->setModifiedBy($userId);
        
        $newTotal = ( floatval($order->getTotal()) - floatval($totalValueOld) ) + floatval($totalValue);
        $order->setTotal($newTotal);
        
        $this->getEntityManager()->persist($ordOrderProduct);
        $this->getEntityManager()->persist($order);
        
        $this->getEntityManager()->flush();
        $order->build($this->getEntityManager(), false);
        return $order;
    }
    
    /* associate service a order */
    function associateServicesOrder($order, $services, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($services as $service) {
            /* Getting the product referent to the item */
            $serService = $this->getEntityManager()->getRepository(SerService::class)->find($service['ID']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setQuantity($service['QUANT']);
            $po->setNote($serService->getName());
            $po->setTotal($serService->getPrice() * $service['QUANT']);
            $po->setSerService($serService);
            $po->setOrdOrder($order);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            array_push($productOrders, $po);
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $productOrders;
    }
    
    function getOrdersFromServicesByDay($eventId, $userId, $dateTime, $initial=NULL, $max=NULL) {
        $order          = OrdOrder::class;
        $item           = OrdOrderProduct::class;
        $user           = GenUser::class;
        $userType       = GenUserType::class;
        $eventSeller    = EvtEventSeller::class;
        $statusUserType = GenStatusUserType::class;
        
        $date           = $this->stringToDate($dateTime);
        
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o FROM $order o 
            LEFT JOIN o.evtEvent s 
            JOIN o.status st
            JOIN $statusUserType sut WITH sut.genStatus = st.id
            WHERE o.evtEvent = $eventId 
            AND o.type = 'ORDER_SERVICE' AND o.paymentStatus = 'P'
            AND (o.createdAt BETWEEN '$date 00:00:00' AND '$date 23:59:59')
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    function getOrderByOrderSheet($userId, $orderSheet, $nrorg) {
        $ordOrder = OrdOrder::class;
        
        $today  = new \DateTime();
        $date   = $today->format('Y-m-d');
        
        $order = $this->getEntityManager()->createQuery("
            SELECT o FROM $ordOrder o
            WHERE o.nrorg = $nrorg
            AND o.orderSheet = '$orderSheet'
            AND (o.createdAt BETWEEN '$date 00:00:00' AND '$date 23:59:59')
        ")->getResult();
        
        return $order;
    }
    
    function removeOrderProduct($orderId, $productId) {
        $order =  $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['id' => $orderId]);

        $orderProduct = $this->getEntityManager()->createQuery("
            SELECT op FROM " . OrdOrderProduct::class . " op WHERE op.ordOrder = '$orderId' AND op.ordMenuProduct = $productId AND (op.status is null OR op.status <> 'I')
        ")->getResult()[0];
        
        if(empty($orderProduct)) {
            $orderProduct = $this->getEntityManager()->createQuery("
                SELECT op FROM " . OrdOrderProduct::class . " op WHERE op.ordOrder = '$orderId' AND op.serService = $productId AND (op.status is null OR op.status <> 'I')
            ")->getResult()[0];
        }
        
        $newTotal = $order->getTotal() - $orderProduct->getTotal();
        $order->setTotal(floatval($newTotal));
        $orderProduct->setStatus("I");
        
        $this->getEntityManager()->merge($order);
        $this->getEntityManager()->merge($orderProduct);
        
        $this->getEntityManager()->flush();
        $order->build($this->getEntityManager(), false);
        return $order->toArray();
    }
    
    function getOrder($orderId) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['orderIdentifier' => $orderId]);
        if ($order !== NULL) $order->build($this->getEntityManager(), false);
        return $order;
    }
    
    function getStoreSalesStatistics($storeId, $initialDate, $finalDate) {
        $order = OrdOrder::class;
        $orderProduct = OrdOrderProduct::class;
        $paymentMethod = PayPaymentMethod::class;
        $dateFormatDql = DateFormatDql::class;
        
        $config = $this->getEntityManager()->getConfiguration();
        
        $config->addCustomDatetimeFunction('DATE', $dateFormatDql);
        
        $today = new \DateTime();
        $todayString = $today->format('Y-m-d');
        
        $sevenDaysAgo = (new \DateTime())->modify('-6 days');
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');
        
        $dateQuery = "";
        if ($initialDate || $finalDate) {
            if ($initialDate) $dateQuery .= "AND o.createdAt > '$initialDate 00:00:00'";
            if ($finalDate) $dateQuery .= " AND o.createdAt < '$finalDate 23:59:59'";
        }  else {
            // $dateQuery = "AND o.createdAt > '$todayString 00:00:00' AND o.createdAt < '$todayString 23:59:59'";
            $dateQuery = "";
        }
        
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $nrorg = $store ? $store->getNrorg() : NULL;

        $result1 = $this->getEntityManager()->createQuery(
            "
            SELECT SUM(o.total) as salesTotal, COUNT(o.id) as numberOfSales
            FROM $order o
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            $dateQuery
            "
        )->getResult();
        
        $result2 = $this->getEntityManager()->createQuery(
            "
            SELECT mp.name, SUM(op.quantity) as cnt, SUM(op.total) as total
            FROM $order o
            JOIN $orderProduct op WITH op.ordOrder = o
            JOIN op.ordMenuProduct as mp
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            $dateQuery
            GROUP BY mp.id
            "
        )->getResult();
        
        $result3 = $this->getEntityManager()->createQuery(
            "
            SELECT pm.label as name, SUM(o.total) as total
            FROM $order o
            JOIN $paymentMethod pm WITH ( pm.paymentMethod = o.paymentMethod AND pm.nrorg = o.nrorg )
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            $dateQuery
            GROUP BY pm.id
            "
        )->getResult();
        
        $result4 = $this->getEntityManager()->createQuery(
            "
            SELECT DATE(o.createdAt) orderDate, SUM(o.total) as total
            FROM $order o
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            AND o.createdAt > '$sevenDaysAgo 00:00:00' AND o.createdAt < '$todayString 23:59:59'
            AND o.createdAt is not null
            GROUP BY orderDate
            ORDER BY o.createdAt
            "
        )->getResult();
        
        if (count($result1) > 0) {
            $statistics = $result1[0];
            $statistics['salesTotal'] = doubleval($statistics['salesTotal']);
            $statistics['averageTicket'] = $statistics['numberOfSales'] ? $statistics['salesTotal'] / $statistics['numberOfSales'] : 0;
            
            $statistics['salesByProduct'] = $result2;
            $statistics['paymentMethods'] = $result3;
            $statistics['salesByDay'] = $result4;
        } else $statistics = [];
        
        return $statistics;
    }
    
    function getSeller($userId, $eventId) {
        $seller = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(["genUser" => $userId, "evtEvent" => $eventId]);
        
        return $seller ? $seller->getId() : null;
    }
    
    public function getUserByIdAndNrorg($userId, $nrorg) {
        $genUser = GenUser::class;
        $genOrganizationUserRel = GenOrganizationUserRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id userId, our.firebaseToken
            FROM $genUser u
            JOIN $genOrganizationUserRel our WITH (our.nrorg = '$nrorg' AND u = our.genUser)
            WHERE u.id = '$userId' AND u.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    /* Checks how to procede with the payment of an order and after that changes its status */
    function makePaymentTransactionInProgress($order) {
        $paymentMethod = $order->getPaymentMethod();
        $creditCardId  = $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL;
        
        $requiresPaymentMethod = $this->requiresPaymentApi($paymentMethod);
        $isPaymentMoment = $this->isPaymentMoment($order->getEvtEvent()->getId(), $order->getStatus()->getId(), $order->getNrorg());
        
        /* If this payment method is managed by the system */
        if ($requiresPaymentMethod) {
            /* If it's the moment to make the payment, call Payment API */
            if ($isPaymentMoment) {
                /* Send the order data to the payment API */
                $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
                $orderId    = $order->getId();
                $value      = $order->getTotal();
                $ccToken    = $creditCard->getToken();
                $customerId = $creditCard->getCustomerId();
                $gateway    = $creditCard->getGateway();
                $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
                $merchant   = $order->getEvtEvent()->getMerchantKey();
                $merchantId = $order->getEvtEvent()->getMerchantId();
                
                $payment    = $this->paymentApi->execPaymentCreditCard($merchant, $merchantId, $gateway, $order);
                
                /* Getting data from the payment */
                $status     = $payment['status'];
                $order->setTransactionId($payment['transaction_id']);
                
                /* Set the payment status */
                $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
                
                /* If the payment was not successful */
                if ($status !== 'captured') {
                    throw new Exception('Payment not successful. Got status \'' . $status . '\'', 7);
                }
            } else {
                /* If it's not the moment to make the payment, set status pendent  */
                $order->setPaymentStatus('P');
            }
        } else if ($isPaymentMoment) {
            /* If the payment will not be managed by the system, set status Ok */
            $order->setPaymentStatus('A');
        } 
        
        $this->getEntityManager()->flush();
        $this->sendMessages($order, $reason, false, true);
    } 
    
    /*check for changes in the order*/
    function checkForChangesInTheOrder($order) {
        // $order->build($this->getEntityManager());
        // $oldTotal = 0.00;
        // $reason         = "alteração no pedido.";
        
        // /*estornando o valor da OrdOrder antes das alterações*/
        // if ($order->getPaymentStatus() == 'A' && $order->getPaymentMethod() == OrdOrder::PAYMENT_METHOD_CREDITCARD) {
            
        //     /* Getting the gateway and the merchant key from the store */
        //     $creditCard = $order->getPayCreditcard();
        //     $gateway    = $creditCard->getGateway();
        //     $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
        //     $merchant   = $order->getEvtEvent()->getMerchantKey();
        //     $merchantId = $order->getEvtEvent()->getMerchantId();
            
        //     /*Executing revoke*/
        //     try {
        //         $revokeStatus = $this->paymentApi->revoke($order->getTransactionId(), $oldTotal, $gateway, $merchant, $merchantId);
        //         $this->sendMessages($order, $reason, true, false);
        //     } catch (\Exception $e) {
        //         var_dump($e->getMessage());
        //         throw new Exception('Unable to execute revoke', 11);
        //     }
        //     if ($revokeStatus['success'] != true) {
        //         throw new Exception('Unable to execute revoke', 11);
        //     }
            
        //     $this->createRevokeOrder($order);
        // }
        
        // /*executar o pagamento do novo valor da OrdOrder*/
        // $this->makePaymentTransactionInProgress($order);
    }
    
    function sendMessages($order, $reason, $revoke, $exec) {
        $storeName  = $order->getEvtEvent()->getName();
        $user       = $order->getGenUser();
        
        if($revoke) {
            $message        = "A loja $storeName estornou o valor " . $order->getTotal() . " de sua compra pelo motivo $reason";
        } else if ($exec) {
            $message        = "A loja $storeName realizou a cobrança de um novo valor " . $order->getTotal() . " de sua compra pelo motivo $reason";
        } else {
            $message        = "O seu pedido numero: " . $order->getOrderIdentifier() . " na loja $storeName passou para o status: " . $order->getStatus()->getLabelName() . " ;)";
        }
        
        $notificationData = ['orderId' => $orderId];
        
        $title = "Sobre sua compra em $storeName";
        $originNotification = "Order_Orderbackend";
        $status             = "P";
        $dateTime           = new \DateTime();
        
        if ($user !== NULL) {
            try {
                $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user->getId(), $originNotification, $dateTime, $notificationData, $title, $user->getFirebaseToken());
            } catch (\Exception $e) { }
        }
    }
}