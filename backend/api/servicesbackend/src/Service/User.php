<?php
namespace Zeedhi\ApiServices\Service;

use Zeedhi\ApiServices\Model\Entities\GenContact;
use Zeedhi\ApiServices\Model\Entities\GenDataUpdate;
use Zeedhi\ApiServices\Model\Entities\GenUser;
use Zeedhi\ApiServices\Model\Entities\GenUserType;
use Zeedhi\ApiServices\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiServices\Model\Entities\PayCreditcard;
use Zeedhi\ApiServices\Model\Entities\GenAddress;
use Zeedhi\ApiServices\Model\Entities\GenDependent;
use Zeedhi\ApiServices\Model\Entities\BpWallet;
use Zeedhi\ApiServices\Model\Entities\PayWallet;
use Zeedhi\ApiServices\Model\Entities\GenEditableFields;
use Zeedhi\ApiServices\Model\Entities\GenOrganization;
use Zeedhi\ApiServices\Model\Entities\GenConfiguration;

use Zeedhi\ApiServices\Service\SMTP;

use Doctrine\ORM\EntityManager;
use Facebook\Facebook;
use Facebook\Authentication\AccessToken;
use Zeedhi\ApiServices\Helpers\ReplaceDql;

class User extends UserOperation {
    
    /** @var Facebook */
    private $facebook;
    const SALT = 'BIPPOPSALT';
    /**
     * Auth constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     * @param Facebook $facebook
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getUserData($userId, $userTypeId, $nrorg) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $user->build($this->getEntityManager(), $userTypeId, $nrorg);
        
        return $user;
    }
    
    /**
     * Creates a request to update address that will appear in the backoffice 
     * waiting for approval.
     */
    public function requestAddressChange($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL) {
        $typeName    = explode('_', $type)[0];

        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => $type]);

        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        switch ($permission) {
            case 'N':
                throw Exception::unauthorizedAction();
                break;
            case 'A':
                if ($old != NULL) $old->setStatus('O');
                // Do not break
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus($status);
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        
        /* Creating data update request */
        if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setGenAddress($address);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $dataUpdate->setNrorg($nrorg);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    /**
     * Creates a request to update user's basic information. The request will
     * appear in the backoffice and once it's accepted, the data will be transfered
     * to the original user (parent).
     */
    public function requestUserChange($id, $nrorg, $firstName, $lastName, $cpf, $document) {
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        $s1=NULL; $s2=NULL; $s3=NULL;
        if ($firstName != NULL && $user->getFirstName() != $firstName) $s1 = $this->requestFirstNameChange($id, $firstName, $nrorg, $document);
        if ($lastName != NULL && $user->getLastName() != $lastName)    $s2 = $this->requestLastNameChange($id, $lastName, $nrorg, $document);
        if ($cpf != NULL && $user->getCpf() != $cpf)                   $s3 = $this->requestCpfChange($id, $cpf, $nrorg, $document);
        
        return [$s1, $s2, $s3];
    }
    
    private function requestFirstNameChange($id, $firstName, $nrorg, $document) {
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'FIRST_NAME']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setFirstName($firstName);
            $user->setModifiedAt(new \DateTime());
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setFirstName($firstName);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    private function requestLastNameChange($id, $lastName, $nrorg, $document) {
        $param        = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'LAST_NAME']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setModifiedAt(new \DateTime());
            $user->setLastName($lastName);
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setLastName($lastName);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    private function requestCpfChange($id, $cpf, $nrorg, $document) {
        $param       = $this->getEntityManager()->getRepository(GenEditableFields::class)->findOneBy(['nrorg' => $nrorg, 'fieldName' => 'CPF']);
        $user         = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        
        /* Checking the permission to perform this action */
        $permission  = $param->getGenPermission()->getName();
        
        switch ($permission) {
            case 'N':
            case 'A':
            case 'R':
                $status = $permission;
                break;
            default:
                throw new Exception('Unknown permission for this action');
        }
        
        if ($status == 'A') {
            $user->setModifiedAt(new \DateTime());
            $user->setCpf($cpf);
        } else if ($status == 'R') {
            $dataUpdate = new GenDataUpdate();
            $dataUpdate->setCpf($cpf);
            $dataUpdate->setGenUser($user);
            $dataUpdate->setNrorg($nrorg);
            $dataUpdate->setUpdateStatus('P');
            $dataUpdate->setDocument($document);
            $this->getEntityManager()->persist($dataUpdate);
        }
        
        $this->getEntityManager()->flush();
        
        return $status;
    }
    
    /**
     * Adds a contact method to a user.
     */
    public function addContactMethod($userId, $type, $phoneNumber) {
        $user    = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $contact = new GenContact();
        $contact->setGenUser($user);
        $contact->setType($type);
        $contact->setPhone($phoneNumber);
        
        $this->getEntityManager()->persist($contact);
        $this->getEntityManager()->flush();
        
        return $contact;
    }
    
    /**
     * Removes a contact method
     */
    public function removeContactMethod($contactMethodId, $userId) {
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->find($contactMethodId);
        
        if ($contact == NULL || $contact->getGenUser() == NULL || $contact->getGenUser()->getId() !== $userId) {
            throw Exception::contactMethodNotFound();
        }
        
        $this->getEntityManager()->remove($contact);
        $this->getEntityManager()->flush();
        
        return $contact;
    }
    
    /**
     * Verifies if the specified phone
 number matches the registered one.
     */
    public function verifyMobilePhone($userId, $phoneNumber) {
        $contact = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $userId, 'type' => 'MOBILE']);
        return $phoneNumber == $contact->getPhone();
    }
    
    /**
     * getContactMethodsByCpf
     * Returns the contact methods of an user based on it's CPF.
     * 
     */
    public function getContactMethodsByCpf($cpf) {
        $contactMethods = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.email, c.phone
            FROM Zeedhi\ApiServices\Model\Entities\GenUser u
            LEFT JOIN Zeedhi\ApiServices\Model\Entities\GenContact c WITH c.genUser = u.id AND c.type = 'MOBILE'
            WHERE u.cpf = $cpf
            "
        )->getResult();
        
        return $this->factoryContactMethods($contactMethods);
    }
    
    /**
     * getContactMethodsByExternalId
     * Returns the contact methods of an user based on it's External Id.
     * 
     */
    public function getContactMethodsByExternalId($eId) {
        $contactMethods = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.email, c.phone
            FROM Zeedhi\ApiServices\Model\Entities\GenUserTypeRel p
            JOIN Zeedhi\ApiServices\Model\Entities\GenUser u WITH u.id = p.genUser
            LEFT JOIN Zeedhi\ApiServices\Model\Entities\GenContact c WITH c.genUser = u.id AND c.type = 'MOBILE'
            WHERE p.externalId = $eId
            "
        )->getResult();
        
        return $this->factoryContactMethods($contactMethods);
    }
    
    /**
     * factoryContactMethods
     * Formats a contact method.
     * 
     */
    private function factoryContactMethods($contactMethods) {
        if (count($contactMethods) > 0) {
            $userId      = $contactMethods[0]['id'];
            $maskedEmail = $contactMethods[0]['email'] != NULL ? GenContact::maskEmail($contactMethods[0]['email']) : NULL; 
            $maskedPhone = $contactMethods[0]['phone'] != NULL ? GenContact::maskPhone($contactMethods[0]['phone']) : NULL;
            
            $response = ['USER_ID' => $userId, 'MASKED_EMAIL' => $maskedEmail, 'EMAIL' => $contactMethods[0]['email'], 'PHONE' => $maskedPhone];
            
        } else $response = [];
        
        return $response;
    }
    
    /**
     * getUserDependents
     * Gets the dependents of an user in an organization.
     * 
     */
    public function getUserDependents($id, $nrorg) {
        $user     = $this->getEntityManager()->getRepository(GenUser::class)->find($id);
        $parentId = $user->getId();

        if ($user != NULL) {
            $dependents = $this->getEntityManager()->createQuery(
                "
                SELECT DISTINCT d
                FROM 'Zeedhi\ApiServices\Model\Entities\GenDependent' d
                JOIN d.parent pr
                JOIN d.dependent dr
                JOIN 'Zeedhi\ApiServices\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = dr
                JOIN utr.genUserType ut
                WHERE pr.id = $parentId
                AND d.nrorg = $nrorg
                AND ut.id = 1
                AND dr.status = 'A'
                "
            )->getResult();
            foreach ($dependents as $dependent) {
                $dependent->build($this->getEntityManager(), 1);
            }
            return $dependents;
        } return [];
    }
    
    /**
     * addDependent
     * Link an user to another as a dependent.
     * 
     */
    public function addDependent($parentId, $dependentId, $monthlyLimit, $receiptsTo, $nrorg) {
        $parent    = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId);
        $dependent = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['parent' => $parent, 'dependent' => $dependent]);
        
        if ($dependency != NULL) return $dependency;
        
        if ($dependent == NULL || $parent == NULL) {
            throw Exception::userNotFound();
        }
        
        if ($receiptsTo == NULL) {
            $parent->build($this->getEntityManager());
            $receiptsTo = $parent->getEmail();
        }
        
        $dependentRel = new GenDependent();
        $dependentRel->setDependent($dependent);
        $dependentRel->setParent($parent);
        $dependentRel->setNrorg($nrorg);
        $dependentRel->setMonthlyLimit($monthlyLimit);
        $dependentRel->setReceiptsTo($receiptsTo);
        $dependentRel->setStatus('A');
        
        $this->getEntityManager()->persist($dependentRel);
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * removeDependent
     * Unlink a dependent from it's parent.
     * 
     */
    public function removeDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL || $user == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $this->getEntityManager()->remove($dependentRel);
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * enableDependent
     * Sets a dependent's status to enabled.
     * 
     */
    public function enableDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setStatus('A');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * disableDependent
     * Sets a dependent's status to disabled.
     * 
     */
    public function disableDependent($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setStatus('D');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    /**
     * enableDependentForMainCard
     * Sets that the dependent can use the parent's main card.
     * 
     */
    public function enableDependentForMainCard($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setCanUseMainCard('T');
        }
        
        $this->getEntityManager()->flush();

        return $dependent;
    }
    
    /**
     * disableDependentForMainCard
     * Sets that the dependent can not use the parent's main card.
     * 
     */
    public function disableDependentForMainCard($userId, $dependentId, $nrorg) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
    
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setCanUseMainCard('F');
        }
        
        $this->getEntityManager()->flush();
        
        return $dependent;
    }
    
    public function editDependentMonthlyLimit($userId, $dependentId, $nrorg, $monthlyLimit) {
        $user          = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependent     = $this->getEntityManager()->getRepository(GenUser::class)->find($dependentId);
        
        if ($dependent == NULL) {
            throw Exception::userNotFound();
        }
        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['dependent' => $dependent, 'parent' => $user, 'nrorg' => $nrorg]);
    
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setMonthlyLimit($monthlyLimit);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function editDependentReceiptsTo($userId, $dependentId=NULL, $nrorg, $receiptsTo) {
        $filter = array();
        $filter = array_merge($filter, array('nrorg' => $nrorg));
        $filter = array_merge($filter, array('parent' => $userId));
        if ($dependentId != NULL) $filter = array_merge($filter, array('dependent'   => $dependentId));

        
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy($filter);
        
        foreach ($dependentRels as $dependentRel) {
            $dependentRel->setReceiptsTo($receiptsTo);
        };
        
        $this->getEntityManager()->flush();
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $card   = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard();
        if ($card == NULL) return 0;
        $cardId = $card->getId();
        $today  = new \DateTime();
        $firstDayOfMonth   = $today->format('Y-m-d');

        $orders = $this->getEntityManager()->createQuery(
        "
        SELECT o
        FROM 'Zeedhi\ApiServices\Model\Entities\OrdOrder' o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createDate >= '$firstDayOfMonth'
        "
        )->getResult();
        
        $total = 0;
        
        foreach ($orders as $order) {
            if ($order->getTotal() != NULL)
                $total += $order->getTotal();
        }
        
        return $total;
    }
    
    public function setMainCreditCard($userId, $cardId) {
        $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($cardId);
        
        $user->setMainCreditcard($creditCard);
        $this->getEntityManager()->flush();
    }
    
    /*CRISTIANO Integração*/
    
    /*Buscar organization*/
    public function getOrganization($nrorg) {
        $organization    = $this->getEntityManager()->getRepository(GenOrganization::class)->find($nrorg);
        
        return $organization;
    }
    
    /*cria um userDependent com informações do WEBSERVICE, bem como o perfil*/
    public function createGenUserDependentWebService($firstName, $lastName, $email = null, $password, $nrorg, $externalId = null, $cpf = null){
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $email ? $user->setEmail($email) : "";
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        $cpf ? $user->setCpf($cpf) : "";
        
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, 1));
        $userProfile->setStatus('A');
        $externalId ? $userProfile->setExternalId($externalId) : "";
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    /*atualiza um user*/
    public function updateGenUserWebService($userId, $firstName, $lastName, $email, $nrorg, $externalId, $image, $cpf, $userTypeId){
        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmail($email);
        $user->setImage($image);
        $user->setStatus('A');
        $user->setCpf($cpf);
        $user->setModifiedAt(new \DateTime());
        
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId]);
        if (empty($userProfile) || $userProfile->getGenUserType()->getId() == 3) {
            $userProfile = new GenUserTypeRel();
        }
        
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userType   = $this->getEntityManager()->getRepository(GenUserType::class)->find($userTypeId);
        $userProfile->setGenUserType($userType);
        $userProfile->setStatus('A');
        $userProfile->setExternalId($externalId);
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }
    
    /*atualiza os telefones do user*/
    public function updateContactMethod($userId, $type, $phoneNumber) {
        
        $contact    = $this->getEntityManager()->getRepository(GenContact::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
        if(!empty($contact)){
            $user    = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
            $user->setModifiedAt(new \DateTime());
            $contact->setGenUser($user);
            $contact->setType($type);
            $contact->setPhone($phoneNumber);
            
            $this->getEntityManager()->persist($contact);
            $this->getEntityManager()->flush();
            
            return $contact;
        }else {
            $this->addContactMethod($userId, $type, $phoneNumber);
        }
    }
    
    /*buscar endereços de um user*/
    public function getUserAddress($userId, $nrorg){
        return $this->getEntityManager()->getRepository(GenAddress::class)->findBy(['genUser' => $userId, 'status' => "A"]);
    }
    
    // /*buscar um user por email*/
    // public function getUserWebService($email, $externalId, $cep){
    //     return $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email]);
    // }
    
    /*buscar um user por email*/
    public function getUserWebService($cpf = NULL, $externalId = NULL){
        $queryCpf        = isset($cpf)        && $cpf        ? $cpf        : NULL;
        $queryExternalId = isset($externalId) && $externalId ? $externalId : NULL;
        
        if($queryCpf){
            $user     = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['cpf' => $queryCpf, 'status' => 'A']);
            $response = !empty($user) ? $user : NULL;
        }else {
            $userType = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['externalId' => $queryExternalId, 'status' => 'A']);
            $response = !empty($userType) ? $userType : NULL;
        }

        return $response;
    }
    
    /*cryptografa uma senha*/
    private function cryptPassword($password) {
        //@toDo Remover StrToUpper
        return strtoupper(hash('sha512', self::SALT.$password));
    }
    
    /*cria um endereço*/
    public function createAddress($userId, $nrorg, $cep, $status, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL) {
        $typeName    = explode('_', $type)[0];

        $user        = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
        
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        if(!empty($old)){
            $old->setStatus("O");
            $this->getEntityManager()->persist($old);
            $this->getEntityManager()->flush();
        }
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus($status);
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        return $status;
    }
    
    /*atualiza um endereço*/
    public function requestAddressChangeWebService($userId, $nrorg, $cep, $street, $neighborhood, $city, $provincy, $number, $complement, $country, $type, $document=NULL) {
        $typeName    = explode('_', $type)[0];

        $user        = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['genUser' => $userId, 'status' => 'A', 'nrorg' => $nrorg, 'type' => $typeName]);
        
        /* Creating new address */
        $address = new GenAddress();
        $address->setGenUser($user);
        $address->setStatus("A");
        $address->setCreateDate(new \DateTime());
        
        $old->setStatus("O");
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setComplement($complement);
        $address->setCountry($country);
        $address->setNrorg($nrorg);
        $address->setType($typeName);
        $address->setOld($old);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
    }
    
    public function getConfiguration($nrorg){
        return $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
    }
    
    public function getGenUserType($userId){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'status' => 'A']);
    }

    public function getUsersFromOrganization($nrorg) {
        $users = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiServices\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u
            WHERE utr.nrorg = $nrorg AND utr.genUserType != 1
            GROUP BY u.id 
            "
        )->getResult();
        
        foreach ($users as $user) {
            $user->build($this->getEntityManager(), $nrorg);
        }
        
        return $users;
    }
    
    public function getDataUpdateRequests($nrorg) {
        $requests = $this->getEntityManager()->createQuery(
            "
            SELECT du
            FROM 'Zeedhi\ApiServices\Model\Entities\GenDataUpdate' du
            WHERE du.nrorg = $nrorg
            AND du.updateStatus = 'P'
            ORDER BY du.createdAt
            DESC
            "
        )->getResult();
        
        foreach($requests as $request) {
            $request->build($this->getEntityManager());
        }
        
        return $requests;
    }

    public function acceptDataUpdateRequest($dataUpdateId, $address, $userId) {
        $dataUpdate = $this->getEntityManager()->getRepository(GenDataUpdate::class)->find($dataUpdateId);
        
        if ($address) {
            $userCurrentAddress = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['type' => $address['type'], 'genUser' => $dataUpdate->getGenUser(), 'nrorg' => $dataUpdate->getNrorg(), 'status' => 'A']);
            if ($userCurrentAddress) $userCurrentAddress->setStatus('O');
            
            $genAddress = new GenAddress();
            $genAddress->setCep($address['newCep']);
            $genAddress->setStreet($address['newStreet']);
            $genAddress->setNeighborhood($address['newNeighborhood']);
            $genAddress->setCity($address['newCity']);
            $genAddress->setProvincy($address['newState']);
            $genAddress->setNumber($address['newNumber']);
            $genAddress->setComplement($address['newComplement']);
            $genAddress->setStatus('A');
            $genAddress->setGenUser($dataUpdate->getGenUser());
            $genAddress->setNrorg($dataUpdate->getNrorg());
            $genAddress->setType($address['type']);
            $this->getEntityManager()->persist($genAddress);
            
            $dataUpdate->setGenAddress($genAddress);
        }
        
        $dataUpdate->setApprovedBy($this->getEntityManager()->getRepository(GenUser::class)->find($userId));
        $dataUpdate->setUpdateStatus('A');
        
        $this->getEntityManager()->flush();
    }
    
    public function updateUserProfileStatus($userId, $userTypeId, $nrorg, $status, $adminId) {
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
        
        if ($userProfile) $userProfile->setStatus($status);
        else {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setNrorg($nrorg);
            $userProfile->setStatus($status);
            $userProfile->setCreatedBy($adminId);
            $this->getEntityManager()->persist($userProfile);
        }
        
        $userProfile->setModifiedBy($adminId);
        $this->getEntityManager()->flush();
    }

    public function createUser($firstName, $lastName, $cpf, $email, $password, $nrorg) {
        $user = new GenUser();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        
        $userProfile = new GenUserTypeRel();
        $userProfile->setGenUser($user);
        $userProfile->setNrorg($nrorg);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, GenUserType::VENDEDOR_ID));
        $userProfile->setStatus('A');
        
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    public function updateUser($user, $firstName, $lastName, $cpf, $email, $password, $nrorg) {

        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCpf($cpf);
        $user->setEmail($email);
        $user->setPassword($this->cryptPassword($password));
        $user->setStatus('A');
        
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $user->getId(), 'genUserType' => 3, 'nrorg' => $nrorg]);
        if(empty($userProfile)) {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($user);
            $userProfile->setNrorg($nrorg);
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, GenUserType::VENDEDOR_ID));
            $userProfile->setStatus('A');
            $this->getEntityManager()->persist($userProfile);
            $this->getEntityManager()->flush();
        }
        
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }
    
    public function updateFCMToken($userId, $token, $nrorg) {
        $organizationUserRel = $this->getEntityManager()->getRepository(GenOrganizationUserRel::class)->findOneBy(['genUser' => $userId, 'nrorg' => $nrorg]);
        
        if($organizationUserRel == NULL) { 
            $organizationUserRel = new GenOrganizationUserRel(); 
            $organizationUserRel->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $organizationUserRel->setStatus("A");
            $organizationUserRel->setNrorg($nrorg);
        }
        $organizationUserRel->setFirebaseToken($token);
         
        $this->getEntityManager()->persist($organizationUserRel);
        $this->getEntityManager()->flush();
        return $organizationUserRel;
    }
    
    public function addProfileUser($userId, $userTypeId, $nrorg, $status, $adminId) {
        $userProfile = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId, 'nrorg' => $nrorg]);
        
        if ($userProfile) {
            $userProfile->setStatus($status);
            $userProfile->setModifiedBy($adminId);
        } else {
            $userProfile = new GenUserTypeRel();
            $userProfile->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
            $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
            $userProfile->setNrorg($nrorg);
            $userProfile->setStatus($status);
            $userProfile->setCreatedBy($adminId);
        }
        
        $this->getEntityManager()->persist($userProfile);
        $this->getEntityManager()->flush();
    }
    
    public function getGenUser($email) {
        return $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $email, 'status' => 'A']);
    }
    
    public function getGenUserTypeWB($userId, $userTypeId){
        return $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['genUser' => $userId, 'genUserType' => $userTypeId]);
    }
    
    public function activateUserTypeRel($genUserTypeRel) {
        $genUserTypeRel->setStatus('A');
        $this->getEntityManager()->flush();
        
    }
    
    public function getUserFromOrganizationAndProfile($cpf, $npf) {
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiServices\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiServices\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u.id
            WHERE ((u.cpf = '$cpf' AND u.cpf <> '') OR utr.externalId = '$npf') 
            AND u.status = 'A' AND utr.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getDependentsData($userId, $nrorg) {
        $genUser = GenUser::class;
        $payWallet = PayWallet::class;
        $genDependent = GenDependent::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $dependents = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT pr.id parentId, dr.id dependentId, dr.firstName, dr.lastName, pw.id walletId, pw.balance
            FROM $genDependent d
            JOIN d.parent pr
            JOIN d.dependent dr
            LEFT JOIN $payWallet pw WITH d.dependent = pw.genUser
            JOIN $genUserTypeRel utr WITH utr.genUser = dr
            JOIN utr.genUserType ut
            WHERE pr.id = $userId
            AND d.nrorg = $nrorg
            AND ut.id = 1
            AND dr.status = 'A'
            "
        )->getResult();
        $response = [];
        foreach ($dependents as $key => $dependent) {
            array_push($response, [
                'DEPENDENT_ID' => $dependent["dependentId"],
                'FULL_NAME' => $dependent['firstName'] ."". strrchr($dependent['lastName'], ' '),
                'WALLET_ID' => $dependent['walletId'],
                'BALANCE' => $dependent["balance"]
            ]);
        }
        
        return $response;
    }
    
    public function getUserDataById($nrorg, $userId, $userTypeId) {
        $genUser        = GenUser::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u.id, u.cpf, u.email, utr.externalId
            FROM $genUser u
            JOIN $genUserTypeRel utr WITH utr.genUser = u.id
            WHERE u.id = $userId AND u.status = 'A' AND utr.status = 'A' 
            AND utr.genUserType = $userTypeId AND utr.nrorg = $nrorg
            "
        )->getResult();
        
        return $user;
    }
    
    public function getUserFromOrganizationAndProfileOne($nrorg, $cpf, $npf, $email) {
        $config = $this->getEntityManager()->getConfiguration();
        $genUser = GenUser::class;
        $genContact = GenContact::class;
        $genUserTypeRel = GenUserTypeRel::class;
        $replaceDql = ReplaceDql::class;
        $config->addCustomDatetimeFunction('REPLACE', $replaceDql);
        
        $queryWhere = "";
        
        if($npf){
            $queryWhere = "utr.externalId = '$npf' AND utr.nrorg = '$nrorg' AND utr.status = 'A' AND";
        }elseif($cpf) {
            $queryWhere = "REPLACE(REPLACE(REPLACE(u.cpf,'.', ''),'-', ''),'/', '') = REPLACE(REPLACE(REPLACE('$cpf','.', ''),'-', ''),'/', '') AND";
        }else {
            $queryWhere = "u.email = '$email' AND";
        }
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id, CONCAT(u.firstName, ' ', TRIM(u.lastName)) fullName, u.cpf, u.email, gc.phone
            FROM $genUser u
            LEFT JOIN $genContact gc WITH (gc.genUser = u AND gc.type = 'MOBILE')
            LEFT JOIN $genUserTypeRel utr WITH utr.genUser = u
            WHERE $queryWhere u.status = 'A'
            " )->getResult();
        
        return $user;
    }
}
