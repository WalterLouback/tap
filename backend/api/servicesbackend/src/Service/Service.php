<?php
namespace Zeedhi\ApiServices\Service;

use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventMenu;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\GenUser as EvtGenUser;

use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller as OrdEvtEventSeller;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent as OrdEvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtEventMenu as OrdEvtEventMenu;
use Zeedhi\ApiOrders\Model\Entities\OrdConfigStore;
use Zeedhi\ApiOrders\Model\Entities\EvtSellerType;
use Zeedhi\ApiOrders\Model\Entities\GenStatus;
use Zeedhi\ApiOrders\Model\Entities\GenStatusUserType;
use Zeedhi\ApiOrders\Model\Entities\OrdWorkflow;
use Zeedhi\ApiOrders\Model\Entities\GenStructure;
use Zeedhi\ApiOrders\Model\Entities\GenUser as OrdGenUser;

use Zeedhi\ApiGeneral\Model\Entities\GenUser;
use Zeedhi\ApiGeneral\Model\Entities\GenCategory;

use Zeedhi\ApiServices\Model\Entities\OrdConfigStore as SerOrdConfigStore;
use Zeedhi\ApiServices\Model\Entities\OrdWorkflow as SerOrdWorkflow;
use Zeedhi\ApiServices\Model\Entities\GenStructure as SerGenStructure;
use Zeedhi\ApiServices\Model\Entities\EvtEvent as SerEvtEvent;
use Zeedhi\ApiServices\Model\Entities\EvtEventMenu as SerEvtEventMenu;
use Zeedhi\ApiServices\Model\Entities\GenUser as SerGenUser;
use Zeedhi\ApiServices\Model\Entities\GenCategory as SerGenCategory;
use Zeedhi\ApiServices\Model\Entities\EvtEventSeller as SerEvtEventSeller;

use Zeedhi\ApiServices\Model\Entities\GenSchedule;
use Zeedhi\ApiServices\Model\Entities\GenScheduleUserRel;
use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiServices\Model\Entities\SerServiceEventRel;
use Zeedhi\ApiServices\Model\Entities\SerServiceSellerRel;
use Zeedhi\ApiServices\Model\Entities\SerServiceStructureRel;

use Doctrine\ORM\EntityManager;

class Service extends UserOperation {
    
    /**
     * Auth constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    /*METODOS QUE MANIPULAM A TABELA SER_SERVICE*/
    public function getServicesByNrorg($nrorg, $type, $eventId) {
        $serService         = SerService::class;
        $serServiceEventRel = SerServiceEventRel::class;
        
        $queryType          = $type ? " AND ss.type = '$type' ": " ";
        $queryEvent         = $eventId ? " AND sser.evtEvent = '$eventId' ": " ";
        
        $services           = $this->getEntityManager()->createQuery("
                                SELECT ss FROM $serService ss
                                LEFT JOIN $serServiceEventRel sser WITH sser.serService = ss
                                WHERE ss.nrorg = '$nrorg' AND ss.status = 'A' AND ss.statusapp = 'A'
                                $queryType
                                $queryEvent
                                ORDER BY ss.status  ASC
                                ")->getResult();
        $data       = array();
        
        foreach($services as $service) {
            $serviceEventRels = $this->getServiceEventRelsByService($service->getId(), $nrorg);
            array_push($data, $service->toArray($serviceEventRels));
        }
        
        return $data;
    }
    
    public function getServicesByNrorgBackOffice($nrorg, $type, $eventId) {
        $serService         = SerService::class;
        $serServiceEventRel = SerServiceEventRel::class;
        
        $queryType          = $type ? " AND ss.type = '$type' ": " ";
        $queryEvent         = $eventId ? " AND sser.evtEvent = '$eventId' ": " ";
        
        $services           = $this->getEntityManager()->createQuery("
                                SELECT ss FROM $serService ss
                                LEFT JOIN $serServiceEventRel sser WITH sser.serService = ss
                                WHERE ss.nrorg = '$nrorg'
                                $queryType
                                $queryEvent
                                ORDER BY ss.status  ASC
                                ")->getResult();
        $data       = array();
        
        foreach($services as $service) {
            $serviceEventRels = $this->getServiceEventRelsByService($service->getId(), $nrorg);
            array_push($data, $service->toArray($serviceEventRels));
        }
        
        return $data;
    }
    
    public function getServicesChildrenByNrorg($nrorg, $eventId, $type) {
        $schedule = GenSchedule::class;
        $scheduleUserRel = GenScheduleUserRel::class;
        $service = SerService::class;
        $serviceEventRel = SerServiceEventRel::class;
        $serviceSellerRel = SerServiceSellerRel::class;
        $users = [];
        
        $today = $this->getDate();
        
        $services = $this->getEntityManager()->createQuery("
            SELECT DISTINCT s.id serviceId, s.name serviceName, 0 totalSchedule FROM $service s
            LEFT JOIN $serviceEventRel ser WITH (ser.evtEvent = '$eventId' AND ser.serService = s.id)
            WHERE s.status = 'A' 
            AND s.nrorg = '$nrorg' 
            AND ser.evtEvent = '$eventId' 
            AND s.type = '$type' AND s.parentId <> ''
            GROUP BY serviceId
        ")->getResult();
        
        if(!empty($services)) {
            foreach($services as $i => $service) {
                $services[$i]['professionals'] = $this->getEntityManager()->createQuery("
                    SELECT ss1.id serviceId, sss.id serviceSellerId, ees.id eventSellerId, gu.id userId, CONCAT(gu.firstName, ' ', gu.lastName) profissional, 
                        sch.id scheduleId,
                        (SELECT DISTINCT count(sc1.id) 
                         FROM $schedule sc1
                         WHERE sc1.status = 'A' AND sc1.serService = " . $service['serviceId'] . " 
                         AND sc1.serServiceSeller = sss.id) totalScheduleSeller
                    FROM $serviceSellerRel sss
                    LEFT JOIN $schedule sch WITH (sch.serService = " . $service['serviceId'] . " AND sch.serServiceSeller = sss.id)
                    LEFT JOIN sss.evtEventSeller ees WITH ees.id = sss.evtEventSeller
                    LEFT JOIN ees.genUser gu
                    LEFT JOIN sch.serService ss1
                    WHERE sch.initialDate >= '" . $today->format('Y-m-d') . "' AND sch.serService = " . $service['serviceId'] . " GROUP BY sss.evtEventSeller
                    
                ")->getResult();
            }
            
            foreach($services as $i => $service) {
                foreach($service['professionals'] as $j => $value){
                    $services[$i]['professionals'][$j]['users'] =
                    $this->getEntityManager()->createQuery("
                        SELECT gu.id userId, CONCAT(gu.firstName, ' ', gu.lastName) fullName, gu.image foto FROM $scheduleUserRel scur
                        LEFT JOIN scur.genUser gu WITH (scur.genUser = gu.id AND scur.status = 'A')
                        LEFT JOIN scur.genSchedule gs
                        WHERE gs.status = 'A' AND gs.serServiceSeller = ". $value["serviceSellerId"] . " AND gs.serService = " .$value["serviceId"]. "
                        
                    ")->getResult();
                }
            }
            
            foreach($services as $i =>  $service){
                foreach($service['professionals'] as $professional) {
                    $services[$i]['totalSchedule'] += $professional['totalScheduleSeller'];
                }    
            }
            
            $orderBy = "totalSchedule";
            
            // $data = $this->group_by($orderBy, $services);
            
            usort($services, function ($a, $b) {
            	return $a['totalSchedule'] < $b['totalSchedule'];
            });
            
            return $services;
        } else {
            return [];
        }
    }
    
    public function createService($name, $status, $time, $eventId, $decription, $cancellationFine, $cancellationTime, $nrorg, $createdBy, $modifiedBy, 
                                    $parentId, $color, $price, $icon, $image, $type, $statusApp, $futureDaysToSchedule, $userSchedulingLimit, $dependentSchedulingLimit) {
        $service = new SerService();
        
        if($name)
            $service->setName($name);
        if($status)
            $service->setStatus($status);
        if($time)
            $service->setTime($time);
        // if($eventId)
        //     $service->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        if($decription)
            $service->setDecription($decription);
        if($cancellationFine)
            $service->setCancellationFine($cancellationFine);
        if($cancellationTime)
            $service->setCancellationTime($cancellationTime);
        if($nrorg)
            $service->setNrorg($nrorg);
        if($createdBy)
            $service->setCreatedBy($createdBy);
        if($modifiedBy)
            $service->setModifiedBy($modifiedBy);
        if($parentId)
            $service->setParentId($this->getEntityManager()->getReference(SerService::class, $parentId));
        if($color)
            $service->setColor($color);
        if($price)
            $service->setPrice($price);
        if($icon)
            $service->setIcon($icon);
        if($image)
            $service->setImage($image);
        if($type)
            $service->setType($type);
        if($statusApp)
            $service->setStatusapp($statusApp);
        if($futureDaysToSchedule)
            $service->setFutureDaysToSchedule($futureDaysToSchedule);
        if($userSchedulingLimit)
            $service->setUserSchedulingLimit($userSchedulingLimit);
        if($dependentSchedulingLimit)
            $service->setDependentSchedulingLimit($dependentSchedulingLimit);
        
        
        $this->getEntityManager()->persist($service);
        
        $this->getEntityManager()->flush();
        
        return $service;
    }
    
    public function updateService($serviceId, $name, $status, $time, $eventId, $decription, $cancellationFine, $cancellationTime, 
                                    $nrorg, $createdBy, $modifiedBy, $parentId, $color, $price, $icon, $image, $type,
                                    $statusApp, $futureDaysToSchedule, $userSchedulingLimit, $dependentSchedulingLimit) {
        $service = $this->getEntityManager()->getRepository(SerService::class)->findOneBy(['id' => $serviceId]);
        
        $service->setName($name);
        $service->setStatus($status);
        $service->setTime($time);
        $service->setDecription($decription);
        $service->setCancellationFine($cancellationFine);
        $service->setCancellationTime($cancellationTime);
        $service->setNrorg($nrorg);
        $service->setCreatedBy($createdBy);
        $service->setModifiedBy($modifiedBy);
        $service->setParentId($parentId ? $this->getEntityManager()->getReference(SerService::class, $parentId) : NULL);
        $service->setColor($color);
        $service->setPrice($price);
        $service->setIcon($icon);
        $service->setImage($image);
        $service->setType($type);
        $service->setStatusapp($statusApp);
        $service->setFutureDaysToSchedule($futureDaysToSchedule);
        $service->setUserSchedulingLimit($userSchedulingLimit);
        $service->setDependentSchedulingLimit($dependentSchedulingLimit);
        
        $this->getEntityManager()->merge($service);
        
        $this->getEntityManager()->flush();
        
        return $service;
    }
    
    public function removeService($serviceId, $status, $userId) {
        $service = $this->getEntityManager()->getRepository(SerService::class)->findOneBy(['id' => $serviceId]);
        
        $service->setStatus($status);
        $service->setStatusapp($status);
        $service->setModifiedBy($userId);
        
        $this->getEntityManager()->merge($service);
        
        $this->getEntityManager()->flush();
        
        return $service;
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA SER_SERVICE_SELLER_REL*/
    public function createServiceSellerRel($sellerId, $serviceId, $nrorg, $createdBy, $modifiedBy) {
        $serviceSellerRel = new SerServiceSellerRel();
        
        $serviceSellerRel->setEvtEventSeller($this->getEntityManager()->getReference(EvtEventSeller::class, $sellerId));
        $serviceSellerRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        
        $serviceSellerRel->setStatus('A');
        
        if($nrorg)
            $serviceSellerRel->setNrorg($nrorg);
        if($createdBy)
            $serviceSellerRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceSellerRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($serviceSellerRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceSellerRel;
    }
    
    public function updateServiceSellerRel($serviceSellerRelId, $sellerId, $serviceId, $nrorg, $createdBy, $modifiedBy, $status) {
        $serviceSellerRel = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->find($serviceSellerRelId);
        
        if($sellerId)
            $serviceSellerRel->setEvtEventSeller($this->getEntityManager()->getReference(EvtEventSeller::class, $sellerId));
        if($serviceId)
            $serviceSellerRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        if($nrorg)
            $serviceSellerRel->setNrorg($nrorg);
        if($createdBy)
            $serviceSellerRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceSellerRel->setModifiedBy($modifiedBy);
        if($status)
            $serviceSellerRel->setStatus($status);
        
        $this->getEntityManager()->merge($serviceSellerRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceSellerRel;
    }
    
    public function getServiceSellerRelsBySeller($sellerId, $nrorg) {
        $serviceSellerRels = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->findBy(["evtEventSeller" => $sellerId, "nrorg" => $nrorg]);
        
        return $serviceSellerRels;
    }
    
    public function getServiceSellerRelsByService($serviceId, $eventId, $nrorg) {
        $serServiceSellerRel    = SerServiceSellerRel::class;
        
        $serviceSellerRels = $this->getEntityManager()->createQuery("
            SELECT sssr FROM $serServiceSellerRel sssr
            JOIN sssr.evtEventSeller ees
            WHERE ees.status = 'A' AND ees.evtEvent = '$eventId' AND sssr.serService = '$serviceId' AND sssr.nrorg = '$nrorg' AND sssr.status = 'A'
        ")->getResult();
        
        return $serviceSellerRels;
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA SER_SERVICE_STRUCTURE_REL*/
    public function createServiceStructureRel($structureId, $serviceId, $nrorg, $createdBy, $modifiedBy) {
        $serviceStructureRel = new SerServiceStructureRel();
        
        $serviceStructureRel->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        $serviceStructureRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        
        if($nrorg)
            $serviceStructureRel->setNrorg($nrorg);
        if($createdBy)
            $serviceStructureRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceStructureRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($serviceStructureRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceStructureRel;
    }
    
    public function updateServiceStructureRel($serviceStructureRelId, $structureId, $serviceId, $nrorg, $createdBy, $modifiedBy) {
        $serviceStructureRel = $this->getEntityManager()->getRepository(SerServiceStructureRel::class)->find($serviceStructureRelId);
        
        $serviceStructureRel->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        $serviceStructureRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        
        if($nrorg)
            $serviceStructureRel->setNrorg($nrorg);
        if($createdBy)
            $serviceStructureRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceStructureRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->merge($serviceStructureRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceStructureRel;
    }
    
    public function getServiceStructureRelsByStructure($structureId, $nrorg) {
        $serviceStructureRels = $this->getEntityManager()->getRepository(SerServiceStructureRel::class)->findBy(["genStructure" => $structureId, "nrorg" => $nrorg]);
        
        return $serviceStructureRels;
    }
    
    public function getServiceStructureRelsByService($serviceId, $nrorg) {
        $serviceStructureRels = $this->getEntityManager()->getRepository(SerServiceStructureRel::class)->findBy(["serService" => $serviceId, "nrorg" => $nrorg]);
        
        return $serviceStructureRels;
    }
    /*FIM*/
    
    public function getEventsFromOrganizationAndType($nrorg, $type) {
        $evtEvent     = EvtEvent::class;
        $genCategory  = GenCategory::class;
        $evtEventMenu = EvtEventMenu::class;
        $genStructure = GenStructure::class;
        
        $event =  $this->getEntityManager()->createQuery("
            SELECT DISTINCT evt.id, evt.type, evt.status, evt.nrorg, evt.name, evt.imageMap, evt.imageLogo, evt.imageCover, evt.initialDate, evt.finalDate, evt.classification, 
            evt.rating, evt.salesMethod, evt.location, evt.about, genCat.id as categoryId, genCat.name as nameCategory, genStruct.id as structureId, 
            genStruct.name as nameStructure
            FROM $evtEvent evt
            LEFT JOIN  $genCategory genCat WITH evt.genCategory = genCat.id
            LEFT JOIN  $genStructure genStruct WITH evt.genStructure = genStruct.id
            LEFT JOIN  $evtEventMenu evtm WITH evtm.evtEvent = evt.id
            WHERE evt.nrorg = $nrorg AND evt.type = '$type'
            ORDER BY evt.initialDate DESC
        ")->getResult();
        
        return $event;
    }
    
    
    /*METODOS QUE MANIPULAM A TABELA SER_SERVICE_EVENT_REL*/
    /*Adicionei paramentros, é necessario alterar o service admin*/
    public function createServiceEventRel($eventId, $serviceId, $nrorg, $createdBy, $modifiedBy, $initialTime, $finalTime, $interval) {
        $serviceEventRel = new SerServiceEventRel();
        
        $serviceEventRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $serviceEventRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        
        if($nrorg)
            $serviceEventRel->setNrorg($nrorg);
        if($createdBy)
            $serviceEventRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceEventRel->setModifiedBy($modifiedBy);
        
        $serviceEventRel->setStatus('A');
        
        $this->getEntityManager()->persist($serviceEventRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceEventRel;
    }
    /*Adicionei paramentros, é necessario alterar o service admin*/
    public function updateServiceEventRel($serviceEventRelId, $eventId, $serviceId, $nrorg, $createdBy, $modifiedBy, $initialTime, $finalTime, $interval, $status=null) {
        $serviceEventRel = $this->getEntityManager()->getRepository(SerServiceEventRel::class)->find($serviceEventRelId);
        
        $serviceEventRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $serviceEventRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        
        if($nrorg)
            $serviceEventRel->setNrorg($nrorg);
        if($createdBy)
            $serviceEventRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $serviceEventRel->setModifiedBy($modifiedBy);
        if($status)
            $serviceEventRel->setStatus($status);
        
        $this->getEntityManager()->merge($serviceEventRel);
        
        $this->getEntityManager()->flush();
        
        return $serviceEventRel;
    }
    
    public function getServiceEventRelsByEvent($eventId, $nrorg) {
        $serviceEventRels = $this->getEntityManager()->getRepository(SerServiceEventRel::class)->findBy(["evtEvent" => $eventId, "nrorg" => $nrorg]);
        
        return $serviceEventRels;
    }
    
    public function getServiceEventRelsByService($serviceId, $nrorg) {
        $serviceEventRels = $this->getEntityManager()->getRepository(SerServiceEventRel::class)
            ->findBy(["serService" => $serviceId, "nrorg" => $nrorg, "status" => "A"]);
        
        return $serviceEventRels;
    }
    /*FIM*/
    
    /* Devolve as lojas a que um vendedor está associado */
    function getProvidersAssociatedToUser($userId, $nrorg) {
        $evtEventSeller = EvtEventSeller::class;
        $evtEvent       = EvtEvent::class;
        
        $providers = $this->getEntityManager()->createQuery("
            SELECT ee.id, ee.name, ee.imageLogo, ee.type FROM $evtEvent ee
            LEFT JOIN $evtEventSeller ees WITH ees.evtEvent = ee.id
            WHERE ees.genUser = '$userId'
            AND ees.nrorg = '$nrorg' AND ee.type <> 'S' AND ee.type <> 'W'
        ")->getResult();
        
        return $providers;
    }
    
    function group_by($key, $data) {
    $result = array();
        
    foreach($data as $val) {
        if(array_key_exists($key, $val)){
            $result[$val[$key]][] = $val;
        }else{
            $result[""][] = $val;
        }
    }
    
    return $result;
    }
    
    function getProviders($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll=NULL, $initial=NULL, $max=NULL) {
        $store          = SerEvtEvent::class;
        $eventMenu      = SerEvtEventMenu::class;
        
        $structureQuery = $structureId !== NULL ? "AND s.genStructure = $structureId" : "";
        $parentQuery    = $parentId !== NULL ? "AND s.parentEvent = $parentId" : "";
        $nameQuery      = $storeName !== NULL ? "AND s.name = '$storeName'" : "";
        $nrorgQuery     = $nrorg !== NULL ? "AND s.nrorg = $nrorg" : "";
        $ownerQuery     = $ownerUser !== NULL ? "AND s.ownerUserId = $ownerUser" : "";
        $menuJoinType   = $showAll !== NULL ? "LEFT" : "INNER";
        
        $stores = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $store s
            $menuJoinType JOIN $eventMenu em WITH em.evtEvent = s.id
            WHERE s.type <> 'W' AND s.type <> 'S' AND s.type <> 'E'
            $structureQuery
            $parentQuery
            $nameQuery
            $nrorgQuery
            $ownerQuery
            AND s.status <> 'I'
            ORDER BY s.id
            "
        );
        
        if ($initial !== NULL) $stores->setFirstResult($initial);
        if ($max !== NULL) $stores->setMaxResults($max);
        
        $stores = new \Doctrine\ORM\Tools\Pagination\Paginator($stores);
        
        /* Informar locais de entrega da loja e se ela est<C3><A1> aberta */
        foreach ($stores as $store) {
            $store->setDeliversToTable($this->getEntityManager());
            $store->setDeliversToBalcony($this->getEntityManager());
            $store->isOpened($this->getEntityManager());
            $store->setWorkshifts($this->getEntityManager());
        }
        
        return $stores;
    }
    
    /* Devolve os produtos da loja, divididos por isCurrentWorkshift */
    function getProviderData($store) {
        $store = $this->getEntityManager()->getRepository(SerEvtEvent::class)->findOneBy(['id' => $store->getId()]);
        if ($store != NULL) $store->build($this->getEntityManager());
        
        return $store;
    }
    
    function updateProvider($storeId, $about, $imageCover, $imageLogo, $name, $type) {
        $store = $this->getEntityManager()->getRepository(OrdEvtEvent::class)->find($storeId);
        
        if ($store !== NULL) {
            $store->setAbout($about);
            if ($imageCover) $store->setImageCover($this->imageUploader->upload($imageCover));
            if ($imageLogo) $store->setImageLogo($this->imageUploader->upload($imageLogo));
            $store->setName($name);
            $store->setStatus('A');
            if($type){
                $store->setType($type);
            }
            
            $ordConfigStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        }
        
        $this->getEntityManager()->flush();
        
        return $store;
    }
    
    /* Cria uma loja com os parâmetros passados */
    function createProvider($name, $about, $structureId, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $type) {
        $structure   = $this->getEntityManager()->getRepository(SerGenStructure::class)->find($structureId);
        $ownerUser   = $this->getEntityManager()->getRepository(SerGenUser::class)->findOneBy(['email' => $ownerEmail]);
        $orgWorkflow = $this->getEntityManager()->getRepository(SerOrdWorkflow::class)->findOneBy(['nrorg' => $structure->getNrorg()]);
        
        if ($ownerUser == NULL) throw new \Exception('Owner user not found', 2);
        
        $store = new SerEvtEvent();
        $store->setName($name);
        $store->setAbout($about);
        $store->setStatus($status);
        $store->setType($type);
        $store->setStructure($structure);
        $store->setOwnerUser($ownerUser);
        $store->setNrorg($structure->getNrorg());
        if ($imageCover) $store->setImageCover($this->imageUploader->upload($imageCover));
        if ($imageLogo) $store->setImageLogo($this->imageUploader->upload($imageLogo));
        
        $configStore = new SerOrdConfigStore();
        $configStore->setEvent($store);
        $configStore->setNrorg($structure->getNrorg());
        $configStore->setWorkflow($orgWorkflow);
        
        $this->getEntityManager()->persist($store);
        $this->getEntityManager()->persist($configStore);
        $this->getEntityManager()->flush();
        
        $sellerType = $this->createSellerType($store->getId(), 'Owner', []);
        $this->linkUserToStore($store->getId(), $ownerEmail, $sellerType->getId(), $adminId);
        
        return $store;
    }
    
    function createSellerType($storeId, $sellerTypeName, $visibleStatus) {
        $store = $this->getEntityManager()->getRepository(OrdEvtEvent::class)->find($storeId);
        
        $sellerType = new EvtSellerType();
        $sellerType->setEvtEvent($store);
        $sellerType->setName($sellerTypeName);
        $sellerType->setNrorg($store->getNrorg());
        
        foreach ($visibleStatus as $status) {
            $status = $this->getEntityManager()->getRepository(GenStatus::class)->find($status['ID']);
            
            $statusUserType = new GenStatusUserType();
            $statusUserType->setGenStatus($status);
            $statusUserType->setEvtSellerType($sellerType);
            
            $this->getEntityManager()->persist($statusUserType);
        }
        
        $this->getEntityManager()->persist($sellerType);
        $this->getEntityManager()->flush();
        
        return $sellerType;
    }
    
    function linkUserToStore($storeId, $sellerEmail, $sellerTypeId, $adminId) {
        $store = $this->getEntityManager()->getRepository(OrdEvtEvent::class)->find($storeId);
        $user  = $this->getEntityManager()->getRepository(OrdGenUser::class)->findOneBy(['email' => $sellerEmail]);
        $sellerType = $this->getEntityManager()->getRepository(EvtSellerType::class)->find($sellerTypeId);
        
        if ($user == NULL) throw new Exception('User not found', 17);
        
        $sellerExists = $this->getEntityManager()->getRepository(OrdEvtEventSeller::class)->findOneBy(['genUser' => $user->getId(), 'evtEvent' => $storeId]) != NULL;
        if ($sellerExists == true) throw new Exception('Seller already linked', 18);
        
        $seller = new OrdEvtEventSeller();
        $seller->setEvtEvent($store);
        $seller->setGenUser($user);
        $seller->setNrorg($store->getNrorg());
        $seller->setEvtSellerType($sellerType);
        $seller->setCreatedBy($adminId);
        
        $this->getEntityManager()->persist($seller);
        $this->getEntityManager()->flush();
    }
    
    public function getDate() {
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d H:i:s');
        $newDate    = new \DateTime($date);
        
        return $newDate;
    }
    
    public function getEventsFromOrganizationAndTypeVersionAdminBackend($nrorg, $type) {
        $evtEvent     = EvtEvent::class;
        $genCategory  = GenCategory::class;
        $evtEventMenu = EvtEventMenu::class;
        $genStructure = GenStructure::class;
        
        $event =  $this->getEntityManager()->createQuery("
            SELECT DISTINCT evt.id, evt.type, evt.status, evt.nrorg, evt.name, evt.imageMap, evt.imageLogo, evt.imageCover, evt.initialDate, evt.finalDate, evt.classification, 
            evt.rating, evt.salesMethod, evt.location, evt.about, genCat.id as categoryId, genCat.name as nameCategory, genStruct.id as structureId, 
            genStruct.name as nameStructure
            FROM $evtEvent evt
            LEFT JOIN  $genCategory genCat WITH evt.genCategory = genCat.id
            LEFT JOIN  $genStructure genStruct WITH evt.genStructure = genStruct.id
            LEFT JOIN  $evtEventMenu evtm WITH evtm.evtEvent = evt.id
            WHERE evt.status = 'A' AND evt.nrorg = $nrorg AND evt.type IN ('AGENDA','INTEREST')
            ORDER BY evt.initialDate DESC
        ")->getResult();
        
        return $event;
    }
    
    public function getServicesChildrenByNrorgAndEvent($nrorg, $eventId, $type) {
        $genUser = GenUser::class;
        
        $serService = SerService::class;
        $serServiceEventRel = SerServiceEventRel::class;
        $serServiceSellerRel = SerServiceSellerRel::class;
        
        $evtEventSeller = EvtEventSeller::class;
        $evtEvent = EvtEvent::class;
        
        $users = [];
        
        $services = $this->getEntityManager()->createQuery("
            SELECT ss.id serviceId, ss.name serviceName, sssr.id serviceSellerId, structure.id structureId,
            ees.id eventSellerId, CONCAT(seller.firstName, ' ', seller.lastName) profissional, seller.image photo
            FROM $evtEvent ee
            JOIN ee.genStructure structure
            JOIN $evtEventSeller ees WITH ees.evtEvent = ee.id
            JOIN $genUser seller WITH seller.id = ees.genUser
            JOIN $serServiceEventRel sser WITH sser.evtEvent  = ee.id
            JOIN $serService ss WITH ss.id = sser.serService
            JOIN $serServiceSellerRel sssr WITH (sssr.serService = ss.id AND sssr.evtEventSeller = ees.id AND sssr.status = 'A')
            WHERE ee.id = '$eventId' 
            AND ee.nrorg = '$nrorg' 
            AND ss.type = '$type' 
            AND ee.status = 'A'
            AND ss.status = 'A'
        ")->getResult();
        $groupServices = array();

        foreach ($services as $i => $value) {
            array_push($groupServices, [
                'serviceId' => $value["serviceId"], 
                'structureId' => $value["structureId"], 
                'serviceName' => $value["serviceName"],
                'totalSchedule' => $this->countSchedule($value["serviceId"], null, $type, 'A'),
                'professionals' => []
            ]);
        }
        $data = $this->my_array_unique($groupServices, false);
        
        foreach ($services as $i => $value) {
            foreach($data as $j => $service) {
                if($service["serviceId"] == $value["serviceId"]) {
                    array_push($data[$j]['professionals'], [
                        'serviceSellerId' => $value['serviceSellerId'],
                        'eventSellerId' => $value['eventSellerId'],
                        'profissional' => $value['profissional'],
                        'photo' => $value['photo'],
                        'totalScheduleSeller' => $this->countSchedule($value["serviceId"], $value['serviceSellerId'], $type, 'A')    
                    ]);
                }
            }
        }
        
        return $data;
    }
    
    function my_array_unique($array, $keep_key_assoc = false){
        $duplicate_keys = array();
        $tmp = array();       
    
        foreach ($array as $key => $val){
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;
    
            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }
    
        foreach ($duplicate_keys as $key)
            unset($array[$key]);
    
        return $keep_key_assoc ? $array : array_values($array);
    }
    
    function countSchedule($serviceId, $serviceSellerId, $type, $status) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $today = $this->getDate();
        
        $queryService = $serviceId ? "AND gs.serService = '$serviceId' " : "";
        $queryServiceSeller = $serviceSellerId ? "AND gs.serServiceSeller = '$serviceSellerId' " : "";
        
        $totalSchedule = $this->getEntityManager()->createQuery("
            SELECT COUNT(gs.id) FROM $genSchedule gs
            LEFT JOIN gs.serServiceSeller sss
            WHERE gs.status = 'A' 
            AND gs.type = '$type'
            AND sss.status = 'A'
            AND gs.status = '$status'
            AND gs.finalDate >= '" . $today->format('Y-m-d H:i:s') . "'
            $queryService
            $queryServiceSeller
        ")->getResult();
        
        return $totalSchedule ? $totalSchedule[0][1] : [];
    }
    
    function getEventSellersFromProvider($storeId) {
        $store = $this->getEntityManager()->getRepository(SerEvtEvent::class)->find($storeId);
        $sellers = $this->getEntityManager()->createQuery(
            "
            SELECT ev
            FROM " . SerGenUser::class . " u
            JOIN " . SerEvtEventSeller::class . " ev WITH ev.genUser = u.id
            JOIN ev.evtEvent evt
            WHERE evt.id = '$storeId'
            "    
        )->getResult();
        foreach ($sellers as $seller) {
            $seller->build($this->getEntityManager(), $store->getNrorg());
        }
        return $sellers;
    }
    
    function unlinkUserFromProvider($storeId, $userId, $adminId) {
        $seller = $this->getEntityManager()->getRepository(SerEvtEventSeller::class)->findOneBy(['genUser' => $userId, 'evtEvent' => $storeId]);
        
        $serviceSellerRels = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->findBy(['evtEventSeller' => $seller->getId()]);
        
        $seller->setEvtEvent(null);
        $seller->setModifiedBy($adminId);
        
        $this->getEntityManager()->merge($seller);
        
        if(!empty($serviceSellerRels)) {
            foreach($serviceSellerRels as $serviceSeller) {
                $serviceSeller->setSerService(null);
                $serviceSeller->setStatus('I');
                
                $this->getEntityManager()->merge($serviceSeller);
            }
        }
        
        $this->getEntityManager()->flush();
    }
}