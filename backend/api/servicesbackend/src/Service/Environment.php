<?php
namespace Zeedhi\ApiServices\Service;

interface Environment {

    public function getUserId();

    public function setUserId($userId);

}