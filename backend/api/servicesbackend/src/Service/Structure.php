<?php
namespace Zeedhi\ApiServices\Service;

use Zeedhi\ApiServices\Model\Entities\GenStructure;
use Zeedhi\ApiServices\Model\Entities\GenAddress;
use Zeedhi\ApiServices\Model\Entities\EvtEvent;
use Zeedhi\ApiServices\Model\Entities\SerServiceEventRel;

use Doctrine\ORM\EntityManager;

class Structure extends UserOperation {
    
    protected $entityManager;
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    function getStructures($nrorg, $parentId, $event, $level, $initial=NULL, $max=NULL, $type=NULL) {
        $structure = GenStructure::class;
        
        $parentQuery = $parentId !== NULL ? "AND s.parent = $parentId" : "";
        $levelQuery  = $level    !== NULL ? "AND s.level = $level" : "";
        $eventQuery  = $event    !== NULL ? "AND s.evtEvent = $event" : "";
        $typeQuery   = $type     !== NULL ? "AND s.type = '$type'" : "";
        
        $structures  = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $structure s
            WHERE s.nrorg = $nrorg
            AND s.status = 'A'
            $parentQuery
            $levelQuery
            $eventQuery
            $typeQuery
            ORDER BY s.id
            "
        );
        
        if ($initial !== NULL) $structures->setFirstResult($initial);
        if ($max !== NULL) $structures->setMaxResults($max);
        
        $structures = new \Doctrine\ORM\Tools\Pagination\Paginator($structures);
        
        foreach ($structures as $structure) {
            $structure->build($this->getEntityManager());
        }
        
        return $structures;
    }
    
    function getStructureById($structureId, $nrorg) {
        $structure = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg]);
        if ($structure == NULL) throw new Exception('Structure not found', 1);
        
        $structure->build($this->getEntityManager());
        
        return $structure;
    }
    
    function getGenAddressById($addressId, $nrorg) {
        $genAddress = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['id' => $addressId, 'nrorg' => $nrorg]);

        return $genAddress;
    }
    
    public function createStructureAddress($street, $number, $neighborhood, $city, $provincy, $cep, $userId, $nrorg) {
        $structureAddress = new GenAddress();
        $structureAddress->setCreatedBy($userId);
        $this->buildGenAddress($structureAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg);
        $this->getEntityManager()->persist($structureAddress);
        $this->getEntityManager()->flush();
        
        return $structureAddress;
    }
    
    public function buildGenAddress($genAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg) {
        $genAddress->setStreet($street);
        $genAddress->setNumber($number);
        $genAddress->setNeighborhood($neighborhood);
        $genAddress->setCity($city);
        $genAddress->setProvincy($provincy);
        $genAddress->setCep($cep);
        $genAddress->setNrorg($nrorg);
    }
    
    public function updateGenAddress($genAddressId, $nrorg, $street, $number, $neighborhood, $city, $provincy, $cep, $userId) {
        $genAddress = $this->getGenAddressById($genAddressId, $nrorg);
        $genAddress->setModifiedBy($userId);
        $this->buildGenAddress($genAddress, $street, $number, $neighborhood, $city, $provincy, $cep, $nrorg);
        $this->getEntityManager()->flush();
        return $genAddress;
    }
    
    public function updateStructure($structureId, $nrorg, $name, $description, $parent, $structureLevel, $evtEventId, $type, $status, $userId, $genAddress, $email = NULL) {
        $genStructure = $this->getStructureById($structureId, $nrorg);
        $genStructure->setModifiedBy($userId);
        if($parent)   $parent = $this->getStructureById($parent, $nrorg); 
        $this->buildGenStructure($genStructure, $name, $nrorg, $description, $type, $status, $parent, $structureLevel, $evtEventId, $genAddress, $email);
        $this->getEntityManager()->flush();
        return true;
    }
    
    public function buildGenStructure($structure, $name, $nrorg, $description, $type, $status, $parentId, $structureLevel, $evtEventId, $genAddress, $email = NULL) {
        $structure->setName($name);
        $structure->setEmail($email);
        $structure->setNrorg($nrorg);
        $structure->setDescription($description);
        $structure->setType($type);
        $structure->setStatus($status);
        $structure->setGenAddress($genAddress);
        if ($structureLevel !== NULL) $structure->setLevel($structureLevel);
        if ($parentId != NULL) $structure->setParent($parentId);
        $structure->setEvtEvent($evtEventId);
    }
    
    public function createStructure($name, $nrorg, $description, $type, $userId, $parent = NULL, $genAddress = NULL, $structureLevel, $evtEventId = NULL, $email = NULL) {
        $structure = new GenStructure();
        $structure->setCreatedBy($userId);
        if($parent)   $parent = $this->getStructureById($parent, $nrorg); 
        $this->buildGenStructure($structure, $name, $nrorg, $description, $type, 'A', $parent, $structureLevel, $evtEventId, $genAddress, $email);
        $this->getEntityManager()->persist($structure);
        $this->getEntityManager()->flush();
        
        return $structure;
    }
    
    public function inactivateStructure($structureId, $nrorg) {
        $genStructure = $this->getStructureById($structureId, $nrorg);
        $genStructure->setStatus('I');
        $this->getEntityManager()->flush();
        return $genStructure;
    }
    
    public function getStructuresWithService($nrorg, $level, $status) {
        $evtEvent = EvtEvent::class;
        $genStructure = GenStructure::class;
        $serServiceEventRel = SerServiceEventRel::class;
        
        $structures = $this->getEntityManager()->createQuery(
            "
            SELECT gs
            FROM $genStructure gs
            LEFT JOIN $evtEvent ee WITH ee.status = 'A'
            LEFT JOIN $serServiceEventRel sser WITH sser.status = 'A'
            WHERE gs.level = '$level' 
            AND gs.nrorg = '$nrorg' AND gs.status = '$status'
            AND ee.genStructure = gs.id AND sser.evtEvent = ee.id
            "
        )->getResult();
        
        $data       = array();
        
        foreach($structures as $structure) {
            array_push($data, $structure->toArray($this->getEntityManager()));
        }
        
        return $data;
    }
}