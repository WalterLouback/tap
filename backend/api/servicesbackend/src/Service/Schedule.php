<?php
namespace Zeedhi\ApiServices\Service;

use Zeedhi\ApiServices\Model\Entities\GenSchedule;
use Zeedhi\ApiServices\Model\Entities\GenScheduleUserRel;
use Zeedhi\ApiServices\Model\Entities\GenScheduleStructureRel;
use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiServices\Model\Entities\SerServiceEventRel;
use Zeedhi\ApiServices\Model\Entities\SerServiceSellerRel;
use Zeedhi\ApiServices\Model\Entities\SerTimesheet;
use Zeedhi\ApiServices\Model\Entities\SerTimeTimesheet;
use Zeedhi\ApiServices\Model\Entities\SerTimesheetRel;
use Zeedhi\ApiServices\Model\Entities\SerDayTimesheet;
use Zeedhi\ApiServices\Model\Entities\GenStructure;
use Zeedhi\ApiServices\Model\Entities\GenUser;
use Zeedhi\ApiServices\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiServices\Model\Entities\GenContact;


use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller;

use Zeedhi\ApiEvents\Model\Entities\EvtEvent;

use Doctrine\ORM\EntityManager;

class Schedule extends UserOperation {
    
    /**
     * Auth constructor.
     * @param EntityManager $entityManager
     * @param Environment   $environment
     */
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SCHEDULE*/
    public function getSchedulesByNrorgAndService($serviceId, $nrorg) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $schedules = $this->getEntityManager()->createQuery("
            SELECT gs FROM $genSchedule gs
            LEFT JOIN $genScheduleUserRel gsur WITH gsur.genSchedule = gs
            WHERE gs.nrorg = '$nrorg' 
            AND gs.serService = '$serviceId' AND gs.status = 'A'
        ")->getResult();
        
        $data       = array();
        
        foreach($schedules as $schedule) {
            if($schedule->getFinalDate() >= $this->getCurretDate()){
                array_push($data, $schedule->toArray($this->getEntityManager()));
            }
        }
        
        return $data;
    }
    
    public function getSchedulesByNrorgAndServiceSeller($serviceSellerId, $nrorg) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $schedules = $this->getEntityManager()->createQuery("
            SELECT gs FROM $genSchedule gs
            LEFT JOIN $genScheduleUserRel gsur WITH gsur.genSchedule = gs
            WHERE gs.nrorg = '$nrorg' 
            AND gs.serServiceSeller = '$serviceSellerId' AND gs.status = 'A'
        ")->getResult();
        
        
        $data       = array();
        
        foreach($schedules as $schedule) {
            if($schedule->getFinalDate() >= $this->getCurretDate()){
                array_push($data, $schedule->toArray($this->getEntityManager()));
            }
        }
        
        return $data;
    }
    /**
     * para criar um agendamento este metodo verifica se o tempo de execucao do 
     * servico e maior que o periodo do quandro de horarios
     * se o tempo for maior este metodo inseri mais de um agendamento.
     * ao inserir um agendamento este metodo verifica se e o primeiro agendamento, 
     * se for este metodo guarda o registro na var $schedule 
     * e inseri o ID do primeiro registro nos demais, e retorna o primeiro registro.
     */
    public function createSchedule($initialDate, $finalDate, $type=null, $appointment, 
                                    $nrorg, $createdBy, $modifiedBy, $serviceId, 
                                    $serviceSellerId, $timeTimesheetId, $structureId=null) {
        $timesThatNeedToBeOccupied = $this->checksServiceTimeTakesMoreThanOneHour($timeTimesheetId, $serviceId, $nrorg);
        
        if($timesThatNeedToBeOccupied["numberInserts"] > 1) {
            $timeTimesheetIds = $this->getSerTimeTimesheetIds($timesThatNeedToBeOccupied, $nrorg);
            $schedule = null;
            
            foreach($timeTimesheetIds as $value) {
                
                if($schedule === null) {
                    $firstSchedule = $this->insertschedule($initialDate, $finalDate, $type=null, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $value["id"], $structureId=null, null);
                    $schedule = $firstSchedule;
                }else {
                    $firstSchedule = $this->insertschedule($initialDate, $finalDate, $type=null, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $value["id"], $structureId=null, $schedule->getId());
                }
            }
        }else {
            $schedule = $this->insertschedule($initialDate, $finalDate, $type=null, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timeTimesheetId, $structureId=null, null);
        }
        return $schedule;
    }
    
    function getSerTimeTimesheetIds($array, $nrorg) {
        return $this->getEntityManager()->createQuery(
            "   SELECT stt.id
                FROM " . SerTimeTimesheet::class . " stt
                WHERE stt.initialTime >= '". $array['initialTime'] ."' AND stt.finalTime <= '" . $array['finalTime'] . "' AND stt.serTimesheet = '" . $array['serTimesheet'] . "' AND stt.status = 'A' AND stt.nrorg = $nrorg
            ")->getResult();
    }
    
    function insertschedule($initialDate, $finalDate, $type=null, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timeTimesheetId, $structureId=null, $scheduleId=null) {
        $schedule = new GenSchedule();
        $service  = $this->getEntityManager()->getRepository(SerService::class)->findOneBy(["id" => $serviceId, "nrorg" => $nrorg]);
        
        if($service != null && !empty(trim($service->getType()))) {
            $type = $service->getType();
        } else { 
            throw new Exception('This service does not have a type.', 4485);
        }
        
        if($structureId)
            $schedule->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        if($nrorg)
            $schedule->setNrorg($nrorg);
        if($createdBy)
            $schedule->setCreatedBy($createdBy);
        if($modifiedBy)
            $schedule->setModifiedBy($modifiedBy);
        if($initialDate)
            $schedule->setInitialDate($initialDate);
        if($finalDate)
            $schedule->setFinalDate($finalDate);
        if($type)
            $schedule->setType($type);
        if($appointment)
            $schedule->setAppointment($appointment);
        if($serviceId)
            $schedule->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        if($serviceSellerId)
            $schedule->setSerServiceSeller($this->getEntityManager()->getReference(SerServiceSellerRel::class, $serviceSellerId));
        if($timeTimesheetId)
            $schedule->setSerTimeTimesheet($this->getEntityManager()->getReference(SerTimeTimesheet::class, $timeTimesheetId));
        if($scheduleId)
            $schedule->setGenSchedule($this->getEntityManager()->getReference(GenSchedule::class, $scheduleId));
        
        $schedule->setStatus("A");
        
        $this->getEntityManager()->persist($schedule);
        $this->getEntityManager()->flush();
        
        return $schedule;
    }
    
    public function updateSchedule($scheduleId, $initialDate, $finalDate, $type, $appointment, $nrorg, $createdBy, $modifiedBy, $serviceId, $serviceSellerId, $timeTimesheetId, $structureId=null) {
        $schedule = $this->getEntityManager()->getRepository(GenSchedule::class)->find($scheduleId);
        
        if($structureId)
            $schedule->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        if($nrorg)
            $schedule->setNrorg($nrorg);
        if($createdBy)
            $schedule->setCreatedBy($createdBy);
        if($modifiedBy)
            $schedule->setModifiedBy($modifiedBy);
        if($initialDate)
            $schedule->setInitialDate($initialDate);
        if($finalDate)
            $schedule->setFinalDate($finalDate);
        if($type)
            $schedule->setType($type);
        if($appointment)
            $schedule->setAppointment($appointment);
        if($serviceId)
            $schedule->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        if($serviceSellerId) {
             $schedule->setSerServiceSeller($this->getEntityManager()->getReference(SerServiceSellerRel::class, $serviceSellerId));
        }else {
            $schedule->setSerServiceSeller(null);
        }
        if($timeTimesheetId)
            $schedule->setSerTimeTimesheet($this->getEntityManager()->getReference(SerTimeTimesheet::class, $timeTimesheetId));
        
        $this->getEntityManager()->persist($schedule);
        
        $this->getEntityManager()->flush();
        
        return $schedule;
    }
    /*FIM*/
    
    /*METODOS QUE MANIPULAM A TABELA GEN_SCHEDULE_USER_REL*/
    public function getScheduleUserRelsByUser($userId, $serviceSellerId, $timeTimesheetId, $nrorg, $scheduleId=null) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        $queryBackoffice = "";
        
        if($serviceSellerId != null && $timeTimesheetId != null) {
            $serServiceSeller = $this->getEntityManager()->getRepository(SerServiceSellerRel::class)->findOneBy(["id" => $serviceSellerId, "nrorg" => $nrorg]);
            $serviceSellerId = $serServiceSeller->getId();
            $queryBackoffice = " AND gs.serServiceSeller = '$serviceSellerId' AND gs.serTimeTimesheet = '$timeTimesheetId' ";
        }
        
        $querySchedule = $scheduleId !== null ? "AND gsur.genSchedule = '$scheduleId' " : "";
        
        $scheduleUserRels = $this->getEntityManager()->createQuery(
            "
            SELECT gsur
            FROM $genScheduleUserRel gsur
            LEFT JOIN $genSchedule gs WITH gsur.genSchedule = gs.id
            WHERE gsur.genUser = '$userId' AND gs.nrorg = '$nrorg' AND gs.status = 'A'
            $querySchedule
            $queryBackoffice
            "
        )->getResult();
        $data       = array();
        
        foreach($scheduleUserRels as $scheduleUserRel) {
            array_push($data, $scheduleUserRel->toArray($this->getEntityManager()));
        }
        
        return !empty($data) ? $data[0] : [];
    }
    
    /*METODOS QUE RETORNA OS AGENDAMENTOS DE UM USUARIO*/
    public function getAllSchedulesByUser($userId, $nrorg, $scheduleId=null) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $querySchedule = $scheduleId !== null ? "AND gsur.genSchedule = '$scheduleId' " : "";
        
        $today = $this->getDate()->format('Y-m-d');
        
        $scheduleUserRels = $this->getEntityManager()->createQuery(
            "
            SELECT gsur
            FROM $genScheduleUserRel gsur
            LEFT JOIN $genSchedule gs WITH gsur.genSchedule = gs.id
            WHERE gsur.genUser = '$userId' AND gs.nrorg = '$nrorg'
            AND gs.status = 'A' AND gs.initialDate >= '$today'
            $querySchedule
            "
        )->getResult();
        $data       = array();
        
        foreach($scheduleUserRels as $scheduleUserRel) {
            array_push($data, $scheduleUserRel->toArray($this->getEntityManager()));
        }
        
        return !empty($data) ? $data : [];
    }
    
    public function getScheduleUserRelsBySchedule($scheduleId, $nrorg) {
        $genSchedule = GenSchedule::class;
        $genScheduleUserRel = GenScheduleUserRel::class;
        
        $scheduleUserRels = $this->getEntityManager()->createQuery(
            "
            SELECT gsur
            FROM $genScheduleUserRel gsur
            LEFT JOIN gsur.genSchedule gs WITH (gsur.genSchedule = gs.id)
            WHERE gs.nrorg = '$nrorg' AND gs.id = '$scheduleId' AND gs.status IN ('A', 'C')
            "
        )->getResult();
        
        $data       = array();
        
        foreach($scheduleUserRels as $scheduleUserRel) {
            array_push($data, $scheduleUserRel->toArray($this->getEntityManager()));
        }
        
        return !empty($data) ? $data[0] : [];
    }
    
    public function scheduleUser($userId, $scheduleId, $nrorg, $createdBy, $modifiedBy) {
        $scheduleUserRel = new GenScheduleUserRel();
        
        $scheduleUserRel->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        $scheduleUserRel->setGenSchedule($this->getEntityManager()->getReference(GenSchedule::class, $scheduleId));
        
        $scheduleUserRel->setStatus("A"); /* A =>  Agendado, Cliente agendou um serviço */
        
        if($nrorg)
            $scheduleUserRel->setNrorg($nrorg);
        if($createdBy)
            $scheduleUserRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $scheduleUserRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($scheduleUserRel);
        
        $this->getEntityManager()->flush();
        
        return $scheduleUserRel;
    }
    
    public function cancelSchedule($userId, $scheduleId, $nrorg, $modifiedBy) {
        $schedule = $this->getEntityManager()->getRepository(GenSchedule::class)->findOneBy(["id" => $scheduleId, "nrorg" => $nrorg, "status" => 'A']);
        $scheduleUserRel = $this->getEntityManager()->getRepository(GenScheduleUserRel::class)->findOneBy(["genUser" => $userId, "genSchedule" => $scheduleId, "nrorg" => $nrorg, "status" => 'A']);
        
        $schedule->setStatus("I"); /* I =>  Cancelado, Cliente o agendamento */
        $scheduleUserRel->setStatus("I"); /* I =>  Cancelado, Cliente o agendamento */
        
        if($modifiedBy)
            $scheduleUserRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($schedule);
        $this->getEntityManager()->persist($scheduleUserRel);
        
        $this->getEntityManager()->flush();
        
        return $scheduleUserRel;
    }
    
    public function checkInSchedule($userId, $scheduleId, $nrorg, $modifiedBy) {
        $scheduleUserRel    = $this->getEntityManager()->getRepository(GenScheduleUserRel::class)->findOneBy(["genUser" => $userId, "genSchedule" => $scheduleId, "nrorg" => $nrorg, "status" => 'A']);
        $schedule           = $this->getEntityManager()->getRepository(GenSchedule::class)->findOneBy(["id" => $scheduleId, "nrorg" => $nrorg, "status" => 'A']);
        
        if($scheduleUserRel === NULL || $schedule === NULL){
            return NULL;
        } else {
            $scheduleUserRel->setStatus("C"); /* C =>  CONFIRMADO, Cliente realizou o serviço */
            $schedule->setStatus("C");
            
            if($modifiedBy)
                $scheduleUserRel->setModifiedBy($modifiedBy);
            
            $this->getEntityManager()->persist($scheduleUserRel);
            
            $this->getEntityManager()->flush();
            
            return $scheduleUserRel;
        }
    }
    /*FIM*/
    
    public function createScheduleStructureRel($structureId, $scheduleId, $nrorg, $createdBy, $modifiedBy) {
        $scheduleStructureRel = new GenScheduleStructureRel();
        
        $scheduleStructureRel->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        $scheduleStructureRel->setGenSchedule($this->getEntityManager()->getReference(GenSchedule::class, $scheduleId));
        
        if($nrorg)
            $scheduleStructureRel->setNrorg($nrorg);
        if($createdBy)
            $scheduleStructureRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $scheduleStructureRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($scheduleStructureRel);
        
        $this->getEntityManager()->flush();
        
        return $scheduleStructureRel;
    }
    
    public function updateScheduleStructureRel($scheduleStructureRelId, $structureId, $scheduleId, $nrorg, $createdBy, $modifiedBy) {
        $scheduleStructureRel = $this->getEntityManager()->getRepository(GenScheduleStructureRel::class)->find($scheduleStructureRelId);
        
        $scheduleStructureRel->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $structureId));
        $scheduleStructureRel->setGenSchedule($this->getEntityManager()->getReference(GenSchedule::class, $scheduleId));
        
        if($nrorg)
            $scheduleStructureRel->setNrorg($nrorg);
        if($createdBy)
            $scheduleStructureRel->setCreatedBy($createdBy);
        if($modifiedBy)
            $scheduleStructureRel->setModifiedBy($modifiedBy);
        
        $this->getEntityManager()->persist($scheduleStructureRel);
        
        $this->getEntityManager()->flush();
        
        return $scheduleStructureRel;
    }
    
    public function getScheduleBySchedule($scheduleId) {
        $schedule = $this->getEntityManager()->getRepository(GenSchedule::class)->find($scheduleId);
        
        return $schedule;
    }
    
    public function getScheduleBySchedule1($scheduleId) {
        $schedule = $this->getEntityManager()->getRepository(GenSchedule::class)->find($scheduleId);
        
        return $schedule->toArray($this->getEntityManager());
    }
    /*FIM*/
    
    public function getTimeTimeSheet($timeTimesheetId) {
        $time = $this->getEntityManager()->getRepository(SerTimeTimesheet::class)->find($timeTimesheetId);
        return $time;
    }
    
    public function getVacantTimesheet($eventId, $dayOfWeek, $date, $nrorg, $timesheetIds) {
        $timeTimesheet = new SerTimeTimesheet();
        
        return $timeTimesheet->build($this->getEntityManager(), null, $dayOfWeek, $date, $nrorg, $timesheetIds);
    }
    
        
    public function getVacantTimesheetWithInterested($eventId, $dayOfWeek, $date, $nrorg, $timesheetIds) {
        $timeTimesheet = new SerTimeTimesheet();
        
        return $timeTimesheet->buildInterested($this->getEntityManager(), null, $dayOfWeek, $date, $nrorg, $timesheetIds);
    }

    public function getTimesheetId($eventId, $serviceId=null) {
        if($serviceId != null){
            $serTimesheets = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findBy(['serService' => $serviceId]);
            if(empty($serTimesheets)) {
                if($eventId != null) {
                    $serTimesheets = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findBy(['evtEvent' => $eventId]);
                } else {
                    throw new Exception('please inform event_id.', 49877);
                }
            }
        } else {
            $serTimesheets = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findBy(['evtEvent' => $eventId]);
        }
        
        $data = array();
        
        if(!empty($serTimesheets)) {
            foreach($serTimesheets as $serTimesheet) {
                array_push($data, $serTimesheet->toArrayId());
            }
        }
        return $data;
    }
    
    public function getDataProfessionalByEvent($eventId, $nrorg){
        $professionals = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findBy(['evtEvent' => $eventId, 'nrorg' => $nrorg]);
        
        $data = array();
        
        if(!empty($professionals)) {
            foreach($professionals as $professional){
                array_push($data, $professional->toArray());
            }
        }
        return $data;
    }
    
    public function getAvailableProfessionalsByHour($professionals, $serviceId, $timeTimesheetId, $dataAppointment){
        $serServiceSellerRel = SerServiceSellerRel::class;
        $genSchedule = GenSchedule::class;
        $date = $dataAppointment->format('Y-m-d');
        
        $queryIds = '(';
        
        foreach($professionals as $i => $professional){
            $queryIds = $queryIds . $professional['ID'];
            
            if($i + 1 == sizeof($professionals)){
                $queryIds = $queryIds . ')';
            } else {
                $queryIds = $queryIds . ', ';
            }
        }
        
        $arraySerServiceSeller = $this->getEntityManager()->createQuery("
            SELECT sssr 
            FROM $serServiceSellerRel sssr
            WHERE sssr.evtEventSeller IN $queryIds
            AND sssr.serService = $serviceId
            AND sssr.status = 'A'
            AND sssr.id NOT IN (
                SELECT sssr2.id FROM $genSchedule gs
                JOIN gs.serServiceSeller sssr2
                WHERE gs.serTimeTimesheet = $timeTimesheetId
                AND (gs.initialDate BETWEEN '$date 00:00:00' AND '$date 23:59:59')
            )
        ")->getResult();
        
        $data = array();
        
        if(!empty($arraySerServiceSeller)) {
            foreach($arraySerServiceSeller as $serServiceSeller) {
                array_push($data, $serServiceSeller->toArray());
            }
        }
        return $data;
        
    }
    
    /* Busca todos os profissionais de um servico num fornecedor. */
    public function getProfessionalsByService($eventId, $serviceId, $nrorg, $date) {
        $serServiceSellerRel = SerServiceSellerRel::class;
        
        $arrayProfessionals = $this->getEntityManager()->createQuery("
            SELECT sssr 
            FROM $serServiceSellerRel sssr
            JOIN sssr.evtEventSeller ees
            JOIN ees.evtEvent evtn
            JOIN sssr.serService serv
            WHERE evtn.id = $eventId
            AND serv.id = $serviceId
            AND sssr.nrorg = $nrorg
            AND sssr.status = 'A'
        ")->getResult();
        
        $data = array();
        
        if(!empty($arrayProfessionals)) {
            foreach($arrayProfessionals as $professional) {
                array_push($data, $professional->toArray($date));
            }
        }
        
        $status = 'A';
        $schedules = array();
        $unavailable = array();
        
        foreach($data as $i => $val) {
            $professionalId = $val["ID"]; /*id do serServiceSeller*/
            $daysAvailables = $val["DAYS_AVAILABLE"]; /* dias livres */
            
            foreach($daysAvailables as $t => $daysAvailable) {
                // $daysAvailable['day'] -> dia da semana, 0 = segunda e 6 = domingo
                array_push($data[$i]["DAYS_AVAILABLE"][$t]["hour"], $this->getHourByProfessional($professionalId, $data[$i]["DAYS_AVAILABLE"][$t]['date'], $data[$i]["DAYS_AVAILABLE"][$t]['day']));
                if(count($data[$i]["DAYS_AVAILABLE"][$t]["hour"][0]) == 0) {
                    array_push($unavailable, $data[$i]["DAYS_AVAILABLE"][$t]);
                }
            }
        }
        
        return [$data, $unavailable];   
    }
    
    /* busca horarios disponiveis do profissional. */
    public function getHourByProfessional($professionalId, $dateAppointment, $dayOfWeek) {
        $serTimeTimesheet = SerTimeTimesheet::class;
        $genSchedule = GenSchedule::class;
        $serTimesheet = SerTimesheet::class;
        $serTimesheetRel = SerTimesheetRel::class;
        $evtEventSeller = EvtEventSeller::class;
        $serServiceSellerRel = SerServiceSellerRel::class;
        
        $serTimeTimesheetRef = new SerTimeTimesheet();
        if(!is_string($dateAppointment)) {
            $date = $dateAppointment->format('Y-m-d');
        }else {
            $date = $dateAppointment;
        }
        
        date_default_timezone_set('America/Sao_Paulo');
        $today = date('Y-m-d');
        $time  = date('H:i:s');
        
        $professionals = $this->getEntityManager()->createQuery("
                SELECT sssr.id
                FROM $serServiceSellerRel sssr
                JOIN sssr.evtEventSeller ees
                WHERE sssr.status = 'A'
                AND ees.id = (
            	    SELECT ees2.id
            	    FROM $serServiceSellerRel sssr2
            	    JOIN sssr2.evtEventSeller ees2
                    WHERE sssr2.id = $professionalId)
            ")->getResult();

        $professionalIds = [];

        foreach($professionals as $professional) {
            array_push($professionalIds, $professional["id"]);
        }
        $arrayProfessionals = join(", ", $professionalIds);

        if($date == $today) {
            $arrayHours = $this->getEntityManager()->createQuery("
                SELECT stt FROM $serTimeTimesheet stt
                LEFT JOIN $serTimesheetRel str WITH str.serTimesheet = stt.serTimesheet
                LEFT JOIN $serTimesheet st WITH st.id = stt.serTimesheet
                LEFT JOIN $evtEventSeller ees WITH str.evtEvent = ees.evtEvent
                LEFT JOIN $serServiceSellerRel sssr WITH sssr.evtEventSeller = ees.id
                WHERE sssr.id IN ($arrayProfessionals)
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%' AND stt.initialTime >= '$time'
                AND stt.status = 'A'
                AND st.status = 'A'
                AND (sssr.initialTime IS NULL OR stt.initialTime >= sssr.initialTime) 
                AND (sssr.finalTime IS NULL OR stt.finalTime <= sssr.finalTime)
                AND stt.id NOT IN (
                    SELECT stt2.id
                    FROM $genSchedule gs
                    JOIN gs.serTimeTimesheet stt2
                    JOIN gs.serServiceSeller sssr1
                    WHERE sssr1.id IN ($arrayProfessionals)
                    AND (gs.initialDate BETWEEN '$date 00:00:00' AND '$date 23:59:59')
                )
            ")->getResult();
        } else {
            $arrayHours = $this->getEntityManager()->createQuery("
                SELECT stt FROM $serTimeTimesheet stt
                LEFT JOIN $serTimesheetRel str WITH str.serTimesheet = stt.serTimesheet
                LEFT JOIN $serTimesheet st WITH st.id = stt.serTimesheet
                LEFT JOIN $evtEventSeller ees WITH str.evtEvent = ees.evtEvent
                LEFT JOIN $serServiceSellerRel sssr WITH sssr.evtEventSeller = ees.id
                WHERE sssr.id IN ($arrayProfessionals)
                AND stt.serDayTimesheetValues LIKE '%$dayOfWeek%'
                AND st.status = 'A'
                AND stt.status = 'A'
                AND (sssr.initialTime IS NULL OR stt.initialTime >= sssr.initialTime) 
                AND (sssr.finalTime IS NULL OR stt.finalTime <= sssr.finalTime)
                AND stt.id NOT IN (
                    SELECT stt2.id
                    FROM $genSchedule gs
                    JOIN gs.serTimeTimesheet stt2
                    JOIN gs.serServiceSeller sssr1
                    WHERE sssr1.id IN ($arrayProfessionals)
                    AND (gs.initialDate BETWEEN '$date 00:00:00' AND '$date 23:59:59')
                )
            ")->getResult();
        }
        
        $data = array();
        
        if(!empty($arrayHours)) {
            foreach ($arrayHours as $hour) {
                array_push($data, $hour->toArray());
            }
        }
        return $data;
    }
        
    public function getUserFromOrganizationAndProfile($nrorg, $cpf, $npf, $email) {
        $genUser = GenUser::class;
        $genContact = GenContact::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $queryWhere = "";
        
        if($npf){
            $queryWhere = "utr.externalId = '$npf' AND utr.nrorg = '$nrorg' AND utr.status = 'A' AND";
        }elseif($cpf) {
            $queryWhere = "u.cpf = '$cpf' AND";
        }else {
            $queryWhere = "u.email = '$email' AND";
        }
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id, CONCAT(u.firstName, ' ', u.lastName) fullName, u.cpf, u.email, gc.phone
            FROM $genUser u
            LEFT JOIN $genContact gc WITH (gc.genUser = u AND gc.type = 'MOBILE')
            LEFT JOIN $genUserTypeRel utr WITH utr.genUser = u
            WHERE $queryWhere u.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getUserByExternalId($externalId) {
        $genUser = GenUser::class;
        $genContact = GenContact::class;
        $genUserTypeRel = GenUserTypeRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT u.id
            FROM $genUserTypeRel utr
            JOIN utr.genUser u
            WHERE utr.externalId = '$externalId' AND u.status = 'A' AND utr.status = 'A' AND utr.genUserType = 1
            "
        )->getResult();
        
        return !empty($user) ? $user : NULL;
    }
    
    public function getGeneralServiceSchedule($eventId, $type, $serviceSellerId, $serviceId, $date) {
        $serService         = SerService::class;
        $serServiceEvent    = SerServiceEventRel::class;
        $serServiceSeller   = SerServiceSellerRel::class;
        $genSchedule        = GenSchedule::class;
        
        $queryServiceSellerId   = $serviceSellerId ? " AND gs.serServiceSeller IN (" . join(", ", $serviceSellerId) . ")" : "";
        $queryServiceId         = $serviceId ? " AND gs.serService IN (". join(", ", $serviceId) . ")" : "";
        
        $services  = $this->getEntityManager()->createQuery("
            SELECT DISTINCT ss.id FROM $serService ss
            LEFT JOIN $serServiceEvent sse WITH sse.serService = ss.id 
            LEFT JOIN $serServiceSeller sss WITH sss.serService = ss.id 
            LEFT JOIN $genSchedule gs WITH gs.serService = ss.id 
            WHERE sse.evtEvent = '$eventId' AND ss.type = '$type'
        ")->getResult();
        
        if(!empty($services)) {
            $serviceIds = [];
            
            foreach($services as $service) {
                array_push($serviceIds, $service["id"]);
            }
            $arrayIds = join(", ", $serviceIds);
            
            $schedules = $this->getEntityManager()->createQuery("
                SELECT gs FROM $genSchedule gs
                WHERE gs.type = '$type' AND gs.status IN ('A', 'C') AND gs.serService IN ($arrayIds)
                AND (gs.initialDate BETWEEN '$date 00:00:00' AND '$date 23:59:59')
                $queryServiceSellerId
                $queryServiceId
            ")->getResult();
            
            $data = [];
            if(!empty($schedules)) {
                foreach($schedules as $schedule) {
                    array_push($data, $schedule->toArray($this->getEntityManager()));
                }
                
                $key = "SERVICE_ID";
                $result = $this->group_by($key, $data);
                
                return $result;
            } else {
                return [];
            }
        }else {
            return [];
        }
        
    }
    
    public function group_by($key, $data) {
        $result = array();
            
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[] = $val;
            }
        }
        
        return $result;
    }
    /*MANAGING METHODS USER*/
    public function importUser($firstName, $lastName, $cpf = NULL, $email = NULL, $password = NULL, $image = NULL, $nrorg, $status, $externalId = NULL) {
        $genUser = new GenUser();
        
        $genUser->setFirstName($firstName);
        $genUser->setLastName($lastName);
        $genUser->setStatus($status);
        !empty($image)       ? $genUser->setImage($image)   : "";
        !empty($cpf)         ? $genUser->setCpf($cpf)       : "";
        !empty($email)       ? $genUser->setEmail($email)   : "";
        
        $user = $this->getEntityManager()->persist($genUser);
        
        $this->addProfileUser($user, 1, $nrorg, $status, $externalId);
        
        return $user->getId();
    }
    
    public function addProfileUser($user, $userTypeId, $nrorg, $status, $externalId = NULL) {
        $userProfile = new GenUserTypeRel();
        
        $userProfile->setGenUser($user);
        $userProfile->setGenUserType($this->getEntityManager()->getReference(GenUserType::class, $userTypeId));
        $userProfile->setNrorg($nrorg);
        $userProfile->setStatus($status);
        $externalId ? $userProfile->setExternalId($externalId) : "";
        
        return $this->getEntityManager()->persist($userProfile);
    }
    
    /*pegar data atual menos 1 dia*/
    public function getCurretDate() {
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d ' . '23:59:55');
        $newDate    = new \DateTime($date);
        $newDate->sub(new \DateInterval('P1D'));
        
        return $newDate;
    }
    
    public function getDate() {
        date_default_timezone_set('America/Sao_Paulo');
        $date       = date('Y-m-d');
        $newDate    = new \DateTime($date);
        
        return $newDate;
    }
    
    public function getTimes() {
        $serTimeTimesheet   = SerTimeTimesheet::class;
        
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT stt.id, stt.initialTime, stt.finalTime, stt.status
            FROM $serTimeTimesheet stt 
            WHERE stt.status = 'A' AND stt.nrorg is NULL AND stt.serTimesheet is NULL
            "
        )->getResult();
        
        return $times;
    }
    
    public function getTimesheetByProvider($providerId, $nrorg, $timesheetId) {
        $serTimesheetRel    = SerTimesheetRel::class;
        $serTimeTimesheet   = SerTimeTimesheet::class;
        $serDayTimesheet    = SerDayTimesheet::class;
        $daysOfWeek = ['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo'];
        
        $queryProvider      = $providerId ? "str.evtEvent = '$providerId' AND " : "";

        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT stt.id, stt.initialTime, stt.finalTime, stt.status, stt.type,
            st.id typeTimesheet, st.name nameTimesheet, stt.serDayTimesheetValues daysOfWeek, stt.maxScheduling as max_scheduling
            FROM $serTimeTimesheet stt 
            LEFT JOIN $serTimesheetRel str WITH ($queryProvider str.status = 'A')
            LEFT JOIN stt.serTimesheet st
            WHERE $queryProvider stt.status = 'A' AND stt.nrorg = '$nrorg' AND stt.serTimesheet = '$timesheetId' AND st.status = 'A'
            ORDER BY stt.initialTime
            "
        )->getResult();
        
        $days = [];
        if(!empty($times)) {
            foreach($times as $key => $time) {
                $arrayIds = explode(",", $times[$key]["daysOfWeek"]);
                
                foreach($arrayIds as $i => $id) {
                    $days[] = $daysOfWeek[$id];
                }
                $times[$key]["daysOfWeekSelect"] = $days;
                unset($days);
            }
        }
        return $times;
    }
    
    public function getTimesheetsByProvider($providerId, $nrorg, $serviceId=null) {
        $serTimesheetRel    = SerTimesheetRel::class;
        $serTimesheet       = SerTimesheet::class;
        
        $query = "";
        $query1 = "";
        
        if($serviceId != null) {
            $query = "LEFT JOIN $serTimesheetRel str WITH str.status = 'A'";
            $query1 = "str.serService = '$serviceId' AND ";
        }else {
            $query = "LEFT JOIN $serTimesheetRel str WITH str.status = 'A'";
            $query1 = "str.evtEvent = '$providerId' AND ";
        }
        
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, 
            st.name, st.time, st.nrorg
            FROM $serTimesheet st
            $query
            WHERE $query1 str.serTimesheet = st.id 
            AND st.status = 'A' AND st.nrorg = '$nrorg'
            ORDER BY st.id
            "
        )->getResult();
        
        $data = array();
        
        foreach($times as $o => $time) {
            $data[$o] = [
                'id' => $time['id'],
                'initialTime' => $time['initialTime'],
                'finalTime' => $time['finalTime'],
                'name' => $time['name'],
                'time' => $time['time'],
                'status' => $time['status'],
                'nrorg' => $time['nrorg'],
                'hours' => []
            ];
            array_push($data[$o]['hours'], $this->getTimeTimesheetByTimesheetId($nrorg, $time["id"]));
        }
        
        return $data;
    }
    
    public function getTimesheetsByService($serviceId, $nrorg, $status) {
        $serTimesheetRel    = SerTimesheetRel::class;
        $serTimesheet       = SerTimesheet::class;
        
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, 
            st.name, st.time, st.nrorg
            FROM $serTimesheet st
            LEFT JOIN $serTimesheetRel str WITH str.status = '$status'
            WHERE str.serService = $serviceId AND str.serTimesheet = st.id 
            AND st.status = 'A' AND st.nrorg = '$nrorg'
            ORDER BY st.id
            "
        )->getResult();
        
        $data = array();
        
        foreach($times as $o => $time) {
            $data[$o] = [
                'id' => $time['id'],
                'initialTime' => $time['initialTime'],
                'finalTime' => $time['finalTime'],
                'name' => $time['name'],
                'time' => $time['time'],
                'status' => $time['status'],
                'nrorg' => $time['nrorg'],
                'hours' => []
            ];
            array_push($data[$o]['hours'], $this->getTimeTimesheetByTimesheetId($nrorg, $time["id"]));
        }
        
        return $data;
    }
    
    public function createTimesheet($providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name) {
        $serTimesheet = new SerTimesheet();
        $serTimesheetRel = new SerTimesheetRel();
        
        $name ? $serTimesheet->setName($name) : "";
        $time ? $serTimesheet->setTime($time) : "";
        $nrorg ? $serTimesheet->setNrorg($nrorg) : "";
        $status ? $serTimesheet->setStatus($status) : "";
        $finalTime ? $serTimesheet->setFinalTime($finalTime) : "";
        $initialTime ? $serTimesheet->setInitialTime($initialTime) : "";
        
        $this->getEntityManager()->persist($serTimesheet);
        
        if($providerId != NULL && !empty($serTimesheet)){
            $serTimesheetRel->setSerTimesheet($serTimesheet);
            $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $providerId));
            $nrorg ? $serTimesheetRel->setNrorg($nrorg) : "";
            $status ? $serTimesheetRel->setStatus($status) : "";
            
            $this->getEntityManager()->persist($serTimesheetRel);
        }
        
        $this->getEntityManager()->flush();
        
        return $serTimesheet;
    }
    
    public function updateTimesheet($timesheetId, $providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name) {
        $serTimesheet = $this->getEntityManager()->getRepository(SerTimesheet::class)->find($timesheetId);
        
        if(!empty($serTimesheet)) {
        
            $name ? $serTimesheet->setName($name) : "";
            $time ? $serTimesheet->setTime($time) : "";
            $nrorg ? $serTimesheet->setNrorg($nrorg) : "";
            $status ? $serTimesheet->setStatus($status) : "";
            $finalTime ? $serTimesheet->setFinalTime($finalTime) : "";
            $initialTime ? $serTimesheet->setInitialTime($initialTime) : "";
            
            $this->getEntityManager()->persist($serTimesheet);
        
            $serTimesheetRel = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy(['serTimesheet' => $serTimesheet->getId()]);
            if(!empty($serTimesheetRel)) {
                $serTimesheetRel->setSerTimesheet($serTimesheet);
                $providerId ? $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $providerId)) : NULL;
                $nrorg ? $serTimesheetRel->setNrorg($nrorg) : "";
                $status ? $serTimesheetRel->setStatus($status) : "";
                
                $this->getEntityManager()->persist($serTimesheetRel);
            }
            
            $this->getEntityManager()->flush();
            
        }else {
            $serTimesheet = NULL;
        }
        return $serTimesheet;
    }
    
    public function getTimesheetsByNrorg($nrorg) {
        $serTimesheet       = SerTimesheet::class;
        $serTimeTimesheet   = SerTimeTimesheet::class;
        $serDayTimesheet    = SerDayTimesheet::class;
        
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, st.name, st.time
            FROM $serTimesheet st
            WHERE st.nrorg = '$nrorg' AND st.status = 'A' 
            ORDER BY st.id
            "
        )->getResult();
        
        return $times;
    }
    
    public function checkIntervalTimesheet($nrorg, $initialTime, $finalTime, $idTimesheet) {
        $serTimesheet       = SerTimesheet::class;
        
        $dataTimesheet = $this->getTimesheetById($idTimesheet);
        
        $initialTimesheet = $dataTimesheet->getInitialTime();
        $finalTimesheet = $dataTimesheet->getFinalTime();
        
        return ($initialTimesheet <= $initialTime) && ($finalTimesheet >= $finalTime);
    }
    
    public function getTimesheetById($timesheetId) {
        $serTimesheetData = $this->getEntityManager()->getRepository(SerTimesheet::class)->findOneBy(['id' => $timesheetId]);

        return $serTimesheetData;
    }
    
    public function checkExistingTimesheet($nrorg, $initialTime, $finalTime, $timesheetId, $daysOfWeek, $status, $type) {
        $serTimeTimesheet = SerTimeTimesheet::class;

        $times = $this->getEntityManager()->createQuery(
            "
            SELECT stt.serDayTimesheetValues
            FROM $serTimeTimesheet stt
            WHERE stt.serTimesheet = $timesheetId
            AND stt.nrorg = '$nrorg'
            AND stt.initialTime = '$initialTime'
            AND stt.finalTime = '$finalTime'
            AND stt.status = 'A'
            "
        )->getResult();
        
        if(count($times) > 0) {
            $arrayDayofWeek = explode(",", implode(",", $times[0]));
            $daysWeek = explode(",", $daysOfWeek);
            
            foreach($daysWeek as $day) {
                if(in_array($day, $arrayDayofWeek)) {
                    return true;
                }
            }
            return false;
            
        } else {
            return false;
        }
    }
    
    public function removeTimesheetOrganization($timesheetId) {
        $serTimesheet = $this->getEntityManager()->getRepository(SerTimesheet::class)->find($timesheetId);
        
        $serTimesheet->setStatus('I');
        
        $this->getEntityManager()->persist($serTimesheet);
        $this->getEntityManager()->flush();
    }
    
    public function removeTimeTimesheetOrganization($timetimesheetId) {
        $serTimeTimesheet = $this->getEntityManager()->getRepository(SerTimeTimesheet::class)->find($timetimesheetId);
        
        $serTimeTimesheet->setStatus('I');
        
        $this->getEntityManager()->persist($serTimeTimesheet);
        $this->getEntityManager()->flush();
    }
    
    public function createTimeTimesheetOrganization($initialTime, $finalTime, $nrorg, $daysTimesheetId, $timesheetId, $status, $type, $max_scheduling) {
        $serTimeTimesheet = new SerTimeTimesheet();
        
        if($daysTimesheetId !== NULL) {
            $serTimeTimesheet->setSerDayTimesheetValues($daysTimesheetId);
        }
        
        $initialTime ? $serTimeTimesheet->setInitialTime($initialTime) : "";
        $finalTime ? $serTimeTimesheet->setFinalTime($finalTime) : "";
        $nrorg ? $serTimeTimesheet->setNrorg($nrorg) : "";
        $timesheetId ? $serTimeTimesheet->setSerTimesheet($this->getEntityManager()->getReference(SerTimesheet::class, $timesheetId)) : "";
        $serTimeTimesheet->setStatus($status);
        $serTimeTimesheet->setType($type);
        
        if (empty($max_scheduling)) $max_scheduling = null;
        
        $serTimeTimesheet->setMaxScheduling($max_scheduling);
        
        $this->getEntityManager()->persist($serTimeTimesheet);
        $this->getEntityManager()->flush();
    }
    
    public function updateTimeTimesheetOrganization($timetimesheetId, $initialTime, $finalTime, $nrorg, $daysTimesheetId, $timesheetId, $status, $type, $max_scheduling) {
        $serTimeTimesheet = $this->getEntityManager()->getRepository(SerTimeTimesheet::class)->find($timetimesheetId);
        
        $initialTime ? $serTimeTimesheet->setInitialTime($initialTime) : "";
        $finalTime ? $serTimeTimesheet->setFinalTime($finalTime) : "";
        $nrorg ? $serTimeTimesheet->setNrorg($nrorg) : "";
        $daysTimesheetId !== NULL ? $serTimeTimesheet->setSerDayTimesheetValues($daysTimesheetId) : "";
        $timesheetId ? $serTimeTimesheet->setSerTimesheet($this->getEntityManager()->getReference(SerTimesheet::class, $timesheetId)) : "";
        $serTimeTimesheet->setStatus($status);
        $serTimeTimesheet->setType($type);
        $serTimeTimesheet->setMaxScheduling($max_scheduling);
        
        $this->getEntityManager()->merge($serTimeTimesheet);
        $this->getEntityManager()->flush();
    }
    
    public function getDayOfWeek($nrorg) {
        $serDayTimesheets = $this->getEntityManager()->getRepository(SerDayTimesheet::class)->findBy(['status' => 'A'], ['values' => 'ASC']);
        
        $days = [];
        foreach($serDayTimesheets as $serDayTimesheet) {
            array_push($days, ['value' => $serDayTimesheet->getValues(), 'name' => $serDayTimesheet->getName()]);
        }
        
        return $days;
    }
    
    public function getTimeTimesheetIds($nrorg, $timesheetId) {
        $serTimeTimesheet   = SerTimeTimesheet::class;
        
        $timeTimesheets = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT stt.id
            FROM $serTimeTimesheet stt 
            WHERE stt.status = 'A' AND stt.nrorg = '$nrorg' AND stt.serTimesheet = $timesheetId
            "
        )->getResult();
        
        $arrayIds = [];
        
        if(!empty($timeTimesheets)) {
            foreach($timeTimesheets as $timeTimesheet) {
                array_push($arrayIds, $timeTimesheet["id"]);
            }
        }
        return $arrayIds;
    }
    
    public function associateTimesheetWithDay($timeTimesheetId, $dayOfWeek) {
        $serTimeTimesheet = $this->getEntityManager()->getRepository(SerTimeTimesheet::class)->findOneBy(['id' => $timeTimesheetId, 'serDayTimesheet' => null]);
        
        if(!empty($serTimeTimesheet)) {
            $serDayTimesheet = $this->getEntityManager()->getRepository(SerDayTimesheet::class)->findOneBy(['values' => $dayOfWeek]);
            
            $serTimeTimesheet->setSerDayTimesheet($this->getEntityManager()->getReference(SerDayTimesheet::class, $serDayTimesheet->getId()));
            
            $this->getEntityManager()->merge($serTimeTimesheet);
            $this->getEntityManager()->flush();
        }else {
            return null;
        }
    }
    
    public function getTimeTimesheetByTimesheetId($nrorg, $timesheetId) {
        $serTimeTimesheet   = SerTimeTimesheet::class;
        
        $timeTimesheets = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT stt.id, stt.initialTime, stt.finalTime, stt.status
            FROM $serTimeTimesheet stt
            LEFT JOIN stt.serTimesheet st
            WHERE stt.status = 'A' AND stt.nrorg = '$nrorg' AND stt.serTimesheet = $timesheetId ORDER BY stt.initialTime
            "
        )->getResult();
        
        $arrayIds = [];
        if(!empty($timeTimesheets)) {
            foreach($timeTimesheets as $timeTimesheet) {
                array_push($arrayIds, [
                    'id' => $timeTimesheet['id'],
                    'initialTime' => $timeTimesheet['initialTime'],
                    'finalTime' => $timeTimesheet['finalTime'],
                    'status' => $timeTimesheet['status']
                ]);
            }
        }
        return $arrayIds;
    }
    
    public function getDayOfWeekByValue($dayTimesheetId) {
        $serDayTimesheet = $this->getEntityManager()->getRepository(SerDayTimesheet::class)->findOneBy(['values' => $dayTimesheetId]);
        
        return $serDayTimesheet->getId();
    }
    
    public function createTimeTimesheetAuto($initialTime, $finalTime, $nrorg, $dayTimesheet, $timesheetId, $status) {
        $serTimeTimesheet = new SerTimeTimesheet();
        
        $serTimeTimesheet->setSerDayTimesheet($this->getEntityManager()->getReference(SerDayTimesheet::class, $dayTimesheet));
        
        $initialTime ? $serTimeTimesheet->setInitialTime($initialTime) : "";
        $finalTime ? $serTimeTimesheet->setFinalTime($finalTime) : "";
        $nrorg ? $serTimeTimesheet->setNrorg($nrorg) : "";
        $timesheetId ? $serTimeTimesheet->setSerTimesheet($this->getEntityManager()->getReference(SerTimesheet::class, $timesheetId)) : "";
        $serTimeTimesheet->setStatus($status);
        
        $this->getEntityManager()->persist($serTimeTimesheet);
        return  $serTimeTimesheet;
    }
    
    public function myCommit() {
        $this->getEntityManager()->flush();
    }
    
    public function createTimesheetRel($timesheetId, $eventId, $nrorg, $status, $userId, $serviceId) {
        $serTimesheetRel = new SerTimesheetRel();
        
        if($eventId) {
            $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        }
        if($serviceId) {
            $serTimesheetRel->setSerService($this->getEntityManager()->getReference(SerService::class, $serviceId));
        }
        $serTimesheetRel->setSerTimesheet($this->getEntityManager()->getReference(SerTimesheet::class, $timesheetId));
        $serTimesheetRel->setNrorg($nrorg);
        $serTimesheetRel->setStatus($status);
        $serTimesheetRel->setCreatedBy($userId);
        
        $this->getEntityManager()->persist($serTimesheetRel);
        
        return $serTimesheetRel;
    }
    
    public function removeTimesheetRel($timesheetId, $eventId, $nrorg, $status, $userId, $serviceId) {
        $paramQuery = $eventId ? [ 'serTimesheet' => $timesheetId, 'evtEvent' => $eventId, 'nrorg' => $nrorg ] : [ 'serTimesheet' => $timesheetId, 'serService' => $serviceId, 'nrorg' => $nrorg ];
        
        $serTimesheetRel = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy($paramQuery);
        
        $serTimesheetRel->setStatus($status);
        $serTimesheetRel->setModifiedBy($userId);
        
        $this->getEntityManager()->persist($serTimesheetRel);
        
        return $serTimesheetRel;
    }
    
    public function checkIfThereIsSchedule($nrorg, $serviceId, $timeTimesheetId, $serviceSellerId, $initialDate) {
        $genSchedule = GenSchedule::class;
        
        
        $schedule = $this->getEntityManager()->createQuery(
            "
            SELECT gs.id FROM $genSchedule gs
            LEFT JOIN gs.serServiceSeller sss
            WHERE gs.nrorg = '$nrorg' 
            AND gs.serService = '$serviceId'
            AND gs.serTimeTimesheet = '$timeTimesheetId'
            AND sss.evtEventSeller = '$serviceSellerId'
            AND gs.initialDate = '$initialDate'
            AND gs.status = 'A'
            "
        )->getResult();
        
        return !empty($schedule) ? 1 : 0;
    }
    
    public function getTennisCourtsAndTimes($eventId, $date, $dayOfWeek, $serviceId) {
        $serServiceSellerRel = SerServiceSellerRel::class;
        $evtEventSeller = EvtEventSeller::class;
        $genUser = GenUser::class;
        $evtEvent = EvtEvent::class;
        
        $queryService = $serviceId != NULL ? " AND sssr.serService = '$serviceId' ": " ";
        $tennisCourts = $this->getEntityManager()->createQuery("
            SELECT DISTINCT sssr.id serviceSellerId, gs.id structureId, 
            CONCAT(gu.firstName, ' ', gu.lastName) fullName, ss.id serviceId FROM $serServiceSellerRel sssr
            LEFT JOIN $evtEventSeller ees WITH ees.id = sssr.evtEventSeller
            LEFT JOIN $genUser gu WITH gu.id = ees.genUser
            LEFT JOIN $evtEvent ee WITH ee.id = ees.evtEvent
            JOIN sssr.serService ss
            LEFT JOIN ee.genStructure gs
            WHERE ee.id = '$eventId' $queryService
        ")->getResult();
        
        $newTennisCourts = $this->group_by("serviceId", $tennisCourts);
        
        $data = [];
        
        if(!empty($newTennisCourts)) {
            
            foreach($newTennisCourts as $value) {
                foreach($value as $i => $tennisCourt) {
                    $tennisCourt['times'] = $this->getHourByProfessional($tennisCourt["serviceSellerId"], $date, $dayOfWeek);
                    array_push($data, $tennisCourt);
                }
            }
        }
        
        return $data;
    }
    
    /*Verifica se User ja marcou interesse nesse horario*/
    public function makeSureTheUserHasShownInterestAtTheTimetime($userId, $serviceId, $initialDate, $timeTimesheetId) {
        $genScheduleUserRel = GenScheduleUserRel::class;
        $genSchedule = GenSchedule::class;
        
        $result = $this->getEntityManager()->createQuery("
            SELECT gs FROM $genSchedule gs
            LEFT JOIN $genScheduleUserRel gsur WITH gs.id = gsur.genSchedule 
            WHERE gsur.genUser = $userId 
            AND gs.serService = $serviceId 
            AND gs.initialDate LIKE '$initialDate%' 
            AND gs.serTimeTimesheet = $timeTimesheetId
        ")->getResult();
        
        return !empty($result) ? true : false;
    }
    
    /* 1 Busca a duracao do servico e a duracao de cada horario do quadro de horarios.
     * 2 Verifica se $result != null && duracao do servico != null.
     * 3 Divido duracao do horario do QDH por duracao do servico, obter numero de horarios precisa ocupar.
     */
    public function checksServiceTimeTakesMoreThanOneHour($timeTimesheetId, $serviceId, $nrorg) {
        $result = $this->getEntityManager()->createQuery(
            "
            SELECT stt.finalTime as finalTimeQD, stt.initialTime as initialTimeQD, ss.time as timeSS, st.id as serTimesheet
            FROM " . SerTimesheet::class . " st
            LEFT JOIN " . SerTimeTimesheet::class . " stt WITH st.id = stt.serTimesheet
            LEFT JOIN " . SerService::class . " ss WITH ss.id = $serviceId WHERE stt.id = $timeTimesheetId AND ss.nrorg = $nrorg AND stt.nrorg = $nrorg AND stt.status = 'A'
            "
        )->getResult()[0];
        /*duracao de cada horario do quadro*/
        $diff = $this->hoursToMinutes($this->convertStrToTime($result['initialTimeQD'], $result['finalTimeQD']));
        /*duracao do servico*/
        $timeSS = intval($result['timeSS']);
        /*verifica se duracao de servico e maior que duracao de horarios do quadro*/
        if($result != null && $diff < $timeSS) {
            $numberInserts['numberInserts'] = ceil($timeSS / $diff);
            $numberInserts['initialTime'] = $result['initialTimeQD'];
            $initialTime = $result1 = \DateTime::createFromFormat('H:i', $result['initialTimeQD']);
            $finalTime = $initialTime->add(new \DateInterval('PT'. $numberInserts['numberInserts'] .'H'));
            $numberInserts['finalTime'] = $finalTime->format('H:i');
            $numberInserts['serTimesheet'] = $result['serTimesheet'];
        }else {
            $numberInserts['numberInserts'] = 1;
        }
        return $numberInserts;
    }
    
    public function convertStrToTime($str1, $str2) {
        $result1 = \DateTime::createFromFormat('H:i', $str1)->format('Y-m-d H:i:s');
        $result2 = \DateTime::createFromFormat('H:i', $str2)->format('Y-m-d H:i:s');
        
        $diff = $this->dateDiff($result1, $result2,'%H:%I');
        return $diff;
    }
    
    // Transform hours like "1:45" into the total number of minutes, "105". 
    public function hoursToMinutes($hours) { 
        $minutes = 0; 
        if (strpos($hours, ':') !== false) { 
            // Split hours and minutes. 
            list($hours, $minutes) = explode(':', $hours); 
        } 
        return $hours * 60 + $minutes; 
    }
    
    /**
     * @param $dateStart - Data inicial
     * @param $dateEnd - Data final
     * @param $format  - Formato esperado de saida
     */
    function dateDiff( $dateStart, $dateEnd, $format = '%a' ) {
        $d1     =   new \DateTime( $dateStart );
        $d2     =   new \DateTime( $dateEnd );
        
        //Calcula a diferença entre as datas
        $diff   =   $d1->diff($d2, true);
        //Formata no padrão esperado e retorna
        return $diff->format($format);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public function createConfigSchedule($providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name) {
        $serTimesheet = new SerTimesheet();
        $serTimesheetRel = new SerTimesheetRel();
        
        $name ? $serTimesheet->setName($name) : "";
        $time ? $serTimesheet->setTime($time) : "";
        $nrorg ? $serTimesheet->setNrorg($nrorg) : "";
        $status ? $serTimesheet->setStatus($status) : "";
        $finalTime ? $serTimesheet->setFinalTime($finalTime) : "";
        $initialTime ? $serTimesheet->setInitialTime($initialTime) : "";
        
        $this->getEntityManager()->persist($serTimesheet);
        
        if($providerId != NULL && !empty($serTimesheet)){
            $serTimesheetRel->setSerTimesheet($serTimesheet);
            $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $providerId));
            $nrorg ? $serTimesheetRel->setNrorg($nrorg) : "";
            $status ? $serTimesheetRel->setStatus($status) : "";
            
            $this->getEntityManager()->persist($serTimesheetRel);
        }
        
        $this->getEntityManager()->flush();
        
        return $serTimesheet;
    }
    
    public function getConfigSchedule($nrorg) {
        $serTimesheet       = SerTimesheet::class;
        $serTimeTimesheet   = SerTimeTimesheet::class;
        $serDayTimesheet    = SerDayTimesheet::class;
        
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, st.name, st.time
            FROM $serTimesheet st
            WHERE st.nrorg = '$nrorg' AND st.status = 'A' 
            ORDER BY st.id
            "
        )->getResult();
        
        return $times;
    }
    
    public function updateConfigSchedule($timesheetId, $providerId, $nrorg, $initialTime, $finalTime, $status, $time, $name) {
        $serTimesheet = $this->getEntityManager()->getRepository(SerTimesheet::class)->find($timesheetId);
        
        if(!empty($serTimesheet)) {
        
            $name ? $serTimesheet->setName($name) : "";
            $time ? $serTimesheet->setTime($time) : "";
            $nrorg ? $serTimesheet->setNrorg($nrorg) : "";
            $status ? $serTimesheet->setStatus($status) : "";
            $finalTime ? $serTimesheet->setFinalTime($finalTime) : "";
            $initialTime ? $serTimesheet->setInitialTime($initialTime) : "";
            
            $this->getEntityManager()->persist($serTimesheet);
        
            $serTimesheetRel = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy(['serTimesheet' => $serTimesheet->getId()]);
            if(!empty($serTimesheetRel)) {
                $serTimesheetRel->setSerTimesheet($serTimesheet);
                $providerId ? $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $providerId)) : NULL;
                $nrorg ? $serTimesheetRel->setNrorg($nrorg) : "";
                $status ? $serTimesheetRel->setStatus($status) : "";
                
                $this->getEntityManager()->persist($serTimesheetRel);
            }
            
            $this->getEntityManager()->flush();
            
        }else {
            $serTimesheet = NULL;
        }
        return $serTimesheet;
    }
    
    public function removeConfigSchedule($timesheetId, $eventId, $nrorg, $status, $userId, $serviceId) {
        $paramQuery = $eventId ? [ 'serTimesheet' => $timesheetId, 'evtEvent' => $eventId, 'nrorg' => $nrorg ] : [ 'serTimesheet' => $timesheetId, 'serService' => $serviceId, 'nrorg' => $nrorg ];
        
        $serTimesheetRel = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy($paramQuery);
        
        $serTimesheetRel->setStatus($status);
        $serTimesheetRel->setModifiedBy($userId);
        
        $this->getEntityManager()->persist($serTimesheetRel);
        
        return $serTimesheetRel;
    }

     /*
      ----- SERVICE MODALITIES TIMESHEET-----
    */
    public function getAllTimesheetByProviderForTypeModalities($row) {
        $serTimesheet       = SerTimesheet::class;
        $serTimesheetRel    = SerTimesheetRel::class;
        $nrorg = $row['NRORG'];
        $typeId = $row['TYPE_ID'];
        $times = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, st.name, st.time
            FROM $serTimesheet st
            LEFT JOIN $serTimesheetRel str WITH (str.status = 'A')
            WHERE st.nrorg = '$nrorg' AND st.status = 'A' 
            AND str.ordConfigStoreDeliversId != $typeId
            ORDER BY st.id
            "
        )->getResult();
        return $times;
    }
 
    public function getTimesheetByProviderForTypeModalities($row) {
        $serTimesheet       = SerTimesheet::class;
        $serTimesheetRel    = SerTimesheetRel::class;
        $nrorg = $row['NRORG'];
        $typeId = $row['TYPE_ID'];
        $times = $this->getEntityManager()->createQuery(
            "
            select st.id, st.initialTime, st.finalTime, st.status, st.name, st.time, str.ordConfigStoreDeliversId
            from $serTimesheetRel str
            left join $serTimesheet st with st.id = str.serTimesheet and st.nrorg = $nrorg
            where str.ordConfigStoreDeliversId = $typeId AND str.nrorg = $nrorg
            "
        )->getResult();
        //   SELECT DISTINCT st.id, st.initialTime, st.finalTime, st.status, st.name, st.time, str.ordConfigStoreDeliversId
        //     FROM $serTimesheet st
        //     LEFT JOIN $serTimesheetRel str WITH (str.status = 'A' AND str.ordConfigStoreDeliversId = $typeId)
        //     WHERE st.nrorg = '$nrorg' AND st.status = 'A' 
        //     and str.nrorg = $nrorg
        //     ORDER BY st.id
        return $times;
    }
 
    public function createTimeTimesheetForTypeModalities ($rows) {
        try {
            
            $ordConfigStoreDeliversId = $rows['TYPE_DELIVERS_ID'];
            $eventId = $rows['STORE_ID'];
            $nrorg = $rows['NRORG'];
            $row = $rows['ROM'];
            $serTimesheet = $row["timesheet"];
            $status = 'A';
            
            $serTimesheetRel   = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy(['nrorg' => $nrorg, 'ordConfigStoreDeliversId' => $ordConfigStoreDeliversId, 'serTimesheet' => $serTimesheet]);
            
            if ($serTimesheetRel == null) {
                $serTimesheetRel = new SerTimesheetRel;
            }
        
            if($serTimesheet) 
                $serTimesheetRel->setSerTimesheet($this->getEntityManager()->getReference(SerTimesheet::class, $serTimesheet));

            $serTimesheetRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));

            $serTimesheetRel->setOrdConfigStoreDeliversId($ordConfigStoreDeliversId);

            $serTimesheetRel->setStatus($status);
            $serTimesheetRel->setNrorg($nrorg);
            $serTimesheetRel->setCreatedBy($nrorg);
            $serTimesheetRel->setModifiedBy($nrorg);
            $serTimesheetRel->setCreatedAt();
            $serTimesheetRel->setModifiedAt();

            $this->getEntityManager()->persist($serTimesheetRel);
            $this->getEntityManager()->flush();
            
            return ['success', 'all clear'];
        } catch (\Exception $e) {
            return ['error', $e->getMessage()];
        }
    }
   
    public function removeTimeTimesheetOrganizationForTypeModalities ($rows) {
        try {
            $ordConfigStoreDeliversId = $rows['TYPE_DELIVERS_ID'];
            $eventId = $rows['STORE_ID'];
            $nrorg = $rows['NRORG'];
            $row = $rows['ROM'];
            $serTimesheet = $row["id"];


            $serTimesheetRel   = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findOneBy(['nrorg' => $nrorg, 'ordConfigStoreDeliversId' => $ordConfigStoreDeliversId, 'serTimesheet' => $serTimesheet]);
            
            if ($serTimesheetRel == null) return ['error', 'SerTimesheetRel not found'];
        
            $this->getEntityManager()->remove($serTimesheetRel);
            $this->getEntityManager()->flush();

            return ['success', 'all clear'];
        } catch (\Exception $e) {
            return ['error', $e->getMessage()];
        }
    }
   
}