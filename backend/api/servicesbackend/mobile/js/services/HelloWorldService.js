/**
 *
 * @param {RequestFactory}     requestFactory
 * @param {RequestEngine}      requestEngine
 *
 * @constructor
 */
function HelloWorldService(requestFactory, requestEngine) {

    this.greetings = function(name) {
        return requestEngine.doRequest(requestFactory.factory({
            'requestType': 'Row',
            'serviceName': '/greetings',
            'row': {
                'name': name
            }
        })).then(function(response) {
            var message = response.dataset.message.pop();
            if (!message) {
                throw 'Request didn\'t return any message';
            }
            return message.text;
        }).catch(function(response) {
            throw response.data.error;
        });
    };

}

Configuration(function(ContextRegister){
    ContextRegister.register('HelloWorldService', HelloWorldService);
});