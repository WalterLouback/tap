/**
 *
 * @param {HelloWorldService}   HelloWorldService
 * @param {ScreenService}       ScreenService
 *
 * @constructor
 */
function HelloWorldController(HelloWorldService, ScreenService) {

    this.clickSayHiButton = function(widget) {
        var name = widget.currentRow.name;

        HelloWorldService.greetings(name).then(function(message) {
            ScreenService.showMessage(message);
        }).catch(function(error) {
            ScreenService.errorNotification(error);
        });
    };

}

Configuration(function(ContextRegister){
    ContextRegister.register('HelloWorldController', HelloWorldController);
});