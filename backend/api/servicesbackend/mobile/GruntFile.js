module.exports = function(grunt) {

    var srcFiles = ['js/services/*.js', 'js/controllers/*.js'];
    var helpersFiles = ['tests/unit/helpers/**/*.js'];
    var specsFiles = ['tests/unit/specs/**/*.spec.js'];

    grunt.initConfig({
        concat: {
            zh: {
                options: {
                    process: function(src, filepath) {
                        return '\n' + '// FILE: ' + filepath + '\n' + src;
                    }
                },
                src: srcFiles,
                dest: 'dist/main.js'
            }
        },
        jshint: {
            all: ['GruntFile.js', srcFiles, specsFiles]
        },
        jasmine: {
            pivotal: {
                src: srcFiles,
                options: {
                    helpers: helpersFiles,
                    specs: specsFiles,
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        coverage: 'coverage/coverage.json',
                        report: 'coverage'
                    }
                }
            }
        },
        uglify: {
            options: {
                mangle: false,
                compress: true,
                report: 'min',
                banner: '/*! <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/main.min.js': srcFiles.concat()
                }
            }
        },
        buildEnvironment: {
            development: {
                configFile: '../config/development.json'
            },
            deploy: {
                configFile: '../config/deploy.json'
            },
            srcFolders: ['../mobile']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-zh-environment-builder');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify']);
};