describe('HelloWorldController', function() {

    var helloWorldController;
    var helloWorldService;
    var screenService;

    beforeEach(function() {
        helloWorldService = {
            greetings: function() {}
        };

        spyOn(helloWorldService, 'greetings')

        helloWorldService = jasmine.createSpyObj();
        screenService = jasmine.createSpyObj('ScreenService', ['showMessage', 'errorNotification']);

        helloWorldController = new HelloWorldController(helloWorldService, screenService);
    });

    it('clickSayHiButton', function() {
        var widget = {currentRow: {name: 'Jarolim'}};

        helloWorldController.clickSayHiButton(widget);

        expect(helloWorldService.greetings).toHaveBeenCalledWith('Jarolim');
    });

    // it('clickSayHiButton error', function() {

    // });

});