<?php
namespace tests\Zeedhi\HelloWorld\Controller;

use Zeedhi\HelloWorld\Service\HelloWorld as HelloWorldService;
use Zeedhi\HelloWorld\Controller\HelloWorld as HelloWorldController;
use Zeedhi\HelloWorld\Controller\Exception as HelloWorldException;

use Zeedhi\Framework\DTO\Request;
use Zeedhi\Framework\DTO\Response;

class HelloWorldTest extends \PHPUnit_Framework_TestCase {

    protected $helloWorldController;
    protected $helloWorldService;

    public function setUp() {
        $this->helloWorldService = $this->getMockBuilder(HelloWorldService::class)
                                        ->setMethods(array('greetings'))
                                        ->disabLeOriginalConstructor()
                                        ->getMock();

        $this->helloWorldController = new HelloWorldController($this->helloWorldService);
    }

    public function testGreetings() {
        $name = 'name';
        $row = array(
            'name' => $name
        );
        $request = new Request\Row($row, 'POST', '/route', 'USER_ID');
        $response = new Response();

        $this->helloWorldService->expects($this->once())
                                ->method('greetings')
                                ->with($name)
                                ->will($this->returnValue('Hey, '.$name));

        $this->helloWorldController->greetings($request, $response);

        $dataSets = $response->getDataSets();
        $this->assertCount(1, $dataSets);

        $dataSet = $dataSets[0];
        $this->assertEquals('message', $dataSet->getDataSourceName());

        $rows = $dataSet->getRows();
        $this->assertCount(1, $rows);

        $row = $rows[0];
        $this->assertCount(1, $row);
        $this->assertArrayHasKey('text', $row);
        $this->assertEquals('Hey, '.$name, $row['text']);
    }

    public function testGreetingsError() {
        $name = 'name';
        $row = array(
            'not_name' => $name
        );
        $request = new Request\Row($row, 'POST', '/route', 'USER_ID');
        $response = new Response();

        $this->helloWorldService->expects($this->never())
                                ->method('greetings');

        $this->helloWorldController->greetings($request, $response);

        $dataSets = $response->getDataSets();
        $this->assertCount(0, $dataSets);

        $error = $response->getError();
        $this->assertNotNull($error);

        $this->assertEquals('Missing name property', $error->getMessage());
        $this->assertEquals(HelloWorldException::MISSING_NAME, $error->getErrorCode());
    }

}