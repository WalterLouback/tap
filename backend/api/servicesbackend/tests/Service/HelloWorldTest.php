<?php
namespace tests\Zeedhi\HelloWorld\Service;

use Zeedhi\HelloWorld\Service\HelloWorld as HelloWorldService;

class HelloWorldTest extends \PHPUnit_Framework_TestCase {

    protected $helloWorldService;

    public function setUp() {
        $this->helloWorldService = new HelloWorldService();
    }

    public function testGreetings() {
        $name = 'name';
        $sentences = array(
            'Hey, '.$name.'!',
            'Olá, '.$name.'!',
            'Hola, '.$name.'!',
            'Bonjour, '.$name.'!',
            'Guten tag, '.$name.'!',
            'Ciao, '.$name.'!',
            'Namaste, '.$name.'!',
            'Salaam, '.$name.'!',
            'Zdras-tvuy-te, '.$name.'!'
        );

        $text = $this->helloWorldService->greetings($name);

        $this->assertContains($text, $sentences);
    }

}