<?php
namespace Zeedhi\ApiOrders\Listeners;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiOrders\Service\Environment;
use Zeedhi\ApiOrders\Service\ServiceImpl;
use Zeedhi\ApiOrders\Service\ServiceProviderImpl;

use Zeedhi\ApiOrders\Controller\Exception;

use Zeedhi\Framework\DTO\Request;

use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\Cache\Type\FileImpl;
use Zeedhi\Framework\Cache\Type\SessionImpl;
use Zeedhi\Framework\Security\OAuth\OAuthImpl;
use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\Session\Storage\NativeSession;
use Zeedhi\Framework\Session\Attribute\SimpleAttribute;

class TokenAuthentication extends \Zeedhi\Framework\Events\PreDispatch\Listener {
    
    
    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;
    
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment   = $environment;
        $this->ignore        = ['/login', '/requestToken', '/getOrganizationData', '/getLastNews', 
                                '/getTagsFromOrganization', '/getContactMethods', '/verifyMobilePhone', 
                                'validateFacebookToken', '/createPassword', '/facebookLogin', 
                                '/getStructures', '/sendConfirmationEmail', '/validateTemporaryPassword',
                                '/getStores', '/getStoreData', '/getCategories', '/getEventsByOrg', '/getBalance'];
        
        $this->setupOAuth();
    }
    
    private function setupOAuth() {
		$storage = new FileImpl(__DIR__ . "/../cache", ".txt");
        $serviceProvider = new ServiceProviderImpl();
        
        $lifeTime = 10 * 60; //life time in seconds
		$this->oauth = new OAuthImpl(new ServiceProviderImpl(), $storage, time() + $lifeTime);
        
		$this->clientSecret = "SECRET";
    }
    
    public function preDispatch(Request $request) {
        $row   = $request->getRow();
        $route = $request->getRoutePath();
        
        if (!in_array($route, $this->ignore)) {
            if (empty($row['TOKEN']) || empty($row['USER_ID'])) {
                throw Exception::invalidParameters();
            }
            
            $token    = $row['TOKEN'];
            $userId   = $row['USER_ID'];
            
            $clientId = "USER_ID_$userId";
            //$this->checkAccessToken($token, $clientId);
        }
    }
    
    public function checkAccessToken($token, $clientId) {
		$service = $this->oauth->checkAccess($token, $this->clientSecret);
		if ($clientId != $service->getClientId()) {
		    throw Exception::unauthorizedAction();
		}
    }
    
    
}