<?php
namespace Zeedhi\ApiOrders\Service;

use Zeedhi\ApiOrders\Helpers\General;

use Zeedhi\ApiOrders\Model\Entities\EvtEventDeliver;
use Zeedhi\ApiOrders\Model\Entities\GenShift;
use Zeedhi\ApiOrders\Model\Entities\GenAddress;
use Zeedhi\ApiOrders\Model\Entities\GenUser;
use Zeedhi\ApiOrders\Model\Entities\GenUserType;
use Zeedhi\ApiOrders\Model\Entities\GenStatus;
use Zeedhi\ApiOrders\Model\Entities\GenStructure;
use Zeedhi\ApiOrders\Model\Entities\GenStatusUserType;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtSellerType;
use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdOption;
use Zeedhi\ApiOrders\Model\Entities\OrdOrder;
use Zeedhi\ApiOrders\Model\Entities\OrdConfigStore;
use Zeedhi\ApiOrders\Model\Entities\OrdStorePayMethod;
use Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod;
use Zeedhi\ApiOrders\Model\Entities\PayGateway;
use Zeedhi\ApiOrders\Model\Entities\PayGatewayRel;
use Zeedhi\ApiOrders\Model\Entities\OrdWorkflow;
use Zeedhi\ApiOrders\Helpers\ImageUploader;

use Zeedhi\ApiOrders\Model\Entities\OrdConfigStoreDelivers;
use Zeedhi\ApiServices\Model\Entities\SerTimesheetRel;

use Doctrine\ORM\EntityManager;

class Store extends UserOperation {
    
    const TYPE_STORE = 'S';
    
    public function __construct(EntityManager $entityManager, Environment $environment, ImageUploader $imageUploader) {
        parent::__construct($entityManager, $environment);
        $this->imageUploader = $imageUploader;
    }
    
    /* Devolve as lojas de uma organização, podendo receber outros filtros */
    function getStores($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll=NULL, $initial=NULL, $max=NULL) {
        $store          = 'Zeedhi\ApiOrders\Model\Entities\EvtEvent';
        $eventMenu      = 'Zeedhi\ApiOrders\Model\Entities\EvtEventMenu';
        
        $structureQuery = $structureId !== NULL ? "AND s.genStructure = $structureId" : "";
        $parentQuery    = $parentId !== NULL ? "AND s.parentEvent = $parentId" : "";
        $nameQuery      = $storeName !== NULL ? "AND s.name = '$storeName'" : "";
        $nrorgQuery     = $nrorg !== NULL ? "AND s.nrorg = $nrorg" : "";
        $ownerQuery     = $ownerUser !== NULL ? "AND s.ownerUserId = $ownerUser" : "";
        $menuJoinType   = $showAll !== NULL ? "LEFT" : "INNER";
        
        // $statusQuery    = $showAll !== NULL ? "" : "AND s.status <> 'I'";
        $statusQuery = "AND s.status <> 'I'";
        
        $stores = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $store s
            $menuJoinType JOIN $eventMenu em WITH em.evtEvent = s.id
            WHERE s.type = 'S'
            $structureQuery
            $parentQuery
            $nameQuery
            $nrorgQuery
            $ownerQuery
            $statusQuery
            ORDER BY s.id
            "
        );
        
        if ($initial !== NULL) $stores->setFirstResult($initial);
        if ($max !== NULL) $stores->setMaxResults($max);
        
        $stores = new \Doctrine\ORM\Tools\Pagination\Paginator($stores);
        /* Informar locais de entrega da loja e se ela está aberta */
        foreach ($stores as $store) {
            $store->setDeliversToTable($this->getEntityManager());
            $store->setDeliversToBalcony($this->getEntityManager());
            $store->setDeliversToAddress($this->getEntityManager());
            $store->isOpened($this->getEntityManager());
            $store->setWorkshifts($this->getEntityManager());
            $store->setAddress($this->getEntityManager());
            $store->setMinValue($this->getEntityManager());
            $store->setTimerTaa($this->getEntityManager());

        }
        return $stores;
    }
    
    /* Busca as lojas de acordo com o filtro passado*/
    public function getStoreWithFilter($nrorg, $ownerUser, $structureId, $parentId, $storeName, $storeStatus, $itemsPerPage, $page) {
        $store          = 'Zeedhi\ApiOrders\Model\Entities\EvtEvent';
        
        $structureQuery = isset($structureId) ? "AND s.genStructure = $structureId" : "";
        $parentQuery    = isset($parentId)  ? "AND s.parentEvent = $parentId" : "";
        $nameQuery      = isset($storeName) ? "AND s.name = '$storeName'" : "";
        $nrorgQuery     = isset($nrorg) ? "AND s.nrorg = $nrorg" : "";
        $ownerQuery     = isset($ownerUser) ? "AND s.ownerUserId = $ownerUser" : "";
        $statusQuery    = (!isset($storeStatus) && $storeStatus != 'P' && empty($storeStatus)) ? "AND s.status <> 'I' AND s.status <> 'P'" :  "AND s.status = '$storeStatus' AND s.status <> 'I'";

        $stores = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $store s
            WHERE s.type = 'S'
            $structureQuery
            $parentQuery
            $nameQuery
            $nrorgQuery
            $ownerQuery
            $statusQuery
            ORDER BY s.id
            "
        );
        $stores->setFirstResult($itemsPerPage * ($page - 1));
        if (isset($itemsPerPage)) $stores->setMaxResults($itemsPerPage);

        $stores = new \Doctrine\ORM\Tools\Pagination\Paginator($stores);
        
        /* Informar locais de entrega da loja e se ela está aberta */
        foreach ($stores as $store) {
            $store->setDeliversToTable($this->getEntityManager());
            $store->setDeliversToBalcony($this->getEntityManager());
            $store->setDeliversToAddress($this->getEntityManager());
            $store->isOpened($this->getEntityManager());
            $store->setWorkshifts($this->getEntityManager());
            $store->setAddress($this->getEntityManager());
            $store->setMinValue($this->getEntityManager());
        }

        return $stores;
    }
    
    /* Devolve as lojas a que um vendedor está associado */
    function getStoresAssociatedToUser($userId) {
        $stores = [];
        $eventSellers = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findBy(['genUser' => $userId]);
        foreach ($eventSellers as $eventSeller) {
            
            $store = $eventSeller->getEvtEvent();
            if ($store != NULL) {
                $store->build($this->getEntityManager(), $userId, FALSE);
                if ($store != NULL && $store->getStatus() != 'I') {
                    array_push($stores, $eventSeller->getEvtEvent());
                }
            }
        }
        
        return $stores;
    }
    
    /* Devolve o fluxo de pedidos de uma loja */
    function getWorkflowData($storeId) {
        $config   = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        if ($config != NULL && $config->getWorkflow() != NULL) {
            $workflow = $config->getWorkflow();
            return $workflow;
        } else {
            return $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $config->getEvent()->getNrorg()]);
        }
    }
    
    /* Devolve os produtos da loja, divididos por isCurrentWorkshift */
    function getStoreData($store, $allMenus=true, $horaAtual) {
        $userId=NULL;
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $store->getId()]);
        if ($store != NULL) $store->build($this->getEntityManager(), $userId, $allMenus, $horaAtual);
       
        return $store;
        
    }
    
    function getStoreDataOne($store) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $store->getId()]);
        if ($store != NULL) $store->specyficBiuld($this->getEntityManager());
        
        return $store;
    }
    
    /* Devolve o atributo status de uma loja */
    function getStoreStatus($storeId) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        return $store->getStatus();
    }
    
    function isStoreOpened($storeId) {
        return $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId)->isOpened($this->getEntityManager());
    }
    
    function addStablishmentId($storeId,$establishmentId) {
        $store=$this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $store->setEstablishmentId($establishmentId);
        $this->getEntityManager()->flush();

        return $store->getEstablishmentId();
    }
    
    function addAcceptVoucher($storeId,$acceptVoucher) {
        $store=$this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $store->setAcceptVoucher($acceptVoucher);
        $this->getEntityManager()->flush();

        return $store->getAcceptVoucher();
    }
        
     function addLockId($storeId,$lockId) {
        $store=$this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $store->setLockId($lockId);
        $this->getEntityManager()->flush();

        return $store->getLockId();
    }
    
    /* Altera o campo status de uma loja */
    function updateStoreStatus($storeId, $status) {
        /* Obter loja correspondente ao ID enviado */
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        /* Alterar Status da Loja */
        $store->setStatus($status);
        
        /* Atualizar no Banco */
        $this->getEntityManager()->flush();
        
        return $store;
    }
    
    /* Cria uma loja com os parâmetros passados */
    // function createStore($name, $about, $structureId, $deliversToTable, $deliversToBalcony, $deliversToAddress, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $parentEventId, $minValue, $externalID=null) {
    function createStore($name, $about, $structureId, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $parentEventId, $minValue, $externalID=null, $timerTaa) {
        $structure     = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
        $ownerUser     = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $ownerEmail]);
        $orgWorkflow   = $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $structure->getNrorg()]);
        
        if ($ownerUser == NULL) throw new \Exception('Owner user not found', 2);
        
        $store = new EvtEvent();
        $store->setName($name);
        $store->setAbout($about);
        $store->setStructure($structure);
        $store->setNrorg($structure->getNrorg());
        if ($imageCover) $store->setImageCover($this->imageUploader->upload($imageCover, $structure->getNrorg()));
        if ($imageLogo) $store->setImageLogo($this->imageUploader->upload($imageLogo, $structure->getNrorg()));
        $store->setStatus($status);
        $store->setType(self::TYPE_STORE);
        $store->setOwnerUser($ownerUser);
        if (!empty($externalID)) $store->setExternalId($externalID);
        
        if ($parentEventId) {
            $parentEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->find($parentEventId);
            if ($parentEvent) $store->setParentEvent($parentEvent);
            else throw new \Exception('Parent event not found');
        }
        
        $configStore = new OrdConfigStore();
        $configStore->setEvent($store);
        // $configStore->setDeliversToTable($deliversToTable);
        // $configStore->setDeliversToBalcony($deliversToBalcony);
        // $configStore->setDeliversToAddress($deliversToAddress);
        $configStore->setNrorg($structure->getNrorg());
        $configStore->setWorkflow($orgWorkflow);/*Cristiano*/
        $configStore->setMinValue($minValue);
        $configStore->setTimerTaa($timerTaa);
        
        $this->getEntityManager()->persist($store);
        $this->getEntityManager()->persist($configStore);
        $this->getEntityManager()->flush();
        
        $sellerType = $this->createSellerTypeAdmin($store->getId(), 'Owner', [], $store->getNrorg());
        $this->linkUserToStore($store->getId(), $ownerEmail, $sellerType->getId(), $adminId);
        return $store;
    }

    function createSellerTypeAdmin($storeId, $sellerTypeName, $visibleStatus, $nrorg) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        $sellerType = new EvtSellerType();
        $sellerType->setEvtEvent($store);
        $sellerType->setName($sellerTypeName);
        $sellerType->setNrorg($store->getNrorg());
        
        $statusGenStatus = $this->getEntityManager()->getRepository(GenStatus::class)->findBy(['nrorg' => $nrorg]);
        $visibleStatus =  GenStatus::manyToArray($statusGenStatus);
        
        foreach ($visibleStatus as $status) {
            $status = $this->getEntityManager()->getRepository(GenStatus::class)->find($status['ID']);
            
            $statusUserType = new GenStatusUserType();
            $statusUserType->setGenStatus($status);
            $statusUserType->setEvtSellerType($sellerType);
            
            $this->getEntityManager()->persist($statusUserType);
        }
        
        $this->getEntityManager()->persist($sellerType);
        $this->getEntityManager()->flush();
        
        return $sellerType;
    }
    
    function updateStoreWorkshifts($storeId, $workshifts) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $this->cleanStoreWorkshifts($store);
        
        foreach ($workshifts as $workshift) {
            $w = new GenShift();
            $w->setInitialTime(new \DateTime($workshift['INITIAL_TIME']));
            $w->setFinalTime(new \DateTime($workshift['FINAL_TIME']));
            $w->setDay($workshift['DAY']);
            $w->setEvtEvent($store);
            
            $this->getEntityManager()->persist($w);
        }
        
        $this->getEntityManager()->flush();
    }
    
    function cleanStoreWorkshifts($store) {
        $workshifts = $this->getEntityManager()->getRepository(GenShift::class)->findBy(['evtEvent' => $store]);
        
        foreach ($workshifts as $shift) {
            $this->getEntityManager()->remove($shift);
        }
        
        $this->getEntityManager()->flush();
    }
    
    function getOrganizationWorkflow($nrorg) {
        $workflow = $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg]);
        if (!$workflow) {
            $workflow = new OrdWorkflow();
            $workflow->setNrorg($nrorg);
            $this->getEntityManager()->persist($workflow);
            $this->getEntityManager()->flush();
        }
        
        return $workflow;
    }
    
    function updateStatus($statusId, $color, $labelName, $buttonName, $previousId) {
        $status = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        
        $status->setColor($color);
        $status->setLabelName($labelName);
        $status->setButtonName($buttonName);
        
        $nrorg = $status->getNrorg();
        
        if ($previousId) {
            /* Se este status vem após algum outro */
            
            /* Caso este seja o status inicial, setar o inicial como o próximo */
            $workflow = $this->getOrganizationWorkflow($nrorg);
            if ($workflow->getInitialStatus()->getId() == $status->getId()) $workflow->setInitialStatus($status->getNext());
            
            /* Caso haja alguém antes deste status, fazer apontar para o próximo  */
            $oldPrevious = $this->getPreviousStatus($status);
            if ($oldPrevious) $oldPrevious->setNext($status->getNext());
            
            /* Fazer previous apontar para este, e este apontar para o próximo do previous */
            $previous    = $this->getEntityManager()->getRepository(GenStatus::class)->find($previousId);
            $oldNext     = $previous->getNext();
            $status->setNext($oldNext);
            $previous->setNext($status);
        } else {
            /* Se este for ser o primeiro status */
            
            /* Caso haja alguém antes deste status, fazer apontar para o próximo */
            $currPrevious = $this->getPreviousStatus($status);
            if ($currPrevious) $currPrevious->setNext($status->getNext());
            
            /* Fazer este apontar para o status inicial, e o inicial passa a ser este */
            $workflow   = $this->getOrganizationWorkflow($nrorg);
            $oldInitial = $workflow->getInitialStatus();
            
            if ($oldInitial->getId() != $status->getId()) {
                $status->setNext($oldInitial);
            }
            
            $workflow->setInitialStatus($status);
        }
        
        $this->getEntityManager()->flush();
    }
    
    private function getPreviousStatus($status) {
        return $this->getEntityManager()->getRepository(GenStatus::class)->findOneBy(['next' => $status]);
    }
    
    function createStatus($nrorg, $color, $labelName, $buttonName, $previousId) {
        $status = new GenStatus();
        
        $status->setNrorg($nrorg);
        $status->setColor($color);
        $status->setLabelName($labelName);
        $status->setButtonName($buttonName);
        
        $nrorg = $status->getNrorg();
        
        if ($previousId) {
            /* Se este status vem após algum outro */
            
            /* Fazer previous apontar para este, e este apontar para o próximo do previous */
            $previous    = $this->getEntityManager()->getRepository(GenStatus::class)->find($previousId);
            $oldNext     = $previous->getNext();
            $status->setNext($oldNext);
            $previous->setNext($status);
        } else {
            /* Se este for ser o primeiro status */
            
            /* Fazer este apontar para o status inicial, e o inicial passa a ser este */
            $workflow   = $this->getOrganizationWorkflow($nrorg);
            $oldInitial = $workflow->getInitialStatus();
            
            $status->setNext($oldInitial);
            $workflow->setInitialStatus($status);
        }
        
        $this->getEntityManager()->persist($status);
        $this->getEntityManager()->flush();
    }
    
    function removeStatus($statusId, $nrorg) {
        $status   = $this->getEntityManager()->getRepository(GenStatus::class)->findOneBy(['id' => $statusId , 'nrorg' => $nrorg]);
        
        /* Fazer o status anterior passar a referenciar o próximo */
        $previous = $this->getPreviousStatus($status);
        if ($previous) $previous->setNext($status->getNext());
        
        /* Caso este seja o status inicial, fazer o inicial ser o próximo */
        $workflow = $this->getOrganizationWorkflow($status->getNrorg());
        if ($workflow->getInitialStatus() && $workflow->getInitialStatus()->getId() == $statusId) {
            $workflow->setInitialStatus($status->getNext());
        }
        
        $this->getEntityManager()->flush();
    }
    
    function setOrganizationPaymentMoment($nrorg, $statusId) {
        $workflow = $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg]);
        $status   = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        
        $workflow->setPaymentMoment($status);
        
        $this->getEntityManager()->flush();
    }
    
    /* Edita uma loja com os parâmetros passados */
    function updateStore($storeId, $about, $imageCover, $imageLogo, $name, $latitude=null, $longitude=null, $externalID=null, $minValue, $timerTaa=null) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        if ($store !== NULL) {
            
            $store->setAbout($about);
            $store->setName($name);
            $store->setType(self::TYPE_STORE);
            if ($imageCover) $store->setImageCover($this->imageUploader->upload($imageCover, $store->getNrorg()));
            if ($imageLogo) $store->setImageLogo($this->imageUploader->upload($imageLogo, $store->getNrorg()));
            if (!empty($externalID)) $store->setExternalId($externalID);

            $this->getEntityManager()->persist($store);
            $this->getEntityManager()->flush();
            
            $ordConfigStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
            $ordConfigStore->setMinValue($minValue);
            $ordConfigStore->setTimerTaa($timerTaa);
            $this->getEntityManager()->persist($ordConfigStore);
            $this->getEntityManager()->flush();
            
            $store->setAddress($this->getEntityManager());
        }
        
        return $store;
    }
    
    function updateGatewayFromStore($storeId, $gatewayName, $merchant, $merchantId, $adminId) {
        $datetime   = new \DateTime();
        $gatewayRel = $this->getEntityManager()->getRepository(PayGatewayRel::class)->findOneBy(['evtEvent' => $storeId]);
        $gateway    = new PayGateway();
        $gateway->setId($datetime->getTimestamp());
        $gateway->setGateway($gatewayName);
        $gateway->setMerchantKey($merchant);
        $gateway->setMerchantId($merchantId);
        $gateway->setCreatedBy($adminId);
        $this->getEntityManager()->persist($gateway);
        
        if ($gatewayRel) {
            $gatewayRel->setModifiedBy($adminId);
            $gatewayRel->setPayGateway($gateway);
        } else {
            $gatewayRel = new PayGatewayRel();
            $gatewayRel->setId($datetime->getTimestamp());
            $gatewayRel->setPayGateway($gateway);
            $gatewayRel->setCreatedBy($adminId);
            $gatewayRel->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $storeId));
            $gatewayRel->setNrorg($this->getEntityManager()->getReference(EvtEvent::class, $storeId)->getNrorg());
            $this->getEntityManager()->persist($gatewayRel);
        }
        
        $this->getEntityManager()->flush();
    }
    
    function updatePaymentMethod($paymentMethodId, $name, $type, $xPicpayToken, $xSellerToken) {
        $paymentMethod = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->find($paymentMethodId);
        
        $paymentMethod->setLabel($name);
        $paymentMethod->setXPicpayToken($xPicpayToken);
        $paymentMethod->setXSellerToken($xSellerToken);
        $paymentMethodName = '';
        if ($type == 'CC') {
            $paymentMethodName = 'CC';
        } else if ($type == 'TEF_CC' || $type == 'TEF_DC') {
            $paymentMethodName = $type . '_' . $paymentMethod->getId();
        } else if ($type == 'PICPAY') {
            $paymentMethodName = 'PICPAY_' . $paymentMethod->getId();
        } else {
            $paymentMethodName = 'EXT_' . $paymentMethod->getId();
        }
        $paymentMethod->setPaymentMethod($paymentMethodName);
        
        $this->getEntityManager()->flush();
    }
    
    function createPaymentMethod($storeId, $name, $type, $xPicpayToken=NULL, $xSellerToken=NULL) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        $paymentMethod = new PayPaymentMethod();
        $paymentMethod->setLabel($name);
        $paymentMethod->setNrorg($store->getNrorg());
        $paymentMethod->setXPicpayToken($xPicpayToken);
        $paymentMethod->setXSellerToken($xSellerToken);
        
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        if ($configStore == NULL) {
            $configStore = new OrdConfigStore();
            $configStore->setEvent($store);
            $configStore->setNrorg($store->getNrorg());
            $this->getEntityManager()->persist($configStore);
        }
        
        $ordStorePayMethod = new OrdStorePayMethod();
        $ordStorePayMethod->setPaymentMethod($paymentMethod);
        $ordStorePayMethod->setOrdConfigStore($configStore);
        
        $this->getEntityManager()->persist($ordStorePayMethod);
        $this->getEntityManager()->persist($paymentMethod);
        
        $this->getEntityManager()->flush();

        $paymentMethodName = '';
        if ($type == 'CC') {
            $paymentMethodName = 'CC';
        } else if ($type == 'TEF_CC' || $type == 'TEF_DC') {
            $paymentMethodName = $type . '_' . $paymentMethod->getId();
        } else {
            $paymentMethodName = 'EXT_' . $paymentMethod->getId();
        }
        $paymentMethod->setPaymentMethod($paymentMethodName);
        
        $paymentMethodName = '';
        if ($type == 'CC') {
            $paymentMethodName = 'CC';
        } else if ($type == 'TEF_CC' || $type == 'TEF_DC') {
            $paymentMethodName = $type . '_' . $paymentMethod->getId();
        } else if ($type == 'PICPAY') {
            $paymentMethodName = 'PICPAY_' . $paymentMethod->getId();
        } else {
            $paymentMethodName = 'EXT_' . $paymentMethod->getId();
        }

        $paymentMethod->setPaymentMethod($paymentMethodName);
        $this->getEntityManager()->flush();
    }
    
    function removePaymentMethod($paymentMethodId) {
        $ordStorePayMethod = $this->getEntityManager()->getRepository(OrdStorePayMethod::class)->findOneBy(['paymentMethod' => $paymentMethodId]);
        $this->getEntityManager()->remove($ordStorePayMethod);
        $this->getEntityManager()->flush();
    }
    
    /* Edita uma loja com os parâmetros passados */
    function removeStore($storeId) {
        $em        = $this->getEntityManager();
        $store     = $em->getRepository(EvtEvent::class)->find($storeId);
        $removedId = $store->getId();
        
        $store->setStatus('I');
        
        $this->getEntityManager()->flush();
        
        return $removedId;
    }
    
    function updateShift($shiftId, $day, $initialTime, $finalTime) {
        $shift = $this->getEntityManager()->getRepository(GenShift::class)->find($shiftId);
        $shift->setDay($day);
        $shift->setInitialTime(new \DateTime($initialTime));
        $shift->setFinalTime(new \DateTime($finalTime));
        
        $this->getEntityManager()->flush();
    }
    
    function updateSellerFromStore($sellerId, $sellerTypeId) {
        $seller     = $this->getEntityManager()->getRepository(EvtEventSeller::class)->find($sellerId);
        $sellerType = $this->getEntityManager()->getRepository(EvtSellerType::class)->find($sellerTypeId);
        
        $seller->setEvtSellerType($sellerType);
        
        $this->getEntityManager()->flush();
    }
    
    function createStoreShift($storeId, $days, $initialTime, $finalTime) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        foreach($days as $day){
            $shift = new GenShift();
            $shift->setDay($day);
            $shift->setInitialTime(new \DateTime($initialTime));
            $shift->setFinalTime(new \DateTime($finalTime));
            $shift->setEvtEvent($store);
            $this->getEntityManager()->persist($shift);
            $this->getEntityManager()->flush();
        }
    }
    
    function removeShift($shiftId) {
        $shift = $this->getEntityManager()->getRepository(GenShift::class)->find($shiftId);
        $this->getEntityManager()->remove($shift);
        $this->getEntityManager()->flush();
    }
    
    function getEventSellersFromStore($storeId) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $sellers = $this->getEntityManager()->createQuery(
            "
            SELECT ev
            FROM 'Zeedhi\ApiOrders\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiOrders\Model\Entities\EvtEventSeller' ev WITH ev.genUser = u.id
            JOIN ev.evtEvent as evt
            JOIN 'Zeedhi\ApiOrders\Model\Entities\EvtEvent' e WITH e.id = evt.id
            WHERE e.id = $storeId
            "    
        )->getResult();
        foreach ($sellers as $seller) {
            $seller->build($this->getEntityManager(), $store->getNrorg());
        }
        return $sellers;
    }
    
    function getUserTypesFromStore($storeId) {
        $userTypes = $this->getEntityManager()->createQuery(
            "
            SELECT st
            FROM 'Zeedhi\ApiOrders\Model\Entities\EvtSellerType' st
            JOIN st.evtEvent s
            WHERE s.id = $storeId
            "
        )->getResult();
        
        foreach ($userTypes as $userType) {
            $userType->build($this->getEntityManager());
        }
        
        return $userTypes;
    }
 
    function linkUserToStore($storeId, $sellerEmail, $sellerTypeId, $adminId) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $user  = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(['email' => $sellerEmail, 'status' => 'A']);
        $sellerType = $this->getEntityManager()->getRepository(EvtSellerType::class)->find($sellerTypeId);
        
        if ($user == NULL) throw new Exception('User not found', 17);
        
        $sellerExists = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $user->getId(), 'evtEvent' => $storeId, 'status' => 'A']) != NULL;
        if ($sellerExists == true) throw new Exception('Seller already linked', 18);
        
        $seller = new EvtEventSeller();
        $seller->setEvtEvent($store);
        $seller->setGenUser($user);
        $seller->setNrorg($store->getNrorg());
        $seller->setEvtSellerType($sellerType);
        $seller->setCreatedBy($adminId);
        $seller->setStatus('A');
        
        $this->getEntityManager()->persist($seller);
        $this->getEntityManager()->flush();
    }
    
    function unlinkUserFromStore($storeId, $userId, $adminId) {
        $seller = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $userId, 'evtEvent' => $storeId]);
        $seller->setEvtEvent(null);
        $seller->setModifiedBy($adminId);
        
        $this->getEntityManager()->flush();
    }
    
    function createSellerType($storeId, $sellerTypeName, $visibleStatus) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        $sellerType = new EvtSellerType();
        $sellerType->setEvtEvent($store);
        $sellerType->setName($sellerTypeName);
        $sellerType->setNrorg($store->getNrorg());
        
        foreach ($visibleStatus as $status) {
            $status = $this->getEntityManager()->getRepository(GenStatus::class)->find($status['ID']);
            
            $statusUserType = new GenStatusUserType();
            $statusUserType->setGenStatus($status);
            $statusUserType->setEvtSellerType($sellerType);
            
            $this->getEntityManager()->persist($statusUserType);
        }
        
        $this->getEntityManager()->persist($sellerType);
        $this->getEntityManager()->flush();
        
        return $sellerType;
    }
    
    function updateSellerType($sellerTypeId, $sellerTypeName, $visibleStatus) {
        $sellerType = $this->getEntityManager()->getRepository(EvtSellerType::class)->find($sellerTypeId);
        
        $sellerType->setName($sellerTypeName);

        foreach ($visibleStatus as $status) {
            $this->cleanStatusUserType($sellerTypeId);
            
            $status = $this->getEntityManager()->getRepository(GenStatus::class)->find($status['ID']);
            
            $statusUserType = new GenStatusUserType();
            $statusUserType->setGenStatus($status);
            $statusUserType->setEvtSellerType($sellerType);
            
            $this->getEntityManager()->persist($statusUserType);
        }
        
        $this->getEntityManager()->flush();
    }
    
    function removeSellerType($sellerTypeId) {
        $sellerType = $this->getEntityManager()->getRepository(EvtSellerType::class)->find($sellerTypeId);
        
        $sellerType->setEvtEvent(NULL);
        
        $this->getEntityManager()->flush();
    }
    
    function cleanStatusUserType($sellerTypeId) {
        $statusUserType = $this->getEntityManager()->getRepository(GenStatusUserType::class)->findBy(['evtSellerType' => $sellerTypeId]);
        
        foreach ($statusUserType as $statusUT) {
            $this->getEntityManager()->remove($statusUT);
        }
    }
    
    public function requestAddressChange($storeId, $cep, $street, $neighborhood, $city, $provincy, $number, $type, $latitude=NULL, $longitude=NULL) {
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $old         = $this->getEntityManager()->getRepository(GenAddress::class)->findOneBy(['evtEvent' => $storeId, 'status' => 'A', 'type' => $type]);
        
        if ($old != NULL) {
            $old->setStatus('O');
            $this->getEntityManager()->persist($old);
        }
        
        if($number == '') {
            $number = NULL;
        }
        
        /* Creating new address */
        $address = new GenAddress();
        $address->setEvtEvent($store);
        $address->setStatus('A');
        $address->setCreateDate(new \DateTime());
        
        $address->setCep($cep);
        $address->setStreet($street);
        $address->setNeighborhood($neighborhood);
        $address->setCity($city);
        $address->setProvincy($provincy);
        $address->setNumber($number);
        $address->setNrorg($store->getNrorg());
        $address->setType($type);
        $address->setOld($old);
        $address->setLatitude($latitude);
        $address->setLongitude($longitude);
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
        
        return ['status'=>'A', 'id'=>$address->getId()];
    }

    public function getExternalIntegrationData ($row) {
        $eventId = $row['STORE_ID'];
        $evtEvent   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        return $evtEvent;
    }

    public function getServicemodalitiesData ($row) {
        $storeId = $row['STORE_ID'];
        $ordConfigStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        return $ordConfigStore;
    }

    public function UpdateIntegration ($row) {
        try {
            $eventId = $row['STORE_ID'];
            $ipStore = isset($row['IP_STORE']) ? $row['IP_STORE'] : null;
            $integration_commands = isset($row['INTEGRATION_COMMANDS']) ? $row['INTEGRATION_COMMANDS'] : 'F';
            $evtEvent   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
            
            if ($evtEvent == NULL) return ['error', ['evtevent not found']];    
            
            $evtEvent->setIpStore($ipStore);
            $evtEvent->setIntegrationCommands($integration_commands);

            $this->getEntityManager()->persist($evtEvent);
            $this->getEntityManager()->flush();
            return ['success', 'event_id: '. $evtEvent->getId()];
        } catch (\Exception $e) { 
            return ['error', $e->getMessage()];            
        }
    }

    public function saveAddDeliversToWidget ($rows) {
        try {
            $row = $rows['ROW'];
            $nrorg = $rows['NRORG'];
            $eventId = $rows['STORE_ID'];
            
            $typeRef = $row['typeRef'];
            $typeName = $row['typeName'];
            $max_scheduling = $row['max_scheduling'];
            $allowsScheduling = $row['allows_scheduling'];
    
            $evtEvent   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
            
            $OrdConfigStoreDelivers = new OrdConfigStoreDelivers;
            $OrdConfigStoreDelivers->setNrorg($nrorg);
            $OrdConfigStoreDelivers->setTypeDelivers($typeRef);
            $OrdConfigStoreDelivers->setTypeName($typeName);
            $OrdConfigStoreDelivers->setEvent($evtEvent);
            $OrdConfigStoreDelivers->setAllowsScheduling($allowsScheduling);
            $OrdConfigStoreDelivers->setMaxScheduling($max_scheduling);
            
            $OrdConfigStoreDelivers->setCreatedAt();
            $OrdConfigStoreDelivers->setModifiedAt();
            $OrdConfigStoreDelivers->setCreatedBy($nrorg);
            $OrdConfigStoreDelivers->setModifiedBy($nrorg);
            
            $this->getEntityManager()->persist($OrdConfigStoreDelivers);
            $this->getEntityManager()->flush();
            
            return ['success', 'OrdConfigStoreDelivers: '. $OrdConfigStoreDelivers->getId()];
        } catch (\Exception $e) { 
            return ['error', $e->getMessage()];            
        }
    }

    public function getDeliversTo ($row) {
        $storeId = $row['STORE_ID'];
        $nrorg = $row['NRORG'];
        
        $ordConfigStore = $this->getEntityManager()->getRepository(OrdConfigStoreDelivers::class)->findBy(['event' => $storeId, 'nrorg' => (int) $nrorg]);
        return OrdConfigStoreDelivers::manyToArray($ordConfigStore);
        // $arrayType = array();
        // foreach ($arrayOrdConfigSotre as $ordConfig) {
        //     $typeRef = $ordConfig['typeRef'];
        //     if (!isset($arrayType[$typeRef])) {
        //         $arrayType[$typeRef] = array();
        //     }
        //     array_push($arrayType[$typeRef] ,$ordConfig);
        // }
        // return $arrayType;
    }

    public function saveUpdateDeliversToWidget ($rows) {
        try {
            $row = $rows['ROW'];
            $nrorg = $rows['NRORG'];
            $eventId = $rows['STORE_ID'];
            
            $deliversId = $row['id'];
            $typeRef = $row['typeRef'];
            $typeName = $row['typeName'];
            $max_scheduling = $row['max_scheduling'];
            $allowsScheduling = $row['allows_scheduling'];
            
            $OrdConfigStoreDelivers   = $this->getEntityManager()->getRepository(OrdConfigStoreDelivers::class)->find($deliversId);

            if (gettype($OrdConfigStoreDelivers) != 'object') return ['error', 'OrdConfigStoreDelivers not found'];      
            
            $OrdConfigStoreDelivers->setCreatedAt();
            $OrdConfigStoreDelivers->setModifiedAt();
            $OrdConfigStoreDelivers->setCreatedBy($nrorg);
            $OrdConfigStoreDelivers->setModifiedBy($nrorg);
            $OrdConfigStoreDelivers->setTypeName($typeName);
            $OrdConfigStoreDelivers->setMaxScheduling($max_scheduling);
            $OrdConfigStoreDelivers->setAllowsScheduling($allowsScheduling);

            $this->getEntityManager()->persist($OrdConfigStoreDelivers);
            $this->getEntityManager()->flush();
            
            return ['success', 'OrdConfigStoreDelivers: '. $OrdConfigStoreDelivers->getId()];
        } catch (\Exception $e) { 
            return ['error', $e->getMessage()];            
        }
    }
    
    public function saveRemoveDeliversToWidget ($rows) {
        try {
            $row = $rows['ROW'];
            $nrorg = $rows['NRORG'];
            $eventId = $rows['STORE_ID'];
            $typeName = $row['typeName'];
            $allowsScheduling = $row['allows_scheduling'];
            $deliversId = $row['id'];
            
            $OrdConfigStoreDelivers   = $this->getEntityManager()->getRepository(OrdConfigStoreDelivers::class)->find($deliversId);

            if (gettype($OrdConfigStoreDelivers) != 'object') return ['error', 'OrdConfigStoreDelivers not found'];      

            $this->getEntityManager()->remove($OrdConfigStoreDelivers);
            $this->getEntityManager()->flush();

            $serTimesheetRels = $this->getEntityManager()->getRepository(SerTimesheetRel::class)->findBy(['nrorg' => $nrorg, 'ordConfigStoreDeliversId' => $deliversId]);
            if (!empty($serTimesheetRel)) {
                foreach($serTimesheetRels as $serTimesheetRel) {
                    $this->getEntityManager()->remove($serTimesheetRel);
                    $this->getEntityManager()->flush();
                }
            }            

            return ['success', 'OrdConfigStoreDelivers: '. $OrdConfigStoreDelivers->getId()];
        } catch (\Exception $e) { 
            return ['error', $e->getMessage()];            
        }        
    }
    
    public function updateStoreRecipient($storeId, $recipientId){
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        /* Alterar Status da Loja */
        $store->setStoneRecipientId($recipientId);
        
        /* Atualizar no Banco */
        $this->getEntityManager()->flush();
        
        return $store;
        
    }
    
}