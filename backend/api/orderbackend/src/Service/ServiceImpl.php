<?php
namespace Zeedhi\ApiOrders\Service;

class ServiceImpl implements \Zeedhi\Framework\Security\OAuth\Service {
	private $clientId;
	private $secretId;
	private $name;

	public function __construct($name, $clientId = null, $clientSecret = null) {
		$this->clientId = $clientId;
		$this->secretId = $clientSecret;
		$this->name = $name;
	}

	public function getClientId() {
		return $this->clientId;
	}

	public function getClientSecret() {
		return $this->secretId;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}
}