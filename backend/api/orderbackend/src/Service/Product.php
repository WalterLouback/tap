<?php
namespace Zeedhi\ApiOrders\Service;

use Zeedhi\ApiOrders\Helpers\General;

use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtEventMenu;
use Zeedhi\ApiOrders\Model\Entities\GenShift;
use Zeedhi\ApiOrders\Model\Entities\OrdMenu;
use Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdOption;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdProductOrder;
use Zeedhi\ApiOrders\Model\Entities\OrdProductGroup;
use Zeedhi\ApiOrders\Model\Entities\OrdOrder;
use Zeedhi\ApiOrders\Model\Entities\OrdStoreConfig;
use Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod;

use Zeedhi\ApiOrders\Helpers\ImageUploader;
use Zeedhi\ApiOrders\Helpers\DateFormatFunction;

use Doctrine\ORM\EntityManager;

class Product extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment, ImageUploader $imageUploader) {
        parent::__construct($entityManager, $environment);
        $this->imageUploader = $imageUploader;
    }
    
    public function createMenu($name, $nrorg, $products, $storeId, $workshifts, $canFlush=true) {
        /* Criar e persistir Menu */
        $menu = new OrdMenu();
        $menu->setName($name);
        $menu->setNrorg($nrorg);
        $this->getEntityManager()->persist($menu);
        
        /* Buscar Loja */
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        /* Criar e persistir associativa entre Loja e Cardápio */
        $associativa = new EvtEventMenu();
        $associativa->setEvtEvent($store);
        $associativa->setOrdMenu($menu);
        $this->getEntityManager()->persist($associativa);
        
        /* Criar turnos e relacionar ao Menu */
        foreach ($workshifts as $shift) {
            $day         = $shift['DAY'];
            $initialTime = new \DateTime($shift['INITIAL_TIME']);
            $finalTime   = new \DateTime($shift['FINAL_TIME']);
            
            $this->createWorkshift($day, $initialTime, $finalTime, 'MENU', $storeId, $associativa);
        }
        
        /* Criar produtos e relacionar ao Menu */
        foreach($products as $product) {
            $name           = $product['NAME'];
            $detail         = $product['DETAIL'];
            $price          = $product['PRICE'];
            $status         = $product['STATUS'];
            $ordination     = $product['ORDINATION'] ? $product['ORDINATION'] : NULL;
            $externalId     = $product['EXTERNAL_ID'] ? $product['EXTERNAL_ID'] : NULL;
            $extras         = isset($product['EXTRAS']) ? $product['EXTRAS'] : [];
            $productGroupId = isset($product['PRODUCT_GROUP_ID']) ? $product['PRODUCT_GROUP_ID'] : NULL;
            $barCode        = $product['BAR_CODE'] ? $product['BAR_CODE'] : NULL;
            $this->createProduct($name, $detail, $price, $nrorg, $extras, $menu->getId(), $productGroupId, $status, false, $externalId,$barCode);
        }
        
        /* Commit */
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $menu;
    }
    
    public function createWorkshift($day, $initialTime, $finalTime, $type, $storeId, $evtEventMenu) {
        $workshift = new GenShift();
        $workshift->setDay($day);
        $workshift->setInitialTime($initialTime);
        $workshift->setFinalTime($finalTime);
        
        $evtEventMenu->getOrdMenu()->workshiftIsAvailable($storeId, $day, $initialTime, $finalTime, $this->getEntityManager());
        
        if ($type == 'STORE') {
            $workshift->setEvtEvent($this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId));
        }
        else if ($type == 'MENU') {
            $workshift->setEvtEventMenu($evtEventMenu);
        }
        
        $this->getEntityManager()->persist($workshift);
    }
     function getExtras($productId){
        $extras = $this->getEntityManager()->getRepository(OrdExtra::class)->findBy(["ordMenuProduct" => $productId]);
        
        foreach ($extras as $extra) {
            $extra->build($this->getEntityManager());
        }
        
        $newExtras = $this->getNewExtrasProd($productId);
        
        $allProducts = array_merge($extras, $newExtras);
        
        return $allProducts;
    }
    function getNewExtrasProd($productId){
        
        $newExtras = $this->getEntityManager()->createQuery(
            "
            SELECT e
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdExtra' e
            JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProductExtra pe WITH pe.ordExtra = e
            JOIN pe.ordMenuProduct as p
            WHERE p.id = $productId
            "
        )->getResult();
        
        foreach ($newExtras as $newExtra) {
            $newExtra->build($this->getEntityManager());
        }
        return $newExtras;
    }
    
    public function createProductGroup($name, $storeId, $menuId, $canFlush=true) {
        /* Buscar loja e cardápio para associar à categoria */
        $store  = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $menu   = $this->getEntityManager()->getRepository(OrdMenu::class)->find($menuId);
        
        /* Criar e persistir Categoria */
        $productGroup = new OrdProductGroup();
        $productGroup->setName($name);
        $productGroup->setEvtEvent($store);
        $productGroup->setOrdMenu($menu);
        
        $this->getEntityManager()->persist($productGroup);
        
        /* Commit */
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $productGroup;
    }
    
    public function createProduct($name, $detail, $price, $nrorg, $estimatedTime, $extras, $menuId, $productGroupId, $storeId, $status, $image, $canFlush=true, $externalId=null,$barCode=null) {
        /* Criar produto */
        $product = new OrdProduct();
        $product->setName($name);
        $product->setDetail($detail);
        $product->setPrice($price);
        $product->setNrorg($nrorg);
        $product->setImage($this->imageUploader->upload($image, $nrorg));
        if($barCode)$product->setBarCode($barCode);
        if($externalId) $product->setExternalId($externalId);
        $this->getEntityManager()->persist($product);
        
        /* Associar a cardápio e categoria */
        if ($productGroupId !== NULL) {
            $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $menu         = $productGroup->getOrdMenu();
        } else if ($menuId == NULL) {
            /* Se cardápio não for informado, criar um novo*/
            $newMenuName = 'Cardápio sem nome';
            $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
            $menuId = $menu->getId();
        }
        $menuProduct = $this->createMenuProduct($product, $status, $estimatedTime, $menuId, $productGroupId, true, null);
        
        /* Inserir extras no produto */
        foreach ($extras as $extra) {
            $this->createExtra($extra['NAME'], $extra['REQUIRED'], $extra['MULTIPLE'], $extra['OPTIONS'], $menuProduct->getId(), $row['LIMIT_QUANTITY'], false);
        }
        
        /* Commit */
        if ($canFlush) $this->getEntityManager()->flush();
        
        $menuProduct->build($this->getEntityManager());
        
        return $menuProduct;
    }
    
    public function createMenuProduct($product, $status, $estimatedTime, $menuId, $productGroupId, $canFlush=true, $ordination=null) {
        $menu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($menuId);
        $menuProduct = new OrdMenuProduct();
        $menuProduct->setOrdMenu($menu);
        $menuProduct->setOrdProduct($product);
        $menuProduct->setPrice($product->getPrice());
        $menuProduct->setName($product->getName());
        $menuProduct->setDetail($product->getDetail());
        $menuProduct->setNrorg($product->getNrorg());
        $menuProduct->setImage($product->getImage());
        $menuProduct->setEstimatedTime($estimatedTime);
        if ($ordination) $menuProduct->setOrdination($ordination);
        $menuProduct->setStatus($status);
        if ($productGroupId != NULL) $menuProduct->setOrdProductGroup($this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId));
        
        $this->getEntityManager()->persist($menuProduct);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $menuProduct;
    }
    
    public function createExtra($name, $required, $multiple, $options, $menuProductId, $qtdlimit, $canFlush=true, $ordination = null) {

        /* Buscando produto para relacionar ao extra */
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        
        /* Criando o extra e definindo atributos */
        $extra = new OrdExtra();
        $extra->setName($name);
        $extra->setOrdination($ordination);
        $extra->setRequired($required);
        $extra->setMultiple($multiple);
        $extra->setOrdMenuProduct($menuProduct);
        $extra->setLimitQuantity($qtdlimit);

        $this->getEntityManager()->persist($extra);
        $this->getEntityManager()->flush();
        
        /* Criando options do Extra */
        foreach($options as $option) {
            $activeOption = isset($option['active']) ? $option['active'] : 1;
            $this->createOption($option['NAME'], $option['PRICE'], $extra->getId(), false, $activeOption, $option['ORDINATION']);
        }
        
        /* Commit */
        $this->getEntityManager()->flush();
        
        return $extra;
    }
    
    public function createOption($name, $price, $extraId, $canFlush=true, $active, $ordination=null) {
        /* Buscar extra para relacionar à opção */
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        
        /* Criar opção */
        $option = new OrdOption();
        $option->setName($name);
        $option->setPrice($price);
        $option->setOrdExtra($extra);
        $option->setActive($active);
        $option->setOrdination($ordination);
        
        $this->getEntityManager()->persist($option);
        
        /* Commit */
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $option;
    }
    
    public function getProductsFromStore($storeId) {
        $menuProducts = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdProduct' p
            JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct mp WITH mp.ordProduct = p
            JOIN mp.ordMenu as m
            JOIN Zeedhi\ApiOrders\Model\Entities\EvtEventMenu em WITH em.ordMenu = m
            JOIN em.evtEvent as e
            WHERE e.id = $storeId
            "
        )->getResult();
        
        foreach ($menuProducts as $menuProduct) {
            $menuProduct->build($this->getEntityManager());
        }
        
        return $menuProducts;
    }
    
    public function getProductsFromOrganization($nrorg, $storeId) {
        /* Getting products from organization that are not in store */
        $productsFromOrg = $this->getEntityManager()->createQuery(
            "
            SELECT p
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdProduct' p
            WHERE p.id not in (
                SELECT p2.id
                FROM 'Zeedhi\ApiOrders\Model\Entities\OrdProduct' p2
                JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct mp WITH mp.ordProduct = p2
                JOIN mp.ordMenu as m
                JOIN Zeedhi\ApiOrders\Model\Entities\EvtEventMenu em WITH em.ordMenu = m
                JOIN em.evtEvent as e
                WHERE e.id = $storeId
            )
            AND p.nrorg = $nrorg
            OR p.nrorg = 0
            "
        )->getResult();
        
        $productsFromStore = $this->getProductsFromStore($storeId);
        $allProducts = array_merge($productsFromOrg, $productsFromStore);
        
        return $allProducts;
    }
    
    public function cloneProductGroupsFromMenu($sourceMenuId, $targetMenuId, $storeId) {
        $sourceMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($sourceMenuId);
        $targetMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($targetMenuId);
        
        $sourceMenu->build($this->getEntityManager(), $storeId);
        
        $sourceMenu->cloneProductGroups($targetMenu, $this->getEntityManager());
        $this->getEntityManager()->flush();
    }
    
    public function removeProductFromGroup($menuProductId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setOrdProductGroup(null);
        $this->getEntityManager()->flush();
    }
    
    public function removeProductFromStore($menuProductId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setOrdMenu(null);
        $menuProduct->setOrdProductGroup(null);
        $this->getEntityManager()->flush();
    }
    
    public function removeMenuFromStore($menuId, $storeId) {
        $evtEventMenu = $this->getEntityManager()->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $storeId, 'ordMenu' => $menuId]);
        $evtEventMenu->setOrdMenu(null);
        
        $this->getEntityManager()->flush();
    }
    
    public function removeExtraFromProduct($extraId) {
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        $extra->setOrdMenuProduct(null);
        $this->getEntityManager()->flush();
    }
    
    public function removeProductGroup($productGroupId) {
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        $productGroup->build($this->getEntityManager());
        $productGroup->setEvtEvent(null);
        $productGroup->setOrdMenu(null);
        
        $menuProducts = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->findBy(['ordProductGroup' => $productGroupId]);
        foreach ($menuProducts as $menuProduct) {
            $menuProduct->setOrdProductGroup(null);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function removeOption($optionId) {
        $option = $this->getEntityManager()->getRepository(OrdOption::class)->find($optionId);
        $option->setOrdExtra(null);
        $this->getEntityManager()->flush();
    }
    
    public function linkProductToGroup($productId, $productGroupId) {
        $product = $this->getEntityManager()->getRepository(OrdProduct::class)->find($productId);
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        
        $menuProduct = $this->createMenuProduct($product, 'A', 0, $productGroup->getOrdMenu(), $productGroup->getId(), true, null);
        
        $this->getEntityManager()->flush();
    }
    
    public function linkMenuProductToGroup($menuProductId, $productGroupId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        
        $menuProduct = $menuProduct->getClone($this->getEntityManager(), $productGroup);
        
        $this->getEntityManager()->flush();
    }
    
    public function linkProductToStore($productId, $storeId, $productGroupId, $menuId) {
        $product = $this->getEntityManager()->getRepository(OrdProduct::class)->find($productId);
        $store   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        if ($productGroupId !== NULL) {
            $productGroup   = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $productGroupId = $productGroup->getId();
        } else if ($menuId == NULL) {
            /* Se cardápio não for informado, criar um novo*/
            $newMenuName = 'Cardápio sem nome';
            $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
            $menuId = $menu->getId();
        }
        
        $menuProduct = $this->createMenuProduct($product, 'A', 0, $menuId, $productGroupId, true, null);
        
        $this->getEntityManager()->flush();
        return $menuProduct;
    }
    
    public function linkMenuProductToStore($menuProductId, $storeId, $productGroupId, $menuId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        
        if ($productGroupId !== NULL) {
            $productGroup   = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $productGroupId = $productGroup->getId();
        } else if ($menuId == NULL) {
            /* Se cardápio não for informado, criar um novo*/
            $newMenuName = 'Cardápio sem nome';
            $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
            $menuId = $menu->getId();
        }
        
        $menuProduct = $menuProduct->getClone($this->getEntityManager(), $productGroup);
        
        $this->getEntityManager()->flush();
        return $menuProduct;
    }
    
    public function updateMenu($menuId, $name, $workshifts, $storeId) {
        $menu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($menuId);
        
        $menu->setName($name);
        
        $evtEventMenu = $this->getEntityManager()->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $storeId, 'ordMenu' => $menuId]);
        
        if ($workshifts != NULL) {
            $this->cleanMenuWorkshifts($evtEventMenu);
            
            /* Criar turnos e relacionar ao Menu */
            foreach ($workshifts as $shift) {
                $day         = $shift['DAY'];
                $initialTime = new \DateTime($shift['INITIAL_TIME']);
                $finalTime   = new \DateTime($shift['FINAL_TIME']);
                
                $this->createWorkshift($day, $initialTime, $finalTime, 'MENU', $storeId, $evtEventMenu);
            }
        }
        
        $this->getEntityManager()->flush();
    }
    
    private function cleanMenuWorkshifts($evtEventMenu) {
        $workshifts = $this->getEntityManager()->getRepository(GenShift::class)->findBy(['evtEventMenu' => $evtEventMenu]);
        
        foreach ($workshifts as $shift) {
            $this->getEntityManager()->remove($shift);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function updateExtra($extraId, $name, $required, $multiple, $options, $qtdlimit, $ordination = null) {
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        
        $extra->setName($name);
        $extra->setOrdination($ordination);
        $extra->setRequired($required);
        $extra->setMultiple($multiple);
        $extra->setLimitQuantity($qtdlimit);
        if (count($options) > 0) {
            $this->cleanOptionsFromExtra($extra);
            foreach($options as $option) {
                $activeOption = isset($option['active']) ? $option['active'] : 1;
                $this->createOption($option['NAME'], $option['PRICE'], $extra->getId(), false, $activeOption, $option['ORDINATION']);
            }
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function cleanOptionsFromExtra($extra) {
        $extra->build($this->getEntityManager());
        foreach ($extra->getOptions() as $option) {
            $option->setOrdExtra(NULL);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function updateProductGroup($productGroupId, $name, $ordination=null, $parentId=null) {
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
       
    
        $productGroup->setName($name);
        $productGroup->setOrdination($ordination);
        
        if ($parentId == -1) $productGroup->setParentId(null);
        else if ($parentId) $productGroup->setParentId($parentId);
         
        $this->getEntityManager()->flush();
    }
    
    public function updateProduct($menuProductId, $name, $detail, $price, $nrorg, $estimatedTime, $menuId, $productGroupId, $image, $status, $amount, $ordination=null, $externalId=null, $barCode=null) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $product = $this->getEntityManager()->getRepository(OrdProduct::class)->find($menuProduct->getOrdProduct()->getId());
        
        if ($name) $menuProduct->setName($name);
        if ($detail) $menuProduct->setDetail($detail);
        if ($price) $menuProduct->setPrice($price);
        if ($nrorg) $menuProduct->setNrorg($nrorg);
        if ($status) $menuProduct->setStatus($status);
        if ($image) $menuProduct->setImage($this->imageUploader->upload($image, $nrorg));
        if ($estimatedTime) $menuProduct->setEstimatedTime($estimatedTime);
        if ($amount) $menuProduct->setAmount($amount);
        if ($ordination) $menuProduct->setOrdination($ordination);
        if($barCode)$menuProduct->setBarCode($barCode);
        if($externalId !== NULL) {
            
            $product->setExternalId($externalId);
            $this->getEntityManager()->persist($product);
        }
        
        if ($productGroupId && ($menuProduct->getOrdProductGroup() == NULL || $productGroupId != $menuProduct->getOrdProductGroup()->getId())) {
            $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $menuProduct->setOrdProductGroup($productGroup);
            $menuProduct->setOrdMenu($productGroup->getOrdMenu());
        }
        
        $this->getEntityManager()->persist($menuProduct);
        $this->getEntityManager()->flush();
        
        $menuProduct->build($this->getEntityManager());
        return $menuProduct;
    }
    
    function uploadImage($image) {
        $url = 'https://api.imgur.com/3/image';
        $data = array('image' => $image);
        
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }
        
        var_dump($result);
    }
    
    /* Este metodo serve para colocar um produto em destaque OU remover */
    public function highlightProduct($menuProductId, $highlight, $userId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setFeatured($highlight);
        $menuProduct->setModifiedBy($userId);
        $this->getEntityManager()->persist($menuProduct);
        $this->getEntityManager()->flush();
        
        return $menuProduct;
    }
    
    public function getFeaturedProducts($nrorg, $storeIds) {
        $dateFormat     = DateFormatFunction::class;
        
        $config = $this->getEntityManager()->getConfiguration();
        $config->addCustomDatetimeFunction('DATE_FORMAT', $dateFormat);
        
        $queryStoreOne  = !empty($storeIds) ? " LEFT JOIN mp.ordMenu as m JOIN " . EvtEventMenu::class ." em WITH em.ordMenu = m JOIN em.evtEvent as e" : "";
        $queryStore     = !empty($storeIds) ? " AND e.id IN (" . join(", ", $storeIds) . ") " : "";
        $datetime       = date('Y-m-d H:i:s');
        $day_hour       = date('H', strtotime($datetime));
        $day_num        = date('w', strtotime($datetime));
        
        $productsFromOrg = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM " . OrdMenuProduct::class . " mp
            JOIN " . OrdProduct::class . " p WITH mp.ordProduct = p
            JOIN " . EvtEventMenu::class .  " eem WITH (eem.ordMenu = mp.ordMenu)
            JOIN " . GenShift::class . " gs WITH ( gs.evtEventMenu = eem.id AND gs.day = '$day_num' AND DATE_FORMAT(gs.initialTime, '%H %i %s') <= '$day_hour' AND DATE_FORMAT(gs.finalTime, '%H %i %s') >= '$day_hour' )
            $queryStoreOne
            WHERE mp.nrorg = $nrorg AND mp.status = 'A' AND mp.featured is not null AND mp.featured <> '0'
            $queryStore
            AND NOT EXISTS 
                    ( SELECT mp1.id FROM " . OrdExtra::class . " oe 
                        JOIN " . OrdMenuProduct::class . " mp1 IN (oe.ordMenuProduct = mp1) 
                        WHERE mp1.id = mp.id )
            ORDER BY mp.id DESC
            ")->getResult();
        
        return OrdMenuProduct::manyToArray($productsFromOrg);
    }
    
    public function getFeaturedProductsTopTwenty($nrorg, $storeIds) {
        $queryStoreOne = !empty($storeIds) ? " LEFT JOIN mp.ordMenu as m JOIN " . EvtEventMenu::class ." em WITH em.ordMenu = m JOIN em.evtEvent as e" : "";
        $queryStore = !empty($storeIds) ? " AND e.id IN (" . join(", ", $storeIds) . ") " : "";
        $datetime       = date('Y-m-d H:i:s');
        $day_num        = date('w', strtotime($datetime));
        
        $productsFromOrg = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM " . OrdMenuProduct::class . " mp
            JOIN " . OrdProduct::class . " p WITH mp.ordProduct = p
            JOIN " . EvtEventMenu::class .  " eem WITH (eem.ordMenu = mp.ordMenu)
            JOIN " . GenShift::class . " gs WITH ( gs.evtEventMenu = eem.id AND gs.day = '$day_num' )
            $queryStoreOne
            WHERE mp.nrorg = $nrorg AND (mp.featured is null OR mp.featured = false)
            AND mp.status = 'A'
            $queryStore
            AND NOT EXISTS 
                ( SELECT mp1.id FROM " . OrdExtra::class . " oe 
                    JOIN " . OrdMenuProduct::class . " mp1 IN (oe.ordMenuProduct = mp1) 
                    WHERE mp1.id = mp.id )
            ORDER BY mp.id DESC
            ")->setMaxResults(20)->getResult();
        
        return OrdMenuProduct::manyToArray($productsFromOrg);
    }
    
    public function getFeaturedProductsByStore($nrorg, $storeId) {
        $queryStoreOne = " LEFT JOIN mp.ordMenu as m JOIN " . EvtEventMenu::class ." em WITH em.ordMenu = m JOIN em.evtEvent as e";
        $productsFromOrg = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM " . OrdMenuProduct::class . " mp
            JOIN " . OrdProduct::class . " p WITH mp.ordProduct = p
            $queryStoreOne
            WHERE mp.nrorg = $nrorg 
            AND e.id = $storeId
            AND mp.status = 'A'
            AND mp.featured = 1
            ")->getResult();
        
        return OrdMenuProduct::manyToArray($productsFromOrg);
    }
    
    /* Devolve as lojas de uma organização, podendo receber outros filtros */
    function getStores($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll=NULL, $initial=NULL, $max=NULL) {
        $store          = EvtEvent::class;
        $eventMenu      = EvtEventMenu::class;
        
        $structureQuery = $structureId  !== NULL ? "AND s.genStructure = $structureId" : "";
        $parentQuery    = $parentId     !== NULL ? "AND s.parentEvent = $parentId" : "";
        $nameQuery      = $storeName    !== NULL ? "AND s.name = '$storeName'" : "";
        $nrorgQuery     = $nrorg        !== NULL ? "AND s.nrorg = $nrorg" : "";
        $ownerQuery     = $ownerUser    !== NULL ? "AND s.ownerUserId = $ownerUser" : "";
        $menuJoinType   = $showAll      !== NULL ? "LEFT" : "INNER";
        $statusQuery    = $showAll      !== NULL ? "" : "AND s.status <> 'I'";
        
        $stores = $this->getEntityManager()->createQuery(
            "
            SELECT s
            FROM $store s
            $menuJoinType JOIN $eventMenu em WITH em.evtEvent = s.id
            WHERE s.type = 'S'
            $structureQuery
            $parentQuery
            $nameQuery
            $nrorgQuery
            $ownerQuery
            $statusQuery
            ORDER BY s.id
            "
        );
        
        if ($initial !== NULL) $stores->setFirstResult($initial);
        if ($max !== NULL) $stores->setMaxResults($max);
        
        $stores = new \Doctrine\ORM\Tools\Pagination\Paginator($stores);
        
        /* Informar locais de entrega da loja e se ela está aberta */
        foreach ($stores as $store) {
            $store->setDeliversToTable($this->getEntityManager());
            $store->setDeliversToBalcony($this->getEntityManager());
            $store->setDeliversToAddress($this->getEntityManager());
            $store->isOpened($this->getEntityManager());
            $store->setWorkshifts($this->getEntityManager());
           

        }
        
        return $stores;
    }
}