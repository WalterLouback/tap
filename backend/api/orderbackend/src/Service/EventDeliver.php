<?php
namespace Zeedhi\ApiOrders\Service;

use Zeedhi\ApiOrders\Model\Entities\EvtEventDeliver;
use Zeedhi\ApiOrders\Model\Entities\GenAddress;
use Zeedhi\ApiOrders\Model\Entities\GenStructure;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;

use Doctrine\ORM\EntityManager;

class EventDeliver extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getDeliveryData($nrorg, $structureId, $eventId, $userId, $latitude, $longitude) {
        
        $evtEventDeliver= EvtEventDeliver::class; 
        $eventDeliver   = $this->getEntityManager()->createQuery(" SELECT s FROM $evtEventDeliver s WHERE s.evtEvent = $eventId ORDER BY s.distanceKm ")->getResult();
        
        $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg]);
        $adrressIdStore = $structure->toArray()["address"];
        $addressStore   = $this->getEntityManager()->getRepository(GenAddress::class)->find($adrressIdStore);
        $store          = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId, 'nrorg' => $nrorg]);
        
        $store->build($this->getEntityManager());
        
        $addressUser    = null;
        $storeLatitude  = null;
        $storeLongitude = null;
        
        if(!is_null($store->toArray()["address"]) && !is_null($store->toArray()["address"][0]['LATITUDE']) && !is_null($store->toArray()["address"][0]['LONGITUDE']) ){
            $storeLatitude     = $this->replace(floatval($store->toArray()["address"][0]['LATITUDE']));
            $storeLongitude    = $this->replace(floatval($store->toArray()["address"][0]['LONGITUDE']));
        }
        
        return $this->calculateDeliveryRate($addressStore, $addressUser, $eventDeliver, $latitude, $longitude,  $storeLatitude,  $storeLongitude);
    }
    
    function replace($var) {
        return str_replace(",", ".", $var);
    }
    
    public function calculateDeliveryRate($addressStore, $addressUser, $eventDeliver, $latitude, $longitude, $storeLatitude,  $storeLongitude) {
        
         if(is_null($storeLatitude) && is_null($storeLongitude)){
            $storeLatitude  = $this->replace(floatval($addressStore->getLatitude()));
            $storeLongitude = $this->replace(floatval($addressStore->getLongitude()));
         }
         if($addressUser){
             $userLatitude  = $this->replace(floatval($addressUser->getLatitude()));
             $userLongitude = $this->replace(floatval($addressUser->getLongitude()));
         }
         else {
             $userLatitude  = $latitude;
             $userLongitude = $longitude;
         }
        
        return array(
            'DISTANCE' => $this->calcDistance($storeLatitude, $storeLongitude, $userLatitude, $userLongitude),
            'PRICE' => $this->getPriceByDistance($addressStore, $addressUser, $eventDeliver, $latitude, $longitude, $storeLatitude, $storeLongitude)
        );
    }
    
    public function calculateStoreDistance($nrorg, $structureId, $eventId, $userLatitude, $userLongitude) {
        
        $store          = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId, 'nrorg' => $nrorg]);
        $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg]);
        $adrressIdStore = $structure->getGenAddress();
        $addressStore   = $this->getEntityManager()->getRepository(GenAddress::class)->find($adrressIdStore);
        
        if($store->toArray()["latitude"] && $store->toArray()["longitude"]){
            $storeLatitude  = $this->replace(floatval($store->toArray()["latitude"]));
            $storeLongitude = $this->replace(floatval($store->toArray()["longitude"]));
        }
        else {
            $storeLatitude  = $this->replace(floatval($addressStore->getLatitude()));
            $storeLongitude = $this->replace(floatval($addressStore->getLongitude()));
        }
        
        $evtEventDeliver    = EvtEventDeliver::class; 
       
        return $this->calcDistance($storeLatitude, $storeLongitude, $userLatitude, $userLongitude);
    }
    
   
    public function getPriceByDistance($addressStore, $addressUser, $eventDeliver, $latitude, $longitude, $storeLatitude,$storeLongitude) {
        $deliveryPrice      = array();
        if($storeLatitude === null && $storeLongitude === null){
            $storeLatitude  = $this->replace(floatval($addressStore->getLatitude()));
            $storeLongitude = $this->replace(floatval($addressStore->getLongitude()));
         }
         if($addressUser){
             $userLatitude  = $this->replace(floatval($addressUser->getLatitude()));
             $userLongitude = $this->replace(floatval($addressUser->getLongitude()));
         }
         else {
             $userLatitude  = $latitude;
             $userLongitude = $longitude;
         }
        
        $distanceCalc = $this->calcDistance($storeLatitude, $storeLongitude, $userLatitude, $userLongitude);
        
        foreach($eventDeliver as $key => $el) {
            $distance = explode(" ", $el->getDistanceKm());
            
            if( $distanceCalc <= floatval($distance[2]))  {
                return $el->getPrice();
            }
        }
        return "";
    }
    
    public function calcDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $earth_radius = 6371;
        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;
        return $d;
    }
    
    public function getDataByStore($storeId) {
        $data  = $this->getEntityManager()->getRepository(EvtEventDeliver::class)->findBy(['evtEvent' => $storeId]);
        
        return EvtEventDeliver::manyToArray($data);
    }
    
    public function updateRaioKm($eventDeliverId, $distanceKm, $price) {
        $eventDeliver  = $this->getEntityManager()->getRepository(EvtEventDeliver::class)->find($eventDeliverId);
        
        $eventDeliver->setDistanceKm($distanceKm);
        $eventDeliver->setPrice($price);
        
        $this->getEntityManager()->persist($eventDeliver);
        $this->getEntityManager()->flush();
        
        return $eventDeliver->toArray();
    }
    
    public function createRaioKm($storeId, $distanceKm, $price, $nrorg) {
        try {
            $eventDeliver  = new EvtEventDeliver();
            $eventDeliver->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $storeId));
            $eventDeliver->setNrorg($nrorg);
            $eventDeliver->setDistanceKm($distanceKm);
            $eventDeliver->setPrice($price);

            $kmArray = explode(' até ',$distanceKm);
            $eventDeliver->setStartKm((double)$kmArray[0]);
            $eventDeliver->setEndKm((double)$kmArray[1]);

            $this->getEntityManager()->persist($eventDeliver);
            $this->getEntityManager()->flush();
            return $eventDeliver->toArray();
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function deleteRaioKm($storeId, $id, $nrorg) {
        try {
            $data  = $this->getEntityManager()->getRepository(EvtEventDeliver::class)->find($id);
            if ($data == NULL) return ['error', 'EvtEventDeliver not found'];
            $this->getEntityManager()->remove($data);
            $this->getEntityManager()->flush();
            return ['success', $id];
        } catch (\Exception $e) {
            return ['error',$e->getMessage()];
        }
    }
    
}