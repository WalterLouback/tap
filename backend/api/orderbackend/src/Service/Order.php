<?php
namespace Zeedhi\ApiOrders\Service;

use Zeedhi\ApiOrders\Model\Entities\OrdOption;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdConfigStore;
use Zeedhi\ApiOrders\Model\Entities\OrdWorkflow;
use Zeedhi\ApiOrders\Model\Entities\OrdOrder;
use Zeedhi\ApiOrders\Model\Entities\OrdOrderRO;
use Zeedhi\ApiOrders\Model\Entities\OrdOrderStatusRel;
use Zeedhi\ApiOrders\Model\Entities\OrdItemExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdMenu;
use Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdProductGroup;
use Zeedhi\ApiOrders\Model\Entities\EvtEventMenu;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiOrders\Model\Entities\GenStatus;
use Zeedhi\ApiOrders\Model\Entities\GenUser;
use Zeedhi\ApiOrders\Model\Entities\GenDependent;
use Zeedhi\ApiOrders\Model\Entities\GenStructure;
use Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller;
use Zeedhi\ApiOrders\Model\Entities\PayCreditcard;
use Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod;
use Zeedhi\ApiServices\Model\Entities\SerService;
use Zeedhi\ApiOrders\Model\Entities\GenStatusUserType;
use Zeedhi\ApiGeneral\Model\Entities\TokenAuthApi;
use Zeedhi\ApiOrders\Model\Entities\OrdStorePayMethod;
use Zeedhi\ApiOrders\Model\Entities\GenConfiguration;
use Zeedhi\ApiOrders\Model\Entities\GenOrganizationUserRel;
use Zeedhi\ApiOrders\Model\Entities\OrdLockOpening;

use Zeedhi\ApiOrders\Helpers\DateFormatDql;

use Zeedhi\ApiOrders\Service\Notification;

use Zeedhi\ApiOrders\Helpers\FCM;

use Zeedhi\ApiPayment\Controller\Payment;
//From ApiGeneral
use Zeedhi\ApiGeneral\Model\Entities\GenUser as GenUserApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenContact as GenContactApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenAddress as GenAddressApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\EvtEvent as EvtEventApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration as GenConfigurationApiGeneral;

use Doctrine\ORM\EntityManager;

class Order extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment, Payment $payment, Notification $notification) {
        parent::__construct($entityManager, $environment);
        $this->paymentApi   = $payment;
        $this->notification = $notification;
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $cardId = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard()->getId();
        $today  = new \DateTime();
        $firstDayOfMonth   = $today->format('Y-m-d');
        $total = $this->getEntityManager()->createQuery(
        "
        SELECT SUM(o.total) as totalValue
        FROM 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO' o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createdAt >= '$firstDayOfMonth'
        "
        )->getResult()[0]['totalValue'];
        
        return $total;
    }
    
    
    function verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $orderValue, $nrorg) {
        if ($paymentMethod != OrdOrder::PAYMENT_METHOD_CREDITCARD || $orderValue == 0) return ;
        
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg]);
        
        if ($dependency == NULL) return ;
        
        $parent  = $dependency->getParent();
        
        if ($parent == NULL) return ;
        
        if ($parent->getMainCreditcard() == NULL) return ;
        
        if ($parent->getMainCreditcard()->getId() != $creditcardId) return ;
        
        $limit       = $dependency->getMonthlyLimit();
        $totalExpent = $this->getMonthExpensesFromDependent($parent->getId(), $userId, $nrorg);
        $canExpend   = $limit - $totalExpent;

        if ($canExpend < $orderValue) throw new Exception("User's monthly limit has exceeded! Limit is $limit, current is $totalExpent", 12);
    }
    
    
    public function getOpenALock($lockID,$status, $nrorg){ 
        $ordLockOpening = new OrdLockOpening();
        $result=$ordLockOpening->getOpenALock($this->getEntityManager(),$lockID,$status, $nrorg);
        
        if ($result){
            $ordLockOpening->updateOpenLock($this->getEntityManager(),$lockID, $status, $nrorg);
            return $result;
        }else {
            return $result;
        }
    }
    
    public function createGenLock($lockID, $nrorg, $status, $ordOrder, $doorNumber, $createBy){ 
        $ordLockOpening = new OrdLockOpening();
        
        $result=$ordLockOpening->createGenLock($this->getEntityManager(),$lockID, $nrorg, $status, $ordOrder, $doorNumber, $createBy);
        return $result;
       
    }

    
    
    /* Creates new Order with the passed data */
    function createOrder($paymentMethod, $statusId, $total, $deliverTo, $note, $userId, $eventId, $nrorg, $creditcardId, $structureId, $canFlush=true, $deliveryAddress = null, $convenienceFee = null, $deliveryFee = null, $fromTaa) {
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $total, $nrorg);
        
        /* Getting necessary entities to set to the order */
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        
        $initialStatus  = $this->getInitialStatus($eventId, $nrorg);
        
        if ($user == NULL) {
            throw new Exception('User not found!');
        }
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setDeliverTo($deliverTo);
        $order->setStatus($initialStatus);
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEvent($event);
        $order->setNote($note);
        $order->setConvenienceFee($convenienceFee);
        $order->setDeliveryFee($deliveryFee);
        $order->setType('ORDER_STORE');
        $order->setNrorg($nrorg);
        $order->setPaymentStatus('P');
        $order->setFromTaa($fromTaa);
        if ($structureId  !== NULL) {
            $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
            $order->setGenStructure($structure);
        }
        if ($creditcardId !== NULL) {
            $creditcard     = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId);
            $order->setPayCreditcard($creditcard);
        }
        $order->setDeliveryAddress($deliveryAddress);
        //$order->setCreateDate(new \DateTime());
        
        //$order->getPaymentMoment()->getId();
        /* Storing order in database */
        $this->getEntityManager()->persist($order);

        if ($canFlush) $this->getEntityManager()->flush();
        
        $this->saveOrderStatus(new \DateTime(), $order->getId(), NULL, "UPDATING_STATUS");
        return $order;
    }
    
    /* Creates new Order with the passed data */
    function createOrderWithExternalId($statusId, $total, $deliverTo, $note, $sellerId, $eventId, $nrorg, $structureId, $clientNPF, $canFlush=true, $deliveryAddress, $fromTaa) {
        $userTypeRel = $this->getEntityManager()->getRepository(GenUserTypeRel::class)->findOneBy(['externalId' => $clientNPF]);
        if ($userTypeRel == NULL) {
            throw new Exception('User not found!',19);
        }
        
        $user = $userTypeRel->getGenUser();
        
        /* Getting necessary entities to set to the order */
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $seller         = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $user, 'evtEvent' => $event]);
        
        $initialStatus  = $this->getInitialStatus($eventId, $nrorg);
        
        if ($user == NULL) {
            throw new Exception('User not found!',19);
        }
        if ($user->getMainCreditcard() == NULL) {
            throw new Exception('User\'s main card not found', 20);
        }
        
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($user->getId(), OrdOrder::PAYMENT_METHOD_CREDITCARD, $user->getMainCreditcard()->getId(), $total, $nrorg);
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(OrdOrder::PAYMENT_METHOD_CREDITCARD);
        $order->setDeliverTo($deliverTo);
        $order->setStatus($initialStatus);
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEvent($event);
        $order->setEvtEventSeller($seller);
        $order->setNote($note);
        $order->setType('ORDER_STORE');
        $order->setNrorg($nrorg);
        $order->setPayCreditcard($user->getMainCreditcard());
        $order->setDeliveryAddress($deliveryAddress);
        $order->setFromTaa($fromTaa);
        if ($structureId  !== NULL) {
            $structure      = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
            $order->setGenStructure($structure);
        }
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function getInitialStatus($storeId, $nrorg) {
        /* Check if store has workflow configured */
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore !== NULL ? $configStore->getWorkflow() : NULL;
        if ($workflow != NULL) {
            return $workflow->getInitialStatus();
        }
        /* If not, get organization's default workflow */
        else {
            return $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg])->getInitialStatus();
        }
        return NULL;
    }
    
    function getPaymentMoment($storeId, $nrorg) {
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore !== NULL ? $configStore->getWorkflow() : NULL;
        if ($workflow != NULL) {
            return $workflow->getPaymentMoment();
        } else {
            return $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $nrorg])->getPaymentMoment();
        }
        
        return NULL;
    }
    
    /* Creates ProductOrders with the received items */
    function createProductOrders($order, $orderItems, $removeAmount=false, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($orderItems as $item) {
            /* Getting the product referent to the item */
            $product = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($item['productId']);
            if ($removeAmount && $product->getAmount() != NULL) $product->setAmount($product->getAmount() - $item['quant']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setQuantity($item['quant']);
            $po->setTotal($item["price"] * $item['quant']);
            $po->setPrice($item["price"]);
            $po->setOrdMenuProduct($product);
            $po->setOrdOrder($order);
            if(isset($item['note'])) {
                $po->setNote($item['note']);
            }
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            /* In case any extra is associated to the item */
            if (isset($item['extras'])) {
                $this->createItemExtras($product, $po, $item['extras']);
            }
            
            array_push($productOrders, $po);
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $productOrders;
    }
    
    /* Creates extras and options of a product */
    function createItemExtras($product, $productOrder, $itemExtras) {
        $extrasTotalPrice = 0;

        /* Iterating through the extras of the item sent by the front-end */
        foreach ($itemExtras as $e) {
            /* Getting Extra to set to ItemExtra */
            $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($e['extraId']);
            
            /* Instantiating ItemExtra and setting attributes */
            $itemExtra = new OrdItemExtra();
            $itemExtra->setOrdOrderProduct($productOrder);
            $itemExtra->setOrdExtra($extra);
            
            /* Storing ItemExtra in the database */
            $this->getEntityManager()->persist($itemExtra);
            
            /* Iterating through the selected options of this ItemExtra */
            foreach ($e['selectedOptions'] as $o) {
                /* Getting Option to set to SelectedOption */
                $option = $this->getEntityManager()->getRepository(OrdOption::class)->find($o['id']);
                
                /* Instantiating SelectedOption and setting attributes */
                $selectedOption = new OrdSelectedOption();
                $selectedOption->setOrdOption($option);
                $selectedOption->setOrdItemExtra($itemExtra);
                
                /* Storing SelectedOption in database */
                $this->getEntityManager()->persist($selectedOption);
            }
        }


        $productOrder->setTotal($productOrder->getTotal() + ($extrasTotalPrice * $productOrder->getQuantity()));
    }
    
    /* Gets an user's orders in an organization */
    function getOrdersFromUser($userId, $structureId, $storeId, $nrorg, $initial=NULL, $max=NULL) {
        $order   = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO';
        $nrorgQuery = $nrorg !== NULL ? "AND o.nrorg = $nrorg" :  "";
        $storeQuery = $storeId !== NULL ? "AND s.id = $storeId" : "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            WHERE o.genUser = $userId
            AND o.type = 'ORDER_STORE' 
            $nrorgQuery
            $storeQuery
            AND ( o.paymentStatus = 'A' OR o.paymentStatus = 'P' OR o.paymentStatus = 'R')
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets a store's orders */
    function getOrdersFromStore($storeId, $userId, $initial=NULL, $max=NULL, $initialDate=NULL, $initialTime=NULL, $finalDate=NULL, $finalTime=NULL, $cashier=NULL ) {
        $order          = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO';
        $item           = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct';
        $product        = 'Zeedhi\ApiOrders\Model\Entities\OrdProduct';
        $extra          = 'Zeedhi\ApiOrders\Model\Entities\OrdExtra';
        $option         = 'Zeedhi\ApiOrders\Model\Entities\OrdOption';
        $user           = 'Zeedhi\ApiOrders\Model\Entities\GenUser';
        $userType       = 'Zeedhi\ApiOrders\Model\Entities\GenUserType';
        $eventSeller    = 'Zeedhi\ApiOrders\Model\Entities\EvtEventSeller';
        $statusUserType = 'Zeedhi\ApiOrders\Model\Entities\GenStatusUserType';
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat   = $this->stringToDate($finalDate);
        
        $initialDateQuery = "";
        if ($initialDate !== NULL) {
            $initialDateQuery = "AND o.createdAt >= '$initialDateFormat";
            $initialDateQuery .= $initialTime ? " $initialTime:00'" : " 00:00:00'";
        }
        
        $finalDateQuery = "";
        if ($finalDate !== NULL) {
            $finalDateQuery = "AND o.createdAt <= '$finalDateFormat";
            $finalDateQuery .= $finalTime ? " $finalTime:59'" : " 23:59:59'";
        }
        $cashierQuery = $cashier ? "AND c.id = $cashier" : "";
        

        /* Buscando pedidos (o) do tipo ORDER_STORE realizados nesta loja (s)
           cujo status (st) possa ser visualizado por esse usuário ($userId)
           com determinado sellerType (seltype) nesta loja. */
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o, st
            FROM $order o
            LEFT JOIN o.evtEvent s 
            LEFT JOIN o.status st
            LEFT JOIN $eventSeller es WITH ( es.genUser = $userId and es.evtEvent = s )
            LEFT JOIN es.evtSellerType seltype
            LEFT JOIN $statusUserType sut WITH sut.genStatus = st.id
            LEFT JOIN sut.evtSellerType seltype_2
            LEFT JOIN o.ordCashier c
            WHERE o.evtEvent = $storeId
            $initialDateQuery
            $finalDateQuery
            $cashierQuery
            AND ( s.type = 'E' OR seltype.id = seltype_2.id )
            AND ( st.next is not null or st.id is null )
            ORDER BY st.id, o.createdAt
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets a store's orders */
    function getOrdersFromEvent($storeId, $userId, $ordering="desc", $initial=NULL, $max=NULL, $initialDate=NULL, $initialTime=NULL, $finalDate=NULL, $finalTime=NULL, $cashier=NULL ) {
        $order          = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO';
        $item           = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct';
        $product        = 'Zeedhi\ApiOrders\Model\Entities\OrdProduct';
        $extra          = 'Zeedhi\ApiOrders\Model\Entities\OrdExtra';
        $option         = 'Zeedhi\ApiOrders\Model\Entities\OrdOption';
        $user           = 'Zeedhi\ApiOrders\Model\Entities\GenUser';
        $userType       = 'Zeedhi\ApiOrders\Model\Entities\GenUserType';
        $eventSeller    = 'Zeedhi\ApiOrders\Model\Entities\EvtEventSeller';
        $statusUserType = 'Zeedhi\ApiOrders\Model\Entities\GenStatusUserType';
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat   = $this->stringToDate($finalDate);
        
        $initialDateQuery = "";
        if ($initialDate !== NULL) {
            $initialDateQuery = "AND o.createDate >= '$initialDateFormat";
            $initialDateQuery .= $initialTime ? " $initialTime:00'" : " 00:00:00'";
        }
        
        $finalDateQuery = "";
        if ($finalDate !== NULL) {
            $finalDateQuery = "AND o.createDate <= '$finalDateFormat";
            $finalDateQuery .= $finalTime ? " $finalTime:59'" : " 23:59:59'";
        }
        $cashierQuery = $cashier ? "AND c.id = $cashier" : "";

        /* Buscando pedidos (o) do tipo ORDER_STORE realizados nesta loja (s)
           cujo status (st) possa ser visualizado por esse usuário ($userId)
           com determinado sellerType (seltype) nesta loja. */
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o, st
            FROM $order o
            LEFT JOIN o.evtEvent s 
            LEFT JOIN o.status st
            LEFT JOIN $eventSeller es WITH ( es.genUser = $userId and es.evtEvent = s )
            LEFT JOIN es.evtSellerType seltype
            LEFT JOIN $statusUserType sut WITH sut.genStatus = st.id
            LEFT JOIN sut.evtSellerType seltype_2
            LEFT JOIN o.ordCashier c
            WHERE o.evtEvent = $storeId
            $initialDateQuery
            $finalDateQuery
            $cashierQuery
            AND o.paymentStatus = 'A'
            ORDER BY o.createDate $ordering
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    /* Gets the informations related to an order */
    function getOrderData($orderId, $userId) {
        if (is_integer($orderId) || is_numeric($orderId)) {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy(['orderIdentifier' => $orderId]);
        } else {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->find($orderId);
        }
        if ($order !== NULL) $order->build($this->getEntityManager());
        return $order;
    }
    
    /* Checks how to procede with the payment of an order and after that changes its status */
    function makePaymentTransaction($order) {
        if ($order->getPaymentStatus() == 'A') return ;
        
        $paymentMethod = $order->getPaymentMethod();
        $creditCardId  = $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL;
        
        $requiresPaymentMethod = $this->requiresPaymentApi($paymentMethod);
        $isPaymentMoment = $this->isPaymentMoment($order->getEvtEvent()->getId(), $order->getStatus()->getId(), $order->getNrorg());

        /* If this payment method is managed by the system */
        if ($requiresPaymentMethod) {
            /* If it's the moment to make the payment, call Payment API */
            if ($isPaymentMoment) {
                /* Send the order data to the payment API */
                $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
                $orderId    = $order->getId();
                $value      = $order->getTotal();
                $ccToken    = $creditCard->getToken();
                $customerId = $creditCard->getCustomerId();
                $gateway    = $creditCard->getGateway();
                $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
                $merchant   = $order->getEvtEvent()->getMerchantKey();
                $merchantId = $order->getEvtEvent()->getMerchantId();
                
                $payment    = $this->paymentApi->execPaymentCreditCard($merchant, $merchantId, $gateway, $order);
                
                /*salvando historico de transações*/
                $this->saveOrderStatus(new \DateTime(), $order->getId(), $payment['transaction_id'], OrdOrder::TYPE_TRANSACTION);
                
                /* Getting data from the payment */
                $status     = $payment['status'];
                $order->setTransactionId($payment['transaction_id']);
                
                /* Set the payment status */
                $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
                
                /* If the payment was not successful */
                if ($status !== 'captured') {
                    throw new Exception('Payment not successful. Got status \'' . $status . '\'', 7);
                }
            } else {
                /* If it's not the moment to make the payment, set status pendent  */
                $order->setPaymentStatus('P');
            }
        } else if ($isPaymentMoment) {
            /* If the payment will not be managed by the system, set status Ok */
            $order->setPaymentStatus('A');
        } 
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
    } 
    
    /* Checks if it's necessary to call the payment API for this payment method */
    function requiresPaymentApi($paymentMethod) {
        return $paymentMethod == OrdOrder::PAYMENT_METHOD_CREDITCARD || $paymentMethod == OrdOrder::PAYMENT_METHOD_BALANCE;
    }

    /* Checks the store parametrization and returns if the payment should happen at this status */
    function isPaymentMoment($eventId, $status, $nrorg) {
        $isPaymentMoment = $this->getPaymentMoment($eventId, $nrorg)->getId() == $status;

        return $isPaymentMoment;
    }
    
    function setOrderToNextStatus($orderId, $paymentMethod=null, $clientId=null) {
        $order         = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy([ 'orderIdentifier' => $orderId ]);
        $user          = $this->getUserByIdAndNrorg($order->getGenUser()->getId(), $order->getNrorg());
        $currentStatus = $order->getStatus();
        $nextStatus    = $currentStatus->getNext();
        
        if ($nextStatus != NULL) {
            $order->setStatus($nextStatus);
            
            $storeName  = $order->getEvtEvent()->getName();
            $statusName = $nextStatus->getLabelName();
            
            try {
                $this->makePaymentTransaction($order);
                $this->getEntityManager()->flush();
                $message    = $nextStatus->getNext() != NULL ? "Sua compra em $storeName obteve status $statusName." : "Sua compra em $storeName foi concluída.";
            } catch (Exception $e) {
                var_dump($e->getMessage());
                $message    = "Sua compra em $storeName foi cancelada. Transação não autorizada pela instituição financeira.";
                $cancelStatus = $this->getEntityManager()->getRepository(GenStatus::class)->find(7);
                $order->setStatus($cancelStatus);
                $this->getEntityManager()->flush();
            }
            
            $notificationData = ['orderId' => $orderId];
            
            $title = "Sobre sua compra em $storeName";
            $originNotification = "Order_Orderbackend";
            $status             = "P";
            $dateTime           = new \DateTime();
            
            if ($user !== NULL) {
                try {
                    $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user->getId(), $originNotification, $dateTime, $notificationData, $title, $user->getFirebaseToken());
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
            
            $this->saveOrderStatus($dateTime, $order->getId(), NULL, "UPDATING_STATUS");
            
            /*Verifica se o valor da order mudou para estornar um pagamento e realizar outro.*/
            $this->checkForChangesInTheOrder($order);
            
            return $order->getStatus()->getId();
        }
    }
    
    function cancelOrder($orderId, $cancellationReason = NULL) {
        if (is_integer($orderId)) {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->findOneBy(['orderIdentifier' => $orderId]);
        } else {
            $order = $this->getEntityManager()->getRepository(OrdOrderRO::class)->find($orderId);
        }
        if ($order == NULL) return ;
        $cancelStatus = $this->getEntityManager()->getRepository(GenStatus::class)->find(7);
        
        /* If the payment was made via credit card, exec revoke */
        if ($order->getPaymentStatus() == 'A' && $order->getPaymentMethod() == OrdOrder::PAYMENT_METHOD_CREDITCARD) {
            /* Getting the gateway and the merchant key from the store */
            $creditCard = $order->getPayCreditcard();
            $gateway    = $creditCard->getGateway();
            $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
            $merchant   = $order->getEvtEvent()->getMerchantKey();
            $merchantId = $order->getEvtEvent()->getMerchantId();
            
            /* Executing revoke */
            try {
                $revokeStatus = $this->paymentApi->revoke($order->getTransactionId(), $order->getTotal(), $gateway, $merchant, $merchantId);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                throw new Exception('Unable to execute revoke', 11);
            }
            if ($revokeStatus['success'] != true) {
                throw new Exception('Unable to execute revoke', 11);
            }
            
            $this->createRevokeOrder($order);
        }
        
        /* Set the cancelled status */
        $order->setStatus($cancelStatus);
        
        /* If the order was a balance purchase, remove balance */
        if ($order->getType() == OrdOrderRO::PAYMENT_TYPE_WALLET) {
            if ($this->startsWith($order->getPaymentMethod(), 'CC_W') || !$this->startsWith($order->getPaymentMethod(), 'BALANCE')) {
                $wallet = $order->getWallet();
                $balance = $wallet->getBalance();
                if ($balance >= $order->getTotal()) $wallet->setBalance($balance - $order->getTotal());
                else $wallet->setBalance(0);
            }
        }
        
        /* If the order was paid with balance, return the balance to the wallet */
        if ($this->startsWith($order->getPaymentMethod(), 'BALANCE')) {
            $wallet = $order->getWallet();
            if ($wallet) $wallet->setBalance($wallet->getBalance() + $order->getTotal());
        }
        
        /* If the order was paid with picpay, execute revoke */
        if ($this->startsWith($order->getPaymentMethod(), 'PICPAY') && $order->getPaymentStatus() == 'A') {
            $referenceId = $order->getId();
            $pm = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $order->getPaymentMethod()]);
            $picpayTransaction = $this->makeCurl("https://appws.picpay.com/ecommerce/public/payments/$referenceId/status", 'GET', [], $pm->getXPicpayToken(), $pm->getXSellerToken());
            
            if (!isset($picpayTransaction['authorizationId']) || !isset($picpayTransaction['status']))
                throw new \Exception('Error while executing picpay revoke', 11);
            if ($picpayTransaction['status'] == 'refunded')
                throw new \Exception('Order already refunded', 11);
            
            $cancellationResult = $this->makeCurl("https://appws.picpay.com/ecommerce/public/payments/$referenceId/cancellations", 'POST', array('authorizationId' => $picpayTransaction['authorizationId']), $pm->getXPicpayToken(), $pm->getXSellerToken());
            
            if (!isset($cancellationResult['cancellationId']))
                throw new \Exception('Error while executing picpay revoke', 11);
        }
        
        $this->getEntityManager()->flush();
        
        $storeName  = $order->getEvtEvent()->getName();
        $user       = $this->getUserByIdAndNrorg($order->getGenUser()->getId(), $order->getNrorg());
        
        if(empty($cancellationReason) || $cancellationReason['id'] == 3) {
            $message    = "Sua compra em $storeName foi cancelada.";
        }else {
            $requestCancel = $cancellationReason['message'];
            $message   = "Infelizmente a loja $storeName teve que cancelar sua compra pelo motivo $requestCancel";
        }

        // $itemR =  $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findby(['ordOrder' => $orderId]);
        // foreach ($itemR as $e) {
        //     $product = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->findOneBy(['id'=>$e->getOrdMenuProduct()->getId()]);
        //     if($product->getAmount()!==null){
        //         $product->setAmount($product->getAmount()+$e->getQuantity());
        //         $this->getEntityManager()->persist($product);
        //         $this->getEntityManager()->flush();
           
        //      }
        //      }

        $notificationData = ['orderId' => $orderId];
        
        $title = "Sobre sua compra em $storeName";
        $originNotification = "Order_Orderbackend";
        $status             = "P";
        $dateTime           = new \DateTime();
        
        if ($user !== NULL) {
            try {
                $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user[0]["userId"], $originNotification, $dateTime, $notificationData, $title, $user[0]["firebaseToken"]);
            } catch (\Exception $e) { }
        }

        $this->saveOrderStatus($dateTime, $order->getId(), NULL, "UPDATING_STATUS");
    }
    public function getOrderId(){
    $item           = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct';

         $id = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $item o
            WHERE o. = $nrorg
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            ORDER BY o.orderIdentifier desc
            "
        );
    }
    
    /** 
     * Essa função serve para salvar historico de alterações de uma OrdOrder:class
     * @access public
     * @param DateTime $dateTime
     * @param String $orderId deve ser o orderIdentifier de uma Order
     * @param String $transactionId numero da trasação feita na operadora de cartão de credito
     * @param String $type = PAYMENT OR OUTHER
     * @return Object OrdOrderStatusRel
     */
    public function saveOrderStatus($dateTime, $orderId, $transactionId = NULL, $type = NULL) {
        $orderStatus = new OrdOrderStatusRel();
        $order       = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['id' => $orderId]);
        
        $orderStatus->setNrorg($order->getNrorg());
        $orderStatus->setCreateDate($dateTime);
        $orderStatus->setOrdOrder($order);
        $orderStatus->setGenStatus($order->getStatus());
        
        
        if($transactionId) { 
            $orderStatus->setTransactionId($transactionId);
            
            if($type == "CANCELLATION"){
                $ordOrderStatusRel  = $this->getEntityManager()->getRepository(OrdOrderStatusRel::class)->findOneBy(['transactionId' => $transactionId]);
                $orderStatus->setTotal($ordOrderStatusRel->getTotal());
            } else {
                $orderStatus->setTotal($order->getTotal());
            }
        }
        
        if($type) { $orderStatus->setType($type); }
        
        $this->getEntityManager()->persist($orderStatus);
        $this->getEntityManager()->flush();
        
        return $orderStatus;
    }
    
    public function getHistoryStatusOrder($nrorg, $orderId) {
        $order       = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['orderIdentifier' => $orderId]);
        $orderHistory = $this->getEntityManager()->getRepository(OrdOrderStatusRel::class)->findBy(['ordOrder' => $order->getId(), 'nrorg' => $nrorg]);
    
        return $orderHistory;
     }
    
    public function makeCurl($url, $method, $body, $xPicpayToken, $xSellerToken) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "x-picpay-token: $xPicpayToken",
            "x-seller-token: $xSellerToken"
        ));
        // Will be a POST
        if ($method === "POST") curl_setopt($ch, CURLOPT_POST, 1);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($body) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        return json_decode($result, true);
    }
    
    private function createRevokeOrder($originalOrder) {
        $cancelStatus = $this->getEntityManager()->getRepository(GenStatus::class)->find(7);
        
        $revokeOrder = new OrdOrder();
        $revokeOrder->setId(OrdOrder::getGUID());
        $revokeOrder->setPaymentMethod($originalOrder->getPaymentMethod());
        $revokeOrder->setDeliverTo($originalOrder->getDeliverTo());
        $revokeOrder->setStatus($cancelStatus);
        $revokeOrder->setTotal($originalOrder->getTotal());
        $revokeOrder->setGenUser($originalOrder->getGenUser());
        $revokeOrder->setEvtEvent($originalOrder->getEvtEvent());
        $revokeOrder->setNote($originalOrder->getNote());
        $revokeOrder->setType('ORDER_STORE');
        $revokeOrder->setNrorg($originalOrder->getNrorg());
        $revokeOrder->setGenStructure($originalOrder->getGenStructure());
        $revokeOrder->setPayCreditcard($originalOrder->getPayCreditcard());
        $revokeOrder->setPaymentStatus('R');
        $revokeOrder->setFromTaa($originalOrder->getFromTaa());
        /* Storing revoke order in database */
        $this->getEntityManager()->persist($revokeOrder);
        $this->getEntityManager()->flush();
        
        return $revokeOrder;
    }
    
    function getOrdWorkflow($storeId) {
        /* Check if store has workflow configured */
        $configStore = $this->getEntityManager()->getRepository(OrdConfigStore::class)->findOneBy(['event' => $storeId]);
        $workflow    = $configStore->getWorkflow();
        
        if ($workflow == NULL) {
            $workflow = $this->getEntityManager()->getRepository(OrdWorkflow::class)->findOneBy(['nrorg' => $configStore->getNrorg()]);
        }
        
        return $workflow;
    }
    
    function getOrdersFromUserFilter($storeId, $userId, $nrorg, $initialDate=NULL, $max=NULL, $finalDate=NULL, $statusIds=NULL) {
        $order          = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO';
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);
        
        $status = join($statusIds, "' OR st.labelName = '");
        $statusQuery = $statusIds !== NULL ? "AND (st.labelName = '$status')" : "";

        $storeQuery       = $storeId !== NULL ? "AND s.id  = $storeId" : "";
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            JOIN o.status st
            WHERE o.nrorg = $nrorg
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    function getOrdersReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods, $orderTypes, $sellerIds, $itemsPerPage = 100, $page = 1, $deliveryAddress, $fromTaa) {
        $order = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO';
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);
        
        $typesOrder = "";
        if (!is_null($orderTypes) && count($orderTypes) > 0) {
            $typesOrder = "AND (";
            foreach ($orderTypes as $orderType) {
                if ($typesOrder != "AND (") $typesOrder .= " or ";
                switch ($orderType) {
                    case 'PRODUCTS':
                        $typesOrder .= "(o.type = 'ORDER_STORE' or o.type = 'ORDER_EVENT' or o.type = 'ORDER_EVENT_SELLER' or (o.type = 'WALLET' AND o.paymentMethod = 'BALANCE'))";
                        break;
                    case 'BALANCE_PURCHASE':
                        $typesOrder .= "(o.type = 'WALLET' AND o.paymentMethod <> 'BALANCE')";
                        break;
                    case 'WALLET_TRANSFER':
                        $typesOrder .= "(o.type = 'WALLET_TRANSFER')";
                        break;
                    case 'SERVICES':
                        $typesOrder .= "(o.type = 'ORDER_SERVICE')";
                        break;
                    case 'TICKETS':
                        $typesOrder .= "(o.type = 'TICKET')";
                        break;
                }
            }
            $typesOrder .= ")";
        }
        
        if (!is_null($orderStatus))
            $statusOrder = join($orderStatus, ' OR st.id = ');
        $statusQuery = $orderStatus !== NULL ? "AND (st.id = $statusOrder)" : "AND (st.id is null or st.id <> 7)";
        
        if (!is_null($paymentStatus))
            $statusPayment = "'". join($paymentStatus, "' OR o.paymentStatus = '") . "'";
        $paymentStatusQuery = $paymentStatus !== NULL ? "AND (o.paymentStatus = $statusPayment)" : ""; 
        
        $paymentMethodQuery = "";
        if(!is_null($paymentMethods) && count($paymentMethods) == 1) {
            $methodPayment = $paymentMethods[0] == 'E' ? "o.paymentMethod like 'EXT%' " : "o.paymentMethod not like 'EXT%' ";
            $paymentMethodQuery = " AND $methodPayment";
        } 
        
        if (!is_null($paymentMethods))
            $paymentMethodsJoin = "'". join($paymentMethods, "' OR o.paymentMethod like '") . "'";
        $paymentMethodsQuery = $paymentMethods !== NULL ? "AND (o.paymentMethod like $paymentMethodsJoin)" : ""; 
            
        if (!is_null($storeId))
            $storeIds = join($storeId, ' OR s.id = ');
        $storeQuery = $storeId !== NULL ? "AND (s.id = $storeIds)" : "";     
        
        if (!is_null($sellerIds))
            $sellerIdsJoin = join($sellerIds, ' OR es.genUser = ');
        $sellerQuery = $sellerIds !== NULL ? "AND (es.genUser = $sellerIdsJoin)" : "";     
        
        if (!is_null($structureId))
            $structureIds = join($structureId, ' OR sr.id = ');
        $structureQuery = $structureId !== NULL ? "AND (sr.id = $structureIds)" : "";  
        $fromTaaQuery = $fromTaa !== NULL ? "AND (o.fromTaa = '$fromTaa')" : "";   
        $deliverToQuery = $deliverTo !== NULL ? "AND (o.deliverTo = '$deliverTo')" : "";
        $deliveryAddressQuery = $deliveryAddress !== NULL ? "AND (o.deliveryAddress = '$deliveryAddress')" : "";
        
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            LEFT JOIN o.evtEvent s
            LEFT JOIN o.status st
            LEFT JOIN o.genStructure sr
            LEFT JOIN o.evtEventSeller es
            WHERE o.nrorg = $nrorg
            AND o.paymentStatus <> 'R'
            $structureQuery
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            $paymentMethodsQuery
            $paymentStatusQuery
            $deliverToQuery
            $deliveryAddressQuery
            $typesOrder
            $sellerQuery
            $fromTaaQuery
            ORDER BY o.createdAt DESC
            "
        ); 
        
        if($page){
            $orders->setFirstResult($itemsPerPage * ($page - 1));
        }else {
            $orders->setFirstResult(0);
        }
        
        if ($itemsPerPage != NULL) $orders->setMaxResults($itemsPerPage);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
    function getStatus($labelName) {
        $status = $this->getEntityManager()->getRepository(GenStatus::class)->findOneBy(['labelName' => $labelName]);
        
        return $status;
    }
    
    function getOrdersFromUserInProgress($storeId, $userId, $nrorg, $initialDate=NULL, $max=NULL, $finalDate=NULL) {
        $order              = OrdOrderRO::class;
        
        $initialDateFormat  = $this->stringToDate($initialDate);
        $finalDateFormat    = $this->stringToDate($finalDate);
        
        $storeQuery       = $storeId        !== NULL ? "AND s.id  = $storeId" : "";
        $initialDateQuery = $initialDate    !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate      !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent s
            JOIN o.status st
            WHERE o.nrorg = $nrorg AND o.genUser = $userId AND st.next is not null AND o.type = 'ORDER_STORE' 
            $storeQuery
            $initialDateQuery
            $finalDateQuery
            ORDER BY o.orderIdentifier desc
            "
        );
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    private function startsWith ($string, $startString) { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    /* Creates new Order with the passed data */
    function createOrderService($paymentMethod=null, $total, $note, $clientId, $eventId, $nrorg, $creditCardId=null, $canFlush=true, $orderSheet, $sellerId) {
        /* Getting necessary entities to set to the order */
        $seller         = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(["id" => $sellerId]);
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($clientId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        
        if ($user == NULL) {
            throw new Exception('User not found!');
        }
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        
        $order->setId(OrdOrder::getGUID());
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEventSeller($seller);
        $order->setEvtEvent($event);
        $order->setNote($note);
        $order->setType('ORDER_SERVICE');
        $order->setPaymentStatus('P');
        $order->setNrorg($nrorg);
        $order->setOrderSheet($orderSheet);
        $order->setCreatedBy($seller->getGenUser() ->getId());
        $order->setGenStructure($this->getEntityManager()->getReference(GenStructure::class, $event->getStructure()->getId()));
        $order->setStatus($this->getEntityManager()->getReference(GenStatus::class, 31));
        
        $paymentMethod  !== NULL ? $order->setPaymentMethod($paymentMethod) : NULL;
        $creditCardId   !== NULL ? $order->setPayCreditcard($this->getEntityManager()->getReference(PayCreditcard::class, $creditCardId)) : NULL;
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function addPaymentMethodOrderService($orderId, $paymentMethod, $clientId, $canFlush=true) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(["orderIdentifier" => $orderId, "paymentStatus" => "P"]);
        
        if($order == NULL) {
            throw new Exception('Order not found!',5);
        }
        
        if($paymentMethod == "CC") {
            $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->findOneBy(["genUser" => $clientId, "status" => "A"]);
            
            if($creditCard == NULL) {
                throw new Exception('Inactive card!',53125);
            }
            $order->setPayCreditcard($this->getEntityManager()->getReference(PayCreditcard::class, $creditCard->getId()));
        }
        
        $order->setPaymentMethod($paymentMethod);
        
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    function addItemsToOrderSheet($userId, $eventId, $orderId, $products, $totalValue) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->find($orderId);
        
        foreach ($products as $product) {
            $po = new OrdOrderProduct();
            $po->setQuantity($product['quant']);
            $po->setPrice($product['price']);
            $po->setTotal($product['totalItem']);
            $po->setOrdOrder($order);
            $po->setOrdMenuProduct($this->getEntityManager()->getReference(OrdMenuProduct::class, $product['productId']));
            if (isset($product['note'])) $po->setNote($product['note']);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
        }
        
        $order->setTotal($order->getTotal() + $totalValue);
        
        $this->getEntityManager()->flush();
        $order->build($this->getEntityManager());
        return $order;
    }
    
    /* associate service a order */
    function associateServicesOrder($order, $services, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($services as $service) {
            /* Getting the product referent to the item */
            $serService = $this->getEntityManager()->getRepository(SerService::class)->find($service['ID']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setQuantity($service['QUANT']);
            $po->setNote($serService->getName());
            $po->setTotal($serService->getPrice() * $service['QUANT']);
            $po->setSerService($serService);
            $po->setOrdOrder($order);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            array_push($productOrders, $po);
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $productOrders;
    }
    
    function getOrdersFromServicesByDay($eventId, $userId, $dateTime, $initial=NULL, $max=NULL) {
        $order          = OrdOrder::class;
        $item           = OrdOrderProduct::class;
        $user           = GenUser::class;
        $userType       = GenUserType::class;
        $eventSeller    = EvtEventSeller::class;
        $statusUserType = GenStatusUserType::class;
        
        $date           = $this->stringToDate($dateTime);
        
        $orders  = $this->getEntityManager()->createQuery(
            "
            SELECT o FROM $order o 
            LEFT JOIN o.evtEvent s 
            JOIN o.status st
            JOIN $statusUserType sut WITH sut.genStatus = st.id
            WHERE o.evtEvent = $eventId 
            AND o.type = 'ORDER_SERVICE' AND o.paymentStatus = 'P'
            AND (o.createdAt BETWEEN '$date 00:00:00' AND '$date 23:59:59')
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    function getOrderByOrderSheet($userId, $orderSheet, $nrorg) {
        $ordOrder = OrdOrder::class;
        
        $today  = new \DateTime();
        $date   = $today->format('Y-m-d');
        
        $order = $this->getEntityManager()->createQuery("
            SELECT o FROM $ordOrder o
            WHERE o.nrorg = $nrorg
            AND o.orderSheet = '$orderSheet'
            AND (o.createdAt BETWEEN '$date 00:00:00' AND '$date 23:59:59')
        ")->getResult();
        
        return $order;
    }
    
    function removeOrderProduct($orderId, $productId) {
        $order =  $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['id' => $orderId]);
        $orderProduct =  $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findOneBy(['ordOrder' => $orderId, 'ordMenuProduct' => $productId]);
        
        if(empty($orderProduct)) {
            $orderProduct =  $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findOneBy(['ordOrder' => $orderId, 'serService' => $productId]);
        }
        
        $newTotal = $order->getTotal() - $orderProduct->getTotal();
        $order->setTotal($newTotal);
        
        $this->getEntityManager()->merge($order);
        $this->getEntityManager()->remove($orderProduct);
        
        $this->getEntityManager()->flush();
    }
    
    function getOrder($orderId) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy(['orderIdentifier' => $orderId]);
        if ($order !== NULL) $order->build($this->getEntityManager());
        return $order;
    }
    
    function getStoreSalesStatistics($storeId, $initialDate, $finalDate) {
        $order = OrdOrder::class;
        $orderProduct = OrdOrderProduct::class;
        $paymentMethod = PayPaymentMethod::class;
        $dateFormatDql = DateFormatDql::class;
        
        $config = $this->getEntityManager()->getConfiguration();
        
        $config->addCustomDatetimeFunction('DATE', $dateFormatDql);
        
        $today = new \DateTime();
        $todayString = $today->format('Y-m-d');
        
        $sevenDaysAgo = (new \DateTime())->modify('-6 days');
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');
        
        $dateQuery = "";
        if ($initialDate || $finalDate) {
            if ($initialDate) $dateQuery .= "AND o.createdAt > '$initialDate 00:00:00'";
            if ($finalDate) $dateQuery .= " AND o.createdAt < '$finalDate 23:59:59'";
        }  else {
            // $dateQuery = "AND o.createdAt > '$todayString 00:00:00' AND o.createdAt < '$todayString 23:59:59'";
            $dateQuery = "";
        }
        
        $store = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $nrorg = $store ? $store->getNrorg() : NULL;

        $result1 = $this->getEntityManager()->createQuery(
            "
            SELECT SUM(o.total) as salesTotal, COUNT(o.id) as numberOfSales
            FROM $order o
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            AND o.status != 7
            $dateQuery
            "
        )->getResult();
        
        $result2 = $this->getEntityManager()->createQuery(
            "
            SELECT mp.name, SUM(op.quantity) as cnt, SUM(op.total) as total
            FROM $order o
            JOIN $orderProduct op WITH op.ordOrder = o
            JOIN op.ordMenuProduct as mp
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            AND o.status != 7
            $dateQuery
            GROUP BY mp.id
            order by cnt desc
            "
        )->getResult();

        $result3 = $this->getEntityManager()->createQuery(
            "
            SELECT pm.id, pm.label as name, SUM(o.total) as total
            FROM $order o
            JOIN o.evtEvent e
            JOIN " . OrdConfigStore::class . " cs WITH cs.event = e
            JOIN " . OrdStorePayMethod::class . " spm WITH spm.ordConfigStore = cs
            JOIN spm.paymentMethod pm
            WHERE e.id = $storeId
            AND o.paymentMethod = pm.paymentMethod
            AND o.status != 7
            $dateQuery
            GROUP BY pm.id
            "
        )->getResult();
        
        $result4 = $this->getEntityManager()->createQuery(
            "
            SELECT DATE(o.createdAt) orderDate, SUM(o.total) as total
            FROM $order o
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            AND o.createdAt > '$sevenDaysAgo 00:00:00' AND o.createdAt < '$todayString 23:59:59'
            AND o.createdAt is not null
            GROUP BY orderDate
            ORDER BY o.createdAt
            "
        )->getResult();
        
        $result4 = $this->getEntityManager()->createQuery(
            "
            SELECT DATE(o.createdAt) orderDate, SUM(o.total) as total
            FROM $order o
            JOIN o.evtEvent e
            WHERE e.id = $storeId
            AND o.createdAt > '$sevenDaysAgo 00:00:00' AND o.createdAt < '$todayString 23:59:59'
            AND o.createdAt is not null
            AND o.status != 7
            GROUP BY orderDate
            ORDER BY o.createdAt
            "
        )->getResult();
        
        if (count($result1) > 0) {
            $statistics = $result1[0];
            $statistics['salesTotal'] = doubleval($statistics['salesTotal']);
            $statistics['averageTicket'] = $statistics['numberOfSales'] ? $statistics['salesTotal'] / $statistics['numberOfSales'] : 0;
            
            $statistics['salesByProduct'] = $result2;
            $statistics['paymentMethods'] = $result3;
            $statistics['salesByDay'] = $result4;
        } else $statistics = [];
        
        return $statistics;
    }

    public function getUserByIdAndNrorg($userId, $nrorg) {
        $genUser = GenUser::class;
        $genOrganizationUserRel = GenOrganizationUserRel::class;
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id userId, our.firebaseToken
            FROM $genUser u
            JOIN $genOrganizationUserRel our WITH (our.nrorg = '$nrorg' AND u = our.genUser)
            WHERE u.id = '$userId' AND u.status = 'A'
            "
        )->getResult();
        return $user;
    }

   /** 
     * Essa função executa uma trasanção de pagamento junto a operadora de CC.
     * @access private
     * @param Object $order
     * @return Boolean
     */

    private function makePaymentTransactionInProgress($order) {
        $reason         = "alteração no pedido.";
        $paymentMethod = $order->getPaymentMethod();
        $creditCardId  = $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL;
        /* Send the order data to the payment API */
        $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
        $orderId    = $order->getId();
        $value      = $order->getTotal();
        $ccToken    = $creditCard->getToken();
        $customerId = $creditCard->getCustomerId();
        $gateway    = $creditCard->getGateway();
        $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
        $merchant   = $order->getEvtEvent()->getMerchantKey();
        $merchantId = $order->getEvtEvent()->getMerchantId();
        $payment    = $this->paymentApi->execPaymentCreditCard($merchant, $merchantId, $gateway, $order);
        /*salvando historico de transações*/
        $this->saveOrderStatus(new \DateTime(), $orderId, $payment['transaction_id'], OrdOrder::TYPE_TRANSACTION);
        /* Getting data from the payment */
        $status     = $payment['status'];
        $order->setTransactionId($payment['transaction_id']);
        /* Set the payment status */
        $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
        /* If the payment was not successful */
        if ($status !== 'captured') {
            throw new Exception('Payment not successful. Got status \'' . $status . '\'', 7);
        }
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
        $this->sendMessages($order, $reason, false, true);
    } 

    /** 
     * Essa função verifica se o valor total de uma Order foi alterado
     * @access private
     * @param Object $order
     * @return void
     */
    private function checkForChangesInTheOrder($order) {
        if($order->getTransactionId() != NULL && $order->getPaymentStatus() == "A") {
            $orderStatusRel = $this->getEntityManager()->getRepository(OrdOrderStatusRel::class)->findOneBy(['ordOrder' => $order->getId(), 'type' => OrdOrder::TYPE_TRANSACTION], ['createdAt' => 'DESC']);
            if($orderStatusRel != NULL) {
                if($orderStatusRel->getTotal() != $order->getTotal()) {
                    /*executa o metodo para estornar que o cliente pagou ao finalizar a compra*/
                    $this->revokeTheTotalOrderAmount($order, $orderStatusRel);
                    /*executar o pagamento do novo valor da OrdOrder*/
                    $this->makePaymentTransactionInProgress($order);
                }
            }
        }
    }

 /** 

     * Essa função envia uma menssagem para o usuario do sistema.

     * @access public

     * @param Object $order

     * @param String $reason

     * @param Boolean $revoke

     * @param Boolean $exec

     * @return void

     */

    public function sendMessages($order, $reason, $revoke, $exec) {
        $storeName  = $order->getEvtEvent()->getName();
        $user       = $order->getGenUser();
        if($revoke) {
            $message        = "A loja $storeName estornou o valor " . $order->getTotal() . " de sua compra pelo motivo $reason";
        } else if ($exec) {
            $message        = "A loja $storeName realizou a cobrança de um novo valor " . $order->getTotal() . " de sua compra pelo motivo $reason";
        } else {
            $message        = "O seu pedido numero: " . $order->getOrderIdentifier() . " na loja $storeName passou para o status: " . $order->getStatus()->getLabelName() . " ;)";
        }
        $notificationData = ['orderId' => $order->getId()];
        $title = "Sobre sua compra em $storeName";
        $originNotification = "Order_Orderbackend";
        $status             = "P";
        $dateTime           = new \DateTime();
        if ($user !== NULL) {
            try {
                $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user->getId(), $originNotification, $dateTime, $notificationData, $title, $user->getFirebaseToken());
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }
    
    public function verifyTokenAndUrl($nrorg,$storeId) {
        $url = null;
        $token = null;
        $origin = null;
        $external_id = null;
        
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        $origin = $evtEvent->getOriginApiExternal();
        $token = $evtEvent->gettokenApiExternal();
        $url = $evtEvent->geturlApiExternal();
        $external_id = $evtEvent->getExternalId();
        if (empty($token) || empty($url) || empty($origin)) {
            $genconfiguration = $this->getEntityManager()->getRepository(GenConfigurationApiGeneral::class)->findOneBy(['nrorg' => $nrorg]);
            $origin = $genconfiguration->getOriginApiExternal();
            $token = $genconfiguration->gettokenApiExternal();
            $url = $genconfiguration->geturlApiExternal();            
        } 
        $returno['URL'] = $url; 
        $returno['TOKEN'] = $token; 
        $returno['ORIGIN'] = $origin; 
        $returno['EXTERNAL_ID'] = $external_id; 
        
        return $returno;
    }
    
    /*Essa funcao alem de verificar se existe token gerar um token caso nao esista*/
    public function verifyTokenAndUrlV1($nrorg,$storeId) {
        $url = null;
        $token = null;
        $origin = 'EATTK';
        $external_id = null;
        
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        if(isset($evtEvent)) {
            $external_id = $evtEvent->getExternalId();
            // $nrorg = $evtEvent->getNrorg();
            
            $nrorgItegracaoGenconfiguration = $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
            $nrorgItegracao = $nrorgItegracaoGenconfiguration->getNrorgIntegracao();
            
            if($nrorgItegracao != NULL) { 
                $genconfiguration = $this->getEntityManager()->getRepository(GenConfigurationApiGeneral::class)->findOneBy(['nrorg' => $nrorgItegracao]);
            } else {
                $genconfiguration = $this->getEntityManager()->getRepository(GenConfigurationApiGeneral::class)->findOneBy(['nrorg' => $nrorg]);
            }
            
            if($genconfiguration->getTokenApiOdhen() != NULL) {
                // $origin = 'EATTK';
                if($genconfiguration->getDbName() != NULL && $genconfiguration->gettokenApiExternal() != NULL && $genconfiguration->geturlApiExternal() != NULL){
                    $url = $genconfiguration->geturlApiExternal();
                    $token = $genconfiguration->getTokenApiOdhen();
                }else{
                    $token = $genconfiguration->getTokenApiOdhen();
                    $url = 'https://odh-order-api.teknisa.cloud/saas/';                    
                }
            } else {
                // $origin = 'EATTK';
                $url = 'https://odh-order-api.teknisa.cloud';
                $dbName = $genconfiguration->getDbName();
                
                if(!empty($dbName)){
                    $bodyString = "{ \"dbname\": \"$dbName\" }";
                }else{
                    if($nrorgItegracao != NULL){
                        $bodyString = "{ \"dbname\": \"USR_ORG_$nrorgItegracao\" }";
                    }else {
                        $bodyString = "{ \"dbname\": \"USR_ORG_$nrorg\" }";
                    }
                }
                $body = json_decode($bodyString);
                $prefix = "/auth/";
                $stringResponse = $this->makeCurlExternalApi($url, $body, $token, $prefix);
            
                if(isset($stringResponse)) {
                    $explodeToken = explode(":", $stringResponse);
                    $explodeToken = explode('}', $explodeToken[count($explodeToken) -1])[0];
                    $token = explode("\"", $explodeToken)[1];
                    $genconfiguration->setTokenApiOdhen($token);
                    $this->getEntityManager()->flush();
                    $url = 'https://odh-order-api.teknisa.cloud/saas/';
                }
                
            }
            if(empty($token)) {
                // $origin = $evtEvent->getOriginApiExternal();
                $token = $evtEvent->gettokenApiExternal();
                $url = $evtEvent->geturlApiExternal();
                if (empty($token) || empty($url) || empty($origin)) {
                    // $origin = $genconfiguration->getOriginApiExternal();
                    $token = $genconfiguration->gettokenApiExternal();
                    $url = $genconfiguration->geturlApiExternal();
                } 
            }
        }

        $returno['URL'] = $url; 
        $returno['TOKEN'] = $token; 
        $returno['ORIGIN'] = $origin; 
        $returno['EXTERNAL_ID'] = $external_id;
        return $returno;
    }
    
    public function saveOrderExternalApi($row,$order,$token,$url,$origin, $external_id_Store) {
        try {
            $nrorg = $row['NRORG'];
            $body = array();
            $payment = array();
            $address = array();
            $consumerDetail = array();
            
            if(verifyNrorg() != false){
                $nrorg = verifyNrorg();
            }

            $body['saleId'] = $order->getId();// "0000001"; 
            $body['orderNumber'] = "0001";

            //Tipo do pedido delivery (1) ou retirada na loja (2).
            if ($row['DELIVER_TO'] == 'B') 
                $body['orderType'] =  2; 
            else 
                $body['orderType'] =  1; 
            
            // usado para teste
            // $body['origin'] =  "API01";
            // usado em producao
            $body['origin'] = $origin;
            
            $body['date'] =  date("Y-m-d H:i:s"); //"2020-01-01 18:00:00"; 
            $body['scheduleDate'] =  ""; 
            $body['tableNumber'] =  "";
            $body['total'] =  $row['TOTAL_VALUE'];//60.0; 

            $body['troco'] = 0; //VERIFICAR NOME
    
            $CONVENIENCE_FEE = isset($row['CONVENIENCE_FEE']) ? $row['CONVENIENCE_FEE'] : 0;
            $DELIVERY_FEE = isset($row['DELIVERY_FEE']) ? $row['DELIVERY_FEE'] : 0;
            if (empty($CONVENIENCE_FEE)) $CONVENIENCE_FEE = 0;
            if (empty($DELIVERY_FEE)) $DELIVERY_FEE = 0;
            
            $totalTaxa = $DELIVERY_FEE + $CONVENIENCE_FEE;
            $subtotal =  $row['TOTAL_VALUE'] - $totalTaxa;

            $body['subTotal'] =  $subtotal; //58.0;  
            
            //ALTERAR ENTRE AS LINHAS PARA O TESTE DA BRANCH
            $body['subsidiaryCode'] =  str_pad($external_id_Store , 4 , '0' , STR_PAD_LEFT);
            // $body['subsidiaryCode'] =  "0005";
            
            $body['tax'] = $totalTaxa;//2.0; 
            $body['discount'] =  0;
            $body['latitude'] =  0.0; 
            $body['longitude'] =  0.0; 
            $body['cpfInNote'] =  ""; 
            $body['notes'] = $row['NOTE'];// ""; 
            $body['consumerDetail'] = array();
            $body['payments'] = array();
            $body['ItemOrder'] = array();
            //caso sejam vazios os seguintes itens podem ser ocultos do array
            // $body['ComboOrders'] = array();
            // $body['PizzaOrders'] = array();
            
            $userId = $row['USER_ID'];
            $user = $this->getEntityManager()->getRepository(GenUserApiGeneral::class)->find($userId);
            
            $consumerDetail['clientId'] =  $userId; //"123456789"; 
            $consumerDetail['name'] =  $user->getFirstName() . ' ' . $user->getLastName(); //"Cliente"; 
            $consumerDetail['email'] =  $user->getEmail();//"cliente.teste@gmail.com"; 
            $consumerDetail['cpf'] =  $user->getCpf();//""; 
           
            $userContact = $this->getEntityManager()->getRepository(GenContactApiGeneral::class)->findOneBy(['genUser' => $userId]);
            if ($userContact != NULL) {
                $phone =  $userContact->getPhone();
                $phone = str_replace(')','',str_replace('(','',str_replace(' ','',$phone)));
                $consumerDetail['phone']        =  $phone;//"1133332222"; 
                $consumerDetail['cellPhone']    =  $phone;//"11999999999"; 
            } else {
                $consumerDetail['phone']        =  null; 
                $consumerDetail['cellPhone']    =  null;
            }
            
            $consumerDetail['address'] = array();
            $address['zipCode'] = null;
            $address['street'] = null;
            $address['number'] = null;
            $address['neighborhood'] = null;
            $address['city'] = null;
            $address['uf'] = null;
            $address['complement'] = null; 
            $address['referencePoint'] = null;       
            $deliveryAddress = isset($row['DELIVERY_ADDRESS'])   ? $row['DELIVERY_ADDRESS']   : NULL;
            if (!empty($deliveryAddress)) {
                $userAddress = $this->getEntityManager()->getRepository(GenAddressApiGeneral::class)->find($deliveryAddress);
                if ($userAddress != NULL) {
                    $address['zipCode'] =  $userAddress->getCep();//"09605000"; 
                    $address['street'] =  $userAddress->getStreet();//"Avenida Senador Vergueiro"; 
                    $address['number'] =  $userAddress->getNumber();//"4605"; 
                    $address['neighborhood'] =  $userAddress->getNeighborhood();//"Rudge Ramos"; 
                    $address['city'] =  $userAddress->getCity();//"São Bernardo Do Campo"; 
                    $address['uf'] =  $userAddress->getProvincy();//"SP"; 
                    $address['complement'] = $userAddress->getComplement();// ""; 
                    $address['referencePoint'] = null;//"empresa de onibus";     
                    
                    $latitude  = $userAddress->getLatitude();
                    $longitude = $userAddress->getLongitude();
                    if (!empty($longitude) && !empty($latitude)) {
                        $body['latitude']  =  $longitude; 
                        $body['longitude'] =  $latitude;  
                    }
                }
            }

            $paymentMethod = $row['PAYMENT_METHOD'];
            $paymentMethodData = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $paymentMethod]);
            //SE ATENTAR AO ID DO METODO DE PAGAMENTO, SE ELE ESTA CADASTRADO E/OU VINCULADO NO RETAIL
            $payment['paymentId'] =  $paymentMethodData->getId();//"001"; 
            $payment['value'] =  $row['TOTAL_VALUE'];//60.0;
            
            $consumerDetail['address'] = $address;
            $body['consumerDetail'] = $consumerDetail;
            $body['payments'][0] = $payment;
            
            $orderitens = $row['ORDER_ITEMS'];
            for ($index = 0; $index < count($orderitens) ; $index++) {
                $arrayItem = array();
                $item = $orderitens[$index];
                $arrayItem['title']     =  $item["productName"];//"MS CANECA CHOCOLATE";
                $arrayItem['quantity']  =  $item["quant"]; 
                $arrayItem['price']     =  $item["price"]; 
                $arrayItem['total']     =  $item['quant'] * $item["price"]; 
                $arrayItem['notes']     =  $item["note"]; 
                $arrayItem['ref']       =  $item['productId'];// "8110100100";  
                $arrayItem['ComplementCategories'] =  array();
                // if (isset($item['extras']) && !empty($item['extras'])) {
                //     $arrayItem['ComplementCategories'][0] = array();
                //     $arrayItem['ComplementCategories'][0]['title'] = "extras";
                //     $arrayItem['ComplementCategories'][0]['quantity'] = 1; 
                //     $arrayItem['ComplementCategories'][0]['priceUn'] = 0; 
                //     $arrayItem['ComplementCategories'][0]['total'] = 0;
                //     $arrayItem['ComplementCategories'][0]['ref'] = "06|03";
                // }
                $body['ItemOrder'][$index] = $arrayItem;
            }        

            $reponse =  self::makeCurlExternalApi($url, $body, $token,'/order');
            $explodeResponse = explode('"success":',$reponse);
            if (isset($explodeResponse[1])) {
                $responseSucces = explode(',',$explodeResponse[1])[0];
            } else $responseSucces = false;
            
            $order->setOAsuccess($responseSucces);
            $order->setOAReponse($reponse);
            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush();
            return $responseSucces;
        } catch (\Exception $e) {
            $order->setOAsuccess($e->getMessage());
            $order->setOAReponse('false');
            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush();
            return $e->getMessage();
        }
    }

    private function makeCurlExternalApi($url, $body, $token, $prefix) {
        $url = $url.$prefix;
        $ch = curl_init(); 
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER  , true);  // we want headers
        $authorization = "Authorization: Bearer ".$token; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // Closing
        curl_close($ch);
        return $result;
    }
        
   /** 

     * Essa função estornar um valor cobrado de uma Order, caso essa Order tenha seu valor alterado.

     * @access private

     * @param String $orderId deve ser o orderIdentifier de uma Order

     * @return Boolean

     */

    private function revokeTheTotalOrderAmount($order, $orderStatusRel) {
        $reason         = "alteração no pedido.";
        /* If the payment was made via credit card, exec revoke */
        if ($order->getPaymentStatus() == 'A') {
            /* Getting the gateway and the merchant key from the store */
            $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
            $gateway    = $order->getPayCreditcard()->getGateway();
            $merchant   = $order->getEvtEvent()->getMerchantKey();
            $merchantId = $order->getEvtEvent()->getMerchantId();
            /* Executing revoke */
            try {
                $revokeStatus = $this->paymentApi->revoke($orderStatusRel->getTransactionId(), $orderStatusRel->getTotal(), $gateway, $merchant, $merchantId);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                throw new Exception('Unable to execute revoke', 11);
            }
            if ($revokeStatus['success'] != true) {
                throw new Exception('Unable to execute revoke', 11);
            }    
            /*salvando a transacao*/
            $this->saveOrderStatus(new \DateTime(), $order->getId(), $order->getTransactionId(), "CANCELLATION");
            $this->sendMessages($order, $reason, true, false);
            return $revokeStatus['success'];                
        }
    }
    
    /* se tiver no ordproduct usar id pra inserir na menu produtc (caso tambem nao exista na tabela)
    // se n tiver no ordproduct criar nela, usar pra inser na menu
                
    // se ja tiver esse produto, da categoria informada por ele, neste menu
    // retornar falando que dado ja existe (produto ja pertence a categoria)
    */
    public function addMenu($row) {
        try {
            $token = $row['TOKEN'];
            if (empty($token))  return ['error','TOKEN cant be empty'];
            $nrorg = self::tokenAutentication($token);
            $name = isset($row['MENU_NAME']) ? $row['MENU_NAME'] : null;
            $evtEventId = isset($row['EVT_EVENT_ID']) ? $row['EVT_EVENT_ID'] : null;
            if (empty($name) || empty($nrorg)) return ['error', 'Nrorg and Name cant be empty'];
            if (empty($evtEventId)) return ['error','EVT_EVENT_ID not found in row'];
            $orderMenu = new OrdMenu();
            $orderMenu->setName($name);
            $orderMenu->setCreatedAt();
            $orderMenu->setNrorg($nrorg);
            $orderMenu->setCreatedBy($nrorg);
            $this->getEntityManager()->persist($orderMenu);
            $this->getEntityManager()->flush();
            
            $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $evtEventId, 'nrorg' => $nrorg]);
            if (gettype($evtEvent) != 'object') return ['error','EvtEvent not found with EvtEventId: '. $evtEventId];

            $evteventmenu = new EvtEventMenu(); 
            $evteventmenu->setCreatedAt();
            $evteventmenu->setCreatedBy($nrorg);
            $evteventmenu->setOrdMenu($orderMenu);
            $evteventmenu->setEvtEvent($evtEvent);
            $evteventmenu->setNrorg($nrorg);
            $this->getEntityManager()->persist($evteventmenu);
            $this->getEntityManager()->flush();            
            
            
            $ordMenu = $orderMenu->getId();  
            return ['success','OrdMenuId: '. $ordMenu];
        } catch (\Exception $e) {
            return ['error',$e->getMessage()];
        }    
    }

    public function addCategories($row) { 
        try {
            $token = $row['TOKEN'];
            $nrorg = self::tokenAutentication($token);
            $menuid     = isset($row['MENU_ID']) ? $row['MENU_ID'] : null;
            $parentId   = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : null;
            $ordination = isset($row['ORDINATION']) ? $row['ORDINATION'] : null;
            $evtEventId = isset($row['EVT_EVENT_ID']) ? $row['EVT_EVENT_ID'] : null;
            $name       = isset($row['ORD_PRODUCT_GROUP_NAME']) ? $row['ORD_PRODUCT_GROUP_NAME'] : null;
            
            if (empty($menuid)) return ['error', 'MENU_ID not found in row'];
            if (empty($evtEventId)) return ['error', 'EVT_EVENT_ID not found in row'];
            if (empty($name) || empty($nrorg)) return ['error', 'Nrorg and Name cant be empty'];
            //ADICIONANDO MENU

            $newProductGroup = new OrdProductGroup();
            $newProductGroup->setCreatedAt(); 
            $newProductGroup->setName($name); 
            $newProductGroup->setNrorg($nrorg); 
            $newProductGroup->setCreatedBy($nrorg); 
            $newProductGroup->setOrdination($ordination); 
            
            if ($parentId != null) $newProductGroup->setParentId($parentId); 

            if ($menuid != null && $menuid != '') {
                $orderMenu = null;
                $orderMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $menuid, 'nrorg' => $nrorg]);
                if (gettype($orderMenu) == 'object') 
                    $newProductGroup->setOrdMenu($orderMenu);
            }
            
            if ($evtEventId != null && $evtEventId != '') {
                $evtEvent = null;
                $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $menuid, 'nrorg' => $nrorg]);
                if (gettype($evtEvent) == 'object') 
                    $newProductGroup->setEvtEvent($evtEvent); 
            }
            
            $newProductGroup->setOrdMenu($orderMenu); 
            
            $this->getEntityManager()->persist($newProductGroup);
            $this->getEntityManager()->flush();
            $OrdProductGroupId = $newProductGroup->getId();  
            return ['success','OrdProductGroupId: '. $OrdProductGroupId];
        } catch (\Exception $e) {
            return ['error',$e->getMessage()];
        }    
    }
    //public function()
        
    public function addProductToMenu($row) {
        try {
            $token = $row['TOKEN'];
            $nrorg = self::tokenAutentication($token);
            $menuid     = isset($row['MENU_ID']) ? $row['MENU_ID'] : null;
            if (empty($menuid)) return ['error', 'MENU_ID not found in row'];
            $products = isset($row['PRODUCTS_MENU']) ? $row['PRODUCTS_MENU'] : null;

            $orderMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $menuid, 'nrorg' => $nrorg]);
            if (gettype($orderMenu) != 'object') return ['error', 'Menu not found with id: ' . $menuid];
            
            foreach($products as $product) {
                //definição dos dados
                $name                   = isset($product['NAME']) ? $product['NAME'] : null;
                $status                 = isset($product['STATUS']) ? $product['STATUS'] : 'A';
                $image                  = isset($product['IMAGE']) ?  $product['IMAGE'] : null;
                $amount                 = isset($product['AMOUNT']) ? $product['AMOUNT'] : null;
                $detail                 = isset($product['DETAIL']) ?  $product['DETAIL'] : null; 
                $finalTime              = isset($product['FINAL_TIME']) ? $product['FINAL_TIME'] : null;
                $ordination             = isset($product['ORDINATION']) ? $product['ORDINATION'] : null;
                $ordProductExternalID   = isset($product['EXTERNAL_ID']) ? $product['EXTERNAL_ID'] : null;
                $initialTime            = isset($product['INITIAL_TIME']) ? $product['INITIAL_TIME'] : null;
                $estimatedTime          = isset($product['ESTIMATED_TIME']) ? $product['ESTIMATED_TIME'] : 0;
                $ordProduct             = isset($product['ORD_PRODUCT_ID']) ? $product['ORD_PRODUCT_ID'] : null;
                $price                  = isset($product['PRICE']) ? str_replace(',','.',$product['PRICE']) : null;
                $ordProductGroup        = isset($product['ORD_PRODUCT_GROUP_ID']) ? $product['ORD_PRODUCT_GROUP_ID'] : null;
                $ordProductGroupName        = isset($product['ORD_PRODUCT_GROUP_NAME']) ? $product['ORD_PRODUCT_GROUP_NAME'] : null;
                //cria novo objetivo do tipo ordmenuproduct
                $orderMenuProduct = new OrdMenuProduct();
                //seta os dados enviados pela row
                $orderMenuProduct->setCreatedAt();
                $orderMenuProduct->setCreatedBy($nrorg);
                $orderMenuProduct->setAmount($amount);
                $orderMenuProduct->setPrice($price);
                $orderMenuProduct->setFinalTime($finalTime);
                $orderMenuProduct->setInitialTime($initialTime);
                $orderMenuProduct->setNrorg($nrorg);
                $orderMenuProduct->setName($name);
                $orderMenuProduct->setImage($image);
                $orderMenuProduct->setDetail($detail);
                $orderMenuProduct->setStatus($status);
                $orderMenuProduct->setEstimatedTime($estimatedTime);
                $orderMenuProduct->setOrdination($ordination);    
                // seta o menu criado
                $orderMenuProduct->setOrdMenu($orderMenu);
                //ADICIONANDO PRODUTO AO MENU
                //busca objeto produto e grupo produto 
                $productClass = null;
                $productClass = $this->getEntityManager()->getRepository(OrdProduct::class)->find($ordProduct);
                if ($productClass != null && $productClass->getExternalId() != $ordProductExternalID && $ordProductExternalID != null && $ordProductExternalID != '') {
                    $productClass->setExternalId($ordProductExternalID);
                    $this->getEntityManager()->persist($ordproduct);
                    $this->getEntityManager()->flush();
                }
                //seta os objetos  encontrados caso encontre
                if (gettype($productClass) == 'object') {
                    $orderMenuProduct->setOrdProduct($productClass);
                } else {
                    //cria novo objeto da ordproduct 
                    $ordproduct = new OrdProduct();
                    $ordproduct->setCreatedAt();
                    $ordproduct->setCreatedBy($nrorg);
                    $ordproduct->setNrorg($nrorg);
                    $ordproduct->setPrice($price);
                    $ordproduct->setName($name);
                    $ordproduct->setImage($image);
                    $ordproduct->setDetail($detail);
                    if ($ordProductExternalID != null && $ordProductExternalID != '') $ordproduct->setExternalId(strval($ordProductExternalID));    
                    
                    $this->getEntityManager()->persist($ordproduct);
                    $this->getEntityManager()->flush();
                    
                    $orderMenuProduct->setOrdProduct($ordproduct);
                }
                $productGroupClass = null;
                if ($ordProductGroup) $productGroupClass = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($ordProductGroup);
                if (gettype($productGroupClass) == 'object') $orderMenuProduct->setOrdProductGroup($productGroupClass);
                $this->getEntityManager()->persist($orderMenuProduct);
                $this->getEntityManager()->flush();                
            }
         
            return ['success','Product added to menu, with OrdMenuId: '. $menuid];
        } catch (\Exception $e) {
            return ['error',$e->getMessage()];
        }    
    }

    private function tokenAutentication($token) {
        try {
            $tokenAuth = $this->getEntityManager()->getRepository(TokenAuthApi::class)->findOneBy(['token' => $token]);
            if ($tokenAuth != null) {
                $createdAt = $tokenAuth->getCreatedAt();
                $createdAt = $createdAt->format('Y/m/d H:i');
                $date = new \DateTime();
                $Now = $date->format('Y/m/d H:i');
                // Comparando as Datas
                if ((strtotime($Now) - strtotime($createdAt)) > 7200) throw new Exception('token invalid', 403);
                return $tokenAuth->getNrorg();
            } else 
                throw new Exception('token not found', 404);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public function verifyNrorg($entityManager, $nrorg){
       
        $nrorgItegracaoGenConfiguration = $entityManager->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
        $nrorgItegration = $nrorgItegracaoGenConfiguration->getNrorgIntegracao();
       
        if($nrorgItegration != NULL) { 
            $nrorgItegracaoGenConfiguration->setNrorg($nrorgItegration);
            return $nrorgItegration;
        }else{
            return false;
        }

    }
}