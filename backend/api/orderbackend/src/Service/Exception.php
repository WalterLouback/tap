<?php
namespace Zeedhi\ApiOrders\Service;

class Exception extends \Exception {

    const USER_NOT_FOUND = 1;
    const WRONG_OLD_PASSWORD = 2;
    const UNSUPPORTED_CREDIT_CARD_FLAG = 3;
    const INVALID_ORDER_STATUS = 4;
    const ORDER_NOT_FOUND = 5;
    const INSUFICIENT_CREDIT = 6;
    const INVALID_FACEBOOK = 7;
    const FACEBOOK_TOKEN_EXPIRED = 8;
    const USER_ID_NOT_FOUND = 9;

    public static function loginNotFound() {
        return new static('User not found!', self::USER_NOT_FOUND);
    }
    
    public static function wrongOldPassword() {
        return new static('The current password is wrong!', self::WRONG_OLD_PASSWORD);
    }

    public static function unsupportedCreditCardFlag() {
        return new static('The credit card belongs to a non supported brand.', self::UNSUPPORTED_CREDIT_CARD_FLAG);
    }

    public static function invalidOrderStatus($orderStatus) {
        return new static("The selected order has a invalid status: {$orderStatus}", self::INVALID_ORDER_STATUS );
    }

    public static function orderNotFound() {
        return new static("Order not found!", self::ORDER_NOT_FOUND );
    }
    
    public static function balanceInsufficient() {
        return new static("Insuficient credit balance!", self::INSUFICIENT_CREDIT );
    }
    
    public static function invalidFacebook() {
        return new static('Your e-mail was not provided.', self::INVALID_FACEBOOK);
    }

    public static function facebookTokenExpired() {
        return new static('Facebook token expired.', self::FACEBOOK_TOKEN_EXPIRED);
    }
    
    public static function userNotFound() {
        return new static('User ID not found on Database!', self::USER_ID_NOT_FOUND);
    }
    
    public static function userTypeException($userType) {
        return new static('User does not have ' . $userType . ' user type');
    }
     
    /**
    * stringifyParameters
    * 
    * @param Array<String> $params
    * 
    */
    public static function stringifyParameters($params) {
        return join(", ", $params);
    }
    
    public static function invalidParametersException($functionName, $expectedParameters) {
        return new static('Invalid parameters to function \'' . $functionName . '\'. Expected ' . self::stringifyParameters($expectedParameters) . '.');
    }
    
    public static function integrityException($functionName, $attribute) {
        return new static('An integrity issue happened at function \'' . $functionName . '\' with ' . $attribute);
    }
    
    public static function unsuportedPaymentMethod() {
        return new static('The required payment method is not suported');
    }
}