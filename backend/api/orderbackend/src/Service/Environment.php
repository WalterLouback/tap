<?php
namespace Zeedhi\ApiOrders\Service;

interface Environment {

    public function getUserId();

    public function setUserId($userId);

}