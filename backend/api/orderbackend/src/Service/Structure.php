<?php
namespace Zeedhi\ApiOrders\Service;

use Zeedhi\ApiOrders\Model\Entities\GenStructure;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;

use Doctrine\ORM\EntityManager;

class Structure extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    function getStoreStructures($storeId, $nrorg) {
        $structures = $this->getEntityManager()->getRepository(GenStructure::class)->findBy(['nrorg' => $nrorg, 'evtEvent' => $storeId, 'status' => 'A'], ['status' => 'ASC']);
        
        return GenStructure::manyToArray($structures);
    }
    
    public function updateStructure($structureId, $nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId) {
        $structure = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg, 'evtEvent' => $evtEventId]);
        
        $structure->setModifiedBy($userId);
        
        $structure = $this->buildGenStructure($structure, $nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId);
        
        return $structure->toArray();
    }
    
    public function createStructure($nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId) {
        $structure = new GenStructure();
        
        $structure->setCreatedBy($userId);
        
        $structure = $this->buildGenStructure($structure, $nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId);
        
        return $structure->toArray();
    }
    
    public function buildGenStructure($structure, $nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId) {
        $structure->setName($name);
        $structure->setNrorg($nrorg);
        $structure->setDescription($description);
        $structure->setType($type);
        $structure->setStatus($status);
        $structure->setLevel($structureLevel);
        $structure->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $evtEventId));
        
        $this->getEntityManager()->persist($structure);
        $this->getEntityManager()->flush();
        
        return $structure;
    }
    
    public function inactivateStructure($structureId, $nrorg, $evtEventId, $userId) {
        $structure = $this->getEntityManager()->getRepository(GenStructure::class)->findOneBy(['id' => $structureId, 'nrorg' => $nrorg, 'evtEvent' => $evtEventId]);
        
        $structure->setStatus('I');
        $structure->setModifiedBy($userId);
        
        $this->getEntityManager()->persist($structure);
        $this->getEntityManager()->flush();
        
        return $structure->toArray();
    }
}