<?php
namespace Zeedhi\ApiOrders\Listeners;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiOrders\Service\Environment;
use Zeedhi\ApiOrders\Service\ServiceImpl;
use Zeedhi\ApiOrders\Service\ServiceProviderImpl;

use Zeedhi\ApiOrders\Controller\Exception;

use Zeedhi\Framework\DTO\Request;

use Zeedhi\Framework\Cache\Type\ArrayImpl;
use Zeedhi\Framework\Cache\Type\SessionImpl;
use Zeedhi\Framework\Security\OAuth\OAuthImpl;
use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\Session\Storage\NativeSession;
use Zeedhi\Framework\Session\Attribute\SimpleAttribute;

class TokenAuthentication extends \Zeedhi\Framework\Events\PreDispatch\Listener {
    
    
    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;
    
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment   = $environment;
        $this->ignore        = ['/getStores', '/getStoreData'];
        
        $this->setupOAuth();
    }
    
    private function setupOAuth() {
        $session     = new Session(new NativeSession(), new SimpleAttribute());
		$this->oauth = new OAuthImpl(new ServiceProviderImpl(), new SessionImpl($session));
		$this->clientSecret = "Zeedhi Application_SECRET";
    }
    
    public function preDispatch(Request $request) {
        $row   = $request->getRow();
        $route = $request->getRoutePath();
        
        echo $route; die;
        if (!in_array($route, $this->ignore)) {
            if (empty($row['TOKEN']) || empty($row['USER_ID'])) {
                throw Exception::invalidParameters();
            }
            
            $token    = $row['TOKEN'];
            $userId   = $row['USER_ID'];
            
            $clientId = "Zeedhi Application " . $userId . "_ID";
            $this->checkAccessToken($token, $clientId);
        }
    }
    
    public function checkAccessToken($token, $clientId) {
		$service = $this->oauth->checkAccess($token, $this->clientSecret);
		if ($clientId != $service->getClientId()) {
		    throw Exception::unauthorizedAction();
		}
    }
    
}