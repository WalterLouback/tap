<?php
namespace Zeedhi\ApiOrders\Controller;

use Zeedhi\ApiOrders\Helpers\General as General;
use Zeedhi\ApiOrders\Service\EventDeliver as EventDeliverService;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Service\Product as ProductService;
use Zeedhi\ApiOrders\Service\Exception as Exception;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

class Product {
    
    /** @var productService */
    protected $productService;
    
    const WORKSHIFTS         = 'WORKSHIFTS';
    
    const STORE_ID           = 'STORE_ID';
    
    const MENU_ID            = 'MENU_ID';
    const MENU_NAME          = 'MENU_NAME';
    const PRODUCTS           = 'PRODUCTS';
    
    const PRODUCT_GROUP_ID   = 'PRODUCT_GROUP_ID';
    const PRODUCT_GROUP_NAME = 'NAME';
    
    const PRODUCT_ID         = 'PRODUCT_ID';
    const PRODUCT_NAME       = 'NAME';
    const PRODUCT_DETAIL     = 'DETAIL';
    const PRODUCT_PRICE      = 'PRICE';
    const PRODUCT_IMAGE      = 'IMAGE';
    const ESTIMATED_TIME     = 'ESTIMATED_TIME';
    const EXTRAS             = 'EXTRAS';
    const PRODUCT_STATUS     = 'STATUS';
    
    const EXTRA_ID           = 'EXTRA_ID';
    const EXTRA_NAME         = 'NAME';
    const EXTRA_REQUIRED     = 'REQUIRED';
    const EXTRA_MULTIPLE     = 'MULTIPLE';
    const OPTIONS            = 'OPTIONS';
    
    const OPTION_ID          = 'OPTION_ID';
    const OPTION_NAME        = 'NAME';
    const OPTION_PRICE       = 'PRICE';
    
    /**
     * __contruct
     *
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService, EventDeliverService $eventDeliverService) {
        $this->productService       = $productService;
        $this->eventDeliverService  = $eventDeliverService;
    }
    
    /**
     * createMenu
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function createMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::MENU_NAME, General::NRORG, General::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createMenu', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId    = $row[General::STORE_ID];
        $menuName   = $row[self::MENU_NAME];
        $nrorg      = $row[General::NRORG];
        $workshifts = isset($row[self::WORKSHIFTS]) ? $row[self::WORKSHIFTS] : [];
        $products   = isset($row[self::PRODUCTS]) ? $row[self::PRODUCTS] : [];
        
        /* Criar cardápio */
        $menu       = $this->productService->createMenu($menuName, $nrorg, $products, $storeId, $workshifts);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('menu', $menu->toArray()));
    }
    
     function getExtras(DTO\Request\Row $request, DTO\Response $response){
        $row        = $request->getRow();
        $productId      = $row['PRODUCT_ID'];
       
        $extras = $this->productService->getExtras($productId);
        //var_dump($extras);extras;
        $response->addDataSet(new DataSet('extras', OrdExtra::manyToArray($extras)));
    }
    /**
     * createProductGroup
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function createProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::PRODUCT_GROUP_NAME, General::STORE_ID, self::MENU_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createProductGroup', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId  = $row[General::STORE_ID];
        $menuId   = $row[self::MENU_ID];
        $name     = $row[self::PRODUCT_GROUP_NAME];
        
        /* Criar categoria */
        $productGroup     = $this->productService->createProductGroup($name, $storeId, $menuId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('productGroup', $productGroup->toArray()));
    }
    
    /**
     * createProduct
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function createProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::PRODUCT_NAME, General::NRORG, self::PRODUCT_DETAIL, self::PRODUCT_PRICE];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createProduct', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $name           = $row[self::PRODUCT_NAME];
        $nrorg          = $row[General::NRORG];
        $detail         = $row[self::PRODUCT_DETAIL];
        $price          = $row[self::PRODUCT_PRICE];
        $estimatedTime  = $row[self::ESTIMATED_TIME];
        $storeId        = $row[self::STORE_ID];
        $image          = isset($row[self::PRODUCT_IMAGE]) ? $row[self::PRODUCT_IMAGE] : NULL;
        $status         = isset($row[self::PRODUCT_STATUS]) ? $row[self::PRODUCT_STATUS] : 'A';
        $menuId         = isset($row[self::MENU_ID]) ? $row[self::MENU_ID] : NULL;
        $productGroupId = isset($row[self::PRODUCT_GROUP_ID]) ? $row[self::PRODUCT_GROUP_ID] : NULL;
        $extras         = isset($row[self::EXTRAS]) ? $row[self::EXTRAS] : [];
        $externalId     = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : NULL;
        $barCode        = isset($row['BAR_CODE']) ? $row['BAR_CODE'] : NULL;
        
        /* Criar produto */
        $product = $this->productService->createProduct($name, $detail, $price, $nrorg, $estimatedTime, $extras, $menuId, $productGroupId, $storeId, $status, $image, true, $externalId, $barCode);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('product', $product->toArray()));
    }
    
    /**
     * createExtra
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function createExtra(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::EXTRA_NAME, self::EXTRA_REQUIRED, self::EXTRA_MULTIPLE, self::PRODUCT_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createExtra', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $name           = $row[self::EXTRA_NAME];
        $ordination     = isset($row['ORDINATION']) ? $row['ORDINATION'] : null;
        $required       = $row[self::EXTRA_REQUIRED];
        $multiple       = $row[self::EXTRA_MULTIPLE];
        $options        = isset($row[self::OPTIONS]) ? $row[self::OPTIONS] : NULL;
        $productId      = $row[self::PRODUCT_ID];
        $qtdlimit       = $row['LIMIT_QUANTITY'];
        // $qtdlimit       = 3;
        
        /* Criar extra */
        $extra = $this->productService->createExtra($name, $required, $multiple, $options, $productId, $qtdlimit, true, $ordination);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('extra', $extra->toArray()));
    }
    
    /**
     * createOption
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function createOption(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::OPTION_NAME, self::OPTION_PRICE, self::EXTRA_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createOption', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $name           = $row[self::OPTION_NAME];
        $price          = $row[self::OPTION_PRICE];
        $ordination     = isset($row['ORDINATION']) ? $row['ORDINATION'] : NULL;
        $extraId        = $row[self::EXTRA_ID];
        
        /* Criar option */
        $option = $this->productService->createOption($name, $price, $extraId, true, null, $ordination);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('option', $option->toArray()));
    }
    
    function cloneProductGroupsFromMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $sourceMenuId = $row['SOURCE_MENU_ID'];
        $targetMenuId = $row['TARGET_MENU_ID'];
        $storeId      = $row['STORE_ID'];
        
        $this->productService->cloneProductGroupsFromMenu($sourceMenuId, $targetMenuId, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function getProductsFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $storeId  = $row[General::STORE_ID];
        
        $products = $this->productService->getProductsFromStore($storeId);
        
        $response->addDataSet(new DataSet('products', OrdProduct::manyToArray($products)));
    }
    
    function getProductsFromOrganization(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $nrorg    = $row['NRORG'];
        $storeId  = $row[General::STORE_ID];
        
        $products = $this->productService->getProductsFromOrganization($nrorg, $storeId);
        
        $response->addDataSet(new DataSet('products', OrdProduct::manyToArray($products)));
    }
    
    function removeProductFromGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];

        $this->productService->removeProductFromGroup($productId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeProductFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $productId = $row[self::PRODUCT_ID];
        
        $this->productService->removeProductFromStore($productId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeMenuFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $menuId  = $row[self::MENU_ID];
        $storeId = $row[self::STORE_ID];
        
        $this->productService->removeMenuFromStore($menuId, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeExtraFromProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $extraId   = $row[self::EXTRA_ID];
        
        $this->productService->removeExtraFromProduct($extraId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->removeProductGroup($productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeOption(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $optionId  = $row[self::OPTION_ID];
        
        $this->productService->removeOption($optionId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkProductToGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->linkProductToGroup($productId, $productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkMenuProductToGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->linkMenuProductToGroup($productId, $productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkProductToStore(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $storeId        = $row[self::STORE_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        $menuId         = $row[self::MENU_ID];
        
        $menuProduct    = $this->productService->linkProductToStore($productId, $storeId, $productGroupId, $menuId);
        
        $response->addDataSet(new DataSet('product', $menuProduct->toArray()));
    }
    
    function linkMenuProductToStore(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $storeId        = $row[self::STORE_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        $menuId         = $row[self::MENU_ID];
        
        $menuProduct    = $this->productService->linkMenuProductToStore($productId, $storeId, $productGroupId, $menuId);
        
        $response->addDataSet(new DataSet('product', $menuProduct->toArray()));
    }
    
    function updateMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $menuId     = $row[self::MENU_ID];
        $name       = $row[self::MENU_NAME];
        $workshifts = isset($row[self::WORKSHIFTS]) ? $row[self::WORKSHIFTS] : NULL;
        $storeId    = isset($row[self::STORE_ID]) ? $row[self::STORE_ID] : NULL;
        
        $this->productService->updateMenu($menuId, $name, $workshifts, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateExtra(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();
        
        $extraId  = $row[self::EXTRA_ID];
        $name     = $row[self::EXTRA_NAME];
        $ordination = isset($row['ORDINATION']) ? $row['ORDINATION'] : null;
        $required = $row[self::EXTRA_REQUIRED];
        $multiple = $row[self::EXTRA_MULTIPLE];
        $options  = $row[self::OPTIONS];
        $qtdlimit       = $row['LIMIT_QUANTITY'];
        
        
        $this->productService->updateExtra($extraId, $name, $required, $multiple, $options, $qtdlimit, $ordination);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        $name           = $row[self::PRODUCT_GROUP_NAME];
        $ordination     = isset($row['ORDINATION']) ? $row['ORDINATION'] : NULL;
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID']: null;
        
        $this->productService->updateProductGroup($productGroupId, $name, $ordination, $parentId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $menuProductId  = $row[self::PRODUCT_ID];
        $status         = isset($row[self::PRODUCT_STATUS]) ? $row[self::PRODUCT_STATUS] : NULL;
        $name           = isset($row[self::PRODUCT_NAME]) ? $row[self::PRODUCT_NAME] : NULL;
        $nrorg          = isset($row[General::NRORG]) ? $row[General::NRORG] : NULL;
        $detail         = isset($row[self::PRODUCT_DETAIL]) ? $row[self::PRODUCT_DETAIL] : NULL;
        $price          = isset($row[self::PRODUCT_PRICE]) ? $row[self::PRODUCT_PRICE] : NULL;
        $menuId         = isset($row[self::MENU_ID]) ? $row[self::MENU_ID] : NULL;
        $image          = isset($row[self::PRODUCT_IMAGE]) ? $row[self::PRODUCT_IMAGE] : NULL;
        $estimatedTime  = isset($row[self::ESTIMATED_TIME]) ? $row[self::ESTIMATED_TIME] : NULL;
        $productGroupId = isset($row[self::PRODUCT_GROUP_ID]) ? $row[self::PRODUCT_GROUP_ID] : NULL;
        $amount         = isset($row['AMOUNT']) ? $row['AMOUNT'] : NULL;
        $ordination     = isset($row['ORDINATION']) ? $row['ORDINATION'] : NULL;
        $externalId     = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : "";
        $barCode        = isset($row['BAR_CODE']) ? $row['BAR_CODE'] : "";

        /* Criar produto */
        $product = $this->productService->updateProduct($menuProductId, $name, $detail, $price, $nrorg, $estimatedTime, $menuId, $productGroupId, $image, $status, $amount, $ordination, $externalId, $barCode);
        
        $response->addDataSet(new DataSet('product', $product->toArray()));
    }
    
    
     /* Este metodo serve para colocar um produto em destaque OU remover */
    function highlightProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $highlight      = $row['HIGHLIGHT'] == 1 ? TRUE : FALSE;
        $userId         = $row['USER_ID'];
        $menuProductId  = $row['MENU_PRODUCT_ID'];
        $menuProduct    = $this->productService->highlightProduct($menuProductId, $highlight, $userId);
        $response->addDataSet(new DataSet('product', $menuProduct->toArray()));
    }
    
    function getFeaturedProducts(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $nrorg      = $row['NRORG'];
        $latitude   = $row['LATITUDE'];
        $longitude  = $row['LONGITUDE'];
        $storeId    = isset($row['STORE_ID']) ? $row['STORE_ID'] : null;
        
        if($storeId) {
            $products = $this->productService->getFeaturedProductsByStore($nrorg, $storeId);
        } else {
            $stores     = $this->getStoresNearUser($request);
        
            $storesNearIds = [];
            
            foreach($stores as $key => $store) {
               array_push($storesNearIds, $store['id']);
               if($key == 4) {
                   break;
               }
            }
            
            $featuredProducts   = $this->productService->getFeaturedProducts($nrorg, $storesNearIds);
            $topTwentyProducts  = $this->productService->getFeaturedProductsTopTwenty($nrorg, $storesNearIds);
            $products = array_merge($featuredProducts, $topTwentyProducts);
        }
        
        $response->addDataSet(new DataSet('products', $products));
    }
    
    function getStoresNearUser(DTO\Request\Row $request) {
        $row = $request->getRow();
        
        $latitude    = $row['LATITUDE'];
        $longitude   = $row['LONGITUDE'];
        $nrorg       = $row['NRORG'];
        $ownerUser   = NULL;
        $structureId = NULL;
        $parentId    = NULL;
        $storeName   = NULL;
        $initial     = NULL;
        $max         = NULL;
        $showAll     = NULL;
        
        $stores = $this->productService->getStores($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll, $initial, $max);
        $orderedStores = array();
        $nearStores = array();
        
        if($latitude && $longitude){
            foreach ($stores as $store){
                $storeDistanceData = (object)[
                        'store'=> $store, 
                        'distance'=> $this->eventDeliverService->calculateStoreDistance( $store->getNrorg(), $store->getStructure(), $store->getId(), $latitude, $longitude)
                    ];
                array_push($orderedStores, $storeDistanceData);
            }
            
            usort($orderedStores, function($a, $b) {
                    return $a->distance < $b->distance ? -1 : 1;
            });
            /* Preparar a resposta */
            foreach ($orderedStores as $s){
                array_push($nearStores, $s->store);
            }
        }
        else $nearStores = $stores;
        
        return EvtEvent::manyToArray($nearStores);
    }
    
}