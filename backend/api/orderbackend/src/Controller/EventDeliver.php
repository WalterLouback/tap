<?php
namespace Zeedhi\ApiOrders\Controller;

use Zeedhi\ApiOrders\Service\EventDeliver as EventDeliverService;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Zeedhi\ApiOrders\Helpers\Util;

class EventDeliver {
    
    public function __construct(EventDeliverService $eventDeliverService) {
        $this->eventDeliverService = $eventDeliverService;
    }
    
    function getDeliveryData(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        $nrorg          = $row['NRORG'];
        $structureId    = $row['STRUCTURE_ID'];
        $eventId        = $row['EVENT_ID'];
        $userId         = isset($row['USER_ID']) ? $row['USER_ID'] : null;
        $longitude      = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : null;
        $latitude       = isset($row['LATITUDE']) ? $row['LATITUDE'] : null;
        
        $eventDeliver   = $this->eventDeliverService->getDeliveryData($nrorg ,  $structureId,  $eventId, $userId, $latitude, $longitude); 
        
        $response->addDataSet(new DataSet('response', $eventDeliver));
    }
    
    function getDataByStore(DTO\Request $request, DTO\Response $response) {
        $row            =  $row = Util::getParamsAsArray($request);
        $nrorg          = $row['NRORG'];
        $storeId        = $row['STORE_ID'];
        $userId         = $row['USER_ID'];
        $token          = $row['TOKEN'];
        
        $data           = $this->eventDeliverService->getDataByStore($storeId);
        
        $response->addDataSet(new DataSet('response', $data));
    }
    
    function updateRaioKm(DTO\Request $request, DTO\Response $response) {
        $row            =  $row = Util::getParamsAsArray($request);
        $nrorg          = $row['NRORG'];
        $userId         = $row['USER_ID'];
        $token          = $row['TOKEN'];
        $eventDeliverId = $row['EVENT_DELIVER_ID'];
        $distanceKm     = $row['DISTANCE_KM'];
        $price          = $row['PRICE'];
        
        $data           = $this->eventDeliverService->updateRaioKm($eventDeliverId, $distanceKm, $price);
        
        $response->addDataSet(new DataSet('response', $data));
    }
    
    function createRaioKm(DTO\Request $request, DTO\Response $response) {
        $row            =  $row = Util::getParamsAsArray($request);
        $nrorg          = $row['NRORG'];
        $userId         = $row['USER_ID'];
        $token          = $row['TOKEN'];
        $storeId        = $row['STORE_ID'];
        $distanceKm     = $row['DISTANCE_KM'];
        $price          = $row['PRICE'];
        
        $data           = $this->eventDeliverService->createRaioKm($storeId, $distanceKm, $price, $nrorg);
        
        $response->addDataSet(new DataSet('response', $data));
    }
    
    function deleteRaioKm(DTO\Request $request, DTO\Response $response) {
        $row            =  $row = Util::getParamsAsArray($request);
        $id             = $row['ID'];
        $nrorg          = $row['NRORG'];
        $token          = $row['TOKEN'];
        $userId         = $row['USER_ID'];
        $storeId        = $row['STORE_ID'];
        $data           = $this->eventDeliverService->deleteRaioKm($storeId, $id, $nrorg);
        $response->addDataSet(new DataSet('response', $data));
    }
    
}