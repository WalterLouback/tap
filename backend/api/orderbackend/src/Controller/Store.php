<?php
namespace Zeedhi\ApiOrders\Controller;

use Zeedhi\ApiOrders\Helpers\General as General;

use Zeedhi\ApiOrders\Service\Store as StoreService;
use Zeedhi\ApiOrders\Service\Exception as Exception;
use Zeedhi\ApiOrders\Service\EventDeliver as EventDeliverService;

use Zeedhi\ApiOrders\Model\Entities\GenUser;
use Zeedhi\ApiOrders\Model\Entities\GenUserType;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller;
use Zeedhi\ApiOrders\Model\Entities\EvtSellerType;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdConfigStore;

use Zeedhi\ApiOrders\Helpers\Util;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

class Store {
    
    /** @var storeService */
    protected $storeService;
    
    /** @var general */
    protected $general;
    
    const STORE_ID            = 'STORE_ID';
    const STORE_ABOUT         = 'ABOUT';
    const STORE_IMAGE_COVER   = 'IMAGE_COVER';
    const STORE_IMAGE_LOGO    = 'IMAGE_LOGO';
    const STORE_NAME          = 'NAME';
    const STORE_STATUS        = 'STATUS';
    const STORE_OWNER_ID      = 'OWNER_ID';
    const STORE_OWNER_EMAIL   = 'OWNER_USER_EMAIL';
    
    const STRUCTURE_ID        = 'STRUCTURE_ID';
    const DELIVERS_TO_TABLE   = 'DELIVERS_TO_TABLE';
    const DELIVERS_TO_BALCONY = 'DELIVERS_TO_BALCONY';
    const DELIVERS_TO_ADDRESS = 'DELIVERS_TO_ADDRESS';
    
    const SELLER_TYPE_ID      = 'SELLER_TYPE_ID';
    
    /**
     * __contruct
     *
     * @param StoreService $storeService
     */
    public function __construct(StoreService $storeService, General $general, EventDeliverService $eventDeliverService) {
        $this->storeService = $storeService;
        $this->general      = $general;
        $this->general      = $general;
        $this->eventDeliverService      = $eventDeliverService;
    }
    
    /**
     * getStores
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStores(DTO\Request $request, DTO\Response $response) {
        $row      = Util::getParamsAsArray($request);
        
        /* Obter parâmetros da requisição */
        $nrorg       = isset($row[General::NRORG])        ? $row[General::NRORG] : NULL;
        $ownerUser   = isset($row[General::PROFILE_ID])   ? $row[General::PROFILE_ID] : NULL;
        $structureId = isset($row[General::STRUCTURE_ID]) ? $row[General::STRUCTURE_ID] : NULL;
        $parentId    = isset($row[General::PARENT_ID])    ? $row[General::PARENT_ID]    : NULL;
        $storeName   = isset($row[General::STORE_NAME])   ? $row[General::STORE_NAME]   : NULL;
        $initial     = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max         = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $showAll     = isset($row['SHOW_ALL'])            ? $row['SHOW_ALL']   : NULL;
        $storeStatus = isset($row['storeStatus_e'])       ? $row["storeStatus_e"]  : NULL;
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 200;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 1;
        // var_dump($row,($request instanceof DTO\Request\Filter));
        // die;
        /* Obter as lojas */
        if($request instanceof DTO\Request\Filter) {
            $stores = $this->storeService->getStoreWithFilter($nrorg, $ownerUser, $structureId, $parentId, $storeName, $storeStatus, $itemsPerPage, $page);
            /* Preparar a resposta */
            $response->addDataSet(new DataSet('stores', EvtEvent::manyToArray($stores)));
        } else {
            $stores = $this->storeService->getStores($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll, $initial, $max);
            
            /* Preparar a resposta */
            $response->addDataSet(new DataSet('stores', EvtEvent::manyToArray($stores)));
        }
        
    }
    
    function getStoresNearUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $nrorg       = isset($row[General::NRORG])        ? $row[General::NRORG] : NULL;
        $ownerUser   = isset($row[General::PROFILE_ID])   ? $row[General::PROFILE_ID] : NULL;
        $structureId = isset($row[General::STRUCTURE_ID]) ? $row[General::STRUCTURE_ID] : NULL;
        $parentId    = isset($row[General::PARENT_ID])    ? $row[General::PARENT_ID]    : NULL;
        $storeName   = isset($row[General::STORE_NAME])   ? $row[General::STORE_NAME]   : NULL;
        $initial     = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max         = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $showAll     = isset($row['SHOW_ALL'])            ? $row['SHOW_ALL']   : NULL;
        $latitude    = isset($row['LATITUDE'])             ? $row['LATITUDE']    : NULL;
        $longitude   = isset($row['LONGITUDE'])             ? $row['LONGITUDE']    : NULL;
        /* Obter as lojas */
        $stores = $this->storeService->getStores($nrorg, $ownerUser, $structureId, $parentId, $storeName, $showAll, $initial, $max);
        $orderedStores = array();
        $nearStores = array();
        
        if($latitude && $longitude){
            foreach ($stores as $store){
                $storeDistanceData = (object)['store'=>$store, 'distance'=>$this->eventDeliverService->calculateStoreDistance( $store->getNrorg(), $store->getStructure(), $store->getId(), $latitude, $longitude)];
                array_push($orderedStores, $storeDistanceData);
            }
            
            usort($orderedStores, function($a, $b) {
                    return $a->distance < $b->distance ? -1 : 1;
            });
            /* Preparar a resposta */
            foreach ($orderedStores as $s){
                array_push($nearStores, $s->store);
            }
        }
        else $nearStores = $stores;
        
        
        $response->addDataSet(new DataSet('stores', EvtEvent::manyToArray($nearStores)));
    }
    
    function isOpened(DTO\Request\Row $request, DTO\Response $response) {
            $row = $request->getRow();
            $storeId = $row['STORE_ID'];
            $isClose = $this->storeService->isStoreOpened($storeId);
            //var_dump($isClose);die;
            $response->addDataSet(new DataSet('opened', array("value"=>$isClose)));
        
    }
    function addStablishmentId(DTO\Request\Row $request, DTO\Response $response) {
    $row = $request->getRow();
    $storeId = $row['STORE_ID'];
    $establishmentId = $row['ESTABLISHMENT_ID'];

    $teste=$this->storeService->addStablishmentId($storeId,$establishmentId);
    var_dump($teste);die;

    }
    
    function addAcceptVoucher(DTO\Request\Row $request, DTO\Response $response) {
    $row = $request->getRow();
    $storeId = $row['STORE_ID'];
    $acceptVoucher = $row['ACCEPT_VOUCHER'];

    $teste=$this->storeService->addAcceptVoucher($storeId,$acceptVoucher);
    var_dump($teste);die;
    }
    
    function addLockId(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $storeId = $row['STORE_ID'];
        $lockId = $row['LOCK_ID'];
    
        $teste=$this->storeService->addLockId($storeId,$lockId);
        var_dump($teste);die;
    }
    
    function getStoresAssociatedToUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $userId = isset($row['ASSOCIATED_USER_ID']) ? $row['ASSOCIATED_USER_ID'] : $row[GenUser::USER_ID];
        
        /* Obter as lojas */
        $stores = $this->storeService->getStoresAssociatedToUser($userId);
        
        $response->addDataSet(new DataSet('stores', EvtEvent::manyToArray($stores)));
    }
    
    
    function getWorkflowData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $storeId = $row[General::STORE_ID];
        
        $workflow = $this->storeService->getWorkflowData($storeId);
        
        if ($workflow != NULL) {
            $array = $workflow->toArray();
        } else {
            $array = array();
        }
        
        $response->addDataSet(new DataSet('workflow', $array));
    }
    
    /**
     * getStoreData
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStoreData(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [General::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getStoreData', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId    = $row[General::STORE_ID];
        $horaAtual = $row["CURRENT_TIME"];
        
        /* @param $allMenus indicar se esta rota deve retornar todos os menus ou so os menus ativos no momento */
        $allMenus  = isset($row["ALL_MENUS"]) && $row["ALL_MENUS"] == 'TRUE' ? TRUE : FALSE;
       
        /* Instanciar Loja */
        $store  = new EvtEvent();
        $store->setId($storeId);
        
        /* Obter os produtos e os métodos de pagamento */
        $store  = $this->storeService->getStoreData($store, $allMenus, $horaAtual);
        
        if ($store == NULL) {
            throw new Exception('The requested store doest not exist!');
        }
        
        $response->addDataSet(new DataSet('store', $store->toArray()));
    }
    
    function getStoreDataOne(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [General::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getStoreData', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId = $row[General::STORE_ID];
        
        /* Instanciar Loja */
        $store = new EvtEvent();
        $store->setId($storeId);
        
        /* Obter os produtos e os métodos de pagamento */
        $store          = $this->storeService->getStoreDataOne($store);
        
        if ($store == NULL) {
            throw new Exception('The requested store doest not exist!');
        }
        
        $response->addDataSet(new DataSet('store', $store->toArray()));
    }
    
    function createStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obter parâmetros da requisição */
        try {
            $name              = $row[self::STORE_NAME];
            $about             = isset($row[self::STORE_ABOUT]) ? $row[self::STORE_ABOUT] : NULL;
            $structureId       = $row[self::STRUCTURE_ID];
            // $deliversToTable   = $row[self::DELIVERS_TO_TABLE];
            // $deliversToBalcony = $row[self::DELIVERS_TO_BALCONY];
            // $deliversToAddress = $row[self::DELIVERS_TO_ADDRESS];
            $ownerEmail        = $row[self::STORE_OWNER_EMAIL];
            $adminId           = $row['USER_ID'];
            $imageCover        = isset($row[self::STORE_IMAGE_COVER]) ? $row[self::STORE_IMAGE_COVER] : NULL;
            $imageLogo         = isset($row[self::STORE_IMAGE_LOGO]) ? $row[self::STORE_IMAGE_LOGO] : NULL;
            $status            = isset($row[self::STORE_STATUS]) ? $row[self::STORE_STATUS] : 'A';
            $parentEventId     = isset($row['PARENT_EVENT_ID']) ? $row['PARENT_EVENT_ID'] : NULL;
            
            /*Endereço da Strutura*/
            $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
            $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
            $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
            $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
            $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
            $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
            $latitude       = isset($row['LATITUDE']) ? $row['LATITUDE'] : NULL;
            $longitude      = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : NULL;
            $type           = "DELIVERY";
            $minValue       = isset($row['MIN_VALUE']) ? $row['MIN_VALUE'] : NULL;
            $timerTaa       = isset($row['TIMER_TAA']) ? $row['TIMER_TAA'] : NULL;
            $externalID     = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : NULL;
            $barCode        = isset($row['BAR_CODE']) ? $row['BAR_CODE'] : NULL;
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());die;
            throw new \Exception('Missing parameters', 1);
        }
        
        /* Criar loja */
        // $store = $this->storeService->createStore($name, $about, $structureId, $deliversToTable, $deliversToBalcony,  $deliversToAddress, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $parentEventId, $minValue, $externalID);
        $store = $this->storeService->createStore($name, $about, $structureId, $ownerEmail, $imageCover, $imageLogo, $status, $adminId, $parentEventId, $minValue, $externalID, $barCode, $timerTaa);
        
        if($street != NULL && $neighborhood != NULL && $city != NULL && $provincy != NULL) {
            $this->storeService->requestAddressChange($store->getId(), $cep, $street, $neighborhood, $city, $provincy, $number, $type, $latitude, $longitude);
        }
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function getOrganizationWorkflow(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Obter parâmetros da requisição */
        try {
            $nrorg = $row['NRORG'];
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 1);
        }
        
        /* Obter dados do fluxo */
        $workflow = $this->storeService->getOrganizationWorkflow($nrorg);
        $array = [];
        if (get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter') {
            $array = $workflow->toArray()['status'];
            array_push($array, array(
                'ID' => 7,
                'LABEL_NAME' => 'Cancelado'
            ));
        } else $array = $workflow->toArray();
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('workflow', $array));
    }
    
    function updateStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        try {
            $statusId   = $row['STATUS_ID'];
            $color      = $row['COLOR'];
            $labelName  = $row['LABEL_NAME'];
            $buttonName = $row['BUTTON_NAME'];
            $previousId  = $row['PREVIOUS_ID'];
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 1);
        }
        
        $this->storeService->updateStatus($statusId, $color, $labelName, $buttonName, $previousId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function createStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        try {
            $nrorg      = $row['NRORG'];
            $color      = $row['COLOR'];
            $labelName  = $row['LABEL_NAME'];
            $buttonName = $row['BUTTON_NAME'];
            $previousId = $row['PREVIOUS_ID'];
            
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 1);
        }
        
        $this->storeService->createStatus($nrorg, $color, $labelName, $buttonName, $previousId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function setOrganizationPaymentMoment(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        try {
            $nrorg    = $row['NRORG'];
            $statusId = $row['STATUS_ID'];
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 1);
        }
        
        $this->storeService->setOrganizationPaymentMoment($nrorg, $statusId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        try {
            $statusId = $row['STATUS_ID'];
            $nrorg    = $row['NRORG'];
        } catch (\Exception $e) {
            throw new \Exception('Missing parameters', 1);
        }
        
        $this->storeService->removeStatus($statusId, $nrorg);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    /**
     * updateStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function updateStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID, self::STORE_NAME];
        
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('updateStore', $expectedParameters);
        }
        /* Obter parâmetros da requisição */
        $storeId    = $row[self::STORE_ID];
        $name       = $row[self::STORE_NAME];
        $about      = $row[self::STORE_ABOUT];
        $imageCover = isset($row[self::STORE_IMAGE_COVER]) ? $row[self::STORE_IMAGE_COVER] : NULL;
        $imageLogo  = isset($row[self::STORE_IMAGE_LOGO]) ? $row[self::STORE_IMAGE_LOGO] : NULL;
        // $deliversToTable = isset($row['DELIVERS_TO_TABLE']) ? $row['DELIVERS_TO_TABLE'] : NULL;
        // $deliversToBalcony = isset($row['DELIVERS_TO_BALCONY']) ? $row['DELIVERS_TO_BALCONY'] : NULL;
        // $deliversToAddress = isset($row['DELIVERS_TO_ADDRESS']) ? $row['DELIVERS_TO_ADDRESS'] : NULL;
        
        /*Endereço da Strutura*/
        $street         = isset($row['STREET']) ? $row['STREET'] : NULL;
        $number         = isset($row['NUMBER']) ? $row['NUMBER'] : NULL;
        $neighborhood   = isset($row['NEIGHBORHOOD']) ? $row['NEIGHBORHOOD'] : NULL;
        $city           = isset($row['CITY']) ? $row['CITY'] : NULL;
        $provincy       = isset($row['PROVINCY']) ? $row['PROVINCY'] : NULL;
        $cep            = isset($row['CEP']) ? $row['CEP'] : NULL;
        $latitude       = isset($row['LATITUDE']) ? $row['LATITUDE'] : NULL;
        $longitude      = isset($row['LONGITUDE']) ? $row['LONGITUDE'] : NULL;
        $minValue       = isset($row['MIN_VALUE']) ? $row['MIN_VALUE'] : NULL;
        $timerTaa       = isset($row['TIMER_TAA']) ? $row['TIMER_TAA'] : NULL;
        $type           = "DELIVERY";
        $externalID     = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : NULL;
        $barCode        = isset($row['BAR_CODE']) ? $row['BAR_CODE'] : NULL;
        if($street != NULL && $neighborhood != NULL && $city != NULL && $provincy != NULL) {
            $this->storeService->requestAddressChange($storeId, $cep, $street, $neighborhood, $city, $provincy, $number, $type, $latitude, $longitude);
        }
        
        
        /* Alterar loja */
        // $store = $this->storeService->updateStore($storeId, $about, $imageCover, $imageLogo, $name, $deliversToTable, $deliversToBalcony, $deliversToAddress, $latitude, $longitude,$minValue,$externalID);
        $store = $this->storeService->updateStore($storeId, $about, $imageCover, $imageLogo, $name, $latitude, $longitude, $externalID, $minValue, $timerTaa, $barCode);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('store', $store->toArray()));
    }
    
    /**
     * updateGatewayFromStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function updateGatewayFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $storeId    = $row[self::STORE_ID];
        $adminId    = $row['USER_ID'];
        $gateway    = $row['GATEWAY'];
        $merchant   = $row['MERCHANT_KEY'];
        $merchantId = isset($row['MERCHANT_ID']) ? $row['MERCHANT_ID'] : NULL;
        
        /* Atualizar Gateway */
        $this->storeService->updateGatewayFromStore($storeId, $gateway, $merchant, $merchantId, $adminId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updatePaymentMethod(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $paymentMethodId = $row['PAYMENT_METHOD_ID'];
        $name            = $row['NAME'];
        $type            = $row['TYPE'];
        $xPicpayToken    = isset($row['X_PICPAY_TOKEN']) ? $row['X_PICPAY_TOKEN'] : NULL;
        $xSellerToken    = isset($row['X_SELLER_TOKEN']) ? $row['X_SELLER_TOKEN'] : NULL;

        /* Alterar método de pagamento */
        $this->storeService->updatePaymentMethod($paymentMethodId, $name, $type, $xPicpayToken, $xSellerToken);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removePaymentMethod(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $paymentMethodId = $row['PAYMENT_METHOD_ID'];

        /* Remove método de pagamento */
        $this->storeService->removePaymentMethod($paymentMethodId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function createPaymentMethod(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Obter parâmetros da requisição */
        $storeId = $row['STORE_ID'];
        $name    = $row['NAME'];
        $type    = $row['TYPE'];
        $xPicpayToken = isset($row['X_PICPAY_TOKEN']) ? $row['X_PICPAY_TOKEN'] : NULL;
        $xSellerToken = isset($row['X_SELLER_TOKEN']) ? $row['X_SELLER_TOKEN'] : NULL;

        /* Criar método de pagamento */
        $this->storeService->createPaymentMethod($storeId, $name, $type, $xPicpayToken, $xSellerToken);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateStoreWorkshifts(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $storeId    = $row[self::STORE_ID];
        $workshifts = $row['WORKSHIFTS'];
        
        $this->storeService->updateStoreWorkshifts($storeId, $workshifts);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateShift(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $shiftId     = $row['SHIFT_ID'];
        $day         = $row['DAY'];
        $initialTime = $row['INITIAL_TIME'];
        $finalTime   = $row['FINAL_TIME'];
        
        $this->storeService->updateShift($shiftId, $day, $initialTime, $finalTime);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function createStoreShift(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $storeId     = $row['STORE_ID'];
        $day         = $row['DAY'];
        $initialTime = $row['INITIAL_TIME'];
        $finalTime   = $row['FINAL_TIME'];
        
        $this->storeService->createStoreShift($storeId, $day, $initialTime, $finalTime);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeShift(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $shiftId = $row['SHIFT_ID'];
        
        $this->storeService->removeShift($shiftId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function getEventSellersFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $storeId = $row[self::STORE_ID];
        
        $sellers = $this->storeService->getEventSellersFromStore($storeId);
        
        $response->addDataSet(new DataSet('users', EvtEventSeller::manyToArray($sellers)));
    }
    
    function updateSellerFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        $sellerId     = $row['SELLER_ID'];
        $sellerTypeId = $row['SELLER_TYPE_ID'];
        
        $this->storeService->updateSellerFromStore($sellerId, $sellerTypeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    /**
     * removeStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function removeStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('removeStore', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId    = $row[self::STORE_ID];
        
        /* Remover loja */
        $removedId = $this->storeService->removeStore($storeId);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    /**
     * isStoreOpened
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function isStoreOpened(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('isStoreOpened', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId     = $row[self::STORE_ID];
        
        $opened      = $this->storeService->isStoreOpened($storeId);
        
        /* Enviar resposta */
        echo $opened; die;
    }
    
    /**
     * openStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function openStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('openStore', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId     = $row[self::STORE_ID];
        
        /* Verificar se loja havia sido fechada manualmente */
        $storeStatus = $this->storeService->getStoreStatus($storeId);
        
        /* Se tiver sido, simplesmente remover status C da loja */
        /* Se não, abrir manualmente inserindo status O na loja */
        if ($storeStatus === 'C') {
            $newStatus = 'A';
        } else {
            $newStatus = 'O';
        }
        
        $this->storeService->updateStoreStatus($storeId, $newStatus);
        
        $response->addDataSet(new DataSet('response', ['newStatus' => $newStatus]));
    }
    
    /**
     * closeStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function closeStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::STORE_ID];
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('closeStore', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId     = $row[self::STORE_ID];
        
        /* Verificar se loja havia sido aberta manualmente */
        $storeStatus = $this->storeService->getStoreStatus($storeId);
        
        /* Se tiver sido, simplesmente remover status C da loja */
        /* Se não, abrir manualmente inserindo status O na loja */
        if ($storeStatus === 'O') {
            $newStatus = 'A';
        } else {
            $newStatus = 'C';
        }
        
        $this->storeService->updateStoreStatus($storeId, $newStatus);
        
        $response->addDataSet(new DataSet('response', ['newStatus' => $newStatus]));
    }
    
    function getUserTypesFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $storeId   = $row[self::STORE_ID];
        
        $userTypes = $this->storeService->getUserTypesFromStore($storeId);
        
        $response->addDataSet(new DataSet('userTypes', EvtSellerType::manyToArray($userTypes)));
    }
    
    function linkUserToStore(DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        
        $storeId      = $row[self::STORE_ID];
        $sellerEmail  = $row['SELLER_EMAIL'];
        $sellerTypeId = $row['SELLER_TYPE_ID'];
        $adminId      = $row['USER_ID'];
        
        $this->storeService->linkUserToStore($storeId, $sellerEmail, $sellerTypeId, $adminId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function unlinkUserFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $storeId = $row[self::STORE_ID];
        $userId  = $row['REMOVED_USER_ID'];
        $adminId = $row['USER_ID'];
        
        $this->storeService->unlinkUserFromStore($storeId, $userId, $adminId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function createSellerType(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $storeId = $row[self::STORE_ID];
        $sellerTypeName = $row['NAME'];
        $visibleStatus  = isset($row['VISIBLE_STATUS']) ? $row['VISIBLE_STATUS'] : [];
        
        $this->storeService->createSellerType($storeId, $sellerTypeName, $visibleStatus);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateSellerType(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $sellerTypeId = $row[self::SELLER_TYPE_ID];
        $sellerTypeName = $row['NAME'];
        $visibleStatus  = isset($row['VISIBLE_STATUS']) ? $row['VISIBLE_STATUS'] : [];
        
        $this->storeService->updateSellerType($sellerTypeId, $sellerTypeName, $visibleStatus);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeSellerType(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $sellerTypeId = $row[self::SELLER_TYPE_ID];
        
        $this->storeService->removeSellerType($sellerTypeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }

    public function getExternalIntegrationData (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->getExternalIntegrationData($row);
 		$response->addDataSet(new DataSet('getExternalIntegrationData',$resp->ToArrayIntegration()));
    }

    public function UpdateIntegration (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->UpdateIntegration($row);
 		$response->addDataSet(new DataSet('getExternalIntegrationData',$resp));
    }
    
    public function getDeliversTo (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->getDeliversTo($row);
 		$response->addDataSet(new DataSet('getDeliversTo',$resp));
    }    

    public function saveAddDeliversToWidget (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->saveAddDeliversToWidget($row);
 		$response->addDataSet(new DataSet('saveAddDeliversToWidget',$resp));
    }

    public function saveUpdateDeliversToWidget (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->saveUpdateDeliversToWidget($row);
 		$response->addDataSet(new DataSet('saveUpdateDeliversToWidget',$resp));
    }

    public function saveRemoveDeliversToWidget (DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $resp = $this->storeService->saveRemoveDeliversToWidget($row);
 		$response->addDataSet(new DataSet('saveRemoveDeliversToWidget',$resp));
    }
    
    public function updateStoreRecipient (DTO\Request\Row $request, DTO\Response $response) {
        $row          = $request->getRow();
        
        $storeId      = $row['STORE_ID'];
        $recipientId  = $row['RECIPIENT_ID'];
        
        $store = $this->storeService->updateStoreRecipient($storeId, $recipientId);
        
        $response->addDataSet(new DataSet('store', ['store' => $store->toArray()]));
    }
}