<?php
namespace Zeedhi\ApiOrders\Controller;

class Exception extends \Exception {

    const INVALID_PARAMETERS = 1;
    const MISSING_PARAMETERS = 2;
    const NO_ONE_LOGGED = 3;
    const UNAUTHORIZED_ACTION = 4;

    public static function invalidParameters() {
        return new static('Invalid parameters!', self::INVALID_PARAMETERS);
    }
    
    public static function missingParameters() {
        return new static("It is missing parameters!", self::MISSING_PARAMETERS);
    }
    
    public static function noOneLogged() {
        return new static('There is no one logged!', self::NO_ONE_LOGGED);
    }
    
    public static function unauthorizedAction() {
        return new static('Unauthorized Action!', self::UNAUTHORIZED_ACTION);
    }
    
}