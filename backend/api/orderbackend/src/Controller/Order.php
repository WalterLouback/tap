<?php
namespace Zeedhi\ApiOrders\Controller;

use Zeedhi\ApiOrders\Helpers\General as General;

use Zeedhi\ApiOrders\Service\Order as OrderService;
use Zeedhi\ApiOrders\Service\Exception as Exception;

use Zeedhi\ApiOrders\Model\Entities\OrdOption;
use Zeedhi\ApiOrders\Model\Entities\OrdProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdOrder;
use Zeedhi\ApiOrders\Model\Entities\OrdItemExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiOrders\Model\Entities\EvtEvent;
use Zeedhi\ApiOrders\Model\Entities\EvtEventSeller;
use Zeedhi\ApiOrders\Model\Entities\PayCreditcard;
use Zeedhi\ApiOrders\Model\Entities\GenStatus;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO\Response\Message;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiOrders\Helpers\Util;

class Order {
    
    /** @var orderService */
    protected $orderService;
    
    /** @var general */
    protected $general;
    
    const ORDER_ID           = 'ORDER_ID';
    const ORDER_ITEMS        = 'ORDER_ITEMS';
    const ORDER_STATUS       = 'ORDER_STATUS';
    
    /**
     * __contruct
     *
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService, General $general) {
        $this->orderService = $orderService;
        $this->general      = $general;
    }
    
    /**
     * finishOrder
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function finishOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[General::ORDER_ITEMS];
        $localValue       = $row[General::TOTAL_VALUE];
        $deliverTo        = $row[General::DELIVER_TO];
        $userId           = $row[General::USER_ID];
        $storeId          = $row[General::STORE_ID];
        $nrorg            = $row[General::NRORG];
        $paymentMethod    = $row[General::PAYMENT_METHOD];
        $structureId      = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID]   : NULL;
        $creditCardId     = isset($row[General::CREDIT_CARD_ID]) ? $row[General::CREDIT_CARD_ID] : NULL;
        $note             = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $deliveryAddress  = isset($row['DELIVERY_ADDRESS'])? $row['DELIVERY_ADDRESS'] : NULL;
        $convenienceFee   = isset($row['CONVENIENCE_FEE'])? $row['CONVENIENCE_FEE'] : NULL;
        $deliveryFee      = isset($row['DELIVERY_FEE'])? $row['DELIVERY_FEE'] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        $fromTaa          = isset($row['ORDER_FROM_TAA'])? $row['ORDER_FROM_TAA'] : NULL;
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrder($paymentMethod, $status, $localValue, $deliverTo, $note, $userId, $storeId, $nrorg, $creditCardId, $structureId, true, $deliveryAddress, $convenienceFee, $deliveryFee, $fromTaa);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, false, true);
        /* Making financial transaction */
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId);
        /* Saving in external api order */
        $dataTokenUrl = $this->orderService->verifyTokenAndUrlV1($nrorg,$storeId);
        if (!empty($dataTokenUrl['TOKEN']) && !empty($dataTokenUrl['URL']) && !empty($dataTokenUrl['ORIGIN']) && !empty($dataTokenUrl['EXTERNAL_ID'])) 
            $apiOder = $this->orderService->saveOrderExternalApi($row,$order,$dataTokenUrl['TOKEN'],$dataTokenUrl['URL'],$dataTokenUrl['ORIGIN'], $dataTokenUrl['EXTERNAL_ID']);
        else $apiOder = false;
        /* Sending response */
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'status' => $order->getStatus()->toString(),
            'paymentStatus' => $order->getPaymentStatus(),
            'apiOder' => $apiOder
        ]));
    }
    
    function finishOrderWithExternalId(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[General::ORDER_ITEMS];
        $localValue       = $row[General::TOTAL_VALUE];
        $deliverTo        = $row[General::DELIVER_TO];
        $sellerId         = $row[General::USER_ID];
        $storeId          = $row[General::STORE_ID];
        $nrorg            = $row[General::NRORG];
        $clientNPF        = $row['EXTERNAL_ID'];
        $structureId      = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID]   : NULL;
        $note             = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        $deliveryAddress      = isset($row['DELIVERY_ADDRESS'])   ? $row['DELIVERY_ADDRESS']   : NULL;
        $fromTaa          = isset($row['ORDER_FROM_TAA'])   ? $row['ORDER_FROM_TAA']   : NULL;
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderWithExternalId($status, $localValue, $deliverTo, $note, $sellerId, $storeId, $nrorg, $structureId, $clientNPF, false, $deliveryAddress, $fromTaa);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true, false);
        /* Making financial transaction */
        $this->orderService->makePaymentTransaction($order);
        /* Sending response */
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'status' => $order->getStatus()->toString(),
            'paymentStatus' => $order->getPaymentStatus()
        ]));
    }
    
    function addItemsToOrderSheet(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId = $row['USER_ID'];
        $eventId = $row['EVENT_ID'];
        $orderId = $row['ORDER_ID'];
        $products = $row['PRODUCTS'];
        $totalValue = $row['TOTAL_VALUE'];
        
        $order = $this->orderService->addItemsToOrderSheet($userId, $eventId, $orderId, $products, $totalValue);
    }
     
     /**
     * getOrdersFromUser
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $initial       = isset($row[General::FIRST_RESULT])   ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        
        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUser($userId, $structureId, $storeId, $nrorg, $initial, $max);

        $response->addDataSet(new DataSet('orders', OrdOrder::manyToArray($orders)));
    }
    
    function getOrdersFromUserFilter(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['FINAL_DATE'])   ? $row['FINAL_DATE']     : NULL;
        $finalDate     = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE']   : NULL;
        $status        = isset($row['ORDER_STATUS']) ? $row['ORDER_STATUS']   : NULL;

        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUserFilter($storeId, $userId, $nrorg, $initialDate, $max, $finalDate, $status);
        $orderFactory   = OrdOrder::manyToArray($orders);
        $factoredOrders = [];
        foreach($orderFactory as $order) {
            $result = $order;
            $result['initial_status'] = $this->orderService->getOrdWorkflow($order['storeId'])->getInitialStatus()->getId();
            array_push($factoredOrders, $result);
        }
        
        $response->addDataSet(new DataSet('orders', $factoredOrders));
    }
    
    /**
     * getOrdersReport
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    
    function getOrdersReport(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersReport', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID]) && $row[General::STRUCTURE_ID] != '' && count($row[General::STRUCTURE_ID]) > 0  ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID]) && $row[General::STORE_ID] != '' && count($row[General::STORE_ID]) > 0 ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $orderTypes    = isset($row['ORDER_TYPES']) ? $row['ORDER_TYPES'] : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) && $row['INITIAL_DATE'] != ''  ? $row['INITIAL_DATE']     : NULL;
        $finalDate     = isset($row['FINAL_DATE']) && $row['FINAL_DATE'] != '' ? $row['FINAL_DATE']   : NULL;
        $orderStatus   = isset($row['ORDER_STATUS']) && $row['ORDER_STATUS'] != '' && count($row['ORDER_STATUS']) > 0 ? $row['ORDER_STATUS'] : NULL;
        $deliverTo     = isset($row['DELIVER_TO'])  && $row['DELIVER_TO'] != '' ? $row['DELIVER_TO']   : NULL;
        $paymentStatus = isset($row['PAYMENT_STATUS']) && $row['PAYMENT_STATUS'] != '' && count($row['PAYMENT_STATUS']) > 0 ? $row['PAYMENT_STATUS'] : NULL;
        $paymentMethod = isset($row['PAYMENT_METHOD'])  && $row['PAYMENT_METHOD'] != '' ? $row['PAYMENT_METHOD']   : NULL;
        $sellerIds     = isset($row['SELLER_IDS'])  && $row['SELLER_IDS'] != '' ? $row['SELLER_IDS']   : NULL;
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 200;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        $statusIds     = NULL;
        $deliveryAddress = isset($row['DELIVERY_ADDRESS']) ? $row['DELIVERY_ADDRESS'] : NULL;
        $fromTaa     = isset($row['ORDER_FROM_TAA'])  && $row['ORDER_FROM_TAA'] != '' ? $row['ORDER_FROM_TAA']   : NULL;
        /* Get orders */
        $orders         = $this->orderService->getOrdersReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethod, $orderTypes, $sellerIds, $itemsPerPage, $page, $deliveryAddress, $fromTaa);
        $orderFactory   = OrdOrder::manyToArrayReport($orders);
        
        $dataSourceName = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? 'getOrdersReport' : 'orders';
        $response->addDataSet(new DataSet($dataSourceName, $orderFactory));
    }
    
     /**
     * getOrdersFromStore
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromStore(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::STORE_ID, General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $storeId       = $row[General::STORE_ID];
        $userId        = $row[General::USER_ID];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $initialTime   = isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : NULL;
        $finalDate     = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        $finalTime     = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : NULL;
        $cashier       = isset($row['CASHIER']) ? $row['CASHIER'] : NULL;
        
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromStore($storeId, $userId, $initial, $max, $initialDate, $initialTime, $finalDate, $finalTime, $cashier);
        
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
     /**
     * getOrdersFromEvent
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromEvent(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::STORE_ID, General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $storeId       = $row[General::STORE_ID];
        $userId        = $row[General::USER_ID];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        $order         = isset($row['ORDER']) ? $row['ORDER'] : NULL;
        $initialDate   = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $initialTime   = isset($row['INITIAL_TIME']) ? $row['INITIAL_TIME'] : NULL;
        $finalDate     = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        $finalTime     = isset($row['FINAL_TIME']) ? $row['FINAL_TIME'] : NULL;
        $cashier       = isset($row['CASHIER']) ? $row['CASHIER'] : NULL;
        
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromEvent($storeId, $userId, $order, $initial, $max, $initialDate, $initialTime, $finalDate, $finalTime, $cashier);
        
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
    function setOrderToNextStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId        = $row[General::ORDER_ID];
        $paymentMethod  = isset($row["PAYMENT_METHOD"]) && !empty($row["PAYMENT_METHOD"]) ? $row["PAYMENT_METHOD"] : NULL;
        $clientId       = isset($row["CLIENT_ID"]) && !empty($row["CLIENT_ID"]) ? $row["CLIENT_ID"] : NULL;
        
        $status = $this->orderService->setOrderToNextStatus($orderId, $paymentMethod, $clientId);

        $response->addDataSet(new DataSet("response", [ 'status' => $status, 'transaction' => 'ok' ]));
    }
    
    function cancelOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId = $row[General::ORDER_ID];
        $cancellationReason = ['id' => "", 'message' => ""];
        if(isset($row['CANCELLATION_ID'])){
            $cancellationReason['id']      = $row['CANCELLATION_ID'];
            $cancellationReason['message'] = $row['MESSAGE_CANCELLATION'];
        }
        
        $this->orderService->cancelOrder($orderId, $cancellationReason);
        
        $response->addDataSet(new DataSet("response", [ 'status' => 'Ok']));
    }
     
    public function getHistoryStatusOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        $orderId = $row['ORDER_ID'];
        
        $orderHistory = $this->orderService->getHistoryStatusOrder($nrorg, $orderId);
        
        $historyData = [];
        
         foreach ($orderHistory as $history) {
            $array = $history->toArray();
            
            array_push($historyData, $array);
        }
        
        $response->addDataSet(new DataSet('orderHistory', $historyData));
    }   
  
     /**
     * getOrderData
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrderData(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::ORDER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrderData', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $orderId = $row[General::ORDER_ID];
        $userId  = $row[General::USER_ID];
        
        /* Get data from the order */
        $order   = $this->orderService->getOrderData($orderId, $userId);
        $orderData = $order != NULL ? $order->toArray() : [];
        
        $response->addDataSet(new DataSet('order', $orderData));
    }
    
    function getOrdersFromUserInProgress(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
    
        /* Checks if the parameters are valid */
        $expectedParameters = [General::USER_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
       
        /* Get the parameters from the request */
        $userId        = $row[General::USER_ID];
        $nrorg         = isset($row[General::NRORG])          ? $row[General::NRORG] : NULL;
        $structureId   = isset($row[General::STRUCTURE_ID])   ? $row[General::STRUCTURE_ID] : NULL;
        $storeId       = isset($row[General::STORE_ID])       ? $row[General::STORE_ID] : NULL;
        $max           = isset($row[General::MAX_RESULTS])    ? $row[General::MAX_RESULTS]   : NULL;
        $initialDate   = isset($row['FINAL_DATE'])   ? $row['FINAL_DATE']     : NULL;
        $finalDate     = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE']   : NULL;
        
        /* Get orders */
        $orders         = $this->orderService->getOrdersFromUserInProgress($storeId, $userId, $nrorg, $initialDate, $max, $finalDate);
        $orderFactory   = OrdOrder::manyToArray($orders);
        $factoredOrders = [];
        
        foreach($orderFactory as $order) {
            $result = $order;
            $result['initial_status'] = $this->orderService->getOrdWorkflow($order['storeId'])->getInitialStatus()->getId();
            array_push($factoredOrders, $result);
        }
        
        $response->addDataSet(new DataSet('orders', $factoredOrders));
    }
    
    function startOrderService(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Getting the request's parameters */
        $userId          = $row[General::USER_ID];
        $total           = $row[General::TOTAL_VALUE];
        $eventId         = $row[General::EVENT_ID];
        $nrorg           = $row[General::NRORG];
        $paymentMethod   = $row[General::PAYMENT_METHOD];
        $creditCardId    = isset($row[General::CREDIT_CARD_ID]) ? $row[General::CREDIT_CARD_ID] : NULL;
        $note            = isset($row['NOTE']) ? $row['NOTE'] : NULL;
        $clientId        = $row[General::CLIENT_ID];
        $orderSheet      = $row[General::ORDER_SHEET];
        $services        = $row['SERVICES'];
        
        /*verifica se existe uma order para uma comanda*/
        $existOrderSheet = $this->orderService->getOrderByOrderSheet($clientId, $orderSheet, $nrorg);
        
        if(!empty($existOrderSheet)) {
            $order  = $existOrderSheet;
            $msg    = "there is a record for this user with this order sheet!";
        } else {
            /* Instantiating the order without flushing */
            $order = $this->orderService->createOrderService($paymentMethod, $total, $note, $clientId, $eventId, $nrorg, $creditCardId, true, $orderSheet, $userId);
            $orderProduct = $this->orderService->associateServicesOrder($order, $services, true);
            $msg    = "order of service created with success!";
            /* Making financial transaction */
            $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId);
            /* Sending response */
        }
        $response->addDataSet(new DataSet('order', [
            'id' => $order->getId(),
            'msg' => $msg
        ]));
    }
    
    function getOrdersFromServicesByDay(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [General::EVENT_ID];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromStore', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $eventId       = $row[General::EVENT_ID];
        $userId        = $row[General::USER_ID];
        $dateTime      = $row["DATETIME"];
        $initial       = isset($row[General::FIRST_RESULT]) ? $row[General::FIRST_RESULT]   : NULL;
        $max           = isset($row[General::MAX_RESULTS])  ? $row[General::MAX_RESULTS]   : NULL;
        
        /* Get orders */
        $orders    = $this->orderService->getOrdersFromServicesByDay($eventId, $userId, $dateTime, $initial, $max);
        
        $response->addDataSet(new DataSet("orders", OrdOrder::manyToArray($orders)));
    }
    
    function removeOrderProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderId    = $row["ORDER_ID"];
        $nrorg      = $row["NRORG"];
        $productId  = $row["PRODUCT_ID"];
        
        $order      = $this->orderService->getOrder($orderId);
        
        $this->orderService->removeOrderProduct($order->getId(), $productId);
        
        $response->addDataSet(new DataSet("response", [ 'status' => 'Ok']));
    }
    
    function getStoreSalesStatistics(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $storeId     = $row['STORE_ID'];
        $initialDate = isset($row['INITIAL_DATE']) ? $row['INITIAL_DATE'] : NULL;
        $finalDate   = isset($row['FINAL_DATE']) ? $row['FINAL_DATE'] : NULL;
        
        $statistics  = $this->orderService->getStoreSalesStatistics($storeId, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet("statistics", $statistics));
    }
   
    
    function addMenu (DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        $resp  = $this->orderService->addMenu($row);
        $response->addDataSet(new DataSet("addMenu", $resp));
    }   
    
    function addCategories (DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        $resp  = $this->orderService->addCategories($row);
        $response->addDataSet(new DataSet("addCategories", $resp));
    }    
    
    function addProductToMenu (DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        $resp  = $this->orderService->addProductToMenu($row);
        $response->addDataSet(new DataSet("addProductToMenu", $resp));
    }    
    
    function getLocker(DTO\Request $request, DTO\Response $response) {
        $row = $request ->getRow();
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        $status = isset($row['STATUS']) ? $row['STATUS'] : NULL;
        $lockId = isset($row['LOCK_ID']) ? $row['LOCK_ID'] : NULL;
        $resp  = $this->orderService->getOpenALock($lockId, $status, $nrorg);
        
        $result = $resp===true ? ["opened"=>$resp]: ["opened"=>$resp];
        $response->addDataSet(new DataSet("Locker", $result));
    }
    
    function createLocker(DTO\Request $request, DTO\Response $response) {
        $row = $request ->getRow();
        $lockId = isset($row['LOCK_ID']) ? $row['LOCK_ID'] : NULL;
        $nrorg = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        $status = isset($row['STATUS']) ? $row['STATUS'] : NULL;
        $ordOrder = isset($row['ORD_ORDER_ID']) ? $row['ORD_ORDER_ID'] : NULL;
        $doorNumber = isset($row['DOOR_NUMBER']) ? $row['DOOR_NUMBER'] : NULL;
        $createBy = isset($row['USER_ID']) ? $row['USER_ID'] : NULL;
        
        $resp  = $this->orderService->createGenLock($lockId, $nrorg, $status, $ordOrder, $doorNumber, $createBy);
        $response->addDataSet(new DataSet("Locker", $resp));
    }  

    function openTheLock(DTO\Request $request, DTO\Response $response) {
        $req = $request->getParameters();
        
        if(array_key_exists('lockId', $req) && array_key_exists('nrorg', $req)) {
            $lockId = $req['lockId'];
            $nrorg  = $req['nrorg'];
            $status = 'P';
            
            $resp  = $this->orderService->getOpenALock($lockId, $status, $nrorg);
        }
        
        $result = $resp == true ? '{"22": "1"}' : '{"22": "0"}';
        echo $result; die;
    }
    
}