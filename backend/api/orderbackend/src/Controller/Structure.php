<?php
namespace Zeedhi\ApiOrders\Controller;

use Zeedhi\ApiOrders\Helpers\General as General;

use Zeedhi\ApiOrders\Service\Structure as StructureService;
use Zeedhi\ApiOrders\Service\Exception as Exception;

use Zeedhi\ApiOrders\Model\Entities\GenStructure;

use Zeedhi\ApiOrders\Helpers\Util;

use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

class Structure {
    
    /** @var StructureService */
    protected $structureService;
    
    /** @var general */
    protected $general;
    
    /**
     * __contruct
     *
     * @param StructureService $StructureService
     */
    public function __construct(StructureService $structureService, General $general) {
        $this->structureService = $structureService;
        $this->general      = $general;
    }
    
    /**
     * getStructures
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getStoreStructures(DTO\Request $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [General::NRORG];
        if (!$this->general->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getStructures', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $token      = $row['TOKEN'];
        $userId     = $row[GENERAL::USER_ID];
        $nrorg      = $row[GENERAL::NRORG];
        $storeId    = $row[GENERAL::STORE_ID];
        
        /* Obter estruturas */
        $structures = $this->structureService->getStoreStructures($storeId, $nrorg);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('structures', $structures));
    }
    
    /**
     * createStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function createStructureStore(DTO\Request\Row $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        /* Obtêm os parametros da requisição*/
        $name           = $row['NAME'];
        $type           = $row['TYPE'];
        $nrorg          = $row['NRORG'];
        $status         = $row['STATUS'];
        $userId         = $row['USER_ID'];
        $description    = $row['DESCRIPTION'];
        $evtEventId     = $row['EVENT_ID'];
        $structureLevel = $row['STRUCTURE_LEVEL'];
        
        /*Cria unidade*/
        $structure = $this->structureService->createStructure($nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    /**
     * updateStructure
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
     
    public function updateStructureStore(DTO\Request\Row $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        /* Obtêm os parametros da requisição*/
        $name           = $row['NAME'];
        $type           = $row['TYPE'];
        $nrorg          = $row['NRORG'];
        $status         = $row['STATUS'];
        $userId         = $row['USER_ID'];
        $description    = $row['DESCRIPTION'];
        $evtEventId     = $row['EVENT_ID'];
        $structureId    = $row['STRUCTURE_ID'];
        $structureLevel = $row['STRUCTURE_LEVEL'];
        
        /*Obtem a unidade*/
        $structure = $this->structureService->updateStructure($structureId, $nrorg, $name, $description, $structureLevel, $evtEventId, $type, $status, $userId);
        
        /* Preparar a resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function inactivateStructureStore(DTO\Request\Row $request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        
        $this->structureService->inactivateStructure($row['STRUCTURE_ID'], $row['NRORG'], $row['EVENT_ID'], $row['USER_ID']);
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
}