<?php
namespace Zeedhi\ApiOrders\Helpers;

use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\DataSource\DataSet;
use Doctrine\ORM\EntityManager;
use Zeedhi\ApiOrders\Model\Entities\GenConfiguration;

class FCM {
    
    private static $authorization = '';
    const TOPIC_ALL = "all";
    
    public function __construct($nrorg, $entityManager) {
        $genConfiguration = GenConfiguration::class;
        $result = $entityManager->createQuery(
            "
            SELECT c.fcmServerKey
            FROM $genConfiguration c
            WHERE c.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($result) > 0 && isset($result[0]['fcmServerKey'])) 
            self::$authorization = $result[0]['fcmServerKey'];
    }
    
    public function sendNotificationToApp($deviceToken, $title, $body, $data, $priority='high') {
        if (!self::$authorization) throw new \Exception('FCM Server Key not configured for organization.');
        
        $ch = curl_init();
        
        $data['title']   = $title;
        $data['message'] = $body;

        curl_setopt($ch, CURLOPT_URL,"https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: key='.self::$authorization
        ));
        $body = array(
            //'to'  => $deviceToken,
            'registration_ids' => array($deviceToken),
            'notification'=> array( "body" => $body, "title"=> $title, "icon"=> "fcm_push_icon"),
            'data' => $data
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $server_output = curl_exec($ch);
        
        curl_close ($ch);
    }
    
    public function sendNotificationToAllUsers($title, $body, $data, $priority='high') {
        if (!self::$authorization) throw new \Exception('FCM Server Key not configured for organization.');
        
        $ch = curl_init();
        
        $data['title']   = $title;
        $data['message'] = $body;

        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: key='.self::$authorization
        ));
        $topicName = FCM::TOPIC_ALL;
        $body = array(
            'to'  => "/topics/$topicName",
            'notification'=> array( "body" => $body, "title"=> $title, "icon"=> "fcm_push_icon", "sound" => "default", "click_action" => "FCM_PLUGIN_ACTIVITY" ),
            'data' => $data
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $server_output = curl_exec($ch);
        
        curl_close ($ch);
    }
    
    public function subscribeDeviceToTopic($deviceToken, $topicName) {
        if (!self::$authorization) throw new \Exception('FCM Server Key not configured for organization.');
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "https://iid.googleapis.com/iid/v1/$deviceToken/rel/topics/$topicName");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: key='.self::$authorization
        ));
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $server_output = curl_exec($ch);
        
        curl_close($ch);
    }
    
}