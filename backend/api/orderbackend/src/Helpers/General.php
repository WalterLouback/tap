<?php
namespace Zeedhi\ApiOrders\Helpers;

use Zeedhi\Framework\Session\Session;
use Zeedhi\Framework\DataSource\DataSet;
use Doctrine\ORM\EntityManager;

class General {
    
    const USER_ID            = 'USER_ID';
    const CLIENT_ID          = 'CLIENT_ID';
    const PROFILE_ID         = 'PROFILE_ID';
    const STORE_ID           = 'STORE_ID';
    const ORDER_SHEET        = 'ORDER_SHEET';
    const STRUCTURE_ID       = 'STRUCTURE_ID';
    const EVENT_SELLER_ID    = 'EVENT_SELLER_ID';
    const EVENT_ID           = 'EVENT_ID';
    const STRUCTURE_TYPE     = 'STRUCTURE_TYPE';
    const ORDER_ID           = 'ORDER_ID';
    const NRORG              = 'NRORG';
    const STORE_NAME         = 'STORE_NAME';
    const PARENT_ID          = 'PARENT_ID';
    const FILTER             = 'FILTER';
    const ORDER_ITEMS        = 'ORDER_ITEMS';
    const PAYMENT_METHOD     = 'PAYMENT_METHOD';
    const TOTAL_VALUE        = 'TOTAL_VALUE';
    const DELIVER_TO         = 'DELIVER_TO';
    const CREDIT_CARD_ID     = 'CREDIT_CARD_ID';
    const CARTAO_CREDITO_APP = 'CARTAO_CREDITO_APP';
    const BALANCE            = 'BALANCE';
    const CARTAO_NA_ENTREGA  = 'CARTAO_NA_ENTREGA';
    const DINHEIRO           = 'DINHEIRO';  
    const FIRST_RESULT       = 'FIRST_RESULT';
    const MAX_RESULTS        = 'MAX_RESULTS';
    const STRUCTURE_LEVEL    = 'LEVEL';
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public static function factoryOrderDataSet($order) {
        return new DataSet("order", [array(
            "ID" => $order->getId(),
            "CREATE_DATE" => $order->getCreateDate(),
            "PAYMENT_METHOD" => $order->getPaymentMethod(),
            "QR_CODE" => $order->getQrCode(),
            "STATUS" => $order->getStatus(),
            "TOTAL" => $order->getTotal(),
            "PROFILE_ID" => $order->getGenUserTypeRel() != NULL ? $order->getGenUserTypeRel()->getId() : NULL,
            "SELLER_ID" => null !== $order->getEvtEventSeller() ? $order->getEvtEventSeller()->getId() : NULL,
            "CREDIT_CARD_ID" => null !== $order->getPayCreditcard() ? $order->getPayCreditcard()->getId() : NULL,
            "STORE_ID" => $order->getEvtEvent()->getId(),
            "NRORG" => $order->getNrorg()
        )]);
    }
    
    /**
      * checkParameters
      * 
      * @param DTO\Request\Row $row
      * @param Array<String>   $params
      * 
      */
    public static function checkParameters($row, $params) {
        foreach($params as $param) {
            if (!isset($row[$param])) 
                return false;
        }
        return true;
    }
    
    public static function oneToManyQuery($objs, $alias) {
        $objsQuery = "";
        foreach($objs as $obj) {
            $objId = $obj->getId();
            if ($objsQuery != "") $objsQuery = $objsQuery . "OR ";
            $objsQuery = $objsQuery . "$alias = $objId ";
        }
        $objsQuery = "WHERE ( " . $objsQuery . ")";
        return $objsQuery;
    }
    
    public function getEntityManager() {
        return $this->entityManager;
    }
    
}