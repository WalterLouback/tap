<?php
namespace Zeedhi\ApiGeneral\Model\Entities\Base;


abstract class GenDependent {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $monthlyLimit = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $receiptsTo;
    /** @var string  */
    protected $canUseMainCard;
    /** @var string  */
    protected $status;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $dependent;
    /** @var \Zeedhi\ApiGeneral\Model\Entities\GenUser  */
    protected $parent;

	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getMonthlyLimit() {
        return $this->monthlyLimit;
    }
	public function setMonthlyLimit($monthlyLimit) {
        $this->monthlyLimit = $monthlyLimit;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getCanUseMainCard() {
        return $this->canUseMainCard;
    }
	public function setCanUseMainCard($canUseMainCard) {
        $this->canUseMainCard = $canUseMainCard;
    }
	public function getDependent() {
        return $this->dependent;
    }
	public function setDependent($dependent = NULL) {
        $this->dependent = $dependent;
    }
	public function getParent() {
        return $this->parent;
    }
	public function setParent($parent = NULL) {
        $this->parent = $parent;
    }
	public function getReceiptsTo() {
        return $this->receiptsTo;
    }
	public function setReceiptsTo($receiptsTo = NULL) {
        $this->receiptsTo = $receiptsTo;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    
}