<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class OrdWorkflow extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdWorkflow {

    public function getStatusArray($current=NULL, $statusArray=array()) {
        if ($current == NULL) $current = $this->getInitialStatus();
        if ($current == NULL) return $statusArray;
        
        array_push($statusArray, $current);
        if ($current->getNext() != NULL) {
            return $this->getStatusArray($current->getNext(), $statusArray);
        }
            
        return $statusArray;
    }

    public function toArray() {
        return array(
            'status' => GenStatus::manyToArray($this->getStatusArray()),
            'paymentMoment' => $this->getPaymentMoment() ? $this->getPaymentMoment()->toArray() : NULL
        );
    }

}