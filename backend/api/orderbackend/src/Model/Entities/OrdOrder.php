<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiOrders\Model\Entities\GenContact;

class OrdOrder extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdOrder {

    const               STATUS_PAYMENT_PENDING  = 'P';
    const                          STATUS_PAID  = 'A';
    const                      STATUS_CANCELED  = 'C';
    const                STATUS_NOT_AUTHORIZED  = 'N';

    const            PAYMENT_METHOD_CREDITCARD  = 'CC';
    const               PAYMENT_METHOD_BALANCE  = 'BALANCE';
    const           PAYMENT_METHOD_EXTERNAL_CC  = 'EXT_CC';
    const           PAYMENT_METHOD_EXTERNAL_DC  = 'EXT_DC';
    const         PAYMENT_METHOD_EXTERNAL_CASH  = 'EXT_CASH';
    const PAYMENT_METHOD_CREDITCARD_FOR_WALLET  = 'CC_W';
    
    const                     DELIVER_TO_TABLE  = 'T';
    const                   DELIVER_TO_BALCONY  = 'B';
    const                   DELIVER_TO_ADDRESS  = 'A';
    const                   TYPE_TRANSACTION    = 'PAYMENT';
    var                                  $user  = [];
    var                                $seller  = [];
    
    public function build($entityManager) {
        $this->setItems($entityManager);
        $this->setPaymentMethodName($entityManager);
        $this->setIsFinished($entityManager);
        $this->setUserData($entityManager);
        $this->setSellerData($entityManager);
        $this->setFromTaa($entityManager);

    }
    
    public function getPaymentMethodName() {
        return property_exists($this, 'paymentMethodName') ? $this->paymentMethodName : NULL;
    }
    
    public function getIsFinished() {
        return property_exists($this, 'isFinished') ? $this->isFinished : NULL;
    }
    
    public function setPaymentMethodName($entityManager) {
        $orderId = $this->getId();
        $response = $entityManager->createQuery(
            "
            SELECT pm.label
            FROM 'Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod' pm
            JOIN 'Zeedhi\ApiOrders\Model\Entities\OrdOrderRO' o WITH ( o.paymentMethod = pm.paymentMethod AND ( o.nrorg = pm.nrorg or pm.nrorg = 0) )
            WHERE o.id = '$orderId'
            "
        )->getResult();
        if (count($response) > 0)
            $this->paymentMethodName = $response[0]['label'];
    }
    
    public function setIsFinished($entityManager) {
        $orderId = $this->getId();
        $status  = $entityManager->getRepository(GenStatus::class)->find($this->getStatus());
        
        $this->isFinished = $status->getNext() == NULL;
    }
    
    public function toArray() {
        $array = [];
        
        $array['qr'] = $this->getId();
        $array['structure'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getName() : NULL;
        $array['orderStatus'] = $this->getStatus() != NULL ? $this->getStatus()->toString() : NULL;
        $array['paymentStatus'] = $this->getPaymentStatus();
        $array['nrorg'] = $this->getNrorg();
        $array['note'] = $this->getNote();
        $arrya['status'] = $this->getStatus() != NULL ? $this->getStatus()->getId() : NULL;
        $array['storeId'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['storeName'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getName() : NULL;
        $array['storeType'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getType() : NULL;
        $array['storeLogo'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getImageLogo() : NULL;
        $array['userFirstName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getFirstName() : NULL;
        $array['userLastName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getLastName() : NULL;
        $array['statusColor'] = $this->getStatus() != NULL ? $this->getStatus()->getColor() : NULL;
        $array['isFinished'] = (bool) $this->getIsFinished();
        $array['total'] = $this->getTotal();
        $array['transactionId'] = $this->getTransactionId();
        $array['createDate'] = $this->getCreatedAt();
        $array['items'] = OrdOrderProduct::manyToArray($this->getItems());
        if($this->getOrderSheet()) { 
            foreach($array['items'] as $i => $item) {
                $array['items'][$i]['sellerId'] = $this->getSellerData()['sellerId'];
                $array['items'][$i]['professional'] = $this->getSellerData()['fullName'];
            }
        }
        $array['userData'] = $this->getGenUser() != NULL ? $this->user : NULL;
        $array['orderSheet'] = $this->getOrderSheet();
        $array['id'] = $this->getOrderIdentifier();
        $array['deliverTo'] = $this->getDeliverTo();
        $array['creditCardId'] = $this->getPayCreditcard() ? $this->getPayCreditcard()->getId() : NULL;
        $array['paymentMethod'] = $this->getPaymentMethod();
        $array['paymentMethodName'] = $this->getPaymentMethodName();
        $array['convenienceFee'] = $this->getConvenienceFee();
        $array['deliveryFee'] = $this->getDeliveryFee();
        $array['fromTaa'] = $this->getFromTaa();
        
        
        return $array;
    }
    
    public function getItems() {
        return property_exists($this, 'items') ? $this->items : [];
    }

    public function setItems($entityManager) {
        $items = $entityManager->getRepository(OrdOrderProduct::class)->findBy(['ordOrder' => $this->getId()], ['status' => 'DESC']);
        foreach ($items as $item) {
            $item->build($entityManager);
        }
        
        $this->items = $items;
    }
    
    public static function manyToArray($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public static function manyToArrayReport($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArrayReport();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArrayReport() {
        $array = [];
        $array['id'] = $this->getOrderIdentifier();
        $array['createDate'] = $this->getCreatedAt()->__toString();
        $array['deliverTo'] = $this->getDeliverTo();
        $array['structureId'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getId() : NULL;
        $array['structureName'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getName() : NULL;
        $array['paymentMethod'] = $this->getPaymentMethod();
        $array['paymentMethodName'] = $this->getPaymentMethodName();
        $array['orderStatus'] = $this->getStatus() != NULL ? $this->getStatus()->toString() : NULL;
        $array['paymentStatus'] = $this->getPaymentStatus();
        $array['total'] = $this->getTotal();
        $array['nrorg'] = $this->getNrorg();
        $array['note'] = $this->getNote();
        $array['status'] = $this->getStatus() != NULL ? $this->getStatus()->getId() : NULL;
        $array['creditCardId'] = $this->getPayCreditcard() ? $this->getPayCreditcard()->getId() : NULL;
        $array['storeId'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['storeName'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getName() : NULL;
        $array['userFirstName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getFirstName() : NULL;
        $array['userLastName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getLastName() : NULL;
        $array['userFullName'] = $array['userFirstName'] . ' ' . $array['userLastName'];
        $array['isFinished'] = (bool) $this->getIsFinished();
        $array['convenienceFee'] = $this->getConvenienceFee();
        $array['deliveryFee'] = $this->getDeliveryFee();
        $array['qr'] = $this->getId();
        $array['fromTaa'] = $this->getFromTaa();
        
        return $array;
    }
    
    public static function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        
            return $uuid;
        }
    }
    
    public function setUserData($entityManager) {
        $genUser        = GenUser::class;
        $userTypeRel    = GenUserTypeRel::class;
        $contact        = GenContact::class;
        
        $userId         = $this->getGenUser()->getId();
        
        $user = $entityManager->createQuery(
            "
            SELECT DISTINCT gu.id, CONCAT(gu.firstName, ' ', gu.lastName) fullName, gu.cpf, gu.email, gc.phone, utr.externalId
            FROM $genUser gu
            LEFT JOIN $userTypeRel utr WITH utr.genUser = $userId
            LEFT JOIN $contact gc WITH gc.genUser = $userId 
            WHERE gu.id = $userId
            "
        )->getResult();
        
        $this->user = $user ? $user[0] : [];
    }
    
    public function getUserData() {
        return property_exists($this, 'user') ? $this->user : [];
    }
    
    public function setSellerData($entityManager) {
        $genUser        = GenUser::class;
        $eventSeller    = EvtEventSeller::class;
        
        
        $sellerId        = $this->getEvtEventSeller()->getId();
        
        $seller = $entityManager->createQuery(
            "
            SELECT DISTINCT gu.id sellerId, CONCAT(gu.firstName, ' ', gu.lastName) fullName
            FROM $genUser gu
            LEFT JOIN $eventSeller es WITH es.genUser = gu.id 
            WHERE es.id = $sellerId
            "
        )->getResult();
        
        $this->seller = $seller[0];
    }
    
    public function getSellerData() {
        return property_exists($this, 'seller') ? $this->seller : [];
    }
}