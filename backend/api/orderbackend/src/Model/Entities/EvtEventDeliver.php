<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class EvtEventDeliver extends \Zeedhi\ApiOrders\Model\Entities\Base\EvtEventDeliver {
    
    public function build($entityManager) {
        
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NRORG' => $this->getNrorg(),
            'DISTANCE_KM' => str_replace(".", ",", $this->getDistanceKm()),
            'PRICE' => $this->getPrice(),
            'CREATED_BY' => $this->getCreatedBy(),
            'MODIFIED_BY' => $this->getModifiedBy(),
            'CREATED_AT' => $this->getCreatedAt(),
            'MODIFIED_AT' => $this->getModifiedAt(),
            'EVT_EVENT' => $this->getEvtEvent() ? [ 'EVENT_ID' => $this->getEvtEvent()->getId(), 'EVENT_NAME' => $this->getEvtEvent()->getName() ] : NULL
        );
    }
    
    public static function manyToArray($eventDeliver) {
        $arrays = [];
        foreach ($eventDeliver as $el) {
            $array = $el->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
}