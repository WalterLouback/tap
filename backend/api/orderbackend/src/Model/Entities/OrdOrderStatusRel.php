<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class OrdOrderStatusRel extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdOrderStatusRel {
    
    public function toArray() {
        $array = [];
        
        $array['id'] = $this->getId();
        $array['orderId'] = $this->getOrdOrder()->getOrderIdentifier();
        $array['status'] = $this->getGenStatus()->getLabelName();
        $array['createDate'] = $this->getCreateDate();
        $array['nrorg'] = $this->getNrorg();
        $array['total'] = $this->getTotal();
        $array['transactionId'] = $this->getTransactionId();
        $array['type'] = $this->getType();
        
        return $array;
    }
    
}