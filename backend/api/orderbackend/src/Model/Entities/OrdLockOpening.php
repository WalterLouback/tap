<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class OrdLockOpening extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdLockOpening {

    public function toArray() {
        return array(
            "ID"        => $this->getId(),
            "LOCK_ID"   => $this->getLockId(),
            "STATUS"    => $this->getStatus(),
            "ORD_ORDER_ID"  => $this->getOrdOrder() ? $this->getOrdOrder()->getId() : null,
            "NRORG"     => $this->getNrorg(),
            "DOOR_NUMBER"   => $this->getDoorNumber()
        );
    }
        
    public function createGenLock($entityManager, $lockID=null, $nrorg=null, $status=null, $ordOrder=null, $doorNumber=null, $createdBy=null){
        $genLock = new OrdLockOpening();
        
        $lockID     ? $genLock->setLockId($lockID)  : null;
        $nrorg      ? $genLock->setNrorg($nrorg)    : null;
        $status     ? $genLock->setStatus($status)  : null;
        $ordOrder   ? $genLock->setOrdOrder($entityManager->getRepository(OrdOrder::class)->find($ordOrder)) : null;
        $doorNumber ? $genLock->setDoorNumber($doorNumber)  : null;
        $createdBy  ? $genLock->setCreatedBy($createdBy)    : null;
        
        $entityManager->persist($genLock);
        $entityManager->flush();
        
        return $genLock->toArray();
    }
        
    public function updateOpenLock($entityManager, $lockerID, $status, $nrorg){
        $lockop = $entityManager->getRepository(OrdLockOpening::class)->findOneBy(['status'=>$status,'lockId'=>$lockerID, 'nrorg'=>$nrorg]);
        
        $lockop->setStatus('I');
        
        $entityManager->merge($lockop);
        $entityManager->flush();
    }
        
    public function getOpenALock($entityManager, $lockerID, $status, $nrorg){      
    
        $opened = $entityManager->createQuery("SELECT count(olo.id) FROM ".OrdLockOpening::class." olo 
        WHERE olo.status = '$status' AND olo.lockId = '$lockerID' AND olo.nrorg ='$nrorg'")->getResult()[0][1]; 
        
        return ($opened == 0) ? false : true;
    }
        
    public static function manyToArray($statusArray) {
        $arr = [];
        
        foreach ($statusArray as $status) {
            array_push($arr, $status->toArray());
        }
        
        return $arr;
    }

    public function toString() {
        return $this->getLabelName();
    }

}