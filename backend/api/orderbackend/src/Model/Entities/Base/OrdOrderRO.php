<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdOrderRO {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $id;
    /** @var decimal  */
    protected $total;
    /** @var integer  */
    protected $orderIdentifier;
    /** @var string  */
    protected $deliverTo;
    /** @var string  */
    protected $nsu;
    /** @var string  */
    protected $transactionId;
    /** @var string  */
    protected $paymentStatus;
    /** @var string  */
    protected $paymentMethod;
    /** @var string  */
    protected $orderKey;
    /** @var \Datetime  */
    protected $createDate;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEventSeller  */
    protected $evtEventSeller;
    /** @var \Zeedhi\ApiOrders\Model\Entities\PayCreditcard  */
    protected $payCreditcard;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStatus  */
    protected $status;
    /** @var string */
    protected $note;
    /** @var string */
    protected $type;
    /** @var string */
    protected $creditCardNumbers;
    /** @var string */
    protected $creditCardBrand;
    /** @var string */
    protected $cardHolderName;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdCashier  */
    protected $ordCashier;
    /** @var \Zeedhi\ApiEvents\Model\Entities\PayWallet  */
    protected $wallet;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenContact  */
    protected $genContact;
    /** @var int */
    protected $deliveryAddress;
    /** var float */
    protected $deliveryFee;
    /** @var decimal  */
    protected $convenienceFee;
    
    protected $fromTaa;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {        
        $this->modifiedAt = new \DateTime();
    }
    public function getType() {
        return $this->type;
    }
    public function setType($type = NULL) {
        $this->type = $type;
    }
    public function getCreditCardNumbers() {
        return $this->creditCardNumbers;
    }
    public function setCreditCardNumbers($creditCardNumbers = NULL) {
        $this->creditCardNumbers = $creditCardNumbers;
    }
    public function getCreditCardBrand() {
        return $this->creditCardBrand;
    }
    public function setCreditCardBrand($creditCardBrand = NULL) {
        $this->creditCardBrand = $creditCardBrand;
    }
    public function getCardHolderName() {
        return $this->cardHolderName;
    }
    public function setCardHolderName($cardHolderName = NULL) {
        $this->cardHolderName = $cardHolderName;
    }
    public function getNote() {
        return $this->note;
    }
    public function setNote($note = NULL) {
        $this->note = $note;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getDeliverTo() {
        return $this->deliverTo;
    }
	public function setDeliverTo($deliverTo) {
        $this->deliverTo = $deliverTo;
    }
	public function getOrderIdentifier() {
        return $this->orderIdentifier;
    }
	public function setOrderIdentifier($orderIdentifier) {
        $this->orderIdentifier = $orderIdentifier;
    }
	public function getNsu() {
        return $this->nsu;
    }
	public function setNsu($nsu) {
        $this->nsu = $nsu;
    }
	public function getTransactionId() {
        return $this->transactionId;
    }
	public function setTransactionId($transactionId) {
        $this->transactionId = $transactionId;
    }
	public function getPaymentStatus() {
        return $this->paymentStatus;
    }
	public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }
	public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
    }
	public function getOrderKey() {
        return $this->orderKey;
    }
	public function setOrderKey($orderKey = NULL) {
        $this->orderKey = $orderKey;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus(\Zeedhi\ApiOrders\Model\Entities\GenStatus $status = NULL) {
        $this->status = $status;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiOrders\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getEvtEventSeller() {
        return $this->evtEventSeller;
    }
	public function setEvtEventSeller(\Zeedhi\ApiOrders\Model\Entities\EvtEventSeller $evtEventSeller = NULL) {
        $this->evtEventSeller = $evtEventSeller;
    }
	public function getPayCreditcard() {
        return $this->payCreditcard;
    }
	public function setPayCreditcard(\Zeedhi\ApiOrders\Model\Entities\PayCreditcard $payCreditcard = NULL) {
        $this->payCreditcard = $payCreditcard;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiOrders\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
    public function getGenContact() {
        return $this->genContact;
    }
	public function setGenContact(\Zeedhi\ApiOrders\Model\Entities\GenContact $genContact = NULL) {
        $this->genContact = $genContact;
    }
    public function getOrdCashier() {
        return $this->ordCashier;
    }
	public function setOrdCashier(\Zeedhi\ApiEvents\Model\Entities\OrdCashier $ordCashier = NULL) {
        $this->ordCashier = $ordCashier;
    }
    public function getWallet() {
        return $this->wallet;
    }
	public function setWallet(\Zeedhi\ApiEvents\Model\Entities\PayWallet $wallet = NULL) {
        $this->wallet = $wallet;
    }
    public function getDeliveryAddress() {
        return $this->deliveryAddress;
    }
	public function setDeliveryAddress($deliveryAddress = null) {
        $this->deliveryAddress = $deliveryAddress;
    }
    public function getDeliveryFee() {
        return $this->deliveryFee;
    }
    public function setDeliveryFee($deliveryFee = null) {
        $this->deliveryFee = $deliveryFee;
    }
    public function getConvenienceFee() {
        return $this->convenienceFee;
    }
    public function setConvenienceFee($convenienceFee = null) {
        $this->convenienceFee = $convenienceFee;
        return $this;
    }
    public function getFromTaa() {
        return $this->fromTaa;
    }
    	public function setFromTaa($fromTaa) {
        $this->fromTaa = $fromTaa;
    }
}