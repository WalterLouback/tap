<?php

namespace Zeedhi\ApiOrders\Model\Entities\Base;

class SerServiceSellerRel
{
    protected $evtEventSeller;
    
    protected $serService;
    
    protected $nrorg;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;

    protected $id;
    
    protected $status;  
    
    protected $initialTime;
    
    protected $finalTime;

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime();

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEvtEventSeller()
    {
        return $this->evtEventSeller;
    }

    public function setEvtEventSeller(\Zeedhi\ApiOrders\Model\Entities\EvtEventSeller $evtEventSeller = NULL)
    {
        $this->evtEventSeller = $evtEventSeller;

        return $this;
    }

    public function getSerService()
    {
        return $this->serService;
    }

    public function setSerService(\Zeedhi\ApiOrders\Model\Entities\SerService $serService = NULL)
    {
        $this->serService = $serService;

        return $this;
    }
    
    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
    
    public function getInitialTime()
    {
        return $this->initialTime;
    }

    public function setInitialTime($initialTime)
    {
        $this->initialTime = $initialTime;

        return $this;
    }

    public function getFinalTime()
    {
        return $this->finalTime;
    }

    public function setFinalTime($finalTime)
    {
        $this->finalTime = $finalTime;

        return $this;
    }
}
