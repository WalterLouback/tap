<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdMenuProductExtra {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct  */
    protected $ordMenuProduct;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdExtra  */
    protected $ordExtra;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $nrorg;
    /** @var int */
    protected $ordination;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
    
	public function getCreatedBy() {
        return $this->createdBy;
    }
    
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
    
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	

    public function getOrdExtra() {
        return $this->ordExtra;
    }

    public function setOrdExtra(\Zeedhi\ApiOrders\Model\Entities\OrdExtra $ordExtra = NULL) {
        $this->ordExtra = $ordExtra;
    }
    
	public function getOrdMenuProduct() {
        return $this->ordMenuProduct;
    }
    
	public function setOrdMenuProduct(\Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct $ordMenuProduct = NULL) {
        $this->ordMenuProduct = $ordMenuProduct;
    }
    
	public function getNrorg() {
        return $this->nrorg;
    }
    
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
    
    public function getOrdination() {
        return $this->ordination;
    }

    public function setOrdination($ordination) {
        $this->ordination = $ordination;
    }
}