<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdMenuProduct {
    
    
    /** @var float  */
    protected $price;
    /** @var \Datetime  */
    protected $finalTime;
    /** @var \Datetime  */
    protected $initialTime;
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $estimatedTime;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $image;
    /** @var string  */
    protected $detail;
    /** @var int  */
    protected $amount = 0;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdMenu  */
    protected $ordMenu;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdProduct  */
    protected $ordProduct;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdProductGroup  */
    protected $ordProductGroup;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $ordination;
    /** @var int  */
    protected $featured;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getAmount() {
        return $this->amount;
    }
	public function setAmount($amount) {
        $this->amount = $amount;
    }
    public function getPrice() {
        return $this->price;
    }
	public function setPrice($price = NULL) {
        $this->price = $price;
    }
	public function getFinalTime() {
        return $this->finalTime;
    }
	public function setFinalTime(\Datetime $finalTime = NULL) {
        $this->finalTime = $finalTime;
    }
	public function getInitialTime() {
        return $this->initialTime;
    }
	public function setInitialTime(\Datetime $initialTime = NULL) {
        $this->initialTime = $initialTime;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getImage() {
        return $this->image;
    }
	public function setImage($image) {
        $this->image = $image;
    }
	public function getDetail() {
        return $this->detail;
    }
	public function setDetail($detail = NULL) {
        $this->detail = $detail;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getEstimatedTime() {
        return $this->estimatedTime;
    }
	public function setEstimatedTime($estimatedTime) {
        $this->estimatedTime = $estimatedTime;
    }
	public function getOrdMenu() {
        return $this->ordMenu;
    }
	public function setOrdMenu(\Zeedhi\ApiOrders\Model\Entities\OrdMenu $ordMenu = NULL) {
        $this->ordMenu = $ordMenu;
    }
	public function getOrdProduct() {
        return $this->ordProduct;
    }
	public function setOrdProduct(\Zeedhi\ApiOrders\Model\Entities\OrdProduct $ordProduct = NULL) {
        $this->ordProduct = $ordProduct;
    }
	public function getOrdProductGroup() {
        return $this->ordProductGroup;
    }
	public function setOrdProductGroup(\Zeedhi\ApiOrders\Model\Entities\OrdProductGroup $ordProductGroup = NULL) {
        $this->ordProductGroup = $ordProductGroup;
    }
    public function getOrdination() {
        return $this->ordination;
    }   
    public function setOrdination($ordination = NULL) {
        $this->ordination = $ordination;
        return $this;
    }
    public function getFeatured() {
        return $this->featured;
    }
    public function setFeatured($featured = NULL) {
        $this->featured = $featured;
        return $this;
    }
}