<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class GenUserStructureRel {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel  */
    protected $genUserTypeRel;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure($genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUserTypeRel() {
        return $this->genUserTypeRel;
    }
	public function setGenUserTypeRel(\Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel $genUserTypeRel = NULL) {
        $this->genUserTypeRel = $genUserTypeRel;
    }
}