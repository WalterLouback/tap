<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class GenStatus {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $buttonName;
    /** @var string  */
    protected $labelName;
    /** @var integer  */
    protected $nrorg;
    /** @var string */
    protected $color;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStatus  */
    protected $next;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
    public function getButtonName() {
        return $this->buttonName;
    }
	public function setButtonName($buttonName = NULL) {
        $this->buttonName = $buttonName;
    }
    public function getLabelName() {
        return $this->labelName;
    }
	public function setLabelName($labelName = NULL) {
        $this->labelName = $labelName;
    }
    public function getColor() {
        return $this->color;
    }
	public function setColor($color = NULL) {
        $this->color = $color;
    }
    public function getNext() {
        return $this->next;
    }
    public function setNext(\Zeedhi\ApiOrders\Model\Entities\GenStatus $next = NULL) {
        $this->next = $next;
    }
}