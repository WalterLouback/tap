<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdConfigStore {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $deliversToTable;
    /** @var string  */
    protected $deliversToBalcony;
    /** @var string  */
    protected $deliversToAddress;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $event;
    /** @var \Zeedhi\ApiOrders\Model\Entities\OrdWorkflow  */
    protected $workflow;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
    protected $minValue;
    /** @var int  */
    protected $timerTaa;
    
    protected $askName;
    
    protected $acceptVoucher;
    
    protected $lockId;
    
    protected $establishmentId;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
    public function getDeliversToTable() {
        return $this->deliversToTable;
    }
	public function setDeliversToTable($deliversToTable) {
        $this->deliversToTable = $deliversToTable;
    }
    public function getDeliversToBalcony() {
        return $this->deliversToBalcony;
    }
	public function setDeliversToBalcony($deliversToBalcony) {
        $this->deliversToBalcony = $deliversToBalcony;
    }
    public function getDeliversToAddress() {
        return $this->deliversToAddress;
    }
	public function setDeliversToAddress($deliversToAddress) {
        $this->deliversToAddress = $deliversToAddress;
    }
	public function getEvent() {
        return $this->event;
    }
	public function setEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $event = NULL) {
        $this->event = $event;
    }
	public function getWorkflow() {
        return $this->workflow;
    }
	public function setWorkflow(\Zeedhi\ApiOrders\Model\Entities\OrdWorkflow $workflow = NULL) {
        $this->workflow = $workflow;
    }
    public function getMinValue() {
        return $this->minValue;
    }
    public function setMinValue($minValue) {
        $this->minValue = $minValue;
    }
       public function getTimerTaa() {
        return $this->timerTaa;
    }
    public function setTimerTaa($timerTaa) {
        $this->timerTaa = $timerTaa;
    }
    public function getAskName() {
        return $this->askName;
    }
    public function getAcceptVoucher() {
        return $this->acceptVoucher;
    }
    public function setAskName($askName) {
        $this->askName = $askName;
    }
    public function setAcceptVoucher($acceptVoucher) {
        $this->acceptVoucher = $acceptVoucher;
    }
    public function getLockId() {
        return $this->lockId;
    }
    public function setLockId($lockId) {
        $this->lockId = $lockId;
    }
      public function getEstablishmentId() {
        return $this->establishmentId;
    }
    public function setEstablishmentId($establishmentId) {
        $this->establishmentId = $establishmentId;
    }
}