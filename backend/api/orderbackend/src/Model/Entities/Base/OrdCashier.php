<?php

namespace Zeedhi\ApiOrders\Model\Entities\Base;

/**
 * OrdCashier
 */
class OrdCashier
{
    /**
     * @var datetime|null
     */
    protected $openning;

    /**
     * @var datetime|null
     */
    protected $closing;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var \Zeedhi\ApiOrders\Model\Entities\EvtEventSeller
     */
    protected $eventSeller;

    /** @var int  */
    protected $nrorg;
    
    /** @var int  */
    protected $createdAt;
    
    /** @var int  */
    protected $createdBy;
    
    /** @var \Datetime  */
    protected $modifiedAt;
    
    /** @var int  */
    protected $modifiedBy;
    
    /**
     * Set openning.
     *
     * @param string|null $openning
     *
     * @return OrdCashier
     */
    public function setOpenning($openning = null)
    {
        $this->openning = $openning;

        return $this;
    }

    /**
     * Get openning.
     *
     * @return string|null
     */
    public function getOpenning()
    {
        return $this->openning;
    }

    /**
     * Set closing.
     *
     * @param string|null $closing
     *
     * @return OrdCashier
     */
    public function setClosing($closing = null)
    {
        $this->closing = $closing;

        return $this;
    }

    /**
     * Get closing.
     *
     * @return string|null
     */
    public function getClosing()
    {
        return $this->closing;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventSeller.
     *
     * @param \Zeedhi\ApiOrders\Model\Entities\EvtEventSeller|null $eventSeller
     *
     * @return OrdCashier
     */
    public function setEventSeller(\Zeedhi\ApiOrders\Model\Entities\EvtEventSeller $eventSeller = null)
    {
        $this->eventSeller = $eventSeller;

        return $this;
    }

    /**
     * Get eventSeller.
     *
     * @return \Zeedhi\ApiOrders\Model\Entities\EvtEventSeller|null
     */
    public function getEventSeller()
    {
        return $this->eventSeller;
    }
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
    
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
    
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getNrorg() {
        return $this->nrorg;
    }
    
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
}
