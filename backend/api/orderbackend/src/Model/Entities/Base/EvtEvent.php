<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class EvtEvent {
    
    
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $provincy;
    /** @var string  */
    protected $city;
    /** @var string  */
    protected $neighborhood;
    /** @var string  */
    protected $street;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $imageMap;
    /** @var string  */
    protected $imageLogo;
    /** @var string  */
    protected $imageCover;
    /** @var \Datetime  */
    protected $initialDate;
    /** @var \Datetime  */
    protected $finalDate;
    /** @var string  */
    protected $address;
    /** @var string  */
    protected $about;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $parentEvent;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenUser  */
    protected $ownerUser;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStructure  */
    protected $genStructure;
    
    protected $timerTaa;
    
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var string  */
    protected $values;
    /** @var string  */
    protected $latitude;
    /** @var string  */
    protected $longitude;
    /** @var string  */
    protected $urlApiExternal;
    /** @var string  */
    protected $tokenApiExternal;
    /** @var string  */
    protected $originApiExternal;
    /** @var string  */
    protected $externalId;  
    /** @var string  */
    protected $integrationCommands;
    /** @var string  */
    protected $ipStore;
    /** @var string  */
    protected $stoneRecipientId;
    /** @var string  */
    
    
	public function getValues() {
        return $this->values;
    }
    public function setValues($values = NULL) {
        $this->values = $values;
    }
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

	public function getOwnerUser() {
        return $this->ownerUser;
    }
	public function setOwnerUser(\Zeedhi\ApiOrders\Model\Entities\GenUser $ownerUser = NULL) {
        $this->ownerUser = $ownerUser;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type = NULL) {
        $this->type = $type;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getStructure() {
        return $this->genStructure;
    }
	public function setStructure(\Zeedhi\ApiOrders\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getProvincy() {
        return $this->provincy;
    }
	public function setProvincy($provincy = NULL) {
        $this->provincy = $provincy;
    }
	public function getCity() {
        return $this->city;
    }
	public function setCity($city = NULL) {
        $this->city = $city;
    }
	public function getNeighborhood() {
        return $this->neighborhood;
    }
	public function setNeighborhood($neighborhood = NULL) {
        $this->neighborhood = $neighborhood;
    }
	public function getStreet() {
        return $this->street;
    }
	public function setStreet($street = NULL) {
        $this->street = $street;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getImageMap() {
        return $this->imageMap;
    }
	public function setImageMap($imageMap = NULL) {
        $this->imageMap = $imageMap;
    }
	public function getImageLogo() {
        return $this->imageLogo;
    }
	public function setImageLogo($imageLogo = NULL) {
        $this->imageLogo = $imageLogo;
    }
	public function getImageCover() {
        return $this->imageCover;
    }
	public function setImageCover($imageCover = NULL) {
        $this->imageCover = $imageCover;
    }
	public function getInitialDate() {
        return $this->initialDate;
    }
	public function setInitialDate(\Datetime $initialDate = NULL) {
        $this->initialDate = $initialDate;
    }
	public function getFinalDate() {
        return $this->finalDate;
    }
	public function setFinalDate(\Datetime $finalDate = NULL) {
        $this->finalDate = $finalDate;
    }
	public function getAbout() {
        return $this->about;
    }
	public function setAbout($about = NULL) {
        $this->about = $about;
    }
	public function getParentEvent() {
        return $this->parentEvent;
    }
	public function setParentEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $parentEvent = NULL) {
        $this->parentEvent = $parentEvent;
    }
    public function getLatitude() {
        return $this->latitude;
    }
	public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
	public function getLongitude() {
        return $this->longitude;
    }
	public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
	public function geturlApiExternal() {
        return $this->urlApiExternal;
    }
	public function seturlApiExternal($urlApiExternal) {
        $this->urlApiExternal = $urlApiExternal;
    }  
	public function gettokenApiExternal() {
        return $this->tokenApiExternal;
    }
	public function settokenApiExternal($tokenApiExternal) {
        $this->tokenApiExternal = $tokenApiExternal;
    }  
	public function getOriginApiExternal() {
        return $this->originApiExternal;
    }
	public function setOriginApiExternal($originApiExternal) {
        $this->originApiExternal = $originApiExternal;
    }    
	public function getExternalId() {
        if ($this->externalId != null && $this->externalId != "") { 
            return $this->formatNumber($this->externalId); }
        else { 
            return $this->externalId;
        }
    }
	public function setExternalId($externalId) {
        $this->externalId = $externalId;
    }    
	public function getIpStore() {
        return $this->ipStore;
    }
	public function setIpStore($ipStore) {
        $this->ipStore = $ipStore;
    }     
	public function getIntegrationCommands() {
        return $this->integrationCommands;
    }
	public function setIntegrationCommands($integrationCommands) {
        $this->integrationCommands = $integrationCommands;
    }     
    public function getStoneRecipientId()
    {
        return $this->stoneRecipientId;
    }

    public function setStoneRecipientId($stoneRecipientId=NULL)
    {
        $this->stoneRecipientId = $stoneRecipientId;
        return $this;
    }
     public function getTimerTaa()
    {
        return $this->timerTaa;
    }

    public function setTimerTaa($timerTaa)
    {
        $this->timerTaa = $timerTaa;
        return $this;
    }
    
    /*manipulando resultados*/
    function formatNumber($num) {
    	$amount = strlen($num);
    	$result = "";
    		
    	switch ($amount) {
            case 1:
                $result = "000" . $num;
        		break;
            case 2:
                $result = "00" . $num;
        		break;
            case 3:
                $result = "0" . $num;
        		break;
        	default:
        	    $num;
    	}
    	
    	return $result;
    }
}