<?php

namespace Zeedhi\ApiOrders\Model\Entities\Base;

class OrdLockOpening
{
    protected $nrorg;

    protected $id;

    protected $lockId;
    
    protected $status;
    
    protected $ordOrder;

    protected $createdAt;

    protected $modifiedAt;

    protected $createdBy;

    protected $modifiedBy;
    
    protected $doorNumber;

    public function getLockId(): ?int
    {
        return $this->lockId;
    }

    public function setLockId(?int $lockId): self
    {
        $this->lockId = $lockId;

        return $this;
    }

    public function getNrorg(): ?int
    {
        return $this->nrorg;
    }

    public function setNrorg(?int $nrorg): self
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

   public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

   public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?string $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrdOrder(): ?OrdOrder
    {
        return $this->ordOrder;
    }

    public function setOrdOrder(?OrdOrder $ordOrder): self
    {
        $this->ordOrder = $ordOrder;

        return $this;
    }
    
    public function getDoorNumber(): ?int 
    {
        return $this->doorNumber;
    }

    public function setDoorNumber(?int $doorNumber): self
    {
        $this->doorNumber = $doorNumber;

        return $this;
    }
    
}
