<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdProduct {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var int  */
    protected $price;
    /** @var string  */
    protected $name;
    /** @var string  */
    protected $image;
    /** @var string  */
    protected $detail;
    /** @var string  */
    protected $externalId;
     /** @var string  */
    protected $barCode;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;

	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getPrice() {
        return $this->price;
    }
	public function setPrice($price) {
        $this->price = $price;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getImage() {
        return $this->image;
    }
	public function setImage($image) {
        $this->image = $image;
    }
	public function getDetail() {
        return $this->detail;
    }
	public function setDetail($detail = NULL) {
        $this->detail = $detail;
    }
	public function getExternalId() {
        return $this->externalId;
    }
	public function setExternalId($externalId = NULL) {
        $this->externalId = $externalId;
    }
    	public function getBarCode() {
        return $this->barCode;
    }
	public function setBarCode($barCode = NULL) {
        $this->barCode = $barCode;
    }
}