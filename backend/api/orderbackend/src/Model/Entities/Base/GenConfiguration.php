<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class GenConfiguration {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $logoImage;
    /** @var string  */
    protected $fcmServerKey;
    /** @var string  */
    protected $awsS3AccessKey;
    /** @var string  */
    protected $awsS3SecretKey;
    /** @var string  */
    protected $awsS3Bucket;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var decimal  */
    protected $convenienceFee;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getMerchantKey() {
        return $this->merchantKey;
    }
	public function setMerchantKey($merchantKey = NULL) {
        $this->merchantKey = $merchantKey;
    }
	public function getLogoImage() {
        return $this->logoImage;
    }
	public function setLogoImage($logoImage = NULL) {
        $this->logoImage = $logoImage;
    }
	public function getFcmServerKey() {
        return $this->fcmServerKey;
    }
	public function setFcmServerKey($fcmServerKey = NULL) {
        $this->fcmServerKey = $fcmServerKey;
    }
    public function getAwsS3AccessKey() {
        return $this->awsS3AccessKey;
    }
    public function setAwsS3AccessKey($awsS3AccessKey) {
        $this->awsS3AccessKey = awsS3AccessKey;
    }
    public function getAwsS3SecretKey() {
        return $this->awsS3SecretKey;
    }
    public function setAwsS3SecretKey($awsS3SecretKey) {
        $this->awsS3SecretKey = $awsS3SecretKey;
    }
    public function getAwsS3Bucket() {
        return $this->awsS3Bucket;
    }
    public function setAwsS3Bucket($awsS3Bucket) {
        $this->awsS3Bucket = awsS3Bucket;
    }
    public function getConvenienceFee() {
        return $this->convenienceFee;
    }
    public function setConvenienceFee($convenienceFee = null) {
        $this->convenienceFee = $convenienceFee;
        return $this;
    }
}