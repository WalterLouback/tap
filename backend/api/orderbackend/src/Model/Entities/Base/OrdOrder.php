<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class OrdOrder {
    
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $id;
    /** @var decimal  */
    protected $total;
    /** @var integer  */
    protected $orderIdentifier;
    /** @var string  */
    protected $deliverTo;
    /** @var string  */
    protected $nsu;
    /** @var string  */
    protected $transactionId;
    /** @var string  */
    protected $paymentStatus;
    /** @var string  */
    protected $paymentMethod;
    /** @var string  */
    protected $orderKey;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEventSeller  */
    protected $evtEventSeller;
    /** @var \Zeedhi\ApiOrders\Model\Entities\PayCreditcard  */
    protected $payCreditcard;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiOrders\Model\Entities\GenStatus  */
    protected $status;
    /** @var string */
    protected $note;
    /** @var string */
    protected $type;
    /** @var float */
    protected $deliveryFee;
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
    protected $orderSheet;
    /** @var int */
    protected $deliveryAddress;
    /** @var decimal  */
    protected $convenienceFee;
    /** @var string */
    protected $OAReponse;
    /** @var string */
    protected $OAsuccess;
    
    protected $fromTaa;

	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
	public function getType() {
        return $this->type;
    }
    public function setType($type = NULL) {
        $this->type = $type;
    }
    public function getNote() {
        return $this->note;
    }
    public function setNote($note = NULL) {
        $this->note = $note;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getDeliverTo() {
        return $this->deliverTo;
    }
	public function setDeliverTo($deliverTo) {
        $this->deliverTo = $deliverTo;
    }
	public function getOrderIdentifier() {
        return $this->orderIdentifier;
    }
	public function setOrderIdentifier($orderIdentifier) {
        $this->orderIdentifier = $orderIdentifier;
    }
	public function getNsu() {
        return $this->nsu;
    }
	public function setNsu($nsu) {
        $this->nsu = $nsu;
    }
	public function getTransactionId() {
        return $this->transactionId;
    }
	public function setTransactionId($transactionId) {
        $this->transactionId = $transactionId;
    }
	public function getPaymentStatus() {
        return $this->paymentStatus;
    }
	public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }
	public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
    }
	public function getOrderKey() {
        return $this->orderKey;
    }
	public function setOrderKey($orderKey = NULL) {
        $this->orderKey = $orderKey;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus(\Zeedhi\ApiOrders\Model\Entities\GenStatus $status = NULL) {
        $this->status = $status;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiOrders\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getEvtEventSeller() {
        return $this->evtEventSeller;
    }
	public function setEvtEventSeller(\Zeedhi\ApiOrders\Model\Entities\EvtEventSeller $evtEventSeller = NULL) {
        $this->evtEventSeller = $evtEventSeller;
    }
	public function getPayCreditcard() {
        return $this->payCreditcard;
    }
	public function setPayCreditcard(\Zeedhi\ApiOrders\Model\Entities\PayCreditcard $payCreditcard = NULL) {
        $this->payCreditcard = $payCreditcard;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiOrders\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
	}
    public function getOrderSheet() {
        return $this->orderSheet;
    }
	public function setOrderSheet($orderSheet = null) {
        $this->orderSheet = $orderSheet;
    }
    public function getDeliveryAddress() {
        return $this->deliveryAddress;
    }
	public function setDeliveryAddress($deliveryAddress = null) {
        $this->deliveryAddress = $deliveryAddress;
    }
    public function getConvenienceFee() {
        return $this->convenienceFee;
    }
    public function setConvenienceFee($convenienceFee = null) {
        $this->convenienceFee = $convenienceFee;
    }
    public function getDeliveryFee() {
        return $this->deliveryFee;
    }
    public function setDeliveryFee($deliveryFee = null) {
        $this->deliveryFee = $deliveryFee;
        return $this;
    }
    public function getOAReponse() {
        return $this->OAReponse;
    }

    public function setOAReponse($OAReponse = null) {
        $this->OAReponse = $OAReponse;
    }
    public function getOAsuccess() {
        return $this->OAsuccess;
    }

    public function setOAsuccess($OAsuccess = null) {
        $this->OAsuccess = $OAsuccess;
    }
    public function getFromTaa() {
        return $this->fromTaa;
    }
    	public function setFromTaa($fromTaa) {
        $this->fromTaa = $fromTaa;
    }
}