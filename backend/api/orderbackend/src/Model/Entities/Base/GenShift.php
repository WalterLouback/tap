<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;


abstract class GenShift {
    
    
    /** @var \Datetime  */
    protected $finalTime;
    /** @var \Datetime  */
    protected $initialTime;
    /** @var string  */
    protected $day;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEventMenu  */
    protected $evtEventMenu;
    /** @var \Zeedhi\ApiOrders\Model\Entities\EvtEvent  */
    protected $evtEvent;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getFinalTime() {
        return $this->finalTime;
    }
	public function setFinalTime(\Datetime $finalTime = NULL) {
        $this->finalTime = $finalTime;
    }
	public function getInitialTime() {
        return $this->initialTime;
    }
	public function setInitialTime(\Datetime $initialTime = NULL) {
        $this->initialTime = $initialTime;
    }
	public function getDay() {
        return $this->day;
    }
	public function setDay($day = NULL) {
        $this->day = $day;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtEventMenu() {
        return $this->evtEventMenu;
    }
	public function setEvtEventMenu(\Zeedhi\ApiOrders\Model\Entities\EvtEventMenu $evtEventMenu = NULL) {
        $this->evtEventMenu = $evtEventMenu;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}