<?php
namespace Zeedhi\ApiOrders\Model\Entities\Base;

abstract class EvtEventDeliver {
    
    protected $id;
    /*string*/
    protected $distanceKm;
    /*decimal*/
    protected $price;
    /*bigint*/
    protected $nrorg;
    /*datetime*/
    protected $createdBy;
    /*datetime*/
    protected $modifiedBy;
    /*bigint*/
    protected $createdAt;
    /*bigint*/
    protected $modifiedAt;
    /*bigint*/
    protected $evtEvent;
    /*bigint*/
    protected $startKm;
    /*bigint*/
    protected $endKm;

    public function getNrorg()
    {
        return $this->nrorg;
    }

    public function setNrorg($nrorg)
    {
        $this->nrorg = $nrorg;

        return $this;
    }

    public function getDistanceKm()
    {
        return $this->distanceKm;
    }

    public function setDistanceKm($distanceKm)
    {
        $this->distanceKm = $distanceKm;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt = NULL)
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt($modifiedAt = NULL)
    {
        $date = new \DateTime();
        $this->modifiedAt = $date->format('Y-m-d H:i:s');

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEvtEvent()
    {
        return $this->evtEvent;
    }

    public function setEvtEvent(\Zeedhi\ApiOrders\Model\Entities\EvtEvent $evtEvent = NULL)
    {
        $this->evtEvent = $evtEvent;

        return $this;
    }
    public function getStartKm()
    {
        return $this->startKm;
    }
    public function setStartKm($startKm)
    {
        $this->startKm = $startKm;
        return $this;
    }
    public function getEndKm()
    {
        return $this->endKm;
    }
    public function setEndKm($endKm)
    {
        return $this->endKm = $endKm;
        return $this;
    }    

}
