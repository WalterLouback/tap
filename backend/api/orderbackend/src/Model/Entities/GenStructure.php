<?php
namespace Zeedhi\ApiOrders\Model\Entities;

class GenStructure extends \Zeedhi\ApiOrders\Model\Entities\Base\GenStructure {
    
    public function build($entityManager=NULL) {}
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['description'] = $this->getDescription();
        $array['level'] = $this->getLevel();
        $array['externalQrcode'] = $this->getExternalQrcode();
        $array['address'] = $this->getGenAddress();
        $array['status'] = $this->getStatus();
        $array['store'] = isset($array['store']) ? $this->getEvtEvent()->toArray() : null;
        
        return $array;
    }
    
    public static function manyToArray($structures) {
        $arrays = [];
        foreach ($structures as $structure) {
            array_push($arrays, $structure->toArray());
        }
        
        return $arrays;
    }
}