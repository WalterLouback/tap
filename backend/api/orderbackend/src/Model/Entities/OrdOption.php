<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiOrders\Helpers\General as General;

class OrdOption extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdOption {
    
    public static function manyToArray($options) {
        $arrays = [];
        foreach ($options as $option) {
            $array = $option->toArray();
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function getClone($entityManager) {
        $newOption = new OrdOption();
        $newOption->setName($this->getName());
        $newOption->setPrice($this->getPrice());
        $newOption->setActive($this->getActive());
        
        $entityManager->persist($newOption);
        
        return $newOption;
    }
    
    public function toArray() {
        $array['id']            = $this->getId();
        $array['name']          = $this->getName();
        $array['price']         = $this->getPrice();
        $array['active']        = $this->getActive();
        $array['ordination']    = $this->getOrdination();
        
        return $array;
    }
    
}