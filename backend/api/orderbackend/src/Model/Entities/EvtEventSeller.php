<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class EvtEventSeller extends \Zeedhi\ApiOrders\Model\Entities\Base\EvtEventSeller {
    
    function build($entityManager, $nrorg) {
        $this->getGenUser()->build($entityManager, $nrorg);
        $this->setAvailable($entityManager);
    }
    
    function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_DATA' => $this->getGenUser() ? $this->getGenUser()->toArray() : [],
            'SELLER_TYPE' => $this->getEvtSellerType() ? $this->getEvtSellerType()->toArray() : [],
            'AVAILABLE' => $this->getAvailable(),
        );
    }
    
    static function manyToArray($sellers) {
        $array = [];
        foreach ($sellers as $seller) {
            array_push($array, $seller->toArray());
        }
        return $array;
    }
    
        public function getAvailable() {
        return property_exists($this, 'available') ? $this->available : NULL;
    }
    
    public function setAvailable($entityManager) {
        $sellerId = $this->getId();
        $this->available = $entityManager->createQuery(
            "
            SELECT SER.initialTime, SER.finalTime 
            FROM ".  SerServiceSellerRel::class ." SER
            WHERE SER.evtEventSeller = $sellerId AND SER.status = 'A' 
            "            
        )->getResult();
    }

}