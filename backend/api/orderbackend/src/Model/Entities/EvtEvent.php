<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiOrders\Helpers\General as General;
use Zeedhi\ApiOrders\Model\Entities\OrdMenu;
use Zeedhi\ApiOrders\Model\Entities\EvtEventMenu;

class EvtEvent extends \Zeedhi\ApiOrders\Model\Entities\Base\EvtEvent {
    
    public function build($entityManager, $userId=NULL, $allMenus=TRUE, $horaAtual) {
        $this->setMenus($entityManager, $allMenus, $horaAtual);
        if($allMenus) $this->setAllMenus($entityManager, $horaAtual);
        $this->setAcceptVoucher($entityManager);
        $this->setPaymentMethods($entityManager);
        $this->setMinValue($entityManager);
        $this->setDeliversToTable($entityManager);
        $this->setDeliversToBalcony($entityManager);
        $this->setDeliversToAddress($entityManager);
        $this->setPaymentMoment($entityManager);
        $this->setContactMethods($entityManager);
        $this->isOpened($entityManager);
        $this->setWorkshifts($entityManager);
        $this->setMerchantKey($entityManager);
        $this->setAdminUsers($entityManager);
        $this->setMinAndMaxTime($entityManager);
        $this->setChildren($entityManager);
        $this->setAddress($entityManager);
        $this->setTimerTaa($entityManager);
        $this->setTempo($horaAtual);
        $this->setAskName($entityManager);
        $this->setLockId($entityManager);
        $this->setEstablishmentId($entityManager);
        if ($userId) $this->setSellerData($entityManager, $userId);
    }
    
    public function specyficBiuld($entityManager, $userId=NULL) {
        $this->setMenusOne($entityManager);
        $this->setAllMenus($entityManager);
        $this->setPaymentMethods($entityManager);
        $this->setMinValue($entityManager);
        $this->setDeliversToTable($entityManager);
        $this->setDeliversToBalcony($entityManager);
        $this->setDeliversToAddress($entityManager);
        $this->setPaymentMoment($entityManager);
        $this->setContactMethods($entityManager);
        $this->isOpened($entityManager);
        $this->setWorkshifts($entityManager);
        $this->setMerchantKey($entityManager);
        $this->setMinAndMaxTime($entityManager);
        $this->setChildren($entityManager);
        $this->setAddress($entityManager);
        $this->setTimerTaa($entityManager);

        if ($userId) $this->setSellerData($entityManager, $userId);
    }
    
    public function getMenus() {
        return property_exists($this, 'menus') ? $this->menus : [];
    }
    
    public function getAllMenus() {
        return property_exists($this, 'allMenus') ? $this->allMenus : [];
    }
    
    public function getPaymentMethods() {
        return property_exists($this, 'paymentMethods') ? $this->paymentMethods : [];
    }
    
    public function getDeliversToTable() {
        return property_exists($this, 'deliversToTable') ? $this->deliversToTable : 'F';
    }
    
    public function getDeliversToBalcony() {
        return property_exists($this, 'deliversToBalcony') ? $this->deliversToBalcony : 'F';
    }
    
    public function getDeliversToAddress() {
        return property_exists($this, 'deliversToAddress') ? $this->deliversToAddress : 'F';
    }

    public function getPaymentMoment() {
        return property_exists($this, 'paymentMoment') ? $this->paymentMoment : NULL;
    }

    public function getMerchantKey() {
        return property_exists($this, 'merchantKey') ? $this->merchantKey : NULL;
    }

    public function getMerchantId() {
        return property_exists($this, 'merchantId') ? $this->merchantId : NULL;
    }

    public function getContactMethods() {
        return property_exists($this, 'contactMethods') ? $this->contactMethods : [];
    }
    
    function getWorkshifts() {
        return property_exists($this, 'workshifts') ? $this->workshifts : [];
    }
    
    function getSellerData() {
        return property_exists($this, 'sellerData') ? $this->sellerData : NULL;
    }
    
    function getAdminUsers() {
        return property_exists($this, 'adminUsers') ? $this->adminUsers : [];
    }
    
    function getMinTime() {
        return property_exists($this, 'minTime') ? $this->minTime : 0;
    }
    
    function getMaxTime() {
        return property_exists($this, 'maxTime') ? $this->maxTime : 0;
    }
    
    function getMinValue() {
        return property_exists($this, 'minValue') ? $this->minValue : 0;
    }
     function getTimerTaa() {
        return property_exists($this, 'timerTaa') ? $this->timerTaa : null;
    }
    
    function getChildren() {
        return property_exists($this, 'children') ? $this->children : [];
    }
    
    public function getAddress() {
        return property_exists($this, 'address') ? $this->address : [];
    }
    public function getTempo() {
        return property_exists($this, 'horaAtual') ? $this->horaAtual : [];
    }
    public function setTempo($horaAtual) {
       $this->horaAtual = $horaAtual;
    }
     function getAskName() {
        return property_exists($this, 'askName') ? $this->askName : null;
    }
     function getAcceptVoucher() {
        return property_exists($this, 'acceptVoucher') ? $this->acceptVoucher : null;
    }
     function getLockId() {
        return property_exists($this, 'lockId') ? $this->lockId : null;
    }
     function getEstablishmentId() {
        return property_exists($this, 'establishmentId') ? $this->establishmentId : null;
}
    
    public function setAddress($entityManager) {
        $evtEvent = $this->getId();
        $address = $entityManager->createQuery(
            "
            SELECT a
            FROM " . GenAddress::class . " a
            WHERE a.evtEvent = $evtEvent
            AND a.status = 'A'
            ORDER BY a.createdAt DESC
            "
        )->getResult();
        if (count($address) > 0) $this->address = $address;
    }
    
    function setWorkshifts($entityManager) {
        $storeId = $this->getId();
        $this->workshifts = $entityManager->createQuery(
            "
            SELECT s
            FROM 'Zeedhi\ApiOrders\Model\Entities\GenShift' s
            WHERE s.evtEvent = $storeId
            "
        )->getResult();
    }
    
    public function getShiftsOfDay($date, $entityManager) {
        $workshift = 'Zeedhi\ApiOrders\Model\Entities\GenShift';
        $storeId   = $this->getId();
        
        $dayOfWeek = strftime("%A", strtotime($date));

        $daysOfWeeksArray = array(
            'Sunday'    => 0, 'Monday' => 1, 'Tuesday'   => 2, 'Wednesday' => 3, 
            'Thursday'  => 4, 'Friday' => 5, 'Saturday'  => 6
        );

        $formatedDay = $daysOfWeeksArray[$dayOfWeek];
        
        $dayWorkshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime 
            FROM $workshift ws
            WHERE ws.evtEvent = $storeId
            AND ws.day = $formatedDay
            "
        )->getResult();
        
        return $dayWorkshifts;
    }
    
    public function isOpened($entityManager=NULL) {
        if (!$entityManager) {
            if (property_exists($this, 'opened')) return $this->opened;
            else return NULL;
        }
        
        $opened      = false;
        
        /* Obter status da loja para verificar se ela foi fechada ou aberta manualmente */
        $storeStatus = $this->getStatus();
        
        if ($storeStatus == 'O') {
            $opened  = true;
        }
        else if ($storeStatus != 'C') {
            
            /* Obter horários de funcionamento da loja no dia de hoje */
            $today        = new \DateTime();
            $todayClone   = new \DateTime();
            
            /* Definindo os turnos do dia */
            $formatedDate = $todayClone->format('Y-m-d H:i:s');
            $shifts       = $this->getShiftsOfDay($formatedDate, $entityManager);
            /* Adicionando fuso horário */
            $today->sub(new \DateInterval('PT2H'));
            
            /* Percorrer os turnos e verificar se horário atual se encaixa em algum */
            foreach ($shifts as $shift) {
                /* Obtendo horário atual */
                $currHours = $today->format('H');
                $currMins  = $today->format('i');
                /* Obtendo horário de início do turno */
                $inDate    = clone $shift['initialTime'];
                $inDate->sub(new \DateInterval('PT2H'));
                $inHours   = $inDate->format('H');
                $inMins    = $inDate->format('i');
                /* Obtendo horário de término do turno */
                $finDate   = clone $shift['finalTime'];
                $finDate->sub(new \DateInterval('PT2H'));
                $finHours  = $finDate->format('H');
                $finMins   = $finDate->format('i');
                
                /* Verificando se horário atual se encaixa no turno */
                if ($currHours > $inHours || ($currHours == $inHours && $currMins >= $inMins)) {
                    if ($currHours < $finHours || ($currHours == $finHours && $currMins <= $finMins)) {
                        $opened = true;
                        break;
                    }
                }
            }
        }
        
        $this->opened = $opened;
        
        return $opened;
    }
    
    public function setMenus($entityManager, $allMenus=TRUE, $horaAtual) {
        $menu      = OrdMenu::class;
        $eventMenu = EvtEventMenu::class;
        $storeId = $this->getId();
        $menus   = $entityManager->createQuery(
            "
            SELECT m 
            FROM $menu m
            JOIN $eventMenu em WITH em.ordMenu = m
            JOIN em.evtEvent as s 
            WHERE s.id = $storeId 
            ORDER BY m.name
            "
        )->getResult();
        
        $currentWorkshift    = [];
        $notCurrentWorkshift = [];
        
        foreach ($menus as $menu) {
            $menu->build($horaAtual, $entityManager, $this->getId());
            if ($menu->isCurrentWorkshift($horaAtual) == true) {
                array_push($currentWorkshift, $menu);
            } else if($allMenus) {
                array_push($notCurrentWorkshift, $menu);
            }
        }
        
        $menus = [ 'currentWorkshift'    => $currentWorkshift,
                   'notCurrentWorkshift' => $notCurrentWorkshift ];
        
        $this->menus = $menus;
    }
    
    public function setAllMenus($entityManager, $horaAtual) {
        $nrorg   = $this->getNrorg();
        $allMenus   = $entityManager->createQuery(
            "
            SELECT m 
            FROM " . OrdMenu::class . " m
            JOIN " . EvtEventMenu::class . " em WITH em.ordMenu = m
            JOIN em.evtEvent as s 
            WHERE s.nrorg = '$nrorg'
            ORDER BY m.name
            "
        )->getResult();
        
        $currentWorkshift    = [];
        $notCurrentWorkshift = [];
        
        foreach ($allMenus as $menu) {
            $menu->build($entityManager, $this->getId());
            if ($menu->isCurrentWorkshift($horaAtual) == true) {
                array_push($currentWorkshift, $menu);
            } else {
                array_push($notCurrentWorkshift, $menu);
            }
        }
        
        $allMenus = [ 'allCurrentWorkshift'    => $currentWorkshift,
                      'allNotCurrentWorkshift' => $notCurrentWorkshift ];
        
        $this->allMenus = $allMenus;
    }
    
    public function setMenusOne($entityManager) {
        $menu      = 'Zeedhi\ApiOrders\Model\Entities\OrdMenu';
        $eventMenu = 'Zeedhi\ApiOrders\Model\Entities\EvtEventMenu';
        $storeId = $this->getId();
        $menus   = $entityManager->createQuery(
            "
            SELECT m 
            FROM $menu m
            JOIN $eventMenu em WITH em.ordMenu = m
            JOIN em.evtEvent as s 
            WHERE s.id = $storeId 
            ORDER BY m.name
            "
        )->getResult();
        
        $currentWorkshift    = [];
        $notCurrentWorkshift = [];
        
        $menus = [ 'currentWorkshift'    => $currentWorkshift,
                   'notCurrentWorkshift' => $notCurrentWorkshift ];
        
        $this->menus = $menus;
    }
    
    public function setPaymentMethods($entityManager) {
        $storeId = $this->getId();
        if ($this->acceptVoucher == true) {
            $this->paymentMethods = $entityManager->createQuery(
                "
                SELECT pm
                FROM '\Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod' pm
                WHERE pm.paymentMethod LIKE '%TAA'
                "    
        )->getResult();
        } elseif ($this->acceptVoucher == false || $this->acceptVoucher == null) {
             $this->paymentMethods = $entityManager->createQuery(
                "
                SELECT pm
                FROM '\Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod' pm
                WHERE pm.paymentMethod IN ('TEF_CC_TAA', 'TEF_DC_TAA')
                "    
        )->getResult(); 
        }
       if ($this->paymentMethods== null){
             $this->paymentMethods = $entityManager->createQuery(
            "
            SELECT pm
            FROM '\Zeedhi\ApiOrders\Model\Entities\PayPaymentMethod' pm
            JOIN '\Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c WITH c.event = $storeId
            JOIN '\Zeedhi\ApiOrders\Model\Entities\OrdStorePayMethod' spm WITH pm.id = spm.paymentMethod
            JOIN spm.ordConfigStore c2
            WHERE c.id = c2.id
            "    
        )->getResult();
        }
    }
    
    public function setContactMethods($entityManager) {
        $storeId = $this->getId();
        $this->contactMethods = $entityManager->createQuery(
            "
            SELECT c
            FROM \Zeedhi\ApiOrders\Model\Entities\GenContact c
            JOIN c.evtEvent s
            WHERE s.id = $storeId
            "
        )->getResult();
    }
    
    public function setMinValue($entityManager) {
        $storeId = $this->getId();
        $this->minValue = $entityManager->createQuery(
            "
            SELECT c.minValue
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->minValue) > 0) $this->minValue = $this->minValue[0]['minValue'];
    }
     public function setTimerTaa($entityManager) {
        $storeId = $this->getId();
        $this->timerTaa = $entityManager->createQuery(
            "
            SELECT c.timerTaa
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->timerTaa) > 0) $this->timerTaa = $this->timerTaa[0]['timerTaa'];
    }
    
    public function setDeliversToTable($entityManager) {
        $storeId = $this->getId();
        $this->deliversToTable = $entityManager->createQuery(
            "
            SELECT c.deliversToTable
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->deliversToTable) > 0) $this->deliversToTable = $this->deliversToTable[0]['deliversToTable'];
    }
    
    public function setDeliversToBalcony($entityManager) {
        $storeId = $this->getId();
        $deliversToBalcony = $entityManager->createQuery(
            "
            SELECT c.deliversToBalcony
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($deliversToBalcony) > 0) $this->deliversToBalcony = $deliversToBalcony[0]['deliversToBalcony'];
    }
    
    public function setDeliversToAddress($entityManager) {
        $storeId = $this->getId();
        $deliversToAddress = $entityManager->createQuery(
            "
            SELECT c.deliversToAddress
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($deliversToAddress) > 0) $this->deliversToAddress = $deliversToAddress[0]['deliversToAddress'];
    }
    
    public function setPaymentMoment($entityManager) {
        $storeId       = $this->getId();
        
        /* If store has its own workflow */
        $paymentMoment = $this->getPaymentMomentFromEventWorkflow($entityManager, $storeId);
        if (count($paymentMoment) > 0) $this->paymentMoment = $paymentMoment[0];
        else {
            /* Else get organization's workflow */
            $paymentMoment = $this->getPaymentMomentFromOrganizationWorkflow($entityManager, $this->getNrorg());
            if (count($paymentMoment) > 0) $this->paymentMoment = $paymentMoment[0];
        }
    }
    
    private function getPaymentMomentFromEventWorkflow($entityManager, $storeId) {
        return $entityManager->createQuery(
            "
            SELECT pm
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            JOIN 'Zeedhi\ApiOrders\Model\Entities\OrdWorkflow' w WITH c.workflow = w.id
            JOIN 'Zeedhi\ApiOrders\Model\Entities\GenStatus' pm WITH w.paymentMoment = pm.id
            WHERE c.event = $storeId
            "
        )->getResult();
    }
    
    private function getPaymentMomentFromOrganizationWorkflow($entityManager, $nrorg) {
        return $entityManager->createQuery(
            "
            SELECT pm
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdWorkflow' w
            JOIN 'Zeedhi\ApiOrders\Model\Entities\GenStatus' pm WITH w.paymentMoment = pm.id
            WHERE w.nrorg = $nrorg
            "
        )->getResult();
    }
    
    public function setMerchantKey($entityManager) {
        $storeId = $this->getId();
        $gatewayFromStore = $entityManager->createQuery(
            "
            SELECT g.merchantKey, g.merchantId
            FROM 'Zeedhi\ApiOrders\Model\Entities\PayGateway' g
            JOIN 'Zeedhi\ApiOrders\Model\Entities\PayGatewayRel' gr WITH g.id = gr.payGateway
            WHERE gr.evtEvent = $storeId
            "
        )->getResult();
        
        if (count($gatewayFromStore) === 0) {
            $gateway = PayGatewayRel::getMerchantKeyByOrganization($this->getNrorg(), $entityManager);
        } else {
            $gateway = $gatewayFromStore;
        }
            
        if (count($gateway) > 0) {
            $this->merchantKey = $gateway[0]['merchantKey'];
            $this->merchantId  = $gateway[0]['merchantId'];
        }
    }
    
    public function setSellerData($entityManager, $userId) {
        $storeId = $this->getId();
        $sellerData = $entityManager->createQuery(
            "
            SELECT es
            FROM 'Zeedhi\ApiOrders\Model\Entities\EvtEventSeller' es
            JOIN 'Zeedhi\ApiOrders\Model\Entities\EvtEvent' e WHERE es.evtEvent = e
            JOIN es.genUser u
            WHERE e.id = $storeId
            AND u.id = $userId
            "
        )->getResult();
        if (count($sellerData) > 0) {
            $this->sellerData = $sellerData[0];
        }
    }
    
    public function setAdminUsers($entityManager) {
        $storeId = $this->getId();
        $admins  = $entityManager->createQuery(
            "
            SELECT u
            FROM 'Zeedhi\ApiOrders\Model\Entities\GenUser' u
            JOIN 'Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel' utr WITH utr.genUser = u.id
            JOIN utr.genUserType ut
            JOIN 'Zeedhi\ApiOrders\Model\Entities\EvtEvent' e WITH e.id = $storeId
            WHERE ut.id = 4
            AND utr.status = 'A'
            AND utr.nrorg = e.nrorg
            "
        )->getResult();
        $this->adminUsers = $admins;
    }
    
    public function setMinAndMaxTime($entityManager) {
        $storeId = $this->getId();
        $queryResult = $entityManager->createQuery(
            "
            SELECT e.id, MIN(mp.estimatedTime) as minTime, MAX(mp.estimatedTime) as maxTime
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct' mp
            JOIN mp.ordMenu as m
            JOIN 'Zeedhi\ApiOrders\Model\Entities\EvtEventMenu' em WITH ( em.evtEvent = $storeId AND em.ordMenu = m.id )
            JOIN em.evtEvent e
            WHERE mp.estimatedTime <> 0
            GROUP BY e.id, mp.estimatedTime
            "
        )->getResult();
        if (count($queryResult) > 0) {
            $this->minTime = $queryResult[0]['minTime'];
            $this->maxTime = $queryResult[0]['maxTime'];
        }
    }
    
    public function setChildren($entityManager) {
        $id = $this->getId();
        $children = $entityManager->createQuery(
            "
            SELECT e
            FROM 'Zeedhi\ApiOrders\Model\Entities\EvtEvent' e
            WHERE e.parentEvent = $id
            AND e.status != 'I'
            AND e.status != 'C'
            "
        )->getResult();
        $this->children = EvtEvent::manyToArray($children);
    }
        public function setAskName($entityManager) {
        $storeId = $this->getId();
        $this->askName = $entityManager->createQuery(
            "
            SELECT c.askName
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->askName) > 0) $this->askName = $this->askName[0]['askName'];
    }
     public function setAcceptVoucher($entityManager) {
        $storeId = $this->getId();
        $this->acceptVoucher = $entityManager->createQuery(
            "
            SELECT c.acceptVoucher
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->acceptVoucher) > 0) $this->acceptVoucher = $this->acceptVoucher[0]['acceptVoucher'];
    }
     public function setLockId($entityManager) {
        $storeId = $this->getId();
        $this->lockId = $entityManager->createQuery(
            "
            SELECT c.lockId
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->lockId) > 0) $this->lockId = $this->lockId[0]['lockId'];
    }
    
    public function setEstablishmentId($entityManager) {
        $storeId = $this->getId();
        $this->establishmentId = $entityManager->createQuery(
            "
            SELECT c.establishmentId
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdConfigStore' c
            WHERE c.event = $storeId
            "
        )->getResult();
        if (count($this->establishmentId) > 0) $this->establishmentId = $this->establishmentId[0]['establishmentId'];
    }
    
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['about'] = $this->getAbout();
        $array['address'] = $this->getAddress() != NULL ? GenAddress::manyToArray($this->getAddress()) : NULL;
        $array['imageCover'] = $this->getImageCover();
        $array['imageLogo'] = $this->getImageLogo();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['status'] = $this->getStatus();
        $array['structureId'] = $this->getStructure() != NULL ? $this->getStructure()->getId() : NULL;
        $array['structureName'] = $this->getStructure() != NULL ? $this->getStructure()->getName() : NULL;
        $array['ownerUserId'] = $this->getOwnerUser() != NULL ? $this->getOwnerUser()->getId() : NULL;
        $array['parentEventId'] = $this->getParentEvent() != NULL ? $this->getParentEvent()->getId() : NULL;
        $array['opened'] = (bool) $this->isOpened();
        $array['sellerData'] = $this->getSellerData() ? $this->getSellerData()->toArray() : NULL;
        $array['workshifts'] = GenShift::manyToArray($this->getWorkshifts());
        $array['admins'] = GenShift::manyToArray($this->getAdminUsers());
        $array['minTime'] = $this->getMinTime();
        $array['maxTime'] = $this->getMaxTime();
        $array['minValue'] = $this->getMinValue();
        $array['children'] = $this->getChildren();
        $array['type'] = $this->getType();
        $array['latitude'] = $this->getLatitude();
        $array['longitude'] = $this->getLongitude();
        $array['EXTERNAL_ID'] = $this->getExternalId();
        $array['stoneRecipientId'] = $this->getStoneRecipientId();
        $array['timerTaa'] = $this-> getTimerTaa();
        $array['urlApiExternal'] = $this->geturlApiExternal();
        $array['store_ip'] = $this->getIpStore();
        $array['tempo'] = $this->getTempo();
        $array['askName'] = $this->getAskName();
        $array['acceptVoucher'] = $this->getAcceptVoucher();
        $array['lockId'] = $this->getLockId();
        $array['establishmentId'] = $this->getEstablishmentId();
        if ($array['EXTERNAL_ID'] != NULL)
            $array['EXTERNAL_ID'] = str_pad($array['EXTERNAL_ID'], 4, "0", STR_PAD_LEFT);
        
        if ($this->getMerchantKey()) $array['merchantKey'] = $this->getMerchantKey();
        if ($this->getMerchantId()) $array['merchantId'] = $this->getMerchantId();
        
        if ($this->getMenus()) {
            $array['menus'] = [
                'currentWorkshift'    => OrdMenu::manyToArray($this->getMenus()['currentWorkshift']),
                'notCurrentWorkshift' => OrdMenu::manyToArray($this->getMenus()['notCurrentWorkshift'])
            ];
        }
        
        if ($this->getAllMenus()) {
            $array['allMenus'] = [
                'allCurrentWorkshift'    => OrdMenu::manyToArray($this->getAllMenus()['allCurrentWorkshift']),
                'allNotCurrentWorkshift' => OrdMenu::manyToArray($this->getAllMenus()['allNotCurrentWorkshift'])
            ];
        }
        if (count($this->getContactMethods()) > 0) $array['contactMethods'] = GenContact::manyToArray($this->getContactMethods());
        $array['paymentMethods'] = OrdProduct::manyToArray($this->getPaymentMethods());
        if ($this->getPaymentMoment()) $array['paymentMoment'] = $this->getPaymentMoment()->toString();
        
        if ($this->getDeliversToTable()) $array['deliversToTable'] = $this->getDeliversToTable() == 'T';
        if ($this->getDeliversToBalcony()) $array['deliversToBalcony'] = $this->getDeliversToBalcony() == 'T';
        if ($this->getDeliversToAddress()) $array['deliversToAddress'] = $this->getDeliversToAddress() == 'T';
        
        return $array;
    }
    
    public function toArrayStoresFilter() {
        $array = [];
        $address = GenAddress::manyToArray($this->getAddress());
        
        $array['id'] = $this->getId();
        $array['NRORG'] = $this->getNrorg();
        $array['name'] = $this->getName();
        $array['structureName'] = $this->getStructure() != NULL ? $this->getStructure()->getName() : NULL;
        $array['about'] = $this->getAbout();
        $array['storeStatus'] = $this->getStatus() !== 'P' ? 'Sim' : 'Não';
        $array['status'] = $this->getStatus();
        if ($this->getDeliversToTable()) $array['deliversToTable'] = $this->getDeliversToTable() == 'T';
        if ($this->getDeliversToBalcony()) $array['deliversToBalcony'] = $this->getDeliversToBalcony() == 'T';
        if ($this->getDeliversToAddress()) $array['deliversToAddress'] = $this->getDeliversToAddress() == 'T';
        $array['deliversToTableString'] = $this->getDeliversToTable() == 'T' ? 'Sim' : 'Não';
        $array['deliversToBalconyString'] = $this->getDeliversToBalcony() == 'T' ? 'Sim' : 'Não';
        $array['deliversToAddressString'] = $this->getDeliversToAddress() == 'T' ? 'Sim' : 'Não';
        $array['address'] = $address;
        $array['STREET'] = $address[0]['STREET'];
        $array['NUMBER'] = $address[0]['NUMBER'];
        $array['NEIGHBORHOOD'] = $address[0]['NEIGHBORHOOD'];
        $array['CITY'] = $address[0]['CITY'];
        $array['PROVINCY'] = $address[0]['PROVINCY'];
        $array['CEP'] = $address[0]['CEP'];
        $array['LATITUDE'] = $address[0]['LATITUDE'];
        $array['LONGITUDE'] = $address[0]['LONGITUDE'];
        $array['opened'] = (bool) $this->isOpened();
        $array['sellerData'] = $this->getSellerData() ? $this->getSellerData()->toArray() : NULL;
        $array['workshifts'] = GenShift::manyToArray($this->getWorkshifts());
        $array['minValue'] = $this->getMinValue();
        $array['timerTaa'] = $this->getTimerTaa();
        $array['paymentMethods'] = OrdProduct::manyToArray($this->getPaymentMethods());
        if ($this->getPaymentMoment()) $array['paymentMoment'] = $this->getPaymentMoment()->toString();
        $array['store_ip'] = $this->getIpStore();
        
        return $array;
    }
    
    public static function manyToArray($events) {
        $arrays = [];
        foreach ($events as $event) {
            $array = $event->toArray();
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public static function manyToArrayStoresFilter($events) {
        $arrays = [];
        foreach ($events as $event) {
            $array = $event->toArrayStoresFilter();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function ToArrayIntegration () {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['about'] = $this->getAbout();
        $array['store_ip'] = $this->getIpStore();
        $array['integration_commands'] = $this->getIntegrationCommands();
        return $array;
    }
    
}