<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class OrdMenuProduct extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdMenuProduct {
    
    public function build($entityManager) {
        $menuId         = $this->getOrdMenu() != NULL ? $this->getOrdMenu()->getId() : NULL;
        $productGroupId = $this->getOrdProductGroup() != NULL ? $this->getOrdProductGroup()->getId() : NULL;
        $this->getOrdProduct()->build($entityManager, $menuId, $productGroupId);
        $this->setExtras($entityManager);
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    private function setExtras($entityManager) {
        $extra  = 'Zeedhi\ApiOrders\Model\Entities\OrdExtra';
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $extras = $entityManager->createQuery(
            "
            SELECT e
            FROM $extra e
            JOIN e.ordMenuProduct as mp
            WHERE mp.id = $id
            ORDER BY e.ordination
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
        
        $newExtras = $this->getNewExtrasProd($id,$entityManager);
        if($newExtras)
        $allProducts=$newExtras;
        else $allProducts=$extras;
       // $allProducts = array_merge($extras, $newExtras);
        
        
        
        $this->extras = $allProducts;
    }
    function getNewExtrasProd($productId,$entityManager){
        
        $newExtras = $entityManager->createQuery(
            "
            SELECT e
            FROM 'Zeedhi\ApiOrders\Model\Entities\OrdExtra' e
            JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProductExtra pe WITH pe.ordExtra = e
            JOIN pe.ordMenuProduct as p
            WHERE p.id = $productId and e.status = 'A'
            "
        )->getResult();
        
        foreach ($newExtras as $newExtra) {
            $newExtra->build($entityManager);
        }
        return $newExtras;
    }
    
    public function getClone($entityManager, $productGroupId=NULL) {
        $newMenuProduct = new OrdMenuProduct();
        
        if ($productGroupId) $newMenuProduct->setOrdProductGroup($productGroupId);
        $newMenuProduct->setName($this->getName());
        $newMenuProduct->setDetail($this->getDetail());
        $newMenuProduct->setPrice($this->getPrice());
        $newMenuProduct->setNrorg($this->getNrorg());
        $newMenuProduct->setImage($this->getImage());
        $newMenuProduct->setStatus($this->getStatus());
        $newMenuProduct->setEstimatedTime($this->getEstimatedTime());
        $newMenuProduct->setOrdMenu($this->getOrdMenu());
        $newMenuProduct->setOrdProduct($this->getOrdProduct());
        $newMenuProduct->setInitialTime($this->getInitialTime());
        $newMenuProduct->setFinalTime($this->getFinalTime());
        
        $entityManager->persist($newMenuProduct);
        
        $this->build($entityManager);
        foreach ($this->getExtras() as $extra) {
            $e = $extra->getClone($entityManager);
            $e->setOrdMenuProduct($newMenuProduct);
        }
        
        return $newMenuProduct;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'originalProduct' => $this->getOrdProduct()->toArray(),
            'menuId' => $this->getOrdMenu() ? $this->getOrdMenu()->getId() : NULL,
            'menuName' => $this->getOrdMenu() ? $this->getOrdMenu()->getName() : NULL,
            'productGroupId' => $this->getOrdProductGroup() ? $this->getOrdProductGroup()->getId() : NULL,
            'storeId' => $this->getOrdProductGroup() ? $this->getOrdProductGroup()->getEvtEvent()->getId() : NULL,
            'price' => $this->getPrice(),
            'name' => $this->getName(),
            'detail' => $this->getDetail(),
            'nrorg' => $this->getNrorg(),
            'estimatedTime' => $this->getEstimatedTime(),
            'image' => $this->getImage(),
            'status' => $this->getStatus(),
            'ordination' => $this->getOrdination(),
            'featured' => $this->getFeatured(),
            'extras' => OrdExtra::manyToArray($this->getExtras())
        );
    }

    public static function manyToArray($products) {
        $arrays = [];
        foreach ($products as $product) {
            array_push($arrays, $product->toArray());
        }
        
        return $arrays;
    }
}