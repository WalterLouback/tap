<?php

namespace  Zeedhi\ApiOrders\Model\Entities;

class SerServiceEventRel extends \Zeedhi\ApiOrders\Model\Entities\Base\SerServiceEventRel
{
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "NRORG" => $this->getNrorg(),
            "STATUS" => $this->getStatus(),
            "EVENT_ID" => $this->getEvtEvent()->getId(),
            "EVENT_NAME" => $this->getEvtEvent()->getName(),
            "SERVICE_ID" => $this->getSerService()->getId(),
            "CREATED_BY" => $this->getCreatedBy(),
            "MODIFIED_BY" => $this->getModifiedBy(),
            "CREATED_AT" => $this->getCreatedAt(),
            "MODIFIED_AT" => $this->getModifiedAt()
        );
    }
}
