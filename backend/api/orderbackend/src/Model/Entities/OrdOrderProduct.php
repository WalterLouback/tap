<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiOrders\Model\Entities\OrdItemExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdExtra;
use Zeedhi\ApiOrders\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiOrders\Model\Entities\OrdOption;

class OrdOrderProduct extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdOrderProduct {
    
    public function build($entityManager) {
        $this->setExtras($entityManager);
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['status'] = $this->getStatus();
        $array['note'] = $this->getNote();
        $array['unitaryPrice'] = $this->getPrice();
        $array['quantity'] = $this->getQuantity();
        $array['total'] = $this->getTotal();
        
        if ($this->getEvtEventTicket()) {
            $array['name'] = $this->getEvtEventTicket()->getName();
        }
        else if ($this->getOrdMenuProduct()) {
            $array['name'] = $this->getOrdMenuProduct()->getName();
            $array['image'] = $this->getOrdMenuProduct()->getImage();
            $array['unitaryPrice'] = $this->getPrice() ? $this->getPrice() : $this->getOrdMenuProduct()->getPrice();
            $array['productId'] = $this->getOrdMenuProduct()->getId();
            $array['extras'] = OrdItemExtra::manyToArray($this->getExtras());
        }
        
        if($this->getSerService()){
            $array['serviceId'] = $this->getSerService()->getId();
            $array['name'] = $this->getSerService()->getName();
        }
        return $array;
    }
    
    public static function manyToArray($items) {
        $arrays = [];
        foreach ($items as $item) {
            $array = $item->toArray();
            array_push($arrays, $array);
        }
        return $arrays;
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    /* Devolve os extras dos itens enviados por parâmetro */
    function setExtras($entityManager) {
        $item   = OrdOrderProduct::class;
        $itemExtra = OrdItemExtra::class;
        $extra  = OrdExtra::class;
        $selectedOption = OrdSelectedOption::class;
        $option = OrdOption::class;
            
        $id = $this->getId();
            
        /* Realizando Query 
           o  = Pedido
           po = Item do Pedido
           e  = Extra
           ie = Extra do Item do Pedido
           op = Option
           so = Selected Option
        */
        $extras = $entityManager->createQuery(
            "
            SELECT ie
            FROM $itemExtra ie
            JOIN ie.ordExtra as e 
            JOIN ie.ordOrderProduct as i
            JOIN i.ordOrder as o
            WHERE i.id = $id
            ORDER BY e.id
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
            
        $this->extras = $extras;
    }

    
}