<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiOrders\Helpers\General as General;

class OrdExtra extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdExtra {
    
    public function build($entityManager) {
        $this->setOptions($entityManager);
    }
    
    public static function manyToArray($extras) {
        $arrays = [];
        foreach ($extras as $extra) {
            array_push($arrays, $extra->toArray());
        }
        
        return $arrays;
    }
    
    public function getClone($entityManager) {
        $newExtra = new OrdExtra();
        
        $newExtra->setName($this->getName());
        $newExtra->setRequired($this->getRequired());
        $newExtra->setMultiple($this->getMultiple());
        $newExtra->setOrdination($this->getOrdination());
        
        $entityManager->persist($newExtra);
        
        $this->build($entityManager);
        foreach ($this->getOptions() as $option) {
            $o = $option->getClone($entityManager);
            $o->setOrdExtra($newExtra);
        }
        
        return $newExtra;
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['required'] = $this->getRequired();
        $array['multiple'] = $this->getMultiple();
        $array['limitQuantity'] = $this->getlimitQuantity();
        $array['options'] = OrdOption::manyToArray($this->getOptions());
        $array['ordination']    = $this->getOrdination();
        
        return $array;
    }
    
    public function getOptions() {
        return property_exists($this, 'options') ? $this->options : [];
    }
    
    public function setOptions($entityManager) {
        $item   = 'Zeedhi\ApiOrders\Model\Entities\OrdOrderProduct';
        $extra  = 'Zeedhi\ApiOrders\Model\Entities\OrdExtra';
        $option = 'Zeedhi\ApiOrders\Model\Entities\OrdOption';
        $extras = [];
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $queryResult = $entityManager->createQuery(
            "
            SELECT op
            FROM $option op
            JOIN op.ordExtra e
            WHERE e.id = $id
            ORDER BY op.ordination
            "
        )->getResult();
        
        $this->options = $queryResult;
    }
    
}