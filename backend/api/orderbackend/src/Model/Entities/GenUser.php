<?php
namespace Zeedhi\ApiOrders\Model\Entities;

use Zeedhi\ApiGeneral\Model\Entities\GenContact;
use Zeedhi\ApiOrders\Model\Entities\SerServiceSellerRel;

class GenUser extends \Zeedhi\ApiOrders\Model\Entities\Base\GenUser {

    const USER_ID  = 'USER_ID';
    const FIRST_NAME = 'FIRST_NAME';
    const LAST_NAME = 'LAST_NAME';
    const CPF      = 'CPF';
    const EMAIL    = 'EMAIL';
    const PASSWORD = 'PASSWORD';
    const TEMP_PASSWORD = 'TEMP_PASSWORD';
    const NEW_PASSWORD = 'NEW_PASSWORD';
    
    const FB_ID = 'FB_ID';    
    const FB_TOKEN = 'FB_TOKEN';
    
    const AUTH_TYPE = 'AUTH_TYPE';
    const AUTH_CODE = 'AUTH_CODE';
    
    const AUTH_TYPE_PHONE = 'PHONE';
    
    const IMAGE = 'IMAGE';
    
    public function build($entityManager, $nrorg=NULL) {
        $this->setUserProfiles($entityManager);
        $this->setAssociatedOrganizations($entityManager);
        $this->setContacts($entityManager);
        $this->setServiceSellerRel($entityManager);
    }
    
    public function getUserProfiles() {
        return property_exists($this, 'userProfiles') ? $this->userProfiles : [];
    }
    
    public function getUserProfile($userTypeId, $nrorg) {
        if (property_exists($this, 'userProfiles')) {
            foreach ($this->userProfiles as $userProfile) {
                if ($userProfile->getNrorg() == $nrorg && $userProfile->getGenUserType()->getId() == $userTypeId) {
                    return $userProfile;
                }
            }
        }
        throw new \Exception('User does not have the requested user profile.', 14);
    }
    
    public function setUserProfiles($entityManager) {
        $userProfiles = $entityManager->getRepository(GenUserTypeRel::class)->findBy(['genUser' => $this->getId()]);
        foreach ($userProfiles as $userProfile) {
            $userProfile->build($entityManager);
        }
        $this->userProfiles = $userProfiles;
    }
    
    public function setAssociatedOrganizations($entityManager) {
        $userId = $this->getId();
        $associatedOrganizations = $entityManager->createQuery(
            "
            SELECT DISTINCT o.nrorg, o.name
            FROM 'Zeedhi\ApiOrders\Model\Entities\GenOrganization' o
            JOIN 'Zeedhi\ApiOrders\Model\Entities\GenUserTypeRel' p WITH p.nrorg = o.nrorg
            WHERE p.genUser = $userId
            "
        )->getResult();
        $this->associatedOrganizations = $associatedOrganizations;
    }
    
    public static function manyToArray($users) {
        $arrays = [];
        foreach ($users as $user) {
            array_push($arrays, $user->toArray());
        }
        return $arrays;
    }
    
    public function toArray($userTypeId=NULL, $nrorg=NULL) {
        return array(
            'ID' => $this->getId(),
            'CPF' => $this->getCpf(),
            'FIRST_NAME' => $this->getFirstName(),
            'LAST_NAME' => $this->getLastName(),
            'EMAIL' => $this->getEmail(),
            'CONTACTS' => GenContact::manyToArray($this->getContacts()),
            'SERVICE_ID' => $this->getServiceSellerRel(),
            'IMAGE' => $this->getImage(),
            'ORGANIZATION_DATA' => $nrorg != NULL && $this->getUserProfile($userTypeId, $nrorg) != NULL ? $this->getUserProfile($userTypeId, $nrorg)->toArray() : NULL
        );
    }
    
    public function setContacts($entityManager) {
        $genContact = GenContact::class;
        $userId   = $this->getId();
        $this->contacts = $entityManager->createQuery(
            "
            SELECT c
            FROM  $genContact c
            WHERE c.genUser = $userId
            "
        )->getResult();
    }
    
    public function setServiceSellerRel($entityManager) {
        $serServiceSellerRel = SerServiceSellerRel::class;
        
        $userId   = $this->getId();
        
        $this->services = $entityManager->createQuery(
            "
            SELECT ss.id, ss.name
            FROM  $serServiceSellerRel ssr
            LEFT JOIN ssr.serService ss
            LEFT JOIN ssr.evtEventSeller ees
            WHERE ees.genUser = '$userId' AND ees.status = 'A' AND ssr.status = 'A'
            "
        )->getResult();
    }
    
    public function getContacts() {
        return property_exists($this, 'contacts') ? $this->contacts : [];
    }
    
    public function getServiceSellerRel() {
        return property_exists($this, 'services') ? $this->services : [];
    }
    
}