<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class GenUserType extends \Zeedhi\ApiOrders\Model\Entities\Base\GenUserType {
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getName()
        );
    }
    
    public static function manyToArray($userTypes) {
        $array = [];
        foreach ($userTypes as $userType) {
            array_push($array, $userType->toArray());
        }
        
        return $array;
    }
    
}