<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class OrdProductGroup extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdProductGroup {
    
    public function build($entityManager) {
        $this->setProducts($entityManager);
    }
    
    public function getProducts() {
        return property_exists($this, 'products') ? $this->products : [];
    }
    
    public function setProducts($entityManager) {
        $product = 'Zeedhi\ApiOrders\Model\Entities\OrdProduct';
        
        $id = $this->getId();
        
        $menuProducts = $entityManager->createQuery(
            "
            SELECT mp
            FROM $product p
            JOIN Zeedhi\ApiOrders\Model\Entities\OrdMenuProduct mp WITH mp.ordProduct = p
            JOIN mp.ordProductGroup as pg
            JOIN mp.ordProduct as p2
            WHERE pg.id = $id
            ORDER BY mp.ordination
            "
        )->getResult();
        
        foreach ($menuProducts as $menuProduct) {
            $menuProduct->build($entityManager);
        }
        
        $this->products = $menuProducts;
    }
    
    public function getClone($entityManager) {
        $newPg = new OrdProductGroup();
        $newPg->setName($this->getName());
        $newPg->setOrdMenu($this->getOrdMenu());
        $newPg->setEvtEvent($this->getEvtEvent());
        $entityManager->persist($newPg);
        
        return $newPg;
    }

    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['ordination'] = $this->getOrdination();
        $array['products'] = OrdProduct::manyToArray($this->getProducts());
        $array['parentId'] = $this->getParentId();
        
        return $array;
    }
    
    public static function manyToArray($productGroups) {
        $arrays = [];
        foreach ($productGroups as $productGroup) {
            array_push($arrays, $productGroup->toArray());
        }
        return $arrays;
    }
    
}