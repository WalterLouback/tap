<?php
namespace Zeedhi\ApiOrders\Model\Entities;

class OrdConfigStoreDelivers extends \Zeedhi\ApiOrders\Model\Entities\Base\OrdConfigStoreDelivers {
    
    public function toArray () {
        $array = [];
        $array['id'] = $this->getId();
        $array['nrorg'] = $this->getNrorg();
        $array['typeName'] = $this->getTypeName();
        $array['typeRef'] = $this->getTypeDelivers();
        $array['max_scheduling'] = $this->getMaxScheduling();
        $array['allows_scheduling'] = $this->getAllowsScheduling();
        $array['allows_scheduling_String'] = $this->getAllowsScheduling() == 'Y' ? 'Sim' : 'Não';    
        // $array['minValue'] = $this->getMinValue();
        return $array;
    }
 
    public static function manyToArray($OrdConfigStoreDelivers) {
        $arrays = [];
        foreach ($OrdConfigStoreDelivers as $OrdConfigStoreDeliver) {
            $array = $OrdConfigStoreDeliver->toArray();
            array_push($arrays, $array);
        }
        return $arrays;
    } 
    
}