<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class GenUserTypeRel extends \Zeedhi\ApiOrders\Model\Entities\Base\GenUserTypeRel {
    
    const EXTERNAL_ID = 'EXTERNAL_ID';
    
    public function build($entityManager) {
        $this->setUser($entityManager);
    }
    
    public function getUser() {
        return property_exists($this, 'user') ? $this->user : NULL;
    }
    
    public function setUser($entityManager) {
        $this->user = $entityManager->getRepository(GenUser::class)->find($this->getgenUser());
    }
    
    public static function manyToArray($userProfiles) {
        $arrays = [];
        foreach ($userProfiles as $userProfile) {
            array_push($arrays, $userProfile->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_ID' => $this->getGenUser()->getId(),
            'USER_TYPE' => $this->getGenUserType()->getName(),
            'EXTERNAL_ID' => $this->getExternalId(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}