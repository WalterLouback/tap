<?php
namespace Zeedhi\ApiOrders\Model\Entities;


class GenContact extends \Zeedhi\ApiOrders\Model\Entities\Base\GenContact {
    
    public static function manyToArray($contacts) {
        $array = [];
        foreach ($contacts as $contact) {
            array_push($array, $contact->toArray());
        }
        return $array;
    }

    public function toArray() {
        return [
            'type' => $this->getType(),
            'phone' => $this->getPhone(),
            'isVisibleToUser' => $this->getIsVisibleToUser(),
            'isWhatsapp' => $this->getIsWhatsapp()
        ];
    }
    
}