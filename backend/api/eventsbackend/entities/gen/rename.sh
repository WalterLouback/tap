cd entities/
FILES=*
RF=""
for f in $FILES
do
  echo "Processing $f file..."
  RF=${f/"ApiOrders"/"ApiEvents"}
  echo "Renamed to $RF"
  mv $f $RF
done