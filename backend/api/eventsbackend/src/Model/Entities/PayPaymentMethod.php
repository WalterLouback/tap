<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class PayPaymentMethod extends \Zeedhi\ApiEvents\Model\Entities\Base\PayPaymentMethod {
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "PAYMENT_METHOD" => $this->getPaymentMethod(),
            "LABEL" => $this->getLabel()
        );
    }

    
}