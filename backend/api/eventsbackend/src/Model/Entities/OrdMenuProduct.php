<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdMenuProduct extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdMenuProduct {
    
    public function build($entityManager) {
        $menuId         = $this->getOrdMenu() != NULL ? $this->getOrdMenu()->getId() : NULL;
        $productGroupId = $this->getOrdProductGroup() != NULL ? $this->getOrdProductGroup()->getId() : NULL;
        $this->getOrdProduct()->build($entityManager, $menuId, $productGroupId);
        $this->setExtras($entityManager);
        // $this->setOfferProducts($entityManager);
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    public function getOfferProducts() {
        return property_exists($this, 'offerProducts') ? $this->offerProducts : NULL;
    }
    
    private function setExtras($entityManager) {
        $extra  = 'Zeedhi\ApiEvents\Model\Entities\OrdExtra';
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $extras = $entityManager->createQuery(
            "
            SELECT e
            FROM $extra e
            JOIN e.ordMenuProduct as mp
            WHERE mp.id = $id
            ORDER BY e.id
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
        
        $this->extras = $extras;
    }
    
    public function setOfferProducts($entityManager) {
        $this->offerProducts  = $entityManager->getRepository(OrdOfferProduct::class)->findBy(['ordOffer' => $this->getId()]);
    }
    
    public static function manyToArray($menuProducs) {
        $arrays = [];
        foreach ($menuProducs as $menuProduc) {
            array_push($arrays, $menuProduc->toArray());
        }
        
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'amount' => $this->getAmount(),
            'productId' => $this->getOrdProduct()->getId(),
            'menuId' => $this->getOrdMenu() ? $this->getOrdMenu()->getId() : NULL,
            'productGroupId' => $this->getOrdProductGroup() ? $this->getOrdProductGroup()->getId() : NULL,
            'productGroupName' => $this->getOrdProductGroup() ? $this->getOrdProductGroup()->getName() : NULL,
            'price' => $this->getPrice(),
            'name' => $this->getName(),
            'detail' => $this->getDetail(),
            'nrorg' => $this->getNrorg(),
            'estimatedTime' => $this->getEstimatedTime(),
            'image' => $this->getImage(),
            'status' => $this->getStatus(),
            'extras' => OrdExtra::manyToArray($this->getExtras()),
            'offerProducts' => $this->getOfferProducts() ? OrdExtra::manyToArray($this->getOfferProducts()) : NULL
        );
    }

    
}