<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenNews extends \Zeedhi\ApiEvents\Model\Entities\Base\GenNews {
    
    public static function manyToArray($newsArray) {
        $arrays = [];
        foreach ($newsArray as $news) {
            array_push($arrays, $news->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'TITLE' => $this->getTitle(),
            'NEWS' => $this->getNews(),
            'NRORG' => $this->getNrorg(),
            'ACTIVE' => $this->getActive(),
            'IMAGE' => $this->getImage(),
            'AUTHOR_ID' => $this->getGenUserTypeRel() != NULL ? $this->getGenUserTypeRel()->getGenUser()->getId() : NULL
        );
    }

    
}