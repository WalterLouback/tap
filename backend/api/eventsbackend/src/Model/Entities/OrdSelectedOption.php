<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdSelectedOption extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdSelectedOption {
    
    public function toArray() {
        return $this->getOrdOption()->toArray();
        $selectedOption['price'] = $this->getPrice();
    }
    
    public static function manyToArray($arrays) {
        $options = [];
        foreach ($arrays as $option) {
            array_push($options, $option->toArray());
        }
        return $options;
    }
    
}