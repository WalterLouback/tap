<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class EvtEventTicket extends \Zeedhi\ApiEvents\Model\Entities\Base\EvtEventTicket {
    
    public function build($entityManager) {
        $this->setPurchasedAmount($entityManager);
    }
    
    public function setPurchasedAmount($entityManager) {
        $orderProduct = OrdOrderProduct::class;
        $ticketId = $this->getId();
        
        $result = $entityManager->createQuery(
            "
            SELECT SUM(op.total) total, COUNT(op.id) quant
            FROM $orderProduct op
            WHERE op.evtEventTicket = $ticketId
            "
        )->getResult();
        
        $this->purchasedAmount = $result[0]['quant'];
        $this->purchasedValue = $result[0]['total'];
    }
    
    public function getPurchasedAmount() {
        return property_exists($this, 'purchasedAmount') ? $this->purchasedAmount : 0;
    }
    
    public function getPurchasedValue() {
        return property_exists($this, 'purchasedValue') ? $this->purchasedValue : 0;
    }
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'eventId' => $this->getEvtEvent() ? $this->getEvtEvent()->getId() : NULL,
            'eventName' => $this->getEvtEvent() ? $this->getEvtEvent()->getName() : NULL,
            'remainingAmount' => $this->getAmount(),
            'purchasedAmount' => (float) $this->getPurchasedAmount(),
            'salesInitialDate' => $this->getStartSale() ? $this->getStartSale()->format('d/m/Y') : NULL,
            'salesFinalDate' => $this->getFinalSale() ? $this->getFinalSale()->format('d/m/Y') : NULL,
            'price' => $this->getPrice(),
            'purchasedValue' => (float) $this->getPurchasedValue()
        );
    }
    
    public static function manyToArray($tickets) {
        $result = [];
        foreach ($tickets as $ticket) {
            array_push($result, $ticket->toArray());
        }
        
        return $result;
    }
    
}