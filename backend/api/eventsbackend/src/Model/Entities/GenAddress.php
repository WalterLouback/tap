<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenAddress extends \Zeedhi\ApiEvents\Model\Entities\Base\GenAddress {
    
    const NRORG = 'NRORG';
    const CEP = 'CEP';
    const STREET = 'STREET';
    const NEIGHBORHOOD = 'NEIGHBORHOOD';
    const CITY = 'CITY';
    const PROVINCY = 'PROVINCY';
    
    public static function manyToArray($addresses) {
        $array = [];
        foreach ($addresses as $address) {
            array_push($array, $address->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "CEP" => $this->getCep(),
            "STREET" => $this->getStreet(),
            "NEIGHBORHOOD" => $this->getNeighborhood(),
            "CITY" => $this->getCity(),
            "PROVINCY" => $this->getProvincy(),
            "NUMBER" => $this->getStreetNumber(),
        );
    }
    
}