<?php
namespace Zeedhi\ApiEvents\Model\Entities;

use Zeedhi\ApiEvents\Helpers\General as General;

class EvtSellerType extends \Zeedhi\ApiEvents\Model\Entities\Base\EvtSellerType {
    
    public function build($entityManager) {
        $this->setVisibleStatus($entityManager);
    }
    
    public function getVisibleStatus() {
        return property_exists($this, 'visibleStatus') ? $this->visibleStatus : [];
    }
    
    public function setVisibleStatus($entityManager) {
        $statusUserType      = $entityManager->getRepository(GenStatusUserType::class)->findBy(['evtSellerType' => $this->getId()]);
        $this->visibleStatus = [];
        foreach ($statusUserType as $statusUT) {
            array_push($this->visibleStatus, $statusUT->getGenStatus());
        }
    }
    
    public function toArray() {
        return array(
            "ID" => $this->getId(),
            "NAME" => $this->getName(),
            "VISIBLE_STATUS" => GenStatus::manyToArray($this->getVisibleStatus())
        );
    }
    
    public static function manyToArray($sellerTypes) {
        $array = [];
        foreach ($sellerTypes as $sellerType) {
            array_push($array, $sellerType->toArray());
        }
        
        return $array;
    }
    
}