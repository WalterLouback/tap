<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class PayGatewayRel extends \Zeedhi\ApiEvents\Model\Entities\Base\PayGatewayRel {
    
    public static function getMerchantKeyByOrganization($nrorg, $entityManager) {

        $payGateway = '\Zeedhi\ApiEvents\Model\Entities\PayGateway';
        $payGatewayRel = '\Zeedhi\ApiEvents\Model\Entities\PayGatewayRel';
        $genConfiguration = 'Zeedhi\ApiEvents\Model\Entities\GenConfiguration';

        $merchantKey = $entityManager->createQuery(
            "
            SELECT pg.merchantKey, pg.merchantId
            FROM $genConfiguration gc 
            JOIN $payGatewayRel pgr WITH gc.id = pgr.genConfiguration
            JOIN $payGateway pg WITH pg.id = pgr.payGateway
            WHERE gc.nrorg = $nrorg 
            "
        )->getResult();

        return $merchantKey;
    }
    
    
}