<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdOfferProduct extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdOfferProduct {
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'amount' => $this->getAmount(),
            'price' => $this->getPrice(),
            'product' => $this->getOrdMenuProduct() !== NULL ? $this->getOrdMenuProduct()->toArray() : NULL
        );
    }
    
}