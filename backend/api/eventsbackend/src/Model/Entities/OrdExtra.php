<?php
namespace Zeedhi\ApiEvents\Model\Entities;

use Zeedhi\ApiEvents\Helpers\General as General;

class OrdExtra extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdExtra {
    
    public function build($entityManager) {
        $this->setOptions($entityManager);
    }
    
    public static function manyToArray($extras) {
        $arrays = [];
        foreach ($extras as $extra) {
            array_push($arrays, $extra->toArray());
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['required'] = $this->getRequired();
        $array['multiple'] = $this->getMultiple();
        $array['productId'] = $this->getOrdMenuProduct() != NULL ? $this->getOrdMenuProduct()->getId() : NULL;
        $array['options'] = OrdOption::manyToArray($this->getOptions());
        
        return $array;
    }
    
    public function getOptions() {
        return property_exists($this, 'options') ? $this->options : [];
    }
    
    public function setOptions($entityManager) {
        $item   = 'Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct';
        $extra  = 'Zeedhi\ApiEvents\Model\Entities\OrdExtra';
        $option = 'Zeedhi\ApiEvents\Model\Entities\OrdOption';
        $extras = [];
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $queryResult = $entityManager->createQuery(
            "
            SELECT op
            FROM $option op
            JOIN op.ordExtra e
            WHERE e.id = $id
            ORDER BY op.name
            "
        )->getResult();
        
        $this->options = $queryResult;
    }
    
}