<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdOrderRO extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdOrderRO {

    const               STATUS_PAYMENT_PENDING  = 'P';
    const                          STATUS_PAID  = 'A';
    const                      STATUS_CANCELED  = 'C';
    const                STATUS_NOT_AUTHORIZED  = 'N';

    const            PAYMENT_METHOD_CREDITCARD  = 'CC';
    const               PAYMENT_METHOD_BALANCE  = 'BALANCE';
    const           PAYMENT_METHOD_EXTERNAL_CC  = 'EXT_CC';
    const           PAYMENT_METHOD_EXTERNAL_DC  = 'EXT_DC';
    const         PAYMENT_METHOD_EXTERNAL_CASH  = 'EXT_CASH';
    const PAYMENT_METHOD_CREDITCARD_FOR_WALLET  = 'CC_W';
    
    const                     DELIVER_TO_TABLE  = 'T';
    const                   DELIVER_TO_BALCONY  = 'B';
    const                   DELIVER_TO_BALCONY  = 'A';
    
    const PAYMENT_TYPE_ORDER_STORE = 'ORDER_STORE';
    const PAYMENT_TYPE_ORDER_EVENT = 'ORDER_EVENT';
    const PAYMENT_TYPE_ORDER_EVENT_SELLER = 'ORDER_EVENT_SELLER';
    const PAYMENT_TYPE_WALLET = 'WALLET';
    const PAYMENT_TYPE_WALLET_RECEIVE = 'WALLET_RECEIVE';
    const PAYMENT_TYPE_WALLET_TRANSFER = 'WALLET_TRANSFER';
    
    public function build($entityManager) {
        $this->setItems($entityManager);
        $this->setPaymentMethodName($entityManager);
        $this->setIsFinished($entityManager);
        $this->setContactUser($entityManager);
        $this->setEnabledToCancel($entityManager);
    }
    
    public function getFormattedType() {
        switch ($this->getType()) {
            case OrdOrderRO::PAYMENT_TYPE_WALLET:
                return $this->startsWith($this->getPaymentMethod(), 'CC_W') || !$this->startsWith($this->getPaymentMethod(), 'BALANCE') ? 'Compra de saldo' : 'Compra de produtos';
            case OrdOrderRO::PAYMENT_TYPE_ORDER_STORE:
            case OrdOrderRO::PAYMENT_TYPE_ORDER_EVENT:
            case OrdOrderRO::PAYMENT_TYPE_ORDER_EVENT_SELLER:
                return 'Compra de produtos';
            case OrdOrderRO::PAYMENT_TYPE_WALLET_RECEIVE:
            case OrdOrderRO::PAYMENT_TYPE_WALLET_TRANSFER:
                return 'Transferência de saldo';
            default:
                return '';
        }
    }
    
    private function startsWith ($string, $startString) { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
    public function getPaymentMethodName() {
        return property_exists($this, 'paymentMethodName') ? $this->paymentMethodName : NULL;
    }
    
    public function getIsFinished() {
        return property_exists($this, 'isFinished') ? $this->isFinished : NULL;
    }
    
    public function getGenContact() {
        return property_exists($this, 'genContact') ? $this->genContact : NULL;
    }
    
    public function getEnabledToCancel() {
        return property_exists($this, 'enabledToCancel') ? $this->enabledToCancel : NULL;
    }
    
    public function setPaymentMethodName($entityManager) {
        $orderId = $this->getId();
        $response = $entityManager->createQuery(
            "
            SELECT pm.label
            FROM 'Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod' pm
            JOIN 'Zeedhi\ApiEvents\Model\Entities\OrdOrderRO' o WITH ( o.paymentMethod = pm.paymentMethod )
            WHERE o.id = '$orderId'
            "
        )->getResult();
        if (count($response) > 0)
            $this->paymentMethodName = $response[0]['label'];
    }
    
    public function setContactUser($entityManager) {
        if ($this->getGenUser() !== NULL) {
            $userId   = $this->getGenUser()->getId();
            $response = $entityManager->createQuery(
                "
                SELECT c.phone
                FROM 'Zeedhi\ApiEvents\Model\Entities\GenContact' c
                WHERE c.genUser = '$userId'
                "
            )->getResult();
            if (count($response) > 0)
                $this->genContact = $response[0]['phone'];
        }
    }
    
    public function setEnabledToCancel($entityManager) {
        if ($this->getEvtEvent() == NULL || $this->getStatus() == NULL) return true;
        $storeId       = $this->getEvtEvent()->getId();
        $nrorg         = $this->getNrorg();
        
        $initialStatus = $this->getInitialStatusFromEventWorkflow($entityManager, $storeId);
        if (count($initialStatus) == 0) $initialStatus = $this->getInitialStatusFromOrganizationWorkflow($entityManager, $nrorg); 
        
        if ($initialStatus == NULL) return true;
        
        $this->enabledToCancel = $initialStatus[0]->getId() == $this->getStatus()->getId();
    }
    
    private function getInitialStatusFromEventWorkflow($entityManager, $storeId) {
        return $entityManager->createQuery(
            "
            SELECT s
            FROM 'Zeedhi\ApiEvents\Model\Entities\GenStatus' s
            JOIN 'Zeedhi\ApiEvents\Model\Entities\OrdWorkflow' w WITH w.initialStatus = s
            JOIN 'Zeedhi\ApiEvents\Model\Entities\OrdConfigStore' c WITH c.workflow = w
            WHERE c.event = $storeId
            "
        )->getResult();
    }
    
    private function getInitialStatusFromOrganizationWorkflow($entityManager, $nrorg) {
        return $entityManager->createQuery(
            "
            SELECT s
            FROM 'Zeedhi\ApiEvents\Model\Entities\GenStatus' s
            JOIN 'Zeedhi\ApiEvents\Model\Entities\OrdWorkflow' w WITH w.initialStatus = s
            WHERE w.nrorg = $nrorg
            "
        )->getResult();
    }
    
    public function setIsFinished($entityManager) {
        if ($this->getStatus() !== NULL) {
            $orderId = $this->getId();
            $status  = $entityManager->getRepository(GenStatus::class)->find($this->getStatus());
            
            $this->isFinished = $status->getNext() == NULL;
        }
    }
    
    public function toArray() {
        $array = [];
        
        $array['id'] = $this->getOrderIdentifier();
        $array['createDate'] = $this->getCreatedAt();
        $array['deliverTo'] = $this->getDeliverTo();
        $array['structure'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getName() : NULL;
        $array['paymentMethod'] = $this->getPaymentMethod();
        $array['paymentMethodName'] = $this->getPaymentMethodName();
        $array['orderStatus'] = $this->getStatus() != NULL ? $this->getStatus()->toString() : NULL;
        $array['paymentStatus'] = $this->getPaymentStatus();
        $array['total'] = $this->getTotal();
        $array['nrorg'] = $this->getNrorg();
        $array['note'] = $this->getNote();
        $array['status'] = $this->getStatus() != NULL ? $this->getStatus()->getId() : NULL;
        $array['creditCardId'] = $this->getPayCreditcard() ? $this->getPayCreditcard()->getId() : NULL;
        $array['storeId'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['storeName'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getName() : NULL;
        $array['storeLogo'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getImageLogo() : NULL;
        $array['userFirstName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getFirstName() : NULL;
        $array['userLastName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getLastName() : NULL;
        $array['userLastName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getLastName() : NULL;
        $array['userProfilePicture'] = $this->getGenUser() != NULL ? $this->getGenUser()->getImage() : NULL;
        $array['numberPhone'] = $this->getGenContact();
        $array['statusColor'] = $this->getStatus() != NULL ? $this->getStatus()->getColor() : NULL;
        $array['items'] = OrdOrderProduct::manyToArray($this->getItems());
        $array['isFinished'] = (bool) $this->getIsFinished();
        $array['enabledToCancel'] = (bool) $this->enabledToCancel;
        $array['qr'] = $this->getId();
        $array['deliveryAddress'] = $this->getDeliveryAddress();
        $array['orderIdentifier'] = $this->getOrderIdentifier();
        $array['fromTaa'] = $this->getFromTaa();
        return $array;
    }
    
    public function getItems() {
        return property_exists($this, 'items') ? $this->items : [];
    }

    public function setItems($entityManager) {
        $items = $entityManager->getRepository(OrdOrderProduct::class)->findBy(['ordOrder' => $this->getId()]);
        foreach ($items as $item) {
            $item->build($entityManager);
        }
        $this->items = $items;
    }
    
    public static function manyToArray($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArray();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public static function manyToArrayReport($orders) {
        $arrays = [];
        foreach ($orders as $order) {
            $array = $order->toArrayReport();
            
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArrayReport() {
        $array = [];
        $array['id'] = $this->getOrderIdentifier();
        $array['orderId'] = $this->getId();
        $array['createDate'] = $this->getCreatedAt()->__toString();
        $array['deliverTo'] = $this->getDeliverTo();
        $array['type'] = $this->getFormattedType();
        $array['structureId'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getId() : NULL;
        $array['structureName'] = $this->getGenStructure() != NULL ? $this->getGenStructure()->getName() : NULL;
        $array['paymentMethod'] = $this->getPaymentMethod();
        $array['paymentMethodName'] = $this->getPaymentMethodName();
        $array['orderStatus'] = $this->getStatus() != NULL ? $this->getStatus()->toString() : NULL;
        $array['paymentStatus'] = $this->getPaymentStatus();
        $array['total'] = $this->getTotal();
        $array['nrorg'] = $this->getNrorg();
        $array['note'] = $this->getNote();
        $array['status'] = $this->getStatus() != NULL ? $this->getStatus()->getId() : NULL;
        $array['creditCardId'] = $this->getPayCreditcard() ? $this->getPayCreditcard()->getId() : NULL;
        $array['storeId'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getId() : NULL;
        $array['storeName'] = $this->getEvtEvent() != NULL ? $this->getEvtEvent()->getName() : NULL;
        $array['userFirstName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getFirstName() : NULL;
        $array['userLastName'] = $this->getGenUser() != NULL ? $this->getGenUser()->getLastName() : NULL;
        $array['userFullName'] = $array['userFirstName'] . ' ' . $array['userLastName'];
        $array['isFinished'] = (bool) $this->getIsFinished();
        $array['qr'] = $this->getId();
        $array['deliveryAddress'] = $this->getDeliveryAddress();
        $array['fromTaa'] = $this->getFromTaa();
        
        return $array;
    }
    
    public static function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        
            return $uuid;
        }
    }
    
}