<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class EvtUserTicketRel extends \Zeedhi\ApiEvents\Model\Entities\Base\EvtUserTicketRel {
    
    public function build($entityManager) {
        $this->setPaidValue($entityManager);
        $this->setBuyerPhone($entityManager);
    }
    
    public function setPaidValue($entityManager) {
        $orderProduct = OrdOrderProduct::class;
        $etId = $this->getEvtEventTicket() ? $this->getEvtEventTicket()->getId() : NULL;
        $userId = $this->getGenUser() ? $this->getGenUser()->getId() : NULL;
        
        if ($etId != NULL && $userId != NULL) {
            $result = $entityManager->createQuery(
                "
                SELECT op.total
                FROM $orderProduct op
                LEFT JOIN op.ordOrder o
                WHERE op.evtEventTicket = $etId
                AND o.genUser = $userId
                "
            )->getResult();
            
            if (count($result) > 0) {
                $this->paidValue = $result[0]['total'];
            }
        }
    }
    
    public function setBuyerPhone($entityManager) {
        $orderProduct = OrdOrderProduct::class;
        $etId = $this->getEvtEventTicket() ? $this->getEvtEventTicket()->getId() : NULL;
        $userId = $this->getGenUser() ? $this->getGenUser()->getId() : NULL;
        
        if ($etId != NULL && $userId != NULL) {
            $result = $entityManager->createQuery(
                "
                SELECT op.total
                FROM $orderProduct op
                LEFT JOIN op.ordOrder o
                WHERE op.evtEventTicket = $etId
                AND o.genUser = $userId
                "
            )->getResult();
            
            if (count($result) > 0) {
                $this->paidValue = $result[0]['total'];
            }
        }
    }
    
    public function getPaidValue() {
        return property_exists($this, 'paidValue') ? $this->paidValue : NULL;
    }
    
    public function toArray() {
        return array(
            "paidBy" => $this->getGenUser() ? $this->getGenUser()->getFirstName() . " " . $this->getGenUser()->getLastName() : NULL,
            "buyerEmail" => $this->getGenUser() ? $this->getGenUser()->getEmail() : NULL,
            "ownerName" => $this->getOwnerName() ? $this->getOwnerName() : ($this->getGenUser() ? $this->getGenUser()->getFirstName() . " " . $this->getGenUser()->getLastName() : NULL),
            "paidValue" => (float) $this->getPaidValue(),
            "date" => $this->getCreatedAt() ? $this->getCreatedAt()->format("d/m/Y") : NULL,
            "eventName" => $this->getEvtEventTicket() && $this->getEvtEventTicket()->getEvtEvent() ? $this->getEvtEventTicket()->getEvtEvent()->getName() : NULL,
            "ticketName" => $this->getEvtEventTicket() ? $this->getEvtEventTicket()->getName() : NULL
        );
    }
    
    public static function manyToArray($tickets) {
        $result = [];
        foreach ($tickets as $ticket) {
            array_push($result, $ticket->toArray());
        }
        
        return $result;
    }
    
}