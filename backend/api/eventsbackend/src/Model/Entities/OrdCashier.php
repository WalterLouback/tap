<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdCashier extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdCashier {
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'openning' => $this->getOpenning(),
            'closing' => $this->getClosing(),
            'nrorg' => $this->getNrorg()
        );
    }

    
}