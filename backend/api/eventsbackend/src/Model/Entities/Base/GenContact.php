<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class GenContact {
    
    
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEvent  */
    protected $genStructure;
    /** @var string  */
    protected $phone;
    /** @var string  */
    protected $type;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiEvents\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getPhone() {
        return $this->phone;
    }
	public function setPhone($phone = NULL) {
        $this->phone = $phone;
    }
	public function getType() {
        return $this->type;
    }
	public function setType($type) {
        $this->type = $type;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
}