<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class GenIntegration {
    
    protected $id;
    protected $type;
    protected $baseUrl;
    protected $subsidiaryCode;
    protected $token;
    protected $evtEvent;
    protected $nrorg;
    protected $createdAt;
    protected $modifiedAt;
    protected $createdBy;
    protected $modifiedBy;

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getType(){
		return $this->type;
	}

	public function setType($type){
		$this->type = $type;
	}

	public function getBaseUrl(){
		return $this->baseUrl;
	}

	public function setBaseUrl($baseUrl){
		$this->baseUrl = $baseUrl;
	}

	public function getSubsidiaryCode(){
		return $this->subsidiaryCode;
	}

	public function setSubsidiaryCode($subsidiaryCode){
		$this->subsidiaryCode = $subsidiaryCode;
	}

	public function getToken(){
		return $this->token;
	}

	public function setToken($token){
		$this->token = $token;
	}

	public function getEvtEvent(){
		return $this->evtEvent;
	}

	public function setEvtEvent($evtEvent){
		$this->evtEvent = $evtEvent;
	}

	public function getNrorg(){
		return $this->nrorg;
	}

	public function setNrorg($nrorg){
		$this->nrorg = $nrorg;
	}

	public function getCreatedAt(){
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt){
		$this->createdAt = $createdAt;
	}

	public function getModifiedAt(){
		return $this->modifiedAt;
	}

	public function setModifiedAt($modifiedAt){
		$this->modifiedAt = $modifiedAt;
	}

	public function getCreatedBy(){
		return $this->createdBy;
	}

	public function setCreatedBy($createdBy){
		$this->createdBy = $createdBy;
	}

	public function getModifiedBy(){
		return $this->modifiedBy;
	}

	public function setModifiedBy($modifiedBy){
		$this->modifiedBy = $modifiedBy;
	}

}