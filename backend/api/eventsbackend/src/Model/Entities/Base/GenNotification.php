<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class GenNotification {

    /** @var int  */
    protected $id;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $message;
    /** @var string  */
    protected $originNotification;
    /** @var int  */
    protected $dateTime;

    
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
	public function getMessage() {
        return $this->message;
    }
	public function setMessage($message = NULL) {
        $this->message = $message;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
    public function getOriginNotification() {
        return $this->originNotification;
    }
	public function setOriginNotification($originNotification = NULL) {
        $this->originNotification = $originNotification;
    }
    public function getDateTime() {
        return $this->dateTime;
    }
	public function setDateTime($dateTime) {
        $this->dateTime = $dateTime;
    }
    
	
}