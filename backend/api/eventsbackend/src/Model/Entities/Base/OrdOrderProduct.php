<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class OrdOrderProduct {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var string */
    protected $itemIdentifier;
    /** @var int  */
    protected $total;
    /** @var string */
    protected $status;
    /** @var int  */
    protected $quantity;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct  */
    protected $ordMenuProduct;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdOrder  */
    protected $ordOrder;
    /** @var string */
    protected $note;
    
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEventTicket  */
    protected $evtEventTicket;

    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
	public function getNote() {
        return $this->note;
    }
    public function setNote($note = NULL) {
        $this->note = $note;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getItemIdentifier() {
        return $this->itemIdentifier;
    }
	public function setItemIdentifier($itemIdentifier) {
        $this->itemIdentifier = $itemIdentifier;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getQuantity() {
        return $this->quantity;
    }
	public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getOrdMenuProduct() {
        return $this->ordMenuProduct;
    }
	public function setOrdMenuProduct(\Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct $ordMenuProduct = NULL) {
        $this->ordMenuProduct = $ordMenuProduct;
    }
	public function getOrdOrder() {
        return $this->ordOrder;
    }
	public function setOrdOrder(\Zeedhi\ApiEvents\Model\Entities\OrdOrder $ordOrder = NULL) {
        $this->ordOrder = $ordOrder;
    }
	public function getEvtEventTicket() {
        return $this->evtEventTicket;
    }
	public function setEvtEventTicket(\Zeedhi\ApiEvents\Model\Entities\EvtEventTicket $evtEventTicket = NULL) {
        $this->evtEventTicket = $evtEventTicket;
    }
}