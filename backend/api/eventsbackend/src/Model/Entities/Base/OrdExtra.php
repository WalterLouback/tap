<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;

abstract class OrdExtra {
    
    /** @var int  */
    protected $multiple;
    /** @var int  */
    protected $required;
    /** @var string  */
    protected $name;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct  */
    protected $ordMenuProduct;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getMultiple() {
        return $this->multiple;
    }
	public function setMultiple($multiple = NULL) {
        $this->multiple = $multiple;
    }
	public function getRequired() {
        return $this->required;
    }
	public function setRequired($required = NULL) {
        $this->required = $required;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name = NULL) {
        $this->name = $name;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getOrdMenuProduct() {
        return $this->ordMenuProduct;
    }
	public function setOrdMenuProduct(\Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct $ordMenuProduct = NULL) {
        $this->ordMenuProduct = $ordMenuProduct;
    }
    
}