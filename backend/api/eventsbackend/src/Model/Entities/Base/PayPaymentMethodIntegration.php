<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class PayPaymentMethodIntegration {
    
    protected $id;
    protected $genIntegration;
    protected $paymentMethod;
    protected $externalId;
    protected $createdAt;
    protected $modifiedAt;
    protected $createdBy;
    protected $modifiedBy;
    protected $nrorg;

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getGenIntegration(){
		return $this->genIntegration;
	}

	public function setGenIntegration($genIntegration){
		$this->genIntegration = $genIntegration;
	}

	public function getPaymentMethod(){
		return $this->paymentMethod;
	}

	public function setPaymentMethod($paymentMethod){
		$this->paymentMethod = $paymentMethod;
	}

	public function getExternalId(){
		return $this->externalId;
	}

	public function setExternalId($externalId){
		$this->externalId = $externalId;
	}

	public function getCreatedAt(){
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt){
		$this->createdAt = $createdAt;
	}

	public function getModifiedAt(){
		return $this->modifiedAt;
	}

	public function setModifiedAt($modifiedAt){
		$this->modifiedAt = $modifiedAt;
	}

	public function getCreatedBy(){
		return $this->createdBy;
	}

	public function setCreatedBy($createdBy){
		$this->createdBy = $createdBy;
	}

	public function getModifiedBy(){
		return $this->modifiedBy;
	}

	public function setModifiedBy($modifiedBy){
		$this->modifiedBy = $modifiedBy;
	}

	public function getNrorg(){
		return $this->nrorg;
	}

	public function setNrorg($nrorg){
		$this->nrorg = $nrorg;
	}

}