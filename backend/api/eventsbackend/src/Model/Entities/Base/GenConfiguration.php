<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class GenConfiguration {
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $logoImage;
    /** @var string  */
    protected $logoImageSmall;
    /** @var string  */
    protected $primaryColor;
    /** @var string  */
    protected $secondaryColor;
    /** @var string  */
    protected $yesColor;
    /** @var string  */
    protected $noColor;
    /** @var string  */
    protected $awsS3AccessKey;
    /** @var string  */
    protected $awsS3SecretKey;
    /** @var string  */
    protected $awsS3Bucket;
    
	/** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var boolean  */
    protected $useIntegration;
    /** @var string  */
    protected $fcmServerKey;
    /** @var string  */
    protected $host;
    /** @var string  */
    protected $hostFrom;
    /** @var string  */
    protected $hostPassword;
    /** @var string  */
    protected $hostFromName;
    /** @var boolean  */
    protected $preferenceFlow;
    /** @var string  */
    protected $googleAnalyticsKey;
    /** @var string  */
    protected $appLayoutConfig;
    /** @var string  */
    protected $allowExternalUsers;
    /** @var string  */
    protected $keyGoogle;
    /** @var decimal  */
    protected $convenienceFee;
    /** @var string  */
    protected $urlApiExternal;
    /** @var string  */
    protected $tokenApiExternal;
    /** @var string  */
    protected $originApiExternal;
    /** @var string */
    protected $tokenApiOdhen;
    /** @var string  */
    protected $url;
    
    protected $hostPort;
    /** @var string  */
    protected $hostSecure;
    
    protected $dbName;
    /** @var string  */
    protected $nrorgIntegracao;

	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt($a) {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }

    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getLogoImage() {
        return $this->logoImage;
    }
	public function setLogoImage($logoImage = NULL) {
        $this->logoImage = $logoImage;
    }
	public function getLogoImageSmall() {
        return $this->logoImageSmall;
    }
	public function setLogoImageSmall($logoImageSmall = NULL) {
        $this->logoImageSmall = $logoImageSmall;
    }
	public function getPrimaryColor() {
        return $this->primaryColor;
    }
	public function setPrimaryColor($primaryColor = NULL) {
        $this->primaryColor = $primaryColor;
    }
	public function getSecondaryColor() {
        return $this->secondaryColor;
    }
	public function setSecondaryColor($secondaryColor = NULL) {
        $this->secondaryColor = $secondaryColor;
    }
	public function getYesColor() {
        return $this->yesColor;
    }
	public function setYesColor($yesColor = NULL) {
        $this->yesColor = $yesColor;
    }
	public function getNoColor() {
        return $this->noColor;
    }
	public function setNoColor($noColor = NULL) {
        $this->noColor = $noColor;
    }
    public function getUseIntegration() {
        return $this->useIntegration;
    }
	public function setUseIntegration($useIntegration = NULL) {
        $this->useIntegration = $useIntegration;
    }
    public function getFcmServerKey() {
        return $this->fcmServerKey;
    }
	public function setFcmServerKey($fcmServerKey = NULL) {
        $this->fcmServerKey = $fcmServerKey;
    }
    public function getAwsS3AccessKey() {
        return $this->awsS3AccessKey;
    }
    public function setAwsS3AccessKey($awsS3AccessKey = NULL) {
        $this->awsS3AccessKey = $awsS3AccessKey;
    }
    public function getAwsS3SecretKey() {
        return $this->awsS3SecretKey;
    }
    public function setAwsS3SecretKey($awsS3SecretKey = NULL) {
        $this->awsS3SecretKey = $awsS3SecretKey;
    }
    public function getAwsS3Bucket() {
        return $this->awsS3Bucket;
    }
    public function setAwsS3Bucket($awsS3Bucket = NULL) {
        $this->awsS3Bucket = $awsS3Bucket;
    }
    
    public function getHost() {
        return $this->host;
    }
    public function setHost($host = NULL) {
        $this->host = $host;
    }
    public function getHostFrom() {
        return $this->hostFrom;
    }
    public function setHostFrom($hostFrom = NULL) {
        $this->hostFrom = $hostFrom;
    }
    public function getHostPassword() {
        return $this->hostPassword;
    }
    public function setHostPassword($hostPassword = NULL) {
        $this->hostPassword = $hostPassword;
    }
    public function getHostFromName() {
        return $this->hostFromName;
    }
    public function setHostFromName($hostFromName = NULL) {
        $this->hostFromName = $hostFromName;
    }
    public function getPreferenceFlow() {
        return $this->preferenceFlow;
    }
    public function setPreferenceFlow($preferenceFlow = NULL) {
        $this->preferenceFlow = $preferenceFlow;
    }
    public function getGoogleAnalyticsKey() {
        return $this->googleAnalyticsKey;
    }
    
    public function setGoogleAnalyticsKey($googleAnalyticsKey = NULL) {
        $this->googleAnalyticsKey = $googleAnalyticsKey;
    }
    
    public function getAppLayoutConfig() {
        return $this->appLayoutConfig;
    }

    public function setAppLayoutConfig($appLayoutConfig = NULL) {
        $this->appLayoutConfig = $appLayoutConfig;
        return $this;
    }
    public function getAllowExternalUsers() {
        return $this->allowExternalUsers;
    }
    public function setAllowExternalUsers($allowExternalUsers = NULL) {
        $this->allowExternalUsers = $allowExternalUsers;
    }
    public function getKeyGoogle() {
        return $this->keyGoogle;
    }
    public function setKeyGoogle($keyGoogle = null) {
        $this->keyGoogle = $keyGoogle;
        return $this;
    }
    public function getConvenienceFee() {
        return $this->convenienceFee;
    }
    public function setConvenienceFee($convenienceFee = null) {
        $this->convenienceFee = $convenienceFee;
        return $this;
    }  
	public function geturlApiExternal() {
        return $this->urlApiExternal;
    }
	public function seturlApiExternal($urlApiExternal = NULL) {
        $this->urlApiExternal = $urlApiExternal;
    }  
	public function gettokenApiExternal() {
        return $this->tokenApiExternal;
    }
	public function settokenApiExternal($tokenApiExternal = NULL) {
        $this->tokenApiExternal = $tokenApiExternal;
    }     
	public function getOriginApiExternal() {
        return $this->originApiExternal;
    }
	public function setOriginApiExternal($originApiExternal = NULL) {
        $this->originApiExternal = $originApiExternal;
    }
    public function getTokenApiOdhen() {
        return $this->tokenApiOdhen;
    }
    public function setTokenApiOdhen($tokenApiOdhen = NULL) {
        $this->tokenApiOdhen = $tokenApiOdhen;
    }
    public function getURL() {
        return $this->url;
    }
    public function setURL($url = null) {
        $this->url = $rul;
        return $this;
    }
    public function getSecure() {
        return $this->hostSecure;
    }
    public function setSecure($hostSecure = null) {
        $this->hostSecure = $hostSecure;
        return $this;
    }
    public function getPort() {
        return $this->hostPort;
    }
    public function setPort($hostPort = null) {
        $this->hostPort = $hostPort;
        return $this;
    }
    public function getDbName() {
        return $this->dbName;
    }
    public function setDbName($dbName = null) {
        $this->dbName = $dbName;
        return $this;
    }
    public function getNrorgIntegracao() {
        return $this->nrorgIntegracao;
    }
    public function setNrorgIntegracao($nrorgIntegracao = null) {
        $this->nrorgIntegracao = $nrorgIntegracao;
        return $this;
    }
}