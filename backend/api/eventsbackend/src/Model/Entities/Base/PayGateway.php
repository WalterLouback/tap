<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class PayGateway {
    
    
    /** @var string  */
    protected $gateway;
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $merchantId;
    /** @var int  */
    protected $id = 0;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getGateway() {
        return $this->gateway;
    }
	public function setGateway($gateway = NULL) {
        $this->gateway = $gateway;
    }
    public function getMerchantKey() {
        return $this->merchantKey;
    }
	public function setMerchantKey($merchantKey = NULL) {
        $this->merchantKey = $merchantKey;
    }
    public function getMerchantId() {
        return $this->merchantId;
    }
	public function setMerchantId($merchantId = NULL) {
        $this->merchantId = $merchantId;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
}