<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;

abstract class OrdOrder {
    
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $id;
    /** @var int */
    protected $orderIdentifier;
    /** @var int  */
    protected $total;
    /** @var string */
    protected $note;
    /** @var string  */
    protected $deliverTo;
    /** @var string  */
    protected $type;
    /** @var string  */
    protected $nsu;
    /** @var string  */
    protected $transactionId;
    /** @var string  */
    protected $creditCardBrand;
    /** @var string  */
    protected $creditCardNumbers;
    /** @var string  */
    protected $cardHolderName;
    /** @var string  */
    // protected $creditCardCustomerName;
    /** @var string  */
    protected $paymentStatus;
    /** @var string  */
    protected $paymentMethod;
    /** @var string  */
    protected $orderKey;
    /** @var \Datetime  */
    protected $createDate;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEventSeller  */
    protected $evtEventSeller;
    /** @var \Zeedhi\ApiEvents\Model\Entities\PayWallet  */
    protected $payWallet;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEvent  */
    protected $evtEvent;
    /** @var \Zeedhi\ApiEvents\Model\Entities\PayCreditcard  */
    protected $payCreditcard;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenStructure  */
    protected $genStructure;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenStatus  */
    protected $ordCashier;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdCashier  */
    protected $status;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int */
    protected $deliveryAddress;
    /** @var string */
    protected $OAReponse;
    /** @var string */
    protected $OAsuccess;
    /** @var string */
    protected $stoneNote;
    /** @var decimal  */
    protected $convenienceFee;
    /** @var string  */
    protected $barCode;
    
    protected $fromTaa;
    
    protected $posStatus;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getOrderIdentifier() {
        return $this->orderIdentifier;
    }
	public function setOrderIdentifier($orderIdentifier) {
        $this->orderIdentifier = $orderIdentifier;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getTotal() {
        return $this->total;
    }
	public function setTotal($total) {
        $this->total = $total;
    }
	public function getDeliverTo() {
        return $this->deliverTo;
    }
	public function setDeliverTo($deliverTo) {
        $this->deliverTo = $deliverTo;
    }

	public function getType() {
        return $this->type;
    }
	public function setType($type) {
        $this->type = $type;
    }
	public function getTransactionId() {
        return $this->transactionId;
    }
	public function setTransactionId($transactionId) {
        $this->transactionId = $transactionId;
    }
	public function getNsu() {
        return $this->nsu;
    }
	public function setNsu($nsu) {
        $this->nsu = $nsu;
    }
	public function getCreditCardBrand() {
        return $this->creditCardBrand;
    }
	public function setCreditCardBrand($creditCardBrand) {
        $this->creditCardBrand = $creditCardBrand;
    }
	public function getCreditCardNumbers() {
        return $this->creditCardNumbers;
    }
	public function setCreditCardNumbers($creditCardNumbers) {
        $this->creditCardNumbers = $creditCardNumbers;
    }
	public function getCardHolderName() {
        return $this->cardHolderName;
    }
	public function setCardHolderName($cardHolderName) {
        $this->cardHolderName = $cardHolderName;
    }
	public function getPaymentStatus() {
        return $this->paymentStatus;
    }
	public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }
	public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
    }
	public function getOrderKey() {
        return $this->orderKey;
    }
	public function setOrderKey($orderKey = NULL) {
        $this->orderKey = $orderKey;
    }
	public function getNote() {
        return $this->note;
    }
	public function setNote($note = NULL) {
        $this->note = $note;
    }
	public function getCreateDate() {
        return $this->createDate;
    }
	public function setCreateDate(\Datetime $createDate) {
        $this->createDate = $createDate;
	}
	public function getStatus() {
        return $this->status;
    }
	public function setStatus(\Zeedhi\ApiEvents\Model\Entities\GenStatus $status = NULL) {
        $this->status = $status;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEventSeller() {
        return $this->evtEventSeller;
    }
	public function setEvtEventSeller(\Zeedhi\ApiEvents\Model\Entities\EvtEventSeller $evtEventSeller = NULL) {
        $this->evtEventSeller = $evtEventSeller;
    }
	public function getPayWallet() {
        return $this->payWallet;
    }
	public function setPayWallet(\Zeedhi\ApiEvents\Model\Entities\PayWallet $payWallet = NULL) {
        $this->payWallet = $payWallet;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getPayCreditcard() {
        return $this->payCreditcard;
    }
	public function setPayCreditcard(\Zeedhi\ApiEvents\Model\Entities\PayCreditcard $payCreditcard = NULL) {
        $this->payCreditcard = $payCreditcard;
    }
	public function getGenStructure() {
        return $this->genStructure;
    }
	public function setGenStructure(\Zeedhi\ApiEvents\Model\Entities\GenStructure $genStructure = NULL) {
        $this->genStructure = $genStructure;
    }
	public function getOrdCashier() {
        return $this->ordCashier;
    }
	public function setOrdCashier($ordCashier) {
        $this->ordCashier = $ordCashier;
    }
    public function getDeliveryAddress() {
        return $this->deliveryAddress;
    }
	public function setDeliveryAddress($deliveryAddress) {
        $this->deliveryAddress = $deliveryAddress;
    }
    public function getOAReponse() {
        return $this->OAReponse;
    }
    public function setOAReponse($OAReponse = null) {
        $this->OAReponse = $OAReponse;
    }
    public function getOAsuccess() {
        return $this->OAsuccess;
    }

    public function setOAsuccess($OAsuccess = null) {
        $this->OAsuccess = $OAsuccess;
    }
    
    public function getStoneNote() {
        return $this->stoneNote;
    }

    public function setStoneNote($stoneNote = null) {
        $this->stoneNote = $stoneNote;
    }
    
    public function getConvenienceFee() {
        return $this->convenienceFee;
    }
    
    public function setConvenienceFee($convenienceFee = null) {
        $this->convenienceFee = $convenienceFee;
    }
    
    public function getBarCode() {
        return $this->barCode;
    }
    
    public function setBarCode($barCode = null) {
        $this->barCode = $barCode;
    }
      public function getFromTaa() {
        return $this->fromTaa;
    }
    	public function setFromTaa($fromTaa) {
        $this->fromTaa = $fromTaa;
    }
     public function getPosStatus() {
        return $this->posStatus;
    }
    	public function setPosStatus($posStatus) {
        $this->posStatus = $posStatus;
    }

}