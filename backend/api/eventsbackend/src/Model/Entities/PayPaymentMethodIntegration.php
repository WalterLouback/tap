<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class PayPaymentMethodIntegration extends \Zeedhi\ApiEvents\Model\Entities\Base\PayPaymentMethodIntegration {
    
    public function toArray() {
        return array(
            "ID" => $this->getId()
        );
    }

    
}