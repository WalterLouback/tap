<?php
namespace Zeedhi\ApiEvents\Model\Entities;

use \Zeedhi\ApiEvents\Model\Entities\GenOrganization;

class PayWallet extends \Zeedhi\ApiEvents\Model\Entities\Base\PayWallet {
    
    public function setOrgName($entityManager) {
        $genOrganization = GenOrganization::class;
        $nrorg = $this->getNrorg();
        
        $queryResult = $entityManager->createQuery(
            "
            SELECT o.name
            FROM $genOrganization o
            WHERE o.nrorg = $nrorg
            "
        )->getResult();
        
        if (count($queryResult) > 0) $this->orgName = $queryResult[0]['name'];
    }
    
    public function getOrgName() {
        return property_exists($this, 'orgName') ? $this->orgName : '';
    }
    
    public static function getGUID(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = 
                 substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        
            return $uuid;
        }
    }

    
}