<?php
namespace Zeedhi\ApiEvents\Model\Entities;

class GenUser extends \Zeedhi\ApiEvents\Model\Entities\Base\GenUser {

    const USER_ID  = 'USER_ID';
    const FIRST_NAME = 'FIRST_NAME';
    const LAST_NAME = 'LAST_NAME';
    const CPF      = 'CPF';
    const EMAIL    = 'EMAIL';
    const PASSWORD = 'PASSWORD';
    const TEMP_PASSWORD = 'TEMP_PASSWORD';
    const NEW_PASSWORD = 'NEW_PASSWORD';
    
    const FB_ID = 'FB_ID';    
    const FB_TOKEN = 'FB_TOKEN';
    
    const AUTH_TYPE = 'AUTH_TYPE';
    const AUTH_CODE = 'AUTH_CODE';
    
    const AUTH_TYPE_PHONE = 'PHONE';
    
    const IMAGE = 'IMAGE';
    
    public function build($entityManager) {
        $this->setCreditCards($entityManager);
        $this->setUserProfiles($entityManager);
        $this->setTags($entityManager);
        $this->setAddress($entityManager);
        $this->setContacts($entityManager);
    }
    
    public function getCreditCards() {
        return property_exists($this, 'creditCards') ? $this->creditCards : [];
    }
    
    public function getUserProfiles() {
        return property_exists($this, 'userProfiles') ? $this->userProfiles : [];
    }
    
    public function getTags() {
        return property_exists($this, 'tags') ? $this->tags : [];
    }
    
    public function getAddress() {
        return property_exists($this, 'address') ? $this->address : [];
    }
    
    public function getContacts() {
        return property_exists($this, 'contacts') ? $this->contacts : [];
    }
    
    public function setCreditCards($entityManager) {
        $creditCards = $entityManager->getRepository(PayCreditcard::class)->findBy(['genUser' => $this->getId(), 'status' => 'A']);
        $this->creditCards = $creditCards;
    }
    
    public function setUserProfiles($entityManager) {
        $userProfiles = $entityManager->getRepository(GenUserTypeRel::class)->findBy(['genUser' => $this->getId()]);
        foreach ($userProfiles as $userProfile) {
            $userProfile->build($entityManager);
        }
        $this->userProfiles = $userProfiles;
    }
    
    public function setTags($entityManager) {
        $id = $this->getId();
        $tags = $entityManager->createQuery(
            "
            SELECT t
            FROM 'Zeedhi\ApiEvents\Model\Entities\GenTag' t
            JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtTagUser' tu WITH t = tu.evtTag
            JOIN 'Zeedhi\ApiEvents\Model\Entities\GenUser' u WITH u = tu.genUser
            WHERE tu.genUser = $id
            "
        )->getResult();
        $this->tags = $tags;
    }
    
    public function setAddress($entityManager) {
        $userId = $this->getId();
        $this->address = $entityManager->createQuery(
            "
            SELECT a
            FROM 'Zeedhi\ApiEvents\Model\Entities\GenAddress' a
            WHERE a.genUser = $userId
            AND a.status = 'A'
            "
        )->getResult();
    }
    
    public function setContacts($entityManager) {
        $userId   = $this->getId();
        $this->contacts = $entityManager->createQuery(
            "
            SELECT c
            FROM 'Zeedhi\ApiEvents\Model\Entities\GenContact' c
            WHERE c.genUser = $userId
            "
        )->getResult();
    }
    
    public static function manyToArray($users) {
        $arrays = [];
        foreach ($users as $user) {
            array_push($arrays, $user->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'CPF' => $this->getCpf(),
            'FIRST_NAME' => $this->getFirstName(),
            'BIRTH_DATE' => $this->getBirthDate(),
            'LAST_NAME' => $this->getLastName(),
            'ADDRESS' => $this->getAddress() != NULL ? GenAddress::manyToArray($this->getAddress()) : NULL,
            'CONTACTS' => GenContact::manyToArray($this->getContacts()),
            'EMAIL' => $this->getEmail(),
            'IMAGE' => $this->getImage(),
            'CREDIT_CARDS' => PayCreditcard::manyToArray($this->getCreditCards()),
            'USER_PROFILES' => GenUserTypeRel::manyToArray($this->getUserProfiles()),
            'TAGS' => GenTag::manyToArray($this->getTags())
        );
    }

         
    public function getReceiptsToParent($entityManager, $nrorg) {
        $dependency = $entityManager->getRepository(GenDependent::class)->findOneBy(['dependent' => $this->getId(), 'nrorg' => $nrorg]);
        $parent     = $dependency->getParent();
        $cardFromParent = $parent->getMainCreditcard()->getId();
        return $dependent->getReceiptsTo();
    }    
}