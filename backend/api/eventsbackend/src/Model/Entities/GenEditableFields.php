<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenEditableFields extends \Zeedhi\ApiEvents\Model\Entities\Base\GenEditableFields {
    
    public function getPermissionName() {
        return property_exists($this, 'permission') ? $this->permission : "";
    }
    
    public function setPermissionName($entityManager) {
        $paramId = $this->getId();
        $permission = $entityManager->createQuery(
            "
            SELECT pe
            FROM '\Zeedhi\ApiEvents\Model\Entities\GenPermission' pe
            JOIN '\Zeedhi\ApiEvents\Model\Entities\GenEditableFields' pa WITH pe.id = pa.genPermission
            WHERE pa.id = $paramId
            "
        )->getResult();
        if (count($permission) > 0) {
            $this->permission = $permission[0]->getName();
        }
    }
    
    public static function manyToArray($params) {
        $array = [];
        foreach ($params as $param) {
            array_push($array, $param->toArray());
        }
        return $array;
    }
    
    public function toArray() {
        return array(
            "FIELD_NAME" => $this->getFieldName(),
            "PERMISSION" => $this->getPermissionName()
        );
    }
    
}