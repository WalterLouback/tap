<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenOrganizationUserRel extends \Zeedhi\ApiEvents\Model\Entities\Base\GenOrganizationUserRel {

    public static function manyToArray($oldArrays) {
        $arrays = [];
        foreach ($oldArrays as $el) {
            array_push($arrays, $el->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'USER_ID' => $this->getGenUser()->getId(),
            'NRORG' => $this->getNrorg(),
            'FIRE_BASE_TOKEN' => $this->getFirebaseToken(),
            'STATUS' => $this->getStatus()
        );
    }
    
}