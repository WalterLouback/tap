<?php
namespace Zeedhi\ApiEvents\Listeners;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiEvents\Model\Filters\FilterByUser;
use Zeedhi\ApiEvents\Service\Environment;

use Zeedhi\Framework\DTO\Request;

class PreDispatchSqlFilterEnable extends \Zeedhi\Framework\Events\PreDispatch\Listener {
    
    
    /** @var EntityManager */
    private $entityManager;
    /** @var Environment */
    private $environment;
    
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        $this->entityManager = $entityManager;
        $this->environment = $environment;
    }
    
    
    public function preDispatch(Request $request) {
        $userId = $this->environment->getUserId();
        if ($userId !== null) {
            $filter = $this->entityManager->getFilters()->enable(FilterByUser::NAME);
            $filter->setParameter(FilterByUser::USER_ID, $userId);
        }
    }
}