<?php
namespace Zeedhi\ApiEvents\Helpers;

use Ratchet\Server\IoServer;

require dirname(__DIR__) . '../../../../vendor/autoload.php';

$server = IoServer::factory(
    new WebSocket(),
    8080
);

echo 'WebSocket running';

$server->run();