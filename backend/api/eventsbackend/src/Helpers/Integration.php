<?php
namespace Zeedhi\ApiEvents\Helpers;

class Integration {
    
    /* Metodos construtores */
    public function consumesWebServiceGET($url) {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($result, true);
    }
    
    public function consumesWebServicePost($url, $parameters) {
        $data = sprintf("%s?%s", $url, http_build_query($parameters));
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        curl_close($curl);
        
        $objResult = json_decode($result, true);
        
        return $objResult;
    }
    
    public function consumesWebServicePostTwo($url, $parameters) {
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($parameters),
          CURLOPT_HTTPHEADER => array(
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        $objResult = json_decode($response, true);
        
        return $objResult;
    }
    
    public function getOrganization($nrorg) {
        switch ($nrorg) {
            case 0:
                $response = "teknisa";
                break;
            case 1:
                $response = "minas";
                break;
            case 2:
                $response = "nautico";
                break;
            default:
                throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    public function eventList($token, $nrorg, $npf, $dev=false) {
        $url = "https://socio.minastc.com.br/ws/listar_eventos.php";
        
        $parameters['t'] = $token;
        $parameters['f'] = $nrorg ? $this->getOrganization($nrorg) : null; /*opcional*/
        $parameters['a'] = $npf ? $npf : null; /*opcional*/
        
        if($dev) {
            $parameters['desenvolvimento'] = "sim"; /*opcional*/
        }
        
        return $this->consumesWebServicePost($url, $parameters);
    }
    
    public function infoEvent($token, $codEvent, $idEvent, $nrorg=null, $npf=null, $dev=false) {
        $url = "https://socio.minastc.com.br/ws/listar_tipo_ingressos.php";
        
        $parameters['f'] = $nrorg ? $this->getOrganization($nrorg) : null; /*opcional*/
        $parameters['a'] = $npf ? $npf : null; /*opcional*/
        $parameters['t'] = $token;
        $parameters['n'] = $idEvent;
        $parameters['c'] = $codEvent;
        
        if($dev) {
            $parameters['desenvolvimento'] = "sim";
        }
        
        return $this->consumesWebServicePost($url, $parameters);
    }
    
    public function buyTicket($token, $arrayTicket, $numberSequentialEvent, $dev=false) {
        $url = "https://socio.minastc.com.br/ws/concluir_compra_ingressos.php";
        
        $parameters['t'] = $token;
        $parameters['n'] = $numberSequentialEvent;
        $parameters['i'] = $arrayTicket;
        
        if($dev) {
            $parameters['desenvolvimento'] = "sim";
        }
        
        return $this->consumesWebServicePostTwo($url, $parameters);
    }
}    