<?php
namespace Zeedhi\ApiEvents\Helpers;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class WebSocket implements MessageComponentInterface {
    
    public function onOpen(ConnectionInterface $conn) {
        echo "Connection stablished";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        echo "Connection message";
    }

    public function onClose(ConnectionInterface $conn) {
        echo "Connection closed";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "Connection error";
    }
    
}