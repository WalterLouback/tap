<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Service\Order as OrderService;


use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventFavorite;

use Zeedhi\ApiEvents\Helpers\Environment;

const EVENT_ID        = 'EVENT_ID';

use Doctrine\ORM\EntityManager;

class Favorite extends UserOperation{
    
    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }
    
    public function createFavoriteEvent($nrorg, $eventId, $userId){
        $favorite = $this->getFavoriteEvent($nrorg, $eventId, $userId);
        if(!empty($favorite))
            throw new Exception('Evento já favoritado');
        $favorite = new EvtEventFavorite();
        $favorite->setNrorg($nrorg);
        $favorite->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $favorite->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        $this->getEntityManager()->persist($favorite);
        $this->getEntityManager()->flush();
        
        return $favorite;

    }
    
    public function deleteFavoriteEvent($nrorg, $eventId, $userId){
        $favorite = $this->getFavoriteEvent($nrorg, $eventId, $userId);
        if(empty($favorite))
            throw new Exception('Favorite not found');
        $this->getEntityManager()->remove($favorite);
        $this->getEntityManager()->flush();
    }
    
    public function getFavoriteEvents($nrorg, $userId){
        return $this->getEntityManager()->getRepository(EvtEventFavorite::class)->findBy(array('nrorg' => $nrorg, 'genUser' => $userId));
    }
 
    public function getFavoriteEvent($nrorg, $eventId, $userId){
        return $this->getEntityManager()->getRepository(EvtEventFavorite::class)->findOneBy(array('nrorg' => $nrorg, 'evtEvent' => $eventId, 'genUser' => $userId));
    }   
}
