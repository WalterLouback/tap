<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\OrdCashier;
use Zeedhi\ApiEvents\Model\Entities\OrdCashierMovement;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdOfferProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod;

use Zeedhi\Framework\ORM\DateTime;

use Doctrine\ORM\EntityManager;

class Cashier {
    
    private $entityManager;
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function startCashier($eventId, $nrorg, $eventSellerId, $initialValue, $movementDate) {
        $eventSeller = $this->entityManager->getRepository(EvtEventSeller::class)->find($eventSellerId);
        
        $cashier = new OrdCashier();
        $cashier->setOpenning(new \DateTime($movementDate));
        $cashier->setNrorg($nrorg);
        $cashier->setEventSeller($eventSeller);
        
        $cashierMovement = new OrdCashierMovement();
        $cashierMovement->setMovementDate(new \DateTime($movementDate));
        $cashierMovement->setValue($initialValue);
        $cashierMovement->setOrdCashier($cashier);
        $cashierMovement->setNrorg($nrorg);
        
        $this->entityManager->persist($cashier);
        $this->entityManager->persist($cashierMovement);
        
        $this->entityManager->flush();
        
        return $cashier;
    }
    
    public function finishCashier($eventId, $nrorg, $cashierId, $finalDate) {
        $cashier = $this->entityManager->getRepository(OrdCashier::class)->find($cashierId);
        
        $cashier->setClosing(new \DateTime($finalDate));
        
        $this->entityManager->flush();
        
        return $cashier;
    }
    
    public function cashierMovement($cashierId, $eventId, $nrorg, $value, $movementDate) {
        $cashier = $this->entityManager->getRepository(OrdCashier::class)->find($cashierId);
        
        $cashierMovement = new OrdCashierMovement();
        $cashierMovement->setMovementDate(new \DateTime($movementDate));
        $cashierMovement->setValue($value);
        $cashierMovement->setOrdCashier($cashier);
        $cashierMovement->setNrorg($nrorg);
        $this->entityManager->persist($cashierMovement);
        
        $this->entityManager->flush();
        
        return $cashierMovement;
    }
    
    public function getCashiersReport($date, $eventId, $nrorg, $eventSellerId, $itemsPerPage = 100, $page = 1) {
        $cashier = OrdCashier::class;
        $event = EvtEvent::class;
        $eventSeller = EvtEventSeller::class;
        $movement = OrdCashierMovement::class;
        $order = OrdOrder::class;
        
        if ($page > 1) return [];
        
        $dateQuery = '';
        if ($date != NULL) {
            $date        = strrpos($date, '%') ? str_replace('%', '', $date) : $date;
            $initialDate = $this->stringToDate($date) . ' 00:00:00';
            $finalDate   = $this->stringToDate($date) . ' 23:59:59';
            $dateQuery   = " AND (c.openning > '$initialDate' AND c.openning < '$finalDate' OR (c.closing > '$initialDate' AND c.closing < '$finalDate'))";
        }
        
        if (!is_null($eventId))
            $eventId = join($eventId, ' OR e.id = ');
        $eventQuery = $eventId != NULL ? " AND (e.id = $eventId) " : "";
        
        $eventSellerQuery = $eventSellerId != NULL ? " AND es.id = $eventSellerId" : "";
        
        $cashiers = $this->entityManager->createQuery(
            "
            SELECT c.id, c.openning, c.closing, e.id as eventId, e.name as eventName, concat(u.firstName, ' ', u.lastName) as userName, SUM(o.total) as totalValue
            FROM $cashier c
            LEFT JOIN $eventSeller es WITH es = c.eventSeller
            LEFT JOIN es.genUser u
            LEFT JOIN es.evtEvent e
            LEFT JOIN $order o WITH o.ordCashier = c
            LEFT JOIN o.status s
            WHERE c.nrorg = $nrorg
            AND (o.id is null or o.paymentStatus <> 'P')
            $dateQuery
            $eventQuery
            $eventSellerQuery
            GROUP BY c.id
            ORDER BY c.id DESC
            "
        )->getResult();
        
        return $cashiers;
    }
    
    public function getMultipleCashiersReport($cashierIdsArray=[], $itemsPerPage = 100, $page = 1) {
        $cashier = OrdCashier::class;
        $event = EvtEvent::class;
        $eventSeller = EvtEventSeller::class;
        $movement = OrdCashierMovement::class;
        $order = OrdOrder::class;
        
        if ($page > 1) return [];
        
        $cashierIdQuery = count($cashierIdsArray) > 0 ? '( c.id = ' . join(' OR c.id = ', $cashierIdsArray) . ')' : '';
        
        $cashiers = $this->entityManager->createQuery(
            "
            SELECT MIN(c.openning) as openning, MAX(c.closing) as closing, e.id as eventId, e.name as eventName, SUM(o.total) as totalValue, '-' as userName
            FROM $cashier c
            LEFT JOIN $eventSeller es WITH es = c.eventSeller
            LEFT JOIN es.genUser u
            LEFT JOIN es.evtEvent e
            LEFT JOIN $order o WITH o.ordCashier = c
            LEFT JOIN o.status s
            WHERE o.paymentStatus <> 'P'
            AND $cashierIdQuery
            GROUP BY c.nrorg
            "
        )->getResult();
        
        return $cashiers;
    }
    
    public function getCashierMovements($cashierIdsArray, $itemsPerPage = 100, $page = 1) {
        $movement = OrdCashierMovement::class;
        
        if ($page > 1) return [];
        
        $cashierIdQuery = count($cashierIdsArray) > 0 ? '( c.id = ' . join(' OR c.id = ', $cashierIdsArray) . ')' : '';
        
        $movements = $this->entityManager->createQuery(
            "
            SELECT m.id, m.movementDate, m.value
            FROM $movement m
            INNER JOIN m.ordCashier c
            WHERE $cashierIdQuery
            "
        )->getResult();
        
        return $movements;
    }
    
    public function getCashierOrdersReport($cashierIdsArray, $itemsPerPage = 100, $page = 1) {
        $menuProduct  = OrdMenuProduct::class;
        $order        = OrdOrder::class;
        $orderProduct = OrdOrderProduct::class;
        $offerProduct = OrdOfferProduct::class;
        
        if ($page > 1) return [];
        
        $cashierIdQuery = count($cashierIdsArray) > 0 ? '( o.ordCashier = ' . join(' OR o.ordCashier = ', $cashierIdsArray) . ')' : '';

        $productsReport = $this->entityManager->createQuery(
            "
            SELECT p.id, p.name, SUM(op.quantity) as amount, SUM(op.total) as total
            FROM $menuProduct p
            INNER JOIN $orderProduct op WITH op.ordMenuProduct = p
            LEFT JOIN $offerProduct off WITH off.ordMenuProduct = p
            INNER JOIN op.ordOrder o
            LEFT JOIN o.status s
            WHERE $cashierIdQuery
            AND o.paymentStatus <> 'P'
            GROUP BY p.id
            "
        )->getResult();
        $balanceReport = $this->entityManager->createQuery(
            "
            SELECT '-1' as id, 'Compra de saldo' as name, COUNT(o.id) as amount, SUM(o.total) as total
            FROM $order o
            LEFT JOIN o.status s
            WHERE o.type = 'WALLET'
            AND o.paymentStatus <> 'P'
            AND $cashierIdQuery
            GROUP BY o.type
            "
        )->getResult();
        
        $response = array_merge($productsReport, $balanceReport);
        
        return $response;
    }
    
    public function getCashierReceiptsReport($cashierIdsArray, $itemsPerPage = 100, $page = 1) {
        $menuProduct = OrdMenuProduct::class;
        $orderProduct = OrdOrderProduct::class;
        $paymentMethod = PayPaymentMethod::class;
        $order = OrdOrder::class;
        
        if ($page > 1) return [];
        
        $cashierIdQuery = count($cashierIdsArray) > 0 ? '( o.ordCashier = ' . join(' OR o.ordCashier = ', $cashierIdsArray) . ')' : '';

        $report = $this->entityManager->createQuery(
            "
            SELECT pm.id, pm.label, COUNT(o.id) as amount, SUM(o.total) as total
            FROM $order o
            LEFT JOIN o.status s
            INNER JOIN $paymentMethod pm WITH pm.paymentMethod = o.paymentMethod
            WHERE $cashierIdQuery
            AND o.paymentStatus <> 'P'
            GROUP BY pm.id
            "
        )->getResult();
        
        $report = array_merge($report, $this->entityManager->createQuery(
            "
            SELECT CONCAT('-', pm.id) as id, CONCAT('Cancelamento - ', pm.label) as label, COUNT(o.id) as amount, SUM(o.total) * -1 as total
            FROM $order o
            LEFT JOIN o.status s
            INNER JOIN $paymentMethod pm WITH pm.paymentMethod = o.paymentMethod
            WHERE $cashierIdQuery
            AND o.status = 7
            AND o.paymentStatus <> 'P'
            GROUP BY pm.id
            "
        )->getResult());
        
        return $report;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
    public function getCurrentCashierFromEventSeller($eventSellerId) {
        $cashier = OrdCashier::class;
        
        $opennedCashiers = $this->entityManager->createQuery(
            "
            SELECT c
            FROM $cashier c
            JOIN c.eventSeller es
            WHERE c.closing is NULL
            AND es.id = $eventSellerId
            ORDER BY c.id DESC
            "
        )->getResult();
        
        if (count($opennedCashiers) > 0) return $opennedCashiers[0];
        else return NULL;
    }
    
}