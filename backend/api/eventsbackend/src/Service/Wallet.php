<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Service\Order as OrderService;


use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\PayGatewayRel;
use Zeedhi\ApiEvents\Model\Entities\PayWallet;
use Zeedhi\ApiEvents\Model\Entities\PayWalletCard;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;

const EVENT_ID        = 'EVENT_ID';

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiEvents\Helpers\Environment as Environment;

class Wallet extends UserOperation{
    
    /** @var OrderService */
    private $orderService;
    
    public function __construct(EntityManager $entityManager, OrderService $orderService) {
        parent::__construct($entityManager);
        $this->orderService = $orderService;
    }
    
    /**
     * getBalance
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param int $nrorg
     * @param int $userProfile
     *
     */
    public function getBalance($nrorg, $userId) {
        $filter = array('genUser' => $userId, 'nrorg' => $nrorg, 'status' => 'A');
        return $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy($filter);
    
    }

    /**
     * @param integer  $nrorg
     * @param integer $userProfile
     * @param float $cardId
     *
     * @return BpOrder
     */
    public function createOrderWallet($userProfile, $selectCardId, $buyingValue, $eventId, $nrorg) {
        $order = $this->orderService->createOrderWallet($userProfile, $selectCardId, $buyingValue, $eventId, $nrorg);
        return $order;
    }
    public function createOrderWalletFromGUID($walletId, $walletCardId, $buyingValue, $identifier, $eventId, $nrorg, $cashierId, $paymentMethod, $userId, $sellerUserId, $status, $transactionDetails) {
        $orderData = $this->orderService->createOrderWalletFromGUID($walletId, $walletCardId, $buyingValue, $identifier, $eventId, $nrorg, $cashierId, $paymentMethod, $userId, $sellerUserId, $status, $transactionDetails);
        return $orderData;
    }
    public function updateAll() {
        $this->getEntityManager()->flush();
    } 
    public function factoryOrderDataSet($order, $walletId=NULL) {
        return $this->orderService->factoryOrderDataSet($order, $walletId);
    }
    public function createWallet($genUser){
        $wallet = new PayWallet();
        $wallet->setId(PayWallet::getGUID());
        $wallet->setGenUser($this->getEntityManager()->getReference(GenUser::class, $genUser));
        $wallet->setBalance(0);
        $wallet->setStatus('A');
        $this->getEntityManager()->persist($wallet);
        $this->getEntityManager()->flush();
        return $wallet;
    }
    
    public function getMerchantKey($nrorg) {
        return PayGatewayRel::getMerchantKeyByOrganization($nrorg, $this->getEntityManager());
    }

    /**
     * getBalanceFromGUID
     *
     * @author Arthur Malheiros ODHEN <arthur.malheiros@teknisa.com>
     * 
     * @param int $nrorg
     * @param int $id
     *
     */
    public function getBalanceFromGUID($id, $walletCardId, $nrorg) {
        if ($nrorg !== NULL) {
            if ($id) {
                $wallet = $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy(['id' => $id, 'nrorg' => $nrorg, 'status' => 'A']);
            } else {
                $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
                if ($walletCard) $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
                else $wallet = NULL;
            }
        } else {
            if ($id) {
                $wallet = $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy(['id' => $id, 'status' => 'A']);
            } else {
                $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'status' => 'A']);
                if ($walletCard) $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
                else $wallet = NULL;
            }
        }
        
        if ($wallet) $wallet->setOrgName($this->getEntityManager());
    
        return $wallet;
    }
    
    /**
     * getBalanceFromIdentifier
     *
     * @author Arthur Malheiros ODHEN <arthur.malheiros@teknisa.com>
     * 
     * @param int $nrorg
     * @param int $identifier
     *
     */
    public function getWalletFromIdentifier($identifier, $nrorg) {
        $filter = array('identifier' => $identifier, 'nrorg' => $nrorg, 'status' => 'A');
        return $this->getEntityManager()->getRepository(PayWallet::class)->findBy($filter);
    }
    
    /**
     * getWalletFromGUID
     *
     * @author Arthur Malheiros ODHEN <arthur.malheiros@teknisa.com>
     * 
     * @param int $nrorg
     * @param string $walletId
     *
     */
    public function getWalletFromGUID($walletId, $nrorg) {
        $filter = array('id' => $walletId, 'nrorg' => $nrorg, 'status' => 'A');
        return $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy($filter);
    }
    
    public function createOrderDeleteWallet($userId, $walletId, $walletCardId, $balance, $identifier, $eventId, $nrorg) {
        $order = $this->orderService->createOrderDeleteWallet($userId, $walletId, $walletCardId, $balance, $identifier, $eventId, $nrorg);
        return $order;
    }
    
    public function createOrderBalanceTransfer($userId, $walletId, $walletCardId, $walletIdReceive, $walletCardIdReceive, $transferValue, $eventId, $nrorg) {
        $order = $this->orderService->createOrderBalanceTransfer($userId, $walletId, $walletCardId, $walletIdReceive, $walletCardIdReceive, $transferValue, $eventId, $nrorg);
        return $order;
    }
    
    /**
    * Transfere o saldo de uma wallet para outra
    * @author Arthur Malheiros ODHEN <arthur.malheiros@teknisa.com>
    *
    * @param PayWallet $wallet
    * @return PayWallet $wallet
    */
    public function transferBalanceFromWallet($wallet, $valueTransfer) {
        $walletBalance = $wallet->getBalance();
        
        $wallet->setBalance($walletBalance - $valueTransfer);
        
        $this->getEntityManager()->flush();
        
        return $wallet;
    }
    
    public function transferBalanceToWallet($walletTransfer, $valueTransfer) {
        $walletTransferBalance = $walletTransfer->getBalance();
        
        $walletTransfer->setBalance($walletTransferBalance + $valueTransfer);
        
        $this->getEntityManager()->flush();
        
        return $walletTransfer;
    }

}
