<?php
namespace Zeedhi\ApiEvents\Service;

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\GenContact;
use Zeedhi\ApiEvents\Model\Entities\EvtSellerProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProductGroup;


class EventSeller extends UserOperation {

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }

    public function getOrdProductMenuOnVisible($nrorg, $eventId, $sellerId) {
        $ordMenuProduct = 'Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct';
        $ordProductGroup = 'Zeedhi\ApiEvents\Model\Entities\OrdProductGroup';
        $evtSellerProduct = 'Zeedhi\ApiEvents\Model\Entities\EvtSellerProduct';
        
        $productGroup = $this->getEntityManager()->createQuery("
            SELECT  omp.id, omp.name,  omp.price, omp.detail, omp.image, omp.status, categoria.name as nameCat, categoria.id as idCat FROM $ordMenuProduct omp
            JOIN $evtSellerProduct esp WITH omp.id = esp.ordMenuProduct
            JOIN $ordProductGroup categoria WITH omp.ordProductGroup = categoria.id
            WHERE esp.evtEventSeller = $sellerId
        ")->getResult();
        
        return $productGroup;
    }
        
    public function createEvtEventSeller($evtEvent, $nrorg, $user, $createdBy) {
        $evtEventSeller = new EvtEventSeller();
        
        $evtEventSeller->setEvtEvent($evtEvent);
        $evtEventSeller->setNrorg($nrorg);
        $evtEventSeller->setGenUser($user);
        $evtEventSeller->setCreatedBy($createdBy);
        $evtEventSeller->setStatus('A');
        
        $this->getEntityManager()->persist($evtEventSeller);
        $this->getEntityManager()->flush();
        
        return $evtEventSeller;
    }
    
    public function getEvtEvent($evtEventId){
        return $this->getEntityManager()->getRepository(EvtEvent::class)->find($evtEventId);
    }
    
    public function getGenUser($userId){
        return $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
    }
    
    public function getGenUserByEmail($email){
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->findOneBy(["email" => $email]);
        return $genUser;
    }
    
    public function getEvtEventSeller($userId, $eventId){
        $eventSeller = $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(["genUser" => $userId, "evtEvent" => $eventId]);
        return $eventSeller;
    }
    
    public function getEventSellersByEventId($evtEventId) {
        $user           = GenUser::class;
        $evtEventSeller = EvtEventSeller::class;
        
        
        $users = $this->getEntityManager()->createQuery("
            SELECT  user.id, user.firstName, user.lastName, user.status, user.email, user.cpf, user.image
            FROM $evtEventSeller evtSeller
            JOIN $user user  WITH user.id = evtSeller.genUser
            WHERE evtSeller.evtEvent = $evtEventId
        ")->getResult();
        
        return $users;
    }
    
    public function getUserContact($userId) {
        $genContact     = GenContact::class;
        $user           = GenUser::class;
        
        $contacts = $this->getEntityManager()->createQuery("
            SELECT  contact.phone
            FROM $genContact contact
            JOIN $user user  WITH user.id = contact.genUser
            WHERE contact.genUser = $userId
        ")->getResult();
        
        return $contacts;
    }
    
    /* CRISTIANO
     * Este metodo associa um evento a um ou mas menus
     */
    public function associateMenuProductEventSeller($userId, $nrorg, $eventSellerId, $arrayOrdMenus) {
        foreach($arrayOrdMenus as $ordMenuId) {
            $evtSellerProduct = new EvtSellerProduct();
            
            $evtEventSeller   = $this->getEntityManager()->getRepository(EvtEventSeller::class)->find($eventSellerId);
            $ordMenuProduct   = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($ordMenuId);
            
            $evtSellerProduct->setCreatedBy($userId);
            $evtSellerProduct->setNrorg($nrorg);
            $evtSellerProduct->setOrdMenuProduct($ordMenuProduct);
            $evtSellerProduct->setEvtEventSeller($evtEventSeller);
            $evtSellerProduct->setStatus("A");
            
            $this->getEntityManager()->persist($evtSellerProduct);
            $this->getEntityManager()->flush();
        }
    }
    
    public function getSellerDataFromEvent($eventId) {
        $evtEventSeller = EvtEventSeller::class;
        $genContact     = GenContact::class;
        $user           = GenUser::class;
        
        $eventSellers = $this->getEntityManager()->createQuery("
            SELECT eventSeller.id, event.id as eventId, user.firstName, user.cpf, user.id as userId, user.lastName, user.email, contact.phone 
            FROM $evtEventSeller eventSeller
            JOIN $user user WITH eventSeller.genUser = user.id AND user.status = 'A'
            LEFT JOIN $genContact contact WITH eventSeller.genUser = contact.genUser
            JOIN eventSeller.evtEvent event
            WHERE eventSeller.evtEvent = $eventId AND eventSeller.status = 'A'
        ")->getResult();
        
        return $eventSellers;
    }
    //Cristiano
    public function getProductsFromSeller($eventId) {
        $evtSellerProduct = EvtSellerProduct::class;
        $evtEventSeller   = EvtEventSeller::class;
        $ordMenuProduct   = OrdMenuProduct::class;

        $productsSeller = $this->getEntityManager()->createQuery("
            SELECT menuProduct.id, menuProduct.name, menuProduct.detail, menuProduct.price, productGroup.name as productGroupName, sellerProduct.id as sellerProductId 
            FROM $evtEventSeller eventSeller
            JOIN $evtSellerProduct sellerProduct WITH eventSeller.id = sellerProduct.evtEventSeller
            JOIN sellerProduct.ordMenuProduct menuProduct
            JOIN menuProduct.ordProductGroup productGroup
            WHERE eventSeller.evtEvent = $eventId AND sellerProduct.status != 'I'
        ")->getResult();
        
        return $productsSeller;
    }
    
    public function getMenuProductsFromSellerBySellerId($sellerId) {
        $evtSellerProduct = EvtSellerProduct::class;
        $evtEventSeller   = EvtEventSeller::class;
        $ordMenuProduct   = OrdMenuProduct::class;
        
        $menuProducts = $this->getEntityManager()->createQuery("
            SELECT mp.id, mp.name, mp.detail, mp.price, productGroup.name as productGroupName, sp.id as sellerProductId 
            FROM $evtSellerProduct sp
            JOIN $ordMenuProduct mp WITH mp.id = sp.ordMenuProduct
            JOIN mp.ordProductGroup productGroup
            WHERE sp.evtEventSeller = $sellerId AND sp.status != 'I'
        ")->getResult();
        
        return $menuProducts;        
    }
    
    public function disableProductMenu($sellerProductId) {
        $evtSellerProduct = $this->getEntityManager()->getRepository(EvtSellerProduct::class)->find($sellerProductId);
        $evtSellerProduct->setStatus("I");
        
        $this->getEntityManager()->persist($evtSellerProduct);
        $this->getEntityManager()->flush();
    }
    
    public function getProductsSellerNotSell($eventSellerId, $eventId) {
       $evtSellerProduct   = EvtSellerProduct::class;
        $ordProductGroup    = OrdProductGroup::class;
        $ordMenuProduct     = OrdMenuProduct::class;
        $evtEventMenu       = EvtEventMenu::class;
        $evtEventSeller     = EvtEventSeller::class;
        
        $productsSellerNotSell = $this->getEntityManager()->createQuery("
            SELECT omp.id, omp.name,  omp.price, omp.detail, omp.image,omp.status, opg.name as productGroupName, opg.id as productGroupId
            FROM $ordMenuProduct omp
            JOIN $ordProductGroup opg WITH opg.id = omp.ordProductGroup
            WHERE opg.evtEvent = $eventId
             AND omp.id not in (
                 SELECT omp1.id
                 FROM $evtSellerProduct esp1
                 JOIN esp1.ordMenuProduct omp1
                 WHERE esp1.evtEventSeller = $eventSellerId
                 AND esp1.status != 'I'
             )
            GROUP BY omp.id
            ")->getResult();
        
        return $productsSellerNotSell;
    }
}