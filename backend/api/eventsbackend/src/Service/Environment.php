<?php
namespace Zeedhi\ApiEvents\Service;

interface Environment {

    public function getUserId();

    public function setUserId($userId);

}