<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;
use Zeedhi\ApiEvents\Model\Entities\GenDependent;
use Zeedhi\ApiEvents\Model\Entities\GenUser;

use Zeedhi\Framework\ORM\DateTime;

use Doctrine\ORM\EntityManager;

class CreditCard extends UserOperation {

    /** @var InstantBuyPlatform */
    private $paymentPlatform;

    /**
     * @param EntityManager       $entityManager
     */
    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }

    /**
    * Get the creditcard by id.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param int $cardId
    * @return PayCreditcard
    */
    public function getCreditCard($cardId) {
        $creditCard = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($cardId);
        return $creditCard;
    }
    
    /**
    * Verify card ownership.
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param int $userId
    * @param int $cardId
    * @param int $nrorg
    * @return array
    */
    public function verifyCardOwnership($userId, $cardId, $nrorg) {
        $creditCard  = $this->getCreditCard($cardId);
        $cardOwnerId = $creditCard->getGenUser()->getId();
        if ($cardOwnerId == $userId)
            return ["cardOwnership" => PayCreditcard::FROM_USER];
        $dependentRels = $this->getEntityManager()->getRepository(GenDependent::class)->findBy(['nrorg' => $nrorg, 'dependent' => $userId]);
        
        foreach($dependentRels as $dependentRel) {
            $parentId  = $dependentRel->getParent()->getId();
            
            if($cardOwnerId == $parentId)
                return ["cardOwnership" => PayCreditcard::FROM_PARENT, "receiveNotification" => $dependentRel->getReceiveNotification(), "email" => $dependentRel->getReceiptsTo()];
        }
        
        return ["cardOwnership" => PayCreditcard::FROM_OTHER];
    }
    
    /**
    * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
    * 
    * @param 
    * @return 
    */
    public function getUser($userId) {
       $user = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
       return $user; 
    }
    
}