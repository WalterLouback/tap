<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProductGroup;

use Zeedhi\ApiGeneral\Helpers\Environment;

const EVENT_ID        = 'EVENT_ID';

use Doctrine\ORM\EntityManager;

class Product extends UserOperation{
    
    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }

    public function getProductGroups($event_id) {
        $product   = 'Zeedhi\ApiEvents\Model\Entities\OrdProduct';
        $productGroup   = 'Zeedhi\ApiEvents\Model\Entities\OrdProductGroup';
        
        return  $this->getEntityManager()->createQuery(
            "
            SELECT  pg.id , pg.name as category  
            FROM  $productGroup pg
            JOIN  $product p WITH p.ordProductGroup = pg.id
            WHERE pg.evtEvent = $event_id
            GROUP BY pg.id , pg.name
            "
        )->getResult();
    }
    
    public function getProducts($event_id) {
        $product   = 'Zeedhi\ApiEvents\Model\Entities\OrdProduct';
        $productGroup   = 'Zeedhi\ApiEvents\Model\Entities\OrdProductGroup';
        
        return $this->getEntityManager()->createQuery(
            "
            SELECT  p.id as pId, p.name as productName, p.price, p.detail,  pg.id as productGroupId, pg.name as nameProduct  
            FROM  $productGroup pg
            JOIN  $product p WITH p.ordProductGroup = pg.id 
            WHERE pg.evtEvent = $event_id
            "
        )->getResult();
    }
    
    public function getMenus($eventId) {
        $event = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $event->build($this->getEntityManager());
        return $event;
    }

}
