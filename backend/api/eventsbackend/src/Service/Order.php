<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;
use Zeedhi\ApiEvents\Model\Entities\PayWallet;
use Zeedhi\ApiEvents\Model\Entities\PayWalletCard;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderRO;
use Zeedhi\ApiEvents\Model\Entities\OrdCashier;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\EvtEventTicket;
use Zeedhi\ApiEvents\Model\Entities\GenStatus;
use Zeedhi\ApiEvents\Model\Entities\GenStructure;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\GenIntegration;
use Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod;
use Zeedhi\ApiEvents\Model\Entities\PayPaymentMethodIntegration;
use Zeedhi\ApiEvents\Service\Notification as NotificationService;
use Zeedhi\ApiEvents\Helpers\FCM;
use Zeedhi\ApiEvents\Model\Entities\GenOrganizationUserRel;
use Zeedhi\ApiEvents\Model\Entities\GenConfiguration;

use Zeedhi\ApiEvents\Model\Entities\OrdExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiEvents\Model\Entities\OrdItemExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdOption;

use Zeedhi\ApiEvents\Service\Exception  as Exception;
use Zeedhi\Framework\DataSource\DataSet;

//From ApiGeneral
use Zeedhi\ApiGeneral\Model\Entities\TokenAuthApi;
use Zeedhi\ApiGeneral\Model\Entities\GenUser as GenUserApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenContact as GenContactApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenAddress as GenAddressApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\EvtEvent as EvtEventApiGeneral;
use Zeedhi\ApiGeneral\Model\Entities\GenConfiguration as GenConfigurationApiGeneral;

use Doctrine\ORM\EntityManager;

class Order extends UserOperation {
    
    /**
     * __construct
     * 
     * @param EntityManager         $entityManager
     */
    public function __construct(EntityManager $entityManager, $paymentApiCC, $paymentApiBalance, NotificationService $notification) {
        parent::__construct($entityManager);
        $this->paymentApiCC = $paymentApiCC;
        $this->paymentApiBalance = $paymentApiBalance;
        $this->notification = $notification;
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $cardId = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard()->getId();
        $today  = new \DateTime();
        $firstDayOfMonth = $today->format('Y-m-d');

        $total = $this->getEntityManager()->createQuery(
        "
        SELECT SUM(o.total) as totalValue
        FROM " . OrdOrder::class . " o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createdAt >= '$firstDayOfMonth'
        "
        )->getResult()[0]['totalValue'];
        
        return $total;
    }
    
    public function verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $orderValue, $nrorg) {
        if ($paymentMethod != 'CC' || $orderValue == 0) return ;
        
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg]);
        
        if ($dependency == NULL) return ;
        
        $parent  = $dependency->getParent();
        
        if ($parent == NULL) return ;
        
        if ($parent->getMainCreditcard() == NULL) return ;
        
        if ($parent->getMainCreditcard()->getId() != $creditcardId) return ;
        
        $limit       = $dependency->getMonthlyLimit();
        $totalExpent = $this->getMonthExpensesFromDependent($parent->getId(), $userId, $nrorg);
        $canExpend   = $limit - $totalExpent;

        if ($canExpend < $orderValue) throw new Exception("User's monthly limit has exceeded! Limit is $limit, current is $totalExpent", 12);
    }

    /**
     * ordProDuct
     * 
     * @param array  $products
     * @param int $eventId
     * @param int $selectCardId
     *
     * @return OrDorder
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException    
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function createOrder($creditCardId, $eventId, $userId, $orderItems, $status=null, $fromTaa) {
        $event = $this->getEntityManager()->find(EvtEvent::class, $eventId);
        if ($event == NULL) throw new Exception('Invalid Order', 122);
        
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        $order->setType('TICKET');
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CREDITCARD);
        $order->setPayCreditcard($this->getEntityManager()->find(PayCreditCard::class, $creditCardId));
        $order->setEvtEvent($event);
        $order->setNrorg($event->getNrorg());
        $order->setPaymentStatus($status);
        $order->setFromTaa($fromTaa);
        $this->getEntityManager()->persist($order);

        $orderTotal = 0;
        
        foreach($orderItems as $key=>$orderItem){
            $evtEventTicket  = $this->getEntityManager()->find(EvtEventTicket::class, $orderItem['EVENT_TICKET_ID']);
            if(empty($evtEventTicket)) throw new \Exception('Ticket not found');
            
            $ordOrderProduct[$key] = new OrdOrderProduct();
            $ordOrderProduct[$key]->setOrdOrder($order);
            $ordOrderProduct[$key]->setEvtEventTicket($evtEventTicket);
            $ordOrderProduct[$key]->setQuantity(count($orderItem['USERS']));
            $ordOrderProduct[$key]->setTotal($evtEventTicket->getPrice() * count($orderItem['USERS']));

            $this->getEntityManager()->persist($ordOrderProduct[$key]);
            $orderTotal += $ordOrderProduct[$key]->getTotal();
        }

        $order->setTotal($orderTotal);
        $this->getEntityManager()->flush();

        return $order;
    }

    /* Creates new Order with the passed data */
    public function createOrderEvent($paymentMethod, $statusId, $total, $userId, $sellerId, $eventId, $nrorg, $creditcardId, $cashierId, $creationDate, $transactionDetails, $paymentStatus, $orderType, $walletId, $canFlush=true,  $transactionId=null, $stoneNote=null, $convenienceFee=null, $barCode=null, $fromTaa) {
        /* Getting necessary entities to set to the order */
        $user           = $userId ? $this->getEntityManager()->getRepository(GenUser::class)->find($userId) : NULL;
        $seller         = $sellerId ? $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $sellerId, 'nrorg' => $nrorg, 'status' => 'A', 'evtEvent' => $eventId]) : NULL;
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        $wallet         = $walletId ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        if ($cashierId !== NULL) $cashier = $this->getEntityManager()->getRepository(OrdCashier::class)->find($cashierId);
        else $cashier   = NULL;
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setStatus($status);
        $order->setPaymentStatus($paymentStatus !== NULL ? $paymentStatus : 'A');
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setEvtEventSeller($seller);
        $order->setType($orderType !== NULL ? $orderType : 'ORDER_EVENT');
        $order->setEvtEvent($event);
        $order->setNrorg($nrorg);
        $order->setPayWallet($wallet);
        $order->setOrdCashier($cashier);
        $order->setStoneNote($stoneNote);
        $order->setFromTaa($fromTaa);

        if ($transactionDetails !== NULL) {
            $order->setTransactionId($transactionDetails['authorizationCode']);
            $order->setNsu($transactionDetails['documentId']);
            $order->setCreditCardBrand($transactionDetails['cardBrand']);
            $order->setCreditCardNumbers($transactionDetails['cardNumber']);
        }
        else if($transactionId){
            $order->setTransactionId($transactionId);
        }
        
        if ($creationDate !== NULL) $order->setCreateDate(new \DateTime($creationDate));
        else $order->setCreateDate(new \DateTime());

        if ($creditcardId !== NULL) {
            $creditcard     = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId);
            $order->setPayCreditcard($creditcard);
        }
        if($barCode){
            $order->setBarCode($barCode);
        }
        if($convenienceFee){
            $order->setConvenienceFee($convenienceFee);
        }
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        return $order;
    }
    
    /**
     * @param integer  $selectCardId
     * @param integer $buyingValue
     *
     * @return Ordorder
     */
    public function createOrderWallet($userId, $selectCardId, $buyingValue, $eventId, $nrorg){
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($userId, 'CC', $selectCardId, $buyingValue, $nrorg);
        
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CREDITCARD_FOR_WALLET);
        $order->setType('WALLET');
        $order->setNrorg($nrorg);
        $order->setTotal($buyingValue);
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPayCreditcard($this->getEntityManager()->find(PayCreditcard::class, $selectCardId));
        $order->setPaymentStatus(OrdOrder::STATUS_PAYMENT_PENDING);
        
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
        
        return $order;
    }
    
    public function addBalanceToWallet($walletId, $value) {
        $wallet = $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId);
        $wallet->setBalance($wallet->getBalance() + $value);
        $this->getEntityManager()->flush();
    }
    
    public function createOrderWalletFromGUID($walletId, $walletCardId, $buyingValue, $identifier, $eventId, $nrorg, $cashierId, $paymentMethod, $userId, $sellerUserId, $orderStatus=NULL, $transactionDetails=NULL){
        $order = new OrdOrder();
        $event = $eventId ? $this->getEntityManager()->getReference(EvtEvent::class, $eventId) : null;
        $sellerUser = $sellerUserId ? $this->getEntityManager()->getRepository(EvtEventSeller::class)->findOneBy(['genUser' => $sellerUserId, 'nrorg' => $nrorg, 'status' => 'A', 'evtEvent' => $eventId]) : null;
        
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setType('WALLET');
        if ($orderStatus !== NULL) $order->setStatus($this->getEntityManager()->getReference(GenStatus::class, $orderStatus));
        $order->setNrorg($nrorg);
        $order->setOrdCashier($this->getEntityManager()->getReference(OrdCashier::class, $cashierId));
        $order->setTotal($buyingValue);
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        $order->setEvtEventSeller($sellerUser);
        if ($event) {
            $order->setEvtEvent($event);
        }
        $order->setPaymentStatus($buyingValue > 0 ? OrdOrder::STATUS_PAID : OrdOrder::STATUS_PAYMENT_PENDING);
        if ($transactionDetails !== NULL) {
            $order->setTransactionId($transactionDetails['authorizationCode']);
            $order->setNsu($transactionDetails['documentId']);
            $order->setCreditCardBrand($transactionDetails['cardBrand']);
            $order->setCreditCardNumbers($transactionDetails['cardNumber']);
        }
        
        $this->getEntityManager()->persist($order);
        
        $wallet = $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy(['id' => $walletId, 'nrorg' => $nrorg, 'status' => 'A']);
        
        if (!$wallet && $walletCardId) {
            $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
            if ($walletCard) {
                $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
            }
        }
        
        if ($wallet == NULL) {
            $wallet = new PayWallet();
            $walletId = $walletId ? $walletId : PayWallet::getGUID();
            $wallet->setId($walletId);
            $wallet->setNrorg($nrorg);
            $wallet->setEvtEvent($event);
            $wallet->setBalance(0);
            $wallet->setIdentifier($identifier);
            if ($orderStatus !== 7) {
                $wallet->setStatus('A');
            }
            $this->getEntityManager()->persist($wallet);
        }
        
        if ($walletCardId && !$walletCard) {
            $walletCard = new PayWalletCard();
            $walletCard->setNrorg($nrorg);
            $walletCard->setStatus('A');
            $walletCard->setSerialNumber($walletCardId);
            $walletCard->setWallet($wallet);
            $this->getEntityManager()->persist($walletCard);
        }
        
        $order->setPayWallet($wallet);
            
        $wallet->setBalance($wallet->getBalance() + $buyingValue);
        
        $this->getEntityManager()->flush();
        
        return [ $order, $wallet->getId() ];
    }
    
    public function getWalletFromIdentifier($identifier, $nrorg) {
        if (empty($identifier)) return NULL;
        $filter = array('identifier' => $identifier, 'nrorg' => $nrorg, 'status' => 'A');
        return $this->getEntityManager()->getRepository(PayWallet::class)->findBy($filter);
    }
    
    public function factoryOrderDataSet($order, $walletId=NULL) {
        return array('ORDER' => array(
            "ID" => $order->getId(),
            "TYPE" => $order->getType(),
            "NRORG" => $order->getNrorg(),
            "TOTAL" => $order->getTotal(),
            "STATUS" => $order->getStatus(),
            "GEN_USER_ID" => $order->getGenUser() != NULL ? $order->getGenUser()->getId() : NULL,
            "CREATE_DATE" => $order->getCreatedAt(),
            // "EVENT_ID" => $order->getEvtEvent()->getId(),
            // "EVENT_NAME" => $order->getEvtEvent()->getName(),
            "PAYMENT_METHOD" => $order->getPaymentMethod(),
            "PAYMENT_STATUS" => $order->getPaymentStatus(),
            "TRANSACTION_ID" => $order->getTransactionId(),
            "CREDIT_CARD_ID" => $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL,
            "WALLET_ID" => $walletId
        ));
    }
    
    public function getOrderById($orderId){
        $filter = array('id' => $orderId);
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy($filter);
        if (empty($order)) {
            $filter = array('orderIdentifier' => $orderId);
            $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy($filter);
            if (empty($order)) throw Exception::orderNotFound();
        }
        $order->build($this->getEntityManager());
        return $order;
    }
    
    public function getOrderProductByOrder($orderId){
        $filter = array('ordOrder' => $orderId);
        return $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findBy($filter);
    }
    
    public function getOrdersByUser($userId, $paymentMethod=null, $nrorg, $eventId=null, $initialDate=null, $finalDate=null){
        $dataInicial = $initialDate == null ? new \DateTime() : $initialDate;
        $dataFinal   = $finalDate == null ? new \DateTime() : $finalDate;
        $dataInicial = $initialDate == null ? $dataInicial->format('Y-m-01') : $dataInicial;
        $dataFinal = $finalDate == null ? $dataFinal->format('Y-m-31') : $finalDate;
        $paymentMethodQuery = '';
        
        if($paymentMethod != NULL) {
            $paymentMethodQuery   = $paymentMethod == 'BALANCE' ? "AND (o.paymentMethod = 'BALANCE' OR o.paymentMethod = 'CC_W')" : "AND ( o.paymentMethod = 'CC' OR o.paymentMethod = 'CC_W' ) ";
        }
        
        $dateQuery   = " AND o.createdAt >= '$dataInicial' AND o.createdAt <= '$dataFinal'";
        $eventQuery  = $eventId != NULL ? " AND o.evtEvent = $eventId" : "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            
            SELECT o
            FROM " . OrdOrder::class . " o
            WHERE o.genUser = $userId
            AND (o.paymentStatus = 'A' OR o.paymentStatus = 'R')
            AND o.nrorg = $nrorg
            $eventQuery
            $paymentMethodQuery
            $dateQuery
            ORDER BY o.createdAt
            DESC
            "
        )->getResult();

        return $orders;
    }
    
    public function getOrderByTransactionId($id){
        $filter = array('transactionId' => $id);
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy($filter);
        if ($order) $order->build($this->getEntityManager());
        return $order;
    }
    
    public function getOrdersWithItems($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate){
        $orders = $this->getOrdersByUser($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate);
        $orderFactory = [];
        foreach ($orders as $key=>$order) {
            $order->build($this->getEntityManager());
            $orderFactory[$key] = $order->toArray();
        }
        return $orderFactory;
    }
    
    public function updateAll(){
        $this->getEntityManager()->flush();
    }

    /* Creates ProductOrders with the received items */
    public function createProductOrders($order, $orderItems, $removeAmount=false, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($orderItems as $item) {
            /* Getting the product referent to the item */
            $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($item['productId']);
            if ($removeAmount && $menuProduct->getAmount() != NULL) $menuProduct->setAmount($menuProduct->getAmount() - $item['quant']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setItemIdentifier(isset($item['itemIdentifier']) ? $item['itemIdentifier'] : OrdOrder::getNumericalUID());
            $po->setQuantity($item['quant']);
            $po->setTotal($item['totalItem']);
            $po->setOrdMenuProduct($menuProduct);
            $po->setOrdOrder($order);
            $po->setStatus('P');
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            array_push($productOrders, $po);
            if (isset($item['extras'])) {
                $this->createItemExtras($menuProduct, $po, $item['extras']);
            }
            
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        // $order->build($this->getEntityManager());
        
        return $productOrders;
    }
    
    /* Creates extras and options of a product */
    function createItemExtras($product, $productOrder, $itemExtras) {
        $extrasTotalPrice = 0;

        /* Iterating through the extras of the item sent by the front-end */
        foreach ($itemExtras as $e) {
            /* Getting Extra to set to ItemExtra */
            $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($e['extraId']);
            
            /* Instantiating ItemExtra and setting attributes */
            $itemExtra = new OrdItemExtra();
            $itemExtra->setOrdOrderProduct($productOrder);
            $itemExtra->setOrdExtra($extra);
            
            /* Storing ItemExtra in the database */
            
            /* Iterating through the selected options of this ItemExtra */
            $itemExtraTotalPrice = 0;
            $itemExtraTotalQuantity = 0;
            foreach ($e['selectedOptions'] as $o) {
                /* Getting Option to set to SelectedOption */
                $option = $this->getEntityManager()->getRepository(OrdOption::class)->find($o['id']);
                
                /* Instantiating SelectedOption and setting attributes */
                $selectedOption = new OrdSelectedOption();
                $selectedOption->setOrdOption($option);
                $selectedOption->setOrdItemExtra($itemExtra);
                $selectedOption->setPrice($option->getPrice());
                if (array_key_exists('quantity', $o)){
                    $selectedOption->setSelectedQuantity($o['quantity']);
                    $itemExtraTotalPrice += $o['quantity'] * $option->getPrice();
                    $itemExtraTotalQuantity += $o['quantity'];
                }
                else {
                    $selectedOption->setSelectedQuantity(1);
                    $itemExtraTotalPrice += $option->getPrice();
                }
                /* Storing SelectedOption in database */
                $this->getEntityManager()->persist($selectedOption);
            }
            $this->getEntityManager()->persist($itemExtra);
        }

    }
    
    /* Gets an user's orders in an organization */
    function getOrdersFromUser($userId, $structureId, $nrorg, $initial=NULL, $max=NULL) {
        $order   = OrdOrder::class;
        $nrorgQuery = $nrorg !== NULL ? "AND o.nrorg = $nrorg" :  "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent e
            JOIN o.genUser u
            WHERE u = $userId
            AND e.type = 'E'
            $nrorgQuery
            ORDER BY o.createdAt desc
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        return $orders;
    }
    
    /* Checks how to procede with the payment of an order and after that changes its status */
    function makePaymentTransaction($order, $paymentMethod, $creditCardId=NULL, $nrorg, $walletId=NULL) {
        $user       = $this->getUserByIdAndNrorg($order->getGenUser()->getId(), $order->getNrorg());
        $value      = $order->getTotal();
        $eventName  = $order->getEvtEvent()->getName();
        $message    = "Compra realizada no evento $eventName no valor de R$ $value";
        $isExternal = $paymentMethod !== 'CC' && $paymentMethod !== 'BALANCE' &&$paymentMethod !== 'STONE';
        
        if ($paymentMethod == 'CC') {
            $this->makePaymentTransactionWithCC($order, $paymentMethod, $creditCardId);
        } else if ($paymentMethod == 'BALANCE') {
            $this->makePaymentTransactionWithBalance($order, $paymentMethod, $nrorg, $walletId);
        } else if ($paymentMethod == 'PICPAY') {
            $order->setPaymentStatus('A');
            foreach ($order->getItems() as $orderProduct) {
                $mp = $orderProduct->getOrdMenuProduct();
                if ($mp->getAmount() !== NULL) $mp->setAmount($mp->getAmount() - $orderProduct->getQuantity());
            }
        }else $order->setPaymentStatus('A');
        
        $this->getEntityManager()->flush();
        
        if (!$isExternal) {
            $notificationData = NULL;
        
            $title = "Sobre sua compra";
            $originNotification = "Order_Eventsbackend";
            $status             = "P";
            $dateTime           = new \DateTime();
            
            try {
                if ($user !== NULL) {
                    $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user[0]["userId"], $originNotification, $dateTime, $notificationData, $title, $user[0]["firebaseToken"]);
                }
            } catch (\Exception $e) {
                
            }
        }
    }
    
    public function makePaymentTransactionWithCC($order, $paymentMethod, $creditCardId=NULL) {
        /* Send the order data to the payment API */
        $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
        $orderId    = $order->getId();
        $value      = $order->getTotal();
        $ccToken    = $creditCard->getToken();
        $customerId = $creditCard->getCustomerId();
        $gateway    = $creditCard->getGateway();
        $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
        $merchant   = $order->getEvtEvent()->getMerchantKey();
        $merchantId = $order->getEvtEvent()->getMerchantId();
        $payment    = $this->paymentApiCC->execPaymentCreditCard($merchant, $merchantId, $gateway, $order);
        
        /* Getting data from the payment */
        $status     = $payment['status'];
        $order->setTransactionId($payment['transaction_id']);
        
        /* Set the payment status */
        $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
        
        /* If the payment was not successful */
        if ($status !== 'captured') {
            $this->getEntityManager()->flush();
            throw new Exception('Payment not successful. Got status \'' . $status . '\'');
        }
    }
    
    public function makePaymentTransactionWithBalance($order, $paymentMethod, $nrorg, $walletId=null) {
        $userId     = $order->getGenUser() !== NULL ? $order->getGenUser()->getId() : NULL;
        $walletId   = $order->getPayWallet() !== NULL ? $order->getPayWallet()->getId() : NULL;
        $value      = $order->getTotal();

        if($order->getPayWallet() != NULL && $order->getPayWallet()->getGenUser() !== NULL && $order->getPayWallet()->getGenUser()->getId() != $userId)
            throw new \Exception('Wallet does not belong to the user', 66);
        if($order->getPayWallet() != NULL && $order->getPayWallet()->getStatus() !== 'A')
            throw new \Exception('Wallet is not active', 67);
        $this->paymentApiBalance->execPaymentBalance($value, $userId, $nrorg, $walletId);
        
        $order->setPaymentStatus('A');
    }
    
    public function createOrderEventWithWalletAndEventSeller($orderItems, $totalValue, $evtEventSellerId, $eventId, $nrorg, $paymentMethod, $creditCardId, $walletId, $walletCardId, $userId, $statusId, $orderType, $canFlush=true, $transactionId = null) {
        /* Getting necessary entities to set to the order */
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        $user           = $userId !== NULL ? $this->getEntityManager()->getRepository(GenUser::class)->find($userId) : NULL;
        $wallet         = $walletId !== NULL ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        $walletCard     = $walletCardId !== NULL ? $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'status' => 'A', 'nrorg' => $nrorg]) : NULL;
        $creditcard     = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId) : NULL;
        $evtEventSeller = $evtEventSellerId !== NULL ? $this->getEntityManager()->getRepository(EvtEventSeller::class)->find($evtEventSellerId) : NULL;
        
        if (!$wallet && (!$walletCard || !$walletCard->getWallet())) throw new \Exception('Invalid wallet');
        if (($wallet && $nrorg != $wallet->getNrorg()) || ($walletCard && $nrorg != $walletCard->getNrorg())) throw new \Exception('Invalid nrorg');
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setStatus($status);
        $order->setPaymentStatus('N');
        $order->setTotal($totalValue);
        $order->setGenUser($user);
        $order->setType($orderType);
        $order->setEvtEvent($event);
        $order->setNrorg($nrorg);
        $order->setPayCreditcard($creditcard);
        $order->setEvtEventSeller($evtEventSeller);
        $order->setPayWallet($wallet ? $wallet : $walletCard->getWallet());
        if ($transactionId)$order->setTransactionId($transactionId);
        /* Storing order in database */
        $this->getEntityManager()->persist($order);

        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;    
    }
    
    public function getEventsReport($userId, $nrorg, $eventId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods, $deliveryAddress) {
        $order          = OrdOrderRO::class;
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);

        if (!is_null($orderStatus))
            $statusOrder = join($orderStatus, ' OR st.id = ');
        $statusQuery = $orderStatus !== NULL ? "AND (st.id = $statusOrder)" : "AND (st.id is null or st.id <> 7)";
        
        if (!is_null($paymentStatus))
            $statusPayment = "'". join($paymentStatus, "' OR o.paymentStatus = '") . "'";
        $paymentStatusQuery = $paymentStatus !== NULL ? "AND (o.paymentStatus = $statusPayment)" : ""; 
        
        if (!is_null($paymentMethods))
            $paymentMethodsJoin = "'". join($paymentMethods, "' OR o.paymentMethod like '") . "'";
        $paymentMethodsQuery = $paymentMethods !== NULL ? "AND (o.paymentMethod like $paymentMethodsJoin)" : ""; 
        
        if (!is_null($eventId))
            $eventIds = join($eventId, ' OR e.id = ');
        $eventQuery = $eventId !== NULL ? "AND (e.id = $eventIds)" : "";     
        
        if (!is_null($structureId))
            $structureIds = join($structureId, ' OR sr.id = ');
        $structureQuery = $structureId !== NULL ? "AND (sr.id = $structureIds)" : "";  
           
        $deliverToQuery = $deliverTo !== NULL ? "AND (o.deliverTo = '$deliverTo')" : "";
        $deliveryAddressQuery = $deliverAddress !== NULL ? "AND (o.deliverAddress = '$deliverAddress')" : "";
        
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:01'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent e
            LEFT JOIN o.status st
            LEFT JOIN o.genStructure sr
            WHERE o.nrorg = $nrorg
            AND e.type = 'E'
            $structureQuery
            $eventQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            $paymentStatusQuery
            $deliverToQuery
            $deliveryAddressQuery
            $paymentMethodsQuery
            ORDER BY o.createdAt desc
            "
        ); 
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    public function getOrdersByOrganization($paymentStatus, $orderType, $paymentMethod, $nrorg) {
        $order = OrdOrder::class;
        
        $queryOrderType = $orderType ? " AND o.type = '$orderType' " : "";
        $queryPaymentStatus = $paymentStatus ? " AND o.paymentStatus = '$paymentStatus' " : "";
        $queryPaymentMethod = $paymentMethod ? " AND o.paymentMethod = '$paymentMethod' " : "";
        
        $balance = $this->getEntityManager()->createQuery(
        "
            SELECT o
            FROM $order o
            WHERE o.nrorg = $nrorg
            $queryOrderType
            $queryPaymentStatus
            $queryPaymentMethod
        "
        )->getResult();
        
        return OrdOrder::manyToArray($balance);
    }
    
    public function getBalanceByOrganization($paymentStatus, $orderType, $paymentMethod, $nrorg) {
        $order = OrdOrder::class;
        $queryOrderType = $orderType ? " AND o.type = '$orderType' " : "";
        $queryPaymentStatus = $paymentStatus ? " AND o.paymentStatus = '$paymentStatus' " : "";
        $queryPaymentMethod = $paymentMethod ? " AND o.paymentMethod = '$paymentMethod' " : "";
        
        $balance = $this->getEntityManager()->createQuery(
        "
            SELECT SUM(o.total) as balance
            FROM $order o
            WHERE o.nrorg = $nrorg
            $queryOrderType
            $queryPaymentStatus    
            $queryPaymentMethod
        "
        )->getResult();
        return $balance;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
    public function createOrderDeleteWallet($userId, $walletId, $walletCardId, $balance, $identifier, $eventId, $nrorg){
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CASH_FOR_WALLET);
        $order->setType('WALLET');
        $order->setNrorg($nrorg);
        $order->setTotal($balance);
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPaymentStatus('B'); /*Blocked*/
        
        $this->getEntityManager()->persist($order);
        
        $wallet = $walletId ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        
        if (!$wallet && $walletCardId) {
            $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
            if ($walletCard) $walletCard->setStatus('I');
        } else if ($wallet) {
            $wallet->setStatus('I');
        }
        
        $this->getEntityManager()->flush();
        
        return $order;
    }
    
    public function createOrderBalanceTransfer($userId, $walletId, $walletCardId, $walletIdReceive, $walletCardIdReceive, $transferValue, $eventId, $nrorg){
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(OrdOrder::PAYMENT_METHOD_BALANCE);
        $order->setType('WALLET_TRANSFER');
        $order->setNrorg($nrorg);
        $order->setTotal($transferValue);
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPaymentStatus('A'); /*Transferred*/
        
        $this->getEntityManager()->persist($order);

        $wallet = $walletId ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        
        if (!$wallet && $walletCardId) {
            $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
            $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
        }
        
        if ($wallet ==  NULL || $wallet->getStatus() !== 'A') {
            throw new \Exception('Wallet not found', 1423);
        }

        $order->setPayWallet($wallet);
        if ($wallet->getBalance() < $transferValue) throw new \Exception('Insufficient funds', 111);
        $wallet->setBalance($wallet->getBalance() - $transferValue);
        
        $this->createOrderBalanceReceive($walletIdReceive, $walletCardIdReceive, $transferValue, $eventId, $nrorg);
        
        return $order;
    }
    
    public function createOrderBalanceReceive($walletId, $walletCardId, $transferValue, $eventId, $nrorg){
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(OrdOrder::PAYMENT_METHOD_BALANCE);
        $order->setType('WALLET_RECEIVE');
        $order->setNrorg($nrorg);
        $order->setTotal($transferValue);
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPaymentStatus('A');
        
        $this->getEntityManager()->persist($order);
        
        $wallet = $walletId ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        
        if (!$wallet && $walletCardId) {
            $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
            if ($walletCard) $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
        }
        
        if ($wallet ==  NULL || $wallet->getStatus() !== 'A') {
            throw new \Exception('Wallet not found', 1423);
        } else if ($wallet->getNrorg() != $nrorg) {
            throw new \Exception('Invalid nrorg');
        }
        
        $order->setPayWallet($wallet);
        $wallet->setBalance($wallet->getBalance() + $transferValue);

        $this->getEntityManager()->flush();
        
        return $order;
    }
    
    public function getOrdersFromWallet($paymentMethod=null, $nrorg, $eventId=null, $initialDate=null, $finalDate=null, $walletId){
        $dataInicial = $initialDate == null ? new \DateTime() : $initialDate;
        $dataFinal   = $finalDate == null ? new \DateTime() : $finalDate;
        $dataInicial = $initialDate == null ? $dataInicial->format('Y-m-01 00:00:00') : $dataInicial;
        $dataFinal = $finalDate == null ? $dataFinal->format('Y-m-31 23:59:59') : $finalDate;
        $paymentMethodQuery = '';
        
        if($paymentMethod != NULL) {
            $paymentMethodQuery   = $paymentMethod == 'BALANCE' ? "AND (o.paymentMethod = 'BALANCE' OR o.paymentMethod = 'CC_W')" : "AND ( o.paymentMethod = 'CC' OR o.paymentMethod = 'CC_W' ) ";
        }

        $dateQuery   = " AND o.createdAt >= '$dataInicial' AND o.createdAt <= '$dataFinal'";
        $eventQuery  = $eventId != NULL ? " AND o.evtEvent = $eventId" : "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM " . OrdOrder::class . " o
            JOIN o.payWallet w
            WHERE w.id = '$walletId'
            AND o.nrorg = $nrorg
            $eventQuery
            $paymentMethodQuery
            $dateQuery
            AND ( o.status is null or o.status <> 7 )
            ORDER BY o.createdAt
            DESC
            "
        )->getResult();

        return $orders;
    }
    
    public function getOrdersWithItemsFromWallet($paymentMethod, $nrorg, $eventId, $initialDate, $finalDate, $walletId, $walletCardId){
        if ($walletCardId) {
            $walletCard = $this->getEntityManager()->getRepository(PayWalletCard::class)->findOneBy(['serialNumber' => $walletCardId, 'nrorg' => $nrorg, 'status' => 'A']);
            $wallet = $walletCard->getWallet() ? $walletCard->getWallet() : NULL;
            if ($wallet) $walletId = $wallet->getId();
        }
        $orders = $this->getOrdersFromWallet($paymentMethod, $nrorg, $eventId, $initialDate, $finalDate, $walletId);
        $orderFactory = [];
        foreach ($orders as $key=>$order) {
            $order->build($this->getEntityManager());
            $orderFactory[$key] = $order->toArray();
        }
        return $orderFactory;
    }
    
    public function getOrderProductByIdentifier($itemIdentifier) {
        return $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findOneBy(['itemIdentifier' => $itemIdentifier]);
    }
    
    public function makePdvIntegration($order) {
        $eventId = $order->getEvtEvent()->getId();
        
        $integration = $this->getEntityManager()->getRepository(GenIntegration::class)->findOneBy(['evtEvent' => $eventId]);
        
        if ($integration === NULL) return ;
        
        switch ($integration->getType()) {
            case 'FOR_SALE':
                $this->persistForSaleOrder($order, $integration);
                break;
        }
    }
    
    public function persistForSaleOrder($order, $integration) {
        $paymentMethodIntegration = $this->getEntityManager()->getRepository(PayPaymentMethodIntegration::class)->findOneBy(['paymentMethod' => $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $order->getPaymentMethod()]), 'genIntegration' => $integration]);
        
        if ($paymentMethodIntegration === NULL) return ;
        
        $itemsArray = [];
        
        $order->setItems($this->getEntityManager());
        
        foreach ($order->getItems() as $item) {
            array_push($itemsArray, array(
                "title" => $item->getOrdMenuProduct()->getName(),
                "quantity" => $item->getQuantity(),
                "price" => $item->getOrdMenuProduct()->getPrice(),
                "total" => $item->getTotal(),
                "notes" => $item->getNote(),
                "ref" => $item->getOrdMenuProduct()->getId()
            ));
        }
        
        $body = array(
            "saleId" => $order->getId(),
            "orderNumber" => $order->getOrderIdentifier(),
            "orderType" => 2,
            "date" => $order->getCreateDate()->format('Y-m-d H:i:s'),
            "scheduleDate" => $order->getCreateDate()->format('Y-m-d H:i:s'),
            "subsidiaryCode" => $integration->getSubsidiaryCode(),
            "tax" => 0,
            "subTotal" => $order->getTotal(),
            "total" => $order->getTotal(),
            "troco" => 0,
            "discount" => 0,
            "latitude" => "",
            "longitude" => "",
            "cpfInNote" => "",
            "notes" => $order->getNote(),
            "consumerDetail" => array(
                "clientId" => "0000000001",
                "name" => "Teknisa",
                "email" => "teknisa@teknisa.com",
                "phone" => "(31)2122-2300",
                "cpf" => "26269316000177",
                "address" => array(
                    "zipCode" => "30130151",
                    "street" => "Rua Pernambuco",
                    "number" => "1000",
                    "neighborhood" => "Savassi",
                    "city" => "Belo Horizonte",
                    "uf" => "MG",
                    "complement" => "",
                    "referencePoint" => ""
                )
            ),
            "payments" => array(
                array(
                    "paymentId" => $paymentMethodIntegration->getExternalId(),
                    "value" => $order->getTotal()
                )
            ),
            "ItemOrder" => $itemsArray
        );
        
        $integration->setBaseUrl("http://lucasmacedo1.zeedhi.com/workfolder/odhen-orderapi/backend/service/index.php/");
        $response = $this->makeCurl($integration->getBaseUrl() . "order", $body, $integration->getToken());
        
        return $response;
    }
    
    private function makeCurl($url, $body, $token) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER  , true);  // we want headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json', 'Content-Length: ' . strlen(json_encode($body)), "Authorization: $token" ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        $objResult = json_decode($result, true);
        
        return $objResult;
    }
    
    public function getUserByIdAndNrorg($userId, $nrorg) {
        $genUser = GenUser::class;
        $genOrganizationUserRel = GenOrganizationUserRel::class;
        
        $user = $this->getEntityManager()->createQuery(
            "
            SELECT DISTINCT u.id userId, our.firebaseToken
            FROM $genUser u
            JOIN $genOrganizationUserRel our WITH (our.nrorg = '$nrorg' AND u = our.genUser)
            WHERE u.id = '$userId' AND u.status = 'A'
            "
        )->getResult();
        
        return $user;
    }
    
    public function getOrderByQuery( $orderId){
         $order = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM ".OrdOrder::Class." o
            WHERE o.id = '$orderId'
            "
        )->getResult();
        return $order[0]; 
    }
    public function updateStoneTransactionId($order,$preTransactionId){
        $order->setTransactionId($preTransactionId);
         $this->getEntityManager()->flush();
        
    }
    
    public function updateStoneTransaction($order, $credit_card_brand, $card_holder_name, $credit_card_numbers, $paymentType) {
        $order->setPaymentStatus('A');
        
        if ($credit_card_brand) {
            $order->setCreditCardBrand($credit_card_brand);
        }

        if ($credit_card_numbers) $order->setCreditCardNumbers($credit_card_numbers);
        if ($card_holder_name) $order->setCardHolderName($card_holder_name);
        if ($paymentType) $this->updatePaymentMethod($order, $credit_card_brand,$order->getPaymentMethod(), $paymentType);
        
        
        $this->getEntityManager()->flush();
    }
    
    public function updateProduct($order) {
        foreach ($order->getItems() as $orderProduct) {
            $mp = $orderProduct->getOrdMenuProduct(); 
            
            if($mp->getAmount()!==null) {
                $mp->setAmount($mp->getAmount()-$orderProduct->getQuantity());
                $this->getEntityManager()->persist($mp);
                $this->getEntityManager()->flush();
            }
        }
    }
     
    public function updatePaymentMethod($order, $credit_card_brand,$method, $paymentType) {
        $oldMethod      = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $method]);

        if ($paymentType == 1) {
            $id = 474;
        } elseif ($paymentType == 2) {
            $id = 475;
        }
        
        if ($oldMethod->getid() != $id) {
            $rightMethod = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['id' => $id]);
        } else {
            $rightMethod = $oldMethod;
        }
        
        $currentMethod  = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['parentId' => $oldMethod->getid(), 'label' => $credit_card_brand]);
        
        if($currentMethod) $order->setPaymentMethod( $currentMethod->getPaymentMethod());
        if($rightMethod) $order->setPaymentMethod( $rightMethod->getPaymentMethod());
        
    }
    
    public function closeTicket($api_external_url, $body){
        $response = self::makeCurlExternalApi($api_external_url, $body, '','/comandas/fechamento');
        
        try {
            $explodeError = explode('"error":',$response);
            $explodeBarCode = explode('"barCode":',$response);
            $explodeSaleCode = explode('"saleCode":',$response);
            
            if (isset($explodeError[1]) && isset($explodeBarCode[1]) && isset($explodeSaleCode[1])) {
                $error_api = explode(',', $explodeError[1])[0] == 'true';
                $barCode_api = explode('"',$explodeBarCode[1])[1];
                $saleCode_api = explode('"',$explodeSaleCode[1])[1];
            } else {
                $error_api = true;
                $barCode_api = "2";
                $saleCode_api = "";
            }
        } catch (\Exception $j) {
                $error_api = true;
                $barCode_api = "1";
                $saleCode_api = "";
        }
        return ["error"=>$error_api, "barCode"=>$barCode_api, "saleCode"=>$saleCode_api];
    }
    
    public function verifyTokenAndUrl($nrorg, $storeId) {
        $url = null;
        $token = null;
        $origin = 'EATTK';
        $external_id = null;
        
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        if(isset($evtEvent)) {
            $external_id = $evtEvent->getExternalId();
            
            $nrorgItegracaoGenconfiguration = $this->getEntityManager()->getRepository(GenConfiguration::class)->findOneBy(['nrorg' => $nrorg]);
            $nrorgItegracao = $nrorgItegracaoGenconfiguration->getNrorgIntegracao();
            
            if($nrorgItegracao != NULL) { 
                $genconfiguration = $this->getEntityManager()->getRepository(GenConfigurationApiGeneral::class)->findOneBy(['nrorg' => $nrorgItegracao]);
            } else {
                $genconfiguration = $this->getEntityManager()->getRepository(GenConfigurationApiGeneral::class)->findOneBy(['nrorg' => $nrorg]);
            }
            
            if($genconfiguration->getTokenApiOdhen() != NULL) {
                if($genconfiguration->getDbName() != NULL && $genconfiguration->gettokenApiExternal() != NULL && $genconfiguration->geturlApiExternal() != NULL){
                    $url = $genconfiguration->geturlApiExternal();
                    $token = $genconfiguration->getTokenApiOdhen();
                }else{
                    $token = $genconfiguration->getTokenApiOdhen();
                    $url = 'https://odh-order-api.teknisa.cloud/saas/';                    
                }
            } else {
                $url = 'https://odh-order-api.teknisa.cloud';
                $dbName = $genconfiguration->getDbName();
                
                if(!empty($dbName)){
                    $bodyString = "{ \"dbname\": \"$dbName\" }";
                } else {
                    if($nrorgItegracao != NULL){
                        $bodyString = "{ \"dbname\": \"USR_ORG_$nrorgItegracao\" }";
                    } else {
                        $bodyString = "{ \"dbname\": \"USR_ORG_$nrorg\" }";
                    }
                }
                $body = json_decode($bodyString);
                $prefix = "/auth/";
                $stringResponse = $this->makeCurlExternalApi($url, $body, $token, $prefix);
            
                if(isset($stringResponse)) {
                    $explodeToken = explode(":", $stringResponse);
                    $explodeToken = explode('}', $explodeToken[count($explodeToken) -1])[0];
                    $token = explode("\"", $explodeToken)[1];
                    $genconfiguration->setTokenApiOdhen($token);
                    $this->getEntityManager()->flush();
                    $url = 'https://odh-order-api.teknisa.cloud/saas/';
                }
                
            }
            if(empty($token)) {
                // $origin = $evtEvent->getOriginApiExternal();
                $token = $evtEvent->gettokenApiExternal();
                $url = $evtEvent->geturlApiExternal();
                if (empty($token) || empty($url) || empty($origin)) {
                    // $origin = $genconfiguration->getOriginApiExternal();
                    $token = $genconfiguration->gettokenApiExternal();
                    $url = $genconfiguration->geturlApiExternal();
                } 
            }
        }
        $returno['URL'] = $url; 
        $returno['TOKEN'] = $token; 
        $returno['ORIGIN'] = $origin; 
        $returno['EXTERNAL_ID'] = $external_id;
        
        return $returno;
    }
    
    public function saveOrderExternalApi($row, $order, $token, $url, $origin,$external_id_Store, $userCpf=null) {
        try {
            // verificar se esse numero corresponde ao nrorg do country 12 03 2021
            // $nrorg = 1515;
            $nrorg = $row['NRORG'];
            $body = array();
            $payment = array();
            $address = array();
            $consumerDetail = array();

            $body['saleId'] = $order->getOrderIdentifier();//Identificador unico do pedido na plataforma de terceiros
            $body['orderNumber'] = $this->getLastNumbers($order->getOrderIdentifier());//Identificador resumido do pedido na plataforma de terceiros. (4 digitos)
            $body['orderType'] =  2;//Tipo do pedido delivery (1) ou retirada na loja (2)
            
            // $body['origin'] =  "API01";//usado desenvolvimento
            $body['origin'] = $origin;//usado em producao
            
            $body['date'] =  date("Y-m-d H:i:s"); //"2020-01-01 18:00:00"; 
            $body['scheduleDate'] =  "";
            $body['tableNumber'] = '';
            $body['total'] =  $order->getTotal();//60.0; 
            $body['troco'] = 0; //VERIFICAR NOME
            $subtotal =  $order->getTotal();
            $body['subTotal'] =  $subtotal; //58.0;  
           
            //ALTERAR ENTRE AS LINHAS PARA O TESTE DA BRANCH
            $body['subsidiaryCode'] = $external_id_Store;
            //$body['subsidiaryCode'] =  "0005";
            
            $body['tax'] = 0;//2.0; 
            $body['discount'] =  0;
            $body['latitude'] =  0.0; 
            $body['longitude'] =  0.0; 
            $body['cpfInNote'] =  $userCpf != null ? $userCpf : "";
            $body['notes'] = $order->getNote();
            $body['consumerDetail'] = array();
            $body['payments'] = array();
            $body['ItemOrder'] = array();
            //caso sejam vazios os seguintes itens podem ser ocultos do array
            // $body['ComboOrders'] = array();
            // $body['PizzaOrders'] = array();
            
            $userId = $order->getGenUser();
            $consumerDetail = $this->getUserByCpfNameOrIdUser($row);
            
            $address = array();
            $address['zipCode'] = null;
            $address['street'] = null;
            $address['number'] = null;
            $address['neighborhood'] = null;
            $address['city'] = null;
            $address['uf'] = null;
            $address['complement'] = null; 
            $address['referencePoint'] = null;  
            
            $paymentMethod = $order->getPaymentMethod();
          //  var_dump($paymentMethod);die;
           // var_dump($order->getCardBrand());die;
            // $paymentMethodData = $this->getEntityManager()->getRepository(PayPaymentMethod::class)->findOneBy(['paymentMethod' => $paymentMethod]);
            //SE ATENTAR AO ID DO METODO DE PAGAMENTO, SE ELE ESTA CADASTRADO E/OU VINCULADO NO RETAIL
            // $payment['paymentId'] = $paymentMethodData->getId();//"001"; 
            $payment['paymentId'] = $paymentMethod;//"001"; 
            $payment['value'] =  $order->getTotal();//60.0;
            
            $consumerDetail['address'] = $address;
             
            $body['consumerDetail'] = $consumerDetail;
            $body['payments'][0] = $payment;
            
            $orderArray = $order->toArray();
            $orderitens = $orderArray['items'];
            $arrayOrderItems = array();
            for ($index = 0; $index < count($orderitens) ; $index++) {
                $arrayItem = array();
                $item = $orderitens[$index];
                $itemTitle = $item['name'];
                $itemTitle = preg_replace('/[áàãâä]/ui', 'a', $itemTitle);
                $itemTitle = preg_replace('/[éèêë]/ui', 'e', $itemTitle);
                $itemTitle = preg_replace('/[íìîï]/ui', 'i', $itemTitle);
                $itemTitle = preg_replace('/[óòõôö]/ui', 'o', $itemTitle);
                $itemTitle = preg_replace('/[úùûü]/ui', 'u', $itemTitle);
                $itemTitle = preg_replace('/[ç]/ui', 'c', $itemTitle);
                //$itemTitle = preg_replace('/[^a-z0-9]/i', '', $itemTitle);
                
                $arrayItem['title']     =  strtoupper($itemTitle);//"MS CANECA CHOCOLATE";
                $arrayItem['quantity']  =  $item["quantity"]; 
                $arrayItem['price']     =  $item["total"]/$item["quantity"]; 
                $arrayItem['total']     =  $item["total"]; 
                $arrayItem['notes']     =  ''; 
                $arrayItem['ComplementCategories'] =  array();
               
                $extras = $item['extras'];
                
                $arrayItems = array();
                for($id = 0; $id < count($extras); $id++){
                    $ext = $extras[$id];
                    $option = $ext["selectedOptions"];
                    
                    $arrayOption = array();
                    for($ind = 0; $ind < count($option); $ind++){
                        $opt = $option[$ind];
                        
                        $opt['quantity'] = !isset($opt['quantity']) ? 1 : $opt['quantity'];
                        if (!isset($opt['price']) || isset($opt['price']) && $opt['price'] == 0){
                           $opt['price'] = 0.01; 
                        }
                        $arrayOption['title']    = $opt['name'];
                        $arrayOption['quantity'] = $opt['quantity'];
                        $arrayOption['priceUn']  = $opt['price'];
                        $arrayOption['total']    = $opt['price'] * $opt['quantity'];
                        $arrayItem['notes'] = $arrayItem['notes'].' '.$arrayOption['quantity'].'x '.$arrayOption['title'].'.';
                        $arrayOption['ref']      = isset($opt['integrationCode']) ? $opt['integrationCode'] : null;
                    }
                }
                try {
                    $ordMenuProd = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($item['menuProductId']);
                    $ordProd = $this->getEntityManager()->getRepository(OrdProduct::class)->find($ordMenuProd->getOrdProduct()->getId());
                    $ordProdArray = $ordProd->toArray();
                    $arrayItem['ref'] = $ordProdArray['externalId'];// "8110100100";  
                } catch (\Exception $errobuscaProd) {
                    $reponse = substr($errobuscaProd->getMessage(), 0, 255);
                }
                
                $body['ItemOrder'][$index] = $arrayItem;
            }
            // if(!empty($arrayOrderItems)){
            //     $body['ItemOrder'] = array_merge($body['ItemOrder'], $arrayOrderItems);
            // }
            $reponse =  self::makeCurlExternalApi($url, $body, $token,'index.php/order');
            // $responseSucces = false;
            try {
                $explodeResponse = explode('"success":',$reponse);
                if (isset($explodeResponse[1])) {
                    $responseSucces = explode(',',$explodeResponse[1])[0];
                } else $responseSucces = false;
            } catch (\Exception $j) {
                $responseSucces = substr ($reponse, 170, 425);
            }
            
            $dataBaseMessage = explode('"message":', $reponse)[1];       
            
            $order->setOAsuccess($responseSucces);
            $order->setOAReponse($dataBaseMessage);
            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush();
            //var_dump($responseSucces);die;
            return $responseSucces;
        } catch (\Exception $e) {
            $order->setOAsuccess('false');
            $order->setOAReponse(substr($e->getMessage(), 0, 255));
            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush();
        //var_dump($responseSucces);die;

            return $e->getMessage();
        }
    }
    
    function getUserByCpfNameOrIdUser($row){
        
        
        $cpf = isset($row['USER_CPF'])?$row['USER_CPF']:null;
        $name = isset($row['USER_NAME'])?$row['USER_NAME']:"user not found";
        $userId =isset($row['USER_ID'])?$row['USER_ID']:null;
        $consumerDetail = array();
        $flag = true;
        $user=null;
        // var_dump($row);die;
        if($cpf != null && $cpf!=" "){
            $user = $this->getEntityManager()->getRepository(GenUserApiGeneral::class)->findOneBy(['cpf' => $cpf, 'status' => 'A']);
            if($user==null){
            $consumerDetail['clientId'] = $cpf; 
            $consumerDetail['name']     =$cpf;
            $consumerDetail['email']    = $cpf; 
            $consumerDetail['cpf']      = $cpf; 
            $flag = false;
            }
           // var_dump($consumerDetail);die;
        }elseif($name != null && $name !=" "){
            $consumerDetail['name'] =  $name;
         //   $flag = false;
          //  var_dump($name);die;
        }
        //var_dump($user);die;
        if(!$user ||$user==null  ){
            $user = $this->getEntityManager()->getRepository(GenUserApiGeneral::class)->find($userId);
            // var_dump($user);die;
        }
            // var_dump($user);die;

    if($user &&$user!=null && $user!='null' && $user->getId() && $flag){
            $consumerDetail['clientId'] =  $user->getId(); //"123456789"; 
            $consumerDetail['name'] = $user->getFirstName() . ' ' . $user->getLastName(); //"Cliente"; 
            $consumerDetail['email'] =  $user->getEmail();//"cliente.teste@gmail.com"; 
            $consumerDetail['cpf'] = $cpf != null && $cpf!=" "? $cpf:$user->getCpf();//""; 
           
            $userContact = $this->getEntityManager()->getRepository(GenContactApiGeneral::class)->findOneBy(['genUser' => $userId]);
            if ($userContact != NULL) {
                $phone =  $userContact->getPhone();
                $phone = str_replace(')','',str_replace('(','',str_replace(' ','',$phone)));
                $consumerDetail['phone']        =  $phone;//"1133332222"; 
                $consumerDetail['cellPhone']    =  $phone;//"11999999999"; 
            }else {
                $consumerDetail['phone']        =  null; 
                $consumerDetail['cellPhone']    =  null;
            }
        }else{
            $consumerDetail['clientId'] = $userId; 
            $consumerDetail['name']     = $name;
            $consumerDetail['email']    = $cpf; 
            $consumerDetail['cpf']      = $cpf; 
            $consumerDetail['phone']    =  null; 
            $consumerDetail['cellPhone']=  null;
        }
      // var_dump($consumerDetail);die;
        return $consumerDetail;
    }
    

    private function makeCurlExternalApi($url, $body, $token, $prefix) {
        $url = $url.$prefix;
        $ch = curl_init(); 
        
        // Set the url
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER  , true);  // we want headers
        $authorization = "Authorization: bearer ".$token; // Prepare the authorisation token
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); // Inject the token into the header
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // Closing
        curl_close($ch);
        
        return $result;
    }
    
    public function updateNote($order, $value){
        $order->setStoneNote($value);
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
    }
    
    public function updatePosStatus($order, $params){
        if($order){
        $order->setPosStatus($params['status']);
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
        }
    }
    
    /*obter os 3 ultimos numeros*/
    function getLastNumbers($numer) {
        return substr($numer,(strlen($numer)-3),strlen($numer));
    }
}