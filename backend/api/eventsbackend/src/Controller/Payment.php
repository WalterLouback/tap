<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiPayment\Controller\Payment as PayPayment;
use Zeedhi\ApiEvents\Service\Environment;


class Payment{

    public function __construct(PayPayment $payment) {
        $this->payment = $payment;
    }

    public function execPaymentCC($merchantKey, $merchantId, $gateway, $order) {
        $payment = $this->payment->execPaymentCreditCard($merchantKey, $merchantId, $gateway, $order);
        return $payment;
    }
    
    public function execRevokeCC($transactionId, $value, $gateway, $merchantKey, $merchantId) {
        $payment = $this->payment->revoke($transactionId, $value, $gateway, $merchantKey, $merchantId);
        return $payment;
    }

}