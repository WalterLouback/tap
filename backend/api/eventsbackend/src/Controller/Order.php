<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Order as OrderService;
use Zeedhi\ApiEvents\Service\Exception as Exception;

use Zeedhi\ApiEvents\Model\Entities\OrdOption;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdCashier;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderRO;
use Zeedhi\ApiEvents\Model\Entities\OrdItemExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiEvents\Model\Entities\GenStatus;
use Zeedhi\ApiEvents\Model\Entities\GenOrganization;
use Zeedhi\ApiEvents\Model\Entities\GenStructure;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Doctrine\ORM\EntityManager;

use Zeedhi\ApiEvents\Helpers\Util;

class Order {
    
    /** @var orderService */
    protected $orderService;
    
    const USER_ID           = 'USER_ID';
    const ORDER_ID           = 'ORDER_ID';
    const ORDER_ITEMS        = 'ORDER_ITEMS';
    const ORDER_STATUS       = 'ORDER_STATUS';
    
    /**
     * __contruct
     *
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService, EntityManager $entityManager) {
        $this->orderService  = $orderService;
        $this->entityManager = $entityManager;
    }
    
    public function createOrderEventWithWalletAndEventSeller(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $orderItems       = $row[OrdOrder::ORDER_ITEMS];
        $orderType        = $row[OrdOrder::ORDER_TYPE];
        $totalValue       = $row[OrdOrder::TOTAL_VALUE];
        $evtEventSellerId = isset($row[OrdOrder::EVT_EVENT_SELLER_ID]) ? $row[OrdOrder::EVT_EVENT_SELLER_ID] : NULL;
        $eventId          = $row[EvtEvent::EVENT_ID];
        $nrorg            = $row[GenOrganization::NRORG];
        $paymentMethod    = $row[OrdOrder::PAYMENT_METHOD];
        $creditCardId     = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $walletId         = isset($row[OrdOrder::WALLET_ID]) ? $row[OrdOrder::WALLET_ID] :  NULL;
        $walletCardId     = isset($row['WALLET_CARD']) ? $row['WALLET_CARD'] :  NULL;
        $userId           = isset($row[OrdOrder::COSTUMER_ID]) ? $row[OrdOrder::COSTUMER_ID] : NULL;
        $statusId         = GenStatus::PEDIDO_ENTREGUE;
        
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderEventWithWalletAndEventSeller($orderItems, $totalValue, $evtEventSellerId, $eventId, $nrorg, $paymentMethod, $creditCardId, $walletId, $walletCardId, $userId, $statusId, $orderType, false);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true, false);
        /* Making financial transaction */ 
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, $walletId);
        /* Sending response */
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
    
    /**
     * Apagar a função e usar a nova função createOrderEventWithWalletAndEventSeller
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function finishOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[OrdOrder::ORDER_ITEMS];
        $totalValue       = $row[OrdOrder::TOTAL_VALUE];
        $userId           = $row[GenUser::USER_ID];
        $storeId          = $row[EvtEvent::EVENT_ID];
        $nrorg            = $row[GenOrganization::NRORG];
        $paymentMethod    = $row[OrdOrder::PAYMENT_METHOD];
        $creditCardId     = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        $paymentStatus    = 'P';
        $fromTaa          = $row[OrdOrder::ORDER_FROM_TAA];
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, $userId, NULL, $storeId, $nrorg, $creditCardId, NULL, NULL, NULL, $paymentStatus, NULL, NULL, false,$fromTaa);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, false, true);
        /* Making financial transaction */ 
        // $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, null);
        /* Sending response */
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
    
    /**
     * Função utilizada para vendas realizadas em máquinas POS
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function finishOrderPOS(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Getting the request's parameters */
        $orderItems         = $row[OrdOrder::ORDER_ITEMS];
        $totalValue         = $row[OrdOrder::TOTAL_VALUE];
        $sellerId           = $row[GenUser::USER_ID];
        $storeId            = $row[EvtEvent::EVENT_ID];
        $nrorg              = $row[GenOrganization::NRORG];
        $paymentMethod      = $row[OrdOrder::PAYMENT_METHOD];
        $paymentStatus      = 'A';
        $cashierId          = isset($row['CASHIER_ID']) ? $row['CASHIER_ID'] : NULL;
        $evtEventSellerId   = isset($row['SELLER_USER_ID']) ? $row['SELLER_USER_ID'] : NULL;
        $creditCardId       = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $creationDate       = isset($row['ORDER_TIME']) ? $row['ORDER_TIME'] : NULL;
        $transactionDetails = isset($row['TRANSACTION_DETAILS']) ? $row['TRANSACTION_DETAILS'] : NULL;
        
        $status             = GenStatus::PEDIDO_NOVO;
        
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, NULL, $sellerId, $storeId, $nrorg, $creditCardId, $cashierId, $creationDate, $transactionDetails, $paymentStatus, NULL, NULL, false);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true, true);
        /* Making financial transaction */ 
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, null);
        /* Calling PDV Integration */
        $order = $this->orderService->getOrderById($order->getId());
        
        $this->orderService->makePdvIntegration($order);
        
        /* Sending response */
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
     
     /**
     * getOrdersFromUser
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [GenUser::USER_ID];
        if (!$this->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $userId        = $row[GenUser::USER_ID];
        $nrorg         = isset($row[GenOrganization::NRORG])     ? $row[GenOrganization::NRORG] : NULL;
        $structureId   = isset($row[GenStructure::STRUCTURE_ID]) ? $row[GenStructure::STRUCTURE_ID] : NULL;
        $initial       = isset($row['FIRST_RESULT'])             ? $row['FIRST_RESULT']   : NULL;
        $max           = isset($row['MAX_RESULTS'])              ? $row['MAX_RESULTS']   : NULL;
        
        /* Get orders */
        $orders      = $this->orderService->getOrdersFromUser($userId, $structureId, $nrorg, $initial, $max);
        
        $response->addDataSet(new DataSet('orders', OrdOrder::manyToArray($orders)));
    }

    public function checkoutOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        $orderId   = isset($row[OrdOrder::ORDER_ID]) ? $row[OrdOrder::ORDER_ID] : NULL;
        $itemId    = isset($row['ITEM_IDENTIFIER']) ? $row['ITEM_IDENTIFIER'] : NULL;
        $cashierId = isset($row['CASHIER_ID']) && !empty($row['CASHIER_ID']) ? $row['CASHIER_ID'] : null;
        $eventId   = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        
        if ($orderId) {
            $order = $this->orderService->getOrderById($orderId);
            $event = $order->getEvtEvent();
            
            if ($event != NULL && $eventId != $event->getId())
                throw new \Exception('Invalid order', 1);
            if ($order->getStatus()->getId() == GenStatus::PEDIDO_ENTREGUE || $order->getStatus()->getNext() == NULL)
                throw new \Exception('Order delivered', 2);
            
            if ($order->getPaymentStatus() !== 'A')
                $this->orderService->makePaymentTransaction($order, 'BALANCE', NULL, $order->getNrorg(), null);
            
            $order->setStatus($this->entityManager->getReference(GenStatus::class, GenStatus::PEDIDO_ENTREGUE));
            if ($cashierId) {
                $order->setOrdCashier($this->entityManager->getReference(OrdCashier::class, $cashierId));
            }
            $this->orderService->updateAll();    
            $orderProducts = $this->orderService->getOrderProductByOrder($order->getId());
            $products      = $this->factoryOrderProducts($orderProducts);
            $orderFactory  = $order->toArray();
            $response->addDataSet(new DataSet('order', $orderFactory));
        } else if ($itemId) {
            $item = $this->orderService->getOrderProductByIdentifier($itemId);
            
            if ($item == null)
                throw new \Exception('Order not found', 1);
            if ($item->getStatus() != 'P')
                throw new \Exception('Order delivered', 2);
            if ($item->getOrdOrder()->getPaymentStatus() !== 'A')
                throw new \Exception('Order not paid');
                
            if ($cashierId) {
                $item->getOrdOrder()->setOrdCashier($this->entityManager->getReference(OrdCashier::class, $cashierId));
            }
            $item->setStatus('A');
            $this->orderService->updateAll();
            
            $allItemsAreDelivered = true;
            $order = $item->getOrdOrder();
            $order->setItems($this->entityManager);
            
            foreach ($order->getItems() as $items) {
                $allItemsAreDelivered &= $item->getStatus() == 'A';
            }
            if ($allItemsAreDelivered) {
                $order->setStatus($this->entityManager->getReference(GenStatus::class, GenStatus::PEDIDO_ENTREGUE));
            $this->orderService->updateAll();
            }
            
            $response->addDataSet(new DataSet('orderProduct', $item->toArray()));
        } else {
            throw new \Exception('Invalid request data', 1);   
        }
    }
    
    public function getOrdersWithItems(DTO\Request\Row $request, DTO\Response $response){
        $row           = $request->getRow();

        $userId        = $row[OrdOrder::USER_ID];
        $nrorg         = $row[OrdOrder::NRORG];
        $paymentMethod = isset($row[OrdOrder::PAYMENT_METHOD]) ? $row[OrdOrder::PAYMENT_METHOD] : NULL;
        $eventId       = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $initialDate   = isset($row[OrdOrder::INITIAL_DATE]) ? $row[OrdOrder::INITIAL_DATE] : NULL;
        $finalDate     = isset($row[OrdOrder::FINAL_DATE]) ? $row[OrdOrder::FINAL_DATE] : NULL;
        
        $orders        = $this->orderService->getOrdersWithItems($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet('orders', $orders));
    }
    
    public function factoryOrderProducts($orderProducts){
        foreach($orderProducts as $key=>$orderProduct){
            $product[$key]['id'] = $orderProduct->getOrdMenuProduct()->getId();
            $product[$key]['name'] = $orderProduct->getOrdMenuProduct()->getName();
            $product[$key]['price'] = $orderProduct->getOrdMenuProduct()->getPrice();
            $product[$key]['detail'] = $orderProduct->getOrdMenuProduct()->getDetail();
            $product[$key]['category'] = $orderProduct->getOrdMenuProduct()->getOrdProductGroup()->getName();
        }
        return $product;
    }
    
    /**
      * checkParameters
      * 
      * @param DTO\Request\Row $row
      * @param Array<String>   $params
      * 
      */
    public static function checkParameters($row, $params) {
        foreach($params as $param) {
            if (!isset($row[$param])) 
                return false;
        }
        return true;
    }
    
    function getEventsReport($request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
       
        /* Get the parameters from the request */
        $userId         = $row['USER_ID'];
        $nrorg          = isset($row['NRORG'])          ? $row['NRORG'] : NULL;
        $structureId    = isset($row['STRUCTURE_ID']) && $row['STRUCTURE_ID'] != '' && count($row['STRUCTURE_ID']) > 0  ? $row['STRUCTURE_ID'] : NULL;
        $storeId        = isset($row['STORE_ID']) && $row['STORE_ID'] != '' && count($row['STORE_ID']) > 0 ? $row['STORE_ID'] : NULL;
        $max            = isset($row['MAX_RESULTS'])    ? $row['MAX_RESULTS']   : NULL;
        $initialDate    = isset($row['INITIAL_DATE']) && $row['INITIAL_DATE'] != ''  ? $row['INITIAL_DATE']     : NULL;
        $finalDate      = isset($row['FINAL_DATE']) && $row['FINAL_DATE'] != '' ? $row['FINAL_DATE']   : NULL;
        $orderStatus    = isset($row['ORDER_STATUS']) && $row['ORDER_STATUS'] != '' && count($row['ORDER_STATUS']) > 0 ? $row['ORDER_STATUS']   : NULL;
        $deliverTo      = isset($row['DELIVER_TO'])  && $row['DELIVER_TO'] != '' ? $row['DELIVER_TO']   : NULL;
        $paymentStatus  = isset($row['PAYMENT_STATUS']) && $row['PAYMENT_STATUS'] != '' && count($row['PAYMENT_STATUS']) > 0 ? $row['PAYMENT_STATUS']   : NULL;
        $paymentMethods = isset($row['PAYMENT_METHOD'])  && $row['PAYMENT_METHOD'] != '' ? $row['PAYMENT_METHOD']   : NULL;
        $deliveryAddress      = isset($row['DELIVERY_ADDRESS'])  && $row['DELIVERY_ADDRESS'] != '' ? $row['DELIVERY_ADDRESS']   : NULL;
        $statusIds      = NULL;
        $fromTaa  = isset($row['ORDER_FROM_TAA']) && $row['ORDER_FROM_TAA'] != '' && count($row['ORDER_FROM_TAA']) > 0 ? $row['PAYMENT_STATUS']   : NULL;

        /* Get orders */
        $orders         = $this->orderService->getEventsReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods, $deliveryAddress, $fromTaa);
        $orderFactory   = OrdOrderRO::manyToArrayReport($orders);
        
        $dataSourceName = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? 'getEventsReport' : 'orders';
        $response->addDataSet(new DataSet($dataSourceName, $orderFactory));
    }
    
    public function getOrdersWithItemsFromWallet(DTO\Request\Row $request, DTO\Response $response){
        $row           = $request->getRow();

        $nrorg         = $row[OrdOrder::NRORG];
        $paymentMethod = isset($row[OrdOrder::PAYMENT_METHOD]) ? $row[OrdOrder::PAYMENT_METHOD] : NULL;
        $eventId       = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $initialDate   = isset($row[OrdOrder::INITIAL_DATE]) ? $row[OrdOrder::INITIAL_DATE] : NULL;
        $finalDate     = isset($row[OrdOrder::FINAL_DATE]) ? $row[OrdOrder::FINAL_DATE] : NULL;
        $walletId      = $row[OrdOrder::WALLET_ID];
        $walletCardId  = isset($row['WALLET_CARD']) ? $row['WALLET_CARD'] : NULL;
        
        $orders        = $this->orderService->getOrdersWithItemsFromWallet($paymentMethod, $nrorg, $eventId, $initialDate, $finalDate, $walletId, $walletCardId);
        
        $response->addDataSet(new DataSet('orders', $orders));
    }
    
    function createPendentOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        /* Getting the request's parameters */
        $orderItems         = $row[OrdOrder::ORDER_ITEMS];
        $totalValue         = isset($row[OrdOrder::TOTAL_VALUE]) ? $row[OrdOrder::TOTAL_VALUE] : $row[''];
        $storeId            = isset($row[EvtEvent::EVENT_ID]) ? $row[EvtEvent::EVENT_ID] : $row['STORE_ID'];
        $nrorg              = $row[GenOrganization::NRORG];
        $paymentMethod      = $row[OrdOrder::PAYMENT_METHOD];
        $userId             = $row[GenUser::USER_ID];
        $paymentStatus      = 'P';
        $orderType          = isset($row['ORDER_TYPE']) ? $row['ORDER_TYPE'] : NULL;
        $walletId           = isset($row['WALLET_ID']) ? $row['WALLET_ID'] : NULL;
        $cashierId          = isset($row['CASHIER_ID']) ? $row['CASHIER_ID'] : NULL;
        $evtEventSellerId   = isset($row['SELLER_USER_ID']) ? $row['SELLER_USER_ID'] : NULL;
        $creditCardId       = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $creationDate       = isset($row['ORDER_TIME']) ? $row['ORDER_TIME'] : NULL;
        $transactionDetails = isset($row['TRANSACTION_DETAILS']) ? $row['TRANSACTION_DETAILS'] : NULL;
        $transactionId      = isset($row['TRANSACTION_ID']) ? $row['TRANSACTION_ID'] : NULL; 
        $stoneNote          = isset($row['STONE_NOTE']) ? $row['STONE_NOTE'] : NULL; 
        $status             = isset($row['STATUS']) ? $row['STATUS'] : GenStatus::PEDIDO_NOVO;
        $convenienceFee     = isset($row['CONVENIENCE_FEE']) ? $row['CONVENIENCE_FEE'] : NULL;
        $barCode            = isset($row['BAR_CODE']) ? $row['BAR_CODE'] : NULL;
        $fromTaa            = isset($row['ORDER_FROM_TAA']) ? $row['ORDER_FROM_TAA'] : NULL;
        if (!$barCode){
            /* Instantiating the order without flushing */
            $row = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, $userId, $evtEventSellerId, $storeId, $nrorg, $creditCardId, $cashierId, $creationDate, $transactionDetails, $paymentStatus, $orderType, $walletId, false, $transactionId, $stoneNote, $convenienceFee, $barCode, $fromTaa);
            /* Instantiating the product orders and flushing */
            $this->orderService->createProductOrders($row, $orderItems, false, true);
        }
        else {
            /* Instantiating the order and flushing */
            $row = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, $userId, $evtEventSellerId, $storeId, $nrorg, $creditCardId, $cashierId, $creationDate, $transactionDetails, $paymentStatus, $orderType, $walletId, true, $transactionId, $stoneNote, $convenienceFee, $barCode, $fromTaa);
            // $this->orderService->createProductOrders($row, $orderItems, false, true);
        }
        $order = $this->orderService->getOrderById($row->getId());
        
        /* Sending response */
        $response->addDataSet(new DataSet('order', $row->toArray()));
    }
    
    public function picpayPaymentCallback($request, $response) {
        $row = $request->getRow();

        $referenceId  = $row['REFERENCE_ID'];
        $xPicpayToken = $row['X_PICPAY_TOKEN'];
        $xSellerToken = $row['X_SELLER_TOKEN'];
        
        // $this->makeCurl("https://appws.picpay.com/ecommerce/public/callback", "POST", $xPicpayToken, $xSellerToken);
        $response = $this->makeCurl("https://appws.picpay.com/ecommerce/public/payments/$referenceId/status", "GET", $xPicpayToken, $xSellerToken);
        
        if (!isset($response['status'])) throw new \Exception('Order not found');
        
        $order = $this->orderService->getOrderById($referenceId);
        
        if ($order->getPaymentStatus() !== 'A' && $response['status'] == 'paid') {
            $this->orderService->makePaymentTransaction($order, 'PICPAY', NULL, $order->getNrorg(), null);
            /* Se for compra de saldo, creditar o saldo do usuário */
            if ($order->getType() === 'WALLET') {
                $wallet = $order->getPayWallet();
                $walletId = $wallet !== NULL ? $wallet->getId() : NULL;
                
                $eventId   = $order->getEvtEvent() !== NULL ? $order->getEvtEvent()->getId() : NULL;
                $cashierId = $order->getOrdCashier() !== NULL ? $order->getOrdCashier()->getId() : NULL;
                $this->orderService->addBalanceToWallet($walletId, $order->getTotal());
            }
        }
    }
    public function updateStoneReferenceId($request, $response) {
    $row                = $request->getRow();
    $orderId            = $row['ORDER_ID'];
    $preTransactionId   = $row['pre_transaction_id'];
    $order = $this->orderService->getOrderById($orderId);
    $this->orderService->updateStoneTransactionId($order,$preTransactionId);
    $response->addDataSet(new DataSet('order', $order->toArray()));
    }
    
    public function updateStonePaymentStatus($request, $response) {
        $row                = $request->getRow();
        $segmented          = isset($row['SEGMENTED']) ? $row['SEGMENTED'] : null;
        $nrorg              = $row['NRORG'];
        $orderId            = isset($row['ORDER_ID'])? $row['ORDER_ID'] : null;
        $storeId            = $row['STORE_ID'];
        $credit_card_brand  = isset($row['CREDIT_CARD_BRAND']) ? $row['CREDIT_CARD_BRAND'] : null; 
        $card_holder_name   = isset($row['CARD_HOLDER_NAME']) ? $row['CARD_HOLDER_NAME'] : null; 
        $credit_card_numbers = isset($row['CREDIT_CARD_NUMBERS']) ? $row['CREDIT_CARD_NUMBERS'] : null; 
        $api_external_url   = isset($row['API_EXTERNAL_URL']) ? $row['API_EXTERNAL_URL'] : null;
        $api_request        = isset($row['API_REQUEST']) ? $row['API_REQUEST'] : null;
        $userCpf            = isset($row['USER_CPF']) ? $row['USER_CPF'] : null;
        $api_barcode        = isset($row['API_BARCODE']) ? $row['API_BARCODE'] : null;

        if($api_external_url) {
            $order = $this->orderService->getOrderById($orderId);
            $this->orderService->updateStoneTransaction($order, $credit_card_brand, $card_holder_name, $credit_card_numbers);
            $response_api = $this->orderService->closeTicket($api_external_url, $api_request);
            $response->addDataSet(new DataSet('response', $response_api));
        } 
        else {
            $order = $this->orderService->getOrderById($orderId);
            if ($order->getPaymentStatus() !== 'A' && false ) {
                $this->orderService->updateStoneTransaction($order, $credit_card_brand, $card_holder_name, $credit_card_numbers, $api_external_url, $api_barcode);
                /* Se for compra de saldo, creditar o saldo do usuário */
            $this->orderService->updateProduct($order);
            
            $dataTokenUrl = $this->orderService->verifyTokenAndUrl($nrorg,$storeId);
            if (!empty($dataTokenUrl['TOKEN']) && !empty($dataTokenUrl['URL']) && !empty($dataTokenUrl['ORIGIN']) && !empty($dataTokenUrl['EXTERNAL_ID'])) {
                $apiOrder = $this->orderService->saveOrderExternalApi($row,$order,$dataTokenUrl['TOKEN'],$dataTokenUrl['URL'],$dataTokenUrl['ORIGIN'], $dataTokenUrl['EXTERNAL_ID'], $userCpf);
            }            
            }
            
            $response->addDataSet(new DataSet('order', $order->toArray()));
        }
    }
     public function isIntegration ($request, $response){

        $row                = $request->getRow();
        $nrorg              = $row['NRORG'];
        $storeId            = $row['STORE_ID'];

         

         $dataTokenUrl = $this->orderService->verifyTokenAndUrl($nrorg,$storeId);
            if (!empty($dataTokenUrl['TOKEN']) && !empty($dataTokenUrl['URL']) && !empty($dataTokenUrl['ORIGIN']) && !empty($dataTokenUrl['EXTERNAL_ID'])) {
                $response->addDataSet(new DataSet('integration', array("true")));
            }
            else $response->addDataSet(new DataSet('integration', array("false")));
     }
    public function makeCurl($url, $method, $xPicpayToken, $xSellerToken) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "x-picpay-token: $xPicpayToken",
            "x-seller-token: $xSellerToken"
        ));
        // Will be a POST
        if ($method === "POST") curl_setopt($ch, CURLOPT_POST, 1);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        return json_decode($result, true);
    }
    /*metodo atualizado porque stone disse que este retornava json e html*/
    public function siclosTrasactionEndpoint(DTO\Request $request, DTO\Response $response){
        $req = $request->getParameters();

        if(array_key_exists('test', $req) && $req['test']){
            //   $order = $this->orderService->getOrderByTransactionId($req['pre_transaction_id']);
                 //   var_dump($this->getStoneToken()['token']);die;
          //  $transactionData=$this->getStoneToken($req['pre_transaction_id']);
           // $transactionData = $this->getTransactionData($token,$req['pre_transaction_id']);
          // var_dump($order->toArray()['eventId']);die;
          //$transactionData=$transactionData['transaction'];
            // var_dump($transactionData);die;
            echo '{"success":true}';
        }else if(array_key_exists('stone_transaction_id', $req)){
          $order = $this->orderService->getOrderByTransactionId($req['pre_transaction_id']);
         
            if($order) {
                 $note = str_replace("@@INSERT@@NUMBER@@HERE@@", $this->getLastNumbers($order->getOrderIdentifier()), $order->getStoneNote());
              if($note != null && $note != "") {
                echo '{"success":true, "note": '.$note.'}';
                $this->orderService->updateNote($order, "");
              }else {
                echo '{"success":false}';
              }
            $transactionData=$this->getStoneToken($req['pre_transaction_id']);
            $transactionData=$transactionData['transaction'];
              
                if ($order->getPaymentStatus() !== 'A') {
              // var_dump($order->getGenUser()->getFirstName());die;


               $userCpf=json_decode($note)->client->document_number;
             // var_dump( $userCpf);die;
            // $transactionData['card_brand']="test";
            // $transactionData['card_holder_name']="test";
            // $transactionData['card_number']="test";
         //   $paymentType =$transactionData['payment_type']==1?474:475;
            $credit_card_brand=($transactionData['card_brand']) ?$transactionData['card_brand'] : null;
            $card_holder_name=($transactionData['card_holder_name']) &&$transactionData['card_holder_name'] != "Não possui" ?$transactionData['card_holder_name'] : null;
            $credit_card_numbers=($transactionData['card_number']) ?$transactionData['card_number'] : null;
            $paymentType=($transactionData['payment_type']);
            $nrorg =$order->toArray()['nrorg'];
            $storeId=$order->toArray()['eventId'];

            $this->orderService->updateStoneTransaction($order, $credit_card_brand, $card_holder_name, $credit_card_numbers, $paymentType);
                /* Se for compra de saldo, creditar o saldo do usuário */
           // }
            // var_dump(preg_replace('/[^0-9]/', '', $userCpf));
           // $this->orderService->updateProduct($order);//
            // var_dump();die;
            $row['USER_ID'] =$userCpf!=" "?preg_replace('/[^0-9]/', '', $userCpf):$order->getGenUser()->getId();
            $row['USER_NAME']  = $userCpf!=""?$userCpf:$order->getGenUser()->getFirstName() . ' ' .$order->getGenUser()->getLastName();
            $row['USER_CPF']      = $userCpf; 
            $row['NRORG']      = $nrorg;
            
            $dataTokenUrl = $this->orderService->verifyTokenAndUrl($nrorg,$storeId);
            if (!empty($dataTokenUrl['TOKEN']) && !empty($dataTokenUrl['URL']) && !empty($dataTokenUrl['ORIGIN']) && !empty($dataTokenUrl['EXTERNAL_ID'])) {
            $apiOrder = $this->orderService->saveOrderExternalApi($row,$order,$dataTokenUrl['TOKEN'],$dataTokenUrl['URL'],$dataTokenUrl['ORIGIN'], $dataTokenUrl['EXTERNAL_ID'], $userCpf);
            }            
            }
          
           // $response->addDataSet(new DataSet('order', $order->toArray()));
            
            //
            
        }else {
                echo '{"success":falseb}';
            }
        }
        die;
    }
     public function getTransactionData($token, $transactionId){
         $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer $token"
        ));
        // Will be a POST
        // Set the url
        curl_setopt($ch, CURLOPT_URL, "https://api.siclospag.com.br/connect-split/v1/transactions/single/pre-transaction/$transactionId");
        //  if ($body) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        return json_decode($result, true);
    }
      public function getStoneToken($transactionId){
         $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Picpay headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization:Bearer MTY3YTQ5YWQtZDg5Zi00M2I5LWIwZTAtZGI4OTQ2MWEzOGNk"
        ));
        // Will be a POST
        // Set the url
        curl_setopt($ch, CURLOPT_URL, "https://api.siclospag.com.br/connect-split/v1/token");
        //  if ($body) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
      //  return json_decode($result, true)['token'];die;
        return $this->getTransactionData(json_decode($result, true)['token'],$transactionId);
    }
        
        
    
     public function posAvailable(DTO\Request $request, DTO\Response $response){
        $req = $request->getParameters();
            $order = $this->orderService->getOrderByTransactionId($req['row']['pre_transaction_id']);
            if($order!=null)
             $response->addDataSet(new DataSet('status', array("value"=>$order->getPosStatus())));
        }
        
     public function  updatePosStatus(DTO\Request $request, DTO\Response $response){
        $req = $request->getParameters();
       // var_dump($req['row']['pre_transaction_id']);
       if(array_key_exists('test', $req) && $req['test'] && array_key_exists('stone_transaction_id', $req)){
            //echo '{"success":true}';
          //  $order = $this->orderService->getOrderByTransactionId($req['pre_transaction_id']);
        //    $this->orderService->updatePosStatus($order,$req);
            echo '{"success":true}';
            $msg="requisição de teste";
        }else if(array_key_exists('pre_transaction_id', $req)){
            $order = $this->orderService->getOrderByTransactionId($req['pre_transaction_id']);
           $msg= $this->orderService->updatePosStatus($order,$req);
            //echo '{"success":true}';
        }else{
            $msg="pre_transaction_id não informado";
            echo '{"success":false}';
            
        }
            $date = date( 'Y-m-d H:i:s');
            $file = dirname(__FILE__).'/../../../../logs/'.date( 'Y-m-d' ).'.txt';
            $msg = sprintf( "[%s] : %s%s", $date, $msg, PHP_EOL );

            file_put_contents( $file, $msg, FILE_APPEND );
            die;
        }
    
    
    
    public function siclosTrasactionEndpointTest(DTO\Request $request, DTO\Response $response){
        $req = $request->getParameters();
        
        if(array_key_exists('test', $req) && $req['test']){
            echo '{"success":trrue}';
            die;
        }
        else if(array_key_exists('stone_transaction_id', $req)){
            $order = $this->orderService->getOrderByTransactionId($req['pre_transaction_id']);
            $note = ($order) ? $order->getStoneNote() : "";
            $note = str_replace("@@INSERT@@NUMBER@@HERE@@", $this->getLastNumbers($order->getOrderIdentifier()), $note);
            echo '{"success":true,
                "note": '.$note.'
                }';
            die;
        }
    }
    
    /*obter os 3 ultimos numeros*/
    function getLastNumbers($numer) {
        return substr($numer,(strlen($numer)-3),strlen($numer));
    }
    
}