<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\EventTicket as EventTicketService;
use Zeedhi\ApiEvents\Service\CreditCard as CreditCardService;
use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;

use Zeedhi\ApiEvents\Controller\Payment;

use Zeedhi\ApiEvents\Service\SMTP;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Helpers\Environment;
use Zeedhi\ApiEvents\Helpers\Integration;
use Zeedhi\ApiEvents\Helpers\Util;

use Zeedhi\ApiEvents\Model\Entities\EvtEventTicket;
use Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel;

class EventTicket extends Crud {
    
    const NRORG           = 'NRORG';
    const USER_ID         = 'USER_ID';
    const USER_TYPE_ID    = 'USER_TYPE_ID';
    const EVENT_ID        = 'EVENT_ID';
    const EVENT_TICKET_ID = 'EVENT_TICKET_ID';
    const PAYMENT_METHOD  = 'PAYMENT_METHOD';

    /** @var eventTicket */
    protected $eventTicketService;
    /** @var smptp */
    protected $smtpService;

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param Service $eventTicketService
     */
    public function __construct(Manager $dataSourceManager, EventTicketService $eventTicketService, Payment $payment, CreditCardService $creditCard, SMTP $smtpService, Integration $integration) {
        parent::__construct($dataSourceManager);
        $this->eventTicketService = $eventTicketService;
        $this->payment = $payment;
        $this->creditCardService = $creditCard;
        $this->smtpService = $smtpService;
        $this->integration = $integration;
    }

    public function createOrderTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        $total  = 0;
        $dev    = true;
        
        $verifyCard = $this->creditCardService->verifyCardownerShip($row['USER_ID'], $row['CREDIT_CARD_ID'], $row['NRORG']);
        
        if($verifyCard['cardOwnership'] == 0) {
            throw new \Exception('Invalid credit card', 1);
        }
        
        $ticketsAmount = [];
        foreach ($row['ORDER_ITEMS'] as $item) {
            $eventTicket = $this->eventTicketService->getEventTicket($item['EVENT_TICKET_ID']);
            
            if (isset($ticketsAmount[$eventTicket->getId()])) $ticketsAmount[$eventTicket->getId()] += count($item['USERS']);
            else $ticketsAmount[$eventTicket->getId()] = count($item['USERS']);
            
            if ($eventTicket->getAmount() < $ticketsAmount[$eventTicket->getId()]) {
                $ticketName = $eventTicket->getName();
                throw new \Exception("O ingresso $ticketName não possui quantidade suficiente para sua compra.");
            }

            $total += $eventTicket->getPrice();
            /*status NULL, caso tenha integracao status vai ser "P" para order criada.*/
            $status = ($eventTicket->getCodeTypeTicket() != null) ? "P" : null;
        }
        
        $this->eventTicketService->verifyIfCanPurchase($row['USER_ID'], $row['PAYMENT_METHOD'], $row['CREDIT_CARD_ID'], $total, $row['NRORG']);
        
        $order = $this->eventTicketService->createOrderTicket($row['CREDIT_CARD_ID'], $row['EVENT_ID'], $row['USER_ID'], $row['ORDER_ITEMS'], $status);
        
        $merchantKey = $this->eventTicketService->getMerchantKey($row['NRORG']);
        
        if(empty($merchantKey[0]['merchantKey']) && empty($merchantKey[0]['merchantId'])) {
            throw new \Exception('Organization not has merchantId or merchantKey', 105315);
        }
        
        if ($order->getTotal() > 0) {
            $paymentResponse = $this->createPaymentRequest($order, $merchantKey[0]['merchantKey'], $merchantKey[0]['merchantId']);
        } else $paymentResponse = ['status' => 'captured', 'transaction_id' => ''];
        
        $order->setTransactionId($paymentResponse['transaction_id']);
        $this->eventTicketService->updateAll();
        
        $paymentStatus = $paymentResponse['status'] == 'captured' ? "A": 'N';
        
        if ($paymentResponse['status'] == 'captured' && $status == "P") {
            $user           = $this->eventTicketService->getGenUser($row['USER_ID']);
            $evtEvet        = $this->eventTicketService->getEventById($row['EVENT_ID']);
            $arrayTicket    = array();
            $ticketPurchaseReceipt = array();
            
            foreach ($row['ORDER_ITEMS'] as $items) {
                $orderValue = $this->eventTicketService->getEventTicket($items['EVENT_TICKET_ID']);
                try {
                    foreach($items["USERS"] as $u) {
                        if($u["ID"] != null){
                            $userTypeRel = $this->eventTicketService->getGenUserTypeRel($u["ID"]);
                            array_push($arrayTicket, ["cod_tipo_ingresso" => $orderValue->getCodeTypeTicket(), "cod_associado" => $userTypeRel->getExternalId()]);
                        } else {
                            array_push($arrayTicket, [
                                "cod_tipo_ingresso" => $orderValue->getCodeTypeTicket(), 
                                "cod_associado" => "",
                                "nome" => $u["NAME"],
                                "idade" => $u["AGE"],
                                "identidade" => $u["RG"]
                                ]);
                        }
                    }
                } catch (\Exception $e) {
                    $revoke = $this->createRevokeRequest($order, $merchantKey[0]['merchantKey'], $merchantKey[0]['merchantId'], $paymentResponse['transaction_id']);
                    throw new \Exception($e->getMessage(), 105316);
                }
            }
            
            $resultWb = $this->integration->buyTicket($row["TOKEN_EXT"], $arrayTicket, $evtEvet->getExternalId(), $dev);
            
            if(array_key_exists("res", $resultWb) && $resultWb["res"]["status"] == "erro" && $order->getTotal() > 0) {
                $revoke = $this->createRevokeRequest($order, $merchantKey[0]['merchantKey'], $merchantKey[0]['merchantId'], $paymentResponse['transaction_id']);
                throw new \Exception($resultWb["res"]["des_erro"], 105317);
            }else {
                foreach ($resultWb["element"] as $el) {
                    array_push($ticketPurchaseReceipt, [
                            "cod_tipo_ingresso" => $el["des_ingresso"], 
                            "guid" => $el["des_cod_barras"], 
                            "ownerName" => $el["nom_associado_comp"]
                        ]
                    );
                }
            }
        }
        else {
            throw new \Exception('Payment not authorized', 7);
        }
        
        $order->setPaymentStatus($paymentStatus);
        
        $this->eventTicketService->updateAll();
        $orderFactory = $this->eventTicketService->factoryOrderDataSet($order);
        
        if($paymentResponse['status'] == 'captured') {
            $i = 0;
            foreach($row['ORDER_ITEMS'] as $orderitem){
                foreach ($orderitem['USERS'] as $key=>$value) {
                    if(empty($ticketPurchaseReceipt)) {
                        $ticket = $this->eventTicketService->buyTicket($orderitem['EVENT_TICKET_ID'], $value['ID'], $value['NAME'], $row['USER_ID']);
                    } else {
                        $orderValue = $this->eventTicketService->getEventTicket($items['EVENT_TICKET_ID']);
                            
                        foreach($ticketPurchaseReceipt as $val) {
                            if($orderValue->getCodeTypeTicket() == $val["cod_tipo_ingresso"]) {
                                $ticket = $this->eventTicketService->buyTicket($orderitem['EVENT_TICKET_ID'], $value['ID'], $val['ownerName'], $row['USER_ID'], $val["guid"]);
                            }
                        }
                    }
                    $ticketFactory = $this->factoryeTicketDataSet($ticket);
                    $orderFactory['TICKET'][$key + $i] = $ticketFactory;
                }
                $i = $key + 1;
            }
            if(isset($verifyCard['receiveNotification']) && $verifyCard['receiveNotification'] == 1) {
                $stringBase = '<h2 style="text-align: center;">Ol&aacute;,</h2><h3 style="text-align: center;">Seu dependente <strong>' . $order->getGenUser()->getFirstName() . " " . $order->getGenUser()->getLastName() . '</strong> utilizou R$ <strong>' . $nombre_format_francais = number_format($order->getTotal(), 2, ',', ' ')
 . '</strong> do seu cartão com final <strong>' . $order->getPayCreditcard()->getLastNumbers() . '</strong>.</h3><h3 style="text-align: center;"><em>Obrigado por utilizar o aplicativo.</em></h3>';
                
                $this->smtpService->sendEmail("Notificação de compra", $stringBase, $verifyCard['email']);
            }
        }
        $response->addDataSet(new DataSet('ORDERTICKET', $orderFactory));
    }
    
    public function createPaymentRequest($order, $merchantKey, $merchantId){
        $token = $order->getPayCreditcard()->getToken();
        $customerId = $order->getPayCreditcard()->getCustomerId();
        $gateway = $order->getPayCreditcard()->getGateway();
        $total = $order->getTotal();
        $orderReference = $order->getId();
        return $this->payment->execPaymentCC($merchantKey, $merchantId, $gateway, $order);
    }
    
    public function createRevokeRequest($order, $merchantKey, $merchantId, $transactionId) {
        $gateway = $order->getPayCreditcard()->getGateway();
        $total = $order->getTotal();
        return $this->payment->execRevokeCC($transactionId, $total, $gateway, $merchantKey, $merchantId);
    }
    
    public function factoryeTicketDataSet($ticket) {
        return array(
            "ID" => $ticket->getId(),
            "GEN_USER_ID" => $ticket->getGenUser() !== null ? $ticket->getGenUser()->getId() : null,
            "PAID_FOR_USER" => $ticket->getPaidForUser() !== null ? $ticket->getPaidForUser() : null,
            "OWNER_NAME" => $ticket->getOwnerName() !== null ? $ticket->getOwnerName() : null,
            "EVT_EVENT_TICKET_ID" => $ticket->getEvtEventTicket()->getId(),
            "STATUS" => $ticket->getStatus(),
            "GUID" => $ticket->getGuid()
        );
    }
    
    public function getTicketByEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $event_id = $row[self::EVENT_ID];
        
        $eventTickets = $this->eventTicketService->getTicketByEvent($event_id);
        
        $response->addDataSet(new DataSet('eventTickets', $eventTickets));
    }
    
    public function getTicketsFromEventByEventId(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $event_id = $row[self::EVENT_ID];
        
        $eventTickets = $this->eventTicketService->getTicketsFromEventByEventId($event_id);
        
        $response->addDataSet(new DataSet('eventTickets', $eventTickets));
    }

    public function getUserTicketsByOrg(DTO\Request\Row $request, DTO\Response $response) {
        $userTicketsRow   = $request->getRow();
        
        /* create the parameters */
        $userId  = $userTicketsRow[self::USER_ID];
        
        $nrorg   = $userTicketsRow[self::NRORG];

        $userTickets = $this->eventTicketService->getUserTickets($userId, $nrorg);
        
        $factoryTickets = $this->factoryTickets($userTickets);
        
        $response->addDataSet(new DataSet('userTickets', $factoryTickets));
        
    }    
    
    public function factoryTickets($userTickets){
        $factory = [];
        foreach ($userTickets as $key=>$userTicket) {
            $factory[$key]['id']          = $userTicket->getId();
            $factory[$key]['guid']        = $userTicket->getGUID();
            $factory[$key]['status']      = $userTicket->getStatus();
            $factory[$key]['price']       = $userTicket->getEvtEventTicket()->getPrice();
            $factory[$key]['amount']      = $userTicket->getEvtEventTicket()->getAmount();
            $factory[$key]['ticketName']  = $userTicket->getEvtEventTicket()->getName();
            $factory[$key]['eventId']     = $userTicket->getEvtEventTicket()->getEvtEvent()->getId();
            $factory[$key]['name']        = $userTicket->getEvtEventTicket()->getEvtEvent()->getName();
            $factory[$key]['about']       = $userTicket->getEvtEventTicket()->getEvtEvent()->getAbout();
            $factory[$key]['imageMap']    = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageMap();
            $factory[$key]['location']    = $userTicket->getEvtEventTicket()->getEvtEvent()->getLocation();
            $factory[$key]['imageLogo']   = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageLogo();
            $factory[$key]['finalDate']   = $userTicket->getEvtEventTicket()->getEvtEvent()->getFinalDate();
            $factory[$key]['imageCover']  = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageCover();
            $factory[$key]['initialDate'] = $userTicket->getEvtEventTicket()->getEvtEvent()->getInitialDate();
            $factory[$key]['userName']    = $userTicket->getGenUser() !== null ? $userTicket->getGenUser()->getFirstName() . ' ' . $userTicket->getGenUser()->getLastName(): $userTicket->getOwnerName();
        }
        
        return $factory;
    }
    
    public function createTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId        = $row[self::EVENT_ID];
        $userId         = $row[self::USER_ID];
        $nrorg          = $row[self::NRORG];
        $name           = $row['NAME'];
        $initialSale    = $this->stringToDate($row['INITIAL_SALE']);
        $finalSale      = $this->stringToDate($row['FINAL_SALE']);
        $amount         = $row['AMOUNT'];
        $price          = $row['PRICE'];
        $status         = $row['STATUS'];
        $event          = $this->eventTicketService->getEventById($eventId);
        $allowExternalUsers = $row['ALLOW_EXTERNAL_USERS'];
        $minPurchaseAmount  = $row['MIN_PURCHASE_AMOUNT'];
        
        $event          = $this->eventTicketService->getEventById($eventId);
        if(empty($event)) throw new \Exception("Event not found!", 1);
        
        $ticket         = $this->eventTicketService->createTicket($event, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status, $allowExternalUsers, $minPurchaseAmount);
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
    public function updateTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId        = $row[self::EVENT_ID];
        $userId         = $row[self::USER_ID];
        $nrorg          = $row[self::NRORG];
        $ticketId       = $row['ID'];
        $name           = $row['NAME'];
        $initialSale    = $this->stringToDate($row['INITIAL_SALE']);
        $finalSale      = $this->stringToDate($row['FINAL_SALE']);
        $amount         = $row['AMOUNT'];
        $price          = $row['PRICE'];
        $status         = $row['STATUS'];
        $allowExternalUsers = $row['ALLOW_EXTERNAL_USERS'];
        $minPurchaseAmount  = $row['MIN_PURCHASE_AMOUNT'];
        
        $ticket         = $this->eventTicketService->getEvtEventTicketById($ticketId);
        
        if(empty($ticket)) throw new \Exception("Ticket not found!", 1);
        
        $this->eventTicketService->updateTicket($ticket, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status, $allowExternalUsers, $minPurchaseAmount);
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date;
    }
    
    public function disableTickets(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $ticketIds = $row['TICKET_IDS'];
        
        foreach ($ticketIds as $ticketId) {
            $eventTicket = $this->eventTicketService->getEvtEventTicketById($ticketId);
            $this->eventTicketService->disableTicket($eventTicket);
        }
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
    public function getEventTickets(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $nrorg = $row['NRORG'];
        $eventIds = isset($row['EVENT_IDS']) ? $row['EVENT_IDS'] : null;
        $ticketIds = isset($row['TICKET_IDS']) ? $row['TICKET_IDS'] : null;
        $minValue = isset($row['MIN_VALUE']) ? str_replace('%', '', $row['MIN_VALUE']) : null;
        $maxValue = isset($row['MAX_VALUE']) ? str_replace('%', '', $row['MAX_VALUE']) : null;
        
        $tickets = $this->eventTicketService->getEventTickets($nrorg, $eventIds, $ticketIds, $minValue, $maxValue);
        
        $response->addDataSet(new DataSet('tickets', EvtEventTicket::manyToArray($tickets)));
    }
    
    public function getEventUserTickets(DTO\Request $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        
        $nrorg = $row['NRORG'];
        $eventIds = isset($row['EVENT_IDS']) ? $row['EVENT_IDS'] : null;
        $ticketIds = isset($row['TICKET_IDS']) ? $row['TICKET_IDS'] : null;
        $ownerName = isset($row['OWNER_NAME']) ? $row['OWNER_NAME'] : null;
        $buyerEmail = isset($row['BUYER_EMAIL']) ? $row['BUYER_EMAIL'] : null;
        $buyerPhone = isset($row['BUYER_PHONE']) ? $row['BUYER_PHONE'] : null;
        $itemsPerPage = $request->getFilterCriteria()->getPageSize();
        $page = $request->getFilterCriteria()->getPage();
        
        $tickets = $this->eventTicketService->getEventUserTickets($nrorg, $eventIds, $ticketIds, $ownerName, $buyerEmail, $buyerPhone, $itemsPerPage, $page);
        
        $response->addDataSet(new DataSet('tickets', EvtUserTicketRel::manyToArray($tickets)));
    }
    
}