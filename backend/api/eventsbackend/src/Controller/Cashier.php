<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Zeedhi\ApiGeneral\Helpers\Util;

use Zeedhi\ApiEvents\Service\Cashier as CashierService;

class Cashier {
    
    private $cashierService;
    
    public function __construct(CashierService $cashierService) {
        $this->cashierService = $cashierService;
    }
    
    public function startCashier(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $initialValue = $row['INITIAL_VALUE'];
        $eventSellerId = $row['EVENT_SELLER_ID'];
        $movementDate = $row['INITIAL_TIME'];
        
        $cashier = $this->cashierService->startCashier($eventId, $nrorg, $eventSellerId, $initialValue, $movementDate);
        
        $response->addDataSet(new DataSet('cashier', $cashier->toArray()));
    }
    
    public function finishCashier(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $cashierId = $row['CASHIER_ID'];
        $finalDate = $row['FINAL_TIME'];

        $cashier = $this->cashierService->finishCashier($eventId, $nrorg, $cashierId, $finalDate);
        
        $response->addDataSet(new DataSet('cashier', $cashier->toArray()));
    }
    
    public function cashierMovement(DTO\Request\Row $request, DTO\Response $response) {
        $row = Util::getParamsAsArray($request);
        $cashierId = $row['CASHIER_ID'];
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $value = $row['VALUE'];
        $movementDate = $row['MOVEMENT_TIME'];
        
        $movement = $this->cashierService->cashierMovement($cashierId, $eventId, $nrorg, $value, $movementDate);
        
        $response->addDataSet(new DataSet('movement', []));
    }
    
    public function getCashiersReport(DTO\Request $request, DTO\Response $response) {
        $row           = Util::getParamsAsArray($request);
        $date          = isset($row['openning']) ? $row['openning'] : NULL;
        $eventId       = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $eventSellerId = isset($row['EVENT_SELLER_ID']) ? $row['EVENT_SELLER_ID'] : NULL;
        $cashierIds    = isset($row['CASHIER_IDS']) ? $row['CASHIER_IDS'] : NULL;
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 100;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        $nrorg         = $row['NRORG'];
        
        if ($cashierIds == NULL || count($cashierIds) == 0)
            $cashiers = $this->cashierService->getCashiersReport($date, $eventId, $nrorg, $eventSellerId, $itemsPerPage, $page);
        else $cashiers = $this->cashierService->getMultipleCashiersReport($cashierIds, $itemsPerPage, $page);
        
        $arr = [];
        foreach ($cashiers as $cashier) {
            if (!is_string($cashier['openning'])) $cashier['openning'] = $cashier['openning']->format('d/m/Y H:i');
            if (!is_string($cashier['closing']))$cashier['closing'] = isset($cashier['closing']) ? $cashier['closing']->format('d/m/Y H:i') : '';
            array_push($arr, $cashier);
        }
        
        $response->addDataSet(new DataSet('getCashiersReport', $arr));
    }
    
    public function getCashierMovements($request, DTO\Response $response) {
        $row        = Util::getParamsAsArray($request);
        $cashierIds = isset($row['CASHIER_IDS']) ? $row['CASHIER_IDS'] : NULL;
        $cashierId  = $row['CASHIER_ID'];
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 100;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        
        if ($cashierIds == NULL || count($cashierIds) === 0) $cashierIds = [$cashierId];
        $movements = $this->cashierService->getCashierMovements($cashierIds, $itemsPerPage, $page);
        
        $arr = [];
        foreach ($movements as $movement) {
            $movement['movementDate'] = $movement['movementDate']->format('d/m/Y H:i');
            array_push($arr, $movement);
        }
        
        $response->addDataSet(new DataSet('getCashierMovements', $arr));
    }
    
    public function getCashierOrdersReport($request, DTO\Response $response) {
        $row        = Util::getParamsAsArray($request);
        $cashierIds = isset($row['CASHIER_IDS']) ? $row['CASHIER_IDS'] : NULL;
        $cashierId  = $row['CASHIER_ID'];
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 100;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        
        if ($cashierIds == NULL || count($cashierIds) === 0) $cashierIds = [$cashierId];
        $report    = $this->cashierService->getCashierOrdersReport($cashierIds, $itemsPerPage, $page);
        
        $response->addDataSet(new DataSet('getCashierOrdersReport', $report));
    }
    
    public function getCashierReceiptsReport($request, DTO\Response $response) {
        $row        = Util::getParamsAsArray($request);
        $cashierIds = isset($row['CASHIER_IDS']) ? $row['CASHIER_IDS'] : NULL;
        $cashierId  = $row['CASHIER_ID'];
        $itemsPerPage  = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPageSize() : 100;
        $page          = get_class($request) === 'Zeedhi\Framework\DTO\Request\Filter' ? $request->getFilterCriteria()->getPage() : 0;
        
        if ($cashierIds == NULL || count($cashierIds) === 0) $cashierIds = [$cashierId];
        $report    = $this->cashierService->getCashierReceiptsReport($cashierIds, $itemsPerPage, $page);
        
        $response->addDataSet(new DataSet('getCashierReceiptsReport', $report));
    }
    
    public function getCurrentCashierFromEventSeller($request, DTO\Response $response) {
        $row           = Util::getParamsAsArray($request);
        $eventSellerId = $row['EVENT_SELLER_ID'];
        
        $cashier       = $this->cashierService->getCurrentCashierFromEventSeller($eventSellerId);
        
        $response->addDataSet(new DataSet('cashier', $cashier ? $cashier->toArray() : []));
    }
    
}