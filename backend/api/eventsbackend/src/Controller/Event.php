<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Event as EventService;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Helpers\Util;
use Zeedhi\ApiEvents\Service\Environment;

use Zeedhi\ApiEvents\Helpers\Integration;

class Event extends Crud {

    protected $dataSourceName = 'event';
    
    const NRORG         = 'NRORG';
    const USER_ID       = 'USER_ID';
    const USER_TYPE_ID  = 'USER_TYPE_ID';
    const EVENT_ID      = 'EVENT_ID';
    const STRUCTURE_ID  = 'STRUCTURE_ID';
    const DATE          = 'DATE';
    const TAG           = 'TAG';
    const CATEGORY_ID   = 'CATEGORY_ID';
    const CATEGORIES    = 'CATEGORIES';
    const TAGS          = 'TAGS';
    const STRUCTURES    = 'STRUCTURES';

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param service $eventService
     */
    public function __construct(Manager $dataSourceManager,  EventService $eventService, Integration $integration) {
        parent::__construct($dataSourceManager);
        $this->eventService = $eventService;
        $this->integration = $integration;
    }
    
    /**
     * getEventsByOrg
     * Take events according to organization
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function getEventsByOrg(DTO\Request $request, DTO\Response $response) {
        $eventRow       = Util::getParamsAsArray($request);
        
        $nrorg          = $eventRow[self::NRORG];
        $userId         = $eventRow[self::USER_ID];
        $date           = isset($eventRow[self::DATE]) ? $eventRow[self::DATE] : NULL;
        $tags           = isset($eventRow[self::TAGS]) && $eventRow[self::TAGS] != '[]' ? $this->implodeMap($eventRow[self::TAGS], ' OR etg.evtTag = ') : '';
        $structures     = isset($eventRow[self::STRUCTURES]) && $eventRow[self::STRUCTURES] != '[]' ? $this->implodeMap($eventRow[self::STRUCTURES], ' OR e.genStructure = ') : '';
        $categories     = isset($eventRow[self::CATEGORIES]) && $eventRow[self::CATEGORIES] != '[]' ? $this->implodeMap($eventRow[self::CATEGORIES], ' OR e.genCategory = ') : '';

        $events         = $this->eventService->getEventsByOrg($nrorg, $structures, $tags, $date, $categories, $userId);

        if ( !empty($tags) )  {
            $events = $this->getEvtTagEventsByEventId($events, $date);
        }

        foreach ($events as $key => $event) {
            $events[$key]['ticketId'] = $this->eventService->getTicketFromEvent($nrorg, $event['id']) ? $this->eventService->getTicketFromEvent($nrorg, $event['id'])->getId() : "";
        }
        
        $response->addDataSet(new DataSet('events', $events));
    }

    public function implodeMap($jsonString, $query){
        $array = json_decode($jsonString, TRUE);
        $queryString = implode($query, array_map(function ($entry) {
          return $entry['id'];
        }, $array));

        return $queryString;
    }
    
    public function verifyDate($event, $date) {
        if ($date == NULL){ 
            /* Caso não seja informada a data, exibir todos os eventos que ainda não terminaram/aconteceram.
            Para isto, verificar se data final do evento é posterior à data atual. */
            $dateAtual = new \DateTime();
            $date      = $dateAtual->format('Y-m-d');
            $dateQuery = " AND e.finalDate >= '$date'";
            
            return date_format($event['finalDate'], 'Y-m-d') >= $date; 
        } else {
            /* Se a data for informada, exibir somente os eventos que estão acontecendo no dia.
            Então data inicial do evento deve ser anterior à data atual e data final posterior à data atual. */
            $dateQuery = " AND e.initialDate <= '$date' AND e.finalDate >= '$date'";
            
            return date_format($event['initialDate'], 'Y-m-d') <= $date && date_format($event['finalDate'], 'Y-m-d') >= $date; 
        }
    }
    
    public function getEvtTagEventsByEventId($events, $date){
        $visited = [];
        $formatedEvents = [];
        foreach ($events as $key=>$event) {
            if (in_array($event['id'], $visited) || !$this->verifyDate($event, $date)) {
            } else {
                $evtTagEvents[$key] = $this->eventService->getEvtTagEventsByEventId($event['id']);
                foreach ($evtTagEvents[$key] as $index=>$tag) {
                    $events[$key]['tags'][$index]['name'] = $tag->getEvtTag()->getName();
                    $events[$key]['tags'][$index]['id'] = $tag->getEvtTag()->getId();
                }
                array_push($visited, $event['id']);
                array_push($formatedEvents, $event);
            }    
        }
        return $formatedEvents;
    }
    
    public function getEventsUserHasTickets(DTO\Request\Row $request, DTO\Response $response) {
        $eventRow = $request->getRow();
        $userId   = $eventRow[self::USER_ID];
        $nrorg    = $eventRow[self::NRORG];

        $events   = $this->eventService->getEventsUserHasTickets($userId, $nrorg);
        $eventsWithTags = $this->getEvtTagEventsByEventId($events);

        $response->addDataSet(new DataSet('eventsUserHasTickets', $eventsWithTags));
    }
    
    public function favorite(DTO\Request\Row $request, DTO\Response $response){
        $row    = $request->getRow();
        $userId = $row[self::USER_ID];
        $nrorg  = $row[self::NRORG];
        
        $events = $this->eventService->favorite($nrorg, $userId);
        $eventsFactory = $this->factoryEventFavoriteNewFormat($events);

        $response->addDataSet(new DataSet('calendar', $eventsFactory));
    }
    
    public function factoryEventFavoriteNewFormat($events) {
        $eventsFactory = [];
        $date = new \DateTime();
        $visited = [];
        $currentDate = $date->add(new \DateInterval('P0Y0M0DT4H0M0S'))->format('Y-m-d H:i');
        foreach ($events as $key=>$event) {
            if ((!$event['finalDate'] || !$this->eventFinished($currentDate, $event['finalDate'])) && !in_array($event['id'], $visited)) {
                array_push($eventsFactory, $event);
            }
            array_push($visited, $event['id']);
            $array[$key] = $event['id'];
        }
        return $eventsFactory;
    }
    
    public function factoryEventFavorite($events) {
        $eventsFactory = [];
        $date = new \DateTime();
        $visited = [];
        $currentDate = $date->add(new \DateInterval('P0Y0M0DT4H0M0S'))->format('Y-m-d H:i');
        foreach ($events as $key=>$event) {
            if(!$this->eventFinished($currentDate, $event['finalDate']) && !in_array($event['id'], $visited)){
                $positionIndex = $this->categoryAlreadyExists($eventsFactory, $event);
                if($positionIndex == -1){
                    $eventsFactory['categories'][$key] = ['id' => $event['genCategoryId'], 'NAME' => $event['genCategoryName']];
                    $eventsFactory[$key]["category"] = $event['genCategoryName'];
                    $eventsFactory[$key]["events"] = [ $event ];
                } else {
                    array_push($eventsFactory[$positionIndex]["events"], $event);
                }
            }
            array_push($visited, $event['id']);
            $array[$key] = $event['id'];
        }
        return $eventsFactory;
    }
    
    public function categoryAlreadyExists($eventsFactory, $event) {
        if (!isset($eventsFactory['categories'])) return -1;
        foreach ($eventsFactory['categories'] as $index => $value) {    
            if($eventsFactory['categories'][$index]['id'] == $event['genCategoryId']){
                return $index;
            }
        }
        return -1;
    }
    
    public function getCategories(DTO\Request\Row $request, DTO\Response $response){
        $eventRow = $request->getRow();
        $nrorg   = $eventRow[self::NRORG];
        $categories   = $this->eventService->getCategories($nrorg);
        $categoriesFactory = array();
        foreach ($categories as $key=>$value) {
            $categoriesFactory[$key] = $this->factoryCategoryDataSet($value);
        }
        $response->addDataSet(new DataSet('categories', $categoriesFactory));
    }
    
    public function factoryCategoryDataSet($category) {
        return array(
            "ID" => $category->getId(),
            "NAME" => $category->getName(),
            "NRORG" => $category->getNrorg()
        );
    }
    
    public function getEventsWithMenusFromToday(DTO\Request\Row $request, DTO\Response $response){
        $row    = $request->getRow();
        $date = new \DateTime();
        $events = $this->eventService->getEventsWithMenusFromToday($row['NRORG']);
        
        $initialDate = $date->sub(new \DateInterval('P0Y0M0DT2H0M0S'))->format('d-m-Y H:i');
        $finalDate = $date->add(new \DateInterval('P0Y0M0DT4H0M0S'))->format('d-m-Y H:i');
        foreach($events as $key=>$event) {
            if($this->eventStarted($initialDate, $event['initialDate']) && $this->eventFinished($finalDate, $event['finalDate'])) {
                unset($events[$key]);    
            }
        }
        $response->addDataSet(new DataSet('EVENTS', $events));
    }
    
    public function getEventsWithByEventSeller(DTO\Request\Row $request, DTO\Response $response){
        $row    = $request->getRow();

        $date = new \DateTime();
        $events = $this->eventService->getEventsWithByEventSeller($row['NRORG'], $row['USER_ID']);
        $initialDate = $date->sub(new \DateInterval('P0Y0M0DT2H0M0S'))->format('d-m-Y H:i');
        $finalDate = $date->add(new \DateInterval('P0Y0M0DT4H0M0S'))->format('d-m-Y H:i');
        foreach($events as $key=>$event) {
            if($this->eventStarted($initialDate, $event['initialDate']) && $this->eventFinished($finalDate, $event['finalDate'])) {
                unset($events[$key]);    
            }
        }
        $response->addDataSet(new DataSet('EVENTS', $events));
    }
    
    public function getEventsAssociatedToUser(DTO\Request\Row $request, DTO\Response $response){
        $row    = $request->getRow();

        $date = new \DateTime();
        $events = $this->eventService->getEventsAssociatedToUser($row['NRORG'], $row['ASSOCIATED_USER_ID']);
        $initialDate = $date->sub(new \DateInterval('P0Y0M0DT2H0M0S'))->format('d-m-Y H:i');
        $finalDate = $date->add(new \DateInterval('P0Y0M0DT4H0M0S'))->format('d-m-Y H:i');
        foreach($events as $key=>$event) {
            if($this->eventStarted($initialDate, $event['initialDate']) && $this->eventFinished($finalDate, $event['finalDate'])) {
                unset($events[$key]);    
            }
        }
        $response->addDataSet(new DataSet('EVENTS', $events));
    }
    
    private function eventStarted($currentInitialDate, $eventInitialDate) {
        if ($currentInitialDate != null && $eventInitialDate != null)
            return strtotime($currentInitialDate) >= strtotime(date_format($eventInitialDate, 'd-m-Y H:i'));
        else return false;
    }
    
    private function eventFinished($currentFinalDate, $eventFinalDate) {
        return strtotime($currentFinalDate) > strtotime(date_format($eventFinalDate, 'd-m-Y H:i'));
    }
    
    public function associateSellerToEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $userId  = $row['ASSOCIATED_USER_ID'];
        $eventId = $row[self::EVENT_ID];
        $adminId = $row[self::USER_ID];
        
        $this->eventService->associateSellerToEvent($userId, $eventId, $adminId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function unlinkSellerFromEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $sellerData  = $row['SELLER_DATA'];
        $adminId     = $row[self::USER_ID];
        
        foreach ($sellerData as $value) {
            $sellerId = $value['id'];
            $eventId = $value['eventId'];
            $this->eventService->unlinkSellerFromEvent($sellerId, $eventId, $adminId);
        }
                
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function createEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();

        $userId         = $row['USER_ID'];
        $type           = $row['TYPE'];
        $status         = $row['STATUS'];
        $nrorg          = $row['NRORG'];
        $name           = $row['NAME'];
        $imageLogo      = isset($row['IMAGE_LOGO']) ? $row['IMAGE_LOGO' ] : null;
        $imageMap       = isset($row['IMAGE_MAP']) ? $row['IMAGE_MAP'] : null;
        $imageCover     = isset($row['IMAGE_COVER']) ? $row['IMAGE_COVER'] : null;
        $location       = $row['LOCATION'];
        $salesMethod    = $row['SALES_METHOD'];
        $rating         = $row['RATING'];
        $classification = isset($row['CLASSIFICATION']) ? $row['CLASSIFICATION'] : null;
        $about          = $row['ABOUT'];
        $categoryId     = $row['CATEGORY_ID'];
        $structureId    = $row['STRUCTURE_ID'];
		$initialDate    = isset($row['INITIAL_DATE']) ? $this->stringToDate($row['INITIAL_DATE']) : null;
		$finalDate      = isset($row['FINAL_DATE']) ? $this->stringToDate($row['FINAL_DATE']) : null;
		$parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : null;
        $typeTag        = "E";
        
        $event        = $this->eventService->createEvent($userId, $userId, $type, $status, $nrorg, $name, $imageMap, $imageLogo, $imageCover, $location, $salesMethod, $rating, $classification, $about, $initialDate, $finalDate, $categoryId, $structureId, $parentId);
        $eventId = $event->getId();
        
        if(!empty($row['TAGS']))
            $this->eventService->associateEventTags($row['USER_ID'], $row['NRORG'], $eventId, $row['TAGS'], $row['CATEGORY_ID'], $typeTag);
        
        $response->addDataSet(new DataSet('response', ['id' => $eventId, 'imageCover' => $event->getImageCover()]));
    }
    
    public function updateEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
		$userId      = $row['USER_ID'];
        $type        = $row['TYPE'];
        $status      = $row['STATUS'];
        $nrorg       = $row['NRORG'];
        $name        = $row['NAME'];
        $imageLogo   = isset($row['IMAGE_LOGO']) ? $row['IMAGE_LOGO' ] : null;
        $imageMap    = isset($row['IMAGE_MAP']) ? $row['IMAGE_MAP'] : null;
        $imageCover  = isset($row['IMAGE_COVER']) ? $row['IMAGE_COVER'] : null;
        $location    = $row['LOCATION'];
        $salesMethod = $row['SALES_METHOD'];
        $rating      = $row['RATING'];
        $classification = isset($row['CLASSIFICATION']) ? $row['CLASSIFICATION'] : null;
        $about       = $row['ABOUT'];
        $categoryId  = $row['CATEGORY_ID'];
        $structureId = $row['STRUCTURE_ID'];
		$initialDate = $this->stringToDate($row['INITIAL_DATE']);
		$finalDate   = $this->stringToDate($row['FINAL_DATE']);
        $typeTag     = "E";
		$eventId     = $row['EVT_EVENT_ID'];
        $event       = $this->eventService->getEventById($eventId);
        $userType    = $this->eventService->getGenUserType($row['USER_ID']);
		$parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID'] : null;
        
        $event = $this->eventService->updateEvent($event, $userId, $userId, $type, $status, $nrorg, $name, $imageMap, $imageLogo, $imageCover, $location, $salesMethod, $rating, $classification, $about, $initialDate, $finalDate, $categoryId, $structureId, $parentId);
        
        if(!empty($row['TAGS'])){
            $this->removeEvtTagEvents($eventId);
            $this->eventService->associateEventTags($row['USER_ID'], $row['NRORG'], $eventId, $row['TAGS'], $row['CATEGORY_ID'], $typeTag);
        }
        
        $response->addDataSet(new DataSet('response', [$event->getImageCover()]));
    }
    
    public function removeEvtTagEvents($eventId) {
        $evtTagEvents = $this->eventService->getEvtTagEventsByEventId($eventId);
        
        foreach ($evtTagEvents as $evtTagEvent) {
            $this->eventService->removeRegister($evtTagEvent);
        }
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$newDate = new \DateTime($dateFormat);
		return $newDate;
    }

    public function inativeEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $event = $this->eventService->getEventById($row['EVT_EVENT_ID']);
        $userType  = $this->eventService->getGenUserType($row['USER_ID']);
        
        if(!empty($userType) && $userType[0]['name'] == "Admin") {
            $this->eventService->inativeEvent($event, $row['USER_ID']);
        }else {
            throw Exception::unauthorizedAction();                    
        }
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    /*deprecated*/
    public function associateEventTags(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $userId      = $row['USER_ID'];
		$nrorg       = $row['NRORG'];
		$eventId     = $row['EVT_EVENT_ID'];
		$arrayTags   = $row['TAGS'];
		$categoryId  = $row['CATEGORY_ID'];
        $type        = "E";
        $this->eventService->associateEventTags($userId, $nrorg, $eventId, $arrayTags, $categoryId, $type);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function getEventsFromOrganization($request, DTO\Response $response) {
        $row    = Util::getParamsAsArray($request);
        $nrorg  = $row['NRORG'];
        
        $events = $this->eventService->getEventsFromOrganization($nrorg);
        foreach ($events as $key => $event) {
            $events[$key]['tags'] = $this->eventService->getTagsFromEvent($event['id']);
        }
        
        $response->addDataSet(new DataSet('events', $events));
    }    
    
    public function getEventsAndStores($request, DTO\Response $response) {
        $row        = Util::getParamsAsArray($request);
        $nrorg      = $row['NRORG'];
        $userId     = isset($row['USER_ID']) ? $row['USER_ID'] : NULL;
        $status     = isset($row['STATUS']) ? $row['STATUS'] : NULL;
        
        $events = $this->eventService->getEventsAndStores($nrorg, $userId, $status);
        
        $response->addDataSet(new DataSet('events', $events));
    }
    
    public function changeStatusOfEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        $status      = $row['STATUS'];
        $userId      = $row['USER_ID'];
        $eventId     = $row['EVENT_ID'];
        $event       = $this->eventService->getEventById($eventId);
        
        $status = $this->eventService->changeStatusOfEvent($event, $userId, $status);
        
        $response->addDataSet(new DataSet('response', ['status' => $status]));
    }
    
    /*INICIO DOS METODOS DE INTEGRACAO*/
    public function consumesWebServiceGET($url) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        
        // Will dump a beauty json :3
        return json_decode($result, true);
    }
    
    /* construtor da url para consumir WebService */
    public function consumesWebServicePost($url, $parameters) {
        $data = sprintf("%s?%s", $url, http_build_query($parameters));
        
        // inicia seção CURL 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        // finaliza seção CURL
        curl_close($curl);
        
        $objResult = json_decode($result, true);

        return $objResult;
    }
    
    public function getEventsWebService(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $nrorgs      = array(1, 2);
        
        foreach($nrorgs as $nrorg) {
            $url        = "https://socio.minastc.com.br/ws/eventos.php";
            $data['f']  = $this->getOrganization($nrorg);
            $data['desenvolvimento']  = "sim";
            $result     = $this->consumesWebServicePost($url, $data);
            
            if(!empty($result)){
                foreach ($result as $value) {
                    if(!empty($value['categoria_pilar'])) {
                        $category = $this->checkIfCategoryExistsIfThereIsNoCreate($nrorg, $value['categoria_pilar']);
                    } else {
                        $category = $this->eventService->getCategory($nrorg, "Geral");
                    }
                    $id             = $value['id'];
                    $titulo         = $value['titulo'];
                    $dataInicio     = $value['data_inicio'];
                    $horaInicio     = $value['hora_inicio'];
                    $dataFim        = $value['data_fim'] ? $value['data_fim'] : $value['data_inicio'];
                    $horaFim        = $value['hora_fim'] ? $value['hora_fim'] : "23:59";
                    $descricao      = $value['descricao'];
                    $imagem         = $value['imagem'];
                    $rating         = $value['classificacao'];
                    $location       = $value['local'];
                    $genCategory    = $category;
                    $values         = !empty($value['valor']) ? $value['valor'] : "";
                    
                    $initialDateStr = $dataInicio . " " . $horaInicio;
                    $finalDateStr   = $dataFim . " " . $horaFim;
                    
                    $initialDate    = $this->stringToDate($initialDateStr);
                    $finalDate      = $this->stringToDate($finalDateStr);
                    
                    $event          = $this->eventService->createEventWB($id, $titulo, $descricao, $imagem, $initialDate, $finalDate, $nrorg, $rating, $location, $genCategory, $values);
                }
            }
        }
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function checkIfCategoryExistsIfThereIsNoCreate($nrorg, $genCategory) {
        $category = $this->eventService->getCategory($nrorg, $genCategory);
        
        if (empty($category)) {
            $category =  $this->eventService->createCategory($nrorg, $genCategory);
        }
        return $category;
    }
    
    public function getEventByNrorg($nrorg) {
        $events = $this->eventService->getEventByNrorg($nrorg);
        
        $currentDate = new \DateTime();
        $currentDate->sub(new \DateInterval('PT10H30S'));
        $currentDate->format('Y-m-d');
        $eventsToChange = null;
        
        foreach ($events as $event) {
            if($event["initialDate"] >= $currentDate) {
                array_push($eventsToChange, $event); 
            }
        }
        
        return $eventsToChange;
    }
    
    public function getEventData($request, DTO\Response $response){
        $row        = Util::getParamsAsArray($request);
        $nrorg      = $row['NRORG'];
        $eventId     = $row['EVENT_ID'];
        
        $event = $this->eventService->getEventData($nrorg, $eventId);
        
        $response->addDataSet(new DataSet('event', $event->toArray()));
    }
    
    public function getEventChildren($request, DTO\Response $response){
        $row        = Util::getParamsAsArray($request);
        $nrorg      = $row['NRORG'];
        $eventId     = $row['EVENT_ID'];
        
        $events = $this->eventService->getEventChildren($nrorg, $eventId, 'A');
        
        $response->addDataSet(new DataSet('children', $events));
    }
    
    public function getOrganization($nrorg) {
        switch ($nrorg) {
            case 0:
                $response = "teknisa";
                break;
            case 1:
                $response = "minas";
                break;
            case 2:
                $response = "nautico";
                break;
            default:
                throw Exception::organizationNotFound();
        }
        
        return $response;
    }
    
    public function importEventTickets(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        $dev    = true;
        $token  = $row['TOKEN_EXT'];
        $nrorg  = isset($row['NRORG']) ? $row['NRORG'] : null;
        $npf    = isset($row['EXTERNAL_ID']) ? $row['EXTERNAL_ID'] : null;
        
        $eventList = $this->integration->eventList($token, $nrorg, $npf, $dev); /*buscar os eventos com ingressos*/  
        
        if(!empty($eventList) && $eventList["res"]["status"] == "ok") {
            
            foreach($eventList["res"]["eventos"] as $value) {
                $initialDate        = !empty($value["dataInicioEvento"])? new \DateTime($value["dataInicioEvento"] . ' ' . $value["horaInicioEvento"])  : null;
                $finalDate          = !empty($value["dataFimEvento"])   ? new \DateTime($value["dataFimEvento"] . ' ' . $value["horaFimEvento"])        : null ;
                $numeroSeqEvento    = !empty($value["numeroSeqEvento"]) ? $value["numeroSeqEvento"] : null;
                $nomeEvento         = !empty($value["nomeEvento"])      ? $value["nomeEvento"]      : null;
                $descricaoEvento    = !empty($value["descricaoEvento"]) ? $value["descricaoEvento"] : null;
                $codigoEvento       = !empty($value["codigoEvento"])    ? $value["codigoEvento"]    : null;
                $numeroSeqEvento    = !empty($value["numeroSeqEvento"]) ? $value["numeroSeqEvento"] : null;
                $tipoEvento         = "E";
                $status             = "A";
                
                $tickets = $this->integration->infoEvent($token, $codigoEvento, $numeroSeqEvento, $nrorg, $npf, $dev);
                
                if(array_key_exists("textoEvento", $tickets["res"])) {
                    $descriptionEvent = "";
                    
                    foreach($tickets["res"]["textoEvento"] as $val) {
                        
                        if($val["nro_linha"] == "1") {
                            $descriptionEvent .= ($val["des_linha"]) . "\n";
                        }elseif(substr($val["des_linha"], -1) == ";") {
                            $descriptionEvent .= ($val["des_linha"]) . "\n";
                        } else {
                            $descriptionEvent .= ($val["des_linha"]) . " ";
                        }
                    }
                    
                    $event = $this->eventService->updateEventWB($numeroSeqEvento, $tipoEvento, $descriptionEvent);
                    
                    if(!empty($event) && array_key_exists("ingressos", $tickets["res"])) {
                        foreach($tickets["res"]["ingressos"] as $value) {
                            $initialSale    = null;
                            $finalSale      = null;
                            $qtd_disponivel = 99999;
                            
                            if(!empty($value["dataInicioVenda"]) && !empty($value["dataFimVenda"])) {
                                $initialSale    = new \DateTime($value["dataInicioVenda"]);
                                $finalSale      = new \DateTime($value["dataFimVenda"]);
                            }
                            $ticket = $this->eventService->createOrUpdateTicket($event, $value["codTipoIngresso"], $value["codTipoIngresso"], $initialSale, $finalSale, $qtd_disponivel, $value["vlr_ingresso"], $status, $nrorg);
                        }
                    }
                }
            } 
            
            $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
        } else {
            $response->addDataSet(new DataSet('response', ['error' => $eventList["res"]['des_erro']]));
        }
    }
    /*FIM DE METODOS DE INTEGRACAO*/
    
}   