<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\MenuProduct as MenuProductService;

use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;


use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;


class MenuProduct extends Crud {
    
    const MENU_ID            = 'MENU_ID';
    const MENU_NAME          = 'MENU_NAME';
    const PRODUCTS           = 'PRODUCTS';
    
    const PRODUCT_GROUP_ID   = 'PRODUCT_GROUP_ID';
    const PRODUCT_GROUP_NAME = 'NAME';
    
    const PRODUCT_ID         = 'PRODUCT_ID';
    const PRODUCT_NAME       = 'NAME';
    const PRODUCT_DETAIL     = 'DETAIL';
    const PRODUCT_PRICE      = 'PRICE';
    const PRODUCT_IMAGE      = 'IMAGE';
    const ESTIMATED_TIME     = 'ESTIMATED_TIME';
    const EXTRAS             = 'EXTRAS';
    const PRODUCT_STATUS     = 'STATUS';
    
    const EXTRA_ID           = 'EXTRA_ID';
    const EXTRA_NAME         = 'NAME';
    const EXTRA_REQUIRED     = 'REQUIRED';
    const EXTRA_MULTIPLE     = 'MULTIPLE';
    const OPTIONS            = 'OPTIONS';

    const OPTION_ID          = 'OPTION_ID';
    const OPTION_NAME        = 'NAME';
    const OPTION_PRICE       = 'PRICE';
    
    /** @var productService */
    protected $menuProductService;
    
    public function __construct(Manager $dataSourceManager, MenuProductService $menuProductService) {
        parent::__construct($dataSourceManager);
        $this->menuProductService = $menuProductService;
    }
    
    public function getProductsFromOrganizationThatAreNotInEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row      = $request->getRow();

        $nrorg    = $row['NRORG'];
        $eventId  = $row['EVENT_ID'];
        
        $products = $this->menuProductService->getProductsFromOrganizationThatAreNotInEvent($nrorg, $eventId);
        
        $productsArray = OrdProduct::manyToArray($products);
        
        $response->addDataSet(new DataSet('PRODUCTS', $productsArray));
    }
    
    public function getMenuProductsFromEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row         = $request->getRow();
        $eventId     = $row['EVENT_ID'];
        
        $menuProducts = $this->menuProductService->getMenuProductsFromEvent($eventId);
        
        $menuProducsArray = OrdMenuProduct::manyToArray($menuProducts);
        
        $response->addDataSet(new DataSet('MENU_PRODUCTS', $menuProducsArray));
    }
    
    public function getMenuProductsFromProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $productGroupId = $row['PRODUCT_GROUP_ID'];
        
        $menuProducts = $this->menuProductService->getMenuProductsFromProductGroup($productGroupId);
        // var_dump($menuProducts); die;
        $menuProducsArray = OrdMenuProduct::manyToArray($menuProducts);
        
        $response->addDataSet(new DataSet('MENU_PRODUCTS', $menuProducsArray));
    }
    
    public function getProductGroupsFromEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId     = $row['EVENT_ID'];
        
        $productGroups = $this->menuProductService->getProductGroupsFromEvent($eventId);

        $response->addDataSet(new DataSet('PRODUCT_GROUP', [$productGroups]));
    }
    
    function removeProductFromEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $menuProductIds = $row['MENU_PRODUCT_IDS'];
        
        foreach($menuProductIds as $menuProductId) {
            $this->menuProductService->removeProductFromEvent($menuProductId);
        }
        
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }

    function createMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checar se os parâmetros são válidos */
        $expectedParameters = [self::MENU_NAME, General::NRORG, General::STORE_ID];
        //
        if (!General::checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('createMenu', $expectedParameters);
        }
        
        /* Obter parâmetros da requisição */
        $storeId    =  $row[General::STORE_ID];
        $menuName   =  $row[self::MENU_NAME];
        $nrorg      =  $row[General::NRORG];
        $workshifts =  isset($row[self::WORKSHIFTS]) ? $row[self::WORKSHIFTS] : [];
        $products   =  isset($row[self::PRODUCTS]) ? $row[self::PRODUCTS] : [];
        
        /* Criar cardápio */
        $menu       = $this->productService->createMenu($menuName, $nrorg, $products, $storeId, $workshifts);
        
        /* Enviar resposta */
        $response->addDataSet(new DataSet('menu', $menu->toArray()));
    }
    
    function createProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        $eventId  = $row['EVENT_ID'];
        $name     = $row[self::PRODUCT_GROUP_NAME];
        $nrorg    = $row['NRORG'];
        $userId   = $row['USER_ID'];
        
        $evtEventMenu = $this->menuProductService->getEvtEventMenuByEventId($eventId);
        
        if(!$evtEventMenu) {
            $menu   = $this->menuProductService->createMenu($nrorg, $eventId);
            $menuId = $menu->getId();
        } else {
            $menuId = $evtEventMenu->getOrdMenu()->getId();
        }
        
        
        $productGroupId = $this->menuProductService->createProductGroup($eventId, $menuId, $nrorg, $name, $userId);
        
        $response->addDataSet(new DataSet('response', ['productGroupId' => $productGroupId, 'menuId' => $menuId]));
    }
    
    function updateProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();

        $eventId        = $row['EVENT_ID'];
        $menuId         = $row[self::MENU_ID];
        $name           = $row[self::PRODUCT_GROUP_NAME];
        $nrorg          = intval($row['NRORG']);
        $userId         = $row['USER_ID'];
        $productGroupId = $row['ID'];
        $parentId       = isset($row['PARENT_ID']) ? $row['PARENT_ID']: null;
        $productGroup   = $this->menuProductService->getProductGroupById($productGroupId);
    
        $productGroupId = $this->menuProductService->updateProductGroup($eventId, $menuId, $nrorg, $name, $userId, $productGroup, $parentId);
        
        $response->addDataSet(new DataSet('productGroup', [$productGroupId]));
    }
    
    function createMenuProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obter parâmetros da requisição */
        $name           = $row['PRODUCT_NAME'];
        $nrorg          = $row['NRORG'];
        $detail         = $row['PRODUCT_DETAIL'];
        $price          = $row['PRODUCT_PRICE'];
        $eventId        = $row['EVENT_ID'];
        $productGroupId = $row['PRODUCT_GROUP_ID'];
        $menuId         = $row['MENU_ID'];
        $amount         = $row['AMOUNT'];

        $produto = $this->menuProductService->getProductByName($name);
        
        if(!$produto)
            $produto = $this->menuProductService->createProduct($name, $detail, $price, $nrorg);
            
        $produto->setName($name);
        $produto->setPrice($price);
        $produto->setDetail($detail);
            
        $product = $this->menuProductService->linkProductToStore($produto->getId(), $eventId, $productGroupId, $menuId, $amount);
    
        /* Enviar resposta */
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }

    function cloneProductGroupsFromMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $sourceMenuId = $row['SOURCE_MENU_ID'];
        $targetMenuId = $row['TARGET_MENU_ID'];
        $storeId      = $row['STORE_ID'];
        
        $this->productService->cloneProductGroupsFromMenu($sourceMenuId, $targetMenuId, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function getProductsFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $storeId  = $row[General::STORE_ID];
        
        $products = $this->productService->getProductsFromStore($storeId);
        
        $response->addDataSet(new DataSet('products', OrdProduct::manyToArray($products)));
    }
    
    function removeProductFromGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];

        $this->productService->removeProductFromGroup($productId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeProductFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $productId = $row[self::PRODUCT_ID];
        
        $this->productService->removeProductFromStore($productId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeMenuFromStore(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        
        $menuId  = $row[self::MENU_ID];
        $storeId = $row[self::STORE_ID];
        
        $this->productService->removeMenuFromStore($menuId, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->removeProductGroup($productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkProductToGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->linkProductToGroup($productId, $productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkMenuProductToGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        
        $this->productService->linkMenuProductToGroup($productId, $productGroupId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function linkProductToStore($productId, $storeId, $productGroupId, $menuId) {
        
        $menuProduct    = $this->productService->linkProductToStore($productId, $storeId, $productGroupId, $menuId);
    
    }
    
    function linkMenuProductToStore(DTO\Request\Row $request, DTO\Response $response) {
        $row            = $request->getRow();
        
        $productId      = $row[self::PRODUCT_ID];
        $storeId        = $row[self::STORE_ID];
        $productGroupId = $row[self::PRODUCT_GROUP_ID];
        $menuId         = $row[self::MENU_ID];
        
        $menuProduct    = $this->productService->linkMenuProductToStore($productId, $storeId, $productGroupId, $menuId);
        
        $response->addDataSet(new DataSet('product', $menuProduct->toArray()));
    }
    
    function updateMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $menuId     = $row[self::MENU_ID];
        $name       = $row[self::MENU_NAME];
        $workshifts = isset($row[self::WORKSHIFTS]) ? $row[self::WORKSHIFTS] : NULL;
        $storeId    = isset($row[self::STORE_ID]) ? $row[self::STORE_ID] : NULL;
        
        $this->productService->updateMenu($menuId, $name, $workshifts, $storeId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateMenuProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Obter parâmetros da requisição */
        $menuProductId  = $row[self::PRODUCT_ID];
        $status         = isset($row[self::PRODUCT_STATUS]) ? $row[self::PRODUCT_STATUS] : NULL;
        $name           = isset($row['PRODUCT_NAME']) ? $row['PRODUCT_NAME'] : NULL;
        $nrorg          = isset($row['NRORG']) ? $row['NRORG'] : NULL;
        $detail         = isset($row['PRODUCT_DETAIL']) ? $row['PRODUCT_DETAIL'] : NULL;
        $price          = isset($row['PRODUCT_PRICE']) ? $row['PRODUCT_PRICE'] : NULL;
        $menuId         = isset($row['MENU_ID']) ? $row['MENU_ID'] : NULL;
        $image          = isset($row[self::PRODUCT_IMAGE]) ? $row[self::PRODUCT_IMAGE] : NULL;
        $estimatedTime  = isset($row[self::ESTIMATED_TIME]) ? $row[self::ESTIMATED_TIME] : NULL;
        $productGroupId = isset($row['PRODUCT_GROUP_ID']) ? $row['PRODUCT_GROUP_ID'] : NULL;
        $amount         = isset($row['AMOUNT']) ? $row['AMOUNT'] : null;

        /* Criar produto */
        $product = $this->menuProductService->updateProduct($menuProductId, $name, $detail, $price, $nrorg, $estimatedTime, $menuId, $productGroupId, $image, $status, $amount);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function addOfferProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $menuProductId = $row['MENU_PRODUCT_ID'];
        $offerId = $row['OFFER_ID'];
        $amount = $row['AMOUNT'];
        $price = $row['PRICE'];
        
        $this->menuProductService->addOfferProduct($menuProductId, $offerId, $amount, $price);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function updateOfferProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $offerId = $row['OFFER_PRODUCT_ID'];
        $amount = $row['AMOUNT'];
        $price = $row['PRICE'];
        
        $this->menuProductService->updateOfferProduct($offerId, $amount, $price);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    function removeOfferProduct(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $offerProductId = $row['OFFER_PRODUCT_ID'];

        $this->menuProductService->removeOfferProduct($offerProductId);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
}