<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Product as ProductService;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiGeneral\Helpers\Environment;


class Product extends Crud {
    
    const NRORG           = 'NRORG';
    const USER_ID         = 'USER_ID';
    const USER_TYPE_ID    = 'USER_TYPE_ID';
    const EVENT_ID        = 'EVENT_ID';
    const EVENT_TICKET_ID = 'EVENT_TICKET_ID';

    
    /** @var productService */
    protected $productService;
    
    
    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param  $productService
     */
    public function __construct(Manager $dataSourceManager, Environment $environment, $productService) {
        parent::__construct($dataSourceManager);
        $this->environment = $environment;
        $this->ProductService = $productService;
    }

    /**
     * getProducts
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function getProductGroup(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $event_id = $row[self::EVENT_ID];
        
        $productGps = $this->ProductService->getProductGroups($event_id);
        
        $products = $this->ProductService->getProducts($event_id);
        
        $productGroups = $this->factoryProducts($products, $productGps);

        $response->addDataSet(new DataSet('productGroups', $productGroups));
    }
    
    public function getMenus(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $eventId = $row[self::EVENT_ID];
        
        $event = $this->ProductService->getMenus($eventId);

        $response->addDataSet(new DataSet('event', $event->toArray()));
    }
    
    /**
     * factoryProducts
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param ARRAY $products
     * @param ARRAY $productGroups
     * @return ARRAY $aux
     */
    public function factoryProducts($products, $productGroups) {
        $aux['category'] = array();
        foreach($products as $i=>$p) {   
            if (!array_key_exists($p['nameProduct'], $aux['category'])) {
                $aux['category'][$p['nameProduct']] = array();
            }
            array_push($aux['category'][$p['nameProduct']], $p);
        }
        return $aux;
    }
    
}