<?php
namespace Zeedhi\ApiEvents\Service;
use Zeedhi\ApiEvents\Service\Order as OrderService;

use Zeedhi\ApiEvents\Model\Entities\EvtEventTicket;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\GenDependent; //Cristiano
use Zeedhi\ApiEvents\Model\Entities\PayGatewayRel;


const EVENT_ID        = 'EVENT_ID';


use Doctrine\ORM\EntityManager;

class EventTicket extends UserOperation{
    
    public function __construct(EntityManager $entityManager, OrderService $orderService) {
        parent::__construct($entityManager);
        $this->orderService = $orderService;
    }
    
    /**
     * getTicketByEvent
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param int $event_id
     * @return array de $tickets
     * @throws Exception
     * 
     */
    public function getTicketByEvent($event_id) {
        $eventTicket   = 'Zeedhi\ApiEvents\Model\Entities\EvtEventTicket';
        $tickets = $this->getEntityManager()->createQuery(
            "SELECT et.status, et.finalSale, et.startSale, et.amount, et.price, et.name, et.id 
            FROM $eventTicket et
            WHERE et.evtEvent = $event_id
            AND et.amount > 0"
        )->getResult();
        
        return $tickets;

    }
    
    public function getTicketsFromEventByEventId($event_id) {
        $eventTicket   = 'Zeedhi\ApiEvents\Model\Entities\EvtEventTicket';
        $tickets = $this->getEntityManager()->createQuery(
            "SELECT et.status, et.finalSale, et.startSale, et.amount, et.price, et.name, et.id 
            FROM $eventTicket et
            WHERE et.evtEvent = $event_id AND et.status != 'D'"
        )->getResult();
        
        return $tickets;

    }
    
    /**
     * getUserTickets
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param INT $EVENT_ID
     * @param INT $USER_ID
     *
     * @return array  $tickets
     */
    public function getUserTickets($userId, $nrorg) {
        $ticketsUser   = 'Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel';
        $tickets = $this->getEntityManager()->createQuery(
            "SELECT tu, et, ee
            FROM $ticketsUser tu
            LEFT JOIN tu.evtEventTicket et 
            LEFT JOIN et.evtEvent ee
            WHERE tu.genUser = $userId
            OR tu.paidForUser = $userId
            AND tu.nrorg = $nrorg"
        )->getResult();
        
        return $tickets;
    }
    
    /**
     * buyTicket
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param INT $ticketIid
     * @param INT $userTypeId
     * 
     * @return boolean
     *
     */
    public function buyTicket($ticketId, $userId=null, $ownerName=null, $userPay) {
        $genUser = $userId !== null ? $this->getEntityManager()->getRepository(GenUser::class)->find($userId) : null;    
        $evtEventTicket = $this->getEntityManager()->getRepository(EvtEventTicket::class)->find($ticketId);
        $evtEventTicket->setAmount($evtEventTicket->getAmount() - 1);
        $ticket = new EvtUserTicketRel();
        $ticket->setGuid($ticket->getGUID());
        $ticket->setGenUser($genUser);
        $ticket->setOwnerName($ownerName);
        $ticket->setPaidForUser($userPay);
        $ticket->setNrorg($evtEventTicket->getNrorg());
        $ticket->setEvtEventTicket($evtEventTicket);
        $ticket->setStatus('P');
        $this->getEntityManager()->persist($ticket);
        $this->getEntityManager()->flush();
        
        return $ticket;
    }
    
    public function factoryOrderDataSet($order) {
        return $this->orderService->factoryOrderDataSet($order);
    }
    
    public function updateAll() {
        $this->getEntityManager()->flush();
    }
    
    /**
     * @param integer  $nrorg
     * @param integer $userProfile
     * @param float $cardId
     *
     * @return BpOrder
     */
    public function createOrderTicket($creditCardId, $eventId, $userId, $orderItems) {
        $order = $this->orderService->createOrder($creditCardId, $eventId, $userId, $orderItems);
        return $order;
    }
    
    public function getGenUser($userId){
        $genUser = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        return $genUser;
    }
    
    public function getMerchantKey($nrorg) {
        return PayGatewayRel::getMerchantKeyByOrganization($nrorg, $this->getEntityManager());
    }
    
    public function getEventById($eventId) {
        $event = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        return $event;        
    }  
    
    public function getEvtEventTicketById($ticketId) {
        $ticket = $this->getEntityManager()->getRepository(EvtEventTicket::class)->findOneBy(['id' => $ticketId]);
        return $ticket;        
    }
    
    public function createTicket($event, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status) {
        $ticket = new EvtEventTicket();
        
        $ticket->setEvtEvent($event);
        $ticket->setCreatedBy($userId);
        $ticket->setNrorg($nrorg);
        $ticket->setName($name);
        $ticket->setStartSale($initialSale);
        $ticket->setFinalSale($finalSale);
        $ticket->setAmount($amount);
        $ticket->setPrice($price);
        $ticket->setStatus($status);
        
        $this->getEntityManager()->persist($ticket);
        $this->getEntityManager()->flush();
        
        return $ticket;
    }
    
    public function updateTicket($ticket, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status) {
        $ticket->setModifiedBy($userId);
        $ticket->setNrorg($nrorg);
        $ticket->setName($name);
        $ticket->setStartSale($initialSale);
        $ticket->setFinalSale($finalSale);
        $ticket->setAmount($amount);
        $ticket->setPrice($price);
        $ticket->setStatus($status);
        
        $this->getEntityManager()->flush();
        
        return $ticket;
    }
    
    public function disableTicket($ticket) {
        $ticket->setStatus('D');
        $this->getEntityManager()->flush();
    }
    
    /*Cristiano: verifica se dependente tem limite para comprar com cartão de credito*/
    public function verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $orderValue, $nrorg) {
        if ($paymentMethod != 'CC' || $orderValue == 0) return ;
        
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg]);
        
        if ($dependency == NULL) return ;
        
        $parent  = $dependency->getParent();
        
        if ($parent == NULL) return ;
        
        if ($parent->getMainCreditcard() == NULL) return ;
        
        if ($parent->getMainCreditcard()->getId() != $creditcardId) return ;
        
        $limit       = $dependency->getMonthlyLimit();
        $totalExpent = $this->getMonthExpensesFromDependent($parent->getId(), $userId, $nrorg);
        $canExpend   = $limit - $totalExpent;
        
        if ($canExpend < $orderValue) throw new Exception("User's monthly limit has exceeded! Limit is $limit, current is $totalExpent", 12);
    }
    
    public function getEventTicket($evtEventTicketId) {
        $orderValue = $this->getEntityManager()->getRepository(EvtEventTicket::class)->find($evtEventTicketId);
        
        return $orderValue;
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $cardId = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard()->getId();
        $today  = new \DateTime();
        $firstDayOfMonth = $today->format('Y-m-d');

        $total = $this->getEntityManager()->createQuery(
        "
        SELECT SUM(o.total) as totalValue
        FROM 'Zeedhi\ApiEvents\Model\Entities\OrdOrder' o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createdAt >= '$firstDayOfMonth'
        "
        )->getResult()[0]['totalValue'];
        
        return $total;
    }
    /*Cristiano*/
}
