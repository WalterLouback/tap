<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtTagEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventMenu;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\EvtSellerProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenu;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProductGroup;
use Zeedhi\ApiEvents\Model\Entities\EvtEventTicket;
use Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel;
use Zeedhi\ApiEvents\Model\Entities\GenTag;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\GenCategory;
use Zeedhi\ApiEvents\Model\Entities\GenStructure;
use Zeedhi\ApiEvents\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiEvents\Helpers\ImageUploader;

use Doctrine\ORM\EntityManager;

class Event extends UserOperation {

    public function __construct(EntityManager $entityManager, ImageUploader $imageUploader) {
        parent::__construct($entityManager);
        $this->imageUploader = $imageUploader;
    }
    
    /**
     * getEventsByOrg
     * Filter events by tag, structure and date
     * 
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param int $nrorg
     * @param int $$structure_id
     * 
     */
    public function getEventsByOrg($nrorg, $structures=NULL,$tags=NULL, $date=NULL, $categories=NULL, $userId) {
        $eventTag       = 'Zeedhi\ApiEvents\Model\Entities\EvtTagEvent';
        $event          = 'Zeedhi\ApiEvents\Model\Entities\EvtEvent';
        $evtEventTicket = 'Zeedhi\ApiEvents\Model\Entities\EvtEventTicket';
        
        $dateQuery      = "";
        $categoryQuery  = "";
        $tagQuery       = "";
        $tagSelect      = "";
        $tagGroupBy     = "";
        $joinTag        = "";
        $currentDateOld = new \DateTime();
        $currentDate    = $currentDateOld->format('Y-m-d H:i:s');
        
        $nrorgQuery     = "WHERE e.nrorg = $nrorg AND e.finalDate >= '$currentDate'";/*Cristiano*/
        
        $structQuery    = $structures != NULL ? " AND ( e.genStructure = $structures )" : " ";
        $categoryQuery  = $categories != NULL ? " AND ( e.genCategory = $categories )" : " ";

        if($tags){
            $joinTag    = "JOIN $eventTag etg WITH  etg.evtEvent = e.id JOIN etg.evtTag t "; 
            $tagQuery   = " AND ( etg.evtTag = $tags )";
            $tagSelect  = ", t.id as evtTagId, t.name as evtTagName";
            $tagGroupBy = ", t.id, t.name";
        }
       
        $query  = $joinTag . $nrorgQuery  . $structQuery . $tagQuery . $categoryQuery . " AND e.type = 'E' " . " AND e.status = 'A'";

        $events = $this->getEntityManager()->createQuery("
            SELECT  e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, e.imageLogo, 
                    e.imageCover, e. rating, e.initialDate, e.finalDate, e.about, e.salesMethod, e.values,
                    s.id as genStructureId, s.name as genStructureName, 
                    c.id as genCategoryId, c.name as genCategoryName, 
                    ef.id as favoriteId, em.id as menuId $tagSelect
            FROM  $event e
            LEFT JOIN e.genStructure s
            LEFT JOIN e.genCategory c
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventFavorite' ef WITH e.id = ef.evtEvent AND ef.genUser = $userId
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventMenu' em WITH e.id = em.evtEvent
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventTicket' et WITH e.id = et.evtEvent
            $query
            GROUP BY  e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, 
                      e.imageLogo, e.imageCover, e. rating, e.initialDate, e.finalDate, e.about, e.salesMethod, 
                      s.id, s.name, c.id, c.name, ef.id, em.id $tagGroupBy
            ORDER BY e.initialDate asc")->getResult();
            
        return $events;
    }

    public function getTicketFromEvent($nrorg, $eventId) {
        $evtEventTicket = $this->getEntityManager()->getRepository(EvtEventTicket::class)->findOneBy(['evtEvent' => $eventId, 'nrorg' => $nrorg]);
        
        return $evtEventTicket;
    }
    
    public function getEvtTagEventsByEventId($eventId){
        $evtTagEvents = $this->getEntityManager()->getRepository(EvtTagEvent::class)->findBy(array('evtEvent' => $eventId));
        return $evtTagEvents;
    }
    
    public function getEventsUserHasTickets($userId, $nrorg) {
        $ticketsUser   = 'Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel';

        $eventsUserHasTickets = $this->getEntityManager()->createQuery("
            SELECT     e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, e.imageLogo, e.imageCover, e.initialDate, e.finalDate, e.about, s.id as genStructureId, s.name as genStructureName, c.id as genCategoryId, c.name as genCategoryName
            FROM $ticketsUser tu
            LEFT JOIN tu.evtEventTicket et 
            LEFT JOIN et.evtEvent e 
            JOIN e.genStructure s
            JOIN e.genCategory c
            WHERE tu.genUser = $userId
            AND e.nrorg = $nrorg
            GROUP BY   e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, e.imageLogo, e.imageCover, e.initialDate, e.finalDate, e.about, s.id, s.name, c.id, c.name
            ORDER BY e.initialDate asc
            "
        )->getResult();
        
        return $eventsUserHasTickets;
    }
    
    public function getCategories($nrorg){
        if($nrorg) {
            $categories = $this->getEntityManager()->getRepository(GenCategory::class)->findBy(array('nrorg' => $nrorg));
        } else {
            $categories = $this->getEntityManager()->getRepository(GenCategory::class)->findBy(array('nrorg' => 0));
        }
        return $categories;
    }

    public function favorite($nrorg, $userId){
        $evtEvent    = 'Zeedhi\ApiEvents\Model\Entities\EvtEvent';
        $currentDate = new \DateTime();
        $currentDate->getTimestamp();
        $events = $this->getEntityManager()->createQuery("
            SELECT     e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, e.imageLogo, e.imageCover, e.initialDate, e.finalDate, e.about, e.salesMethod, e.values, c.id as genCategoryId, c.name as genCategoryName, ef.id as favoriteId, et.id as ticketId, em.id as menuId, e.rating
            FROM $evtEvent e
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventFavorite' ef WITH ef.evtEvent = e.id and ef.genUser = $userId
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventTicket' et WITH et.evtEvent = e.id
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtUserTicketRel' etr WITH etr.evtEventTicket = et.id and etr.genUser = $userId
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventMenu' em WITH e.id = em.evtEvent
            
            LEFT JOIN e.genStructure s
            LEFT JOIN e.genCategory c
            WHERE e.nrorg = $nrorg
            AND e.type = 'E'
            AND ( ef.genUser = $userId OR etr.genUser = $userId )
            
            GROUP BY   e.id, e.location, e.type, e.status, e.nrorg, e.name, e.imageMap, e.imageLogo, e.imageCover, e.initialDate, e.finalDate, e.about, e.salesMethod, c.id, c.name, ef.id, et.id, em.id, e.rating
            ORDER BY e.initialDate asc
        ")->getResult();
        return $events;
    }  
    
    public function getEventsWithMenusFromToday($nrorg){    
        $evtEvent   = 'Zeedhi\ApiEvents\Model\Entities\EvtEvent';
        $events = $this->getEntityManager()->createQuery("
            SELECT     e.id, e.name, e.imageCover, e.initialDate, e.finalDate, e.salesMethod
            FROM $evtEvent e
            JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventMenu' ee WITH e.id = ee.evtEvent
            WHERE e.nrorg = $nrorg
            AND e.type = 'E'
            ORDER BY e.initialDate asc
        ")->getResult();
        return $events;
    }
    
    public function getEventsWithByEventSeller($nrorg, $userId){    
        $evtEvent   = 'Zeedhi\ApiEvents\Model\Entities\EvtEvent';
        $events = $this->getEntityManager()->createQuery("
            SELECT e.id, e.name, e.imageCover, e.initialDate, e.finalDate, e.salesMethod, e.status, es.id as sellerId
            FROM $evtEvent e
            JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventMenu' ee WITH e.id = ee.evtEvent
            JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventSeller' es WITH e.id = es.evtEvent
            WHERE e.nrorg = $nrorg
            AND e.status = 'A'
            AND es.genUser = $userId
            ORDER BY e.initialDate asc
        ")->getResult();
        return $events;
    }
    
    public function getEventsAssociatedToUser($nrorg, $userId){    
        $evtEvent   = 'Zeedhi\ApiEvents\Model\Entities\EvtEvent';
        $events = $this->getEntityManager()->createQuery("
            SELECT     e.id, e.name, e.imageCover, e.initialDate, e.finalDate, e.salesMethod, e.status, es.id as sellerId
            FROM $evtEvent e
            LEFT JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventMenu' ee WITH e.id = ee.evtEvent
            JOIN 'Zeedhi\ApiEvents\Model\Entities\EvtEventSeller' es WITH e.id = es.evtEvent
            WHERE e.nrorg = $nrorg
            AND e.type = 'E'
            AND es.genUser = $userId
            ORDER BY e.initialDate asc
        ")->getResult();
        return $events;
    }
    
    public function associateSellerToEvent($userId, $eventId, $adminId) {
        $user  = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $event = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        
        $eventSeller = new EvtEventSeller();
        $eventSeller->setNrorg($event->getNrorg());
        $eventSeller->setGenUser($user);
        $eventSeller->setEvtEvent($event);
        $eventSeller->setCreatedBy($adminId);
        
        $this->getEntityManager()->persist($eventSeller);
        $this->getEntityManager()->flush();
    }
    
    public function unlinkSellerFromEvent($sellerId, $eventId, $adminId) {
        $eventSeller = $this->getEntityManager()->getRepository(EvtEventSeller::class)->find($sellerId);
        
        $eventSeller->setStatus('I');
        $eventSeller->setModifiedBy($adminId);
        
        $this->getEntityManager()->flush();
    }
    
    /* CRISTIANO
     * Este metodo cria eventos
     */
    public function createEvent($userId, $ownerUser = NULL, $type = NULL, $status = NULL, $nrorg = NULL, $name, $imageMap = NULL, $imageLogo = NULL, $imageCover = NULL, $location = NULL, $salesMethod = NULL, $rating = NULL, $classification = NULL, $about = NULL, $initialDate, $finalDate, $categoryId, $structureId) {
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $structure  = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
        $category   = $this->getEntityManager()->getRepository(GenCategory::class)->find($categoryId);
        
        $event = new EvtEvent();
        
        $event->setCreatedBy($userId);
        $event->setOwnerUser($user);
        $event->setType($type);
        $event->setStatus($status);
        $event->setNrorg($nrorg);
        $event->setName($name);
        $event->setImageMap($imageMap);
        // if ($imageLogo) $event->setImageCover($this->imageUploader->upload($imageLogo[0]['b64File']));
        if ($imageCover) $event->setImageCover($this->imageUploader->upload($imageCover[0]['b64File']));
        $event->setInitialDate($initialDate);
        $event->setFinalDate($finalDate);
        $event->setLocation($location);
        $event->setSalesMethod($salesMethod);
        $event->setRating($rating);
        $event->setClassification($classification);
        $event->setAbout($about);
        $event->setStructure($structure);
        $event->setCategory($category);
        
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
        
        return $event;
    }
    
    /* CRISTIANO
     * Este metodo atualiza eventos
     */
    public function updateEvent($event, $userId, $ownerUser = NULL, $type = NULL, $status = NULL, $nrorg = NULL, $name, $imageMap = NULL, $imageLogo = NULL, $imageCover = NULL, $location = NULL, $salesMethod = NULL, $rating = NULL, $classification = NULL, $about = NULL, $initialDate, $finalDate, $categoryId, $structureId) {
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $structure  = $this->getEntityManager()->getRepository(GenStructure::class)->find($structureId);
        $category   = $this->getEntityManager()->getRepository(GenCategory::class)->find($categoryId);
        
        $event->setModifiedBy($userId);
        $event->setType($type);
        $event->setStatus($status);
        $event->setNrorg($nrorg);
        $event->setName($name);
        if ($imageMap) {
            is_array($imageMap) ? $event->setImageMap($this->imageUploader->upload($imageMap[0]['b64File'])) : "";
        }
        if ($imageCover) {
            is_array($imageCover) ? $event->setImageCover($this->imageUploader->upload($imageCover[0]['b64File'])) : "";
        }
        if ($imageLogo) {
            is_array($imageLogo) ? $event->setImageLogo($this->imageUploader->upload($imageLogo[0]['b64File'])) : "";
        }
        $event->setInitialDate($initialDate);
        $event->setFinalDate($finalDate);
        $event->setLocation($location);
        $event->setSalesMethod($salesMethod);
        $event->setRating($rating);
        $event->setClassification($classification);
        $event->setAbout($about);
        $event->setStructure($structure);
        $event->setCategory($category);
        
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
        
        return $event;
    }
    
    /* CRISTIANO
     * Este metodo inativa eventos
     */
    public function inativeEvent($event, $userId) {
        $event->setStatus("I");
        $event->setCreatedBy($userId);
        
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
        
    }
    
    /* CRISTIANO
     * Este metodo busca eventos por id e data inicial
     */
    public function getEventById($eventId) {
        return $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
    }
    
    /* CRISTIANO
     * Este metodo busca eventos por nome e data inicial
     */
    public function getEventOneByName($eventName, $initialDate) {
        return $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['name' => $eventName, 'initialDate' => $initialDate]);
    }
    
    /* CRISTIANO
     * Este metodo busca usuarios admin
     */
    public function getGenUserType($userId){
       $genUser         = 'Zeedhi\ApiEvents\Model\Entities\GenUser';
       $genUserTypeRel  = 'Zeedhi\ApiEvents\Model\Entities\GenUserTypeRel';
       $genUserType     = 'Zeedhi\ApiEvents\Model\Entities\GenUserType';
      
       return $this->getEntityManager()->createQuery("
            SELECT gut.name
            FROM $genUserTypeRel gutr
            INNER JOIN  $genUserType gut WITH gut.id = gutr.genUserType
            WHERE gutr.genUser = $userId AND gut.name = 'Admin'
            ")->getResult();
    }
    
    /* CRISTIANO
     * Este metodo associa um evento a um ou mais tags
     */
    public function associateEventTags($userId, $nrorg, $eventId, $arrayTags, $categoryId, $type) {
        foreach($arrayTags as $tag) {
            // if($tag['TAG_ID'] == null){
            //     $tag['TAG_ID'] = $this->createTag($tag['NAME'], $categoryId, $type, $nrorg);
            // } 
            $evtTagEvent = new EvtTagEvent();
            $evtEvent    = $this->getEventById($eventId);
            $genTag      = $this->getEntityManager()->getRepository(GenTag::class)->findOneBy(['id' => $tag]);
            $evtTagEvent->setCreatedBy($userId);
            $evtTagEvent->setEvtEvent($evtEvent);
            $evtTagEvent->setNrorg($nrorg);
            $evtTagEvent->setEvtTag($genTag);
            $this->getEntityManager()->persist($evtTagEvent);
            $this->getEntityManager()->flush();
        }
    }
    
    public function createTag($tag, $categoryId, $type, $nrorg) {
        $tagExists = $this->getEntityManager()->getRepository(GenTag::class)->findOneBy(['name' => $tag]);
        
        if(!isset($tagExists)){
            $genTag      = new GenTag();
            $genCategory = $this->getEntityManager()->getRepository(GenCategory::class)->find($categoryId);
            
            $genTag->setNrorg($nrorg);
            $genTag->setCategory($genCategory);
            $genTag->setType($type);
            $genTag->setName($tag);
            
            $this->getEntityManager()->persist($genTag);
            $this->getEntityManager()->flush();
            
            return $genTag->getId();
        }
    }
    
    public function removeRegister($register) {
        $this->getEntityManager()->remove($register);
        $this->getEntityManager()->flush();
    }

    public function getEventsFromOrganization($nrorg) {
        $evtEvent     = EvtEvent::class;
        $genCategory  = GenCategory::class;
        $evtEventMenu = EvtEventMenu::class;
        $genStructure = GenStructure::class;
        
        $event =  $this->getEntityManager()->createQuery("
            SELECT  evt.id, om.id as ordMenuId, om.name as ordMenuName, evt.type, evt.status, evt.nrorg, evt.name, evt.imageMap, evt.imageLogo, evt.imageCover, evt.initialDate, evt.finalDate, evt.classification, 
            evt.rating, evt.salesMethod, evt.location, evt.about, genCat.id as categoryId, genCat.name as nameCategory, genStruct.id as structureId, 
            genStruct.name as nameStructure
            FROM $evtEvent evt
            LEFT JOIN  $genCategory genCat WITH evt.genCategory = genCat.id
            LEFT JOIN  $genStructure genStruct WITH evt.genStructure = genStruct.id
            LEFT JOIN  $evtEventMenu evtm WITH evtm.evtEvent = evt.id
            LEFT JOIN  evtm.ordMenu om
            WHERE evt.nrorg = $nrorg AND evt.type = 'E'
            ORDER BY evt.initialDate DESC
        ")->getResult();
        
        return $event;
    }
    
    public function getTagsFromEvent($eventId) {
        $genTag      = GenTag::class;
        $evtTagEvent = EvtTagEvent::class;
        
        $tags =  $this->getEntityManager()->createQuery("
            SELECT genTag.id, genTag.name
            FROM $genTag genTag
            INNER JOIN $evtTagEvent evtTagEvent WITH evtTagEvent.evtTag = genTag.id
            WHERE evtTagEvent.evtEvent = $eventId
        ")->getResult();
        
        return $tags;
    }
    
    public function createEventMenu($nameMenu, $nrorg) {
        $ordMenu      = new OrdMenu();
        
        $ordMenu->setName($nameMenu);
        $ordMenu->setNrorg($nrorg);
        
        $this->getEntityManager()->persist($ordMenu);
        $this->getEntityManager()->flush();
        
        return $ordMenu->getId();
    }
    
    public function changeStatusOfEvent($event, $userId, $status) {
        $event->setStatus($status);
        $event->setModifiedBy($userId);
        $this->getEntityManager()->flush();
        
        return $event->getStatus();
    }
    
    public function createProductGroup($eventId, $ordMenuId, $nrorg, $name) {
        $ordMenuProduct      = new OrdProductGroup();
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        $ordMenu  = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $ordMenuId]);
        
        $ordMenuProduct->setNrorg($nrorg);
        $ordMenuProduct->setName($name);
        $ordMenuProduct->setOrdMenu($ordMenu);
        $ordMenuProduct->setEvtEvent($evtEvent);
        
        $this->getEntityManager()->persist($ordMenuProduct);
        $this->getEntityManager()->flush();
    }
    
    public function associateEventMenu($eventId, $ordMenuId, $nrorg) {
        $evtEventMenu = new EvtEventMenu();
        
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        $ordMenu  = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $ordMenuId]);
        
        $evtEventMenu->setOrdMenu($ordMenu);
        $evtEventMenu->setEvtEvent($evtEvent);
        $evtEventMenu->setNrorg($nrorg);
        
        $this->getEntityManager()->persist($evtEventMenu);
        $this->getEntityManager()->flush();
    }
    
    public function createEventWB($id, $titulo, $descricao, $imagem, $initialDate, $finalDate, $nrorg, $rating, $location, $genCategory, $values) {
        $evtEvent   = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['externalId' => $id, 'nrorg' => $nrorg]);
        
        if(empty($evtEvent)){
            $event = new EvtEvent();
            $event->setExternalId($id);
            $event->setNrorg($nrorg);
            $event->setStatus("A");
            $event->setType("E");
            $event->setName($titulo);
            $event->setImageCover($imagem);
            $event->setInitialDate($initialDate);
            $event->setFinalDate($finalDate);
            $event->setAbout($descricao);
            $event->setCategory($genCategory);
            $event->setSalesMethod("U");
            $event->setRating($rating);
            $event->setLocation($location);
            $event->setValues($values);
           
            $event->setCreatedBy(0);
            $this->getEntityManager()->persist($event);
            $this->getEntityManager()->flush();
            
            return $event;
        }else {
            $evtEvent->setExternalId($id);
            $evtEvent->setNrorg($nrorg);
            $evtEvent->setStatus("A");
            $evtEvent->setType("E");
            $evtEvent->setName($titulo);
            $evtEvent->setImageCover($imagem);
            $evtEvent->setInitialDate($initialDate);
            $evtEvent->setFinalDate($finalDate);
            $evtEvent->setAbout($descricao);
            $evtEvent->setCategory($genCategory);
            $evtEvent->setRating($rating);
            $evtEvent->setLocation($location);
            $evtEvent->setValues($values);

            $evtEvent->setCreatedBy(0);
            $this->getEntityManager()->merge($evtEvent);
            $this->getEntityManager()->flush();
            
            return $evtEvent;
        }
        
    }
    
    public function getCategory($nrorg, $genCategory) {
        $category = $this->getEntityManager()->getRepository(GenCategory::class)->findOneBy(['nrorg' => $nrorg, 'name' => $genCategory]);
        return $category;
    }
    
    public function createCategory($nrorg, $genCategory) {
        $category = new GenCategory();
        
        $category->setNrorg($nrorg);
        $category->setName($genCategory);
        
        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush();
        
        return $category;
    }
    
    public function getEventByNrorg($nrorg) {
        $evtEvent = EvtEvent::class;
        $events = $this->getEntityManager()->getRepository(EvtEvent::class)->findBy(['nrorg' => $nrorg]);
        
        return EvtEvent::manyToArray($events);
    }
}
?>