<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Service\Order as OrderService;


use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\PayGatewayRel;
use Zeedhi\ApiEvents\Model\Entities\PayWallet;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;

const EVENT_ID        = 'EVENT_ID';

use Doctrine\ORM\EntityManager;

use Zeedhi\ApiEvents\Helpers\Environment as Environment;

class Wallet extends UserOperation{
    
    /** @var OrderService */
    private $orderService;
    
    public function __construct(EntityManager $entityManager, OrderService $orderService) {
        parent::__construct($entityManager);
        $this->orderService = $orderService;
    }
    
    /**
     * getBalance
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param int $nrorg
     * @param int $userProfile
     *
     */
    public function getBalance($nrorg, $userId) {
        $filter = array('genUser' => $userId, 'nrorg' => $nrorg);
        return $this->getEntityManager()->getRepository(PayWallet::class)->findOneBy($filter);
    
    }

    /**
     * @param integer  $nrorg
     * @param integer $userProfile
     * @param float $cardId
     *
     * @return BpOrder
     */
    public function createOrderWallet($userProfile, $selectCardId, $buyingValue, $eventId, $nrorg) {
        $order = $this->orderService->createOrderWallet($userProfile, $selectCardId, $buyingValue, $eventId, $nrorg);
        return $order;
    }
    public function createOrderWalletFromGUID($selectCardId, $buyingValue, $identifier, $eventId, $nrorg) {
        $order = $this->orderService->createOrderWalletFromGUID($selectCardId, $buyingValue, $identifier, $eventId, $nrorg);
        return $order;
    }
    public function updateAll() {
        $this->getEntityManager()->flush();
    } 
    public function factoryOrderDataSet($order) {
        return $this->orderService->factoryOrderDataSet($order);
    }
    public function createWallet($genUser){
        $wallet = new PayWallet();
        $wallet->setId(PayWallet::getGUID());
        $wallet->setGenUser($this->getEntityManager()->getReference(GenUser::class, $genUser));
        $wallet->setBalance(0);
        $this->getEntityManager()->persist($wallet);
        $this->getEntityManager()->flush();
        return $wallet;
    }
    
    public function getMerchantKey($nrorg) {
        return PayGatewayRel::getMerchantKeyByOrganization($nrorg, $this->getEntityManager());
    }


}
