<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\OrdCashier;
use Zeedhi\ApiEvents\Model\Entities\OrdCashierMovement;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod;

use Zeedhi\Framework\ORM\DateTime;

use Doctrine\ORM\EntityManager;

class Cashier {
    
    private $entityManager;
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function startCashier($eventId, $nrorg, $eventSellerId, $initialValue, $movementDate) {
        $eventSeller = $this->entityManager->getRepository(EvtEventSeller::class)->find($eventSellerId);
        
        $cashier = new OrdCashier();
        $cashier->setOpenning(new \DateTime($movementDate));
        $cashier->setNrorg($nrorg);
        $cashier->setEventSeller($eventSeller);
        
        $cashierMovement = new OrdCashierMovement();
        $cashierMovement->setMovementDate(new \DateTime($movementDate));
        $cashierMovement->setValue($initialValue);
        $cashierMovement->setOrdCashier($cashier);
        $cashierMovement->setNrorg($nrorg);
        
        $this->entityManager->persist($cashier);
        $this->entityManager->persist($cashierMovement);
        
        $this->entityManager->flush();
        
        return $cashier;
    }
    
    public function finishCashier($eventId, $nrorg, $cashierId, $finalDate) {
        $cashier = $this->entityManager->getRepository(OrdCashier::class)->find($cashierId);
        
        $cashier->setClosing(new \DateTime($finalDate));
        
        $this->entityManager->flush();
        
        return $cashier;
    }
    
    public function cashierMovement($cashierId, $eventId, $nrorg, $value, $movementDate) {
        $cashier = $this->entityManager->getRepository(OrdCashier::class)->find($cashierId);
        
        $cashierMovement = new OrdCashierMovement();
        $cashierMovement->setMovementDate(new \DateTime($movementDate));
        $cashierMovement->setValue($value);
        $cashierMovement->setOrdCashier($cashier);
        $cashierMovement->setNrorg($nrorg);
        $this->entityManager->persist($cashierMovement);
        
        $this->entityManager->flush();
        
        return $cashierMovement;
    }
    
    public function getCashiersReport($date, $eventId, $nrorg, $eventSellerId) {
        $cashier = OrdCashier::class;
        $event = EvtEvent::class;
        $eventSeller = EvtEventSeller::class;
        $movement = OrdCashierMovement::class;
        $order = OrdOrder::class;
        
        $dateQuery = '';
        if ($date != NULL) {
            $initialDate = $this->stringToDate($date) . ' 00:00:00';
            $finalDate   = $this->stringToDate($date) . ' 23:59:59';
            $dateQuery   = " AND c.openning > '$initialDate' AND c.openning < '$finalDate'";
        }
        
        $eventQuery = $eventId != NULL ? " AND e.id = $eventId" : "";
        $eventSellerQuery = $eventSellerId != NULL ? " AND es.id = $eventSellerId" : "";
        
        $cashiers = $this->entityManager->createQuery(
            "
            SELECT c.id, c.openning, c.closing, e.id as eventId, e.name as eventName, u.firstName as userName, SUM(o.total) as totalValue
            FROM $cashier c
            LEFT JOIN $eventSeller es WITH es = c.eventSeller
            LEFT JOIN es.genUser u
            LEFT JOIN es.evtEvent e
            LEFT JOIN $order o WITH o.ordCashier = c
            WHERE c.nrorg = $nrorg
            $dateQuery
            $eventQuery
            $eventSellerQuery
            GROUP BY c.id
            ORDER BY c.id DESC
            "
        )->getResult();
        
        return $cashiers;
    }
    
    public function getCashierMovements($cashierId) {
        $movement = OrdCashierMovement::class;
        
        $movements = $this->entityManager->createQuery(
            "
            SELECT m.id, m.movementDate, m.value
            FROM $movement m
            INNER JOIN m.ordCashier c
            WHERE c.id = $cashierId
            "
        )->getResult();
        
        return $movements;
    }
    
    public function getCashierOrdersReport($cashierId) {
        $menuProduct = OrdMenuProduct::class;
        $orderProduct = OrdOrderProduct::class;

        $productsReport = $this->entityManager->createQuery(
            "
            SELECT p.id, p.name, SUM(op.quantity) as amount, SUM(op.total) as total
            FROM $menuProduct p
            INNER JOIN $orderProduct op WITH op.ordMenuProduct = p
            INNER JOIN op.ordOrder o
            WHERE o.ordCashier = $cashierId
            GROUP BY p.id
            "
        )->getResult();
        
        return $productsReport;
    }
    
    public function getCashierReceiptsReport($cashierId) {
        $menuProduct = OrdMenuProduct::class;
        $orderProduct = OrdOrderProduct::class;
        $paymentMethod = PayPaymentMethod::class;

        $report = $this->entityManager->createQuery(
            "
            SELECT pm.id, pm.label, COUNT(o.id) as amount, SUM(o.total) as total
            FROM $menuProduct p
            INNER JOIN $orderProduct op WITH op.ordMenuProduct = p
            INNER JOIN op.ordOrder o
            INNER JOIN $paymentMethod pm WITH pm.id = o.paymentMethod
            WHERE o.ordCashier = $cashierId
            GROUP BY pm.id
            "
        )->getResult();
        
        return $report;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
}