<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventMenu;
use Zeedhi\ApiEvents\Model\Entities\OrdMenu;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProductGroup;

use Zeedhi\ApiGeneral\Helpers\Environment;

const EVENT_ID        = 'EVENT_ID';

use Doctrine\ORM\EntityManager;

class MenuProduct extends UserOperation{
    
    
    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager);
    }
    
    public function getProductsFromOrganizationThatAreNotInEvent($nrorg, $eventId) {
        /* Getting products from organization that are not in event */
        $products = $this->getEntityManager()->createQuery(
            "
            SELECT p
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdProduct' p
            WHERE p.id not in ( 
                SELECT p2.id
                    FROM 'Zeedhi\ApiEvents\Model\Entities\OrdProduct' p2
                    JOIN Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct mp WITH mp.ordProduct = p2
                    JOIN mp.ordMenu as m
                    JOIN Zeedhi\ApiEvents\Model\Entities\EvtEventMenu em WITH em.ordMenu = m
                    JOIN em.evtEvent as e
                WHERE e.id = $eventId
            )
            AND p.nrorg = $nrorg
            OR p.nrorg = 0
            "
        )->getResult();
        
        return $products;
    }
    
    public function getMenuProductsFromEvent($eventId) {
        $menuProducts = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct' mp
            JOIN Zeedhi\ApiEvents\Model\Entities\OrdProductGroup pg WITH pg.id = mp.ordProductGroup
            WHERE pg.evtEvent = $eventId
            "
        )->getResult();
        
        return $menuProducts;
    }
    
    public function getMenuProductsFromProductGroup($productGroupId) {
        $menuProducts = $this->getEntityManager()->createQuery(
            "
            SELECT mp
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct' mp
            JOIN Zeedhi\ApiEvents\Model\Entities\OrdProductGroup pg WITH pg.id = mp.ordProductGroup
            WHERE pg.id = $productGroupId
            "
        )->getResult();
        
        return $menuProducts;
    }
    
    public function removeProductFromEvent($menuProductId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setOrdMenu(null);
        $menuProduct->setOrdProductGroup(null);
        $this->getEntityManager()->flush();
    }

    
    public function getProductGroupsFromEvent($eventId) {
        $productGroups = $this->getEntityManager()->createQuery(
            "
            SELECT pg.id, pg.name
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdProductGroup' pg
            WHERE pg.evtEvent = $eventId
            "
        )->getResult();
        
        return $productGroups;
    }    
    
    public function getProductByName($name) {

        $products = $this->getEntityManager()->createQuery(
            "
            SELECT pg
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdProduct' pg
            WHERE pg.name = '$name'
            "
        )->getResult();
        
        if (count($products) > 0) return $products[0];
        return null;
    }
    
    public function createMenu($nrorg, $eventId) {
        /* Busca evento */
        $event = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        
        /* Criar e persistir Menu */
        $menu = new OrdMenu();
        $menu->setName($event->getName());
        $menu->setNrorg($nrorg);
        $this->getEntityManager()->persist($menu);
        
        
        /* Criar e persistir associativa entre Loja e Cardápio */
        $associativa = new EvtEventMenu();
        $associativa->setEvtEvent($event);
        $associativa->setOrdMenu($menu);
        $this->getEntityManager()->persist($associativa);

        $this->getEntityManager()->flush();
        
        return $menu;
    }
    
    public function linkProductToStore($productId, $storeId, $productGroupId, $menuId, $amount=0) {
        $product = $this->getEntityManager()->getRepository(OrdProduct::class)->find($productId);
        $store   = $this->getEntityManager()->getRepository(EvtEvent::class)->find($storeId);
        
        if ($productGroupId !== NULL) {
            $productGroup   = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $productGroupId = $productGroup->getId();
        } else if ($menuId == NULL) {
            /* Se cardápio não for informado, criar um novo*/
            $newMenuName = 'Cardápio sem nome';
            $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
            $menuId = $menu->getId();
        }
        
        $menuProduct = $this->createMenuProduct($product, 'A', 0, $menuId, $productGroupId, $amount);
        
        $this->getEntityManager()->flush();
        return $menuProduct;
    }
 

    public function createProduct($name, $detail, $price, $nrorg) {
        /* Criar produto */
        $product = new OrdProduct();
        $product->setName($name);
        $product->setDetail($detail);
        $product->setPrice($price);
        $product->setNrorg($nrorg);
        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();
        return $product;
        
        // /* Associar a cardápio e categoria */
        // if ($productGroupId !== NULL) {
        //     $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        //     $menu         = $productGroup->getOrdMenu();
        // } else if ($menuId == NULL) {
        //     /* Se cardápio não for informado, criar um novo*/
        //     $newMenuName = 'Cardápio sem nome';
        //     $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
        //     $menuId = $menu->getId();
        // }
        // $this->createMenuProduct($product, $status, $estimatedTime, $menuId, $productGroupId);
        
        // /* Inserir extras no produto */
        // foreach ($extras as $extra) {
        //     $this->createExtra($extra['NAME'], $extra['REQUIRED'], $extra['MULTIPLE'], $extra['OPTIONS'], $menuProduct->getId(), false);
        // }
        
        // /* Commit */
        // if ($canFlush) $this->getEntityManager()->flush();
        
        // $product->build($this->getEntityManager());
        
    }
    
    public function createMenuProduct($product, $status, $estimatedTime, $menuId, $productGroupId, $amount, $canFlush=true) {
        $menu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($menuId);
        $menuProduct = new OrdMenuProduct();
        $menuProduct->setOrdMenu($menu);
        $menuProduct->setOrdProduct($product);
        $menuProduct->setPrice($product->getPrice());
        $menuProduct->setName($product->getName());
        $menuProduct->setDetail($product->getDetail());
        $menuProduct->setNrorg($product->getNrorg());
        $menuProduct->setImage($product->getImage());
        $menuProduct->setEstimatedTime($estimatedTime);
        $menuProduct->setStatus($status);
        $menuProduct->setAmount($amount);
        if ($productGroupId != NULL) $menuProduct->setOrdProductGroup($this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId));
        
        $this->getEntityManager()->persist($menuProduct);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $menuProduct;
    }
        
    public function createEventMenu($nameMenu, $nrorg) {
        $ordMenu      = new OrdMenu();
        
        $ordMenu->setName($nameMenu);
        $ordMenu->setNrorg($nrorg);
        
        $this->getEntityManager()->persist($ordMenu);
        $this->getEntityManager()->flush();
        
        return $ordMenu->getId();
    }
    
    public function createProductGroup($eventId, $ordMenuId, $nrorg, $name, $userId = null) {
        $ordMenuProduct      = new OrdProductGroup();
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        $ordMenu  = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $ordMenuId]);
        
        $ordMenuProduct->setNrorg($nrorg);
        $ordMenuProduct->setName($name);
        $ordMenuProduct->seTCreatedBy($userId);
        $ordMenuProduct->setOrdMenu($ordMenu);
        $ordMenuProduct->setEvtEvent($evtEvent);
        
        $this->getEntityManager()->persist($ordMenuProduct);
        $this->getEntityManager()->flush();
        
        return $ordMenuProduct->getId();
    }
    
    public function updateProductGroup($eventId, $ordMenuId, $nrorg, $name, $userId, $ordMenuProduct) {
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        $ordMenu  = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $ordMenuId]);
        
        $ordMenuProduct->setNrorg($nrorg);
        $ordMenuProduct->setName($name);
        $ordMenuProduct->seTCreatedBy($userId);
        $ordMenuProduct->setOrdMenu($ordMenu);
        $ordMenuProduct->setEvtEvent($evtEvent);
        
        $this->getEntityManager()->flush();
        
        return $ordMenuProduct->getId();
    }

    public function associateEventMenu($eventId, $ordMenuId, $nrorg) {
        $evtEventMenu = new EvtEventMenu();
        
        $evtEvent = $this->getEntityManager()->getRepository(EvtEvent::class)->findOneBy(['id' => $eventId]);
        $ordMenu  = $this->getEntityManager()->getRepository(OrdMenu::class)->findOneBy(['id' => $ordMenuId]);
        
        $evtEventMenu->setOrdMenu($ordMenu);
        $evtEventMenu->setEvtEvent($evtEvent);
        $evtEventMenu->setNrorg($nrorg);
        
        $this->getEntityManager()->persist($evtEventMenu);
        $this->getEntityManager()->flush();
    }
    
    public function getEvtEventMenuByEventId($eventId) {
        $evtEventMenu = $this->getEntityManager()->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $eventId]);
        return $evtEventMenu;
    }

    public function getProductGroupById($productGroupId) {
        $ordProductGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->findOneBy(['id' => $productGroupId]);
        return $ordProductGroup;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function createExtra($name, $required, $multiple, $options, $menuProductId, $canFlush=true) {
        /* Buscando produto para relacionar ao extra */
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        
        /* Criando o extra e definindo atributos */
        $extra = new OrdExtra();
        $extra->setName($name);
        $extra->setRequired($required);
        $extra->setMultiple($multiple);
        $extra->setOrdMenuProduct($menuProduct);
        
        $this->getEntityManager()->persist($extra);
        $this->getEntityManager()->flush();
        
        /* Criando options do Extra */
        foreach($options as $option) {
            $this->createOption($option['NAME'], $option['PRICE'], $extra->getId(), false);
        }
        
        /* Commit */
        $this->getEntityManager()->flush();
        
        return $extra;
    }
    
    public function createOption($name, $price, $extraId, $canFlush=true) {
        /* Buscar extra para relacionar à opção */
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        
        /* Criar opção */
        $option = new OrdOption();
        $option->setName($name);
        $option->setPrice($price);
        $option->setOrdExtra($extra);
        
        $this->getEntityManager()->persist($option);
        
        /* Commit */
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $option;
    }
    
    public function cloneProductGroupsFromMenu($sourceMenuId, $targetMenuId, $storeId) {
        $sourceMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($sourceMenuId);
        $targetMenu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($targetMenuId);
        
        $sourceMenu->build($this->getEntityManager(), $storeId);
        
        $sourceMenu->cloneProductGroups($targetMenu, $this->getEntityManager());
        $this->getEntityManager()->flush();
    }
    
    public function removeProductFromGroup($menuProductId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setOrdProductGroup(null);
        $this->getEntityManager()->flush();
    }
    
    public function removeProductFromStore($menuProductId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $menuProduct->setOrdMenu(null);
        $menuProduct->setOrdProductGroup(null);
        $this->getEntityManager()->flush();
    }

    public function removeMenuFromStore($menuId, $storeId) {
        $evtEventMenu = $this->getEntityManager()->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $storeId, 'ordMenu' => $menuId]);
        $evtEventMenu->setOrdMenu(null);
        
        $this->getEntityManager()->flush();
    }
    
    public function removeExtraFromProduct($extraId) {
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        $extra->setOrdProduct(null);
        
        $this->getEntityManager()->flush();
    }
    
    public function removeProductGroup($productGroupId) {
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        $productGroup->build($this->getEntityManager());
        $productGroup->setEvtEvent(null);
        $productGroup->setOrdMenu(null);
        
        $menuProducts = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->findBy(['ordProductGroup' => $productGroupId]);
        foreach ($menuProducts as $menuProduct) {
            $menuProduct->setOrdProductGroup(null);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function removeOption($optionId) {
        $option = $this->getEntityManager()->getRepository(OrdOption::class)->find($optionId);
        $option->setOrdExtra(null);
        $this->getEntityManager()->flush();
    }
    
    public function linkProductToGroup($productId, $productGroupId) {
        $product = $this->getEntityManager()->getRepository(OrdProduct::class)->find($productId);
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        
        $menuProduct = $this->createMenuProduct($product, 'A', 0, $productGroup->getOrdMenu(), $productGroup->getId());
        
        $this->getEntityManager()->flush();
    }
    
    public function linkMenuProductToGroup($menuProductId, $productGroupId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
        
        $menuProduct = $menuProduct->getClone($this->getEntityManager(), $productGroup);
        
        $this->getEntityManager()->flush();
    }
    
    public function linkMenuProductToStore($menuProductId, $storeId, $productGroupId, $menuId) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        
        if ($productGroupId !== NULL) {
            $productGroup   = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $productGroupId = $productGroup->getId();
        } else if ($menuId == NULL) {
            /* Se cardápio não for informado, criar um novo*/
            $newMenuName = 'Cardápio sem nome';
            $menu   = $this->createMenu($newMenuName, $nrorg, [], $storeId, []);
            $menuId = $menu->getId();
        }
        
        $menuProduct = $menuProduct->getClone($this->getEntityManager(), $productGroup);
        
        $this->getEntityManager()->flush();
        return $menuProduct;
    }
    
    public function updateMenu($menuId, $name, $workshifts, $storeId) {
        $menu = $this->getEntityManager()->getRepository(OrdMenu::class)->find($menuId);
        
        $menu->setName($name);
        
        $evtEventMenu = $this->getEntityManager()->getRepository(EvtEventMenu::class)->findOneBy(['evtEvent' => $storeId, 'ordMenu' => $menuId]);
        
        if ($workshifts != NULL) {
            $this->cleanMenuWorkshifts($evtEventMenu);
            
            /* Criar turnos e relacionar ao Menu */
            foreach ($workshifts as $shift) {
                $day         = $shift['DAY'];
                $initialTime = new \DateTime($shift['INITIAL_TIME']);
                $finalTime   = new \DateTime($shift['FINAL_TIME']);
                
                $this->createWorkshift($day, $initialTime, $finalTime, 'MENU', $storeId, $evtEventMenu);
            }
        }
        
        $this->getEntityManager()->flush();
    }
    
    private function cleanMenuWorkshifts($evtEventMenu) {
        $workshifts = $this->getEntityManager()->getRepository(GenShift::class)->findBy(['evtEventMenu' => $evtEventMenu]);
        
        foreach ($workshifts as $shift) {
            $this->getEntityManager()->remove($shift);
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function updateExtra($extraId, $name, $required, $multiple, $options) {
        $extra = $this->getEntityManager()->getRepository(OrdExtra::class)->find($extraId);
        
        $extra->setName($name);
        $extra->setRequired($required);
        $extra->setMultiple($multiple);
        if (count($options) > 0) {
            $this->cleanOptionsFromExtra($extra);
            foreach($options as $option) {
                $this->createOption($option['NAME'], $option['PRICE'], $extra->getId(), false);
            }
        }
        
        $this->getEntityManager()->flush();
    }
    
    public function cleanOptionsFromExtra($extra) {
        $extra->build($this->getEntityManager());
        foreach ($extra->getOptions() as $option) {
            $option->setOrdExtra(NULL);
        }
        
        $this->getEntityManager()->flush();
    }

    public function updateProduct($menuProductId, $name, $detail, $price, $nrorg, $estimatedTime, $menuId, $productGroupId, $status, $amount) {
        $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($menuProductId);
        if ($name) $menuProduct->setName($name);
        if ($detail) $menuProduct->setDetail($detail);
        if ($price) $menuProduct->setPrice($price);
        if ($nrorg) $menuProduct->setNrorg($nrorg);
        if ($status) $menuProduct->setStatus($status);
        if ($estimatedTime) $menuProduct->setEstimatedTime($estimatedTime);
        if ($amount) $menuProduct->setAmount($amount);
        
        if ($productGroupId && ($menuProduct->getOrdProductGroup() == NULL || $productGroupId != $menuProduct->getOrdProductGroup()->getId())) {
            $productGroup = $this->getEntityManager()->getRepository(OrdProductGroup::class)->find($productGroupId);
            $menuProduct->setOrdProductGroup($productGroup);
            $menuProduct->setOrdMenu($productGroup->getOrdMenu());
        }
        
        $this->getEntityManager()->flush();
        return $menuProduct;
    }
    
    function uploadImage($image) {
        $url = 'https://api.imgur.com/3/image';
        $data = array('image' => $image);
        
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }
        
        var_dump($result); die;
    }

}   