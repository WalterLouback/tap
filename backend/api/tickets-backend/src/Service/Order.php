<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;
use Zeedhi\ApiEvents\Model\Entities\PayWallet;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdCashier;
use Zeedhi\ApiEvents\Model\Entities\OrdMenuProduct;
use Zeedhi\ApiGeneral\Model\Entities\GenDependent;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\EvtEventTicket;
use Zeedhi\ApiEvents\Model\Entities\GenStatus;
use Zeedhi\ApiEvents\Model\Entities\GenStructure;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Service\Notification as NotificationService;
use Zeedhi\ApiEvents\Helpers\FCM;

use Zeedhi\ApiEvents\Service\Exception  as Exception;
use Zeedhi\Framework\DataSource\DataSet;

use Zeedhi\ApiGeneral\Helpers\Environment as Environment;

use Doctrine\ORM\EntityManager;

class Order extends UserOperation {
    
    /**
     * __construct
     * 
     * @param EntityManager         $entityManager
     * @param Environment           $environment
     */
    public function __construct(EntityManager $entityManager, $paymentApiCC, $paymentApiBalance, NotificationService $notification) {
        parent::__construct($entityManager);
        $this->paymentApiCC = $paymentApiCC;
        $this->paymentApiBalance = $paymentApiBalance;
        $this->notification = $notification;
    }
    
    public function getMonthExpensesFromDependent($parentId, $dependentId, $nrorg) {
        $cardId = $this->getEntityManager()->getRepository(GenUser::class)->find($parentId)->getMainCreditcard()->getId();
        $today  = new \DateTime();
        $firstDayOfMonth = $today->format('Y-m-d');

        $total = $this->getEntityManager()->createQuery(
        "
        SELECT SUM(o.total) as totalValue
        FROM 'Zeedhi\ApiEvents\Model\Entities\OrdOrder' o
        WHERE o.payCreditcard = $cardId
        AND ( o.paymentMethod = 'CC' or o.paymentMethod = 'CC_W' )
        AND o.genUser = $dependentId
        AND o.nrorg = $nrorg
        AND o.paymentStatus = 'A'
        AND o.createdAt >= '$firstDayOfMonth'
        "
        )->getResult()[0]['totalValue'];
        
        return $total;
    }
    
    public function verifyIfCanPurchase($userId, $paymentMethod, $creditcardId, $orderValue, $nrorg) {
        if ($paymentMethod != 'CC' || $orderValue == 0) return ;
        
        $user       = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $dependency = $this->getEntityManager()->getRepository(GenDependent::class)->findOneBy(['dependent' => $userId, 'nrorg' => $nrorg]);
        
        if ($dependency == NULL) return ;
        
        $parent  = $dependency->getParent();
        
        if ($parent == NULL) return ;
        
        if ($parent->getMainCreditcard() == NULL) return ;
        
        if ($parent->getMainCreditcard()->getId() != $creditcardId) return ;
        
        $limit       = $dependency->getMonthlyLimit();
        $totalExpent = $this->getMonthExpensesFromDependent($parent->getId(), $userId, $nrorg);
        $canExpend   = $limit - $totalExpent;

        if ($canExpend < $orderValue) throw new Exception("User's monthly limit has exceeded! Limit is $limit, current is $totalExpent", 12);
    }

    /**
     * ordProDuct
     * 
     * @param array  $products
     * @param int $eventId
     * @param int $selectCardId
     *
     * @return OrDorder
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException    
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function createOrder($creditCardId, $eventId, $userId, $orderItems) {
        $event = $this->getEntityManager()->find(EvtEvent::class, $eventId);
        if ($event == NULL) throw new Exception('Invalid Order', 122);
        
        $order = new Ordorder();
        $order->setId(OrdOrder::getGUID());
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        $order->setType('TICKET');
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CREDITCARD);
        $order->setPayCreditcard($this->getEntityManager()->find(PayCreditCard::class, $creditCardId));
        $order->setEvtEvent($event);
        $order->setNrorg($event->getNrorg());
        $this->getEntityManager()->persist($order);

        $orderTotal = 0;
        
        foreach($orderItems as $key=>$orderItem){
            $evtEventTicket  = $this->getEntityManager()->find(EvtEventTicket::class, $orderItem['EVENT_TICKET_ID']);
            if(empty($evtEventTicket)) throw new \Exception('Ticket not found');
            
            $ordOrderProduct[$key] = new OrdOrderProduct();
            $ordOrderProduct[$key]->setOrdOrder($order);
            $ordOrderProduct[$key]->setEvtEventTicket($evtEventTicket);
            $ordOrderProduct[$key]->setQuantity(count($orderItem['USERS']));
            $ordOrderProduct[$key]->setTotal($evtEventTicket->getPrice() * count($orderItem['USERS']));

            $this->getEntityManager()->persist($ordOrderProduct[$key]);
            $orderTotal += $ordOrderProduct[$key]->getTotal();
        }

        $order->setTotal($orderTotal);
        $this->getEntityManager()->flush();

        return $order;
    }

    /* Creates new Order with the passed data */
    public function createOrderEvent($paymentMethod, $statusId, $total, $userId, $eventId, $nrorg, $creditcardId, $cashierId, $creationDate, $canFlush=true) {
        /* Getting necessary entities to set to the order */
        $user           = $this->getEntityManager()->getRepository(GenUser::class)->find($userId);
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        if ($cashierId !== NULL) $cashier = $this->getEntityManager()->getRepository(OrdCashier::class)->find($cashierId);
        else $cashier   = NULL;
        
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setStatus($status);
        $order->setPaymentStatus('N');
        $order->setTotal($total);
        $order->setGenUser($user);
        $order->setType('ORDER_EVENT');
        $order->setEvtEvent($event);
        $order->setNrorg($nrorg);
        $order->setOrdCashier($cashier);
        if ($creationDate !== NULL) $order->setCreateDate(new \DateTime($creationDate));

        if ($creditcardId !== NULL) {
            $creditcard     = $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId);
            $order->setPayCreditcard($creditcard);
        }
        
        /* Storing order in database */
        $this->getEntityManager()->persist($order);
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;
    }
    
    /**
     * @param integer  $selectCardId
     * @param integer $buyingValue
     *
     * @return Ordorder
     */
    public function createOrderWallet($userId, $selectCardId, $buyingValue, $eventId, $nrorg){
        /* Verify if the user can make this order */
        $this->verifyIfCanPurchase($userId, 'CC', $selectCardId, $buyingValue, $nrorg);
        
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CREDITCARD_FOR_WALLET);
        $order->setType('WALLET');
        $order->setNrorg($nrorg);
        $order->setTotal($buyingValue);
        $order->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPayCreditcard($this->getEntityManager()->find(PayCreditcard::class, $selectCardId));
        $order->setPaymentStatus(OrdOrder::STATUS_PAYMENT_PENDING);
        
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
        
        return $order;
    }
    
    public function createOrderWalletFromGUID($walletId, $buyingValue, $identifier, $eventId, $nrorg){
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod(Ordorder::PAYMENT_METHOD_CASH_FOR_WALLET);
        $order->setType('WALLET');
        $order->setNrorg($nrorg);
        $order->setTotal($buyingValue);
        // $order->setEvtEvent($this->getEntityManager()->getReference(EvtEvent::class, $eventId));
        $order->setPaymentStatus(OrdOrder::STATUS_PAID);
        
        $this->getEntityManager()->persist($order);
        
        $wallet = $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId);
        
        if ($wallet ==  NULL) {
            $wallet = new PayWallet();
            $wallet->setId($walletId);
            $wallet->setBalance(0);
            $wallet->setNrorg($nrorg);
            $wallet->setIdentifier($identifier);
            $this->getEntityManager()->persist($wallet);
            //throw new \Exception('Wallet not found', 1423);
        }

        $wallet->setBalance($wallet->getBalance() + $buyingValue);

        $this->getEntityManager()->flush();
        
        return $order;
    }
    
    public function factoryOrderDataSet($order) {
        return array('ORDER' => array(
            "ID" => $order->getId(),
            "TYPE" => $order->getType(),
            "NRORG" => $order->getNrorg(),
            "TOTAL" => $order->getTotal(),
            "STATUS" => $order->getStatus(),
            "GEN_USER_ID" => $order->getGenUser() != NULL ? $order->getGenUser()->getId() : NULL,
            "CREATE_DATE" => $order->getCreatedAt(),
            // "EVENT_ID" => $order->getEvtEvent()->getId(),
            // "EVENT_NAME" => $order->getEvtEvent()->getName(),
            "PAYMENT_METHOD" => $order->getPaymentMethod(),
            "PAYMENT_STATUS" => $order->getPaymentStatus(),
            "TRANSACTION_ID" => $order->getTransactionId(),
            "CREDIT_CARD_ID" => $order->getPayCreditcard() != NULL ? $order->getPayCreditcard()->getId() : NULL
        ));
    }
    
    public function getOrderById($orderId){
        $filter = array('id' => $orderId);
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->findOneBy($filter);
        if (empty($order)) throw Exception::orderNotFound();
        $order->build($this->getEntityManager());
        return $order;
    }
    
    public function getOrderProductByOrder($orderId){
        $filter = array('ordOrder' => $orderId);
        return $this->getEntityManager()->getRepository(OrdOrderProduct::class)->findBy($filter);
    }
    
    public function getOrdersByUser($userId, $paymentMethod=null, $nrorg, $eventId=null, $initialDate=null, $finalDate=null){
        $dataInicial = $initialDate == null ? new \DateTime() : $initialDate;
        $dataFinal   = $finalDate == null ? new \DateTime() : $finalDate;
        $dataInicial = $initialDate == null ? $dataInicial->format('Y-m-01') : $dataInicial;
        $dataFinal = $finalDate == null ? $dataFinal->format('Y-m-31') : $finalDate;
        $paymentMethodQuery = '';
        
        if($paymentMethod != NULL) {
            $paymentMethodQuery   = $paymentMethod == 'BALANCE' ? "AND (o.paymentMethod = 'BALANCE' OR o.paymentMethod = 'CC_W')" : "AND ( o.paymentMethod = 'CC' OR o.paymentMethod = 'CC_W' ) ";
        }
        
        $dateQuery   = " AND o.createdAt >= '$dataInicial' AND o.createdAt <= '$dataFinal'";
        $eventQuery  = $eventId != NULL ? " AND o.evtEvent = $eventId" : "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM 'Zeedhi\ApiEvents\Model\Entities\OrdOrder' o
            WHERE o.genUser = $userId
            AND (o.paymentStatus = 'A' OR o.paymentStatus = 'R')
            AND o.nrorg = $nrorg
            $eventQuery
            $paymentMethodQuery
            $dateQuery
            ORDER BY o.createdAt
            DESC
            "
        )->getResult();

        return $orders;
    }
    
    public function getOrdersWithItems($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate){
        $orders = $this->getOrdersByUser($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate);
        $orderFactory = [];
        foreach ($orders as $key=>$order) {
            $order->build($this->getEntityManager());
            $orderFactory[$key] = $order->toArray();
        }
        return $orderFactory;
    }
    
    public function updateAll(){
        $this->getEntityManager()->flush();
    }

    /* Creates ProductOrders with the received items */
    public function createProductOrders($order, $orderItems, $canFlush=true) {
        $productOrders = [];
        /* Iterating throug the items sent by the front-end */
        foreach($orderItems as $item) {
            /* Getting the product referent to the item */
            $menuProduct = $this->getEntityManager()->getRepository(OrdMenuProduct::class)->find($item['productId']);
            
            /* Instantiating OrderProduct and setting attributes */
            $po = new OrdOrderProduct();
            $po->setQuantity($item['quant']);
            $po->setTotal($menuProduct->getPrice() * $item['quant']);
            $po->setOrdMenuProduct($menuProduct);
            $po->setOrdOrder($order);
            
            /* Storing item in the database */
            $this->getEntityManager()->persist($po);
            
            array_push($productOrders, $po);
        }
        
        if ($canFlush) $this->getEntityManager()->flush();
        
        // $order->build($this->getEntityManager());
        
        return $productOrders;
    }
    
    /* Gets an user's orders in an organization */
    function getOrdersFromUser($userId, $structureId, $nrorg, $initial=NULL, $max=NULL) {
        $order   = 'Zeedhi\ApiEvents\Model\Entities\OrdOrder';
        $nrorgQuery = $nrorg !== NULL ? "AND o.nrorg = $nrorg" :  "";
        
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent e
            JOIN o.genUser u
            WHERE u = $userId
            AND e.type = 'E'
            $nrorgQuery
            ORDER BY o.createdAt desc
            "
        );
        
        if ($initial !== NULL) $orders->setFirstResult($initial);
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        return $orders;
    }
    
    /* Checks how to procede with the payment of an order and after that changes its status */
    function makePaymentTransaction($order, $paymentMethod, $creditCardId=NULL, $nrorg, $walletId=NULL) {
        $user       = $order->getGenUser();
        $value      = $order->getTotal();
        $eventName  = $order->getEvtEvent()->getName();
        $message    = "Compra realizada no evento $eventName no valor de R$ $value";
        
        if ($paymentMethod == 'CC') {
            $this->makePaymentTransactionWithCC($order, $paymentMethod, $creditCardId);
            $this->removeAmountFromOrderItems($order);
        } else if ($paymentMethod == 'BALANCE') {
            $this->makePaymentTransactionWithBalance($order, $paymentMethod, $nrorg, $walletId);
            $this->removeAmountFromOrderItems($order);
        }   else {
            // throw  new \Exception('Payment method invalid', 0);
        }
        $this->getEntityManager()->flush();
        
        $notificationData = NULL;
        
        $title = "Sobre o seu pedido";
        $originNotification = "Order_Eventsbackend";
        $status             = "P";
        $dateTime           = new \DateTime();
        
        // $this->notification->inserirNotification($status, $message, $order->getNrorg(), $user->getId(), $originNotification, $dateTime, $notificationData, $title, $user->getFirebaseToken());
    }
    
    public function removeAmountFromOrderItems($order) {
        $order->setItems($this->getEntityManager());
        $items = $order->getItems();
        foreach ($items as $item) {
            $menuProduct = $item->getOrdMenuProduct();
            $menuProduct->setAmount($menuProduct->getAmount() - $item->getQuantity());
        }
        $this->getEntityManager()->flush();
    }
    
    public function makePaymentTransactionWithCC($order, $paymentMethod, $creditCardId=NULL) {
        /* Send the order data to the payment API */
        $creditCard = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditCardId) : NULL;
        $orderId    = $order->getId();
        $value      = $order->getTotal();
        $ccToken    = $creditCard->getToken();
        $customerId = $creditCard->getCustomerId();
        $gateway    = $creditCard->getGateway();
        $order->getEvtEvent()->setMerchantKey($this->getEntityManager());
        $merchant   = $order->getEvtEvent()->getMerchantKey();
        $payment    = $this->paymentApiCC->execPaymentCreditCard($value, $ccToken, $customerId, $orderId, $gateway, $merchant);
        
        /* Getting data from the payment */
        $status     = $payment['status'];
        $order->setTransactionId($payment['transaction_id']);
        
        /* Set the payment status */
        $order->setPaymentStatus($status == 'captured' ? 'A' : 'N');
        
        /* If the payment was not successful */
        if ($status !== 'captured') {
            $this->getEntityManager()->flush();
            throw new Exception('Payment not successful. Got status \'' . $status . '\'');
        }
    }
    
    public function makePaymentTransactionWithBalance($order, $paymentMethod, $nrorg, $walletId=null) {
        $userId     = $order->getGenUser() !== NULL ? $order->getGenUser()->getId() : NULL;
        $walletId   = $order->getPayWallet() !== NULL ? $order->getPayWallet()->getId() : NULL;
        $value      = $order->getTotal();
        
        if($order->getPayWallet() != NULL && $order->getPayWallet()->getGenUser()->getId() != $userId)
            throw new Exception('Wallet does not belong to the user', 66);
        $this->paymentApiBalance->execPaymentBalance($value, $userId, $nrorg, $walletId);
        
        $order->setPaymentStatus('A');
    }
    
    public function createOrderEventWithWalletAndEventSeller($orderItems, $totalValue, $evtEventSellerId, $eventId, $nrorg, $paymentMethod, $creditCardId, $walletId, $userId, $statusId, $orderType, $canFlush=true) {
        /* Getting necessary entities to set to the order */
        $event          = $this->getEntityManager()->getRepository(EvtEvent::class)->find($eventId);
        $status         = $this->getEntityManager()->getRepository(GenStatus::class)->find($statusId);
        $user           = $userId !== NULL ? $this->getEntityManager()->getRepository(GenUser::class)->find($userId) : NULL;
        $wallet         = $walletId !== NULL ? $this->getEntityManager()->getRepository(PayWallet::class)->find($walletId) : NULL;
        $creditcard     = $creditCardId !== NULL ? $this->getEntityManager()->getRepository(PayCreditcard::class)->find($creditcardId) : NULL;
        $evtEventSeller = $evtEventSellerId !== NULL ? $this->getEntityManager()->getRepository(EvtEventSeller::class)->find($evtEventSellerId) : NULL;
        /* Instantiating Order and setting attributes */
        $order = new OrdOrder();
        $order->setId(OrdOrder::getGUID());
        $order->setPaymentMethod($paymentMethod);
        $order->setStatus($status);
        $order->setPaymentStatus('N');
        $order->setTotal($totalValue);
        $order->setGenUser($user);
        $order->setType($orderType);
        $order->setEvtEvent($event);
        $order->setNrorg($nrorg);
        $order->setPayCreditcard($creditcard);
        $order->setEvtEventSeller($evtEventSeller);
        $order->setPayWallet($wallet);
        /* Storing order in database */
        $this->getEntityManager()->persist($order);

        if ($canFlush) $this->getEntityManager()->flush();
        
        return $order;    
    }
    
    public function updateOrderPaymentStatus($orderId, $paymentStatus) {
        $order = $this->getEntityManager()->getRepository(OrdOrder::class)->find($orderId);
        
        $order->setPaymentStatus($paymentStatus);
        if ($paymentStatus == 'A') $this->removeAmountFromOrderItems($order);
        
        $this->getEntityManager()->flush();
    }
    
    public function getEventsReport($userId, $nrorg, $eventId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods) {
        $order          = 'Zeedhi\ApiEvents\Model\Entities\OrdOrderRO';
        
        $initialDateFormat = $this->stringToDate($initialDate);
        $finalDateFormat = $this->stringToDate($finalDate);
        
        if (!is_null($orderStatus))
            $statusOrder = join($orderStatus, ' OR st.id = ');
        $statusQuery = $orderStatus !== NULL ? "AND (st.id = $statusOrder)" : "";
        
        if (!is_null($paymentStatus))
            $statusPayment = "'". join($paymentStatus, "' OR o.paymentStatus = '") . "'";
        $paymentStatusQuery = $paymentStatus !== NULL ? "AND (o.paymentStatus = $statusPayment)" : ""; 
        
        if (!is_null($paymentMethods))
            $paymentMethodsJoin = "'". join($paymentMethods, "' OR o.paymentMethod like '") . "'";
        $paymentMethodsQuery = $paymentMethods !== NULL ? "AND (o.paymentMethod like $paymentMethodsJoin)" : ""; 
        
        if (!is_null($eventId))
            $eventIds = join($eventId, ' OR e.id = ');
        $eventQuery = $eventId !== NULL ? "AND (e.id = $eventIds)" : "";     
        
        if (!is_null($structureId))
            $structureIds = join($structureId, ' OR sr.id = ');
        $structureQuery = $structureId !== NULL ? "AND (sr.id = $structureIds)" : "";  
           
        $deliverToQuery = $deliverTo !== NULL ? "AND (o.deliverTo = '$deliverTo')" : "";
        
        $initialDateQuery = $initialDate !== NULL ? "AND o.createdAt >= '$initialDateFormat 00:00:00'" : "";
        $finalDateQuery   = $finalDate !== NULL ? "AND o.createdAt <= '$finalDateFormat 23:59:59'" : "";
            
        $orders = $this->getEntityManager()->createQuery(
            "
            SELECT o
            FROM $order o
            JOIN o.evtEvent e
            LEFT JOIN o.status st
            LEFT JOIN o.genStructure sr
            WHERE o.nrorg = $nrorg
            AND e.type = 'E'
            $structureQuery
            $eventQuery
            $initialDateQuery
            $finalDateQuery
            $statusQuery
            $paymentStatusQuery
            $deliverToQuery
            $paymentMethodsQuery
            ORDER BY o.orderIdentifier desc
            "
        ); 
        
        if ($max !== NULL) $orders->setMaxResults($max);
        
        $orders = new \Doctrine\ORM\Tools\Pagination\Paginator($orders);
        
        foreach ($orders as $order) {
            $order->build($this->getEntityManager());
        }
        
        return $orders;
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date->format('Y-m-d');
    }
    
}