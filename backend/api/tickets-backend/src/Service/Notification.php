<?php
namespace Zeedhi\ApiEvents\Service;

use Zeedhi\ApiEvents\Model\Entities\GenNotification;
use Zeedhi\ApiEvents\Model\Entities\GenUser;

use Zeedhi\ApiEvents\Helpers\FCM;

use Doctrine\ORM\EntityManager;

class Notification extends UserOperation {
    
    public function __construct(EntityManager $entityManager, Environment $environment) {
        parent::__construct($entityManager, $environment);
    }
    
    public function getNotification($idNotification, $userId) {
        $notification = $this->getEntityManager()->getRepository(GenNotification::class)->findOneBy(['id' => $idNotification, 'genuser' => $userId]);
        
        return $notification;
    }
    
    public function getNotifications($userId) {
        $notification = $this->getEntityManager()->getRepository(GenNotification::class)->findBy(['genuser' => $userId]);
        
        return $notification;
    }
    
    public function inserirNotification($status, $message, $nrorg, $userId, $originNotification, $dateTime, $notificationData, $title, $firebaseToken = null) {
        $notification = new GenNotification();
        
        if($status)
            $notification->setStatus($status);
        if($message)
            $notification->setMessage($message);
        if($nrorg)
            $notification->setNrorg($nrorg);
        if($userId)
            $notification->setGenUser($this->getEntityManager()->getReference(GenUser::class, $userId));
        if($originNotification)
            $notification->setOriginNotification($originNotification);
        if($dateTime)
            $notification->setDateTime($dateTime);
        
        $this->getEntityManager()->persist($notification);
        $this->getEntityManager()->flush();
        
        $fcm = new FCM($nrorg, $this->getEntityManager());
        
        if($notificationData != NULL) {
            $fcm->sendNotificationToApp($firebaseToken, $title, $message, $notificationData);
        }else {
            $fcm->sendNotificationToApp($firebaseToken, $title, $message, null);
        }
        
        return $notification;
    }

}