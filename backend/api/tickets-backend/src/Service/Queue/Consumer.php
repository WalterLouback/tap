<?php

namespace Zeedhi\ApiEvents\Service\Queue;

use Zeedhi\PaymentPlatform\Customer;

use Doctrine\ORM\EntityManager;

use Zeedhi\Queue\Consumer as ConsumerBase;
use Zeedhi\Queue\Connection;

use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyPlatform;
use Zeedhi\PaymentPlatform\InstantBuy\InstantBuyTransaction as PlatformInstantBuyTransaction;

use Zeedhi\ApiEvents\Model\Entities\BpOrder;
use Zeedhi\ApiEvents\Model\Entities\BpWallet;
use Zeedhi\ApiEvents\Service\Environment;
use Zeedhi\ApiEvents\Service\Exception as BipException;

class Consumer extends ConsumerBase {

    /** $dir String Directory of file script passed by caller */
    public $scriptDirectory;
    /** @var EntityManager */
    private $entityManager;
    /** @var PaymentPlatform */
    private $paymentPlatform;
    /** @var Environment */
    private $environment;
    /** @var String Directory of PHP */
    private $DIR_PHP;
    
    public function __construct(Connection $connection, EntityManager $entityManager,
            InstantBuyPlatform $paymentPlatform, Environment $environment, $queueName, $DIR_PHP) {
        static::$queueName = $queueName;
        parent::__construct($connection);
        $this->entityManager = $entityManager;
        $this->paymentPlatform = $paymentPlatform;
        $this->environment = $environment;
        $this->DIR_PHP = $DIR_PHP;
    }
    
    /**
     * makePaymentTransaction
     * 
     * @param BpOrder $bpOrder
     * @return string
     */
       private function makePaymentTransaction(BpOrder $bpOrder) {
                $bpOrder->setOrderReference("order-ref-".$bpOrder->getId());
                $customer = new Customer();
                $customer->setId('cus_3Z4D3jqCYgC9Aa6k');
                $token = 'card_b4MLBnqcDTnvP7ke';
                $transaction = new PlatformInstantBuyTransaction(
                    $bpOrder->getTotal(),
                    $bpOrder->getOrderReference(),
                    // $bpOrder->getCreditcard()->getToken(),
                    $token,
                    $customer
                );
                return $this->paymentPlatform->instantBuyTransaction($transaction);
        }
    
    /**
     * makePaymentTransactionWithBalance
     * 
     * @param BpOrder $bpOrder
     */
    private function makePaymentTransactionWithBalance(BpOrder $bpOrder){
        $wallet = $this->entityManager->getRepository(BpWallet::class)->findOneBy(array(
            "user" => $bpOrder->getUser()->getId()
        ));
        
        if($wallet->getBalance() >= $bpOrder->getTotal() ){
            $wallet->setBalance($wallet->getBalance() - $bpOrder->getTotal());
        }else{
            throw BipException::balanceInsufficient();
        }
    }
    
    /**
     * updateOrderStatus
     * 
     * @param BpOrder $bpOrder
     * @param string  $orderKey
     */
    private function updateOrderStatus(BpOrder $bpOrder, $orderKey = null) {
        if(isset($orderKey)){
            $bpOrder->setOrderKey($orderKey);
        }
        $bpOrder->setStatus(BpOrder::STATUS_PAID);
        $bpOrder->setQrcode(uniqid($bpOrder->getId(), true));
        $this->entityManager->flush();
    }
    
    /**
     * @param BpOrder $bpOrder
     * @param string  $orderKey
     */
    private function updateOrderStatusForWallet(BpOrder $bpOrder, $orderKey) {
        $bpOrder->setOrderKey($orderKey);
        $bpOrder->setStatus(BpOrder::STATUS_CREDITED);
        $this->entityManager->flush();
    }
    
    /**
    * @param integer  $buyingValue
    */
    private function updateUserBalance(BpOrder $bpOrder) {
        /** @var BpWallet $wallet */
        $wallet = $this->entityManager->getRepository(BpWallet::class)->findOneBy(array(
            "user" => $bpOrder->getUser()->getId()
        ));
        
        $wallet->setBalance($wallet->getBalance() + $bpOrder->getTotal());
        
        $this->entityManager->flush();
    }

    /**
     * consumeByCreditCard
     * 
     * @param BpOrder $bpOrder
     */
    private function consumeByCreditCard(BpOrder $bpOrder){
        $orderKey = $this->makePaymentTransaction($bpOrder);
        $this->updateOrderStatus($bpOrder, $orderKey);
    }

    /**
     * consumeByBalance
     * 
     * @param BpOrder $bpOrder
     */
    private function consumeByBalance(BpOrder $bpOrder){
        $this->makePaymentTransactionWithBalance($bpOrder);
        $this->updateOrderStatus($bpOrder);
    }
    
    /**
     * consumeForWallet
     * 
     * @param BpOrder $bpOrder
     */
    private function consumeForWallet(BpOrder $bpOrder){
        $orderKey = $this->makePaymentTransaction($bpOrder);
        $this->updateOrderStatusForWallet($bpOrder, $orderKey);
        $this->updateUserBalance($bpOrder);
    }

    /**
     * log
     * 
     * $param String $message
     */
    private function log($message){
        echo $message, "\n";
    }

    /**
     * consume
     * 
     * @param String $message BpOrder ID
     * 
     * @return void
     */
    public function consume($message){
        $this->log("\nStart task {$message}");
        
        $console = shell_exec("cd {$this->scriptDirectory} && {$this->DIR_PHP} scripts/orderPaymentScript.php {$message}");
        
        $this->log($console);
        $this->log("Processed task {$message}");
        $this->log("-------------------------");
    }
    
    /**
     * exec
     * 
     * @param String $orderId
     */
    public function exec($orderId){
        try {
            $start = microtime(true);
            /** @var BpOrder $bpOrder */
            $bpOrder = $this->entityManager->find(BpOrder::class, $orderId);
            
            if(!empty($bpOrder)){
                if ($bpOrder->getPaymentMethod() == BpOrder::PAYMENT_METHOD_CREDITCARD){
                    $this->consumeByCreditCard($bpOrder);
                    $this->log("Consumed by creditcard");
                }else if($bpOrder->getPaymentMethod() == BpOrder::PAYMENT_METHOD_BALANCE){
                    $this->consumeByBalance($bpOrder);
                    $this->log("Consumed by balance");
                }else if($bpOrder->getPaymentMethod() == BpOrder::PAYMENT_METHOD_CREDITCARD_FOR_WALLET){
                    $this->consumeForWallet($bpOrder);
                    $this->log("Consumed for wallet");
                }    
            }
            $end = microtime(true);
            $time = $end-$start;
            $this->log($time."msec");
        }catch (\Exception $e){
            echo $e->getMessage();
        }
    }
}