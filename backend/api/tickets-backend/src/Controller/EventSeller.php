<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\EventSeller as EventSellerService;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;


class EventSeller extends Crud {
    
    public function __construct(Manager $dataSourceManager,  EventSellerService $eventSellerService) {
        parent::__construct($dataSourceManager);
        $this->eventSellerService = $eventSellerService;
    }
    
    public function getOrdProductMenuOnVisible(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        $products = $this->eventSellerService->getOrdProductMenuOnVisible($row['NRORG'], $row['EVENT_ID'], $row['SELLER_ID']);
        
        $response->addDataSet(new DataSet('products', $products));
    }
    
    /* CRISTIANO
     * Este metodo associa um vendedor a um evento
     */
    public function createEvtEventSeller(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $evtEventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $userId = $row['USER_ID'];
        $createdBy = $row['USER_ID'];
        $email = $row['EMAIL'];

        $evtEvent = $this->eventSellerService->getEvtEvent($evtEventId);
        $user = $this->eventSellerService->getGenUserByEmail($email);
        if(empty($user))
            throw new Exception('User not found!', 1);
            
        $evtEventSeller = $this->eventSellerService->getEvtEventSeller($user->getId(), $evtEventId);
        
        if(!empty($evtEventSeller))
            throw new Exception('Seller already registered!', 2);
            
        $evtEventSeller = $this->eventSellerService->createEvtEventSeller($evtEvent, $nrorg, $user, $createdBy);
        
        $sellerData['name']     = $user->getFirstName();
        $sellerData['sellerId'] = $evtEventSeller->getId();
        
        $dataSet    = new DataSet('sellerData', $sellerData);
        
        $response->addDataSet($dataSet);
    }
    
    /* CRISTIANO
     * Este metodo busca vendedores de um evento
     */
    public function getEventSellersByEventId(DTO\Request\Row $request, DTO\Response $response) {
        $row        = $request->getRow();
        
        $evtEventId = $row['EVT_EVENT_ID'];
        $nrorg = $row['NRORG'];
        $userId = $row['USER_ID'];
        
        $users = $this->eventSellerService->getEventSellersByEventId($evtEventId);
        
        foreach($users as $key => $user) {
            $users[$key]['contacts'] = $this->eventSellerService->getUserContact($user['id']);
        }
        
        $response->addDataSet(new DataSet('eventSellers', [$users]));
    }
    
    /* CRISTIANO
     * Este metodo associa um evento a um ou mas menus
     */
    public function associateMenuProductEventSeller(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $userId = $row['USER_ID'];
		$nrorg   = $row['NRORG'];
		$eventSellerId = $row['EVENT_SELLER_ID'];
		$arrayOrdMenus   = $row['MENU_PRODUCT_IDS'];
        
        $this->eventSellerService->associateMenuProductEventSeller($userId, $nrorg, $eventSellerId, $arrayOrdMenus);
        
        $response->addDataSet(new DataSet('response', ['status' => 'Ok']));
    }
    
    public function getSellerDataFromEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $userId = $row['USER_ID'];
		$nrorg   = $row['NRORG'];
		$eventId = $row['EVENT_ID'];
        
		$eventSellers = $this->eventSellerService->getSellerDataFromEvent($eventId);

        $dataSet = new DataSet('eventSellers', $eventSellers);
		$response->addDataSet($dataSet);
    }
    
    public function getProductsSellerNotSell(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $eventSellerId = $row['EVENT_SELLER_ID'];
        $eventId       = $row['EVENT_ID'];
        
        $productsSellerNotSell = $this->eventSellerService->getProductsSellerNotSell($eventSellerId, $eventId);
        
		$response->addDataSet(new DataSet('productsSellerNotSell', $productsSellerNotSell));
    }
    
    public function disableProductMenu(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        
        $sellerProductIds = $row['SELLER_PRODUCT_IDS'];
        foreach ($sellerProductIds as $sellerProductId) {
            $this->eventSellerService->disableProductMenu($sellerProductId);
        }
        
        $response->addDataSet(new DataSet('response', ['status' => 'ok']));
    }
    
    public function getMenuProductsFromSellerBySellerId(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $menuProducts = $this->eventSellerService->getMenuProductsFromSellerBySellerId($row['SELLER_ID']);
        $dataSet = new DataSet('menuProducts', $menuProducts);
		$response->addDataSet($dataSet);
    }    
}

?>