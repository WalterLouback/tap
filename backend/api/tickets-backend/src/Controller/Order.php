<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Order as OrderService;
use Zeedhi\ApiEvents\Service\Exception as Exception;

use Zeedhi\ApiEvents\Model\Entities\OrdOption;
use Zeedhi\ApiEvents\Model\Entities\OrdProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderProduct;
use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\OrdOrderRO;
use Zeedhi\ApiEvents\Model\Entities\OrdItemExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdExtra;
use Zeedhi\ApiEvents\Model\Entities\OrdSelectedOption;
use Zeedhi\ApiEvents\Model\Entities\EvtEvent;
use Zeedhi\ApiEvents\Model\Entities\EvtEventSeller;
use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;
use Zeedhi\ApiEvents\Model\Entities\GenUser;
use Zeedhi\ApiEvents\Model\Entities\GenUserTypeRel;
use Zeedhi\ApiEvents\Model\Entities\GenStatus;
use Zeedhi\ApiEvents\Model\Entities\GenOrganization;
use Zeedhi\ApiEvents\Model\Entities\GenStructure;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;
use Doctrine\ORM\EntityManager;

class Order {
    
    /** @var orderService */
    protected $orderService;
    
    const USER_ID           = 'USER_ID';
    const ORDER_ID           = 'ORDER_ID';
    const ORDER_ITEMS        = 'ORDER_ITEMS';
    const ORDER_STATUS       = 'ORDER_STATUS';
    
    /**
     * __contruct
     *
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService, EntityManager $entityManager) {
        $this->orderService  = $orderService;
        $this->entityManager = $entityManager;
    }
    
    public function createOrderEventWithWalletAndEventSeller(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        // var_dump($row); die;
        $orderItems       = $row[OrdOrder::ORDER_ITEMS];
        $orderType        = $row[OrdOrder::ORDER_TYPE];
        $totalValue       = $row[OrdOrder::TOTAL_VALUE];
        $evtEventSellerId = isset($row[OrdOrder::EVT_EVENT_SELLER_ID]) ? $row[OrdOrder::EVT_EVENT_SELLER_ID] : NULL;
        $eventId          = $row[EvtEvent::EVENT_ID];
        $nrorg            = $row[GenOrganization::NRORG];
        $paymentMethod    = $row[OrdOrder::PAYMENT_METHOD];
        $creditCardId     = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $walletId         = isset($row[OrdOrder::WALLET_ID]) ? $row[OrdOrder::WALLET_ID] :  NULL;
        $userId           = isset($row[OrdOrder::COSTUMER_ID]) ? $row[OrdOrder::COSTUMER_ID] : NULL;
        $statusId         = GenStatus::PEDIDO_ENTREGUE;
        
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderEventWithWalletAndEventSeller($orderItems, $totalValue, $evtEventSellerId, $eventId, $nrorg, $paymentMethod, $creditCardId, $walletId, $userId, $statusId, $orderType, true);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true);
        /* Making financial transaction */ 
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, $walletId);
        /* Sending response */
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
    
    /**
     * Apagar a função e usar a nova função createOrderEventWithWalletAndEventSeller
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function finishOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        /* Getting the request's parameters */
        $orderItems       = $row[OrdOrder::ORDER_ITEMS];
        $totalValue       = $row[OrdOrder::TOTAL_VALUE];
        $userId           = $row[GenUser::USER_ID];
        $storeId          = $row[EvtEvent::EVENT_ID];
        $nrorg            = $row[GenOrganization::NRORG];
        $paymentMethod    = $row[OrdOrder::PAYMENT_METHOD];
        $creditCardId     = isset($row[PayCreditcard::CREDIT_CARD_ID]) ? $row[PayCreditcard::CREDIT_CARD_ID] : NULL;
        $status           = GenStatus::PEDIDO_NOVO;
        /* Instantiating the order without flushing */
        $order = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, $userId, $storeId, $nrorg, $creditCardId, false);
        /* Instantiating the product orders and flushing */
        $this->orderService->createProductOrders($order, $orderItems, true);
        /* Making financial transaction */ 
        $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, null);
        /* Sending response */
        $response->addDataSet(new DataSet('order', $order->toArray()));
    }
    
    function finishMultipleOrders(DTO\Request\Row $request, DTO\Response $response) {
        $row    = $request->getRow();
        $orders = $row['ORDERS'];
        
        foreach ($orders as $index=>$order) {
            /* Getting the request's parameters */
            $orderItems       = $order[OrdOrder::ORDER_ITEMS];
            $totalValue       = $order[OrdOrder::TOTAL_VALUE];
            $userId           = $order[GenUser::USER_ID];
            $storeId          = $order[EvtEvent::EVENT_ID];
            $nrorg            = $order[GenOrganization::NRORG];
            $paymentMethod    = $order[OrdOrder::PAYMENT_METHOD];
            $paymentStatus    = $order['PAYMENT_STATUS'];
            $cashierId        = isset($order['CASHIER_ID']) ? $order['CASHIER_ID'] : NULL;
            $evtEventSellerId = isset($order['SELLER_USER_ID']) ? $order['SELLER_USER_ID'] : NULL;
            $creditCardId     = isset($order[PayCreditcard::CREDIT_CARD_ID]) ? $order[PayCreditcard::CREDIT_CARD_ID] : NULL;
            $creationDate     = isset($order['ORDER_TIME']) ? $order['ORDER_TIME'] : NULL;
            $status           = GenStatus::PEDIDO_NOVO;
            /* Instantiating the order without flushing */
            $order = $this->orderService->createOrderEvent($paymentMethod, $status, $totalValue, $userId, $storeId, $nrorg, $creditCardId, $cashierId, $creationDate, false);
            /* Instantiating the product orders and flushing */
            $this->orderService->createProductOrders($order, $orderItems, true);
            /* Making financial transaction */ 
            $this->orderService->makePaymentTransaction($order, $paymentMethod, $creditCardId, $nrorg, null);
            
            $order = $this->orderService->getOrderById($order->getId());
            /* Sending response */
            $response->addDataSet(new DataSet($index, $order->toArray()));
        }
        
        
    }
     
     /**
     * getOrdersFromUser
     *
     * @param DTO\Request     $request
     * @param DTO\Response    $response
     *
     * @throws Exception
     */
    function getOrdersFromUser(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        
        /* Checks if the parameters are valid */
        $expectedParameters = [GenUser::USER_ID];
        if (!$this->checkParameters($row, $expectedParameters)) {
            throw Exception::invalidParametersException('getOrdersFromUser', $expectedParameters);
        }
        
        /* Get the parameters from the request */
        $userId        = $row[GenUser::USER_ID];
        $nrorg         = isset($row[GenOrganization::NRORG])     ? $row[GenOrganization::NRORG] : NULL;
        $structureId   = isset($row[GenStructure::STRUCTURE_ID]) ? $row[GenStructure::STRUCTURE_ID] : NULL;
        $initial       = isset($row['FIRST_RESULT'])             ? $row['FIRST_RESULT']   : NULL;
        $max           = isset($row['MAX_RESULTS'])              ? $row['MAX_RESULTS']   : NULL;
        
        /* Get orders */
        $orders      = $this->orderService->getOrdersFromUser($userId, $structureId, $nrorg, $initial, $max);
        
        $response->addDataSet(new DataSet('orders', OrdOrder::manyToArray($orders)));
    }

    public function checkoutOrder(DTO\Request\Row $request, DTO\Response $response) {
        $row     = $request->getRow();
        $orderId = $row[OrdOrder::ORDER_ID];
        $order   = $this->orderService->getOrderById($orderId);
        
        if ($order->getPaymentStatus() !== 'A')
            $this->orderService->makePaymentTransaction($order, 'BALANCE', NULL, $order->getNrorg(), null);
            // throw  new \Exception('Payment status invalid', 0);    
        if ($order->getType() !== 'ORDER_EVENT')
            throw  new \Exception('Invalid order', 1);    
        if ($order->getStatus()->getId() == GenStatus::PEDIDO_ENTREGUE)
            throw  new \Exception('Order delivered', 2);    
        
        $order->setStatus($this->entityManager->getReference(GenStatus::class, GenStatus::PEDIDO_ENTREGUE));
        $this->orderService->updateAll();    
        $orderProducts = $this->orderService->getOrderProductByOrder($order->getId());
        $products      = $this->factoryOrderProducts($orderProducts);
        $orderFactory  = $order->toArray();
        $response->addDataSet(new DataSet('order', $orderFactory));
    }
    
    public function getOrdersWithItems(DTO\Request\Row $request, DTO\Response $response){
        $row           = $request->getRow();

        $userId        = $row[OrdOrder::USER_ID];
        $nrorg         = $row[OrdOrder::NRORG];
        $paymentMethod = isset($row[OrdOrder::PAYMENT_METHOD]) ? $row[OrdOrder::PAYMENT_METHOD] : NULL;
        $eventId       = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $initialDate   = isset($row[OrdOrder::INITIAL_DATE]) ? $row[OrdOrder::INITIAL_DATE] : NULL;
        $finalDate     = isset($row[OrdOrder::FINAL_DATE]) ? $row[OrdOrder::FINAL_DATE] : NULL;
        
        $orders        = $this->orderService->getOrdersWithItems($userId, $paymentMethod, $nrorg, $eventId, $initialDate, $finalDate);
        
        $response->addDataSet(new DataSet('orders', $orders));
    }
    
    public function factoryOrderProducts($orderProducts){
        foreach($orderProducts as $key=>$orderProduct){
            $product[$key]['id'] = $orderProduct->getOrdMenuProduct()->getId();
            $product[$key]['name'] = $orderProduct->getOrdMenuProduct()->getName();
            $product[$key]['price'] = $orderProduct->getOrdMenuProduct()->getPrice();
            $product[$key]['detail'] = $orderProduct->getOrdMenuProduct()->getDetail();
            $product[$key]['category'] = $orderProduct->getOrdMenuProduct()->getOrdProductGroup()->getName();
        }
        return $product;
    }
    
    /**
      * checkParameters
      * 
      * @param DTO\Request\Row $row
      * @param Array<String>   $params
      * 
      */
    public static function checkParameters($row, $params) {
        foreach($params as $param) {
            if (!isset($row[$param])) 
                return false;
        }
        return true;
    }
    
    function getEventsReport(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
       
        /* Get the parameters from the request */
        $userId         = $row['USER_ID'];
        $nrorg          = isset($row['NRORG'])          ? $row['NRORG'] : NULL;
        $structureId    = isset($row['STRUCTURE_ID']) && $row['STRUCTURE_ID'] != '' && count($row['STRUCTURE_ID']) > 0  ? $row['STRUCTURE_ID'] : NULL;
        $storeId        = isset($row['STORE_ID']) && $row['STORE_ID'] != '' && count($row['STORE_ID']) > 0 ? $row['STORE_ID'] : NULL;
        $max            = isset($row['MAX_RESULTS'])    ? $row['MAX_RESULTS']   : NULL;
        $initialDate    = isset($row['INITIAL_DATE']) && $row['INITIAL_DATE'] != ''  ? $row['INITIAL_DATE']     : NULL;
        $finalDate      = isset($row['FINAL_DATE']) && $row['FINAL_DATE'] != '' ? $row['FINAL_DATE']   : NULL;
        $orderStatus    = isset($row['ORDER_STATUS']) && $row['ORDER_STATUS'] != '' && count($row['ORDER_STATUS']) > 0 ? $row['ORDER_STATUS']   : NULL;
        $deliverTo      = isset($row['DELIVER_TO'])  && $row['DELIVER_TO'] != '' ? $row['DELIVER_TO']   : NULL;
        $paymentStatus  = isset($row['PAYMENT_STATUS']) && $row['PAYMENT_STATUS'] != '' && count($row['PAYMENT_STATUS']) > 0 ? $row['PAYMENT_STATUS']   : NULL;
        $paymentMethods = isset($row['PAYMENT_METHOD'])  && $row['PAYMENT_METHOD'] != '' ? $row['PAYMENT_METHOD']   : NULL;
        $statusIds      = NULL;

        /* Get orders */
        $orders         = $this->orderService->getEventsReport($userId, $nrorg, $storeId, $max, $initialDate, $finalDate, $orderStatus, $deliverTo, $paymentStatus, $structureId, $paymentMethods);
        $orderFactory   = OrdOrderRO::manyToArrayReport($orders);
        $response->addDataSet(new DataSet('orders', $orderFactory));
    }
    
    function updateOrdersPaymentStatus(DTO\Request\Row $request, DTO\Response $response) {
        $row           = $request->getRow();
        $orderIds      = $row['ORDER_IDS'];
        $paymentStatus = $row['PAYMENT_STATUS'];
        
        foreach ($orderIds as $orderId) {
            $this->orderService->updateOrderPaymentStatus($orderId, $paymentStatus);
        }
    }
    
}