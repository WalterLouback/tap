<?php
namespace Zeedhi\ApiEvents\Controller;

class Exception extends \Exception {

    const INVALID_LOGIN_PARAMETERS = 1;
    const MISSING_PARAMETERS = 2;
    const NO_ONE_LOGGED = 3;

    public static function invalidLoginParameters() {
        return new static('Invalid login parameters!', self::INVALID_LOGIN_PARAMETERS);
    }
    
    public static function missingParameters() {
        return new static("It is missing parameters!", self::MISSING_PARAMETERS);
    }
    
    public static function noOneLogged() {
        return new static('There is no one logged!', self::NO_ONE_LOGGED);
    }
}