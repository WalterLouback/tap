<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Favorite as FavoriteService;


use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Helpers\Environment;


class Favorite {

    public function __construct(FavoriteService $favoriteService) {
        $this->favoriteService = $favoriteService;
    }
    
    public function createFavoriteEvent(DTO\Request\Row $request, DTO\Response $response){
        $row      = $request->getRow();
        $nrorg    = $row['NRORG'];   
        $eventId  = $row['EVENT_ID'];
        $userId   = $row['USER_ID'];
        
        $favorite = $this->favoriteService->createFavoriteEvent($nrorg, $eventId, $userId);
        $response->addDataSet(new DataSet('createFavoriteEvent', array('status' => 'OK')));
    }
    
    public function deleteFavoriteEvent(DTO\Request\Row $request, DTO\Response $response){
        $row = $request->getRow();
        $nrorg   = $row['NRORG'];
        $eventId = $row['EVENT_ID'];
        $userId  = $row['USER_ID'];
        
        $favorite = $this->favoriteService->deleteFavoriteEvent($nrorg, $eventId, $userId);
        
        $response->addDataSet(new DataSet('deleteFavoriteEvent', array('status' => 'OK')));
        
    }
}