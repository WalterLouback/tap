<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Service\Cashier as CashierService;

class Cashier {
    
    private $cashierService;
    
    public function __construct(CashierService $cashierService) {
        $this->cashierService = $cashierService;
    }
    
    public function startCashier(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $initialValue = $row['INITIAL_VALUE'];
        $eventSellerId = $row['EVENT_SELLER_ID'];
        $movementDate = $row['INITIAL_TIME'];
        
        $cashier = $this->cashierService->startCashier($eventId, $nrorg, $eventSellerId, $initialValue, $movementDate);
        
        $response->addDataSet(new DataSet('cashier', $cashier->toArray()));
    }
    
    public function finishCashier(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $cashierId = $row['CASHIER_ID'];
        $finalDate = $row['FINAL_TIME'];

        $cashier = $this->cashierService->finishCashier($eventId, $nrorg, $cashierId, $finalDate);
        
        $response->addDataSet(new DataSet('cashier', $cashier->toArray()));
    }
    
    public function cashierMovement(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $cashierId = $row['CASHIER_ID'];
        $eventId = $row['EVENT_ID'];
        $nrorg = $row['NRORG'];
        $value = $row['VALUE'];
        $movementDate = $row['MOVEMENT_TIME'];
        
        $movement = $this->cashierService->cashierMovement($cashierId, $eventId, $nrorg, $value, $movementDate);
        
        $response->addDataSet(new DataSet('movement', []));
    }
    
    public function getCashiersReport(DTO\Request\Row $request, DTO\Response $response) {
        $row           = $request->getRow();
        $date          = isset($row['DATE']) ? $row['DATE'] : NULL;
        $eventId       = isset($row['EVENT_ID']) ? $row['EVENT_ID'] : NULL;
        $eventSellerId = isset($row['EVENT_SELLER_ID']) ? $row['EVENT_SELLER_ID'] : NULL;
        $nrorg         = $row['NRORG'];
        
        $cashiers      = $this->cashierService->getCashiersReport($date, $eventId, $nrorg, $eventSellerId);
        
        $response->addDataSet(new DataSet('cashiers', $cashiers));
    }
    
    public function getCashierMovements(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        $cashierId = $row['CASHIER_ID'];
        
        $movements = $this->cashierService->getCashierMovements($cashierId);
        
        $response->addDataSet(new DataSet('movements', $movements));
    }
    
    public function getCashierOrdersReport(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        $cashierId = $row['CASHIER_ID'];
        
        $report    = $this->cashierService->getCashierOrdersReport($cashierId);
        
        $response->addDataSet(new DataSet('report', $report));
    }
    
    public function getCashierReceiptsReport(DTO\Request\Row $request, DTO\Response $response) {
        $row       = $request->getRow();
        $cashierId = $row['CASHIER_ID'];
        
        $report    = $this->cashierService->getCashierReceiptsReport($cashierId);
        
        $response->addDataSet(new DataSet('report', $report));
    }
    
}