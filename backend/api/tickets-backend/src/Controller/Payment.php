<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiPayment\Controller\Payment as PayPayment;
use Zeedhi\ApiEvents\Service\Environment;


class Payment{

    public function __construct(PayPayment $payment) {
        $this->payment = $payment;
    }

    public function execPaymentCC($value, $creditCardToken, $customerId, $orderReference, $merchantKey, $gateway) {
        $payment = $this->payment->execPaymentCreditCard($value, $creditCardToken, $customerId, $orderReference, $merchantKey, $gateway);
        return $payment;
    }

}