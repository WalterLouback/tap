<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\Wallet as WalletService;
use Zeedhi\ApiEvents\Service\CreditCard as CreditCardService;
use Zeedhi\ApiEvents\Controller\Payment;
use Zeedhi\ApiEvents\Controller\Order;

use Zeedhi\ApiEvents\Model\Entities\OrdOrder;
use Zeedhi\ApiEvents\Model\Entities\PayWallet;
use Zeedhi\ApiEvents\Model\Entities\GenUser;

use Zeedhi\ApiEvents\Service\SMTP;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Helpers\Environment as Environment;


class Wallet extends Crud {
    
    const NRORG           = 'NRORG';
    const USER_ID         = 'USER_ID';
    const EVENT_ID        = 'EVENT_ID';
    const EVENT_TICKET_ID = 'EVENT_TICKET_ID';
    const PAYMENT_METHOD  = 'PAYMENT_METHOD';
    const CARD_ID         = 'CARD_ID';
    const BALANCE         = 'BALANCE';

    
    /** @var wallet */
    protected $walletdService;
    /** @var CredidCard */
    protected $creditCard;
    
    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param String $urlPrefix
     */
    public function __construct(Manager $dataSourceManager, $walletService, Payment $payment, CreditCardService $creditCard, SMTP $smtpService) {
        parent::__construct($dataSourceManager);
        $this->walletService = $walletService;
        $this->payment = $payment;
        $this->creditCardService = $creditCard;
        $this->smtpService = $smtpService;
    }

    
    /**
     * getBalance
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     *
     */
    public function getBalance(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        /* MONTA OS PARÂMETROS*/
        $nrorg = $row[self::NRORG];
        $userId = $row[self::USER_ID];
        /* */
        $wallet = $this->walletService->getBalance($nrorg, $userId);
        
        if(!isset($wallet)){
            $walletFactory = array ("ID" => null,
            "BALANCE" => 0,
            "GEN_USER" => $userId,
            "NRORG" => $nrorg);
        } else {
            $walletFactory = $this->factoryWalletDataSet($wallet);
        }
        
        $response->addDataSet(new DataSet('wallet', $walletFactory));
        
    }
    
    
    /**
     * createOrderWallet
     *
     * @author Devidy Oliveira ODHEN <devidy.oliveira@teknisa.com>
     * 
     * @param DTO\Request\Row $request
     * @param DTO\Response    $response
     */    
    public function createOrderWallet(DTO\Request\Row $request, DTO\Response $response) {
        $walletRow = $request->getRow();
        
        $verifyCard = $this->creditCardService->verifyCardownerShip($walletRow['USER_ID'], $walletRow['CARD_ID'], $walletRow['NRORG']);

        if($verifyCard['cardOwnership'] == 0)
            throw new Exception('Invalid credit card', 1);
        
        $order = $this->walletService->createOrderWallet($walletRow['USER_ID'], $walletRow['CARD_ID'], $walletRow['BALANCE'], $walletRow['EVENT_ID'], $walletRow['NRORG']);
        
        $merchantKey = $this->walletService->getMerchantKey($walletRow['NRORG']);
        
        $paymentResponse = $this->createPaymentRequest($order, $merchantKey[0]['merchantKey']);
        $paymentStatus = $paymentResponse['status'] == 'captured' ? "A": 'N';
        
        if ($paymentStatus != 'A') throw new Exception('Payment not succesful', 5);

        if(isset($verifyCard['receiveNotification']) && $verifyCard['receiveNotification'] == 1) {
            $stringBase = '<h2 style="text-align: center;">Ol&aacute;,</h2><h3 style="text-align: center;">Seu dependente <strong>' . $order->getGenUser()->getFirstName() . " " . $order->getGenUser()->getLastName() . '</strong> utilizou R$ <strong>' . $nombre_format_francais = number_format($order->getTotal(), 2, ',', ' ') . '</strong> do seu cartão com final <strong>' . $order->getPayCreditcard()->getLastNumbers() . '</strong>.</h3><h3 style="text-align: center;"><em>Obrigado por utilizar o aplicativo.</em></h3>';
            $this->smtpService->sendEmail("Notificação de compra", $stringBase, $verifyCard['email']);
        }
        
        $order->setPaymentStatus($paymentStatus);
        $order->setTransactionId($paymentResponse['transaction_id']);
        $this->walletService->updateAll();
        $orderFactory = $this->walletService->factoryOrderDataSet($order);
        
        if($paymentResponse['status'] == 'captured') {
            $wallet = $this->updateBalance($walletRow['USER_RECEIVED_ID'], $order->getTotal(), $walletRow['NRORG']);
            $walletFactory = $this->factoryWalletDataSet($wallet);
            $orderFactory = array_merge($orderFactory, $walletFactory);
        }
        
        $response->addDataSet(new DataSet('ORDERWALLET', $orderFactory));
    }
    
    public function createOrderWalletFromGUID(DTO\Request\Row $request, DTO\Response $response) {
        $walletRow    = $request->getRow();
        
        $order        = $this->walletService->createOrderWalletFromGUID($walletRow['WALLET_ID'], $walletRow['BALANCE'], $walletRow['IDENTIFIER'], $walletRow['EVENT_ID'], $walletRow['NRORG']);
        
        $orderFactory = $this->walletService->factoryOrderDataSet($order);
        
        $response->addDataSet(new DataSet('ORDERWALLET', $orderFactory));
    }
    
    public function createPaymentRequest($order, $merchantKey){
        $token = $order->getPayCreditcard()->getToken();
        $customerId = $order->getPayCreditcard()->getCustomerId();
        $gateway = $order->getPayCreditcard()->getGateway();
        $total = $order->getTotal();
        $orderReference = $order->getId();
        return $this->payment->execPaymentCC($total, $token, $customerId, $orderReference, $merchantKey, $gateway);
    }
    
    public function updateBalance($genUser, $value, $nrorg){
        $wallet = $this->walletService->getBalance($nrorg, $genUser);
        if(empty($wallet)) $wallet = $this->walletService->createWallet($genUser);
        $wallet->setBalance($wallet->getBalance() + $value);
        $wallet->setNrorg($nrorg);
        $this->walletService->updateAll($wallet);   
        return $wallet;
        
    }
    
    public function factoryWalletDataSet($wallet) {
        return array("ID" => $wallet->getId(),
            "BALANCE" => $wallet->getBalance(),
            "GEN_USER" => $wallet->getGenUser()->getId(),
            "NRORG" => $wallet->getNrorg()
        );
    }

}