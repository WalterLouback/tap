<?php
namespace Zeedhi\ApiEvents\Controller;

use Zeedhi\ApiEvents\Service\EventTicket as EventTicketService;
use Zeedhi\ApiEvents\Service\CreditCard as CreditCardService;
use Zeedhi\ApiEvents\Model\Entities\PayCreditcard;

use Zeedhi\ApiEvents\Controller\Payment;

use Zeedhi\ApiEvents\Service\SMTP;


use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO;

use Zeedhi\ApiEvents\Helpers\Environment;


class EventTicket extends Crud {
    
    const NRORG           = 'NRORG';
    const USER_ID         = 'USER_ID';
    const USER_TYPE_ID    = 'USER_TYPE_ID';
    const EVENT_ID        = 'EVENT_ID';
    const EVENT_TICKET_ID = 'EVENT_TICKET_ID';
    const PAYMENT_METHOD  = 'PAYMENT_METHOD';

    /** @var eventTicket */
    protected $eventTicketService;
    /** @var smptp */
    protected $smtpService;

    /**
     * __construct
     * 
     * @param Manager $dataSourceManager
     * @param Environment $environment
     * @param Service $eventTicketService
     */
    public function __construct(Manager $dataSourceManager, $eventTicketService, Payment $payment, CreditCardService $creditCard, SMTP $smtpService) {
        parent::__construct($dataSourceManager);
        $this->EventTicketService = $eventTicketService;
        $this->payment = $payment;
        $this->creditCardService = $creditCard;
        $this->smtpService = $smtpService;
    }

    public function createOrderTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row  = $request->getRow();
        $total = 0;/*Cristiano*/
        
        $verifyCard = $this->creditCardService->verifyCardownerShip($row['USER_ID'], $row['CREDIT_CARD_ID'], $row['NRORG']);
        if($verifyCard['cardOwnership'] == 0)
            throw new Exception('Invalid credit card', 1);
            
        /*Cristiano*/
        foreach ($row['ORDER_ITEMS'] as $items) {
            $orderValue = $this->EventTicketService->getEventTicket($items['EVENT_TICKET_ID']);
            $total += $orderValue->getPrice();
        }
        $this->EventTicketService->verifyIfCanPurchase($row['USER_ID'], $row['PAYMENT_METHOD'], $row['CREDIT_CARD_ID'], $total, $row['NRORG']);
        /*Cristiano*/
        
        $order = $this->EventTicketService->createOrderTicket($row['CREDIT_CARD_ID'], $row['EVENT_ID'], $row['USER_ID'], $row['ORDER_ITEMS']);
        
        $merchantKey = $this->EventTicketService->getMerchantKey($row['NRORG']);

        $paymentResponse = $this->createPaymentRequest($order, $merchantKey[0]['merchantKey']);
        
        $paymentStatus = $paymentResponse['status'] == 'captured' ? "A": 'N';

        $order->setPaymentStatus($paymentStatus);
        
        $order->setTransactionId($paymentResponse['transaction_id']);
        $this->EventTicketService->updateAll();
        $orderFactory = $this->EventTicketService->factoryOrderDataSet($order);
        
        if($paymentResponse['status'] == 'captured') {
            $i = 0;
            foreach($row['ORDER_ITEMS'] as $orderitem){
                foreach ($orderitem['USERS'] as $key=>$value) {
                    $ticket = $this->EventTicketService->buyTicket($orderitem['EVENT_TICKET_ID'], $value['ID'], $value['NAME'], $row['USER_ID']);
                    $ticketFactory = $this->factoryeTicketDataSet($ticket);
                    $orderFactory['TICKET'][$key + $i] = $ticketFactory;
                }
                $i = $key + 1;
            }
            if(isset($verifyCard['receiveNotification']) && $verifyCard['receiveNotification'] == 1) {
                $stringBase = '<h2 style="text-align: center;">Ol&aacute;,</h2><h3 style="text-align: center;">Seu dependente <strong>' . $order->getGenUser()->getFirstName() . " " . $order->getGenUser()->getLastName() . '</strong> utilizou R$ <strong>' . $nombre_format_francais = number_format($order->getTotal(), 2, ',', ' ')
 . '</strong> do seu cartão com final <strong>' . $order->getPayCreditcard()->getLastNumbers() . '</strong>.</h3><h3 style="text-align: center;"><em>Obrigado por utilizar o aplicativo.</em></h3>';
                
                // str_replace('USUARIO', $stringBase, $order->getGenUser()->getFirstName() . " " . $order->getGenUser()->getLastName());
                
                // $this->smtpService->sendEmail("Notificação de compra", "O Usuário " . $order->getGenUser()->getFirstName() . " " . $order->getGenUser()->getLastName() . " acaba de realizar uma compra no valor de R$" . $order->getTotal() . " no cartão " . $order->getPayCreditcard()->getLastNumbers() . ".", $verifyCard['email']);
                
                $this->smtpService->sendEmail("Notificação de compra", $stringBase, $verifyCard['email']);
            }
        }
        $response->addDataSet(new DataSet('ORDERTICKET', $orderFactory));
    }
    
    public function createPaymentRequest($order, $merchantKey){
        $token = $order->getPayCreditcard()->getToken();
        $customerId = $order->getPayCreditcard()->getCustomerId();
        $gateway = $order->getPayCreditcard()->getGateway();
        $total = $order->getTotal();
        $orderReference = $order->getId();
        return $this->payment->execPaymentCC($total, $token, $customerId, $orderReference, $merchantKey, $gateway);
    }
    
    public function factoryeTicketDataSet($ticket) {
        return array(
            "ID" => $ticket->getId(),
            "GEN_USER_ID" => $ticket->getGenUser() !== null ? $ticket->getGenUser()->getId() : null,
            "PAID_FOR_USER" => $ticket->getPaidForUser() !== null ? $ticket->getPaidForUser() : null,
            "OWNER_NAME" => $ticket->getOwnerName() !== null ? $ticket->getOwnerName() : null,
            "EVT_EVENT_TICKET_ID" => $ticket->getEvtEventTicket()->getId(),
            "STATUS" => $ticket->getStatus(),
            "GUID" => $ticket->getGuid()
        );
    }
    
    public function getTicketByEvent(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $event_id = $row[self::EVENT_ID];
        
        $eventTickets = $this->EventTicketService->getTicketByEvent($event_id);
        
        $response->addDataSet(new DataSet('eventTickets', $eventTickets));
    }
    
    public function getTicketsFromEventByEventId(DTO\Request\Row $request, DTO\Response $response) {
        $row   = $request->getRow();
        
        $event_id = $row[self::EVENT_ID];
        
        $eventTickets = $this->EventTicketService->getTicketsFromEventByEventId($event_id);
        
        $response->addDataSet(new DataSet('eventTickets', $eventTickets));
    }

    public function getUserTicketsByOrg(DTO\Request\Row $request, DTO\Response $response) {
        $userTicketsRow   = $request->getRow();
        
        /* create the parameters */
        $userId  = $userTicketsRow[self::USER_ID];
        
        $nrorg   = $userTicketsRow[self::NRORG];

        $userTickets = $this->EventTicketService->getUserTickets($userId, $nrorg);
        
        $factoryTickets = $this->factoryTickets($userTickets);
        
        $response->addDataSet(new DataSet('userTickets', $factoryTickets));
        
    }    
    
    public function factoryTickets($userTickets){
        $factory = [];
        foreach ($userTickets as $key=>$userTicket) {
            $factory[$key]['id']          = $userTicket->getId();
            $factory[$key]['status']      = $userTicket->getStatus();
            $factory[$key]['price']       = $userTicket->getEvtEventTicket()->getPrice();
            $factory[$key]['amount']      = $userTicket->getEvtEventTicket()->getAmount();
            $factory[$key]['ticketName']  = $userTicket->getEvtEventTicket()->getName();
            $factory[$key]['eventId']     = $userTicket->getEvtEventTicket()->getEvtEvent()->getId();
            $factory[$key]['name']        = $userTicket->getEvtEventTicket()->getEvtEvent()->getName();
            $factory[$key]['about']       = $userTicket->getEvtEventTicket()->getEvtEvent()->getAbout();
            $factory[$key]['imageMap']    = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageMap();
            $factory[$key]['location']    = $userTicket->getEvtEventTicket()->getEvtEvent()->getLocation();
            $factory[$key]['imageLogo']   = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageLogo();
            $factory[$key]['finalDate']   = $userTicket->getEvtEventTicket()->getEvtEvent()->getFinalDate();
            $factory[$key]['imageCover']  = $userTicket->getEvtEventTicket()->getEvtEvent()->getImageCover();
            $factory[$key]['initialDate'] = $userTicket->getEvtEventTicket()->getEvtEvent()->getInitialDate();
            $factory[$key]['userName']    = $userTicket->getGenUser() !== null ? $userTicket->getGenUser()->getFirstName() . ' ' . $userTicket->getGenUser()->getLastName(): $userTicket->getOwnerName();
        }
        
        return $factory;
    }
    
    public function createTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId        = $row[self::EVENT_ID];
        $userId         = $row[self::USER_ID];
        $nrorg          = $row[self::NRORG];
        $name           = $row['NAME'];
        $initialSale    = $this->stringToDate($row['INITIAL_SALE']);
        $finalSale      = $this->stringToDate($row['FINAL_SALE']);
        $amount         = $row['AMOUNT'];
        $price          = $row['PRICE'];
        $status         = $row['STATUS'];
        $event          = $this->EventTicketService->getEventById($eventId);
        
        if(empty($event)) throw new \Exception("Event not found!", 1);
        
        $ticket         = $this->EventTicketService->createTicket($event, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status);
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
    public function updateTicket(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $eventId        = $row[self::EVENT_ID];
        $userId         = $row[self::USER_ID];
        $nrorg          = $row[self::NRORG];
        $ticketId       = $row['ID'];
        $name           = $row['NAME'];
        $initialSale    = $this->stringToDate($row['INITIAL_SALE']);
        $finalSale      = $this->stringToDate($row['FINAL_SALE']);
        $amount         = $row['AMOUNT'];
        $price          = $row['PRICE'];
        $status         = $row['STATUS'];
        
        $ticket         = $this->EventTicketService->getEvtEventTicketById($ticketId);
        
        if(empty($ticket)) throw new \Exception("Ticket not found!", 1);
        
        $this->EventTicketService->updateTicket($ticket, $userId, $nrorg, $name, $initialSale, $finalSale, $amount, $price, $status);
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
    public function stringToDate($string) {
        $dateFormat = str_replace('/', '-', $string);
        date('Y-m-d H:i:s', strtotime($dateFormat));
		$date = new \DateTime($dateFormat);
		return $date;
    }
    
    public function disableTickets(DTO\Request\Row $request, DTO\Response $response) {
        $row = $request->getRow();
        $ticketIds = $row['TICKET_IDS'];
        
        foreach ($ticketIds as $ticketId) {
            $eventTicket = $this->EventTicketService->getEvtEventTicketById($ticketId);
            $this->EventTicketService->disableTicket($eventTicket);
        }
        
        $response->addDataSet(new DataSet('response', ["status" => "ok"]));
    }
    
}