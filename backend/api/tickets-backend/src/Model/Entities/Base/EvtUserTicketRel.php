<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class EvtUserTicketRel {
    
    
    /** @var string  */
    protected $status;
    /** @var string  */
    protected $guid;
    /** @var string  */
    protected $ownerName;
    /** @var int  */
    protected $paidForUser;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $id = 0;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEventTicket  */
    protected $evtEventTicket;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getStatus() {
        return $this->status;
    }
	public function setStatus($status = NULL) {
        $this->status = $status;
    }
    public function getGuid() {
        return $this->guid;
    }
	public function setGuid($guid = NULL) {
        $this->guid = $guid;
    }
    public function getOwnerName() {
        return $this->ownerName;
    }
	public function setOwnerName($ownerName = NULL) {
        $this->ownerName = $ownerName;
    }
	public function getPaidForUser() {
        return $this->paidForUser;
    }
	public function setPaidForUser($paidForUser = NULL) {
        $this->paidForUser = $paidForUser;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getEvtEventTicket() {
        return $this->evtEventTicket;
    }
	public function setEvtEventTicket(\Zeedhi\ApiEvents\Model\Entities\EvtEventTicket $evtEventTicket = NULL) {
        $this->evtEventTicket = $evtEventTicket;
    }
}