<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class GenConfiguration {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg = 0;
    /** @var string  */
    protected $merchantKey;
    /** @var string  */
    protected $fcmServerKey;
    /** @var string  */
    protected $logoImage;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
	public function getMerchantKey() {
        return $this->merchantKey;
    }
	public function setMerchantKey($merchantKey = NULL) {
        $this->merchantKey = $merchantKey;
    }
	public function getFcmServerKey() {
        return $this->fcmServerKey;
    }
	public function setFcmServerKey($fcmServerKey = NULL) {
        $this->fcmServerKey = $fcmServerKey;
    }
	public function getLogoImage() {
        return $this->logoImage;
    }
	public function setLogoImage($logoImage = NULL) {
        $this->logoImage = $logoImage;
    }
}