<?php

namespace Zeedhi\ApiEvents\Model\Entities\Base;

/**
 * OrdCashierMovement
 */
class OrdCashierMovement
{
    /**
     * @var \DateTime|null
     */
    protected $movementDate;

    /**
     * @var string|null
     */
    protected $value;
    
    /**
     * @var string|null
     */
    protected $type_;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var \Zeedhi\ApiEvents\Model\Entities\OrdCashier
     */
    protected $ordCashier;

    /** @var int  */
    protected $nrorg;
    
    /** @var int  */
    protected $createdAt;
    
    /** @var int  */
    protected $createdBy;
    
    /** @var \Datetime  */
    protected $modifiedAt;
    
    /** @var int  */
    protected $modifiedBy;

    /**
     * Set movementDate.
     *
     * @param \DateTime|null $movementDate
     *
     * @return OrdCashierMovement
     */
    public function setMovementDate($movementDate = null)
    {
        $this->movementDate = $movementDate;

        return $this;
    }

    /**
     * Get movementDate.
     *
     * @return \DateTime|null
     */
    public function getMovementDate()
    {
        return $this->movementDate;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return OrdCashierMovement
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ordCashier.
     *
     * @param \Zeedhi\ApiEvents\Model\Entities\OrdCashier|null $ordCashier
     *
     * @return OrdCashierMovement
     */
    public function setOrdCashier(\Zeedhi\ApiEvents\Model\Entities\OrdCashier $ordCashier = null)
    {
        $this->ordCashier = $ordCashier;

        return $this;
    }

    /**
     * Get ordCashier.
     *
     * @return \Zeedhi\ApiEvents\Model\Entities\OrdCashier|null
     */
    public function getOrdCashier()
    {
        return $this->ordCashier;
    }
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
    
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
    
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
    
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getNrorg() {
        return $this->nrorg;
    }
    
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    
    public function getType() {
        return $this->type_;
    }
    
	public function setType($type_ = NULL) {
        $this->type_ = $type_;
    }
}
