<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class EvtTagUser {
    
    
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenTag  */
    protected $evtTag;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getEvtTag() {
        return $this->evtTag;
    }
	public function setEvtTag(\Zeedhi\ApiEvents\Model\Entities\GenTag $evtTag = NULL) {
        $this->evtTag = $evtTag;
    }
}