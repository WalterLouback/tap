<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class OrdStorePayMethod {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var \Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod  */
    protected $paymentMethod;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdConfigStore  */
    protected $ordConfigStore;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getPaymentMethod() {
        return $this->paymentMethod;
    }
	public function setPaymentMethod(\Zeedhi\ApiEvents\Model\Entities\PayPaymentMethod $paymentMethod = NULL) {
        $this->paymentMethod = $paymentMethod;
    }
	public function getOrdConfigStore() {
        return $this->ordConfigStore;
    }
	public function setOrdConfigStore(\Zeedhi\ApiEvents\Model\Entities\OrdConfigStore $ordConfigStore = NULL) {
        $this->ordConfigStore = $ordConfigStore;
    }
}