<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class OrdWorkflow {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenStatus  */
    protected $initialStatus;
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenStatus  */
    protected $paymentMoment;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg) {
        $this->nrorg = $nrorg;
    }
    public function getInitialStatus() {
        return $this->initialStatus;
    }
    public function setInitialStatus(\Zeedhi\ApiEvents\Model\Entities\GenStatus $initialStatus = NULL) {
        $this->initialStatus = $initialStatus;
    }
    public function getPaymentMoment() {
        return $this->paymentMoment;
    }
    public function setPaymentMoment(\Zeedhi\ApiEvents\Model\Entities\GenStatus $paymentMoment = NULL) {
        $this->paymentMoment = $paymentMoment;
    }
}