<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class OrdConfigStore {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $delivers;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEvent  */
    protected $event;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
    public function getDelivers() {
        return $this->delivers;
    }
	public function setDelivers($delivers) {
        $this->delivers = $delivers;
    }
	public function getEvent() {
        return $this->event;
    }
	public function setEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $event = NULL) {
        $this->event = $event;
    }
}