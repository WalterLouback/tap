<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class PayCreditcard {
    
    
    /** @var \Zeedhi\ApiEvents\Model\Entities\GenUser  */
    protected $genUser;
    /** @var int  */
    protected $id = 0;
    /** @var string  */
    protected $token;
    /** @var string  */
    protected $customerId;
    /** @var string  */
    protected $gateway;
    /** @var string  */
    protected $status;
    /** @var int  */
    protected $lastNumbers;
    /** @var string  */
    protected $flag;
    /** @var \Datetime  */
    protected $expirationDate;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getGenUser() {
        return $this->genUser;
    }
	public function setGenUser(\Zeedhi\ApiEvents\Model\Entities\GenUser $genUser = NULL) {
        $this->genUser = $genUser;
    }
	public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getToken() {
        return $this->token;
    }
	public function setToken($token) {
        $this->token = $token;
    }
	public function getCustomerId() {
        return $this->customerId;
    }
	public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }
	public function getGateway() {
        return $this->gateway;
    }
	public function setGateway($gateway) {
        $this->gateway = $gateway;
    }
	public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	public function getLastNumbers() {
        return $this->lastNumbers;
    }
	public function setLastNumbers($lastNumbers) {
        $this->lastNumbers = $lastNumbers;
    }
	public function getFlag() {
        return $this->flag;
    }
	public function setFlag($flag) {
        $this->flag = $flag;
    }
	public function getExpirationDate() {
        return $this->expirationDate;
    }
	public function setExpirationDate(\Datetime $expirationDate) {
        $this->expirationDate = $expirationDate;
    }
}