<?php
namespace Zeedhi\ApiEvents\Model\Entities\Base;


abstract class OrdProductGroup {
    
    
    /** @var int  */
    protected $id = 0;
    /** @var int  */
    protected $nrorg;
    /** @var string  */
    protected $name;
    /** @var \Zeedhi\ApiEvents\Model\Entities\OrdMenu  */
    protected $ordMenu;
    /** @var \Zeedhi\ApiEvents\Model\Entities\EvtEvent  */ 
    protected $evtEvent;
    /** @var \Datetime  */
    protected $createdAt;
    /** @var int  */
    protected $createdBy;
    /** @var \Datetime  */
    protected $modifiedAt;
    /** @var int  */
    protected $modifiedBy;
    /** @var int  */
    protected $parentId;
    
	public function getCreatedAt() {
        return $this->createdAt;
    }
    public function setCreatedAt() {
        $this->createdAt = new \DateTime();
    }
	public function getModifiedAt() {
        return $this->modifiedAt;
    }
    public function setModifiedAt() {
        $this->modifiedAt = new \DateTime();
    }
	public function getCreatedBy() {
        return $this->createdBy;
    }
    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
	public function getModifiedBy() {
        return $this->modifiedBy;
    }
    public function setModifiedBy($modifiedBy) {
        $this->modifiedBy = $modifiedBy;
    }
    
    public function getId() {
        return $this->id;
    }
	public function setId($id) {
        $this->id = $id;
    }
	public function getName() {
        return $this->name;
    }
	public function setName($name) {
        $this->name = $name;
    }
	public function getOrdMenu() {
        return $this->ordMenu;
    }
	public function setOrdMenu(\Zeedhi\ApiEvents\Model\Entities\OrdMenu $ordMenu = NULL) {
        $this->ordMenu = $ordMenu;
    }
	public function getEvtEvent() {
        return $this->evtEvent;
    }
	public function setEvtEvent(\Zeedhi\ApiEvents\Model\Entities\EvtEvent $evtEvent = NULL) {
        $this->evtEvent = $evtEvent;
    }
	public function getNrorg() {
        return $this->nrorg;
    }
	public function setNrorg($nrorg = NULL) {
        $this->nrorg = $nrorg;
    }
    public function getParentId() {
        return $this->parentId;
    }
	public function setParentId($parentId = NULL) {
        $this->parentId = $parentId;
    }
}