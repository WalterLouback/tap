<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenDependent extends \Zeedhi\ApiEvents\Model\Entities\Base\GenDependent {
    
    const DEPENDENT_ID = 'DEPENDENT_ID';
    const MONTHLY_LIMIT = 'MONTHLY_LIMIT';
    const RECEIPTS_TO = 'RECEIPTS_TO';
    
    public function build($entityManager) {
        $this->getDependent()->build($entityManager);
    }
    
    public static function manyToArray($dependents) {
        $arrays = [];
        foreach ($dependents as $dependent) {
            array_push($arrays, $dependent->toArray());
        }
        return $arrays;
    }
    
    public function toArray() {
        return array(
            'USER_DATA' => $this->getDependent()->toArray($this->getNrorg()),
            'DEPENDENCY_DATA' => array(
                'MONTHLY_LIMIT' => $this->getMonthlyLimit(),
                'RECEIPTS_TO' => $this->getReceiptsTo(),
                'STATUS' => $this->getStatus(),
                'CAN_USE_MAIN_CARD' => $this->getCanUseMainCard() == 'T' ? true : false
            )
        );
    }
    
}