<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class OrdMenu extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdMenu {
    
    public function build($entityManager) {
        $this->setProductGroups($entityManager);
        $this->isCurrentWorkshift($entityManager);
    }
    
    public function getProductGroups() {
        return property_exists($this, 'productGroups') ? $this->productGroups : [];
    }

    public function setProductGroups($entityManager) {
        $productGroup  = 'Zeedhi\ApiEvents\Model\Entities\OrdProductGroup';
        $menuId        = $this->getId();
        
        $productGroups = $entityManager->createQuery(
            "
            SELECT pg
            FROM $productGroup pg
            JOIN pg.ordMenu as m
            WHERE m.id = $menuId
            ORDER BY pg.name
            "
        )->getResult();

        foreach ($productGroups as $productGroup) {
            $productGroup->build($entityManager);
        }
        
        $this->productGroups = $productGroups;
    }

    public function getShiftsOfDay($date, $entityManager) {
        $workshift = 'Zeedhi\ApiEvents\Model\Entities\GenShift';
        $menuId   = $this->getId();
        
        $dayOfWeek = strftime("%A", strtotime($date));

        $daysOfWeeksArray = array(
            'Sunday'    => 0, 'Monday' => 1, 'Tuesday'   => 2, 'Wednesday' => 3, 
            'Thursday'  => 4, 'Friday' => 5, 'Saturday'  => 6
        );

        $formatedDay = $daysOfWeeksArray[$dayOfWeek];
        
        $dayWorkshifts = $entityManager->createQuery(
            "
            SELECT ws.initialTime, ws.finalTime 
            FROM $workshift ws
            JOIN ws.evtEventMenu em
            WHERE em.ordMenu = $menuId
            AND ws.day = $formatedDay
            "
        )->getResult();

        return $dayWorkshifts;
    }
    
    public function isCurrentWorkshift($entityManager=NULL) {
        if (!$entityManager) {
            if (property_exists($this, 'isCurrentWorkshift')) return $this->isCurrentWorkshift;
            else return NULL;
        }
        
        $isCurrentWorkshift     = false;
        
        /* Obter horários de funcionamento da loja no dia de hoje */
        $today        = new \DateTime();
        $formatedDate = $today->format('Y-m-d H:i:s');
        $shifts       = $this->getShiftsOfDay($formatedDate, $entityManager);
        
        /* Percorrer os turnos e verificar se horário atual se encaixa em algum */
        foreach ($shifts as $shift) {
            /* Obtendo horário atual */
            $currHours = $today->format('H');
            $currMins  = $today->format('i');
            /* Obtendo horário de início do turno */
            $inDate    = $shift['initialTime'];
            $inHours   = $inDate->format('H');
            $inMins    = $inDate->format('i');
            /* Obtendo horário de término do turno */
            $finDate   = $shift['finalTime'];
            $finHours  = $finDate->format('H');
            $finMins   = $finDate->format('i');
            
            /* Verificando se horário atual se encaixa no turno */
            if ($currHours > $inHours || ($currHours == $inHours && $currMins >= $inMins)) {
                if ($currHours < $finHours || ($currHours == $finHours && $currMins <= $finMins)) {
                    $isCurrentWorkshift = true;
                    break;
                }
            }
        }
        
        $this->isCurrentWorkshift = $isCurrentWorkshift;
        
        return $isCurrentWorkshift;
    }
    
    public function toArray() {
        $array = [];
        $array['name']  = $this->getName();
        $array['nrorg'] = $this->getNrorg();
        $array['isCurrentWorkshift'] = $this->isCurrentWorkshift();
        $array['productGroups'] = OrdProductGroup::manyToArray($this->getProductGroups());
        
        return $array;
    }
    
    public static function manyToArray($menus) {
        $arrays = [];
        foreach ($menus as $menu) {
            array_push($arrays, $menu->toArray());
        }
        return $arrays;
    }
    
}