<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenConfiguration extends \Zeedhi\ApiEvents\Model\Entities\Base\GenConfiguration {
    
    public function toArray() {
        return array(
            'LOGO_IMAGE' => $this->getLogoImagE()
        );
    }
    
}