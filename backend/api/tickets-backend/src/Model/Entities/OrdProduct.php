<?php
namespace Zeedhi\ApiEvents\Model\Entities;

use Zeedhi\ApiEvents\Helpers\General as General;

class OrdProduct extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdProduct {
    
    public function build($entityManager, $menuId=NULL, $productGroupId=NULL) {
        $this->setStatus($entityManager, $menuId);
        $this->setEstimatedTime($entityManager, $menuId);
        $this->setMenu($entityManager, $menuId);
        $this->setProductGroup($entityManager, $productGroupId);
        $this->setMenuProduct($entityManager, $menuId);
    }
    
    public function getMenu() {
        return property_exists($this, 'menu') ? $this->menu : NULL;
    }
    
    public function getProductGroup() {
        return property_exists($this, 'productGroup') ? $this->productGroup : NULL;
    }
    
    public function getStatus() {
        return property_exists($this, 'status') ? $this->status : NULL;
    }
    
    public function getMenuProduct() {
        return property_exists($this, 'menuProduct') ? $this->menuProduct : NULL;
    }
    
    public function getEstimatedTime() {
        return property_exists($this, 'estimatedTime') ? $this->estimatedTime : NULL;
    }
    
    public function setMenu($entityManager, $menuId) {
        if ($menuId != NULL)
            $this->menu = $entityManager->getRepository(OrdMenu::class)->find($menuId);
    }
    
    public function setProductGroup($entityManager, $productGroupId) {
        if ($productGroupId != NULL)
            $this->productGroup = $entityManager->getRepository(OrdProductGroup::class)->find($productGroupId);
    }
    
    public function setMenuProduct($entityManager, $menuId) {
        if ($menuId != NULL)
            $this->menuProduct = $entityManager->getRepository(OrdMenuProduct::class)->findOneBy(['ordProduct' => $this->getId(), 'ordMenu' => $menuId]);
    }
    
    public function setStatus($entityManager, $menuId) {
        if ($menuId != NULL) {
            $menuProduct  = $entityManager->getRepository(OrdMenuProduct::class)->findOneBy(['ordProduct' => $this->getId(), 'ordMenu' => $menuId]);
            $this->status = $menuProduct->getStatus();
        }
    }
    
    public function setEstimatedTime($entityManager, $menuId) {
        if ($menuId != NULL) {
            $menuProduct  = $entityManager->getRepository(OrdMenuProduct::class)->findOneBy(['ordProduct' => $this->getId(), 'ordMenu' => $menuId]);
            $this->estimatedTime = $menuProduct->getEstimatedTime();
        }
    }
    
    public static function manyToArray($products) {
        $arrays = [];
        foreach ($products as $product) {
            array_push($arrays, $product->toArray());
        }
        
        return $arrays;
    }
    
    public function getClone($entityManager, $productGroup=NULL) {
        $newProduct = new OrdProduct();
        
        $newProduct->setName($this->getName());
        $newProduct->setDetail($this->getDetail());
        $newProduct->setPrice($this->getPrice());
        $newProduct->setNrorg($this->getNrorg());
        $newProduct->setImage($this->getImage());
        
        $entityManager->persist($newProduct);
        
        $this->build($entityManager);
        foreach ($this->getExtras() as $extra) {
            $e = $extra->getClone($entityManager);
            $e->setOrdProduct($newProduct);
        }
        
        return $newProduct;
    }
    
    public function toArray() {
        $array = [];
        
        $array['id'] = $this->getMenuProduct() != NULL ? $this->getMenuProduct()->getId() : NULL;
        $array['productId'] = $this->getId();
        $array['name'] = $this->getName();
        $array['price'] = $this->getPrice();
        $array['estimatedTime'] = $this->getEstimatedTime();
        $array['status'] = $this->getStatus();
        $array['detail'] = $this->getDetail();
        $array['image'] = $this->getImage();
        $array['extras'] = OrdExtra::manyToArray($this->getExtras());
        $array['menuId'] = $this->getMenu() ? $this->getMenu()->getId() : NULL;
        $array['productGroupId'] = $this->getProductGroup() ? $this->getProductGroup()->getId() : NULL;
        
        return $array;
    }
    
    public function getExtras() {
        return property_exists($this, 'extras') ? $this->extras : [];
    }
    
    private function setExtras($entityManager) {
        $extra  = 'Zeedhi\ApiEvents\Model\Entities\OrdExtra';
        
        $id = $this->getId();
        
        /* Realizando Query 
           e  = Extra
           op = Option
           p  = Product
        */
        $extras = $entityManager->createQuery(
            "
            SELECT e
            FROM $extra e
            JOIN e.ordMenuProduct as mp
            JOIN mp.ordProduct as p
            WHERE p.id = $id
            ORDER BY e.id
            "
        )->getResult();
        
        foreach ($extras as $extra) {
            $extra->build($entityManager);
        }
        
        $this->extras = $extras;
    }
    
}