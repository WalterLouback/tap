<?php
namespace Zeedhi\ApiEvents\Model\Entities;

use Zeedhi\ApiEvents\Helpers\General as General;

class OrdOption extends \Zeedhi\ApiEvents\Model\Entities\Base\OrdOption {
    
    public static function manyToArray($options) {
        $arrays = [];
        foreach ($options as $option) {
            $array = $option->toArray();
            array_push($arrays, $array);
        }
        
        return $arrays;
    }
    
    public function toArray() {
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['price'] = $this->getPrice();
        
        return $array;
    }
    
}