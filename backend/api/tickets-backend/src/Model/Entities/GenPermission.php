<?php
namespace Zeedhi\ApiEvents\Model\Entities;


class GenPermission extends \Zeedhi\ApiEvents\Model\Entities\Base\GenPermission {
    
    public function toArray() {
        return array(
            'ID' => $this->getId(),
            'NRORG' => $this->getNrorg()
        );
    }
    
}