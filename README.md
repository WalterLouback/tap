# produto

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### code push, atualização do app 

## ao gerar a apk a chave CodePushDeploymentKey na configuração será a chave que receberá as atualizações correspondentes
## Não pode gerar apks com versão com digitos superiores a 3, exemplos que não podem, 1.9.2.1 , 2.0.0.0, com esse numero de versão o
## aplicativo não receberá codepush
## ao subir o aplicativo habilitar ele e colocar as versões afetadas https://appcenter.ms/users/mtcteknisa/apps/EatTakeTAA/distribute/code-push
## subir build para o appcenter do minas tenis club
# chave instanciada em config.xml:6TbLMZjVoBj4u-xVLtnVdt4f0-lTbaloE-QEK
appcenter codepush release-cordova -a mtcteknisa/EatTakeTAA -d MinasTC

## subir build para o appcenter de teste,(rodrigo)
# chave instanciada em config.xml:OFxYXqX_7YhXxhTp2zRA2zfpeVWJXh3YMsU3i
appcenter codepush release-cordova -a mtcteknisa/EatTakeTAA -d Staging

## subir build para o appcenter dos clientes gerais
# chave instanciada em config.xml:KtMMnJszRrR0AB04vGycwTHkBlr3BmkpkJ03z
appcenter codepush release-cordova -a mtcteknisa/EatTakeTAA -d Production

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
